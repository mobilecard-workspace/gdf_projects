package org.addcel.gdf.constant;

public class Url {
	
	private static final String DEVELOPMENT = "http://50.57.192.213:8080/GDFServicios/";
	private static final String PRODUCTION = "http://50.57.192.210:8080/GDFServices/";
	
//			"http://50.57.192.210:8080/GDFServicios/";
	
	private static final String BASE_URL = PRODUCTION;
	

	public static final String CONSUMIDOR_TERMINOS = BASE_URL + "ConsumidorTerminos";
	public static final String BUSCA_COMUNICADOS = BASE_URL + "BuscaComunicados";
	public static final String CONSUMIDOR = BASE_URL + "Consumidor";
	public static final String CONSULTA_PAGOS = BASE_URL + "ConsultaPagos";
	public static final String REENVIAR_RECIBO = BASE_URL + "ReenvioReciboGDF";
	public static final String GET_TOKEN = BASE_URL + "getToken";
	public static final String PAGO_PROSA = BASE_URL + "ProsaPago";
}
