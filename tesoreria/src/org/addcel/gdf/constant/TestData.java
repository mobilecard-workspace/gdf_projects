package org.addcel.gdf.constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class TestData {

	public static JSONArray vencidos;
	private static final String TAG = "TestData";
	
	public static void setVencidos() {
		vencidos = new JSONArray();
		
		JSONObject vencido1 = new JSONObject();
		JSONObject vencido2 = new JSONObject();
		
		try {
			vencido1.put("periodo", "6/2013");
			vencido1.put("saldo", "10.00");
			vencido1.put("emitido", "20.00");
			vencido1.put("pagado", "10.00");
			
			vencido2.put("periodo", "10/2013");
			vencido2.put("saldo", "12.00");
			vencido2.put("emitido", "20.00");
			vencido2.put("pagado", "0.00");
			
			vencidos.put(vencido1);
			vencidos.put(vencido2);
			
			Log.i(TAG, vencidos.toString());
		} catch (JSONException e) {
			Log.e(TAG, "", e);
		}
	}
}
