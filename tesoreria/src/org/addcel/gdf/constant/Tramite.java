package org.addcel.gdf.constant;

public class Tramite {
	public static final int TENENCIA = 0;
	public static final int INFRACCIONES = 1;
	
	public static final int NOMINA = 0;
	
	public static final int PREDIAL = 0;
	public static final int PREDIAL_VENCIDO = 1;
	public static final int AGUA = 2;
}
