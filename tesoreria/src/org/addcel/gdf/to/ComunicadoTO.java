package org.addcel.gdf.to;

public class ComunicadoTO {
	
	private int idComunicado;
	private String contenido;
	private int idEmpresa;
	private int idAplicacion;
	private int prioridad;
	private String fgColor;
	private String bgColor;
	private int tamFuente;
	
	public int getIdComunicado() {
		return idComunicado;
	}
	public void setIdComunicado(int idComunicado) {
		this.idComunicado = idComunicado;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public int getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public int getIdAplicacion() {
		return idAplicacion;
	}
	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
	public int getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}
	public String getFgColor() {
		return fgColor;
	}
	public void setFgColor(String fgColor) {
		this.fgColor = fgColor;
	}
	public String getBgColor() {
		return bgColor;
	}
	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}
	public int getTamFuente() {
		return tamFuente;
	}
	public void setTamFuente(int tamFuente) {
		this.tamFuente = tamFuente;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return contenido;
	}
}
