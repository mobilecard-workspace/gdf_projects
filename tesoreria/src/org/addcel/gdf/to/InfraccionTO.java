package org.addcel.gdf.to;

import org.json.JSONObject;

public class InfraccionTO {

	private String placa;
	private String folio;
	private String lineaCaptura;
	private String monto;
	private String totalPago;
	private String fechaInfraccion;
	private String actualizacion;
	private String recargos;
	private String diasMulta;
	private int codeCalc;
	
	private JSONObject json;
	
	public InfraccionTO() {
		// TODO Auto-generated constructor stub
	}
	
	public InfraccionTO(JSONObject json) {
		this.json = json;
		
		this.placa = json.optString("placa");
		this.monto = json.optString("multa");
		this.folio = json.optString("folio");
		this.lineaCaptura = json.optString("linea_captura");
		this.totalPago = json.optString("totalPago");
		this.fechaInfraccion = json.optString("fechainfraccion");
		this.actualizacion = json.optString("actualizacion");
		this.recargos = json.optString("recargos");
		this.diasMulta = json.optString("dias_multa");
		this.codeCalc = json.optInt("codeCalc");
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getLineaCaptura() {
		return lineaCaptura;
	}

	public void setLineaCaptura(String lineaCaptura) {
		this.lineaCaptura = lineaCaptura;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getTotalPago() {
		return totalPago;
	}

	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}

	public String getFechaInfraccion() {
		return fechaInfraccion;
	}

	public void setFechaInfraccion(String fechaInfraccion) {
		this.fechaInfraccion = fechaInfraccion;
	}

	public String getActualizacion() {
		return actualizacion;
	}

	public void setActualizacion(String actualizacion) {
		this.actualizacion = actualizacion;
	}

	public String getRecargos() {
		return recargos;
	}

	public void setRecargos(String recargos) {
		this.recargos = recargos;
	}

	public String getDiasMulta() {
		return diasMulta;
	}

	public void setDiasMulta(String diasMulta) {
		this.diasMulta = diasMulta;
	}

	public int getCodeCalc() {
		return codeCalc;
	}

	public void setCodeCalc(int codeCalc) {
		this.codeCalc = codeCalc;
	}
	
	public JSONObject getJson() {
		return json;
	}

	public void setJson(JSONObject json) {
		this.json = json;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return folio + " - $" + totalPago; 
	}
}
