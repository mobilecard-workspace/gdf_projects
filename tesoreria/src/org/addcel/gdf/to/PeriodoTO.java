package org.addcel.gdf.to;

import java.util.List;

import org.addcel.util.Text;
import org.json.JSONObject;

import android.content.Context;
import android.widget.ArrayAdapter;

public class PeriodoTO {
	
    private String importe ;
    private String banco ;
    private String concepto ;
    private int idBitacora ;
    private String cuentaP ;
    private int idProducto ;
    private String lineaCaptura ;
    private String reduccion ;
    private int statusPago ;
    private String totalPago ;
    private int idUsuario ;
    private String errorCel ;
    private int intImpuesto ;
    private String vencimiento ;
    private String bimestre ;
    private String error_descripcion ;
    
    private JSONObject json;
    
    public PeriodoTO() {}
    
    public PeriodoTO(JSONObject _json) {
    	
    	json = _json;
    	
    	importe = _json.optString("importe");
    	banco = _json.optString("banco");
    	concepto = _json.optString("concepto");
    	idBitacora = _json.optInt("id_bitacora");
    	cuentaP = _json.optString("cuentaP");
    	idProducto = _json.optInt("id_producto");
    	lineaCaptura = _json.optString("linea_captura");
    	reduccion = _json.optString("reduccion");
    	statusPago = _json.optInt("statusPago");
    	totalPago = _json.optString("totalPago");
    	idUsuario = _json.optInt("id_usuario");
    	errorCel = _json.optString("error_cel");
    	intImpuesto = _json.optInt("intImpuesto");
    	vencimiento = _json.optString("vencimiento");
    	bimestre = _json.optString("bimestre");
    	error_descripcion = _json.optString("error_descripcion");
    }

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public int getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}

	public String getCuentaP() {
		return cuentaP;
	}

	public void setCuentaP(String cuentaP) {
		this.cuentaP = cuentaP;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getLineaCaptura() {
		return lineaCaptura;
	}

	public void setLineaCaptura(String lineaCaptura) {
		this.lineaCaptura = lineaCaptura;
	}

	public String getReduccion() {
		return reduccion;
	}

	public void setReduccion(String reduccion) {
		this.reduccion = reduccion;
	}

	public int getStatusPago() {
		return statusPago;
	}

	public void setStatusPago(int statusPago) {
		this.statusPago = statusPago;
	}

	public String getTotalPago() {
		return totalPago;
	}

	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getErrorCel() {
		return errorCel;
	}

	public void setErrorCel(String errorCel) {
		this.errorCel = errorCel;
	}

	public int getIntImpuesto() {
		return intImpuesto;
	}

	public void setIntImpuesto(int intImpuesto) {
		this.intImpuesto = intImpuesto;
	}

	public String getVencimiento() {
		return vencimiento;
	}

	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}

	public String getBimestre() {
		return bimestre;
	}

	public void setBimestre(String bimestre) {
		this.bimestre = bimestre;
	}

	public String getError_descripcion() {
		return error_descripcion;
	}

	public void setError_descripcion(String error_descripcion) {
		this.error_descripcion = error_descripcion;
	}

	public JSONObject getJson() {
		return json;
	}

	public void setJson(JSONObject json) {
		this.json = json;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return bimestre + " - " + Text.formatCurrency(Double.valueOf(totalPago), true);
	}
	
	
	public static ArrayAdapter<PeriodoTO> BuildAdapter(Context _c, List<PeriodoTO> periodos) {
		ArrayAdapter<PeriodoTO> adapter = new ArrayAdapter<PeriodoTO>(_c, android.R.layout.simple_spinner_item, periodos);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		return adapter;
	}
	
	
    
    

}
