package org.addcel.gdf.view;

import java.util.List;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.TipoConsulta;
import org.addcel.gdf.constant.Tramite;
import org.addcel.gdf.constant.Url;
import org.addcel.gdf.sql.CuentaPredial;
import org.addcel.gdf.sql.PredialDataSource;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class PredialVencidoActivity extends SherlockActivity {
	
	Spinner cuentaSpinner;
	EditText cuentaText, confCuentaText;	
	Button calcularButton, pagarButton;
	boolean ingresoDirecto;
	SessionManager session;
	JSONObject requestPago;
	PredialDataSource dataSource;


	private static final String TAG = "PredialVencidoActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_predial_vencido);
		session = new SessionManager(PredialVencidoActivity.this);
		
		ingresoDirecto = true;
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_predial, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
		case R.id.action_predial:
			if (session.isLoggedIn()) {
				Dialogos.showPredialDialog(Tramite.PREDIAL_VENCIDO, this, dataSource);
			} else {
				CustomToast.build(PredialVencidoActivity.this, "Debes iniciar sesi�n mobilecard para poder guardar una cuenta.");
			}
		break;
		case R.id.action_login:
			if (!session.isLoggedIn()) {
				startActivity(new Intent(PredialVencidoActivity.this, LoginActivity.class));
			} else {
				Dialogos.makeYesNoAlert(PredialVencidoActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
					
					@Override
					public void ok(DialogInterface dialog, int id) {
						// TODO Auto-generated method stub
						session.logoutUser();
						dialog.dismiss();
					}
					
					@Override
					public void cancel(DialogInterface dialog, int id) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
			}
			break;
		case R.id.action_registro:
			if (session.isLoggedIn()) {
				CustomToast.build(PredialVencidoActivity.this, "Ya eres usuario MobileCard.");
			} else {
				startActivity(new Intent(PredialVencidoActivity.this, RegistroActivity.class));
			}
			break;
		case R.id.action_update:
			if (session.isLoggedIn()) {
				startActivity(new Intent(PredialVencidoActivity.this, DataUpdateActivity.class));
			} else {
				CustomToast.build(PredialVencidoActivity.this, "Inicia Sesi�n para actualizar tus datos");
			}
			break;
		case R.id.action_password:
			if (session.isLoggedIn()) {
				CustomToast.build(PredialVencidoActivity.this, "Ya eres usuario MobileCard.");
			} else {
				startActivity(new Intent(PredialVencidoActivity.this, PassRecoverActivity.class));
			}
			break;

		default:
			break;
	}
		
		
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		dataSource = new PredialDataSource(PredialVencidoActivity.this);
		dataSource.open();
		List<CuentaPredial> cuentas = dataSource.getAllCuentas();
		ArrayAdapter<CuentaPredial> adapter = new ArrayAdapter<CuentaPredial>(PredialVencidoActivity.this, android.R.layout.simple_spinner_item, cuentas);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		cuentaSpinner.setAdapter(adapter);
		dataSource.close();
	}
	
	private void initGUI() {
		cuentaSpinner = (Spinner) findViewById(R.id.spinner_predial);
		cuentaText = (EditText) findViewById(R.id.edit_predial);
		confCuentaText = (EditText) findViewById(R.id.edit_predial_conf);
		calcularButton = (Button) findViewById(R.id.button_consultar);
		pagarButton = (Button) findViewById(R.id.button_pagar);
		
		cuentaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (0 == arg2) {
					ingresoDirecto = true;
					cuentaText.setVisibility(View.VISIBLE);
					confCuentaText.setVisibility(View.VISIBLE);
				} else {
					ingresoDirecto = false;
					cuentaText.setVisibility(View.GONE);
					confCuentaText.setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				ingresoDirecto = true;
				cuentaText.setVisibility(View.VISIBLE);
				confCuentaText.setVisibility(View.VISIBLE);
				
			}
		});
		
		calcularButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sendData();
			}
		});
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		
		initData();
	}
	
	private String getCuenta(boolean ingresoDirecto) {
		if (ingresoDirecto)
			return cuentaText.getText().toString().trim();
		else
			return ((CuentaPredial) cuentaSpinner.getSelectedItem()).getCuenta();
	}
	
	private void sendData() {
		if (session.isLoggedIn()) {
			if (validar()) {
				String cuenta = getCuenta(ingresoDirecto);
				Log.i(TAG, cuenta);
				String request = Request.buildPVencidosRequest(cuenta, TipoConsulta.ADEUDOS, (String[]) null);
				Log.i(TAG, request);
				new WebServiceClient(new ConsultaListener(), PredialVencidoActivity.this, true, Url.CONSUMIDOR).execute(request);
			}
		} else {
			CustomToast.build(PredialVencidoActivity.this, "Debes iniciar sesi�n mobilecard para poder calcular tu pago.");
			
		}
	}
	
	private boolean validar() {
		if (ingresoDirecto) {
			
			String cuenta = cuentaText.getText().toString().trim();
			String confirmacion = confCuentaText.getText().toString().trim();
			
			if (cuenta == null || "".equals(cuenta)) {
				cuentaText.requestFocus();
				cuentaText.setError("El campo cuenta no puede ir vac�o");
				return false;
			}
			
			if (confirmacion == null || "".equals(confirmacion)) {
				confCuentaText.requestFocus();
				confCuentaText.setError("El campo confirmaci�n no puede ir vac�o");
				return false;
			}
			
			if (!cuenta.equals(confirmacion)) {
				confCuentaText.requestFocus();
				confCuentaText.setError("Confirmaci�n no coincide con cuenta capturada");
				return false;
			}
			return true;
		} else {
			return true;
		}
	}
	
	private class ConsultaListener implements WSResponseListener {
		
		private static final String TAG = "ConsultaListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (response != null && !"".equals(response)) {
				response = AddcelCrypto.decryptSensitive(response);
				Log.i(TAG, response);
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					
					int idError = json.optInt("idError", -1);
					String mensajeError = json.optString("mensajeError", "Error de conexi�n. Intente de nuevo m�s tarde.");
					
					switch (idError) {
					case 0:
						JSONArray detalle = json.optJSONArray("detalle");
						
						if (detalle.length() > 0) {
							Intent intent = new Intent(PredialVencidoActivity.this, ListaVencidosActivity.class);
							intent.putExtra("json", json.toString());
							startActivity(intent);
						} else {
							CustomToast.build(PredialVencidoActivity.this, mensajeError);
						}
						break;

					default:
						CustomToast.build(PredialVencidoActivity.this, mensajeError);
						break;
					}
					
					
					/*
					 * {
					 * 	"idError":0,
					 * 	"mensajeError":null,
					 * 	"cadbc":null,
					 * 	"cuenta":"048198090002",
					 * 	"detalle":[
					 * 		{
					 * 			"actualizacion":"10.50",
					 * 			"cond_Gast_Ejec":null,
					 * 			"cond_mult_omi":null,
					 * 			"cond_rec":0,
					 * 			"impuesto_bimestral":"42",
					 * 			"multa_omision":"0",
					 * 			"periodo":"20082",
					 * 			"recargos":"6.28",
					 * 			"requerido":"NO",
					 * 			"subtotal":null
					 * 		},
					 * 		{
					 * 			"actualizacion":"10.43",
					 * 			"cond_Gast_Ejec":null,
					 * 			"cond_mult_omi":null,
					 * 			"cond_rec":0,
					 * 			"impuesto_bimestral":"42",
					 * 			"multa_omision":"0",
					 * 			"periodo":"20083",
					 * 			"recargos":"6.28",
					 * 			"requerido":"NO",
					 * 			"subtotal":null
					 * 		}
					 * 	],
					 * 	"gastos_ejecucion":0,
					 * "multa_incumplimiento":0
					 * }
					 */
					
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					CustomToast.build(PredialVencidoActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.");
				}
				
			} else {
				CustomToast.build(PredialVencidoActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.");
			}
		}
	}
}
