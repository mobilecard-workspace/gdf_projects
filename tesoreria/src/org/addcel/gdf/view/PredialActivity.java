package org.addcel.gdf.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Tramite;
import org.addcel.gdf.constant.Url;
import org.addcel.gdf.sql.CuentaPredial;
import org.addcel.gdf.sql.PredialDataSource;
import org.addcel.gdf.to.PeriodoTO;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class PredialActivity extends SherlockActivity {

    Spinner cuentaSpinner, periodosSpinner;
    EditText cuentaText, confCuentaText;
    Button calcularButton, pagarButton;
    boolean ingresoDirecto;
    SessionManager session;
    JSONObject requestPago;
    PredialDataSource dataSource;
    List<PeriodoTO> periodos;

    private static final String TAG = "PredialActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_predial);
        session = new SessionManager(PredialActivity.this);

        ingresoDirecto = true;
        initGUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        getSupportMenuInflater().inflate(R.menu.menu_predial, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        int id = item.getItemId();

        switch (id) {
            case R.id.action_predial:
                if (session.isLoggedIn()) {
                    Dialogos.showPredialDialog(Tramite.PREDIAL, this, dataSource);
                } else {
                	CustomToast.build(PredialActivity.this, "Debes iniciar sesi�n mobilecard para poder guardar una cuenta.");
                }
            break;
            case R.id.action_login:
                if (!session.isLoggedIn()) {
                    startActivity(new Intent(PredialActivity.this, LoginActivity.class));
                } else {
                    Dialogos.makeYesNoAlert(PredialActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {

                        @Override
                        public void ok(DialogInterface dialog, int id) {
                            // TODO Auto-generated method stub
                            session.logoutUser();
                            dialog.dismiss();
                        }

                        @Override
                        public void cancel(DialogInterface dialog, int id) {
                            // TODO Auto-generated method stub
                            dialog.cancel();
                        }
                    });
                }
                break;
            case R.id.action_registro:
                if (session.isLoggedIn()) {
                	CustomToast.build(PredialActivity.this, "Ya eres usuario MobileCard.");
                } else {
                    startActivity(new Intent(PredialActivity.this, RegistroActivity.class));
                }
                break;
            case R.id.action_update:
                if (session.isLoggedIn()) {
                    startActivity(new Intent(PredialActivity.this, DataUpdateActivity.class));
                } else {
                	CustomToast.build(PredialActivity.this, "Inicia Sesi�n para actualizar tus datos");
                }
                break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					CustomToast.build(PredialActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(PredialActivity.this, PassRecoverActivity.class));
				}
				break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initData() {
        dataSource = new PredialDataSource(PredialActivity.this);
        dataSource.open();
        List<CuentaPredial> cuentas = dataSource.getAllCuentas();
        ArrayAdapter<CuentaPredial> adapter = new ArrayAdapter<CuentaPredial>(PredialActivity.this, android.R.layout.simple_spinner_item, cuentas);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cuentaSpinner.setAdapter(adapter);
        dataSource.close();
    }

    private void initGUI() {
        cuentaSpinner = (Spinner) findViewById(R.id.spinner_predial);
        periodosSpinner = (Spinner) findViewById(R.id.spinner_periodos);
        periodosSpinner.setEnabled(false);
        cuentaText = (EditText) findViewById(R.id.edit_predial);
        confCuentaText = (EditText) findViewById(R.id.edit_predial_conf);
        calcularButton = (Button) findViewById(R.id.button_consultar);
        pagarButton = (Button) findViewById(R.id.button_pagar);

        cuentaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3) {
                // TODO Auto-generated method stub
                if (0 == arg2) {
                    ingresoDirecto = true;
                    cuentaText.setVisibility(View.VISIBLE);
                    confCuentaText.setVisibility(View.VISIBLE);
                } else {
                    ingresoDirecto = false;
                    cuentaText.setVisibility(View.GONE);
                    confCuentaText.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
                ingresoDirecto = true;
                cuentaText.setVisibility(View.VISIBLE);
                confCuentaText.setVisibility(View.VISIBLE);

            }
        });

        calcularButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                sendData();
            }
        });

        pagarButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                executePayment();
            }
        });

        Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
        ((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);

        initData();
    }

    private String getCuenta(boolean ingresoDirecto) {
        if (ingresoDirecto)
            return cuentaText.getText().toString().trim();
        else
            return ((CuentaPredial) cuentaSpinner.getSelectedItem()).getCuenta();
    }

    private void sendData() {
        if (session.isLoggedIn()) {
            if (validar()) {
                String cuenta = getCuenta(ingresoDirecto);

                String request = Request.buildPredialRequest(cuenta);
                Log.i(TAG, request);
                new WebServiceClient(new ConsultaListener(), PredialActivity.this, true, Url.CONSUMIDOR).execute(request);
            }
        } else {
        	CustomToast.build(PredialActivity.this, "Debes iniciar sesi�n mobilecard para poder calcular tu pago.");

        }
    }

    public void executePayment() {
        if (requestPago == null)
        	CustomToast.build(PredialActivity.this, "Env�e datos de pago.");
        else{
        	
        	double montoMax = requestPago.optDouble("monto_maximo_operacion", 5000.0);
			
			if (requestPago.optDouble("totalPago", 0) <= montoMax) {
				
	            Log.d(TAG, "Request PAGO: " + requestPago.toString());
	            Intent i = new Intent(PredialActivity.this, PagoActivity.class);
	            i.putExtra("datos", requestPago.toString());
	            i.putExtra("servicio", Servicios.PREDIAL);
	            startActivity(i);
			} else {
				CustomToast.build(PredialActivity.this, "El l�mite m�ximo para la transacci�n es: " + Text.formatCurrency(montoMax, true));
			}
        }
    }

    private boolean validar() {
        if (ingresoDirecto) {

            String cuenta = cuentaText.getText().toString().trim();
            String confirmacion = confCuentaText.getText().toString().trim();

            if (cuenta == null || "".equals(cuenta)) {
                cuentaText.requestFocus();
                cuentaText.setError("El campo cuenta no puede ir vac�o");
                return false;
            }

            if (confirmacion == null || "".equals(confirmacion)) {
                confCuentaText.requestFocus();
                confCuentaText.setError("El campo confirmaci�n no puede ir vac�o");
                return false;
            }

            if (!cuenta.equals(confirmacion)) {
                confCuentaText.requestFocus();
                confCuentaText.setError("Confirmaci�n no coincide con cuenta capturada");
                return false;
            }
            return true;
        } else {
            return true;
        }
    }

    private void cleanScreen() {
        ((TextView) findViewById(R.id.view_periodo)).setText("");
        ((TextView) findViewById(R.id.view_derecho)).setText("");
        ((TextView) findViewById(R.id.view_reduccion)).setText("");
        ((TextView) findViewById(R.id.view_cuenta)).setText("");
        ((TextView) findViewById(R.id.view_captura)).setText("");
        ((TextView) findViewById(R.id.view_total)).setText("");

        periodosSpinner.setAdapter(PeriodoTO.BuildAdapter(PredialActivity.this, new ArrayList<PeriodoTO>()));
    }

    private class ConsultaListener implements WSResponseListener {

        private static final String TAG = "ConsultaListener";

        @Override
        public void StringResponse(String response) {
            // TODO Auto-generated method stub
            Log.i(TAG, response);

            if (response != null && !"".equals(response)) {
                response = AddcelCrypto.decryptSensitive(response);

                try {
                    JSONObject json = new JSONObject(response);
                    Log.i(TAG, json.toString());

                    String numError = json.optString("numError", "");
                    String error = json.optString("error", "");

//                  {"error":"esta  cuenta no  tiene  adeudos","numError":"S001"}

                    if ("".equals(numError)) {

                        JSONArray predialRespuestas = json.optJSONArray("predialRespuestas");

                        if (predialRespuestas.length() > 0) {

                            periodos = new ArrayList<PeriodoTO>(predialRespuestas.length());

                            for (int i = 0; i < predialRespuestas.length(); i++) {
                                PeriodoTO periodo = new PeriodoTO(predialRespuestas.optJSONObject(i));
                                periodos.add(periodo);
                            }

                            periodosSpinner.setAdapter(PeriodoTO.BuildAdapter(PredialActivity.this, periodos));
                            periodosSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                                @Override
                                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                                    // TODO Auto-generated method stub
                                    PeriodoTO seleccion = (PeriodoTO) arg0.getAdapter().getItem(arg2);
                                    requestPago = seleccion.getJson();

                                    String importe = Text.formatCurrency(seleccion.getJson().optDouble("importe"), true);
                                    String reduccion = Text.formatCurrency(seleccion.getJson().optDouble("reduccion"), true);
                                    String totalPago = Text.formatCurrency(seleccion.getJson().optDouble("totalPago"), true);

                                    ((TextView) findViewById(R.id.view_periodo)).setText(seleccion.getJson().optString("bimestre"));
                                    ((TextView) findViewById(R.id.view_derecho)).setText(importe);
                                    ((TextView) findViewById(R.id.view_reduccion)).setText(reduccion);
                                    ((TextView) findViewById(R.id.view_cuenta)).setText(seleccion.getJson().optString("cuentaP"));
                                    ((TextView) findViewById(R.id.view_captura)).setText(seleccion.getJson().optString("linea_captura"));
                                    ((TextView) findViewById(R.id.view_total)).setText(totalPago);

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> arg0) {
                                    // TODO Auto-generated method stub
                                    requestPago = null;
                                    cleanScreen();
                                }

                            });

                            periodosSpinner.setEnabled(true);
                        }


                    } else {
                        cleanScreen();
                        periodosSpinner.setEnabled(false);
                        CustomToast.build(PredialActivity.this, error);
                    }
                } catch (JSONException e) {
                    cleanScreen();
                    periodosSpinner.setEnabled(false);
                    CustomToast.build(PredialActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
                    Log.e(TAG, "JSONException", e);
                }
            } else {
                cleanScreen();
                periodosSpinner.setEnabled(false);
                CustomToast.build(PredialActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
            }
        }
    }

}
