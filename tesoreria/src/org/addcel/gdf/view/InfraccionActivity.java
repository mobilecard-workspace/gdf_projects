package org.addcel.gdf.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.gdf.sql.Placa;
import org.addcel.gdf.sql.PlacaDataSource;
import org.addcel.gdf.to.InfraccionTO;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.ConnectivityUtil;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class InfraccionActivity extends SherlockActivity {
	Spinner placaSpinner, infraccionSpinner;
	EditText placaEdit, placaConfEdit;
	TextView folioView, fechaView, montoView, actualizacionView, recargosView, multaView, lineaView, importeView;
	Button calcularButton, pagarButton;
	boolean ingresoDirecto;
	SessionManager session;
	JSONObject requestPago;

	PlacaDataSource dataSource;
	
	private static final String TAG = "InfraccionActivity";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_infraccion);
		session = new SessionManager(InfraccionActivity.this);
		
		ingresoDirecto = true;
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_tenencia, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_placa:
				if (session.isLoggedIn()) {
					Dialogos.showPlacaDialog(InfraccionActivity.this, dataSource, Servicios.INFRACCION);
				} else {
					CustomToast.build(InfraccionActivity.this, "Debes iniciar sesi�n mobilecard para poder guardar una placa.");
				}
				break;
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(InfraccionActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(InfraccionActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					CustomToast.build(InfraccionActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(InfraccionActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(InfraccionActivity.this, DataUpdateActivity.class));
				} else {
					CustomToast.build(InfraccionActivity.this, "Inicia Sesi�n para actualizar tus datos");
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					CustomToast.build(InfraccionActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(InfraccionActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		dataSource = new PlacaDataSource(InfraccionActivity.this);
		dataSource.open();
		List<Placa> placas = dataSource.getAllPlacas();
		ArrayAdapter<Placa> adapter = new ArrayAdapter<Placa>(InfraccionActivity.this, android.R.layout.simple_spinner_item, placas);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		placaSpinner.setAdapter(adapter);
		dataSource.close();
	}
	
	private void initGUI() {
		placaSpinner = (Spinner) findViewById(R.id.spinner_placa);
		placaEdit = (EditText) findViewById(R.id.edit_placa);
		placaConfEdit = (EditText) findViewById(R.id.edit_placa_conf);
		calcularButton = (Button) findViewById(R.id.button_calcular);
		infraccionSpinner = (Spinner) findViewById(R.id.spinner_infraccion);
		infraccionSpinner.setEnabled(false);
		
		folioView = (TextView) findViewById(R.id.view_folio);
		fechaView = (TextView) findViewById(R.id.view_fecha);
		actualizacionView = (TextView) findViewById(R.id.view_actualizacion);
		recargosView = (TextView) findViewById(R.id.view_recargos);
		multaView = (TextView) findViewById(R.id.view_multa);
		lineaView = (TextView) findViewById(R.id.view_linea);
		montoView = (TextView) findViewById(R.id.view_monto);
		importeView = (TextView) findViewById(R.id.view_importe);
		
		pagarButton = (Button) findViewById(R.id.button_pagar);
		
		placaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (0 == arg2) {
					ingresoDirecto = true;
					placaEdit.setVisibility(View.VISIBLE);
					placaConfEdit.setVisibility(View.VISIBLE);
				} else {
					ingresoDirecto = false;
					placaEdit.setVisibility(View.GONE);
					placaConfEdit.setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				ingresoDirecto = true;
				placaEdit.setVisibility(View.VISIBLE);
				placaConfEdit.setVisibility(View.VISIBLE);
			}
		});
		

		
		calcularButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (ConnectivityUtil.checkConnection(InfraccionActivity.this)) {
					consulta();
				} else {
					CustomToast.build(InfraccionActivity.this, "No hay conexi�n a internet disponible.");
					
				}
			}
		});
		
		infraccionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				InfraccionTO infraccion = (InfraccionTO) arg0.getAdapter().getItem(arg2);
				
				if (infraccion != null) {
					requestPago = infraccion.getJson();
					
					double actualizacionD = Double.parseDouble(infraccion.getActualizacion());
					double recargosD = Double.parseDouble(infraccion.getRecargos());
					double montoD = Double.parseDouble(infraccion.getMonto());
					double importeD = Double.parseDouble(infraccion.getTotalPago());
					
					folioView.setText(infraccion.getFolio());
					fechaView.setText(infraccion.getFechaInfraccion());
					actualizacionView.setText(Text.formatCurrency(actualizacionD, true));
					recargosView.setText(Text.formatCurrency(recargosD, true));
					multaView.setText(infraccion.getDiasMulta());
					lineaView.setText(infraccion.getLineaCaptura());
					montoView.setText(Text.formatCurrency(montoD, true));
					importeView.setText(Text.formatCurrency(importeD, true));
				} else 
					CustomToast.build(InfraccionActivity.this, "Consulte datos de placa.");
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				requestPago = null;
				
				folioView.setText("");
				fechaView.setText("");
				actualizacionView.setText("");
				recargosView.setText("");
				multaView.setText("");
				lineaView.setText("");
				montoView.setText("");
				importeView.setText("");
			}
		});
		
		pagarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (requestPago == null) 
					Toast.makeText(InfraccionActivity.this, "Consulte datos de pago.", Toast.LENGTH_SHORT).show();
				else{
					
					double montoMax = requestPago.optDouble("monto_maximo_operacion", 5000.0);
					
					if (requestPago.optDouble("totalPago", 0) <= montoMax) {
					
						Intent i = new Intent(InfraccionActivity.this, PagoActivity.class);
						i.putExtra("datos", requestPago.toString());
						i.putExtra("servicio", Servicios.INFRACCION);
						startActivity(i);
					} else {
						CustomToast.build(InfraccionActivity.this, "El l�mite m�ximo para la transacci�n es: " + Text.formatCurrency(montoMax, true));
					}
				}
			}
		});
		

		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		
		initData();
		
	}
	
	private String getPlaca(boolean ingresoDirecto) {
		if (ingresoDirecto)
			return placaEdit.getText().toString().trim();
		else 
			return ((Placa) placaSpinner.getSelectedItem()).getPlaca();
	}
	
	private void setInfraccionSpinner(List<InfraccionTO> infracciones, boolean enable) {
		ArrayAdapter<InfraccionTO> adapter = new ArrayAdapter<InfraccionTO>(InfraccionActivity.this, android.R.layout.simple_spinner_item, infracciones);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		infraccionSpinner.setAdapter(adapter);
		infraccionSpinner.setEnabled(enable);
	}
	
	private void consulta() {
		
		if (session.isLoggedIn()) {
			if (validar()) {
				String placa = getPlaca(ingresoDirecto);
				
				String request = Request.buildInfraccionesRequest(placa);
				Log.i(TAG, request);
				new WebServiceClient(new ConsultaListener(), InfraccionActivity.this, true, Url.CONSUMIDOR).execute(request);
			}
		} else {
			CustomToast.build(InfraccionActivity.this, "Debes iniciar sesi�n mobilecard para poder calcular tu pago.");
		}
	}
	
	private void executePayment() {
		String idUsuario = session.getUserDetails().get(SessionManager.USR_ID);
		int idProducto = Servicios.INFRACCION;
		String request = Request.buildProsaRequest(InfraccionActivity.this, requestPago, idUsuario, idProducto);
		
		new WebServiceClient(new ConsultaListener(), InfraccionActivity.this, true, Url.PAGO_PROSA).execute(request);
	}
	
	private boolean validar() {
		if (ingresoDirecto) {
			
			String placa = placaEdit.getText().toString().trim();
			String confirmacion = placaConfEdit.getText().toString().trim();
			
			if (placa == null || "".equals(placa)) {
				placaEdit.requestFocus();
				placaEdit.setError("El campo placa no puede ir vac�o");
				return false;
			}
			
			if (confirmacion == null || "".equals(confirmacion)) {
				placaConfEdit.requestFocus();
				placaConfEdit.setError("El campo confirmaci�n no puede ir vac�o");
				return false;
			}
			
			if (!placa.equals(confirmacion)) {
				placaConfEdit.requestFocus();
				placaConfEdit.setError("Confirmaci�n no coincide con placa capturada");
				return false;
			}
			return true;
		} else {
			return true;
		}
	}
	
	private void cleanScreen() {
		setInfraccionSpinner(new ArrayList<InfraccionTO>(), false);
		requestPago = null;
		
		folioView.setText("");
		fechaView.setText("");
		actualizacionView.setText("");
		recargosView.setText("");
		montoView.setText("");
		multaView.setText("");
		lineaView.setText("");
		importeView.setText("");
	}
	
	private class ConsultaListener implements WSResponseListener {
		
		private static final String TAG = "ConsultaListener";
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptSensitive(response);
				
				try {
					JSONObject json = new JSONObject(response);
				
					Log.i(TAG, json.toString());
					
					String error = json.optString("numError");
					String errorDesc = json.optString("error", "Error de conexi�n. Intente de nuevo m�s tarde.");
					
					if ("".equals(error)) {
						JSONArray array = json.optJSONArray("arreglo_folios");
						
						if (array.length() > 0) {
							
							List<InfraccionTO> infracciones = new ArrayList<InfraccionTO>(array.length());
							
							for (int i = 0; i < array.length(); i++) {
								infracciones.add(new InfraccionTO(array.optJSONObject(i)));
							}
							setInfraccionSpinner(infracciones, true);
						} else {
							CustomToast.build(InfraccionActivity.this, errorDesc);
						}
						
					} else {
						cleanScreen();
						CustomToast.build(InfraccionActivity.this, errorDesc);
					}
				} catch (JSONException e) {
					cleanScreen();
					Log.e(TAG, "", e);
					CustomToast.build(InfraccionActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.");
				}
			} else {
				cleanScreen();
				CustomToast.build(InfraccionActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.");
			}
		}
	}
	
	

}
