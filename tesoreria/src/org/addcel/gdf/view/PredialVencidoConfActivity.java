package org.addcel.gdf.view;
import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.TipoConsulta;
import org.addcel.gdf.constant.Url;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;


public class PredialVencidoConfActivity extends SherlockActivity {
	
	SessionManager session;
	String periodos;
	String header;
	
	TextView headerView;
	
	LinearLayout detalleLayout;
	
	TextView actualizacionView, ejecucionView, ejecucionCondView,
			recargosView, multaView, multaCondView, capturaView, totalView;
	
	double emitido, pagado, actualizacion, ejecucion, ejecucionCondonados, recargos, multa, multaCondonada;
	
	Button pagarButton;
	JSONObject requestPago;
	
	private static final String TAG = "PredialVencidoConfActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_predial_conf);
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int itemId = item.getItemId();
		
		switch (itemId) {
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(PredialVencidoConfActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(PredialVencidoConfActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					Toast.makeText(PredialVencidoConfActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
				} else {
					startActivity(new Intent(PredialVencidoConfActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(PredialVencidoConfActivity.this, DataUpdateActivity.class));
				} else {
					Toast.makeText(PredialVencidoConfActivity.this, "Inicia Sesi�n para actualizar tus datos", Toast.LENGTH_SHORT).show();
				}
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		session = new SessionManager(PredialVencidoConfActivity.this);
		header = getIntent().getStringExtra("cuenta");
		periodos = getIntent().getStringExtra("periodos");
	}
	
	public void initGUI() {
		initData();
		headerView = (TextView) findViewById(R.id.header_text);
		headerView.setText("Cuenta: " + header);
		Log.i(TAG, "Periodos: " + periodos);
		
		
		detalleLayout = (LinearLayout) findViewById(R.id.layout_detalle);
		actualizacionView = (TextView) findViewById(R.id.view_actualizacion);
		ejecucionView = (TextView) findViewById(R.id.view_ejecucion);
		ejecucionCondView = (TextView) findViewById(R.id.view_ejecucion_cond);
		recargosView = (TextView) findViewById(R.id.view_recargos);
		multaView = (TextView) findViewById(R.id.view_multa);
		multaCondView = (TextView) findViewById(R.id.view_multa_cond);
		capturaView = (TextView) findViewById(R.id.view_captura);
		totalView = (TextView) findViewById(R.id.view_total);
		
		pagarButton = (Button) findViewById(R.id.button_pagar);
		
		pagarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				executePayment();
			} 
		});
		
		getPaymentData();
	}
	
	private void getPaymentData() {
		String request = Request.buildPVencidosRequest(header, TipoConsulta.PERIODOS, periodos);
		new WebServiceClient(new ConsultaListener(), PredialVencidoConfActivity.this, true, Url.CONSUMIDOR).execute(request);
	}
	
	public void executePayment() {
		if (requestPago == null) 
			Toast.makeText(PredialVencidoConfActivity.this, "Env�e datos de pago.", Toast.LENGTH_SHORT).show();
		else{
			Intent i = new Intent(PredialVencidoConfActivity.this, PagoActivity.class);
			i.putExtra("datos", requestPago.toString());
			i.putExtra("servicio", Servicios.PREDIAL_VENCIDOS);
			startActivity(i);
		}
	}
	
	private LinearLayout buildDetalleLayout(JSONObject detalle) {
		
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(0, 0, 0, 0);
		
		LinearLayout detalleLayout = new LinearLayout(PredialVencidoConfActivity.this);
		detalleLayout.setOrientation(LinearLayout.VERTICAL);
		detalleLayout.setLayoutParams(params);
		
		String actualizacionStr = Text.formatCurrency(detalle.optDouble("actualizacion"), true);
		String recargoStr = Text.formatCurrency(detalle.optDouble("recargos"), true);
		String multaStr = Text.formatCurrency(detalle.optDouble("multa_omision"), true);
		String subtotalStr = Text.formatCurrency(detalle.optDouble("subtotal"), true);
		
		LinearLayout periodo = buildRowLayout("Periodo", detalle.optString("periodo"));
		LinearLayout actualizacion = buildRowLayout("Actualizaci�n", actualizacionStr);
		LinearLayout recargo = buildRowLayout("Recargo", recargoStr);
		LinearLayout multa = buildRowLayout("Multa", multaStr);
		LinearLayout subtotal = buildRowLayout("Subtotal Periodo", subtotalStr);
		
		detalleLayout.addView(periodo);
		detalleLayout.addView(actualizacion);
		detalleLayout.addView(recargo);
		detalleLayout.addView(multa);
		detalleLayout.addView(subtotal);
		
		
		return detalleLayout;
	}
	
	private View buildSeparator() {
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, 1);
		params.setMargins(0, 4, 0, 4);
		
		View separator = new View(PredialVencidoConfActivity.this);
		separator.setLayoutParams(params);
		separator.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
		
		return separator;
		
	}
	
	private LinearLayout buildRowLayout(String label, String valor) {
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		LinearLayout row = new LinearLayout(PredialVencidoConfActivity.this);
		row.setLayoutParams(params);
		row.setOrientation(LinearLayout.HORIZONTAL);
		TextView labelView = buildText(true, label);
		row.addView(labelView);
		TextView valorView = buildText(false, valor);
		row.addView(valorView);
		
		return row;
	}
	
	private TextView buildText(boolean label, String text) {
		LayoutParams params = new android.widget.LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);
		params.setMargins(8, 0, 8, 0);
		
		TextView view = new TextView(PredialVencidoConfActivity.this);
		view.setLayoutParams(params);
		view.setText(text);
		view.setTextAppearance(PredialVencidoConfActivity.this, android.R.style.TextAppearance_Small);
		
		if (label) {
			view.setTextColor(getResources().getColor(R.color.gdf_gris));
			view.setGravity(Gravity.LEFT);
		}
		else {
			view.setTextColor(getResources().getColor(android.R.color.darker_gray));
			view.setGravity(Gravity.RIGHT);
		}
		
		return view;
		
	}
	
	
	private class ConsultaListener implements WSResponseListener {
		
		private static final String TAG = "ConsultaListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptSensitive(response);
				Log.i(TAG, response);
				
				try {
					JSONObject json = new JSONObject(response);
					
					int idError = json.optInt("idError", -1);
					String mensajeError = json.optString("mensajeError", "Error de conexi�n. Intente de nuevo m�s tarde.");
					
					switch (idError) {
					case 0:
						requestPago = json;
						String linea = json.optString("linea_captura");
						String totalCuenta = Text.formatCurrency(json.optDouble("total_cuenta", 0.0), true);
						
						ejecucion = json.optDouble("gastos_ejecucion");
						pagado = 0.0;
					
						
						JSONArray detalle = json.optJSONArray("detalle");
						
//						double emitido, pagado, actualizacion, ejecucion, ejecucionCondonados, recargos, multa, multaCondonada;
						
						for (int i = 0; i < detalle.length(); i++) {
							JSONObject detalleJSON = detalle.optJSONObject(i);
							detalleLayout.addView(buildDetalleLayout(detalleJSON));
							
							if (i < detalle.length() - 1) {
								detalleLayout.addView(buildSeparator());
							}
							
							multaCondonada += detalleJSON.optDouble("cond_mult_omi");
							ejecucionCondonados += detalleJSON.optDouble("cond_Gast_Ejec");
							
						}
						
						actualizacionView.setText(Text.formatCurrency(json.optDouble("sumActualizacion"), true));
						ejecucionView.setText(Text.formatCurrency(json.optDouble("gastos_ejecucion"), true));
						ejecucionCondView.setText(Text.formatCurrency(ejecucionCondonados, true));
						recargosView.setText(Text.formatCurrency(json.optDouble("sumRecargos"), true));
						multaView.setText(Text.formatCurrency(json.optDouble("sumMultas"), true));
						multaCondView.setText(Text.formatCurrency(multaCondonada, true));
						
						capturaView.setText(linea);
						totalView.setText(totalCuenta);
						break;

					default:
						Toast.makeText(PredialVencidoConfActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
						finish();
						
						break;
					}
					
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					Toast.makeText(PredialVencidoConfActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
					finish();
				}
				/*
				 * {
				 * 	"idError":0,
				 * 	"linea":"8104819809000EHHD760",
				 * 	"detalle":[
				 * 	{
				 * 		"actualizacion":"11.03",
				 * 		"cond_Gast_Ejec":"0.00",
				 * 		"cond_mult_omi":"0.00",
				 * 		"cond_rec":0,
				 * 		"impuesto_bimestral":"42",
				 * 		"multa_omision":"0",
				 * 		"periodo":"2008/1",
				 * 		"recargos":"6.32",
				 * 		"requerido":"no",
				 * 		"subtotal":"59.35"
				 * 	},
				 * 	{
				 * 		"actualizacion":"11.03",
				 * 		"cond_Gast_Ejec":"0.00",
				 * 		"cond_mult_omi":"0.00",
				 * 		"cond_rec":0,
				 * 		"impuesto_bimestral":"42",
				 * 		"multa_omision":"0",
				 * 		"periodo":"2008/1",
				 * 		"recargos":"6.32",
				 * 		"requerido":"no",
				 * 		"subtotal":"59.35"
				 * 	}
				 * ],
				 * "gastos_ejecucion":0,
				 * "total_cuenta":"119",
				 * "id_bitacora":0,
				 * "id_usuario":0,
				 * "id_producto":0,
				 * "statusPago":0,
				 * "totalPago":"0.00",
				 * "banco":"986"
				 * }*/				
			} else {
				Toast.makeText(PredialVencidoConfActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				finish();
			}
				
		}
	}

}
