package org.addcel.gdf.view;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Keys;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.gdf.to.DetalleTO;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class PagoActivity extends SherlockActivity {
	
	private SessionManager session;
	private String datos;
	private int servicio;
	
	private JSONObject requestPago;
	
	private TextView servicioView, montoView, capturaView;
	private EditText cvvText, passwordText;
	private Button pagarButton;

	private static final String TAG = "PagoActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pago);
		
		session = new SessionManager(PagoActivity.this);
		
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_pago, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int itemId = item.getItemId();
		
		switch (itemId) {
		case R.id.action_update:
			Log.i(TAG, "Bot�n actualizar");
			startActivity(new Intent(PagoActivity.this, DataUpdateActivity.class));
			break;

		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		datos = getIntent().getStringExtra("datos");
		servicio = getIntent().getIntExtra("servicio", -1);
		setRequestPago();
	}
	
	private void initGUI() {
		initData();
		

		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.text_secretaria)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.text_finanzas)).setTypeface(gothamBold);
		
		servicioView = (TextView) findViewById(R.id.text_servicio);
		montoView = (TextView) findViewById(R.id.text_monto);
		capturaView = (TextView) findViewById(R.id.text_captura);
		
		cvvText = (EditText) findViewById(R.id.edit_cvv2);
		passwordText = (EditText) findViewById(R.id.edit_password);
		pagarButton = (Button) findViewById(R.id.button_pagar);
		
		pagarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				executePayment();
			}
		});
		
		loadData();
	}
	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		pagarButton.setEnabled(true);
		super.onRestart();
	}
	
	private void executePayment() {
		pagarButton.setEnabled(false);
		String request = Request.buildTokenRequest(Keys.USER, Keys.PASS, Keys.PROVEEDOR);
		new WebServiceClient(new TokenListener() , PagoActivity.this, true, Url.GET_TOKEN).execute(request);
	}
	
	private void loadData() {
		servicioView.setText(getServicioName(servicio));
		
		double totalD = 0.0;
		totalD = Double.parseDouble(requestPago.optString("totalPago"));
		capturaView.setText(requestPago.optString("linea_captura"));
		
		montoView.setText(Text.formatCurrency(totalD, true));
	}
		
	private void setRequestPago() {
		try {
			requestPago = new JSONObject(datos);
		} catch(JSONException e) {
			requestPago = new JSONObject();
			Log.e(TAG, "JSONException", e);
		}
	}
	
	private String getServicioName(int servicio) {
		switch (servicio) {
			case Servicios.AGUA:
				return "Agua Vigente";
			case Servicios.INFRACCION:
				return "Infracci�n";
			case Servicios.NOMINA:
				return "N�mina";
			case Servicios.PREDIAL:
				return "Predial Vigente";
			case Servicios.TENENCIA:
				return "Tenencia";
			case Servicios.PREDIAL_VENCIDOS:
				return "Predial Vencido";
	
			default:
				return "";
		}
	}
	
	private class TokenListener implements WSResponseListener {
		
		private static final String TAG = "TokenListener";
		
		@Override
		public void StringResponse(String response) {
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptSensitive(response);
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					
					int numError = json.optInt("numError", -1);
					String error = json.optString("error", "Error de conexi�n. Intente de nuevo m�s tarde");
					
					switch (numError) {
						case 0:
							String token = json.optString("token");
							
							requestPago.put("token", token);
							requestPago.put("cvv2", cvvText.getText().toString().trim());
							requestPago.put("password", passwordText.getText().toString().trim());
							
							String request = Request.buildProsaRequest(PagoActivity.this, 
																		requestPago, 
																		session.getUserDetails().get(SessionManager.USR_ID), 
																		servicio);
							
							new WebServiceClient(new PagoListener(), PagoActivity.this, true, Url.PAGO_PROSA).execute(request);
							break;
	
						default:
							CustomToast.build(PagoActivity.this, error);
							break;
					}
					
				} catch (JSONException e) {
					CustomToast.build(PagoActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
					Log.e(TAG, "JSONException", e);
				}
			} else {
				CustomToast.build(PagoActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
			}
			
		}
	}
	
	private class PagoListener implements WSResponseListener {
		
		private static final String TAG = "PagoListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptSensitive(response);
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					/*
					 * {
					 * 		"numError":0,
					 * 		"error":"El pago fue exitoso. El comprobante ha sido enviado al correo electr�nico registrado.",
					 * 		"transaccion":124161,
					 * 		"autorizacion":014867,
					 * 		"referencia":169941
					 * }*/
					int numError = json.optInt("numError", -1);
					String error = json.optString("error", "Error de conexi�n. Intente de nuevo m�s tarde" );
					long referencia = json.optLong("referencia");
					int transaccion = json.optInt("transaccion");
					
					DetalleTO detalle = new DetalleTO(error, String.valueOf(referencia), "", requestPago.optString("totalPago"));
					Intent intent = new Intent(PagoActivity.this, DetalleActivity.class);
					
					switch (numError) {
						case 0:
							String autorizacion = json.optString("autorizacion");
							detalle.setAutorizacion(autorizacion);
							detalle.setExitoso(true);
							intent.putExtra("detalle", detalle);
							startActivity(intent);
							finish();
							break;
	
						default:
							detalle.setExitoso(false);
							intent.putExtra("detalle", detalle);
							startActivity(intent);
							break;
					}
				} catch (JSONException e) {
					pagarButton.setEnabled(true);
					CustomToast.build(PagoActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
					Log.e(TAG, "JSONException", e);
				}
			} else {
				pagarButton.setEnabled(true);
				CustomToast.build(PagoActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
			}
			
			
		}
	}
}
