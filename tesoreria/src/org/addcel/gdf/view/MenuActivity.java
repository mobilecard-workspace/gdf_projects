package org.addcel.gdf.view;

import java.util.LinkedList;
import java.util.List;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.adapter.ComunicadosAdapter;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Url;
import org.addcel.gdf.to.ComunicadoTO;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.Dialogos;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class MenuActivity extends SherlockActivity {
	
	private ListView avisosList;
	private Button consultarButton, pagoLineaButton;
	private ComunicadosAdapter adapter;
	private List<ComunicadoTO> comunicados;
	private SessionManager session;
	
	private static final String TAG = "MenuActivity";
	
	public ListView getAvisosList() {
		if (null == avisosList) {
			avisosList = (ListView) findViewById(R.id.list_avisos);
		}
		
		return avisosList;
	}
	
	public ComunicadosAdapter getAdapter() {
		if (null == adapter) {
			adapter = new ComunicadosAdapter(this, comunicados);
		}
		
		return adapter;
	}
	
	public List<ComunicadoTO> getComunicados() {
		return comunicados;
	}
	
	public void setComunicados(List<ComunicadoTO> comunicados) {
		this.comunicados = comunicados;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		session = new SessionManager(MenuActivity.this);
		initGUI();
		
		String request = Request.buildComunicadosRequest(1);
		new WebServiceClient(new ComunicadosListener(), MenuActivity.this, true, Url.BUSCA_COMUNICADOS).execute(request);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(MenuActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(MenuActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					Toast.makeText(MenuActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
				} else {
					startActivity(new Intent(MenuActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(MenuActivity.this, DataUpdateActivity.class));
				} else {
					Log.i(TAG, "Inicia Sesi�n para actualizar tus datos");
					Toast.makeText(MenuActivity.this, "Inicia Sesi�n para actualizar tus datos.", Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					Toast.makeText(MenuActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
				} else {
					startActivity(new Intent(MenuActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void initGUI() {

		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.text_secretaria)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.text_finanzas)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.view_titulo)).setTypeface(gothamBold);
		
		
		consultarButton = (Button) findViewById(R.id.button_consultar);
		pagoLineaButton = (Button) findViewById(R.id.button_pago);
		
		
		consultarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(MenuActivity.this, ConsultaActivity.class));
				
			}
		});
		
		pagoLineaButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MenuActivity.this, DepartamentosActivity.class));
			}
		});
	}
	
	private class ComunicadosListener implements WSResponseListener {
		
		private final static String TAG = "ComunicadosListener";
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				try {
					JSONObject json = new JSONObject(AddcelCrypto.decryptHard(response));
					
					if (null != json) {
						
						JSONArray comunicados = json.getJSONArray("comunicados");
						
						if (0 < comunicados.length()) {
							List<ComunicadoTO> comunicadosList = new LinkedList<ComunicadoTO>();
							
							for (int i = 0; i < comunicados.length(); i++) {
								JSONObject comunicadoJSON = comunicados.getJSONObject(i);
								
								ComunicadoTO comunicado = new ComunicadoTO();
								comunicado.setBgColor(comunicadoJSON.optString("bgColor"));
								comunicado.setContenido(comunicadoJSON.optString("contenido"));
								comunicado.setFgColor(comunicadoJSON.optString("fgColor"));
								comunicado.setIdAplicacion(comunicadoJSON.optInt("idAplicacion"));
								comunicado.setIdComunicado(comunicadoJSON.optInt("idComunicado"));
								comunicado.setIdEmpresa(comunicadoJSON.optInt("idEmpresa"));
								comunicado.setPrioridad(comunicadoJSON.optInt("idPrioridad"));
								comunicado.setTamFuente(comunicadoJSON.optInt("tamFuente"));
								
								comunicadosList.add(comunicado);
								
							}
							
							getAvisosList().setAdapter(new ComunicadosAdapter(MenuActivity.this, comunicadosList));
						}
					}
					
				} catch (JSONException e) {
					Log.e(TAG, "JSONException", e);
				}
			}
			
		}
	}
}
