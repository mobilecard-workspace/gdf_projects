package org.addcel.gdf.view;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class DetalleInfraccionActivity extends SherlockActivity {	
	String jsonString;
	JSONObject json;
	SessionManager session;

	private static final String TAG = "DetalleInfraccionActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalle_infraccion);
		
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_reenvio, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		int itemId = item.getItemId();
		
		switch (itemId) {
		case R.id.action_reenviar:
			executeReenvio();
			break;
		case R.id.action_update:
			startActivity(new Intent(DetalleInfraccionActivity.this, DataUpdateActivity.class));
			break;
		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		session = new SessionManager(DetalleInfraccionActivity.this);
		jsonString = getIntent().getStringExtra("json");
		
		try {
			json = new JSONObject(jsonString);
		} catch (JSONException e) {
			Log.e(TAG, "", e);
		}
	}
	
	public void initGUI() {
		initData();		

		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.text_secretaria)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.text_finanzas)).setTypeface(gothamBold);
		
		double montoD = json.optDouble("importe");
		double actualizacionD = json.optDouble("actualizacion");
		double recargosD = json.optDouble("recargos");
		double importeD = json.optDouble("totalPago");
		
		((TextView) findViewById(R.id.view_folio)).setText(json.optString("folio"));
		((TextView) findViewById(R.id.view_fecha)).setText(json.optString("fechainfraccion"));
		((TextView) findViewById(R.id.view_monto)).setText(Text.formatCurrency(montoD, true));
		((TextView) findViewById(R.id.view_actualizacion)).setText(Text.formatCurrency(actualizacionD, true));
		((TextView) findViewById(R.id.view_recargos)).setText(Text.formatCurrency(recargosD, true));
		((TextView) findViewById(R.id.view_multa)).setText(json.optString("dias_multa"));
		((TextView) findViewById(R.id.view_linea)).setText(json.optString("linea_captura"));
		((TextView) findViewById(R.id.view_importe)).setText(Text.formatCurrency(importeD, true));
	}
	
	public void executeReenvio() {
		final String idUsuario = session.getUserDetails().get(SessionManager.USR_ID);
		final String idBitacora = json.optString("id_bitacora");
		final int idProducto = Servicios.INFRACCION;
		
		Dialogos.makeReenvioAlert(DetalleInfraccionActivity.this, "�Desea reenviar recibo de pago?", new DialogOkListener() {
			
			@Override
			public void ok(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				String request = Request.buildReenvioRequest(idUsuario, idBitacora, idProducto, null);
				new WebServiceClient(new ReenvioListener(), DetalleInfraccionActivity.this, true, Url.REENVIAR_RECIBO).execute(request);
			}
			
			@Override
			public void cancel(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				
				try {
					JSONObject json = new JSONObject();
					json.put("idUsuario", idUsuario);
					json.put("idBitacora", idBitacora);
					json.put("idProducto", idProducto);
					WebServiceClient client = new WebServiceClient(new ReenvioListener(), DetalleInfraccionActivity.this, true, Url.REENVIAR_RECIBO);
					Dialogos.showEmailDialog(DetalleInfraccionActivity.this, json, client);
				} catch (JSONException e) {
					Log.e(TAG, "", e);
				}
				
			}
		});
	}
	
	private class ReenvioListener implements WSResponseListener {
		
		private static final String TAG = "ReenvioListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					
					String error = json.optString("error", "Error de conexi�n. Intente de nuevo m�s tarde");
					CustomToast.build(DetalleInfraccionActivity.this, error);
					
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					CustomToast.build(DetalleInfraccionActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
				}
			} else {
				CustomToast.build(DetalleInfraccionActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
			}
			
			
		}
	}
	
	
}
