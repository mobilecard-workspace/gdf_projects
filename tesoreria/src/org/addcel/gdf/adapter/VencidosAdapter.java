package org.addcel.gdf.adapter;

import java.util.List;

import org.addcel.gdf.R;
import org.addcel.util.Text;
import org.addcel.widget.InertCheckBox;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class VencidosAdapter extends BaseAdapter {
	
	private Context con;
	private List<JSONObject> data;
	private LayoutInflater inflater;
	
	public VencidosAdapter(Context _con, List<JSONObject> _data) {
		con = _con;
		data = _data;
		inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return (long) arg0;
	}
	
	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		if (data.get(position).optString("requerido").equals("SI")) {
			return false;
		} else 
			return true;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		ViewHolder viewHolder;
		
		if (null == arg1) {
			arg1 = inflater.inflate(R.layout.item_vencido, arg2, false);
			viewHolder = new ViewHolder();
			viewHolder.viewPeriodo = (TextView) arg1.findViewById(R.id.view_periodo);
			viewHolder.viewSaldo = (TextView) arg1.findViewById(R.id.view_saldo);
			viewHolder.viewRequerido = (TextView) arg1.findViewById(R.id.view_requerido);
			viewHolder.checkSeleccionado = (InertCheckBox) arg1.findViewById(R.id.checked_seleccionado);
			
			arg1.setTag(viewHolder);
		} else{
			viewHolder = (ViewHolder) arg1.getTag();
		}
		
		JSONObject vencido = data.get(arg0);
		
		if (null != vencido) {
			double saldo = vencido.optDouble("subtotal");
			
			viewHolder.viewPeriodo.setText(vencido.optString("periodo"));
			viewHolder.viewSaldo.setText(Text.formatCurrency(saldo, true));
			viewHolder.viewRequerido.setText(vencido.optString("requerido"));
			
			final ListView lv = (ListView) arg2;
			viewHolder.checkSeleccionado.setChecked(lv.isItemChecked(arg0));
		
		}
		
		return arg1;
	}
	
	private class ViewHolder {
		TextView viewPeriodo;
		TextView viewSaldo;
		TextView viewRequerido;
		InertCheckBox checkSeleccionado;
	}

}
