package org.addcel.gdf.adapter;

import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.to.ComunicadoTO;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ComunicadosAdapter extends BaseAdapter {
	
	private Context con;
	private List<ComunicadoTO> comunicados;
	private LayoutInflater inflater;
	private Typeface typeface;
	
	public ComunicadosAdapter(Context con, List<ComunicadoTO> comunicados) {
		this.con = con;
		this.comunicados = comunicados;
		this.inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.typeface = Typeface.createFromAsset(con.getAssets(), Fonts.BODONI);
	}
	
	public Context getCon() {
		return con;
	}

	public void setCon(Context con) {
		this.con = con;
	}

	public List<ComunicadoTO> getComunicados() {
		return comunicados;
	}

	public void setComunicados(List<ComunicadoTO> comunicados) {
		this.comunicados = comunicados;
	}

	public LayoutInflater getInflater() {
		if (null == inflater) {
			inflater = (LayoutInflater) getCon().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		return inflater;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return getComunicados().size();
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return getComunicados().get(arg0);
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return getComunicados().get(arg0).getIdComunicado();
	}
	
	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	public View getView(int arg0, View arg1, ViewGroup arg2) {
		
		ViewHolder viewHolder;
		
		if (null == arg1) {
			arg1 = inflater.inflate(R.layout.item_comunicado, arg2, false);
			viewHolder = new ViewHolder();
			viewHolder.comunicado = (TextView) arg1.findViewById(R.id.id_comunicado);
			arg1.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) arg1.getTag();
		}
		
		ComunicadoTO comunicado = getComunicados().get(arg0);
		
		if (null != comunicado) {
			viewHolder.comunicado.setText(comunicado.getContenido());
			viewHolder.comunicado.setTextSize(comunicado.getTamFuente());
			viewHolder.comunicado.setTextColor(Color.parseColor("#" + comunicado.getFgColor()));
//			viewHolder.comunicado.setTypeface(typeface);
		}
		
		return arg1;
	}
	
	private class ViewHolder {
		TextView comunicado;
	}
	

}
