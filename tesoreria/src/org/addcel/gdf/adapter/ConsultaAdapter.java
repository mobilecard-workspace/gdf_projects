package org.addcel.gdf.adapter;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.constant.Servicios;
import org.addcel.util.Text;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ConsultaAdapter extends BaseAdapter {
	
	private Context con;
	private List<JSONObject> data;
	private LayoutInflater inflater;
	private int servicio;
	
	public ConsultaAdapter(Context _con, List<JSONObject> data, int servicio) {
		this.con = _con;
		this.data = data;
		this.servicio = servicio;
		this.inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		ViewHolder viewHolder;
		
		if (null == arg1) {
			arg1 = inflater.inflate(R.layout.item_consulta, arg2, false);
			viewHolder = new ViewHolder();
			viewHolder.anio = (TextView) arg1.findViewById(R.id.text_anio);
			viewHolder.dia = (TextView) arg1.findViewById(R.id.text_dia);
			viewHolder.mes = (TextView) arg1.findViewById(R.id.text_mes);
			viewHolder.concepto = (TextView) arg1.findViewById(R.id.text_periodo);
			viewHolder.lineaCaptura = (TextView) arg1.findViewById(R.id.text_referencia);
			viewHolder.total = (TextView) arg1.findViewById(R.id.text_total);
			
			arg1.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) arg1.getTag();
		}
		
		JSONObject consulta = data.get(arg0);
		
		if (null != consulta) {
			Date d = Text.getDateFromString(consulta.optString("fechaPago"));
			Calendar c = Calendar.getInstance();
			c.setTime(d);
			viewHolder.dia.setText(String.valueOf(c.get(Calendar.DAY_OF_MONTH)));
			
			String mesString = new DateFormatSymbols().getMonths()[c.get(Calendar.MONTH)];
			viewHolder.mes.setText(mesString);
		
			viewHolder.anio.setText(String.valueOf(c.get(Calendar.YEAR)));
			viewHolder.lineaCaptura.setText(consulta.optString("linea_captura"));
			viewHolder.total.setText(Text.formatCurrency(Double.parseDouble(consulta.optString("totalPago")), true));

			switch (servicio) {
				case Servicios.TENENCIA:
					viewHolder.concepto.setText("Tenencia");
					break;
				case Servicios.INFRACCION:
					viewHolder.concepto.setText("Infracci�n");
					break;
				case Servicios.NOMINA:
					viewHolder.concepto.setText("N�mina");
					break;
				case Servicios.PREDIAL:
					viewHolder.concepto.setText("Predial Vigente");
					break;
				case Servicios.AGUA:
					viewHolder.concepto.setText("Agua Vigente");
					break;
				case Servicios.PREDIAL_VENCIDOS:
					viewHolder.concepto.setText("Predial Vencido");
					break;
				default:
					break;
			}
			
		}
		
		return arg1;
	}
	
	private class ViewHolder {
		TextView dia;
		TextView mes;
		TextView anio;
		TextView lineaCaptura;
		TextView concepto;
		TextView total;
	}

}
