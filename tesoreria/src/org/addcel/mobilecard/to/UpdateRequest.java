package org.addcel.mobilecard.to;

import org.addcel.crypto.Crypto;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class UpdateRequest {

	private String login;
	private String password;
	
	private static final String TAG = "UpdateRequest";
	
	public UpdateRequest() {
		// TODO Auto-generated constructor stub
	}
	
	public UpdateRequest(String login, String password) {
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{Login: " + login + ", Password: " + password + "}";
	}
	
	public String build() {
		JSONObject json = new JSONObject();
		
		try {
			json.put("login", login);
			json.put("password", password);
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String request = Crypto.aesEncrypt(Text.parsePass(password), json.toString());
		
		return Text.mergeStr(request, password);
	}
}
