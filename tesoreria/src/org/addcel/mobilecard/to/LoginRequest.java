package org.addcel.mobilecard.to;

import org.addcel.crypto.Crypto;
import org.addcel.util.DeviceUtils;
import org.addcel.util.Text;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class LoginRequest {
	
	private String login;
	private String password;
	
	public LoginRequest(String login, String password) {
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{login: " + login + ", " + "password: " + password + "}";
	}
	
	public String build(Context ctx) {
		JSONObject j = new JSONObject();
		
		try {

			j.put("login", login);
			j.put("passwordS",Crypto.sha1(password));
			j.put("password", password);
			j.put("imei", DeviceUtils.getIMEI(ctx));
			j.put("tipo", DeviceUtils.getTipo());
			j.put("modelo", DeviceUtils.getModel());
			j.put("key", DeviceUtils.getIMEI(ctx));
			
			
			String aesJson = Crypto.aesEncrypt(Text.parsePass(password), j.toString());
			
			return Text.mergeStr(aesJson, password);
			
			
		} catch (JSONException e) {
			return "";
		}
	}
	
	
}
