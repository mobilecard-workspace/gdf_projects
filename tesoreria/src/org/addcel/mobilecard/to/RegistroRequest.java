package org.addcel.mobilecard.to;

import java.util.Date;

import org.addcel.crypto.Crypto;
import org.addcel.gdf.constant.Keys;
import org.addcel.util.DeviceUtils;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;


public class RegistroRequest {
	
	private String login;
	private String nacimiento;
	private String telefono;
	private String nombre;
	private String apellido;
	private String materno;
	private String sexo;
	private String telCasa;
	private String telOficina;
	private String ciudad;
	private String calle;
	private String numExt;
	private String numInterior;
	private String colonia;
	private String cp;
	private String idEstado;
	private String mail;
	private String tarjeta;
	private String vigencia;
	private String tipoTarjeta;
	private String proveedor;
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getNacimiento() {
		return nacimiento;
	}
	
	public void setNacimiento(String nacimiento) {
		this.nacimiento = nacimiento;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getMaterno() {
		return materno;
	}
	
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	
	public String getSexo() {
		return sexo;
	}
	
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	public String getTelCasa() {
		return telCasa;
	}
	
	public void setTelCasa(String telCasa) {
		this.telCasa = telCasa;
	}
	
	public String getTelOficina() {
		return telOficina;
	}
	
	public void setTelOficina(String telOficina) {
		this.telOficina = telOficina;
	}
	
	public String getCiudad() {
		return ciudad;
	}
	
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	public String getCalle() {
		return calle;
	}
	
	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	public String getNumExt() {
		return numExt;
	}
	
	public void setNumExt(String numExt) {
		this.numExt = numExt;
	}
	
	public String getNumInterior() {
		return numInterior;
	}
	
	public void setNumInterior(String numInterior) {
		this.numInterior = numInterior;
	}
	
	public String getColonia() {
		return colonia;
	}
	
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	
	public String getCp() {
		return cp;
	}
	
	public void setCp(String cp) {
		this.cp = cp;
	}
	
	public String getIdEstado() {
		return idEstado;
	}
	
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getVigencia() {
		return vigencia;
	}
	
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	
	public String getProveedor() {
		return proveedor;
	}
	
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "{" +"login: " + login  +", nombre: " + nombre + ", apellido: " + apellido + "}";
	}
	
	public String build(Context ctx) {
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("login", login);
			json.put("nacimiento", nacimiento);
			json.put("telefono", telefono);
			json.put("registro", Text.formatDate(new Date()));
			json.put("nombre", nombre);
			json.put("apellido", apellido);
			json.put("materno", materno);
			json.put("sexo", sexo);
			json.put("tel_casa",  telCasa);
			json.put("tel_oficina", telOficina);
			json.put("ciudad", ciudad);
			json.put("calle", calle);
			json.put("num_ext", numExt);
			json.put("num_interior", numInterior);
			json.put("colonia", colonia);
			json.put("cp", cp);
			json.put("id_estado", idEstado);
			json.put("mail", mail);
			json.put("tarjeta", tarjeta);
			json.put("vigencia", vigencia);
			json.put("banco", 1);
			json.put("tipotarjeta", tipoTarjeta);
			json.put("proveedor", proveedor);
			json.put("status", 1);
			
			json.put("imei", DeviceUtils.getIMEI(ctx));
			json.put("tipo", DeviceUtils.getTipo());
			json.put("software", DeviceUtils.getSWVersion());
			json.put("modelo", DeviceUtils.getModel());
			json.put("key", DeviceUtils.getIMEI(ctx));
			
			
//			ATRIBUTOS TAGs
			json.put("idtiporecargatag", 0);
			json.put("etiqueta", "");
			json.put("numero", "");
			json.put("dv", 0);
			
			Log.i("request registro", json.toString());
		} catch (JSONException e) {
			return "";
		}
		
		String regKey = Keys.REGISTER;

		String insertString = Crypto.aesEncrypt(Text.parsePass(regKey), json.toString());
		return Text.mergeStr(insertString, regKey);
	}
	
	public String buildUpdate(Context ctx, String password) {
		JSONObject json = new JSONObject();
		
		try {
			json.put("login", login);
			json.put("password", password);
			json.put("nacimiento", nacimiento);
			json.put("telefono", telefono);
			json.put("nombre", nombre);
			json.put("apellido", apellido);
			json.put("materno", materno);
			json.put("sexo", sexo);
			json.put("tel_casa",  telCasa);
			json.put("tel_oficina", telOficina);
			json.put("ciudad", ciudad);
			json.put("calle", calle);
			json.put("num_ext", numExt);
			json.put("num_interior", numInterior);
			json.put("colonia", colonia);
			json.put("cp", cp);
			json.put("id_estado", idEstado);
			json.put("mail", mail);
			json.put("tarjeta", tarjeta);
			json.put("vigencia", vigencia);
			json.put("banco", 1);
			json.put("tipotarjeta", tipoTarjeta);
			json.put("proveedor", proveedor);
			json.put("status", 1);
			
			Log.i("request registro", json.toString());
		} catch (JSONException e) {
			return "";
		}

		String insertString = Crypto.aesEncrypt(Text.parsePass(password), json.toString());
		return Text.mergeStr(insertString, password);
	}


}
