package org.addcel.mobilecard.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.crypto.Crypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.constant.UrlMc;
import org.addcel.mobilecard.to.RegistroRequest;
import org.addcel.mobilecard.to.UpdateRequest;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.addcel.util.Text;
import org.addcel.util.Validador;
import org.addcel.widget.SpinnerValues;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class DataUpdateActivity extends SherlockActivity {
	
	private DatePickerDialog dateDialog;
	private int anio, mes, dia;
	private Date nacimientoDate;
	
	EditText usuarioEdit, celularEdit, correoEdit;
	EditText nombreEdit, paternoEdit, maternoEdit, nacimientoEdit, casaEdit, oficinaEdit;
	EditText ciudadEdit, calleEdit, exteriorEdit, interiorEdit, coloniaEdit, cpEdit;
	EditText numTarjetaEdit;
	
	Spinner proveedorSpinner, generoSpinner, estadoSpinner, tipoTarjetaSpinner, mesSpinner, anioSpinner;
	SpinnerValues proveedorValues, generoValues, estadosValues, tipoTarjetaValues, mesValues, anioValues;
	
	Button registrarButton;
	
	RegistroRequest request;
	SessionManager session;
	
	String tarjetaGuardada, tarjetaModificada, tarjetaFinal;
	
	private static final String TAG = "DataUpdateActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mc_dataupdate);
		session = new SessionManager(DataUpdateActivity.this);
		initGUI();
		
		new WebServiceClient(new ProveedoresListener(), DataUpdateActivity.this, true, UrlMc.PROVEEDORES).execute();
	}
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}
	
	public DatePickerDialog getDateDialog() {
		return dateDialog;
	}

	public void setDateDialog(DatePickerDialog dateDialog) {
		this.dateDialog = dateDialog;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public Date getNacimientoDate() {
		return nacimientoDate;
	}

	public void setNacimientoDate(Date nacimientoDate) {
		this.nacimientoDate = nacimientoDate;
	}

	public void initGUI() {

		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.text_secretaria)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.text_finanzas)).setTypeface(gothamBold);
		
		
		usuarioEdit = (EditText) findViewById(R.id.edit_usuario);
		celularEdit = (EditText) findViewById(R.id.edit_celular);
		correoEdit = (EditText) findViewById(R.id.edit_correo);
		nombreEdit = (EditText) findViewById(R.id.edit_nombre);
		paternoEdit = (EditText) findViewById(R.id.edit_paterno);
		maternoEdit = (EditText) findViewById(R.id.edit_materno);
		nacimientoEdit = (EditText) findViewById(R.id.edit_fecha);
		casaEdit = (EditText) findViewById(R.id.edit_casa);
		oficinaEdit = (EditText) findViewById(R.id.edit_oficina);
		ciudadEdit = (EditText) findViewById(R.id.edit_ciudad);
		calleEdit = (EditText) findViewById(R.id.edit_calle);
		exteriorEdit = (EditText) findViewById(R.id.edit_exterior);
		interiorEdit = (EditText) findViewById(R.id.edit_interior);
		coloniaEdit = (EditText) findViewById(R.id.edit_colonia);
		cpEdit = (EditText) findViewById(R.id.edit_cp);
		numTarjetaEdit = (EditText) findViewById(R.id.edit_tarjeta);
		
		proveedorSpinner = (Spinner) findViewById(R.id.spinner_proveedor);
		generoSpinner = (Spinner) findViewById(R.id.spinner_genero);
		estadoSpinner = (Spinner) findViewById(R.id.spinner_estado);
		tipoTarjetaSpinner = (Spinner) findViewById(R.id.spinner_tarjeta);
		mesSpinner = (Spinner) findViewById(R.id.spinner_mes);
		anioSpinner = (Spinner) findViewById(R.id.spinner_anio);
		registrarButton = (Button) findViewById(R.id.button_registrar);
		
		registrarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (validate())
					executeUpdate();
			}
		});
	}
	
	private void setMesSpinner() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(DataUpdateActivity.this, 
																android.R.layout.simple_spinner_item, 
																getResources().getStringArray(R.array.arr_mes));
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mesSpinner.setAdapter(adapter);
	}
	
	private void setAnioSpinner() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(DataUpdateActivity.this, 
																android.R.layout.simple_spinner_item, 
																AppUtils.getAniosVigencia());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		anioSpinner.setAdapter(adapter);
	}
	
	public DatePickerDialog getDatePickerDialog(Date date) {
		if (dateDialog == null) {
			final Calendar c = Calendar.getInstance();
			
			if (c != null) {
				c.setTime(date);
			}
			
			dateDialog = new DatePickerDialog(DataUpdateActivity.this, new OnDateSetListener() {
				
				@Override
				public void onDateSet(DatePicker view, int year, int monthOfYear,
						int dayOfMonth) {
					// TODO Auto-generated method stub
					dia = dayOfMonth;
					mes = monthOfYear;
					anio = year;
					
					c.set(anio, mes, dia);
					
					setNacimientoDate(c.getTime());
					nacimientoEdit.setText(Text.formatDateForShowing(getNacimientoDate()));
					
				}
			}, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
			
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			
			Log.i(TAG, "Versi�n SO: " + currentapiVersion);

			if (currentapiVersion > 10) {
				dateDialog.getDatePicker().setCalendarViewShown(false);
			}
		}
		
		return dateDialog;
	}
	
	public void showfechaDialog(View v) {
		switch (v.getId()) {
			case R.id.edit_fecha:
				
				if (!getDatePickerDialog(getNacimientoDate()).isShowing()) {
					
					int currentapiVersion = android.os.Build.VERSION.SDK_INT;
					
					Log.i(TAG, "Versi�n SO: " + currentapiVersion);
		
					if (currentapiVersion > 10) {
						getDatePickerDialog(getNacimientoDate()).getDatePicker().setCalendarViewShown(false);
					}
					
					getDateDialog().show();
				}
			break;

		default:
			break;
		}
	}

	public boolean validate() {
		
		String usuario = usuarioEdit.getText().toString().trim();
		String nombre = nombreEdit.getText().toString().trim();
		String paterno = paternoEdit.getText().toString().trim();
		String ciudad = ciudadEdit.getText().toString().trim();
		String calle = calleEdit.getText().toString().trim();
		String exterior = exteriorEdit.getText().toString().trim();
		String colonia = coloniaEdit.getText().toString().trim();
		String tarjeta = numTarjetaEdit.getText().toString().trim();
		
		if (usuario.equals("")) {
			usuarioEdit.requestFocus();
			usuarioEdit.setError("El campo usuario no puede ir vac�o.");
			return false;
		} else if (usuario.length() < 8) {
			usuarioEdit.requestFocus();
			usuarioEdit.setError("El campo usuario no puede ser menor a 8 caracteres");
			return false;
		} else if (nombre.equals("")) {
			nombreEdit.requestFocus();
			nombreEdit.setError("El campo nombre no puede ir vac�o.");
			return false;
		} else if (paterno.equals("")) {
			paternoEdit.requestFocus();
			paternoEdit.setError("El campo apellido paterno no puede ir vac�o.");
			return false;
		} else if (getNacimientoDate() == null) {
			nacimientoEdit.requestFocus();
			nacimientoEdit.setError("Seleccione fecha de nacimiento");
			return false;
		} else if (ciudad.equals("")) {
			ciudadEdit.requestFocus();
			ciudadEdit.setError("El campo ciudad no puede ir vac�o.");
			return false;
		} else if (calle.equals("")) {
			calleEdit.requestFocus();
			calleEdit.setError("El campo calle no puede ir vac�o.");
			return false;
		} else if (exterior.equals("")) {
			exteriorEdit.requestFocus();
			exteriorEdit.setError("El campo n�mero exterior no puede ir vac�o.");
			return false;
		} else if (colonia.equals("")) {
			coloniaEdit.requestFocus();
			coloniaEdit.setError("El campo colonia no puede ir vac�o.");
			return false;
		} else {
			String errorTarjeta = checkTarjeta();
			Log.i(TAG, "Error Tarjeta: " + errorTarjeta);
			
			if (!"".equals(errorTarjeta)) {
				numTarjetaEdit.requestFocus();
				numTarjetaEdit.setError(errorTarjeta);
				return false;
			} else {
				if (tipoTarjetaValues.getValorSeleccionado().equals("1")) {
					if (!Validador.esTarjetaDeCreditoVisa(tarjetaFinal)) {
						numTarjetaEdit.requestFocus();
						numTarjetaEdit.setError("N�mero de tarjeta no v�lido.");
						return false;
					}
				}
				if (tipoTarjetaValues.getValorSeleccionado().equals("2")) {
					if (!Validador.esTarjetaDeCreditoMasterCard(tarjetaFinal)) {
						numTarjetaEdit.requestFocus();
						numTarjetaEdit.setError("N�mero de tarjeta no v�lido.");
						return false;
					}
				}
				
				return true;
			}
		}
	}
	
	public void executeUpdate() {
		request = new RegistroRequest();
		request.setApellido(paternoEdit.getText().toString().trim());
		request.setCalle(calleEdit.getText().toString().trim());
		request.setCiudad(ciudadEdit.getText().toString().trim());
		request.setColonia(coloniaEdit.getText().toString().trim());
		request.setCp(cpEdit.getText().toString().trim());
		request.setIdEstado(estadosValues.getValorSeleccionado());
		request.setLogin(usuarioEdit.getText().toString().trim());
		request.setMail(correoEdit.getText().toString().trim());
		request.setMaterno(maternoEdit.getText().toString().trim());
		request.setNacimiento(Text.formatDate(getNacimientoDate()));
		request.setNombre(nombreEdit.getText().toString().trim());
		request.setNumExt(exteriorEdit.getText().toString().trim());
		request.setNumInterior(interiorEdit.getText().toString().trim());
		request.setProveedor(proveedorValues.getValorSeleccionado());
		request.setSexo(((String) generoSpinner.getSelectedItem()).substring(0, 1));
		request.setTarjeta(tarjetaFinal);
		request.setTelCasa(casaEdit.getText().toString().trim());
		request.setTelefono(celularEdit.getText().toString().trim());
		request.setTelOficina(oficinaEdit.getText().toString().trim());
		request.setTipoTarjeta(tipoTarjetaValues.getValorSeleccionado());
		
		String mes = (String) mesSpinner.getSelectedItem();
		String anio = (String) anioSpinner.getSelectedItem();
		
		request.setVigencia(mes +"/"+anio.substring(2, 4));
		
		String password  = session.getUserDetails().get(SessionManager.USR_PWD);
		
		new WebServiceClient(new UpdateDataListener(), DataUpdateActivity.this, true, UrlMc.UPDATE).execute(request.buildUpdate(DataUpdateActivity.this, password));
	}
	
	private class ProveedoresListener implements WSResponseListener {
		
		private static final String TAG = "ProveedoresListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONObject json = new JSONObject(response);
					JSONArray array = json.optJSONArray("proveedores");
					
					if (array.length() > 0) {
						ArrayList<BasicNameValuePair> proveedores = new ArrayList<BasicNameValuePair>(array.length());
						
						for (int i = 0; i < array.length(); i++) {
							JSONObject proveedor = array.optJSONObject(i);
							proveedores.add(new BasicNameValuePair(proveedor.optString("descripcion"), proveedor.optString("clave")));
						}
						
						proveedorValues = new SpinnerValues(DataUpdateActivity.this, proveedorSpinner, proveedores);
						
						ArrayList<BasicNameValuePair> generos = new ArrayList<BasicNameValuePair>(3);
						generos.add(new BasicNameValuePair("Seleccione...", "S"));
						generos.add(new BasicNameValuePair("Masculino", "M"));
						generos.add(new BasicNameValuePair("Femenino", "F"));
						generoValues = new SpinnerValues(DataUpdateActivity.this, generoSpinner, generos);
						
						new WebServiceClient(new EstadosListener(), DataUpdateActivity.this, true, UrlMc.ESTADOS).execute();
						
					} else {
						Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
						finish();
					}
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
					finish();
				}
			} else {
				Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}
	
	private class EstadosListener implements WSResponseListener {
		
		private static final String TAG = "EstadosListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				Log.i(TAG, response);
				
				try {
					JSONObject json = new JSONObject(response);
					JSONArray array = json.optJSONArray("estados");
					
					if (array.length() > 0) {
						ArrayList<BasicNameValuePair> proveedores = new ArrayList<BasicNameValuePair>(array.length());
						
						for (int i = 0; i < array.length(); i++) {
							JSONObject proveedor = array.optJSONObject(i);
							proveedores.add(new BasicNameValuePair(proveedor.optString("descripcion"), proveedor.optString("clave")));
						}
						
						estadosValues = new SpinnerValues(DataUpdateActivity.this, estadoSpinner, proveedores);
						new WebServiceClient(new TipoTarjetaListener(), DataUpdateActivity.this, true, UrlMc.TARJETAS).execute();
					} else {
						Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
						finish();
					}
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
					finish();
				}
			} else {
				Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}
	
	private class TipoTarjetaListener implements WSResponseListener {
		
		private static final String TAG = "TipoTarjetaListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				Log.i(TAG, response);
				
				try {
					JSONObject json = new JSONObject(response);
					JSONArray array = json.optJSONArray("tarjetas");
					
					if (array.length() > 0) {
						ArrayList<BasicNameValuePair> proveedores = new ArrayList<BasicNameValuePair>(array.length());
						
						for (int i = 0; i < array.length() - 1; i++) {
							JSONObject proveedor = array.optJSONObject(i);
							proveedores.add(new BasicNameValuePair(proveedor.optString("descripcion"), proveedor.optString("clave")));
						}
						
						tipoTarjetaValues = new SpinnerValues(DataUpdateActivity.this, tipoTarjetaSpinner, proveedores);
						setMesSpinner();
						setAnioSpinner();
						
						String login = session.getUserDetails().get(SessionManager.USR_LOGIN);
						String password = session.getUserDetails().get(SessionManager.USR_PWD);
						UpdateRequest request = new UpdateRequest(login, password);
						new WebServiceClient(new GetDataListener(), DataUpdateActivity.this, true, UrlMc.GET_DATA).execute(request.build());
					} else {
						Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
						finish();
					}
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
					finish();
				}
			} else {
				Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}
	
	private class GetDataListener implements WSResponseListener {
		
		private static final String TAG = "GetDataListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				Log.i(TAG, response);
				response = Crypto.aesDecrypt(Text.parsePass(session.getUserDetails().get(SessionManager.USR_PWD)), response);
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString(1));
					
					String apellido = json.optString("apellido");
					String materno = json.optString("materno");
					String[] vigenciaArray = json.optString("vigencia").split("/");
					String mes = vigenciaArray[0];
					
					if (mes.charAt(0) == '0') {
						mes = mes.substring(1);
					}
					
					String anio = vigenciaArray[1];
					String oficina = json.optString("tel_oficina");
					String sexo = json.optString("sexo");
					String exterior = String.valueOf(json.optInt("num_ext"));
					String colonia = json.optString("colonia");
					String nombre = json.optString("nombre");
					String login = json.optString("login");
					String proveedor = String.valueOf(json.optInt("proveedor"));
					String idEstado = String.valueOf(json.optInt("id_estado"));
					String correo = json.optString("mail");
					String ciudad = json.optString("ciudad");
					String nacimiento = json.optString("nacimiento");
					String celular = json.optString("telefono");
					String tipoTarjeta = String.valueOf(json.optString("tipotarjeta"));
					String tarjeta = json.optString("tarjeta");
					String casa = json.optString("tel_casa");
					String cp = json.optString("cp");
					String interior = json.optString("num_interior");
					String calle = json.optString("calle");
					
					paternoEdit.setText(apellido);
					maternoEdit.setText(materno);
					generoValues.setValorSeleccionado(sexo);
					mesSpinner.setSelection(Integer.valueOf(mes) -1);
					anioSpinner.setSelection(AppUtils.getAnioVigenciaPosition(anio));
					oficinaEdit.setText(oficina);
					exteriorEdit.setText(exterior);
					coloniaEdit.setText(colonia);
					nombreEdit.setText(nombre);
					usuarioEdit.setText(login);
					proveedorValues.setValorSeleccionado(proveedor);
					estadosValues.setValorSeleccionado(idEstado);
					correoEdit.setText(correo);
					ciudadEdit.setText(ciudad);
					nacimientoEdit.setText(nacimiento);
					setNacimientoDate(Text.getSimpleDateFromString(nacimiento));
					celularEdit.setText(celular);
					tipoTarjetaValues.setValorSeleccionado(tipoTarjeta);
					tarjetaGuardada = tarjeta;
					
					String lastTarjeta = tarjeta.substring(tarjeta.length() - 4, tarjeta.length());
					String maskedTarjeta = "";
					
					for (int i = 0; i < tarjeta.length() - 4; i++) {
						maskedTarjeta += "X";
					}
					
					tarjetaModificada = maskedTarjeta + lastTarjeta;
					
					numTarjetaEdit.setText(tarjetaModificada);
					casaEdit.setText(casa);
					cpEdit.setText(cp);
					interiorEdit.setText(interior);
					calleEdit.setText(calle);
					
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
					finish();
				}
			} else {
				Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}
	
	private String checkTarjeta() {
		String ret = "";
		String tarjeta = "";
		
		if (tarjetaModificada.equals(numTarjetaEdit.getText().toString().trim()))
			tarjeta = tarjetaGuardada;
		else
			tarjeta = numTarjetaEdit.getText().toString().trim();
		
		tarjetaFinal = tarjeta;
		Log.i(TAG, "tarjeta final: " + tarjetaFinal);
		
		
		if (tarjeta.equals(""))
			ret = "Debe ingresar un n�mero de tarjeta de cr�dito";
		else if (tarjeta.length() < 13)
			ret = "El m�nimo de n�meros de tarjeta de cr�dito es de 13";
		else if (tarjeta.length() > 16)
			ret = "El m�ximo de n�meros de tarjeta de cr�dito es de 20";
		else {
			for (int i = 0; i < tarjeta.length(); i++) {
				char letra = tarjeta.charAt(i);
				if (letra - 48 < 0 && letra - 48 > 9) {
					
					ret = "S�lo se permiten n�meros en la tarjeta de cr�dito";
					break;
				}
			}
		}
		
		return ret;
	}
	
	private class UpdateDataListener implements WSResponseListener {

		private static final String TAG = "UpdateDataListener";
		@Override
		public void StringResponse(String response) {
			Log.i(TAG, "Iniciando Listener");
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = Crypto.aesDecrypt(Text.parsePass(session.getUserDetails().get(SessionManager.USR_PWD)), response);
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					
					int resultado = json.optInt("resultado", -1);
					String mensaje = json.optString("mensaje", "Error de conexi�n. Intente de nuevo m�s tarde.");
					
					switch (resultado) {
					case 1:
						Toast.makeText(DataUpdateActivity.this, mensaje, Toast.LENGTH_SHORT).show();
						finish();
						break;

					default:
						Toast.makeText(DataUpdateActivity.this, mensaje, Toast.LENGTH_SHORT).show();
						break;
					}
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(DataUpdateActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
			}
		}
		
	}

}
