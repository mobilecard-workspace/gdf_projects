package org.addcel.mobilecard.constant;

public class UrlMc {
	
	private static final String DEVELOPMENT = "http://50.57.192.213:8080/";
	private static final String PRODUCTION = "http://mobilecard.mx:8080/";
	
	public static final String BASE_URL = PRODUCTION;

	public static final String LOGIN =  BASE_URL + "AddCelBridge/Servicios/adc_userLogin.jsp";
	public static final String REGISTRO = BASE_URL + "AddCelBridge/Servicios/adc_userInsert.jsp";
//	public static final String REGISTRO = BASE_URL + "AddCelBridge/Servicios/adc_userInsertGobierno.jsp";
	public static final String UPDATE_PASS = BASE_URL + "AddCelBridge/Servicios/adc_userPasswordUpdateMail.jsp";
	public static final String TARJETAS = BASE_URL + "AddCelBridge/Servicios/adc_getCardType.jsp";
	public static final String PROVEEDORES = BASE_URL + "AddCelBridge/Servicios/adc_getProviders.jsp";
	public static final String ESTADOS = BASE_URL + "AddCelBridge/Servicios/adc_getEstados.jsp";
	public static final String GET_DATA = BASE_URL + "AddCelBridge/Servicios/adc_getUserData.jsp";
	public static final String UPDATE = BASE_URL + "AddCelBridge/Servicios/adc_userUpdate.jsp";
	public static final String RECOVER_PASS = BASE_URL + "AddCelBridge/Servicios/adc_RecoveryUP.jsp";
	public static final String UPDATE_PASS_AFTER_RECOVER = BASE_URL + "AddCelBridge/Servicios/adc_userPasswordUpdate.jsp";
}
