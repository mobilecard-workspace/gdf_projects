/**
 * 
 */
package com.addcel.gdf.vo;

/**
 * @author ELopez
 *
 */
public class AplicacionUsuarioVO {

	private int id_usuario;
	private int idAplicacion;
	private String terminos;
	private int estatus;
	
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getId_aplicacion() {
		return idAplicacion;
	}
	public void setId_aplicacion(int id_aplicacion) {
		this.idAplicacion = id_aplicacion;
	}
	public String getTerminos() {
		return terminos;
	}
	public void setTerminos(String terminos) {
		this.terminos = terminos;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	
	
	
}
