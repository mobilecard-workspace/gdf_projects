package com.addcel.gdf.vo;

public class RespuestaVO {
	
	private int codError;
	private String detError;
	
	public int getCodError() {
		return codError;
	}
	public void setCodError(int codError) {
		this.codError = codError;
	}
	public String getDetError() {
		return detError;
	}
	public void setDetError(String detError) {
		this.detError = detError;
	}
	
	

}
