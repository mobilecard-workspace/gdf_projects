/**
 * InputDatosConsulta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.vo.agua;

public class AguaPreguntaFutVO  {
    private java.lang.String cuenta;

    private java.lang.String tipo;

    private java.lang.String id_aplicacion;

    private java.lang.String passwd;

    public AguaPreguntaFutVO() {
    }

    public AguaPreguntaFutVO(
           java.lang.String cuenta,
           java.lang.String tipo,
           java.lang.String id_aplicacion,
           java.lang.String passwd) {
           this.cuenta = cuenta;
           this.tipo = tipo;
           this.id_aplicacion = id_aplicacion;
           this.passwd = passwd;
    }


    /**
     * Gets the cuenta value for this InputDatosConsulta.
     * 
     * @return cuenta
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }


    /**
     * Sets the cuenta value for this InputDatosConsulta.
     * 
     * @param cuenta
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }


    /**
     * Gets the tipo value for this InputDatosConsulta.
     * 
     * @return tipo
     */
    public java.lang.String getTipo() {
        return tipo;
    }


    /**
     * Sets the tipo value for this InputDatosConsulta.
     * 
     * @param tipo
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }


    /**
     * Gets the id_aplicacion value for this InputDatosConsulta.
     * 
     * @return id_aplicacion
     */
    public java.lang.String getId_aplicacion() {
        return id_aplicacion;
    }


    /**
     * Sets the id_aplicacion value for this InputDatosConsulta.
     * 
     * @param id_aplicacion
     */
    public void setId_aplicacion(java.lang.String id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }


    /**
     * Gets the passwd value for this InputDatosConsulta.
     * 
     * @return passwd
     */
    public java.lang.String getPasswd() {
        return passwd;
    }


    /**
     * Sets the passwd value for this InputDatosConsulta.
     * 
     * @param passwd
     */
    public void setPasswd(java.lang.String passwd) {
        this.passwd = passwd;
    }
}
