/**
 * OutputDatosFormato.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.vo.agua;

import java.math.BigDecimal;

import com.addcel.gdf.vo.AbstractVO;

public class AguaRespuestaGuardaVO extends AbstractVO{
	
	private java.lang.String vanio;
	private java.lang.String vbimestre;
	private java.lang.String vderecho;
    private java.lang.String vderdom;
    private java.lang.String vderndom;
    private java.lang.String vuso;
    private java.lang.String viva;
    private java.lang.String vtotal;
    private java.lang.String cuenta;
    private java.lang.String vigencia;
    private java.lang.String numError;
    private BigDecimal monto_maximo_operacion;

    /**
     * Gets the cuenta value for this OutputDatosFormato.
     * 
     * @return cuenta
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }


    /**
     * Sets the cuenta value for this OutputDatosFormato.
     * 
     * @param cuenta
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * Gets the vigencia value for this OutputDatosFormato.
     * 
     * @return vigencia
     */
    public java.lang.String getVigencia() {
        return vigencia;
    }


    /**
     * Sets the vigencia value for this OutputDatosFormato.
     * 
     * @param vigencia
     */
    public void setVigencia(java.lang.String vigencia) {
        this.vigencia = vigencia;
    }

    public java.lang.String getVanio() {
		return vanio;
	}

	public void setVanio(java.lang.String vanio) {
		this.vanio = vanio;
	}

	public java.lang.String getVbimestre() {
		return vbimestre;
	}

	public void setVbimestre(java.lang.String vbimestre) {
		this.vbimestre = vbimestre;
	}

	public java.lang.String getVderdom() {
		return vderdom;
	}

	public void setVderdom(java.lang.String vderdom) {
		this.vderdom = vderdom;
	}

	public java.lang.String getVderndom() {
		return vderndom;
	}

	public void setVderndom(java.lang.String vderndom) {
		this.vderndom = vderndom;
	}

	public java.lang.String getVuso() {
		return vuso;
	}

	public void setVuso(java.lang.String vuso) {
		this.vuso = vuso;
	}

	public java.lang.String getViva() {
		return viva;
	}

	public void setViva(java.lang.String viva) {
		this.viva = viva;
	}

	public java.lang.String getVtotal() {
		return vtotal;
	}

	public void setVtotal(java.lang.String vtotal) {
		this.vtotal = vtotal;
		setTotalPago(vtotal);
	}


	public java.lang.String getVderecho() {
		return vderecho;
	}


	public void setVderecho(java.lang.String vderecho) {
		this.vderecho = vderecho;
	}

	public java.lang.String getNumError() {
		return numError;
	}


	public void setNumError(java.lang.String numError) {
		this.numError = numError;
	}


	public BigDecimal getMonto_maximo_operacion() {
		return monto_maximo_operacion;
	}


	public void setMonto_maximo_operacion(BigDecimal monto_maximo_operacion) {
		this.monto_maximo_operacion = monto_maximo_operacion;
	}

}
