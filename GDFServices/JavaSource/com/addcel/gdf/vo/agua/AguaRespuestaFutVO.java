/**
 * OutputDatosFut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.vo.agua;

import com.addcel.gdf.ws.clientes.agua.DatosFut;
import com.addcel.gdf.ws.clientes.agua.DatosGenerales;

public class AguaRespuestaFutVO {
    private DatosFut[] fut;

    private java.lang.String folio;

    private DatosGenerales[] datosGenerales;

    private java.lang.String error;

    private java.lang.String origen;

    private java.lang.String sql;

    public AguaRespuestaFutVO() {
    }

    public AguaRespuestaFutVO(
           DatosFut[] fut,
           java.lang.String folio,
           DatosGenerales[] datosGenerales,
           java.lang.String error,
           java.lang.String origen,
           java.lang.String sql) {
           this.fut = fut;
           this.folio = folio;
           this.datosGenerales = datosGenerales;
           this.error = error;
           this.origen = origen;
           this.sql = sql;
    }


    /**
     * Gets the fut value for this OutputDatosFut.
     * 
     * @return fut
     */
    public DatosFut[] getFut() {
        return fut;
    }


    /**
     * Sets the fut value for this OutputDatosFut.
     * 
     * @param fut
     */
    public void setFut(DatosFut[] fut) {
        this.fut = fut;
    }


    /**
     * Gets the folio value for this OutputDatosFut.
     * 
     * @return folio
     */
    public java.lang.String getFolio() {
        return folio;
    }


    /**
     * Sets the folio value for this OutputDatosFut.
     * 
     * @param folio
     */
    public void setFolio(java.lang.String folio) {
        this.folio = folio;
    }


    /**
     * Gets the datosGenerales value for this OutputDatosFut.
     * 
     * @return datosGenerales
     */
    public DatosGenerales[] getDatosGenerales() {
        return datosGenerales;
    }


    /**
     * Sets the datosGenerales value for this OutputDatosFut.
     * 
     * @param datosGenerales
     */
    public void setDatosGenerales(DatosGenerales[] datosGenerales) {
        this.datosGenerales = datosGenerales;
    }


    /**
     * Gets the error value for this OutputDatosFut.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this OutputDatosFut.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }


    /**
     * Gets the origen value for this OutputDatosFut.
     * 
     * @return origen
     */
    public java.lang.String getOrigen() {
        return origen;
    }


    /**
     * Sets the origen value for this OutputDatosFut.
     * 
     * @param origen
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }


    /**
     * Gets the sql value for this OutputDatosFut.
     * 
     * @return sql
     */
    public java.lang.String getSql() {
        return sql;
    }


    /**
     * Sets the sql value for this OutputDatosFut.
     * 
     * @param sql
     */
    public void setSql(java.lang.String sql) {
        this.sql = sql;
    }
}
