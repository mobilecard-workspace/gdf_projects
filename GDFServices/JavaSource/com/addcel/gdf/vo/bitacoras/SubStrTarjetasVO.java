package com.addcel.gdf.vo.bitacoras;

public class SubStrTarjetasVO {
	private String subStr6;
	private String subStr7;
	private String subStr8;
	private String subStr9;
	private String subStr10;
	public String getSubStr6() {
		return subStr6;
	}
	public void setSubStr6(String subStr6) {
		this.subStr6 = subStr6;
	}
	public String getSubStr7() {
		return subStr7;
	}
	public void setSubStr7(String subStr7) {
		this.subStr7 = subStr7;
	}
	public String getSubStr8() {
		return subStr8;
	}
	public void setSubStr8(String subStr8) {
		this.subStr8 = subStr8;
	}
	public String getSubStr9() {
		return subStr9;
	}
	public void setSubStr9(String subStr9) {
		this.subStr9 = subStr9;
	}
	public String getSubStr10() {
		return subStr10;
	}
	public void setSubStr10(String subStr10) {
		this.subStr10 = subStr10;
	}
}
