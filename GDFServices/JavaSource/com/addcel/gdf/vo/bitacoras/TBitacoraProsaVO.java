/**
 * 
 */
package com.addcel.gdf.vo.bitacoras;

/**
 * @author ELopez
 *
 */
public class TBitacoraProsaVO {
	
	private long id_bitacoraProsa;
	private long id_bitacora;
	private long id_usuario;
	private String tarjeta;
	private String transaccion;
	private String autorizacion;
	private String fecha;
	private String bit_hora;
	private String concepto;
	private double cargo;
	private double comision;
	private String cx;
	private String cy;
	
	public String toString(){
		return new StringBuffer()
			.append("\nid_bitacoraProsa: ").append(id_bitacoraProsa)
			.append("\nid_bitacora: ").append(id_bitacora)
			.append("\nid_usuario: ").append(id_usuario)
			.append("\ntarjeta: ").append(tarjeta)
			.append("\ntransaccion: ").append(transaccion)
			.append("\nautorizacion: ").append(autorizacion)
			.append("\nfecha: ").append(fecha)
			.append("\nbit_hora: ").append(bit_hora)
			.append("\nconcepto: ").append(concepto)
			.append("\ncargo: ").append(cargo)
			.append("\ncomision: ").append(comision)
			.append("\ncx: ").append(cx)
			.append("\ncy: ").append(cy).toString();
	}

	public long getId_bitacoraProsa() {
		return id_bitacoraProsa;
	}

	public void setId_bitacoraProsa(long id_bitacoraProsa) {
		this.id_bitacoraProsa = id_bitacoraProsa;
	}

	public long getId_bitacora() {
		return id_bitacora;
	}

	public void setId_bitacora(long id_bitacora) {
		this.id_bitacora = id_bitacora;
	}

	public long getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(long id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getTransaccion() {
		return transaccion;
	}

	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}

	public String getAutorizacion() {
		return autorizacion;
	}

	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getBit_hora() {
		return bit_hora;
	}

	public void setBit_hora(String bit_hora) {
		this.bit_hora = bit_hora;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public double getCargo() {
		return cargo;
	}

	public void setCargo(double cargo) {
		this.cargo = cargo;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getCx() {
		return cx;
	}

	public void setCx(String cx) {
		this.cx = cx;
	}

	public String getCy() {
		return cy;
	}

	public void setCy(String cy) {
		this.cy = cy;
	}

	
}
