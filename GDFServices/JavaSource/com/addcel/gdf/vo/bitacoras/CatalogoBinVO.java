package com.addcel.gdf.vo.bitacoras;

public class CatalogoBinVO {
	private String bin;
	private String tipo;
	public CatalogoBinVO(String bin, String tipo) {
		super();
		this.bin = bin;
		this.tipo = tipo;
	}
	public CatalogoBinVO(){
		
	}
	public String getBin() {
		return bin;
	}
	public void setBin(String bin) {
		this.bin = bin;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
