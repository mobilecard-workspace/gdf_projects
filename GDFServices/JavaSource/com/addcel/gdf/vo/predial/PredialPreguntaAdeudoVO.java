/**
 * Tipo_pregunta_adeudo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.vo.predial;

public class PredialPreguntaAdeudoVO {
    private java.lang.Integer clave;

    private java.lang.String cuenta;

    private java.lang.String tipo_adeudo;
    
    public PredialPreguntaAdeudoVO() {
    }

    public PredialPreguntaAdeudoVO(
           java.lang.Integer clave,
           java.lang.String cuenta,
           java.lang.String password,
           java.lang.String tipo_adeudo,
           java.lang.String usuario) {
           this.clave = clave;
           this.cuenta = cuenta;
           this.tipo_adeudo = tipo_adeudo;
    }


    /**
     * Gets the clave value for this Tipo_pregunta_adeudo.
     * 
     * @return clave
     */
    public java.lang.Integer getClave() {
        return clave;
    }


    /**
     * Sets the clave value for this Tipo_pregunta_adeudo.
     * 
     * @param clave
     */
    public void setClave(java.lang.Integer clave) {
        this.clave = clave;
    }


    /**
     * Gets the cuenta value for this Tipo_pregunta_adeudo.
     * 
     * @return cuenta
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }


    /**
     * Sets the cuenta value for this Tipo_pregunta_adeudo.
     * 
     * @param cuenta
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }


    /**
     * Gets the tipo_adeudo value for this Tipo_pregunta_adeudo.
     * 
     * @return tipo_adeudo
     */
    public java.lang.String getTipo_adeudo() {
        return tipo_adeudo;
    }


    /**
     * Sets the tipo_adeudo value for this Tipo_pregunta_adeudo.
     * 
     * @param tipo_adeudo
     */
    public void setTipo_adeudo(java.lang.String tipo_adeudo) {
        this.tipo_adeudo = tipo_adeudo;
    }

}
