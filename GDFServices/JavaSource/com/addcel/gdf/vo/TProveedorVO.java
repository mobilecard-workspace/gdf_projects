package com.addcel.gdf.vo;

import java.io.Serializable;
import java.util.Date;

public class TProveedorVO implements Serializable{
	private long idProveedor;
	private String afiliacion;
	private String maxTransaccion;
	public long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	public String getMaxTransaccion() {
		return maxTransaccion;
	}
	public void setMaxTransaccion(String maxTransaccion) {
		this.maxTransaccion = maxTransaccion;
	}
	
	
}
