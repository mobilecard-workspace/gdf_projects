package com.addcel.gdf.vo.licencia;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.gdf.vo.AbstractVO;

@JsonIgnoreProperties(ignoreUnknown=true)
public class LicenciaConductorResponseVO  extends AbstractVO{
	
	private String rfc;
	
	private double importeInicial;
	
	private double importeFinal;
	
	private String fechaModificacion;
	
	private float costoUnitario;
	
	private String vigencia;
	
	private BigDecimal ejercicio;
	
	private String folio;
	
	private int statusArchivo;
	
	private boolean permanente;
	
	private int clave;
	
	private String concepto;
	
	private String nombreTramite;
	
	private String nombreSubConcepto;
	
	private String numError;
	
	private BigDecimal monto_maximo_operacion;
	
	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public boolean isPermanente() {
		return permanente;
	}

	public void setPermanente(boolean permanente) {
		this.permanente = permanente;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public float getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(float costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public BigDecimal getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(BigDecimal ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public int getStatusArchivo() {
		return statusArchivo;
	}

	public void setStatusArchivo(int statusArchivo) {
		this.statusArchivo = statusArchivo;
	}

	public int getClave() {
		return clave;
	}

	public void setClave(int clave) {
		this.clave = clave;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getNombreTramite() {
		return nombreTramite;
	}

	public void setNombreTramite(String nombreTramite) {
		this.nombreTramite = nombreTramite;
	}

	public double getImporteInicial() {
		return importeInicial;
	}

	public void setImporteInicial(double importeInicial) {
		this.importeInicial = importeInicial;
	}

	public double getImporteFinal() {
		return importeFinal;
	}

	public void setImporteFinal(double importeFinal) {
		this.importeFinal = importeFinal;
	}

	public String getNombreSubConcepto() {
		return nombreSubConcepto;
	}

	public void setNombreSubConcepto(String nombreSubConcepto) {
		this.nombreSubConcepto = nombreSubConcepto;
	}

	public String getNumError() {
		return numError;
	}

	public void setNumError(String numError) {
		this.numError = numError;
	}

	public BigDecimal getMonto_maximo_operacion() {
		return monto_maximo_operacion;
	}

	public void setMonto_maximo_operacion(BigDecimal monto_maximo_operacion) {
		this.monto_maximo_operacion = monto_maximo_operacion;
	}

	
}
