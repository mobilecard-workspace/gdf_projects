/**
 * 
 */
package com.addcel.gdf.vo.terminos;

/**
 * @author ELopez
 *
 */
public class EstatusTerminosUsuarioVO {

	private int idAplicacion; 
	private String descripcionAplicacion; 
	private int idTermino; 
	private String terminos;
	
	public int getIdAplicacion() {
		return idAplicacion;
	}
	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
	public String getDescripcionAplicacion() {
		return descripcionAplicacion;
	}
	public void setDescripcionAplicacion(String descripcionAplicacion) {
		this.descripcionAplicacion = descripcionAplicacion;
	}
	public int getIdTermino() {
		return idTermino;
	}
	public void setIdTermino(int idTermino) {
		this.idTermino = idTermino;
	}
	public String getTerminos() {
		return terminos;
	}
	public void setTerminos(String terminos) {
		this.terminos = terminos;
	}
	
	
}
