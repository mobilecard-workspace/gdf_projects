/**
 * 
 */
package com.addcel.gdf.vo.terminos;

/**
 * @author ELopez
 *
 */
public class TerminosCondicionesVO {

	private String id_usuario;
	private String id_aplicacion;
	private String id_termino;
	private String desc_termino;
	private String status;
	
	private int id_producto;

	public String getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getId_aplicacion() {
		return id_aplicacion;
	}

	public void setId_aplicacion(String id_aplicacion) {
		this.id_aplicacion = id_aplicacion;
	}

	public String getId_termino() {
		return id_termino;
	}

	public void setId_termino(String id_termino) {
		this.id_termino = id_termino;
	}

	public String getDesc_termino() {
		return desc_termino;
	}

	public void setDesc_termino(String desc_termino) {
		this.desc_termino = desc_termino;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getId_producto() {
		return id_producto;
	}

	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}

	
}
