/**
 * 
 */
package com.addcel.gdf.vo.terminos;

/**
 * @author ELopez
 *
 */
public class TerminosUsuarioVO {

	private int id_usuario;
	private int idTermino;
	
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getIdTermino() {
		return idTermino;
	}
	public void setIdTermino(int idTermino) {
		this.idTermino = idTermino;
	}
	
}


