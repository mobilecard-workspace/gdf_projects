package com.addcel.gdf.vo;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import com.addcel.utils.Utilerias;

public class TestCertifiado {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		TestCertifiado test = new TestCertifiado();
		test.generaCertificado();

	}

	public void generaCertificado(){
		//String llaveBanco="��6��	pP;E5\n�";
		String banco = "986";
		String no_autorizacion = "000007665861";
		String fechaPago = "2013-08-19 12:17:22";
		String linea_captura = "84111XX183XGF9EM595J";
		String totalPago = "3250.00";
		String certificadoF = null;
		String cadPag = "";
		String cPago = "";
		String cadCer = "";
		try
		{
//			String cPago = banco + no_autorizacion + Utilerias.formatoFecha(fechaPago) + linea_captura + 
//					Utilerias.formatoImporteProsa(Utilerias.formatoMontoProsa(totalPago));
			cPago = banco;
			System.out.println("line: " + cPago + "\t\t\t\t\t\t\t\tfield: " + banco + " --> Banco");
			
			cPago += no_autorizacion;
			System.out.println("line: " + cPago + "\t\t\t\t\t\t\tfield: " + no_autorizacion + " --> no_autorizacion");
			
			cPago +=  Utilerias.formatoFecha(fechaPago);
			System.out.println("line: " + cPago + "\t\t\t\t\t\tfield: " +  Utilerias.formatoFecha(fechaPago) + " --> fechaPago");
			
			cPago += linea_captura;
			System.out.println("line: " + cPago + "\t\t\tfield: " + linea_captura + " --> linea_captura");
			
			cPago += Utilerias.formatoImporteProsa(Utilerias.formatoMontoProsa(totalPago));
			System.out.println("line: " + cPago + "\tfield: " + Utilerias.formatoImporteProsa(Utilerias.formatoMontoProsa(totalPago)) + " --> totalPago");
			
			System.out.println("cPag: " + cPago);
			
//			byte[] llaveBanco = {3,29,-105,-9,54,-121,-101,9,112,80,59,69,53,20,10,-121}; 		
			byte[] llaveBanco = {57, 15, -29, 39, -63, -33, 122, 125, -21, -102, 124, -42, -128, -128, 85, -95};
			byte[] cadenaPago= cPago.getBytes();		
			byte[] certificado=new byte[llaveBanco.length+cadenaPago.length];
			for(int i = 0; i < cadenaPago.length; i ++){
				cadPag += cadenaPago[i];
			}
			System.out.println("cadPag: " + cadPag);
			
			System.out.println("certificado byte: " + certificado);
			
			System.arraycopy(cadenaPago, 0, certificado, 0, cadenaPago.length);
			System.arraycopy(llaveBanco, 0, certificado, cadenaPago.length,llaveBanco.length);		
			
			for(int i = 0; i < certificado.length; i ++){
				cadCer += certificado[i];
			}
			
			System.out.println("cadCer: " + cadCer);
			
			byte[] md5c = DigestUtils.md5(certificado);
			byte[] base64 = Base64.encodeBase64(md5c);
			System.out.println("base64: " + base64);
			
			certificadoF = new String(base64).substring(0, 22);
			System.out.println("certificadoF: " + certificadoF);
		}catch(Exception e){
			System.out.println("Error al crear certificdo Digital: ");
			e.printStackTrace();
		}
	}
	
}
