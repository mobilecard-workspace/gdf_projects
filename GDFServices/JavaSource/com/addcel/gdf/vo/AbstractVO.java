package com.addcel.gdf.vo;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.utils.Utilerias;

public class AbstractVO {
	private static final Logger logger = LoggerFactory.getLogger(AbstractVO.class);
	
	private long id_bitacora;
	private long id_usuario;
	private int id_producto;
	private String no_autorizacion;
	private String linea_captura;
	private String fechaPago; 
	private int statusPago;
	private String error;
	private String descError;
	private String totalPago = "0.00";
	private String banco = "986";
	private String certificado ;
	private String cvv2;
	private String token;
	private String password;
	// MLS imprimir numero de tarjeta en pdf
	private String ultimos4TDC;
	private String tipoTarjeta;
	
	public AbstractVO(){
		
	}
	
	public AbstractVO(String error, String descError){
		this.error = error;
		this.descError = descError;
	}
	
	//se usa ara guardar la afiliacion que se uso en el pago
	private String afiliacion;
	
	public long getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(long id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public long getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(long id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getId_producto() {
		return id_producto;
	}
	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}
	public String getNo_autorizacion() {
		return no_autorizacion;
	}
	public void setNo_autorizacion(String no_autorizacion) {
		this.no_autorizacion = no_autorizacion;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		if(fechaPago != null && fechaPago.length() == 21){
			this.fechaPago = fechaPago.substring(0, 19);
		}else{
			this.fechaPago = fechaPago;
		}
	}
	public int getStatusPago() {
		return statusPago;
	}
	public void setStatusPago(int statusPago) {
		this.statusPago = statusPago;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getDescError() {
		return descError;
	}
	public void setDescError(String descError) {
		this.descError = descError;
	}
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
	}
	
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		if(totalPago != null){
			try{
				this.totalPago = Utilerias.formatoImporte(totalPago);
			}catch(Exception e){
				logger.error("Error setTotalPago: ", e);
			}
		}
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getCertificado() {
		return certificado;
	}
	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}
	
	public void generaCertificado(){
		//String llaveBanco="��6��	pP;E5\n�";
//		String cadPag = "";
		String cPago = "";
//		String cadCer = "";
		try
		{
//			String cPago = banco + no_autorizacion + Utilerias.formatoFecha(fechaPago) + linea_captura + 
//					Utilerias.formatoImporteProsa(Utilerias.formatoMontoProsa(totalPago));
			cPago = banco;
			//logger.info("line: " + cPago + "\t\t\t\t\t\t\t\tfield: " + banco + " --> Banco");
			
			cPago += Utilerias.formatAddCeros(no_autorizacion, 12, true);
			//logger.info("line: " + cPago + "\t\t\t\t\t\t\tfield: " + no_autorizacion + " --> no_autorizacion");
			
			cPago +=  Utilerias.formatoFecha(fechaPago);
			//logger.info("line: " + cPago + "\t\t\t\t\t\tfield: " +  Utilerias.formatoFecha(fechaPago) + " --> fechaPago");
			
			cPago += linea_captura;
			//logger.info("line: " + cPago + "\t\t\tfield: " + linea_captura + " --> linea_captura");
			
			cPago += Utilerias.formatoImporteProsa(Utilerias.formatoMontoProsa(totalPago));
			//logger.info("line: " + cPago + "\tfield: " + Utilerias.formatoImporteProsa(Utilerias.formatoMontoProsa(totalPago)) + " --> totalPago");
			
			logger.info("cPag: " + cPago);
			
//			byte[] llaveBanco = {3,29,-105,-9,54,-121,-101,9,112,80,59,69,53,20,10,-121}; 		
			byte[] llaveBanco = {57, 15, -29, 39, -63, -33, 122, 125, -21, -102, 124, -42, -128, -128, 85, -95};
			byte[] cadenaPago= cPago.getBytes();		
			byte[] certificado=new byte[llaveBanco.length+cadenaPago.length];
			
//			for(int i = 0; i < cadenaPago.length; i ++){
//				cadPag += cadenaPago[i];
//			}
			//logger.debug("cadPag: " + cadPag);
			//logger.info("certificado byte: " + certificado);
			
			System.arraycopy(cadenaPago, 0, certificado, 0, cadenaPago.length);
			System.arraycopy(llaveBanco, 0, certificado, cadenaPago.length,llaveBanco.length);		
			
//			for(int i = 0; i < certificado.length; i ++){
//				cadCer += certificado[i];
//			}
			
			//logger.debug("cadCer: " + cadCer);
			
			byte[] md5c = DigestUtils.md5(certificado);
			byte[] base64 = Base64.encodeBase64(md5c);
			//logger.debug("base64: " + base64);
			
			this.certificado = new String(base64).substring(0, 22);
			logger.debug("certificadoF: " + this.certificado);
			
		}catch(Exception e){
			logger.error("Error al crear certificdo Digital: ", e);
		}
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	// inicio MLS imprimir numero de tarjeta en pdf
	public String getUltimos4TDC() {
		return ultimos4TDC;
	}
	public void setUltimos4TDC(String ultimos4tdc) {
		ultimos4TDC = ultimos4tdc;
	}
	// fin MLS imprimir numero de tarjeta en pdf
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
}
