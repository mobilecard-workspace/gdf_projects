package com.addcel.gdf.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.service.ProcomThreeSecureService;
import com.addcel.utils.AddcelCrypto;

/**
 * Servlet implementation class Procom3DSecureGDF
 */
public class Procom3DSecureGDF extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(Procom3DSecureGDF.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Procom3DSecureGDF() {
        super();
        // TODO Auto-generated constructor stub
    }         
        	
	/**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    	String json = (String) request.getAttribute("json");

		if (json == null) {
			json = (String) request.getParameter("json");
		}

		if (json == null) {
			try {
				json = getFromStream(request);
			} catch (UnsupportedEncodingException ueE) {
				json = "{\"error\":\"1\",\"numError\":\"UE1001\"}";
			} catch (IOException ioE) {
				json = "{\"error\":\"1\",\"numError\":\"A1001\"}";
			}
		}

		
		if (json != null) {
			try {
				//logger.debug("************* Json: " + json);
				json = AddcelCrypto.decryptSensitive(json);
				//logger.debug("************* Json Decript: " + json);
				logger.debug("Procesando peticion");
				
		        new ProcomThreeSecureService().inicioProceso(json, request, response);
					
			} catch (Exception e) {
        		request.setAttribute("descError", "Error General: " + e.getMessage());
				request.getRequestDispatcher("/procom/error.jsp").forward(request, response);
			}

		}
    }
    
    private String getFromStream(HttpServletRequest request)
			throws UnsupportedEncodingException, IOException {

		String json = null;
		InputStream is = null;

		is = (InputStream) request.getInputStream();

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {

				if (is != null) {
					is.close();
				}
			}
			json = writer.toString();
		}

		return json;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	

}
