package com.addcel.gdf.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.service.ProcomService;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.ConstantesGDF;

/**
 * Servlet implementation class BuscaTerminos
 */
public class GeneraReciboGDF extends HttpServlet {
	private static final Logger logger = LoggerFactory.getLogger(GeneraReciboGDF.class);
	private static final long serialVersionUID = 1L;
	
	// location to store file uploaded
    private static final String UPLOAD_DIRECTORY = "upload";
 
    // upload settings
    private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
    private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GeneraReciboGDF() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Upon receiving file upload submission, parses the request to read
     * upload data and saves the file on disk.
     */
    protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
    	
    	InputStreamReader fileReader = null;
    	BufferedReader buffReader = null;
    	List<String> data = null;
    	TBitacoraVO tbitacora=null;
    	ProcomService ps = new ProcomService();
    	StringBuffer paramEmail = null;
    	StringBuilder cadena = new StringBuilder();
    	String respuesta = null;
    	String jsonObject = null;
    	
    	// checks if the request actually contains upload file
        if (!ServletFileUpload.isMultipartContent(request)) {
            // if not, we stop here
            PrintWriter writer = response.getWriter();
            writer.println("Error: Form must has enctype=multipart/form-data.");
            writer.flush();
            return;
        }
 
        // configures upload settings
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // sets memory threshold - beyond which files are stored in disk
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        // sets temporary location to store files
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
 
        ServletFileUpload upload = new ServletFileUpload(factory);
         
        // sets maximum size of upload file
        upload.setFileSizeMax(MAX_FILE_SIZE);
         
        // sets maximum size of request (include file + form data)
        upload.setSizeMax(MAX_REQUEST_SIZE);
 
        // constructs the directory path to store upload file
        // this path is relative to application's directory
//        String uploadPath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY;
//         
//        // creates the directory if it does not exist
//        File uploadDir = new File(uploadPath);
//        if (!uploadDir.exists()) {
//            uploadDir.mkdir();
//        }
 
        try {
            // parses the request's content to extract file data
//            @SuppressWarnings("unchecked")
            List<FileItem> formItems = upload.parseRequest(request);
 
            if (formItems != null && formItems.size() > 0) {
                // iterates over form's fields
                for (FileItem item : formItems) {
                    // processes only fields that are not form fields
                    if (!item.isFormField()) {
//                        String fileName = new File(item.getName()).getName();
//                        String filePath = uploadPath + File.separator + fileName;
//                        File storeFile = new File(filePath);
// 
//                        // saves the file on disk
//                        item.write(storeFile);
//                        request.setAttribute("message",  "Upload has been done successfully!");
                        
                        fileReader = new InputStreamReader(item.getInputStream());
                		buffReader = new BufferedReader(fileReader);
                    }
                }
                
                if (buffReader != null){
                	data = readFileDataStore(buffReader, request);
                	
                	if(data != null){
                		for(String idBitacora: data){
                			try{
                				tbitacora = ps.selectTbitacora(idBitacora);
        						jsonObject = ps.jsonObject( tbitacora.getId_bitacora(), tbitacora.getId_producto(), 
        								ps.getUltimos4TDC(tbitacora.getTarjeta_compra()));        		

        						paramEmail = new StringBuffer()
    		        				.append("nombre=").append(tbitacora.getDestino())
    		        				.append("&json=").append(jsonObject);
    		        			 
    		        			logger.info("Invio de Emai Parametros: " + paramEmail);
    		        			respuesta = ps.peticionUrlPost(ConstantesGDF.URL_MAIL_GENERA + tbitacora.getId_producto(), paramEmail.toString());
    		        			logger.info("***********    respuesta ***********: " + respuesta);
    		        			cadena.append("<tr><td>").append(tbitacora.getId_bitacora()).append("</td><td>").append(tbitacora.getDestino());
    		        			cadena.append("</td><td>").append(respuesta).append("</td></tr>");
                			}catch(Exception e){
                				logger.error("Error obtencion data: ", e);
                			}
                		}
                		request.setAttribute("data",  cadena);
                	}
                }
                request.setAttribute("message",  "Upload has been done successfully!");
            }
        } catch (Exception ex) {
        	logger.error("Error general: ", ex);
            request.setAttribute("message", "There was an error: " + ex.getMessage());
        }
        // redirects client to message page
        getServletContext().getRequestDispatcher("/view/message.jsp").forward(request, response);
        
    }
    
    public List<String> readFileDataStore(BufferedReader buffReader, HttpServletRequest request){
		List<String> data = new ArrayList<String>();
		String line = null;
		
		try {
			while ((line = buffReader.readLine()) != null){
				data.add(line);
			}
		}catch (Exception pe) {
			request.setAttribute("message", "There was an error: " + pe.getMessage());
			logger.error("Ocurrio un error: {}", pe);
		}
		return data;
	}

    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

}
