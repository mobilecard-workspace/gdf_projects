package com.addcel.gdf.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.service.ComunicadosService;
import com.addcel.gdf.vo.comunicados.ListaComunicadosVO;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.json.me.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class BuscaComunicados
 */
public class BuscaComunicados extends HttpServlet {
	private static final Logger log = LoggerFactory.getLogger(BuscaComunicados.class);
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuscaComunicados() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
    	int idAplicacion = 0;
    	ListaComunicadosVO comunicados = null; 
    	String respJson = null;
    	
    	try{
    		String json = (String) request.getAttribute("json");

    		if (json == null) {
    			json = (String) request.getParameter("json");
    		}
    		
    		if (json == null) {
    			try {
    				json = getFromStream(request);
    			} catch (UnsupportedEncodingException ueE) {
    				respJson = "{\"error\":\"Falta el parametro json\",\"numError\":\"00001\"}";
    			} catch (IOException ioE) {
    				respJson = "{\"error\":\"Falta el parametro json\",\"numError\":\"00001\"}";
    			}
    		}

			if (json != null) {
				json = AddcelCrypto.decryptHard(json);
				
				JSONObject jsObject = new JSONObject(json);
					
				if (jsObject.has("idAplicacion")){
					idAplicacion = Integer.parseInt(jsObject.getString("idAplicacion"));
						
			    	Gson gSon = new GsonBuilder().disableHtmlEscaping().create();
			    	ComunicadosService dao = new ComunicadosService();
			        
			        comunicados = dao.consultaComunicados(idAplicacion);
			        
			        respJson = gSon.toJson(comunicados);
				}else{
					respJson = "{\"error\":\"Falta el parametro idAplicacion\",\"numError\":\"00001\"}";
				}
//				respJson = "{\"comunicados\":[{\"idComunicado\":1,\"contenido\":\"Prueba de comunicado para " +
//						"las aplicaciones del GDF MobileCard\",\"estatus\":0,\"idEmpresa\":0,\"idAplicacion\":0," +
//						"\"prioridad\":1,\"fgColor\":\"EEEEEE\",\"bgColor\":\"HHHHHH\",\"tamFuente\":10},{\"idComunicado\":2," +
//						"\"contenido\":\"Prueba de comunicado, Bienvenido a la aplicacion del Gobierno del Distrito Federal," +
//						" para el pago de Tenencia, Infracciones, Nomina, Agua y Predial.\",\"estatus\":0,\"idEmpresa\":" +
//						"0,\"idAplicacion\":0,\"prioridad\":2,\"fgColor\":\"EEEEEE\",\"bgColor\":\"HHHHHH\",\"tamFuente\":10}]}";
			}else{
				respJson = "{\"error\":\"Falta el parametro json\",\"numError\":\"00001\"}";
			}
		}catch(Exception e){
			log.error("Ocurrio un error Servlet Comunicados", e);
			respJson = "{\"error\":\"" + e.getMessage() + "\",\"numError\":\"00002\"}";
    	}
    	try{
    		respJson = AddcelCrypto.encryptHard(respJson, false);
    		
	    	response.setContentType("application/Json");
			response.setContentLength(respJson.length());
			OutputStream out = response.getOutputStream();
			out.write(respJson.getBytes());
			out.flush();
			out.close();
    	}catch(Exception e){
			log.error("Ocurrio un error Servlet Comunicados", e);
			respJson = "{\"error\":\"" + e.getMessage() + "\",\"numError\":\"00003\"}";
    	}
    }
    
    private String getFromStream(HttpServletRequest request)
			throws UnsupportedEncodingException, IOException {

		String json = null;
		InputStream is = null;

		is = (InputStream) request.getInputStream();

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {

				if (is != null) {
					is.close();
				}
			}
			json = writer.toString();
		}

		return json;
	}
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

}
