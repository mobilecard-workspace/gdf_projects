package com.addcel.gdf.servlet;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.service.CatalogoMarcasService;
import com.addcel.utils.AddcelCrypto;

public class ConsultaCatalogoMarcas  extends HttpServlet {

	private static final Logger log = LoggerFactory.getLogger(ConsultaPagos.class);
	
	private static final long serialVersionUID = 1L;
	
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String json = null;
		
		CatalogoMarcasService marcasService = new CatalogoMarcasService();
		json = marcasService.getMarcas();
//		log.info("Json Respuesta: " + json);
		
		json = AddcelCrypto.encryptHard(json, false);
		
		response.setContentType("application/Json");
		response.setContentLength(json.length());
		OutputStream out = response.getOutputStream();
		out.write(json.getBytes());
		out.flush();
		out.close();
	}
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
}
