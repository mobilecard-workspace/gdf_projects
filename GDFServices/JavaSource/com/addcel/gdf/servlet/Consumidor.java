package com.addcel.gdf.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.service.AguaService;
import com.addcel.gdf.service.Dependency;
import com.addcel.gdf.service.InfraccionService;
import com.addcel.gdf.service.LicenciaConductorServices;
import com.addcel.gdf.service.LicenciaPermanenteServices;
import com.addcel.gdf.service.NominaService;
import com.addcel.gdf.service.PredialServices;
import com.addcel.gdf.service.PredialVencidoServices;
import com.addcel.gdf.service.TarjetaCirculacionServices;
import com.addcel.gdf.service.TenenciaServices;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.json.me.JSONException;
import com.addcel.utils.json.me.JSONObject;


public class Consumidor extends HttpServlet {
	private static final Logger log = LoggerFactory.getLogger(Consumidor.class);
	private static final long serialVersionUID = 1L;
	private static final String patron = "ddhhmmss";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Consumidor() {
        super();
    }
    
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		boolean isError = false;
		
		String json = (String) request.getAttribute("json");

		if (json == null) {
			json = (String) request.getParameter("json");
		}

		if (json == null) {
			try {
				json = getFromStream(request);
			} catch (UnsupportedEncodingException ueE) {
				isError = true;
				json = "{\"error\":\"1\",\"numError\":\"UE1001\"}";
			} catch (IOException ioE) {
				isError = true;
				json = "{\"error\":\"1\",\"numError\":\"A1001\"}";
			}
		}
		
		if (json != null) {

			if (!isError) {
				
				int index = 0;
				json = AddcelCrypto.decryptSensitive(json);
				
				try {
					JSONObject jsObject = new JSONObject(json);
					
					if (jsObject.has("toDO")){
						index = Integer.parseInt(jsObject.getString("toDO"));
						
						Dependency dependency = null;
						
						switch (index) {
						case ConstantesGDF.idProd_Nomina:
							dependency = new NominaService();
							break;

						case ConstantesGDF.idProd_Tenencia:
							dependency = new TenenciaServices();
							break;
							
						case ConstantesGDF.idProd_Infraccion:
							dependency = new InfraccionService();
							break;
							
						case ConstantesGDF.idProd_Predial:
							dependency = new PredialServices();
							break;
						
						case ConstantesGDF.idProd_Agua:
							dependency = new AguaService();
							break;
							
						// MLS Predial vencido	
						case ConstantesGDF.idProd_PredialVencido:
							dependency = new PredialVencidoServices();
							break;
							
						case ConstantesGDF.ID_PRODUCTO_TARJETA_CIRCULACION:
							dependency = new TarjetaCirculacionServices();
							break;
							
						case ConstantesGDF.ID_PRODUCTO_LICENCIA_CONDUCTOR:
							dependency = new LicenciaConductorServices();
							break;
						
						case ConstantesGDF.ID_PRODUCTO_LICENCIA_PERMANENTE:
							dependency = new LicenciaPermanenteServices();
							break;
							
						default:
							break;
						}
						
						json = dependency.consumeWS(json);
						
					} else {
						json = "{\"error\":\"1\",\"numError\":\"00000\"}";
						// no existe el parametro
					}
				} catch (JSONException e) {
					json = "{\"error\":\"1\",\"numError\":\"00000\"}";
					log.info("Error: "+e.getMessage());
				}
			}
		} else {
			json = "{\"error\":\"1\",\"numError\":\"A0001\"}";
		}

		if(json != null){
			json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
		}
		
		response.setContentType("application/Json");
		response.setContentLength(json.length());
		OutputStream out = response.getOutputStream();
		out.write(json.getBytes());
		out.flush();
		out.close();
	}

	private String getFromStream(HttpServletRequest request)
			throws UnsupportedEncodingException, IOException {

		String json = null;
		InputStream is = null;

		is = (InputStream) request.getInputStream();

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {

				if (is != null) {
					is.close();
				}
			}
			json = writer.toString();
		}
		return json;
	}
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}