package com.addcel.gdf.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.service.TBitacoraService;
import com.addcel.gdf.vo.TokenVO;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;


public class GetToken extends HttpServlet {
	private static final Logger log = LoggerFactory.getLogger(GetToken.class);
	private static final long serialVersionUID = 1L;
	private static final String patron = "ddhhmmss";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetToken() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		boolean isError = false;
		Gson gson = new Gson();
		TBitacoraService tbitaService = new TBitacoraService();

		
		String json = (String) request.getAttribute("json");

		if (json == null) {
			json = (String) request.getParameter("json");
		}

		if (json == null) {
			try {
				json = getFromStream(request);
			} catch (UnsupportedEncodingException ueE) {
				isError = true;
				json = "{\"error\":\"1\",\"numError\":\"UE1001\"}";
			} catch (IOException ioE) {
				isError = true;
				json = "{\"error\":\"1\",\"numError\":\"A1001\"}";
			}
		}

		if (json != null) {
			if (!isError) {
				TokenVO tokenVO = null;
				try{
					tokenVO = (TokenVO) gson.fromJson(AddcelCrypto.decryptSensitive(json), TokenVO.class);
					if(!"userPrueba".equals(tokenVO.getUsuario()) || !"passwordPrueba".equals(tokenVO.getPassword())){
						json="{\"numError\":2,\"error\":\"No tiene permisos para consumir este servicio.\"}";
					}else{
						json="{\"token\":\""+AddcelCrypto.encryptSensitive(formato.format(new Date()),tbitaService.getFechaActual())+"\",\"numError\":0,\"error\":\"\"}";
					}
				}catch(Exception e){
//					logger.error("Ocurrio un error al generar el Token: {}", e.getMessage());
					json = "{\"numError\":1,\"error\":\"Ocurrio un error al generar el Token.\"}";
				}
			}

		} else {

			json = "{\"error\":\"1\",\"numError\":\"A0001\"}";
		}
		
		log.info("Json Respues: " + json);
		json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json, false);
		
		response.setContentType("application/Json");
		response.setContentLength(json.length());
		OutputStream out = response.getOutputStream();
		out.write(json.getBytes());
		out.flush();
		out.close();
	}

	private String getFromStream(HttpServletRequest request)
			throws UnsupportedEncodingException, IOException {

		String json = null;
		InputStream is = null;

		is = (InputStream) request.getInputStream();

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {

				if (is != null) {
					is.close();
				}
			}
			json = writer.toString();
		}

		return json;
	}
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}

