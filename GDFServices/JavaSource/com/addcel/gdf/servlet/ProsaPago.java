package com.addcel.gdf.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.service.PagoProsaService;
import com.addcel.utils.AddcelCrypto;


public class ProsaPago extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String patron = "ddhhmmss";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	private static final Logger logger = LoggerFactory.getLogger(ProsaPago.class);   
	       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProsaPago() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("Dentro servicio: /ProsaPago");
		boolean isError = false;
		
		String json = (String) request.getAttribute("json");

		if (json == null) {
			json = (String) request.getParameter("json");
		}

		if (json == null) {
			try {
				json = getFromStream(request);
			} catch (UnsupportedEncodingException ueE) {
				isError = true;
				json = "{\"numError\":\"1\",\"error\":\"El parametro json no puede ser null.\"}";
			} catch (IOException ioE) {
				isError = true;
				json = "{\"numError\":\"1\",\"error\":\"El parametro json no puede ser null.\"}";
			}
		}
		
		if (json != null) {

			if (!isError) {
				
				try {
					json = AddcelCrypto.decryptSensitive(json);
					
					logger.debug("Procesando peticion");
//					json = new PagoProsaService().inicioProceso(json);
					json = "{\"numError\":\"2\",\"error\":\"Esta version esta desactuliazada, favor de descargar la ultima version de la aplicación.\"}";
				} catch (Exception e) {
					json = "{\"numError\":\"2\",\"error\":\"Ocurrio un error durante el pago: \"}";
					// no existe el parametro, error al interpretar el json
				}
			}
		} else {

			json = "{\"numError\":\"1\",\"error\":\"El parametro json no puede ser null.\"}";
		}

		if(json != null){
			json = AddcelCrypto.encryptSensitive(formato.format(new Date()), json);
		}
		
		response.setContentType("application/Json");
		response.setContentLength(json.length());
		OutputStream out = response.getOutputStream();
		out.write(json.getBytes());
		out.flush();
		out.close();
	}

	private String getFromStream(HttpServletRequest request)
			throws UnsupportedEncodingException, IOException {

		String json = null;
		InputStream is = null;

		is = (InputStream) request.getInputStream();

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {

				if (is != null) {
					is.close();
				}
			}
			json = writer.toString();
		}

		return json;
	}
    
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
