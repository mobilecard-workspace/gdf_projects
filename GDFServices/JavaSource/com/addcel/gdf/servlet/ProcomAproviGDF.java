package com.addcel.gdf.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.service.ProcomAproviGDFService;
import com.addcel.gdf.vo.bitacoras.TransactionProcomVO;


/**
 * Servlet implementation class ComercioConGDF
 */
public class ProcomAproviGDF extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(ProcomAproviGDF.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProcomAproviGDF() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	logger.info("Inicio proseso ProcomAproviGDF.");
    	ProcomAproviGDFService procomAproviService = new ProcomAproviGDFService();
    	TransactionProcomVO transactionProcomVO = null;
    	try{
	    	transactionProcomVO = procomAproviService.getProcomRequest(request);
	    	procomAproviService.processProcom(transactionProcomVO, request, response);

    	}catch(Exception e){
    		request.setAttribute("descError", "Error general: " + e.getMessage());
    		request.getRequestDispatcher("/procom/error.jsp").forward(request, response);;
    	}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
		
}
