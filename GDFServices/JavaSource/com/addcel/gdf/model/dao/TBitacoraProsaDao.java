/**
 * 
 */
package com.addcel.gdf.model.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;

public class TBitacoraProsaDao extends SqlMapClientDaoSupport {
	private static final Logger log = LoggerFactory.getLogger(TBitacoraProsaDao.class);
	
	public int insertTBitacoraProsa(TBitacoraProsaVO bitacora) {
		Integer idBitacora = 0; 
		
		//log.info("Datos de la T_Bitacora: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().insert("insertTBitacoraProsa", bitacora);
		log.info("Id_T_Bitacora: " + idBitacora);
		
		return idBitacora;
	}
	
	public int updateTBitacoraProsa(TBitacoraProsaVO bitacora) {
		Integer idBitacora = 0; 
		
		//log.info("Datos de la T_Bitacora: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().update("updateTBitacoraProsa", bitacora);

		return idBitacora;
	}
	
}
