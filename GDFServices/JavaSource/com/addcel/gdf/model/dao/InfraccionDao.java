/**
 * 
 */
package com.addcel.gdf.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.infraccion.InfraccionRespuestaVO;

public class InfraccionDao extends SqlMapClientDaoSupport {
	private static final Logger log = LoggerFactory.getLogger(InfraccionDao.class);
	
	public void insertGDFInfraccion(InfraccionRespuestaVO bitacora) {
		log.info("Datos Insert del Objeto Infraccion: " + bitacora);
		getSqlMapClientTemplate().insert("insertGDFInfraccion", bitacora);
		log.info("Exito en el insert Detalle Infraccion, IDBitacora: " + bitacora.getId_bitacora());
	}
	
	public int updateGDFInfraccion(InfraccionRespuestaVO bitacora) {
		Integer idInfraccion = 0; 
		
		log.info("Datos Update del Objeto  Infraccion: " + bitacora);
		idInfraccion = (Integer )getSqlMapClientTemplate().update("updateGDFInfraccion", bitacora);
		log.info("Exito en el update Detalle Infracciona, IDBitacora: " + bitacora.getId_bitacora());
		return idInfraccion;
	}
		
	public List<InfraccionRespuestaVO> selectGDFInfraccion(HashMap<String, String>  consultaInfraccion){
		List<InfraccionRespuestaVO> listaInfraccion = new ArrayList<InfraccionRespuestaVO>();
		try {
			log.debug("Consulta Bitacora Detalle Infracciona: " + consultaInfraccion);
			listaInfraccion = getSqlMapClientTemplate().queryForList("selectGDFInfraccion", consultaInfraccion);
			log.debug("Bitacora Detalle Infraccion lista tamaño: " + listaInfraccion.size());
		} catch (Exception e) {
			log.error("Ocurrio un error durante la busqueda de Infraaciones DAO", e);
		}
		return listaInfraccion;
	}

}
