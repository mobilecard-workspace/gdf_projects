/**
 * 
 */
package com.addcel.gdf.model.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.bitacoras.TransactionProcomVO;

public class TransactionProcomDao extends SqlMapClientDaoSupport {
	private static final Logger log = LoggerFactory.getLogger(TransactionProcomDao.class);
	
	public int insertTransactionProcom(TransactionProcomVO transactionProcom) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la TransactionProcomVO: " + transactionProcom);
		idBitacora = (Integer )getSqlMapClientTemplate().insert("insertTransactionProcom", transactionProcom);
		log.info("IdTransactionProcom: " + idBitacora);
		
		return idBitacora;
	}
	
	
}
