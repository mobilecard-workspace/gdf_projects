/**
 * 
 */
package com.addcel.gdf.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.agua.AguaRespuestaGuardaVO;

public class AguaDao extends SqlMapClientDaoSupport {
	private static final Logger log = LoggerFactory.getLogger(AguaDao.class);
	
	public void insertGDFAgua(AguaRespuestaGuardaVO bitacora) {
		log.info("Datos Insert del Objeto Agua: " + bitacora);
		getSqlMapClientTemplate().insert("insertGDFAgua", bitacora);
		log.info("Exito en el insert Detalle Agua, IDBitacora: " + bitacora.getId_bitacora());
	}
	
	public int updateGDFAgua(AguaRespuestaGuardaVO bitacora) {
		Integer idAgua = 0; 
		
		log.info("Datos Update del Objeto  Agua: " + bitacora);
		idAgua = (Integer )getSqlMapClientTemplate().update("updateGDFAgua", bitacora);
		log.info("Exito en el update Detalle Agua, IDBitacora: " + bitacora.getId_bitacora());
		return idAgua;
	}
		
	public List<AguaRespuestaGuardaVO> selectGDFAgua(HashMap<String, String>  consultaAgua){
		List<AguaRespuestaGuardaVO> listaAgua = new ArrayList<AguaRespuestaGuardaVO>();
		try {
			log.debug("Consulta Bitacora Detalle Agua: " + consultaAgua);
			listaAgua = getSqlMapClientTemplate().queryForList("selectGDFAgua", consultaAgua);
			log.debug("Bitacora Detalle Agua lista tamaño: " + listaAgua.size());
		} catch (Exception e) {
			log.error("Ocurrio un error durante la obtencion de los comunicados", e);
		}
		return listaAgua;
	}

}
