package com.addcel.gdf.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.tenencia.TenenciaRespuestaVO;

public class TenenciaDao  extends SqlMapClientDaoSupport {
	private static final Logger log = LoggerFactory.getLogger(TenenciaDao.class);
	
	public void insertGDFTenencia(TenenciaRespuestaVO bitacora) {
//		log.info("Datos Insert del Objeto Tenencia: " + bitacora);
		getSqlMapClientTemplate().insert("insertGDFTenencia", bitacora);
		log.info("Exito en el insert Detalle Tenencia, IDBitacora: " + bitacora.getId_bitacora());
	}
	
	public int updateGDFTenencia(TenenciaRespuestaVO bitacora) {
		Integer idTenencia = 0; 
		
//		log.info("Datos Update del Objeto  Tenencia: " + bitacora);
		idTenencia = (Integer )getSqlMapClientTemplate().update("updateGDFTenencia", bitacora);
		log.info("Exito en el update Detalle Tenenciaa, IDBitacora: " + bitacora.getId_bitacora());
		return idTenencia;
	}
		
	public List<TenenciaRespuestaVO> selectGDFTenencia(HashMap<String, String>  consultaTenencia){
		List<TenenciaRespuestaVO> listaTenencia = new ArrayList<TenenciaRespuestaVO>();
		try {
//			log.debug("Consulta Bitacora Detalle Tenenciaa: " + consultaTenencia);
			listaTenencia = getSqlMapClientTemplate().queryForList("selectGDFTenencia", consultaTenencia);
			log.debug("Bitacora Detalle Tenencia lista tamaño: " + listaTenencia.size());
		} catch (Exception e) {
			log.error("Ocurrio un error durante la busqueda de Infraaciones DAO", e);
		}
		return listaTenencia;
	}
	
}
