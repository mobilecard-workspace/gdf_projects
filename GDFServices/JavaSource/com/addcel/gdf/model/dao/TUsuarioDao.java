package com.addcel.gdf.model.dao;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.TProveedorVO;
import com.addcel.gdf.vo.UsuarioVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;

public class TUsuarioDao extends SqlMapClientDaoSupport{
		
	private static final Logger log = LoggerFactory.getLogger(TUsuarioDao.class);
	
	public UsuarioVO selectUsuario(String id){
		UsuarioVO usuario=null;
		try{
			usuario=(UsuarioVO) getSqlMapClientTemplate().queryForObject("selectUsuario", id);
		}catch(Exception e){
			log.error("Ocurrio un error al obtener el usuario: ", e);
		}
		return usuario;
	}
	
	public TBitacoraVO selectTbitacora(String idBitacora){
		TBitacoraVO tbitacora=null;
		try{
			tbitacora = (TBitacoraVO) getSqlMapClientTemplate().queryForObject("selectDataTBitacora", idBitacora);
		}catch(Exception e){
			log.error("Ocurrio un error al obtener el usuario: ", e);
		}
		return tbitacora;
	}
	
	public TProveedorVO getTProveedor(String id){
		TProveedorVO tproveedorVO = null;
		try{
			tproveedorVO =(TProveedorVO) getSqlMapClientTemplate().queryForObject("selectTProveedor", id);
		}catch(Exception e){
			log.error("Ocurrio un error al obtener el usuario: {}", e);
		}
		return tproveedorVO;
	}
	//MLS Método para obtener topes de pago por producto
	public String getMontoMaxProducto(Integer idProducto){
		String result = null;
		try{
			result = (String) getSqlMapClientTemplate().queryForObject("selectTMontoMAxProducto", idProducto);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener el usuario: {}", e);
		}
		return result;		
	}
	
	public Integer selectTParamInt(String param){
		Integer result = null;
		try{
			result = (Integer) getSqlMapClientTemplate().queryForObject("selectTParamInt", param);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener el usuario: {}", e);
		}
		return result;		
	}
	
	public Map<String, Object> selectRuleUsuario(Long idUsuario){
		Map<String, Object> result = null;
		try{
			result = (Map) getSqlMapClientTemplate().queryForObject("selectRuleUsuario", idUsuario);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener selectRuleUsuario: {}", e);
		}
		return result;		
	}
	
	public Map<String, Object> selectRuleTarjeta(String tarjeta){
		Map<String, Object> result = null;
		try{
			result = (Map) getSqlMapClientTemplate().queryForObject("selectRuleTarjeta", tarjeta);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener selectRuleTarjeta: {}", e);
		}
		return result;		
	}
	
	public Map<String, Object> selectRuleAgua(Long idUsuario){
		Map<String, Object> result = null;
		try{
			result = (Map) getSqlMapClientTemplate().queryForObject("selectRuleAgua", idUsuario);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener selectRuleUsuario: {}", e);
		}
		return result;		
	}
	
	public Map<String, Object> selectRuleLicCon(Long idUsuario){
		Map<String, Object> result = null;
		try{
			result = (Map) getSqlMapClientTemplate().queryForObject("selectRuleLicCon", idUsuario);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener selectRuleUsuario: {}", e);
		}
		return result;		
	}
	
	public Map<String, Object> selectRuleLicPer(Long idUsuario){
		Map<String, Object> result = null;
		try{
			result = (Map) getSqlMapClientTemplate().queryForObject("selectRuleLicPer", idUsuario);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener selectRuleUsuario: {}", e);
		}
		return result;		
	}
	
	public Map<String, Object> selectRuleInfraccion(Long idUsuario){
		Map<String, Object> result = null;
		try{
			result = (Map) getSqlMapClientTemplate().queryForObject("selectRuleInfraccion", idUsuario);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener selectRuleUsuario: {}", e);
		}
		return result;		
	}
	
	public Map<String, Object> selectRuleNomina(Long idUsuario){
		Map<String, Object> result = null;
		try{
			result = (Map) getSqlMapClientTemplate().queryForObject("selectRuleNomina", idUsuario);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener selectRuleUsuario: {}", e);
		}
		return result;		
	}
	
	public Map<String, Object> selectRulePredial(Long idUsuario){
		Map<String, Object> result = null;
		try{
			result = (Map) getSqlMapClientTemplate().queryForObject("selectRulePredial", idUsuario);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener selectRuleUsuario: {}", e);
		}
		return result;		
	}
	
	public Map<String, Object> selectRulePredialVen(Long idUsuario){
		Map<String, Object> result = null;
		try{
			result = (Map) getSqlMapClientTemplate().queryForObject("selectRulePredialVen", idUsuario);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener selectRuleUsuario: {}", e);
		}
		return result;		
	}
	
	public Map<String, Object> selectRuleTarCir(Long idUsuario){
		Map<String, Object> result = null;
		try{
			result = (Map) getSqlMapClientTemplate().queryForObject("selectRuleTarCir", idUsuario);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener selectRuleUsuario: {}", e);
		}
		return result;		
	}
	
	public Map<String, Object> selectRuleTenencia(Long idUsuario){
		Map<String, Object> result = null;
		try{
			result = (Map) getSqlMapClientTemplate().queryForObject("selectRuleTenencia", idUsuario);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener selectRuleUsuario: {}", e);
		}
		return result;		
	}

}
