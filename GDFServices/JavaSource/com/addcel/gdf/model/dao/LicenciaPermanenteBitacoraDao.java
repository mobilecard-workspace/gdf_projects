package com.addcel.gdf.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.licencia.LicenciaConductorResponseVO;

public class LicenciaPermanenteBitacoraDao extends SqlMapClientDaoSupport{

	private static final Logger log = LoggerFactory.getLogger(LicenciaPermanenteBitacoraDao.class);

	public void insertGDFLicenciaPermanente(LicenciaConductorResponseVO bitacora) {
		log.info("Datos Insert del Objeto Licencia Permanente: " + bitacora);
		getSqlMapClientTemplate().insert("insertGDFLicenciaPermanente", bitacora);
		log.info("Exito en el insert Detalle Licencia Permanente, IDBitacora: " + bitacora.getId_bitacora());
	}

	public void updateGDFLicenciaPermanente(LicenciaConductorResponseVO bitacora) {
		log.info("Datos Update del Objeto Licencia Permanente: " + bitacora);
		getSqlMapClientTemplate().update("updateGDFLicenciaPermanente", bitacora);
		log.info("Exito en el update Detalle Licencia Permanente, IDBitacora: " + bitacora.getId_bitacora());
	}

	@SuppressWarnings("unchecked")
	public List<LicenciaConductorResponseVO> selectGDFLicenciaPermanente(
			HashMap<String, String> consulta) {
		List<LicenciaConductorResponseVO> listaTarjetaCirculacion = new ArrayList<LicenciaConductorResponseVO>();
		try {
			log.debug("Consulta Bitacora Detalle Licencia Permanente: " + consulta);
			listaTarjetaCirculacion = getSqlMapClientTemplate().queryForList("selectGDFLicenciaPermanente", consulta);
			log.debug("Bitacora Detalle Tarejta Circulacion lista tamaño: " + listaTarjetaCirculacion.size());
		} catch (Exception e) {
			log.error("Ocurrio un error durante la busqueda de Licencia Permanente DAO", e);
		}
		return listaTarjetaCirculacion;
	}
	
}