/**
 * 
 */
package com.addcel.gdf.model.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.terminos.TerminosCondicionesVO;

public class TerminosCondicionesDao extends SqlMapClientDaoSupport {
	private static final Logger log = LoggerFactory.getLogger(TerminosCondicionesDao.class);
	
	public void insertTerminosXUsuario(TerminosCondicionesVO termVO) {
		log.info("Datos Insert del Objeto TerminosCondicionesVO: " + termVO);
		getSqlMapClientTemplate().insert("insertTerminosXUsuario", termVO);
		log.info("Exito en el insert Terminos X Usuario, IDUsuario: " + termVO.getId_usuario());
	}
	
	public List<TerminosCondicionesVO> selectTerminosXUsuario(TerminosCondicionesVO termVO) {
		List<TerminosCondicionesVO> listaTerminos = new ArrayList<TerminosCondicionesVO>();
		
		log.info("Consulta Terminos X Aplicacion, id_aplicacion: " + termVO.getId_aplicacion());
		log.info("id_aplicacion: " + termVO.getId_aplicacion());
		log.info("id_usuario: " + termVO.getId_usuario());
		listaTerminos = getSqlMapClientTemplate().queryForList("selectTerminosXUsuario", termVO);
		log.info("selectTerminosXUsuario lista tamaño: " + listaTerminos.size());
		return listaTerminos;
	}
		
	public List<TerminosCondicionesVO> selectTerminosXAplicacion(TerminosCondicionesVO termVO){
		List<TerminosCondicionesVO> listaTerminos = new ArrayList<TerminosCondicionesVO>();
		try {
			log.info("Consulta Terminos X Aplicacion, id_aplicacion: " + termVO.getId_aplicacion());
			listaTerminos = getSqlMapClientTemplate().queryForList("selectTerminosXAplicacion", termVO);
			log.info("TerminosXAplicacion lista tamaño: " + listaTerminos.size());
		} catch (Exception e) {
			log.error("Ocurrio un error durante la obtencion de los TerminosXAplicacion", e);
		}
		return listaTerminos;
	}
	
	

}
