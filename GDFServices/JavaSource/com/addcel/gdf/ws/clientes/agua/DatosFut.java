/**
 * DatosFut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

public class DatosFut  implements java.io.Serializable {
    private java.lang.String vanio;

    private java.lang.String vbimestre;

    private java.lang.String vderdom;

    private java.lang.String vderndom;

    private java.lang.String vuso;

    private java.lang.String vestatusemi;

    private java.lang.String vderpagado;

    private java.lang.String vderdife;

    private java.lang.String vfolioreq;

    private java.lang.String vtiporeq;

    private java.lang.String vact;

    private java.lang.String viva;

    private java.lang.String vrecargos;

    private java.lang.String vmultas;

    private java.lang.String vtotal;

    private java.lang.String vrecargosc;

    private java.lang.String vmultasc;

    private java.lang.String vfechavence;

    private java.lang.String consubsidio;

    private java.lang.String subsidio;

    private java.lang.String dessubsidio;

    private java.lang.String descrecd;

    private java.lang.String descrecnd;

    private java.lang.String derechoss;

    private java.lang.String bloqueado;

    public DatosFut() {
    }

    public DatosFut(
           java.lang.String vanio,
           java.lang.String vbimestre,
           java.lang.String vderdom,
           java.lang.String vderndom,
           java.lang.String vuso,
           java.lang.String vestatusemi,
           java.lang.String vderpagado,
           java.lang.String vderdife,
           java.lang.String vfolioreq,
           java.lang.String vtiporeq,
           java.lang.String vact,
           java.lang.String viva,
           java.lang.String vrecargos,
           java.lang.String vmultas,
           java.lang.String vtotal,
           java.lang.String vrecargosc,
           java.lang.String vmultasc,
           java.lang.String vfechavence,
           java.lang.String consubsidio,
           java.lang.String subsidio,
           java.lang.String dessubsidio,
           java.lang.String descrecd,
           java.lang.String descrecnd,
           java.lang.String derechoss,
           java.lang.String bloqueado) {
           this.vanio = vanio;
           this.vbimestre = vbimestre;
           this.vderdom = vderdom;
           this.vderndom = vderndom;
           this.vuso = vuso;
           this.vestatusemi = vestatusemi;
           this.vderpagado = vderpagado;
           this.vderdife = vderdife;
           this.vfolioreq = vfolioreq;
           this.vtiporeq = vtiporeq;
           this.vact = vact;
           this.viva = viva;
           this.vrecargos = vrecargos;
           this.vmultas = vmultas;
           this.vtotal = vtotal;
           this.vrecargosc = vrecargosc;
           this.vmultasc = vmultasc;
           this.vfechavence = vfechavence;
           this.consubsidio = consubsidio;
           this.subsidio = subsidio;
           this.dessubsidio = dessubsidio;
           this.descrecd = descrecd;
           this.descrecnd = descrecnd;
           this.derechoss = derechoss;
           this.bloqueado = bloqueado;
    }


    /**
     * Gets the vanio value for this DatosFut.
     * 
     * @return vanio
     */
    public java.lang.String getVanio() {
        return vanio;
    }


    /**
     * Sets the vanio value for this DatosFut.
     * 
     * @param vanio
     */
    public void setVanio(java.lang.String vanio) {
        this.vanio = vanio;
    }


    /**
     * Gets the vbimestre value for this DatosFut.
     * 
     * @return vbimestre
     */
    public java.lang.String getVbimestre() {
        return vbimestre;
    }


    /**
     * Sets the vbimestre value for this DatosFut.
     * 
     * @param vbimestre
     */
    public void setVbimestre(java.lang.String vbimestre) {
        this.vbimestre = vbimestre;
    }


    /**
     * Gets the vderdom value for this DatosFut.
     * 
     * @return vderdom
     */
    public java.lang.String getVderdom() {
        return vderdom;
    }


    /**
     * Sets the vderdom value for this DatosFut.
     * 
     * @param vderdom
     */
    public void setVderdom(java.lang.String vderdom) {
        this.vderdom = vderdom;
    }


    /**
     * Gets the vderndom value for this DatosFut.
     * 
     * @return vderndom
     */
    public java.lang.String getVderndom() {
        return vderndom;
    }


    /**
     * Sets the vderndom value for this DatosFut.
     * 
     * @param vderndom
     */
    public void setVderndom(java.lang.String vderndom) {
        this.vderndom = vderndom;
    }


    /**
     * Gets the vuso value for this DatosFut.
     * 
     * @return vuso
     */
    public java.lang.String getVuso() {
        return vuso;
    }


    /**
     * Sets the vuso value for this DatosFut.
     * 
     * @param vuso
     */
    public void setVuso(java.lang.String vuso) {
        this.vuso = vuso;
    }


    /**
     * Gets the vestatusemi value for this DatosFut.
     * 
     * @return vestatusemi
     */
    public java.lang.String getVestatusemi() {
        return vestatusemi;
    }


    /**
     * Sets the vestatusemi value for this DatosFut.
     * 
     * @param vestatusemi
     */
    public void setVestatusemi(java.lang.String vestatusemi) {
        this.vestatusemi = vestatusemi;
    }


    /**
     * Gets the vderpagado value for this DatosFut.
     * 
     * @return vderpagado
     */
    public java.lang.String getVderpagado() {
        return vderpagado;
    }


    /**
     * Sets the vderpagado value for this DatosFut.
     * 
     * @param vderpagado
     */
    public void setVderpagado(java.lang.String vderpagado) {
        this.vderpagado = vderpagado;
    }


    /**
     * Gets the vderdife value for this DatosFut.
     * 
     * @return vderdife
     */
    public java.lang.String getVderdife() {
        return vderdife;
    }


    /**
     * Sets the vderdife value for this DatosFut.
     * 
     * @param vderdife
     */
    public void setVderdife(java.lang.String vderdife) {
        this.vderdife = vderdife;
    }


    /**
     * Gets the vfolioreq value for this DatosFut.
     * 
     * @return vfolioreq
     */
    public java.lang.String getVfolioreq() {
        return vfolioreq;
    }


    /**
     * Sets the vfolioreq value for this DatosFut.
     * 
     * @param vfolioreq
     */
    public void setVfolioreq(java.lang.String vfolioreq) {
        this.vfolioreq = vfolioreq;
    }


    /**
     * Gets the vtiporeq value for this DatosFut.
     * 
     * @return vtiporeq
     */
    public java.lang.String getVtiporeq() {
        return vtiporeq;
    }


    /**
     * Sets the vtiporeq value for this DatosFut.
     * 
     * @param vtiporeq
     */
    public void setVtiporeq(java.lang.String vtiporeq) {
        this.vtiporeq = vtiporeq;
    }


    /**
     * Gets the vact value for this DatosFut.
     * 
     * @return vact
     */
    public java.lang.String getVact() {
        return vact;
    }


    /**
     * Sets the vact value for this DatosFut.
     * 
     * @param vact
     */
    public void setVact(java.lang.String vact) {
        this.vact = vact;
    }


    /**
     * Gets the viva value for this DatosFut.
     * 
     * @return viva
     */
    public java.lang.String getViva() {
        return viva;
    }


    /**
     * Sets the viva value for this DatosFut.
     * 
     * @param viva
     */
    public void setViva(java.lang.String viva) {
        this.viva = viva;
    }


    /**
     * Gets the vrecargos value for this DatosFut.
     * 
     * @return vrecargos
     */
    public java.lang.String getVrecargos() {
        return vrecargos;
    }


    /**
     * Sets the vrecargos value for this DatosFut.
     * 
     * @param vrecargos
     */
    public void setVrecargos(java.lang.String vrecargos) {
        this.vrecargos = vrecargos;
    }


    /**
     * Gets the vmultas value for this DatosFut.
     * 
     * @return vmultas
     */
    public java.lang.String getVmultas() {
        return vmultas;
    }


    /**
     * Sets the vmultas value for this DatosFut.
     * 
     * @param vmultas
     */
    public void setVmultas(java.lang.String vmultas) {
        this.vmultas = vmultas;
    }


    /**
     * Gets the vtotal value for this DatosFut.
     * 
     * @return vtotal
     */
    public java.lang.String getVtotal() {
        return vtotal;
    }


    /**
     * Sets the vtotal value for this DatosFut.
     * 
     * @param vtotal
     */
    public void setVtotal(java.lang.String vtotal) {
        this.vtotal = vtotal;
    }


    /**
     * Gets the vrecargosc value for this DatosFut.
     * 
     * @return vrecargosc
     */
    public java.lang.String getVrecargosc() {
        return vrecargosc;
    }


    /**
     * Sets the vrecargosc value for this DatosFut.
     * 
     * @param vrecargosc
     */
    public void setVrecargosc(java.lang.String vrecargosc) {
        this.vrecargosc = vrecargosc;
    }


    /**
     * Gets the vmultasc value for this DatosFut.
     * 
     * @return vmultasc
     */
    public java.lang.String getVmultasc() {
        return vmultasc;
    }


    /**
     * Sets the vmultasc value for this DatosFut.
     * 
     * @param vmultasc
     */
    public void setVmultasc(java.lang.String vmultasc) {
        this.vmultasc = vmultasc;
    }


    /**
     * Gets the vfechavence value for this DatosFut.
     * 
     * @return vfechavence
     */
    public java.lang.String getVfechavence() {
        return vfechavence;
    }


    /**
     * Sets the vfechavence value for this DatosFut.
     * 
     * @param vfechavence
     */
    public void setVfechavence(java.lang.String vfechavence) {
        this.vfechavence = vfechavence;
    }


    /**
     * Gets the consubsidio value for this DatosFut.
     * 
     * @return consubsidio
     */
    public java.lang.String getConsubsidio() {
        return consubsidio;
    }


    /**
     * Sets the consubsidio value for this DatosFut.
     * 
     * @param consubsidio
     */
    public void setConsubsidio(java.lang.String consubsidio) {
        this.consubsidio = consubsidio;
    }


    /**
     * Gets the subsidio value for this DatosFut.
     * 
     * @return subsidio
     */
    public java.lang.String getSubsidio() {
        return subsidio;
    }


    /**
     * Sets the subsidio value for this DatosFut.
     * 
     * @param subsidio
     */
    public void setSubsidio(java.lang.String subsidio) {
        this.subsidio = subsidio;
    }


    /**
     * Gets the dessubsidio value for this DatosFut.
     * 
     * @return dessubsidio
     */
    public java.lang.String getDessubsidio() {
        return dessubsidio;
    }


    /**
     * Sets the dessubsidio value for this DatosFut.
     * 
     * @param dessubsidio
     */
    public void setDessubsidio(java.lang.String dessubsidio) {
        this.dessubsidio = dessubsidio;
    }


    /**
     * Gets the descrecd value for this DatosFut.
     * 
     * @return descrecd
     */
    public java.lang.String getDescrecd() {
        return descrecd;
    }


    /**
     * Sets the descrecd value for this DatosFut.
     * 
     * @param descrecd
     */
    public void setDescrecd(java.lang.String descrecd) {
        this.descrecd = descrecd;
    }


    /**
     * Gets the descrecnd value for this DatosFut.
     * 
     * @return descrecnd
     */
    public java.lang.String getDescrecnd() {
        return descrecnd;
    }


    /**
     * Sets the descrecnd value for this DatosFut.
     * 
     * @param descrecnd
     */
    public void setDescrecnd(java.lang.String descrecnd) {
        this.descrecnd = descrecnd;
    }


    /**
     * Gets the derechoss value for this DatosFut.
     * 
     * @return derechoss
     */
    public java.lang.String getDerechoss() {
        return derechoss;
    }


    /**
     * Sets the derechoss value for this DatosFut.
     * 
     * @param derechoss
     */
    public void setDerechoss(java.lang.String derechoss) {
        this.derechoss = derechoss;
    }


    /**
     * Gets the bloqueado value for this DatosFut.
     * 
     * @return bloqueado
     */
    public java.lang.String getBloqueado() {
        return bloqueado;
    }


    /**
     * Sets the bloqueado value for this DatosFut.
     * 
     * @param bloqueado
     */
    public void setBloqueado(java.lang.String bloqueado) {
        this.bloqueado = bloqueado;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DatosFut)) return false;
        DatosFut other = (DatosFut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.vanio==null && other.getVanio()==null) || 
             (this.vanio!=null &&
              this.vanio.equals(other.getVanio()))) &&
            ((this.vbimestre==null && other.getVbimestre()==null) || 
             (this.vbimestre!=null &&
              this.vbimestre.equals(other.getVbimestre()))) &&
            ((this.vderdom==null && other.getVderdom()==null) || 
             (this.vderdom!=null &&
              this.vderdom.equals(other.getVderdom()))) &&
            ((this.vderndom==null && other.getVderndom()==null) || 
             (this.vderndom!=null &&
              this.vderndom.equals(other.getVderndom()))) &&
            ((this.vuso==null && other.getVuso()==null) || 
             (this.vuso!=null &&
              this.vuso.equals(other.getVuso()))) &&
            ((this.vestatusemi==null && other.getVestatusemi()==null) || 
             (this.vestatusemi!=null &&
              this.vestatusemi.equals(other.getVestatusemi()))) &&
            ((this.vderpagado==null && other.getVderpagado()==null) || 
             (this.vderpagado!=null &&
              this.vderpagado.equals(other.getVderpagado()))) &&
            ((this.vderdife==null && other.getVderdife()==null) || 
             (this.vderdife!=null &&
              this.vderdife.equals(other.getVderdife()))) &&
            ((this.vfolioreq==null && other.getVfolioreq()==null) || 
             (this.vfolioreq!=null &&
              this.vfolioreq.equals(other.getVfolioreq()))) &&
            ((this.vtiporeq==null && other.getVtiporeq()==null) || 
             (this.vtiporeq!=null &&
              this.vtiporeq.equals(other.getVtiporeq()))) &&
            ((this.vact==null && other.getVact()==null) || 
             (this.vact!=null &&
              this.vact.equals(other.getVact()))) &&
            ((this.viva==null && other.getViva()==null) || 
             (this.viva!=null &&
              this.viva.equals(other.getViva()))) &&
            ((this.vrecargos==null && other.getVrecargos()==null) || 
             (this.vrecargos!=null &&
              this.vrecargos.equals(other.getVrecargos()))) &&
            ((this.vmultas==null && other.getVmultas()==null) || 
             (this.vmultas!=null &&
              this.vmultas.equals(other.getVmultas()))) &&
            ((this.vtotal==null && other.getVtotal()==null) || 
             (this.vtotal!=null &&
              this.vtotal.equals(other.getVtotal()))) &&
            ((this.vrecargosc==null && other.getVrecargosc()==null) || 
             (this.vrecargosc!=null &&
              this.vrecargosc.equals(other.getVrecargosc()))) &&
            ((this.vmultasc==null && other.getVmultasc()==null) || 
             (this.vmultasc!=null &&
              this.vmultasc.equals(other.getVmultasc()))) &&
            ((this.vfechavence==null && other.getVfechavence()==null) || 
             (this.vfechavence!=null &&
              this.vfechavence.equals(other.getVfechavence()))) &&
            ((this.consubsidio==null && other.getConsubsidio()==null) || 
             (this.consubsidio!=null &&
              this.consubsidio.equals(other.getConsubsidio()))) &&
            ((this.subsidio==null && other.getSubsidio()==null) || 
             (this.subsidio!=null &&
              this.subsidio.equals(other.getSubsidio()))) &&
            ((this.dessubsidio==null && other.getDessubsidio()==null) || 
             (this.dessubsidio!=null &&
              this.dessubsidio.equals(other.getDessubsidio()))) &&
            ((this.descrecd==null && other.getDescrecd()==null) || 
             (this.descrecd!=null &&
              this.descrecd.equals(other.getDescrecd()))) &&
            ((this.descrecnd==null && other.getDescrecnd()==null) || 
             (this.descrecnd!=null &&
              this.descrecnd.equals(other.getDescrecnd()))) &&
            ((this.derechoss==null && other.getDerechoss()==null) || 
             (this.derechoss!=null &&
              this.derechoss.equals(other.getDerechoss()))) &&
            ((this.bloqueado==null && other.getBloqueado()==null) || 
             (this.bloqueado!=null &&
              this.bloqueado.equals(other.getBloqueado())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVanio() != null) {
            _hashCode += getVanio().hashCode();
        }
        if (getVbimestre() != null) {
            _hashCode += getVbimestre().hashCode();
        }
        if (getVderdom() != null) {
            _hashCode += getVderdom().hashCode();
        }
        if (getVderndom() != null) {
            _hashCode += getVderndom().hashCode();
        }
        if (getVuso() != null) {
            _hashCode += getVuso().hashCode();
        }
        if (getVestatusemi() != null) {
            _hashCode += getVestatusemi().hashCode();
        }
        if (getVderpagado() != null) {
            _hashCode += getVderpagado().hashCode();
        }
        if (getVderdife() != null) {
            _hashCode += getVderdife().hashCode();
        }
        if (getVfolioreq() != null) {
            _hashCode += getVfolioreq().hashCode();
        }
        if (getVtiporeq() != null) {
            _hashCode += getVtiporeq().hashCode();
        }
        if (getVact() != null) {
            _hashCode += getVact().hashCode();
        }
        if (getViva() != null) {
            _hashCode += getViva().hashCode();
        }
        if (getVrecargos() != null) {
            _hashCode += getVrecargos().hashCode();
        }
        if (getVmultas() != null) {
            _hashCode += getVmultas().hashCode();
        }
        if (getVtotal() != null) {
            _hashCode += getVtotal().hashCode();
        }
        if (getVrecargosc() != null) {
            _hashCode += getVrecargosc().hashCode();
        }
        if (getVmultasc() != null) {
            _hashCode += getVmultasc().hashCode();
        }
        if (getVfechavence() != null) {
            _hashCode += getVfechavence().hashCode();
        }
        if (getConsubsidio() != null) {
            _hashCode += getConsubsidio().hashCode();
        }
        if (getSubsidio() != null) {
            _hashCode += getSubsidio().hashCode();
        }
        if (getDessubsidio() != null) {
            _hashCode += getDessubsidio().hashCode();
        }
        if (getDescrecd() != null) {
            _hashCode += getDescrecd().hashCode();
        }
        if (getDescrecnd() != null) {
            _hashCode += getDescrecnd().hashCode();
        }
        if (getDerechoss() != null) {
            _hashCode += getDerechoss().hashCode();
        }
        if (getBloqueado() != null) {
            _hashCode += getBloqueado().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DatosFut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "DatosFut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vanio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vanio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vbimestre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vbimestre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vderdom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vderdom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vderndom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vderndom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vuso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vuso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vestatusemi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vestatusemi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vderpagado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vderpagado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vderdife");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vderdife"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vfolioreq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vfolioreq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vtiporeq");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vtiporeq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vact");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vact"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viva");
        elemField.setXmlName(new javax.xml.namespace.QName("", "viva"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vrecargos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vrecargos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vmultas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vmultas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vtotal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vtotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vrecargosc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vrecargosc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vmultasc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vmultasc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vfechavence");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vfechavence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consubsidio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "consubsidio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subsidio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subsidio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dessubsidio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dessubsidio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrecd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descrecd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrecnd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descrecnd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("derechoss");
        elemField.setXmlName(new javax.xml.namespace.QName("", "derechoss"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bloqueado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bloqueado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
