package com.addcel.gdf.ws.clientes.agua;

import java.rmi.RemoteException;

public class FutWSDLPortTypeProxy implements FutWSDLPortType {
  private String _endpoint = null;
  private FutWSDLPortType futWSDLPortType = null;
  
  public FutWSDLPortTypeProxy() {
    _initFutWSDLPortTypeProxy();
  }
  
  public FutWSDLPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initFutWSDLPortTypeProxy();
  }
  
  private void _initFutWSDLPortTypeProxy() {
    try {
      futWSDLPortType = (new FutWSDLLocator()).getfutWSDLPort();
      if (futWSDLPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)futWSDLPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)futWSDLPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (futWSDLPortType != null)
      ((javax.xml.rpc.Stub)futWSDLPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public FutWSDLPortType getFutWSDLPortType() {
    if (futWSDLPortType == null)
      _initFutWSDLPortTypeProxy();
    return futWSDLPortType;
  }

@Override
public OutputFolio obtenerFolioControl(InputDatosConsulta input)
		throws RemoteException {
    if (futWSDLPortType == null)
        _initFutWSDLPortTypeProxy();
      return futWSDLPortType.obtenerFolioControl(input);
}

@Override
public OutputDatosFut obtenerFut(InputDatosConsulta input)
		throws RemoteException {
    if (futWSDLPortType == null)
        _initFutWSDLPortTypeProxy();
      return futWSDLPortType.obtenerFut(input);
}

@Override
public OutputDatosFut obtenerBimestres(InputDatosConsulta input)
		throws RemoteException {
    if (futWSDLPortType == null)
        _initFutWSDLPortTypeProxy();
      return futWSDLPortType.obtenerBimestres(input);
}

@Override
public OutputGastosEjecucion obtenerGastosEjecucion(InputVoid input)
		throws RemoteException {
    if (futWSDLPortType == null)
        _initFutWSDLPortTypeProxy();
      return futWSDLPortType.obtenerGastosEjecucion(input);
}

@Override
public OutputDatosFormato guardarFut(InputFut input) throws RemoteException {
    if (futWSDLPortType == null)
        _initFutWSDLPortTypeProxy();
      return futWSDLPortType.guardarFut(input);
}

@Override
public OutputDatosVigente guardarFutVigente(InputFutVigente input)
		throws RemoteException {
    if (futWSDLPortType == null)
        _initFutWSDLPortTypeProxy();
      return futWSDLPortType.guardarFutVigente(input);
}
  
  
}