/**
 * Totales.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

public class Totales  implements java.io.Serializable {
    private java.lang.String derdom;

    private java.lang.String derndom;

    private java.lang.String act;

    private java.lang.String iva;

    private java.lang.String rec;

    private java.lang.String mul;

    private java.lang.String recc;

    private java.lang.String mulc;

    private java.lang.String tot;

    public Totales() {
    }

    public Totales(
           java.lang.String derdom,
           java.lang.String derndom,
           java.lang.String act,
           java.lang.String iva,
           java.lang.String rec,
           java.lang.String mul,
           java.lang.String recc,
           java.lang.String mulc,
           java.lang.String tot) {
           this.derdom = derdom;
           this.derndom = derndom;
           this.act = act;
           this.iva = iva;
           this.rec = rec;
           this.mul = mul;
           this.recc = recc;
           this.mulc = mulc;
           this.tot = tot;
    }


    /**
     * Gets the derdom value for this Totales.
     * 
     * @return derdom
     */
    public java.lang.String getDerdom() {
        return derdom;
    }


    /**
     * Sets the derdom value for this Totales.
     * 
     * @param derdom
     */
    public void setDerdom(java.lang.String derdom) {
        this.derdom = derdom;
    }


    /**
     * Gets the derndom value for this Totales.
     * 
     * @return derndom
     */
    public java.lang.String getDerndom() {
        return derndom;
    }


    /**
     * Sets the derndom value for this Totales.
     * 
     * @param derndom
     */
    public void setDerndom(java.lang.String derndom) {
        this.derndom = derndom;
    }


    /**
     * Gets the act value for this Totales.
     * 
     * @return act
     */
    public java.lang.String getAct() {
        return act;
    }


    /**
     * Sets the act value for this Totales.
     * 
     * @param act
     */
    public void setAct(java.lang.String act) {
        this.act = act;
    }


    /**
     * Gets the iva value for this Totales.
     * 
     * @return iva
     */
    public java.lang.String getIva() {
        return iva;
    }


    /**
     * Sets the iva value for this Totales.
     * 
     * @param iva
     */
    public void setIva(java.lang.String iva) {
        this.iva = iva;
    }


    /**
     * Gets the rec value for this Totales.
     * 
     * @return rec
     */
    public java.lang.String getRec() {
        return rec;
    }


    /**
     * Sets the rec value for this Totales.
     * 
     * @param rec
     */
    public void setRec(java.lang.String rec) {
        this.rec = rec;
    }


    /**
     * Gets the mul value for this Totales.
     * 
     * @return mul
     */
    public java.lang.String getMul() {
        return mul;
    }


    /**
     * Sets the mul value for this Totales.
     * 
     * @param mul
     */
    public void setMul(java.lang.String mul) {
        this.mul = mul;
    }


    /**
     * Gets the recc value for this Totales.
     * 
     * @return recc
     */
    public java.lang.String getRecc() {
        return recc;
    }


    /**
     * Sets the recc value for this Totales.
     * 
     * @param recc
     */
    public void setRecc(java.lang.String recc) {
        this.recc = recc;
    }


    /**
     * Gets the mulc value for this Totales.
     * 
     * @return mulc
     */
    public java.lang.String getMulc() {
        return mulc;
    }


    /**
     * Sets the mulc value for this Totales.
     * 
     * @param mulc
     */
    public void setMulc(java.lang.String mulc) {
        this.mulc = mulc;
    }


    /**
     * Gets the tot value for this Totales.
     * 
     * @return tot
     */
    public java.lang.String getTot() {
        return tot;
    }


    /**
     * Sets the tot value for this Totales.
     * 
     * @param tot
     */
    public void setTot(java.lang.String tot) {
        this.tot = tot;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Totales)) return false;
        Totales other = (Totales) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.derdom==null && other.getDerdom()==null) || 
             (this.derdom!=null &&
              this.derdom.equals(other.getDerdom()))) &&
            ((this.derndom==null && other.getDerndom()==null) || 
             (this.derndom!=null &&
              this.derndom.equals(other.getDerndom()))) &&
            ((this.act==null && other.getAct()==null) || 
             (this.act!=null &&
              this.act.equals(other.getAct()))) &&
            ((this.iva==null && other.getIva()==null) || 
             (this.iva!=null &&
              this.iva.equals(other.getIva()))) &&
            ((this.rec==null && other.getRec()==null) || 
             (this.rec!=null &&
              this.rec.equals(other.getRec()))) &&
            ((this.mul==null && other.getMul()==null) || 
             (this.mul!=null &&
              this.mul.equals(other.getMul()))) &&
            ((this.recc==null && other.getRecc()==null) || 
             (this.recc!=null &&
              this.recc.equals(other.getRecc()))) &&
            ((this.mulc==null && other.getMulc()==null) || 
             (this.mulc!=null &&
              this.mulc.equals(other.getMulc()))) &&
            ((this.tot==null && other.getTot()==null) || 
             (this.tot!=null &&
              this.tot.equals(other.getTot())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDerdom() != null) {
            _hashCode += getDerdom().hashCode();
        }
        if (getDerndom() != null) {
            _hashCode += getDerndom().hashCode();
        }
        if (getAct() != null) {
            _hashCode += getAct().hashCode();
        }
        if (getIva() != null) {
            _hashCode += getIva().hashCode();
        }
        if (getRec() != null) {
            _hashCode += getRec().hashCode();
        }
        if (getMul() != null) {
            _hashCode += getMul().hashCode();
        }
        if (getRecc() != null) {
            _hashCode += getRecc().hashCode();
        }
        if (getMulc() != null) {
            _hashCode += getMulc().hashCode();
        }
        if (getTot() != null) {
            _hashCode += getTot().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Totales.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "Totales"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("derdom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "derdom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("derndom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "derndom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("act");
        elemField.setXmlName(new javax.xml.namespace.QName("", "act"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iva");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iva"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rec");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mul");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mul"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mulc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mulc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
