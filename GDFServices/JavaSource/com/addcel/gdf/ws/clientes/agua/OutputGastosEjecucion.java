/**
 * OutputGastosEjecucion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

public class OutputGastosEjecucion  implements java.io.Serializable {
    private com.addcel.gdf.ws.clientes.agua.GastosEjecucion[] gastosEjecucion;

    private java.lang.String error;

    public OutputGastosEjecucion() {
    }

    public OutputGastosEjecucion(
           com.addcel.gdf.ws.clientes.agua.GastosEjecucion[] gastosEjecucion,
           java.lang.String error) {
           this.gastosEjecucion = gastosEjecucion;
           this.error = error;
    }


    /**
     * Gets the gastosEjecucion value for this OutputGastosEjecucion.
     * 
     * @return gastosEjecucion
     */
    public com.addcel.gdf.ws.clientes.agua.GastosEjecucion[] getGastosEjecucion() {
        return gastosEjecucion;
    }


    /**
     * Sets the gastosEjecucion value for this OutputGastosEjecucion.
     * 
     * @param gastosEjecucion
     */
    public void setGastosEjecucion(com.addcel.gdf.ws.clientes.agua.GastosEjecucion[] gastosEjecucion) {
        this.gastosEjecucion = gastosEjecucion;
    }


    /**
     * Gets the error value for this OutputGastosEjecucion.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this OutputGastosEjecucion.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OutputGastosEjecucion)) return false;
        OutputGastosEjecucion other = (OutputGastosEjecucion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.gastosEjecucion==null && other.getGastosEjecucion()==null) || 
             (this.gastosEjecucion!=null &&
              java.util.Arrays.equals(this.gastosEjecucion, other.getGastosEjecucion()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGastosEjecucion() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGastosEjecucion());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGastosEjecucion(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OutputGastosEjecucion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "OutputGastosEjecucion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gastosEjecucion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gastosEjecucion"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "GastosEjecucion"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
