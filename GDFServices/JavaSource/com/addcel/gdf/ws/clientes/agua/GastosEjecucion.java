/**
 * GastosEjecucion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

public class GastosEjecucion  implements java.io.Serializable {
    private java.lang.String tipo_tarifa;

    private java.lang.String derecho_inicial;

    private java.lang.String derecho_final;

    private java.lang.String gasto;

    private java.lang.String porcentaje;

    public GastosEjecucion() {
    }

    public GastosEjecucion(
           java.lang.String tipo_tarifa,
           java.lang.String derecho_inicial,
           java.lang.String derecho_final,
           java.lang.String gasto,
           java.lang.String porcentaje) {
           this.tipo_tarifa = tipo_tarifa;
           this.derecho_inicial = derecho_inicial;
           this.derecho_final = derecho_final;
           this.gasto = gasto;
           this.porcentaje = porcentaje;
    }


    /**
     * Gets the tipo_tarifa value for this GastosEjecucion.
     * 
     * @return tipo_tarifa
     */
    public java.lang.String getTipo_tarifa() {
        return tipo_tarifa;
    }


    /**
     * Sets the tipo_tarifa value for this GastosEjecucion.
     * 
     * @param tipo_tarifa
     */
    public void setTipo_tarifa(java.lang.String tipo_tarifa) {
        this.tipo_tarifa = tipo_tarifa;
    }


    /**
     * Gets the derecho_inicial value for this GastosEjecucion.
     * 
     * @return derecho_inicial
     */
    public java.lang.String getDerecho_inicial() {
        return derecho_inicial;
    }


    /**
     * Sets the derecho_inicial value for this GastosEjecucion.
     * 
     * @param derecho_inicial
     */
    public void setDerecho_inicial(java.lang.String derecho_inicial) {
        this.derecho_inicial = derecho_inicial;
    }


    /**
     * Gets the derecho_final value for this GastosEjecucion.
     * 
     * @return derecho_final
     */
    public java.lang.String getDerecho_final() {
        return derecho_final;
    }


    /**
     * Sets the derecho_final value for this GastosEjecucion.
     * 
     * @param derecho_final
     */
    public void setDerecho_final(java.lang.String derecho_final) {
        this.derecho_final = derecho_final;
    }


    /**
     * Gets the gasto value for this GastosEjecucion.
     * 
     * @return gasto
     */
    public java.lang.String getGasto() {
        return gasto;
    }


    /**
     * Sets the gasto value for this GastosEjecucion.
     * 
     * @param gasto
     */
    public void setGasto(java.lang.String gasto) {
        this.gasto = gasto;
    }


    /**
     * Gets the porcentaje value for this GastosEjecucion.
     * 
     * @return porcentaje
     */
    public java.lang.String getPorcentaje() {
        return porcentaje;
    }


    /**
     * Sets the porcentaje value for this GastosEjecucion.
     * 
     * @param porcentaje
     */
    public void setPorcentaje(java.lang.String porcentaje) {
        this.porcentaje = porcentaje;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GastosEjecucion)) return false;
        GastosEjecucion other = (GastosEjecucion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tipo_tarifa==null && other.getTipo_tarifa()==null) || 
             (this.tipo_tarifa!=null &&
              this.tipo_tarifa.equals(other.getTipo_tarifa()))) &&
            ((this.derecho_inicial==null && other.getDerecho_inicial()==null) || 
             (this.derecho_inicial!=null &&
              this.derecho_inicial.equals(other.getDerecho_inicial()))) &&
            ((this.derecho_final==null && other.getDerecho_final()==null) || 
             (this.derecho_final!=null &&
              this.derecho_final.equals(other.getDerecho_final()))) &&
            ((this.gasto==null && other.getGasto()==null) || 
             (this.gasto!=null &&
              this.gasto.equals(other.getGasto()))) &&
            ((this.porcentaje==null && other.getPorcentaje()==null) || 
             (this.porcentaje!=null &&
              this.porcentaje.equals(other.getPorcentaje())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTipo_tarifa() != null) {
            _hashCode += getTipo_tarifa().hashCode();
        }
        if (getDerecho_inicial() != null) {
            _hashCode += getDerecho_inicial().hashCode();
        }
        if (getDerecho_final() != null) {
            _hashCode += getDerecho_final().hashCode();
        }
        if (getGasto() != null) {
            _hashCode += getGasto().hashCode();
        }
        if (getPorcentaje() != null) {
            _hashCode += getPorcentaje().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GastosEjecucion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "GastosEjecucion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo_tarifa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipo_tarifa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("derecho_inicial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "derecho_inicial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("derecho_final");
        elemField.setXmlName(new javax.xml.namespace.QName("", "derecho_final"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gasto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gasto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("porcentaje");
        elemField.setXmlName(new javax.xml.namespace.QName("", "porcentaje"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
