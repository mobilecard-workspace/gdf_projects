/**
 * OutputDatosFormato.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

public class OutputDatosFormato  implements java.io.Serializable {
    private com.addcel.gdf.ws.clientes.agua.DatosFut[] fut;

    private com.addcel.gdf.ws.clientes.agua.Gastos[] ge;

    private com.addcel.gdf.ws.clientes.agua.Totales[] totales;

    private java.lang.String lc;

    private java.lang.String cuenta;

    private java.lang.String per_ini;

    private java.lang.String per_fin;

    private java.lang.String error;

    private java.lang.String vigencia;

    private java.lang.String sql;

    private java.lang.String origen;

    private java.lang.String dag_id;

    private java.lang.String lcCB;

    private java.lang.String id_pago;

    public OutputDatosFormato() {
    }

    public OutputDatosFormato(
           com.addcel.gdf.ws.clientes.agua.DatosFut[] fut,
           com.addcel.gdf.ws.clientes.agua.Gastos[] ge,
           com.addcel.gdf.ws.clientes.agua.Totales[] totales,
           java.lang.String lc,
           java.lang.String cuenta,
           java.lang.String per_ini,
           java.lang.String per_fin,
           java.lang.String error,
           java.lang.String vigencia,
           java.lang.String sql,
           java.lang.String origen,
           java.lang.String dag_id,
           java.lang.String lcCB,
           java.lang.String id_pago) {
           this.fut = fut;
           this.ge = ge;
           this.totales = totales;
           this.lc = lc;
           this.cuenta = cuenta;
           this.per_ini = per_ini;
           this.per_fin = per_fin;
           this.error = error;
           this.vigencia = vigencia;
           this.sql = sql;
           this.origen = origen;
           this.dag_id = dag_id;
           this.lcCB = lcCB;
           this.id_pago = id_pago;
    }


    /**
     * Gets the fut value for this OutputDatosFormato.
     * 
     * @return fut
     */
    public com.addcel.gdf.ws.clientes.agua.DatosFut[] getFut() {
        return fut;
    }


    /**
     * Sets the fut value for this OutputDatosFormato.
     * 
     * @param fut
     */
    public void setFut(com.addcel.gdf.ws.clientes.agua.DatosFut[] fut) {
        this.fut = fut;
    }


    /**
     * Gets the ge value for this OutputDatosFormato.
     * 
     * @return ge
     */
    public com.addcel.gdf.ws.clientes.agua.Gastos[] getGe() {
        return ge;
    }


    /**
     * Sets the ge value for this OutputDatosFormato.
     * 
     * @param ge
     */
    public void setGe(com.addcel.gdf.ws.clientes.agua.Gastos[] ge) {
        this.ge = ge;
    }


    /**
     * Gets the totales value for this OutputDatosFormato.
     * 
     * @return totales
     */
    public com.addcel.gdf.ws.clientes.agua.Totales[] getTotales() {
        return totales;
    }


    /**
     * Sets the totales value for this OutputDatosFormato.
     * 
     * @param totales
     */
    public void setTotales(com.addcel.gdf.ws.clientes.agua.Totales[] totales) {
        this.totales = totales;
    }


    /**
     * Gets the lc value for this OutputDatosFormato.
     * 
     * @return lc
     */
    public java.lang.String getLc() {
        return lc;
    }


    /**
     * Sets the lc value for this OutputDatosFormato.
     * 
     * @param lc
     */
    public void setLc(java.lang.String lc) {
        this.lc = lc;
    }


    /**
     * Gets the cuenta value for this OutputDatosFormato.
     * 
     * @return cuenta
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }


    /**
     * Sets the cuenta value for this OutputDatosFormato.
     * 
     * @param cuenta
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }


    /**
     * Gets the per_ini value for this OutputDatosFormato.
     * 
     * @return per_ini
     */
    public java.lang.String getPer_ini() {
        return per_ini;
    }


    /**
     * Sets the per_ini value for this OutputDatosFormato.
     * 
     * @param per_ini
     */
    public void setPer_ini(java.lang.String per_ini) {
        this.per_ini = per_ini;
    }


    /**
     * Gets the per_fin value for this OutputDatosFormato.
     * 
     * @return per_fin
     */
    public java.lang.String getPer_fin() {
        return per_fin;
    }


    /**
     * Sets the per_fin value for this OutputDatosFormato.
     * 
     * @param per_fin
     */
    public void setPer_fin(java.lang.String per_fin) {
        this.per_fin = per_fin;
    }


    /**
     * Gets the error value for this OutputDatosFormato.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this OutputDatosFormato.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }


    /**
     * Gets the vigencia value for this OutputDatosFormato.
     * 
     * @return vigencia
     */
    public java.lang.String getVigencia() {
        return vigencia;
    }


    /**
     * Sets the vigencia value for this OutputDatosFormato.
     * 
     * @param vigencia
     */
    public void setVigencia(java.lang.String vigencia) {
        this.vigencia = vigencia;
    }


    /**
     * Gets the sql value for this OutputDatosFormato.
     * 
     * @return sql
     */
    public java.lang.String getSql() {
        return sql;
    }


    /**
     * Sets the sql value for this OutputDatosFormato.
     * 
     * @param sql
     */
    public void setSql(java.lang.String sql) {
        this.sql = sql;
    }


    /**
     * Gets the origen value for this OutputDatosFormato.
     * 
     * @return origen
     */
    public java.lang.String getOrigen() {
        return origen;
    }


    /**
     * Sets the origen value for this OutputDatosFormato.
     * 
     * @param origen
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }


    /**
     * Gets the dag_id value for this OutputDatosFormato.
     * 
     * @return dag_id
     */
    public java.lang.String getDag_id() {
        return dag_id;
    }


    /**
     * Sets the dag_id value for this OutputDatosFormato.
     * 
     * @param dag_id
     */
    public void setDag_id(java.lang.String dag_id) {
        this.dag_id = dag_id;
    }


    /**
     * Gets the lcCB value for this OutputDatosFormato.
     * 
     * @return lcCB
     */
    public java.lang.String getLcCB() {
        return lcCB;
    }


    /**
     * Sets the lcCB value for this OutputDatosFormato.
     * 
     * @param lcCB
     */
    public void setLcCB(java.lang.String lcCB) {
        this.lcCB = lcCB;
    }


    /**
     * Gets the id_pago value for this OutputDatosFormato.
     * 
     * @return id_pago
     */
    public java.lang.String getId_pago() {
        return id_pago;
    }


    /**
     * Sets the id_pago value for this OutputDatosFormato.
     * 
     * @param id_pago
     */
    public void setId_pago(java.lang.String id_pago) {
        this.id_pago = id_pago;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OutputDatosFormato)) return false;
        OutputDatosFormato other = (OutputDatosFormato) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fut==null && other.getFut()==null) || 
             (this.fut!=null &&
              java.util.Arrays.equals(this.fut, other.getFut()))) &&
            ((this.ge==null && other.getGe()==null) || 
             (this.ge!=null &&
              java.util.Arrays.equals(this.ge, other.getGe()))) &&
            ((this.totales==null && other.getTotales()==null) || 
             (this.totales!=null &&
              java.util.Arrays.equals(this.totales, other.getTotales()))) &&
            ((this.lc==null && other.getLc()==null) || 
             (this.lc!=null &&
              this.lc.equals(other.getLc()))) &&
            ((this.cuenta==null && other.getCuenta()==null) || 
             (this.cuenta!=null &&
              this.cuenta.equals(other.getCuenta()))) &&
            ((this.per_ini==null && other.getPer_ini()==null) || 
             (this.per_ini!=null &&
              this.per_ini.equals(other.getPer_ini()))) &&
            ((this.per_fin==null && other.getPer_fin()==null) || 
             (this.per_fin!=null &&
              this.per_fin.equals(other.getPer_fin()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.vigencia==null && other.getVigencia()==null) || 
             (this.vigencia!=null &&
              this.vigencia.equals(other.getVigencia()))) &&
            ((this.sql==null && other.getSql()==null) || 
             (this.sql!=null &&
              this.sql.equals(other.getSql()))) &&
            ((this.origen==null && other.getOrigen()==null) || 
             (this.origen!=null &&
              this.origen.equals(other.getOrigen()))) &&
            ((this.dag_id==null && other.getDag_id()==null) || 
             (this.dag_id!=null &&
              this.dag_id.equals(other.getDag_id()))) &&
            ((this.lcCB==null && other.getLcCB()==null) || 
             (this.lcCB!=null &&
              this.lcCB.equals(other.getLcCB()))) &&
            ((this.id_pago==null && other.getId_pago()==null) || 
             (this.id_pago!=null &&
              this.id_pago.equals(other.getId_pago())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFut() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFut());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFut(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getGe() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGe());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGe(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTotales() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTotales());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTotales(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLc() != null) {
            _hashCode += getLc().hashCode();
        }
        if (getCuenta() != null) {
            _hashCode += getCuenta().hashCode();
        }
        if (getPer_ini() != null) {
            _hashCode += getPer_ini().hashCode();
        }
        if (getPer_fin() != null) {
            _hashCode += getPer_fin().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getVigencia() != null) {
            _hashCode += getVigencia().hashCode();
        }
        if (getSql() != null) {
            _hashCode += getSql().hashCode();
        }
        if (getOrigen() != null) {
            _hashCode += getOrigen().hashCode();
        }
        if (getDag_id() != null) {
            _hashCode += getDag_id().hashCode();
        }
        if (getLcCB() != null) {
            _hashCode += getLcCB().hashCode();
        }
        if (getId_pago() != null) {
            _hashCode += getId_pago().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OutputDatosFormato.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "OutputDatosFormato"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fut"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "DatosFut"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ge"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "Gastos"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totales");
        elemField.setXmlName(new javax.xml.namespace.QName("", "totales"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "Totales"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("per_ini");
        elemField.setXmlName(new javax.xml.namespace.QName("", "per_ini"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("per_fin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "per_fin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vigencia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vigencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sql");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sql"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("origen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "origen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dag_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dag_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lcCB");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lcCB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_pago");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id_pago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
