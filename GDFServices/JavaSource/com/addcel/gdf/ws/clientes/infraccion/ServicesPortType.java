/**
 * ServicesPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.infraccion;

public interface ServicesPortType extends java.rmi.Remote {
    public com.addcel.gdf.ws.clientes.infraccion.Arreglo_respuesta_wrapper solicitar_datos(com.addcel.gdf.ws.clientes.infraccion.Tipo_pregunta tipo_pregunta) throws java.rmi.RemoteException;
}
