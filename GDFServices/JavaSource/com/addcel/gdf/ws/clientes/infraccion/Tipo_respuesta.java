/**
 * Tipo_respuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.infraccion;

public class Tipo_respuesta  implements java.io.Serializable {
    private java.lang.String folio;

    private java.lang.String linea_captura;

    private java.lang.String importe;

    private java.lang.String fechainfraccion;

    private java.lang.String multa;

    private java.lang.String actualizacion;

    private java.lang.String recargos;

    private java.lang.String dias_multa;

    public Tipo_respuesta() {
    }

    public Tipo_respuesta(
           java.lang.String folio,
           java.lang.String linea_captura,
           java.lang.String importe,
           java.lang.String fechainfraccion,
           java.lang.String multa,
           java.lang.String actualizacion,
           java.lang.String recargos,
           java.lang.String dias_multa) {
           this.folio = folio;
           this.linea_captura = linea_captura;
           this.importe = importe;
           this.fechainfraccion = fechainfraccion;
           this.multa = multa;
           this.actualizacion = actualizacion;
           this.recargos = recargos;
           this.dias_multa = dias_multa;
    }


    /**
     * Gets the folio value for this Tipo_respuesta.
     * 
     * @return folio
     */
    public java.lang.String getFolio() {
        return folio;
    }


    /**
     * Sets the folio value for this Tipo_respuesta.
     * 
     * @param folio
     */
    public void setFolio(java.lang.String folio) {
        this.folio = folio;
    }


    /**
     * Gets the linea_captura value for this Tipo_respuesta.
     * 
     * @return linea_captura
     */
    public java.lang.String getLinea_captura() {
        return linea_captura;
    }


    /**
     * Sets the linea_captura value for this Tipo_respuesta.
     * 
     * @param linea_captura
     */
    public void setLinea_captura(java.lang.String linea_captura) {
        this.linea_captura = linea_captura;
    }


    /**
     * Gets the importe value for this Tipo_respuesta.
     * 
     * @return importe
     */
    public java.lang.String getImporte() {
        return importe;
    }


    /**
     * Sets the importe value for this Tipo_respuesta.
     * 
     * @param importe
     */
    public void setImporte(java.lang.String importe) {
        this.importe = importe;
    }


    /**
     * Gets the fechainfraccion value for this Tipo_respuesta.
     * 
     * @return fechainfraccion
     */
    public java.lang.String getFechainfraccion() {
        return fechainfraccion;
    }


    /**
     * Sets the fechainfraccion value for this Tipo_respuesta.
     * 
     * @param fechainfraccion
     */
    public void setFechainfraccion(java.lang.String fechainfraccion) {
        this.fechainfraccion = fechainfraccion;
    }


    /**
     * Gets the multa value for this Tipo_respuesta.
     * 
     * @return multa
     */
    public java.lang.String getMulta() {
        return multa;
    }


    /**
     * Sets the multa value for this Tipo_respuesta.
     * 
     * @param multa
     */
    public void setMulta(java.lang.String multa) {
        this.multa = multa;
    }


    /**
     * Gets the actualizacion value for this Tipo_respuesta.
     * 
     * @return actualizacion
     */
    public java.lang.String getActualizacion() {
        return actualizacion;
    }


    /**
     * Sets the actualizacion value for this Tipo_respuesta.
     * 
     * @param actualizacion
     */
    public void setActualizacion(java.lang.String actualizacion) {
        this.actualizacion = actualizacion;
    }


    /**
     * Gets the recargos value for this Tipo_respuesta.
     * 
     * @return recargos
     */
    public java.lang.String getRecargos() {
        return recargos;
    }


    /**
     * Sets the recargos value for this Tipo_respuesta.
     * 
     * @param recargos
     */
    public void setRecargos(java.lang.String recargos) {
        this.recargos = recargos;
    }


    /**
     * Gets the dias_multa value for this Tipo_respuesta.
     * 
     * @return dias_multa
     */
    public java.lang.String getDias_multa() {
        return dias_multa;
    }


    /**
     * Sets the dias_multa value for this Tipo_respuesta.
     * 
     * @param dias_multa
     */
    public void setDias_multa(java.lang.String dias_multa) {
        this.dias_multa = dias_multa;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_respuesta)) return false;
        Tipo_respuesta other = (Tipo_respuesta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.folio==null && other.getFolio()==null) || 
             (this.folio!=null &&
              this.folio.equals(other.getFolio()))) &&
            ((this.linea_captura==null && other.getLinea_captura()==null) || 
             (this.linea_captura!=null &&
              this.linea_captura.equals(other.getLinea_captura()))) &&
            ((this.importe==null && other.getImporte()==null) || 
             (this.importe!=null &&
              this.importe.equals(other.getImporte()))) &&
            ((this.fechainfraccion==null && other.getFechainfraccion()==null) || 
             (this.fechainfraccion!=null &&
              this.fechainfraccion.equals(other.getFechainfraccion()))) &&
            ((this.multa==null && other.getMulta()==null) || 
             (this.multa!=null &&
              this.multa.equals(other.getMulta()))) &&
            ((this.actualizacion==null && other.getActualizacion()==null) || 
             (this.actualizacion!=null &&
              this.actualizacion.equals(other.getActualizacion()))) &&
            ((this.recargos==null && other.getRecargos()==null) || 
             (this.recargos!=null &&
              this.recargos.equals(other.getRecargos()))) &&
            ((this.dias_multa==null && other.getDias_multa()==null) || 
             (this.dias_multa!=null &&
              this.dias_multa.equals(other.getDias_multa())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFolio() != null) {
            _hashCode += getFolio().hashCode();
        }
        if (getLinea_captura() != null) {
            _hashCode += getLinea_captura().hashCode();
        }
        if (getImporte() != null) {
            _hashCode += getImporte().hashCode();
        }
        if (getFechainfraccion() != null) {
            _hashCode += getFechainfraccion().hashCode();
        }
        if (getMulta() != null) {
            _hashCode += getMulta().hashCode();
        }
        if (getActualizacion() != null) {
            _hashCode += getActualizacion().hashCode();
        }
        if (getRecargos() != null) {
            _hashCode += getRecargos().hashCode();
        }
        if (getDias_multa() != null) {
            _hashCode += getDias_multa().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_respuesta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://wsbancos.finanzas.df.gob.mx/fut/SWinfracciones/infracciones_ws_server_ssp.php", "tipo_respuesta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "folio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("linea_captura");
        elemField.setXmlName(new javax.xml.namespace.QName("", "linea_captura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "importe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechainfraccion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fechainfraccion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("multa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "multa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualizacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "actualizacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recargos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recargos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dias_multa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dias_multa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
