/**
 * Arreglo_respuesta_wrapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.infraccion;

public class Arreglo_respuesta_wrapper  implements java.io.Serializable {
    private java.lang.String importe_total;

    private java.lang.String adeudo;

    private java.lang.String error;

    private java.lang.String error_desc;

    private com.addcel.gdf.ws.clientes.infraccion.Tipo_respuesta[] arreglo_folios;

    public Arreglo_respuesta_wrapper() {
    }

    public Arreglo_respuesta_wrapper(
           java.lang.String importe_total,
           java.lang.String adeudo,
           java.lang.String error,
           java.lang.String error_desc,
           com.addcel.gdf.ws.clientes.infraccion.Tipo_respuesta[] arreglo_folios) {
           this.importe_total = importe_total;
           this.adeudo = adeudo;
           this.error = error;
           this.error_desc = error_desc;
           this.arreglo_folios = arreglo_folios;
    }


    /**
     * Gets the importe_total value for this Arreglo_respuesta_wrapper.
     * 
     * @return importe_total
     */
    public java.lang.String getImporte_total() {
        return importe_total;
    }


    /**
     * Sets the importe_total value for this Arreglo_respuesta_wrapper.
     * 
     * @param importe_total
     */
    public void setImporte_total(java.lang.String importe_total) {
        this.importe_total = importe_total;
    }


    /**
     * Gets the adeudo value for this Arreglo_respuesta_wrapper.
     * 
     * @return adeudo
     */
    public java.lang.String getAdeudo() {
        return adeudo;
    }


    /**
     * Sets the adeudo value for this Arreglo_respuesta_wrapper.
     * 
     * @param adeudo
     */
    public void setAdeudo(java.lang.String adeudo) {
        this.adeudo = adeudo;
    }


    /**
     * Gets the error value for this Arreglo_respuesta_wrapper.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this Arreglo_respuesta_wrapper.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }


    /**
     * Gets the error_desc value for this Arreglo_respuesta_wrapper.
     * 
     * @return error_desc
     */
    public java.lang.String getError_desc() {
        return error_desc;
    }


    /**
     * Sets the error_desc value for this Arreglo_respuesta_wrapper.
     * 
     * @param error_desc
     */
    public void setError_desc(java.lang.String error_desc) {
        this.error_desc = error_desc;
    }


    /**
     * Gets the arreglo_folios value for this Arreglo_respuesta_wrapper.
     * 
     * @return arreglo_folios
     */
    public com.addcel.gdf.ws.clientes.infraccion.Tipo_respuesta[] getArreglo_folios() {
        return arreglo_folios;
    }


    /**
     * Sets the arreglo_folios value for this Arreglo_respuesta_wrapper.
     * 
     * @param arreglo_folios
     */
    public void setArreglo_folios(com.addcel.gdf.ws.clientes.infraccion.Tipo_respuesta[] arreglo_folios) {
        this.arreglo_folios = arreglo_folios;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Arreglo_respuesta_wrapper)) return false;
        Arreglo_respuesta_wrapper other = (Arreglo_respuesta_wrapper) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.importe_total==null && other.getImporte_total()==null) || 
             (this.importe_total!=null &&
              this.importe_total.equals(other.getImporte_total()))) &&
            ((this.adeudo==null && other.getAdeudo()==null) || 
             (this.adeudo!=null &&
              this.adeudo.equals(other.getAdeudo()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.error_desc==null && other.getError_desc()==null) || 
             (this.error_desc!=null &&
              this.error_desc.equals(other.getError_desc()))) &&
            ((this.arreglo_folios==null && other.getArreglo_folios()==null) || 
             (this.arreglo_folios!=null &&
              java.util.Arrays.equals(this.arreglo_folios, other.getArreglo_folios())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getImporte_total() != null) {
            _hashCode += getImporte_total().hashCode();
        }
        if (getAdeudo() != null) {
            _hashCode += getAdeudo().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getError_desc() != null) {
            _hashCode += getError_desc().hashCode();
        }
        if (getArreglo_folios() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getArreglo_folios());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getArreglo_folios(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Arreglo_respuesta_wrapper.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://wsbancos.finanzas.df.gob.mx/fut/SWinfracciones/infracciones_ws_server_ssp.php", "arreglo_respuesta_wrapper"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importe_total");
        elemField.setXmlName(new javax.xml.namespace.QName("", "importe_total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adeudo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adeudo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error_desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error_desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arreglo_folios");
        elemField.setXmlName(new javax.xml.namespace.QName("", "arreglo_folios"));
        elemField.setXmlType(new javax.xml.namespace.QName("https://wsbancos.finanzas.df.gob.mx/fut/SWinfracciones/infracciones_ws_server_ssp.php", "tipo_respuesta"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
