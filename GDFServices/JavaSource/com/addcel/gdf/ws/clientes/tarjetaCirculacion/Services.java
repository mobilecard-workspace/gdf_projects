/**
 * Services.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.tarjetaCirculacion;

public interface Services extends javax.xml.rpc.Service {
    public java.lang.String getServicesPortAddress();

    public com.addcel.gdf.ws.clientes.tarjetaCirculacion.ServicesPortType getServicesPort() throws javax.xml.rpc.ServiceException;

    public com.addcel.gdf.ws.clientes.tarjetaCirculacion.ServicesPortType getServicesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
