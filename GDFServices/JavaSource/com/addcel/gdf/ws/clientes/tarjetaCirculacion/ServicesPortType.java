/**
 * ServicesPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.tarjetaCirculacion;

public interface ServicesPortType extends java.rmi.Remote {

    /**
     * Genera la linea de captura de acuerdo a los datos proporcionados
     */
    public com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_respuesta solicitar_lc(com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_pregunta pregunta) throws java.rmi.RemoteException;

    /**
     * Genera la linea de captura de acuerdo a los datos proporcionados
     */
    public com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_respuesta solicitar_lc_emi(com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_pregunta_emi pregunta_emi) throws java.rmi.RemoteException;

    /**
     * Muestra el catalogo de marcas de vehiculos.
     */
    public com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_catalogo_marcas[] solicitar_catalogo_marcas(com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_catalogo_marcas_pregunta pregunta) throws java.rmi.RemoteException;

    /**
     * Muestra los subconceptos de una clave de tramite.
     */
    public com.addcel.gdf.ws.clientes.tarjetaCirculacion.Catalogo_subconceptos[] solicitar_subconceptos(com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_catalogo_subconceptos_pregunta pregunta) throws java.rmi.RemoteException;
}
