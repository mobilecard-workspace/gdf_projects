/**
 * ServerPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.tenencia;

public interface ServerPortType extends java.rmi.Remote {

    /**
     * Calculo Tenencia
     */
    public void calculoTenencia(javax.xml.rpc.holders.StringHolder placa, java.lang.String ejercicio, javax.xml.rpc.holders.StringHolder meses, java.lang.String usuario, java.lang.String password, javax.xml.rpc.holders.StringHolder modelo, javax.xml.rpc.holders.StringHolder ambito, javax.xml.rpc.holders.StringHolder ambito_aux, javax.xml.rpc.holders.StringHolder valor_fact, javax.xml.rpc.holders.StringHolder cve_vehi, javax.xml.rpc.holders.StringHolder fech_factura, javax.xml.rpc.holders.StringHolder num_cilindros, javax.xml.rpc.holders.StringHolder procedencia, javax.xml.rpc.holders.StringHolder rfc, javax.xml.rpc.holders.StringHolder funcion_cobro, javax.xml.rpc.holders.StringHolder numeroError, javax.xml.rpc.holders.StringHolder mensaje, javax.xml.rpc.holders.StringHolder subsidio, javax.xml.rpc.holders.StringHolder subsidio_boolean, javax.xml.rpc.holders.StringHolder depresiacion, javax.xml.rpc.holders.StringHolder tenencia, javax.xml.rpc.holders.StringHolder actualiza_ten, javax.xml.rpc.holders.StringHolder recargo_ten, javax.xml.rpc.holders.StringHolder condonacion_recargo_ten, javax.xml.rpc.holders.StringHolder total_tenencia, javax.xml.rpc.holders.StringHolder derecho, javax.xml.rpc.holders.StringHolder actuliza_derecho, javax.xml.rpc.holders.StringHolder recargo_derecho, javax.xml.rpc.holders.StringHolder total_derechos, javax.xml.rpc.holders.StringHolder total, javax.xml.rpc.holders.StringHolder dagid, javax.xml.rpc.holders.StringHolder lineacaptura, javax.xml.rpc.holders.StringHolder vigencia, javax.xml.rpc.holders.StringHolder lineacapturaCB, javax.xml.rpc.holders.StringHolder dagidautodet) throws java.rmi.RemoteException;
}
