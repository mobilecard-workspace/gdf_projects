package com.addcel.gdf.ws.clientes.tenencia;

public class ServerPortTypeProxy implements com.addcel.gdf.ws.clientes.tenencia.ServerPortType {
  private String _endpoint = null;
  private com.addcel.gdf.ws.clientes.tenencia.ServerPortType serverPortType = null;
  
  public ServerPortTypeProxy() {
    _initServerPortTypeProxy();
  }
  
  public ServerPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initServerPortTypeProxy();
  }
  
  private void _initServerPortTypeProxy() {
    try {
      serverPortType = (new com.addcel.gdf.ws.clientes.tenencia.ServerLocator()).getServerPort();
      if (serverPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)serverPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)serverPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (serverPortType != null)
      ((javax.xml.rpc.Stub)serverPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.gdf.ws.clientes.tenencia.ServerPortType getServerPortType() {
    if (serverPortType == null)
      _initServerPortTypeProxy();
    return serverPortType;
  }
  
  public void calculoTenencia(javax.xml.rpc.holders.StringHolder placa, java.lang.String ejercicio, javax.xml.rpc.holders.StringHolder meses, java.lang.String usuario, java.lang.String password, javax.xml.rpc.holders.StringHolder modelo, javax.xml.rpc.holders.StringHolder ambito, javax.xml.rpc.holders.StringHolder ambito_aux, javax.xml.rpc.holders.StringHolder valor_fact, javax.xml.rpc.holders.StringHolder cve_vehi, javax.xml.rpc.holders.StringHolder fech_factura, javax.xml.rpc.holders.StringHolder num_cilindros, javax.xml.rpc.holders.StringHolder procedencia, javax.xml.rpc.holders.StringHolder rfc, javax.xml.rpc.holders.StringHolder funcion_cobro, javax.xml.rpc.holders.StringHolder numeroError, javax.xml.rpc.holders.StringHolder mensaje, javax.xml.rpc.holders.StringHolder subsidio, javax.xml.rpc.holders.StringHolder subsidio_boolean, javax.xml.rpc.holders.StringHolder depresiacion, javax.xml.rpc.holders.StringHolder tenencia, javax.xml.rpc.holders.StringHolder actualiza_ten, javax.xml.rpc.holders.StringHolder recargo_ten, javax.xml.rpc.holders.StringHolder condonacion_recargo_ten, javax.xml.rpc.holders.StringHolder total_tenencia, javax.xml.rpc.holders.StringHolder derecho, javax.xml.rpc.holders.StringHolder actuliza_derecho, javax.xml.rpc.holders.StringHolder recargo_derecho, javax.xml.rpc.holders.StringHolder total_derechos, javax.xml.rpc.holders.StringHolder total, javax.xml.rpc.holders.StringHolder dagid, javax.xml.rpc.holders.StringHolder lineacaptura, javax.xml.rpc.holders.StringHolder vigencia, javax.xml.rpc.holders.StringHolder lineacapturaCB, javax.xml.rpc.holders.StringHolder dagidautodet) throws java.rmi.RemoteException{
    if (serverPortType == null)
      _initServerPortTypeProxy();
    serverPortType.calculoTenencia(placa, ejercicio, meses, usuario, password, modelo, ambito, ambito_aux, valor_fact, cve_vehi, fech_factura, num_cilindros, procedencia, rfc, funcion_cobro, numeroError, mensaje, subsidio, subsidio_boolean, depresiacion, tenencia, actualiza_ten, recargo_ten, condonacion_recargo_ten, total_tenencia, derecho, actuliza_derecho, recargo_derecho, total_derechos, total, dagid, lineacaptura, vigencia, lineacapturaCB, dagidautodet);
  }
  
  
}