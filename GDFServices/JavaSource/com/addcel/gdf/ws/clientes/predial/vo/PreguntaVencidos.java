/**
 * PreguntaVencidos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.predial.vo;

public class PreguntaVencidos  extends com.addcel.gdf.ws.clientes.predial.vo.Tipo_pregunta_adeudo  implements java.io.Serializable {
    private java.lang.Boolean mesesSinInteres;

    public PreguntaVencidos() {
    }

    public PreguntaVencidos(
           java.lang.Integer clave,
           java.lang.String cuenta,
           java.lang.String password,
           java.lang.String tipo_adeudo,
           java.lang.String usuario,
           java.lang.Boolean mesesSinInteres) {
        super(
            clave,
            cuenta,
            password,
            tipo_adeudo,
            usuario);
        this.mesesSinInteres = mesesSinInteres;
    }


    /**
     * Gets the mesesSinInteres value for this PreguntaVencidos.
     * 
     * @return mesesSinInteres
     */
    public java.lang.Boolean getMesesSinInteres() {
        return mesesSinInteres;
    }


    /**
     * Sets the mesesSinInteres value for this PreguntaVencidos.
     * 
     * @param mesesSinInteres
     */
    public void setMesesSinInteres(java.lang.Boolean mesesSinInteres) {
        this.mesesSinInteres = mesesSinInteres;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PreguntaVencidos)) return false;
        PreguntaVencidos other = (PreguntaVencidos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.mesesSinInteres==null && other.getMesesSinInteres()==null) || 
             (this.mesesSinInteres!=null &&
              this.mesesSinInteres.equals(other.getMesesSinInteres())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getMesesSinInteres() != null) {
            _hashCode += getMesesSinInteres().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PreguntaVencidos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "PreguntaVencidos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mesesSinInteres");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "MesesSinInteres"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
