/**
 * Envio_adeudo_vencido_periodo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.predial.vo;

public class Envio_adeudo_vencido_periodo  implements java.io.Serializable {
    private java.lang.String cadbc;

    private java.lang.String cond_gastos_ejecución;

    private java.lang.String cond_multa_incumplimiento;

    private java.lang.String cuenta;

    private java.lang.String descripcion_error;

    private com.addcel.gdf.ws.clientes.predial.vo.Detalle_periodos_vencidos[] detalle;

    private java.lang.String error;

    private java.lang.String gastos_ejecución;

    private java.lang.String interes;

    private java.lang.String linea;

    private java.lang.String multa_incumplimiento;

    private java.lang.String total_cuenta;

    public Envio_adeudo_vencido_periodo() {
    }

    public Envio_adeudo_vencido_periodo(
           java.lang.String cadbc,
           java.lang.String cond_gastos_ejecución,
           java.lang.String cond_multa_incumplimiento,
           java.lang.String cuenta,
           java.lang.String descripcion_error,
           com.addcel.gdf.ws.clientes.predial.vo.Detalle_periodos_vencidos[] detalle,
           java.lang.String error,
           java.lang.String gastos_ejecución,
           java.lang.String interes,
           java.lang.String linea,
           java.lang.String multa_incumplimiento,
           java.lang.String total_cuenta) {
           this.cadbc = cadbc;
           this.cond_gastos_ejecución = cond_gastos_ejecución;
           this.cond_multa_incumplimiento = cond_multa_incumplimiento;
           this.cuenta = cuenta;
           this.descripcion_error = descripcion_error;
           this.detalle = detalle;
           this.error = error;
           this.gastos_ejecución = gastos_ejecución;
           this.interes = interes;
           this.linea = linea;
           this.multa_incumplimiento = multa_incumplimiento;
           this.total_cuenta = total_cuenta;
    }


    /**
     * Gets the cadbc value for this Envio_adeudo_vencido_periodo.
     * 
     * @return cadbc
     */
    public java.lang.String getCadbc() {
        return cadbc;
    }


    /**
     * Sets the cadbc value for this Envio_adeudo_vencido_periodo.
     * 
     * @param cadbc
     */
    public void setCadbc(java.lang.String cadbc) {
        this.cadbc = cadbc;
    }


    /**
     * Gets the cond_gastos_ejecución value for this Envio_adeudo_vencido_periodo.
     * 
     * @return cond_gastos_ejecución
     */
    public java.lang.String getCond_gastos_ejecución() {
        return cond_gastos_ejecución;
    }


    /**
     * Sets the cond_gastos_ejecución value for this Envio_adeudo_vencido_periodo.
     * 
     * @param cond_gastos_ejecución
     */
    public void setCond_gastos_ejecución(java.lang.String cond_gastos_ejecución) {
        this.cond_gastos_ejecución = cond_gastos_ejecución;
    }


    /**
     * Gets the cond_multa_incumplimiento value for this Envio_adeudo_vencido_periodo.
     * 
     * @return cond_multa_incumplimiento
     */
    public java.lang.String getCond_multa_incumplimiento() {
        return cond_multa_incumplimiento;
    }


    /**
     * Sets the cond_multa_incumplimiento value for this Envio_adeudo_vencido_periodo.
     * 
     * @param cond_multa_incumplimiento
     */
    public void setCond_multa_incumplimiento(java.lang.String cond_multa_incumplimiento) {
        this.cond_multa_incumplimiento = cond_multa_incumplimiento;
    }


    /**
     * Gets the cuenta value for this Envio_adeudo_vencido_periodo.
     * 
     * @return cuenta
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }


    /**
     * Sets the cuenta value for this Envio_adeudo_vencido_periodo.
     * 
     * @param cuenta
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }


    /**
     * Gets the descripcion_error value for this Envio_adeudo_vencido_periodo.
     * 
     * @return descripcion_error
     */
    public java.lang.String getDescripcion_error() {
        return descripcion_error;
    }


    /**
     * Sets the descripcion_error value for this Envio_adeudo_vencido_periodo.
     * 
     * @param descripcion_error
     */
    public void setDescripcion_error(java.lang.String descripcion_error) {
        this.descripcion_error = descripcion_error;
    }


    /**
     * Gets the detalle value for this Envio_adeudo_vencido_periodo.
     * 
     * @return detalle
     */
    public com.addcel.gdf.ws.clientes.predial.vo.Detalle_periodos_vencidos[] getDetalle() {
        return detalle;
    }


    /**
     * Sets the detalle value for this Envio_adeudo_vencido_periodo.
     * 
     * @param detalle
     */
    public void setDetalle(com.addcel.gdf.ws.clientes.predial.vo.Detalle_periodos_vencidos[] detalle) {
        this.detalle = detalle;
    }


    /**
     * Gets the error value for this Envio_adeudo_vencido_periodo.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this Envio_adeudo_vencido_periodo.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }


    /**
     * Gets the gastos_ejecución value for this Envio_adeudo_vencido_periodo.
     * 
     * @return gastos_ejecución
     */
    public java.lang.String getGastos_ejecución() {
        return gastos_ejecución;
    }


    /**
     * Sets the gastos_ejecución value for this Envio_adeudo_vencido_periodo.
     * 
     * @param gastos_ejecución
     */
    public void setGastos_ejecución(java.lang.String gastos_ejecución) {
        this.gastos_ejecución = gastos_ejecución;
    }


    /**
     * Gets the interes value for this Envio_adeudo_vencido_periodo.
     * 
     * @return interes
     */
    public java.lang.String getInteres() {
        return interes;
    }


    /**
     * Sets the interes value for this Envio_adeudo_vencido_periodo.
     * 
     * @param interes
     */
    public void setInteres(java.lang.String interes) {
        this.interes = interes;
    }


    /**
     * Gets the linea value for this Envio_adeudo_vencido_periodo.
     * 
     * @return linea
     */
    public java.lang.String getLinea() {
        return linea;
    }


    /**
     * Sets the linea value for this Envio_adeudo_vencido_periodo.
     * 
     * @param linea
     */
    public void setLinea(java.lang.String linea) {
        this.linea = linea;
    }


    /**
     * Gets the multa_incumplimiento value for this Envio_adeudo_vencido_periodo.
     * 
     * @return multa_incumplimiento
     */
    public java.lang.String getMulta_incumplimiento() {
        return multa_incumplimiento;
    }


    /**
     * Sets the multa_incumplimiento value for this Envio_adeudo_vencido_periodo.
     * 
     * @param multa_incumplimiento
     */
    public void setMulta_incumplimiento(java.lang.String multa_incumplimiento) {
        this.multa_incumplimiento = multa_incumplimiento;
    }


    /**
     * Gets the total_cuenta value for this Envio_adeudo_vencido_periodo.
     * 
     * @return total_cuenta
     */
    public java.lang.String getTotal_cuenta() {
        return total_cuenta;
    }


    /**
     * Sets the total_cuenta value for this Envio_adeudo_vencido_periodo.
     * 
     * @param total_cuenta
     */
    public void setTotal_cuenta(java.lang.String total_cuenta) {
        this.total_cuenta = total_cuenta;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Envio_adeudo_vencido_periodo)) return false;
        Envio_adeudo_vencido_periodo other = (Envio_adeudo_vencido_periodo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cadbc==null && other.getCadbc()==null) || 
             (this.cadbc!=null &&
              this.cadbc.equals(other.getCadbc()))) &&
            ((this.cond_gastos_ejecución==null && other.getCond_gastos_ejecución()==null) || 
             (this.cond_gastos_ejecución!=null &&
              this.cond_gastos_ejecución.equals(other.getCond_gastos_ejecución()))) &&
            ((this.cond_multa_incumplimiento==null && other.getCond_multa_incumplimiento()==null) || 
             (this.cond_multa_incumplimiento!=null &&
              this.cond_multa_incumplimiento.equals(other.getCond_multa_incumplimiento()))) &&
            ((this.cuenta==null && other.getCuenta()==null) || 
             (this.cuenta!=null &&
              this.cuenta.equals(other.getCuenta()))) &&
            ((this.descripcion_error==null && other.getDescripcion_error()==null) || 
             (this.descripcion_error!=null &&
              this.descripcion_error.equals(other.getDescripcion_error()))) &&
            ((this.detalle==null && other.getDetalle()==null) || 
             (this.detalle!=null &&
              java.util.Arrays.equals(this.detalle, other.getDetalle()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.gastos_ejecución==null && other.getGastos_ejecución()==null) || 
             (this.gastos_ejecución!=null &&
              this.gastos_ejecución.equals(other.getGastos_ejecución()))) &&
            ((this.interes==null && other.getInteres()==null) || 
             (this.interes!=null &&
              this.interes.equals(other.getInteres()))) &&
            ((this.linea==null && other.getLinea()==null) || 
             (this.linea!=null &&
              this.linea.equals(other.getLinea()))) &&
            ((this.multa_incumplimiento==null && other.getMulta_incumplimiento()==null) || 
             (this.multa_incumplimiento!=null &&
              this.multa_incumplimiento.equals(other.getMulta_incumplimiento()))) &&
            ((this.total_cuenta==null && other.getTotal_cuenta()==null) || 
             (this.total_cuenta!=null &&
              this.total_cuenta.equals(other.getTotal_cuenta())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCadbc() != null) {
            _hashCode += getCadbc().hashCode();
        }
        if (getCond_gastos_ejecución() != null) {
            _hashCode += getCond_gastos_ejecución().hashCode();
        }
        if (getCond_multa_incumplimiento() != null) {
            _hashCode += getCond_multa_incumplimiento().hashCode();
        }
        if (getCuenta() != null) {
            _hashCode += getCuenta().hashCode();
        }
        if (getDescripcion_error() != null) {
            _hashCode += getDescripcion_error().hashCode();
        }
        if (getDetalle() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetalle());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDetalle(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getGastos_ejecución() != null) {
            _hashCode += getGastos_ejecución().hashCode();
        }
        if (getInteres() != null) {
            _hashCode += getInteres().hashCode();
        }
        if (getLinea() != null) {
            _hashCode += getLinea().hashCode();
        }
        if (getMulta_incumplimiento() != null) {
            _hashCode += getMulta_incumplimiento().hashCode();
        }
        if (getTotal_cuenta() != null) {
            _hashCode += getTotal_cuenta().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Envio_adeudo_vencido_periodo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "envio_adeudo_vencido_periodo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cadbc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Cadbc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cond_gastos_ejecución");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Cond_gastos_ejecución"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cond_multa_incumplimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Cond_multa_incumplimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Cuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcion_error");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Descripcion_error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detalle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Detalle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "detalle_periodos_vencidos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "detalle_periodos_vencidos"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gastos_ejecución");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Gastos_ejecución"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Interes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("linea");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Linea"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("multa_incumplimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Multa_incumplimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("total_cuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Total_cuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
