/**
 * Detalle_periodos_vencidos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.predial.vo;

public class Detalle_periodos_vencidos  implements java.io.Serializable {
    private java.lang.String actualización;

    private java.lang.String cond_Gast_Ejec;

    private java.lang.String cond_mult_omi;

    private java.math.BigDecimal cond_rec;

    private java.lang.String impuesto_bimestral;

    private java.lang.String multa_omision;

    private java.lang.String periodo;

    private java.lang.String recargos;

    private java.lang.String requerido;

    private java.lang.String subtotal;

    public Detalle_periodos_vencidos() {
    }

    public Detalle_periodos_vencidos(
           java.lang.String actualización,
           java.lang.String cond_Gast_Ejec,
           java.lang.String cond_mult_omi,
           java.math.BigDecimal cond_rec,
           java.lang.String impuesto_bimestral,
           java.lang.String multa_omision,
           java.lang.String periodo,
           java.lang.String recargos,
           java.lang.String requerido,
           java.lang.String subtotal) {
           this.actualización = actualización;
           this.cond_Gast_Ejec = cond_Gast_Ejec;
           this.cond_mult_omi = cond_mult_omi;
           this.cond_rec = cond_rec;
           this.impuesto_bimestral = impuesto_bimestral;
           this.multa_omision = multa_omision;
           this.periodo = periodo;
           this.recargos = recargos;
           this.requerido = requerido;
           this.subtotal = subtotal;
    }


    /**
     * Gets the actualización value for this Detalle_periodos_vencidos.
     * 
     * @return actualización
     */
    public java.lang.String getActualización() {
        return actualización;
    }


    /**
     * Sets the actualización value for this Detalle_periodos_vencidos.
     * 
     * @param actualización
     */
    public void setActualización(java.lang.String actualización) {
        this.actualización = actualización;
    }


    /**
     * Gets the cond_Gast_Ejec value for this Detalle_periodos_vencidos.
     * 
     * @return cond_Gast_Ejec
     */
    public java.lang.String getCond_Gast_Ejec() {
        return cond_Gast_Ejec;
    }


    /**
     * Sets the cond_Gast_Ejec value for this Detalle_periodos_vencidos.
     * 
     * @param cond_Gast_Ejec
     */
    public void setCond_Gast_Ejec(java.lang.String cond_Gast_Ejec) {
        this.cond_Gast_Ejec = cond_Gast_Ejec;
    }


    /**
     * Gets the cond_mult_omi value for this Detalle_periodos_vencidos.
     * 
     * @return cond_mult_omi
     */
    public java.lang.String getCond_mult_omi() {
        return cond_mult_omi;
    }


    /**
     * Sets the cond_mult_omi value for this Detalle_periodos_vencidos.
     * 
     * @param cond_mult_omi
     */
    public void setCond_mult_omi(java.lang.String cond_mult_omi) {
        this.cond_mult_omi = cond_mult_omi;
    }


    /**
     * Gets the cond_rec value for this Detalle_periodos_vencidos.
     * 
     * @return cond_rec
     */
    public java.math.BigDecimal getCond_rec() {
        return cond_rec;
    }


    /**
     * Sets the cond_rec value for this Detalle_periodos_vencidos.
     * 
     * @param cond_rec
     */
    public void setCond_rec(java.math.BigDecimal cond_rec) {
        this.cond_rec = cond_rec;
    }


    /**
     * Gets the impuesto_bimestral value for this Detalle_periodos_vencidos.
     * 
     * @return impuesto_bimestral
     */
    public java.lang.String getImpuesto_bimestral() {
        return impuesto_bimestral;
    }


    /**
     * Sets the impuesto_bimestral value for this Detalle_periodos_vencidos.
     * 
     * @param impuesto_bimestral
     */
    public void setImpuesto_bimestral(java.lang.String impuesto_bimestral) {
        this.impuesto_bimestral = impuesto_bimestral;
    }


    /**
     * Gets the multa_omision value for this Detalle_periodos_vencidos.
     * 
     * @return multa_omision
     */
    public java.lang.String getMulta_omision() {
        return multa_omision;
    }


    /**
     * Sets the multa_omision value for this Detalle_periodos_vencidos.
     * 
     * @param multa_omision
     */
    public void setMulta_omision(java.lang.String multa_omision) {
        this.multa_omision = multa_omision;
    }


    /**
     * Gets the periodo value for this Detalle_periodos_vencidos.
     * 
     * @return periodo
     */
    public java.lang.String getPeriodo() {
        return periodo;
    }


    /**
     * Sets the periodo value for this Detalle_periodos_vencidos.
     * 
     * @param periodo
     */
    public void setPeriodo(java.lang.String periodo) {
        this.periodo = periodo;
    }


    /**
     * Gets the recargos value for this Detalle_periodos_vencidos.
     * 
     * @return recargos
     */
    public java.lang.String getRecargos() {
        return recargos;
    }


    /**
     * Sets the recargos value for this Detalle_periodos_vencidos.
     * 
     * @param recargos
     */
    public void setRecargos(java.lang.String recargos) {
        this.recargos = recargos;
    }


    /**
     * Gets the requerido value for this Detalle_periodos_vencidos.
     * 
     * @return requerido
     */
    public java.lang.String getRequerido() {
        return requerido;
    }


    /**
     * Sets the requerido value for this Detalle_periodos_vencidos.
     * 
     * @param requerido
     */
    public void setRequerido(java.lang.String requerido) {
        this.requerido = requerido;
    }


    /**
     * Gets the subtotal value for this Detalle_periodos_vencidos.
     * 
     * @return subtotal
     */
    public java.lang.String getSubtotal() {
        return subtotal;
    }


    /**
     * Sets the subtotal value for this Detalle_periodos_vencidos.
     * 
     * @param subtotal
     */
    public void setSubtotal(java.lang.String subtotal) {
        this.subtotal = subtotal;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Detalle_periodos_vencidos)) return false;
        Detalle_periodos_vencidos other = (Detalle_periodos_vencidos) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.actualización==null && other.getActualización()==null) || 
             (this.actualización!=null &&
              this.actualización.equals(other.getActualización()))) &&
            ((this.cond_Gast_Ejec==null && other.getCond_Gast_Ejec()==null) || 
             (this.cond_Gast_Ejec!=null &&
              this.cond_Gast_Ejec.equals(other.getCond_Gast_Ejec()))) &&
            ((this.cond_mult_omi==null && other.getCond_mult_omi()==null) || 
             (this.cond_mult_omi!=null &&
              this.cond_mult_omi.equals(other.getCond_mult_omi()))) &&
            ((this.cond_rec==null && other.getCond_rec()==null) || 
             (this.cond_rec!=null &&
              this.cond_rec.equals(other.getCond_rec()))) &&
            ((this.impuesto_bimestral==null && other.getImpuesto_bimestral()==null) || 
             (this.impuesto_bimestral!=null &&
              this.impuesto_bimestral.equals(other.getImpuesto_bimestral()))) &&
            ((this.multa_omision==null && other.getMulta_omision()==null) || 
             (this.multa_omision!=null &&
              this.multa_omision.equals(other.getMulta_omision()))) &&
            ((this.periodo==null && other.getPeriodo()==null) || 
             (this.periodo!=null &&
              this.periodo.equals(other.getPeriodo()))) &&
            ((this.recargos==null && other.getRecargos()==null) || 
             (this.recargos!=null &&
              this.recargos.equals(other.getRecargos()))) &&
            ((this.requerido==null && other.getRequerido()==null) || 
             (this.requerido!=null &&
              this.requerido.equals(other.getRequerido()))) &&
            ((this.subtotal==null && other.getSubtotal()==null) || 
             (this.subtotal!=null &&
              this.subtotal.equals(other.getSubtotal())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActualización() != null) {
            _hashCode += getActualización().hashCode();
        }
        if (getCond_Gast_Ejec() != null) {
            _hashCode += getCond_Gast_Ejec().hashCode();
        }
        if (getCond_mult_omi() != null) {
            _hashCode += getCond_mult_omi().hashCode();
        }
        if (getCond_rec() != null) {
            _hashCode += getCond_rec().hashCode();
        }
        if (getImpuesto_bimestral() != null) {
            _hashCode += getImpuesto_bimestral().hashCode();
        }
        if (getMulta_omision() != null) {
            _hashCode += getMulta_omision().hashCode();
        }
        if (getPeriodo() != null) {
            _hashCode += getPeriodo().hashCode();
        }
        if (getRecargos() != null) {
            _hashCode += getRecargos().hashCode();
        }
        if (getRequerido() != null) {
            _hashCode += getRequerido().hashCode();
        }
        if (getSubtotal() != null) {
            _hashCode += getSubtotal().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Detalle_periodos_vencidos.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "detalle_periodos_vencidos"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualización");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Actualización"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cond_Gast_Ejec");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Cond_Gast_Ejec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cond_mult_omi");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Cond_mult_omi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cond_rec");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Cond_rec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impuesto_bimestral");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Impuesto_bimestral"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("multa_omision");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Multa_omision"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("periodo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Periodo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recargos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Recargos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requerido");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Requerido"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subtotal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Subtotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
