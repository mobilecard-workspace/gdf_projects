/**
 * Tipo_respuesta_adeudo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.predial.vo;

public class Tipo_respuesta_adeudo  implements java.io.Serializable {
    private java.lang.String bimestre;

    private java.lang.String concepto;

    private java.lang.String cuentaP;

    private java.lang.String error;

    private java.lang.String error_cel;

    private java.lang.String error_descripcion;

    private java.lang.String importe;

    private java.lang.Integer intImpuesto;

    private java.lang.String linCap;

    private java.lang.String mensaje;

    private java.lang.String reduccion;

    private java.lang.String subsidio;

    private java.lang.String total;

    private java.lang.String vencimiento;

    public Tipo_respuesta_adeudo() {
    }

    public Tipo_respuesta_adeudo(
           java.lang.String bimestre,
           java.lang.String concepto,
           java.lang.String cuentaP,
           java.lang.String error,
           java.lang.String error_cel,
           java.lang.String error_descripcion,
           java.lang.String importe,
           java.lang.Integer intImpuesto,
           java.lang.String linCap,
           java.lang.String mensaje,
           java.lang.String reduccion,
           java.lang.String subsidio,
           java.lang.String total,
           java.lang.String vencimiento) {
           this.bimestre = bimestre;
           this.concepto = concepto;
           this.cuentaP = cuentaP;
           this.error = error;
           this.error_cel = error_cel;
           this.error_descripcion = error_descripcion;
           this.importe = importe;
           this.intImpuesto = intImpuesto;
           this.linCap = linCap;
           this.mensaje = mensaje;
           this.reduccion = reduccion;
           this.subsidio = subsidio;
           this.total = total;
           this.vencimiento = vencimiento;
    }


    /**
     * Gets the bimestre value for this Tipo_respuesta_adeudo.
     * 
     * @return bimestre
     */
    public java.lang.String getBimestre() {
        return bimestre;
    }


    /**
     * Sets the bimestre value for this Tipo_respuesta_adeudo.
     * 
     * @param bimestre
     */
    public void setBimestre(java.lang.String bimestre) {
        this.bimestre = bimestre;
    }


    /**
     * Gets the concepto value for this Tipo_respuesta_adeudo.
     * 
     * @return concepto
     */
    public java.lang.String getConcepto() {
        return concepto;
    }


    /**
     * Sets the concepto value for this Tipo_respuesta_adeudo.
     * 
     * @param concepto
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }


    /**
     * Gets the cuentaP value for this Tipo_respuesta_adeudo.
     * 
     * @return cuentaP
     */
    public java.lang.String getCuentaP() {
        return cuentaP;
    }


    /**
     * Sets the cuentaP value for this Tipo_respuesta_adeudo.
     * 
     * @param cuentaP
     */
    public void setCuentaP(java.lang.String cuentaP) {
        this.cuentaP = cuentaP;
    }


    /**
     * Gets the error value for this Tipo_respuesta_adeudo.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this Tipo_respuesta_adeudo.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }


    /**
     * Gets the error_cel value for this Tipo_respuesta_adeudo.
     * 
     * @return error_cel
     */
    public java.lang.String getError_cel() {
        return error_cel;
    }


    /**
     * Sets the error_cel value for this Tipo_respuesta_adeudo.
     * 
     * @param error_cel
     */
    public void setError_cel(java.lang.String error_cel) {
        this.error_cel = error_cel;
    }


    /**
     * Gets the error_descripcion value for this Tipo_respuesta_adeudo.
     * 
     * @return error_descripcion
     */
    public java.lang.String getError_descripcion() {
        return error_descripcion;
    }


    /**
     * Sets the error_descripcion value for this Tipo_respuesta_adeudo.
     * 
     * @param error_descripcion
     */
    public void setError_descripcion(java.lang.String error_descripcion) {
        this.error_descripcion = error_descripcion;
    }


    /**
     * Gets the importe value for this Tipo_respuesta_adeudo.
     * 
     * @return importe
     */
    public java.lang.String getImporte() {
        return importe;
    }


    /**
     * Sets the importe value for this Tipo_respuesta_adeudo.
     * 
     * @param importe
     */
    public void setImporte(java.lang.String importe) {
        this.importe = importe;
    }


    /**
     * Gets the intImpuesto value for this Tipo_respuesta_adeudo.
     * 
     * @return intImpuesto
     */
    public java.lang.Integer getIntImpuesto() {
        return intImpuesto;
    }


    /**
     * Sets the intImpuesto value for this Tipo_respuesta_adeudo.
     * 
     * @param intImpuesto
     */
    public void setIntImpuesto(java.lang.Integer intImpuesto) {
        this.intImpuesto = intImpuesto;
    }


    /**
     * Gets the linCap value for this Tipo_respuesta_adeudo.
     * 
     * @return linCap
     */
    public java.lang.String getLinCap() {
        return linCap;
    }


    /**
     * Sets the linCap value for this Tipo_respuesta_adeudo.
     * 
     * @param linCap
     */
    public void setLinCap(java.lang.String linCap) {
        this.linCap = linCap;
    }


    /**
     * Gets the mensaje value for this Tipo_respuesta_adeudo.
     * 
     * @return mensaje
     */
    public java.lang.String getMensaje() {
        return mensaje;
    }


    /**
     * Sets the mensaje value for this Tipo_respuesta_adeudo.
     * 
     * @param mensaje
     */
    public void setMensaje(java.lang.String mensaje) {
        this.mensaje = mensaje;
    }


    /**
     * Gets the reduccion value for this Tipo_respuesta_adeudo.
     * 
     * @return reduccion
     */
    public java.lang.String getReduccion() {
        return reduccion;
    }


    /**
     * Sets the reduccion value for this Tipo_respuesta_adeudo.
     * 
     * @param reduccion
     */
    public void setReduccion(java.lang.String reduccion) {
        this.reduccion = reduccion;
    }


    /**
     * Gets the subsidio value for this Tipo_respuesta_adeudo.
     * 
     * @return subsidio
     */
    public java.lang.String getSubsidio() {
        return subsidio;
    }


    /**
     * Sets the subsidio value for this Tipo_respuesta_adeudo.
     * 
     * @param subsidio
     */
    public void setSubsidio(java.lang.String subsidio) {
        this.subsidio = subsidio;
    }


    /**
     * Gets the total value for this Tipo_respuesta_adeudo.
     * 
     * @return total
     */
    public java.lang.String getTotal() {
        return total;
    }


    /**
     * Sets the total value for this Tipo_respuesta_adeudo.
     * 
     * @param total
     */
    public void setTotal(java.lang.String total) {
        this.total = total;
    }


    /**
     * Gets the vencimiento value for this Tipo_respuesta_adeudo.
     * 
     * @return vencimiento
     */
    public java.lang.String getVencimiento() {
        return vencimiento;
    }


    /**
     * Sets the vencimiento value for this Tipo_respuesta_adeudo.
     * 
     * @param vencimiento
     */
    public void setVencimiento(java.lang.String vencimiento) {
        this.vencimiento = vencimiento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_respuesta_adeudo)) return false;
        Tipo_respuesta_adeudo other = (Tipo_respuesta_adeudo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bimestre==null && other.getBimestre()==null) || 
             (this.bimestre!=null &&
              this.bimestre.equals(other.getBimestre()))) &&
            ((this.concepto==null && other.getConcepto()==null) || 
             (this.concepto!=null &&
              this.concepto.equals(other.getConcepto()))) &&
            ((this.cuentaP==null && other.getCuentaP()==null) || 
             (this.cuentaP!=null &&
              this.cuentaP.equals(other.getCuentaP()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.error_cel==null && other.getError_cel()==null) || 
             (this.error_cel!=null &&
              this.error_cel.equals(other.getError_cel()))) &&
            ((this.error_descripcion==null && other.getError_descripcion()==null) || 
             (this.error_descripcion!=null &&
              this.error_descripcion.equals(other.getError_descripcion()))) &&
            ((this.importe==null && other.getImporte()==null) || 
             (this.importe!=null &&
              this.importe.equals(other.getImporte()))) &&
            ((this.intImpuesto==null && other.getIntImpuesto()==null) || 
             (this.intImpuesto!=null &&
              this.intImpuesto.equals(other.getIntImpuesto()))) &&
            ((this.linCap==null && other.getLinCap()==null) || 
             (this.linCap!=null &&
              this.linCap.equals(other.getLinCap()))) &&
            ((this.mensaje==null && other.getMensaje()==null) || 
             (this.mensaje!=null &&
              this.mensaje.equals(other.getMensaje()))) &&
            ((this.reduccion==null && other.getReduccion()==null) || 
             (this.reduccion!=null &&
              this.reduccion.equals(other.getReduccion()))) &&
            ((this.subsidio==null && other.getSubsidio()==null) || 
             (this.subsidio!=null &&
              this.subsidio.equals(other.getSubsidio()))) &&
            ((this.total==null && other.getTotal()==null) || 
             (this.total!=null &&
              this.total.equals(other.getTotal()))) &&
            ((this.vencimiento==null && other.getVencimiento()==null) || 
             (this.vencimiento!=null &&
              this.vencimiento.equals(other.getVencimiento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBimestre() != null) {
            _hashCode += getBimestre().hashCode();
        }
        if (getConcepto() != null) {
            _hashCode += getConcepto().hashCode();
        }
        if (getCuentaP() != null) {
            _hashCode += getCuentaP().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getError_cel() != null) {
            _hashCode += getError_cel().hashCode();
        }
        if (getError_descripcion() != null) {
            _hashCode += getError_descripcion().hashCode();
        }
        if (getImporte() != null) {
            _hashCode += getImporte().hashCode();
        }
        if (getIntImpuesto() != null) {
            _hashCode += getIntImpuesto().hashCode();
        }
        if (getLinCap() != null) {
            _hashCode += getLinCap().hashCode();
        }
        if (getMensaje() != null) {
            _hashCode += getMensaje().hashCode();
        }
        if (getReduccion() != null) {
            _hashCode += getReduccion().hashCode();
        }
        if (getSubsidio() != null) {
            _hashCode += getSubsidio().hashCode();
        }
        if (getTotal() != null) {
            _hashCode += getTotal().hashCode();
        }
        if (getVencimiento() != null) {
            _hashCode += getVencimiento().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_respuesta_adeudo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "tipo_respuesta_adeudo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bimestre");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Bimestre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("concepto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Concepto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cuentaP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "CuentaP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error_cel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Error_cel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error_descripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Error_descripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importe");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Importe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("intImpuesto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "IntImpuesto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("linCap");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "LinCap"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensaje");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Mensaje"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reduccion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Reduccion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subsidio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Subsidio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("total");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vencimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Vencimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
