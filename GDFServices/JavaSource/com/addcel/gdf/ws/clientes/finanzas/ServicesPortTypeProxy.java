package com.addcel.gdf.ws.clientes.finanzas;

public class ServicesPortTypeProxy implements com.addcel.gdf.ws.clientes.finanzas.ServicesPortType {
  private String _endpoint = null;
  private com.addcel.gdf.ws.clientes.finanzas.ServicesPortType servicesPortType = null;
  
  public ServicesPortTypeProxy() {
    _initServicesPortTypeProxy();
  }
  
  public ServicesPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initServicesPortTypeProxy();
  }
  
  private void _initServicesPortTypeProxy() {
    try {
      servicesPortType = (new com.addcel.gdf.ws.clientes.finanzas.ServicesLocator()).getServicesPort();
      if (servicesPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)servicesPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)servicesPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (servicesPortType != null)
      ((javax.xml.rpc.Stub)servicesPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.gdf.ws.clientes.finanzas.ServicesPortType getServicesPortType() {
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType;
  }
  
  public com.addcel.gdf.ws.clientes.finanzas.Tipo_respuesta registrar_pago(com.addcel.gdf.ws.clientes.finanzas.Tipo_pregunta pregunta) throws java.rmi.RemoteException{
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType.registrar_pago(pregunta);
  }
  
  public com.addcel.gdf.ws.clientes.finanzas.Tipo_respuesta_cancela cancelar_pago(com.addcel.gdf.ws.clientes.finanzas.Tipo_pregunta_cancela pregunta) throws java.rmi.RemoteException{
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType.cancelar_pago(pregunta);
  }
  
  
}