/**
 * ServicesLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.nomina;

public class ServicesLocator extends org.apache.axis.client.Service implements com.addcel.gdf.ws.clientes.nomina.Services {

    public ServicesLocator() {
    }


    public ServicesLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ServicesLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ServicesPort
    private java.lang.String ServicesPort_address = "https://wsbancos.finanzas.df.gob.mx:443/fut/nomina/nom_ws_secure_server.php";

    public java.lang.String getServicesPortAddress() {
        return ServicesPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ServicesPortWSDDServiceName = "ServicesPort";

    public java.lang.String getServicesPortWSDDServiceName() {
        return ServicesPortWSDDServiceName;
    }

    public void setServicesPortWSDDServiceName(java.lang.String name) {
        ServicesPortWSDDServiceName = name;
    }

    public com.addcel.gdf.ws.clientes.nomina.ServicesPortType getServicesPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ServicesPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getServicesPort(endpoint);
    }

    public com.addcel.gdf.ws.clientes.nomina.ServicesPortType getServicesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.gdf.ws.clientes.nomina.ServicesBindingStub _stub = new com.addcel.gdf.ws.clientes.nomina.ServicesBindingStub(portAddress, this);
            _stub.setPortName(getServicesPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setServicesPortEndpointAddress(java.lang.String address) {
        ServicesPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.gdf.ws.clientes.nomina.ServicesPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.gdf.ws.clientes.nomina.ServicesBindingStub _stub = new com.addcel.gdf.ws.clientes.nomina.ServicesBindingStub(new java.net.URL(ServicesPort_address), this);
                _stub.setPortName(getServicesPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ServicesPort".equals(inputPortName)) {
            return getServicesPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("https://wsbancos.finanzas.df.gob.mx/fut/nomina/nom_ws_secure_server.php", "Services");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("https://wsbancos.finanzas.df.gob.mx/fut/nomina/nom_ws_secure_server.php", "ServicesPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ServicesPort".equals(portName)) {
            setServicesPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
