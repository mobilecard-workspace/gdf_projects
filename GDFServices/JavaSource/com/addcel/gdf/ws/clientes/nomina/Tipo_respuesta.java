/**
 * Tipo_respuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.nomina;

public class Tipo_respuesta  implements java.io.Serializable {
    private java.lang.String clave;

    private java.lang.String rfc;

    private java.lang.String mes_pago;

    private java.lang.String anio_pago;

    private java.math.BigDecimal remuneraciones;

    private java.math.BigDecimal impuesto;

    private java.math.BigDecimal impuesto_actualizado;

    private java.math.BigDecimal recargos;

    private java.math.BigDecimal recargos_condonado;

    private java.math.BigDecimal interes;

    private java.math.BigDecimal total;

    private java.lang.String vigencia;

    private java.lang.String lineacaptura;

    private java.lang.String lineacapturaCB;

    private java.math.BigDecimal error;

    private java.lang.String error_descripcion;

    public Tipo_respuesta() {
    }

    public Tipo_respuesta(
           java.lang.String clave,
           java.lang.String rfc,
           java.lang.String mes_pago,
           java.lang.String anio_pago,
           java.math.BigDecimal remuneraciones,
           java.math.BigDecimal impuesto,
           java.math.BigDecimal impuesto_actualizado,
           java.math.BigDecimal recargos,
           java.math.BigDecimal recargos_condonado,
           java.math.BigDecimal interes,
           java.math.BigDecimal total,
           java.lang.String vigencia,
           java.lang.String lineacaptura,
           java.lang.String lineacapturaCB,
           java.math.BigDecimal error,
           java.lang.String error_descripcion) {
           this.clave = clave;
           this.rfc = rfc;
           this.mes_pago = mes_pago;
           this.anio_pago = anio_pago;
           this.remuneraciones = remuneraciones;
           this.impuesto = impuesto;
           this.impuesto_actualizado = impuesto_actualizado;
           this.recargos = recargos;
           this.recargos_condonado = recargos_condonado;
           this.interes = interes;
           this.total = total;
           this.vigencia = vigencia;
           this.lineacaptura = lineacaptura;
           this.lineacapturaCB = lineacapturaCB;
           this.error = error;
           this.error_descripcion = error_descripcion;
    }


    /**
     * Gets the clave value for this Tipo_respuesta.
     * 
     * @return clave
     */
    public java.lang.String getClave() {
        return clave;
    }


    /**
     * Sets the clave value for this Tipo_respuesta.
     * 
     * @param clave
     */
    public void setClave(java.lang.String clave) {
        this.clave = clave;
    }


    /**
     * Gets the rfc value for this Tipo_respuesta.
     * 
     * @return rfc
     */
    public java.lang.String getRfc() {
        return rfc;
    }


    /**
     * Sets the rfc value for this Tipo_respuesta.
     * 
     * @param rfc
     */
    public void setRfc(java.lang.String rfc) {
        this.rfc = rfc;
    }


    /**
     * Gets the mes_pago value for this Tipo_respuesta.
     * 
     * @return mes_pago
     */
    public java.lang.String getMes_pago() {
        return mes_pago;
    }


    /**
     * Sets the mes_pago value for this Tipo_respuesta.
     * 
     * @param mes_pago
     */
    public void setMes_pago(java.lang.String mes_pago) {
        this.mes_pago = mes_pago;
    }


    /**
     * Gets the anio_pago value for this Tipo_respuesta.
     * 
     * @return anio_pago
     */
    public java.lang.String getAnio_pago() {
        return anio_pago;
    }


    /**
     * Sets the anio_pago value for this Tipo_respuesta.
     * 
     * @param anio_pago
     */
    public void setAnio_pago(java.lang.String anio_pago) {
        this.anio_pago = anio_pago;
    }


    /**
     * Gets the remuneraciones value for this Tipo_respuesta.
     * 
     * @return remuneraciones
     */
    public java.math.BigDecimal getRemuneraciones() {
        return remuneraciones;
    }


    /**
     * Sets the remuneraciones value for this Tipo_respuesta.
     * 
     * @param remuneraciones
     */
    public void setRemuneraciones(java.math.BigDecimal remuneraciones) {
        this.remuneraciones = remuneraciones;
    }


    /**
     * Gets the impuesto value for this Tipo_respuesta.
     * 
     * @return impuesto
     */
    public java.math.BigDecimal getImpuesto() {
        return impuesto;
    }


    /**
     * Sets the impuesto value for this Tipo_respuesta.
     * 
     * @param impuesto
     */
    public void setImpuesto(java.math.BigDecimal impuesto) {
        this.impuesto = impuesto;
    }


    /**
     * Gets the impuesto_actualizado value for this Tipo_respuesta.
     * 
     * @return impuesto_actualizado
     */
    public java.math.BigDecimal getImpuesto_actualizado() {
        return impuesto_actualizado;
    }


    /**
     * Sets the impuesto_actualizado value for this Tipo_respuesta.
     * 
     * @param impuesto_actualizado
     */
    public void setImpuesto_actualizado(java.math.BigDecimal impuesto_actualizado) {
        this.impuesto_actualizado = impuesto_actualizado;
    }


    /**
     * Gets the recargos value for this Tipo_respuesta.
     * 
     * @return recargos
     */
    public java.math.BigDecimal getRecargos() {
        return recargos;
    }


    /**
     * Sets the recargos value for this Tipo_respuesta.
     * 
     * @param recargos
     */
    public void setRecargos(java.math.BigDecimal recargos) {
        this.recargos = recargos;
    }


    /**
     * Gets the recargos_condonado value for this Tipo_respuesta.
     * 
     * @return recargos_condonado
     */
    public java.math.BigDecimal getRecargos_condonado() {
        return recargos_condonado;
    }


    /**
     * Sets the recargos_condonado value for this Tipo_respuesta.
     * 
     * @param recargos_condonado
     */
    public void setRecargos_condonado(java.math.BigDecimal recargos_condonado) {
        this.recargos_condonado = recargos_condonado;
    }


    /**
     * Gets the interes value for this Tipo_respuesta.
     * 
     * @return interes
     */
    public java.math.BigDecimal getInteres() {
        return interes;
    }


    /**
     * Sets the interes value for this Tipo_respuesta.
     * 
     * @param interes
     */
    public void setInteres(java.math.BigDecimal interes) {
        this.interes = interes;
    }


    /**
     * Gets the total value for this Tipo_respuesta.
     * 
     * @return total
     */
    public java.math.BigDecimal getTotal() {
        return total;
    }


    /**
     * Sets the total value for this Tipo_respuesta.
     * 
     * @param total
     */
    public void setTotal(java.math.BigDecimal total) {
        this.total = total;
    }


    /**
     * Gets the vigencia value for this Tipo_respuesta.
     * 
     * @return vigencia
     */
    public java.lang.String getVigencia() {
        return vigencia;
    }


    /**
     * Sets the vigencia value for this Tipo_respuesta.
     * 
     * @param vigencia
     */
    public void setVigencia(java.lang.String vigencia) {
        this.vigencia = vigencia;
    }


    /**
     * Gets the lineacaptura value for this Tipo_respuesta.
     * 
     * @return lineacaptura
     */
    public java.lang.String getLineacaptura() {
        return lineacaptura;
    }


    /**
     * Sets the lineacaptura value for this Tipo_respuesta.
     * 
     * @param lineacaptura
     */
    public void setLineacaptura(java.lang.String lineacaptura) {
        this.lineacaptura = lineacaptura;
    }


    /**
     * Gets the lineacapturaCB value for this Tipo_respuesta.
     * 
     * @return lineacapturaCB
     */
    public java.lang.String getLineacapturaCB() {
        return lineacapturaCB;
    }


    /**
     * Sets the lineacapturaCB value for this Tipo_respuesta.
     * 
     * @param lineacapturaCB
     */
    public void setLineacapturaCB(java.lang.String lineacapturaCB) {
        this.lineacapturaCB = lineacapturaCB;
    }


    /**
     * Gets the error value for this Tipo_respuesta.
     * 
     * @return error
     */
    public java.math.BigDecimal getError() {
        return error;
    }


    /**
     * Sets the error value for this Tipo_respuesta.
     * 
     * @param error
     */
    public void setError(java.math.BigDecimal error) {
        this.error = error;
    }


    /**
     * Gets the error_descripcion value for this Tipo_respuesta.
     * 
     * @return error_descripcion
     */
    public java.lang.String getError_descripcion() {
        return error_descripcion;
    }


    /**
     * Sets the error_descripcion value for this Tipo_respuesta.
     * 
     * @param error_descripcion
     */
    public void setError_descripcion(java.lang.String error_descripcion) {
        this.error_descripcion = error_descripcion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_respuesta)) return false;
        Tipo_respuesta other = (Tipo_respuesta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.clave==null && other.getClave()==null) || 
             (this.clave!=null &&
              this.clave.equals(other.getClave()))) &&
            ((this.rfc==null && other.getRfc()==null) || 
             (this.rfc!=null &&
              this.rfc.equals(other.getRfc()))) &&
            ((this.mes_pago==null && other.getMes_pago()==null) || 
             (this.mes_pago!=null &&
              this.mes_pago.equals(other.getMes_pago()))) &&
            ((this.anio_pago==null && other.getAnio_pago()==null) || 
             (this.anio_pago!=null &&
              this.anio_pago.equals(other.getAnio_pago()))) &&
            ((this.remuneraciones==null && other.getRemuneraciones()==null) || 
             (this.remuneraciones!=null &&
              this.remuneraciones.equals(other.getRemuneraciones()))) &&
            ((this.impuesto==null && other.getImpuesto()==null) || 
             (this.impuesto!=null &&
              this.impuesto.equals(other.getImpuesto()))) &&
            ((this.impuesto_actualizado==null && other.getImpuesto_actualizado()==null) || 
             (this.impuesto_actualizado!=null &&
              this.impuesto_actualizado.equals(other.getImpuesto_actualizado()))) &&
            ((this.recargos==null && other.getRecargos()==null) || 
             (this.recargos!=null &&
              this.recargos.equals(other.getRecargos()))) &&
            ((this.recargos_condonado==null && other.getRecargos_condonado()==null) || 
             (this.recargos_condonado!=null &&
              this.recargos_condonado.equals(other.getRecargos_condonado()))) &&
            ((this.interes==null && other.getInteres()==null) || 
             (this.interes!=null &&
              this.interes.equals(other.getInteres()))) &&
            ((this.total==null && other.getTotal()==null) || 
             (this.total!=null &&
              this.total.equals(other.getTotal()))) &&
            ((this.vigencia==null && other.getVigencia()==null) || 
             (this.vigencia!=null &&
              this.vigencia.equals(other.getVigencia()))) &&
            ((this.lineacaptura==null && other.getLineacaptura()==null) || 
             (this.lineacaptura!=null &&
              this.lineacaptura.equals(other.getLineacaptura()))) &&
            ((this.lineacapturaCB==null && other.getLineacapturaCB()==null) || 
             (this.lineacapturaCB!=null &&
              this.lineacapturaCB.equals(other.getLineacapturaCB()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.error_descripcion==null && other.getError_descripcion()==null) || 
             (this.error_descripcion!=null &&
              this.error_descripcion.equals(other.getError_descripcion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClave() != null) {
            _hashCode += getClave().hashCode();
        }
        if (getRfc() != null) {
            _hashCode += getRfc().hashCode();
        }
        if (getMes_pago() != null) {
            _hashCode += getMes_pago().hashCode();
        }
        if (getAnio_pago() != null) {
            _hashCode += getAnio_pago().hashCode();
        }
        if (getRemuneraciones() != null) {
            _hashCode += getRemuneraciones().hashCode();
        }
        if (getImpuesto() != null) {
            _hashCode += getImpuesto().hashCode();
        }
        if (getImpuesto_actualizado() != null) {
            _hashCode += getImpuesto_actualizado().hashCode();
        }
        if (getRecargos() != null) {
            _hashCode += getRecargos().hashCode();
        }
        if (getRecargos_condonado() != null) {
            _hashCode += getRecargos_condonado().hashCode();
        }
        if (getInteres() != null) {
            _hashCode += getInteres().hashCode();
        }
        if (getTotal() != null) {
            _hashCode += getTotal().hashCode();
        }
        if (getVigencia() != null) {
            _hashCode += getVigencia().hashCode();
        }
        if (getLineacaptura() != null) {
            _hashCode += getLineacaptura().hashCode();
        }
        if (getLineacapturaCB() != null) {
            _hashCode += getLineacapturaCB().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getError_descripcion() != null) {
            _hashCode += getError_descripcion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_respuesta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://wsbancos.finanzas.df.gob.mx/fut/nomina/nom_ws_secure_server.php", "tipo_respuesta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clave");
        elemField.setXmlName(new javax.xml.namespace.QName("", "clave"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mes_pago");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mes_pago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anio_pago");
        elemField.setXmlName(new javax.xml.namespace.QName("", "anio_pago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remuneraciones");
        elemField.setXmlName(new javax.xml.namespace.QName("", "remuneraciones"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impuesto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "impuesto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impuesto_actualizado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "impuesto_actualizado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recargos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recargos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recargos_condonado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recargos_condonado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "interes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("total");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vigencia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vigencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineacaptura");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lineacaptura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineacapturaCB");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lineacapturaCB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error_descripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error_descripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
