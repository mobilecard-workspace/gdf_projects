/**
 * Tipo_respuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.licencias;

public class Tipo_respuesta  implements java.io.Serializable {
    private java.lang.String concepto;

    private java.math.BigDecimal clave;

    private java.lang.String rfc;

    private float costo_unitario;

    private float importe;

    private java.lang.String vigencia;

    private java.math.BigDecimal ejercicio;

    private java.lang.String folio;

    private java.lang.String lc;

    private java.math.BigDecimal error;

    private java.lang.String error_descripcion;

    public Tipo_respuesta() {
    }

    public Tipo_respuesta(
           java.lang.String concepto,
           java.math.BigDecimal clave,
           java.lang.String rfc,
           float costo_unitario,
           float importe,
           java.lang.String vigencia,
           java.math.BigDecimal ejercicio,
           java.lang.String folio,
           java.lang.String lc,
           java.math.BigDecimal error,
           java.lang.String error_descripcion) {
           this.concepto = concepto;
           this.clave = clave;
           this.rfc = rfc;
           this.costo_unitario = costo_unitario;
           this.importe = importe;
           this.vigencia = vigencia;
           this.ejercicio = ejercicio;
           this.folio = folio;
           this.lc = lc;
           this.error = error;
           this.error_descripcion = error_descripcion;
    }


    /**
     * Gets the concepto value for this Tipo_respuesta.
     * 
     * @return concepto
     */
    public java.lang.String getConcepto() {
        return concepto;
    }


    /**
     * Sets the concepto value for this Tipo_respuesta.
     * 
     * @param concepto
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }


    /**
     * Gets the clave value for this Tipo_respuesta.
     * 
     * @return clave
     */
    public java.math.BigDecimal getClave() {
        return clave;
    }


    /**
     * Sets the clave value for this Tipo_respuesta.
     * 
     * @param clave
     */
    public void setClave(java.math.BigDecimal clave) {
        this.clave = clave;
    }


    /**
     * Gets the rfc value for this Tipo_respuesta.
     * 
     * @return rfc
     */
    public java.lang.String getRfc() {
        return rfc;
    }


    /**
     * Sets the rfc value for this Tipo_respuesta.
     * 
     * @param rfc
     */
    public void setRfc(java.lang.String rfc) {
        this.rfc = rfc;
    }


    /**
     * Gets the costo_unitario value for this Tipo_respuesta.
     * 
     * @return costo_unitario
     */
    public float getCosto_unitario() {
        return costo_unitario;
    }


    /**
     * Sets the costo_unitario value for this Tipo_respuesta.
     * 
     * @param costo_unitario
     */
    public void setCosto_unitario(float costo_unitario) {
        this.costo_unitario = costo_unitario;
    }


    /**
     * Gets the importe value for this Tipo_respuesta.
     * 
     * @return importe
     */
    public float getImporte() {
        return importe;
    }


    /**
     * Sets the importe value for this Tipo_respuesta.
     * 
     * @param importe
     */
    public void setImporte(float importe) {
        this.importe = importe;
    }


    /**
     * Gets the vigencia value for this Tipo_respuesta.
     * 
     * @return vigencia
     */
    public java.lang.String getVigencia() {
        return vigencia;
    }


    /**
     * Sets the vigencia value for this Tipo_respuesta.
     * 
     * @param vigencia
     */
    public void setVigencia(java.lang.String vigencia) {
        this.vigencia = vigencia;
    }


    /**
     * Gets the ejercicio value for this Tipo_respuesta.
     * 
     * @return ejercicio
     */
    public java.math.BigDecimal getEjercicio() {
        return ejercicio;
    }


    /**
     * Sets the ejercicio value for this Tipo_respuesta.
     * 
     * @param ejercicio
     */
    public void setEjercicio(java.math.BigDecimal ejercicio) {
        this.ejercicio = ejercicio;
    }


    /**
     * Gets the folio value for this Tipo_respuesta.
     * 
     * @return folio
     */
    public java.lang.String getFolio() {
        return folio;
    }


    /**
     * Sets the folio value for this Tipo_respuesta.
     * 
     * @param folio
     */
    public void setFolio(java.lang.String folio) {
        this.folio = folio;
    }


    /**
     * Gets the lc value for this Tipo_respuesta.
     * 
     * @return lc
     */
    public java.lang.String getLc() {
        return lc;
    }


    /**
     * Sets the lc value for this Tipo_respuesta.
     * 
     * @param lc
     */
    public void setLc(java.lang.String lc) {
        this.lc = lc;
    }


    /**
     * Gets the error value for this Tipo_respuesta.
     * 
     * @return error
     */
    public java.math.BigDecimal getError() {
        return error;
    }


    /**
     * Sets the error value for this Tipo_respuesta.
     * 
     * @param error
     */
    public void setError(java.math.BigDecimal error) {
        this.error = error;
    }


    /**
     * Gets the error_descripcion value for this Tipo_respuesta.
     * 
     * @return error_descripcion
     */
    public java.lang.String getError_descripcion() {
        return error_descripcion;
    }


    /**
     * Sets the error_descripcion value for this Tipo_respuesta.
     * 
     * @param error_descripcion
     */
    public void setError_descripcion(java.lang.String error_descripcion) {
        this.error_descripcion = error_descripcion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_respuesta)) return false;
        Tipo_respuesta other = (Tipo_respuesta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.concepto==null && other.getConcepto()==null) || 
             (this.concepto!=null &&
              this.concepto.equals(other.getConcepto()))) &&
            ((this.clave==null && other.getClave()==null) || 
             (this.clave!=null &&
              this.clave.equals(other.getClave()))) &&
            ((this.rfc==null && other.getRfc()==null) || 
             (this.rfc!=null &&
              this.rfc.equals(other.getRfc()))) &&
            this.costo_unitario == other.getCosto_unitario() &&
            this.importe == other.getImporte() &&
            ((this.vigencia==null && other.getVigencia()==null) || 
             (this.vigencia!=null &&
              this.vigencia.equals(other.getVigencia()))) &&
            ((this.ejercicio==null && other.getEjercicio()==null) || 
             (this.ejercicio!=null &&
              this.ejercicio.equals(other.getEjercicio()))) &&
            ((this.folio==null && other.getFolio()==null) || 
             (this.folio!=null &&
              this.folio.equals(other.getFolio()))) &&
            ((this.lc==null && other.getLc()==null) || 
             (this.lc!=null &&
              this.lc.equals(other.getLc()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.error_descripcion==null && other.getError_descripcion()==null) || 
             (this.error_descripcion!=null &&
              this.error_descripcion.equals(other.getError_descripcion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConcepto() != null) {
            _hashCode += getConcepto().hashCode();
        }
        if (getClave() != null) {
            _hashCode += getClave().hashCode();
        }
        if (getRfc() != null) {
            _hashCode += getRfc().hashCode();
        }
        _hashCode += new Float(getCosto_unitario()).hashCode();
        _hashCode += new Float(getImporte()).hashCode();
        if (getVigencia() != null) {
            _hashCode += getVigencia().hashCode();
        }
        if (getEjercicio() != null) {
            _hashCode += getEjercicio().hashCode();
        }
        if (getFolio() != null) {
            _hashCode += getFolio().hashCode();
        }
        if (getLc() != null) {
            _hashCode += getLc().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getError_descripcion() != null) {
            _hashCode += getError_descripcion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_respuesta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://10.1.65.12/fut/vehicular/licencias/lic_ws_secure_server.php", "tipo_respuesta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("concepto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "concepto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clave");
        elemField.setXmlName(new javax.xml.namespace.QName("", "clave"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("costo_unitario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "costo_unitario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "importe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vigencia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vigencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ejercicio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ejercicio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "folio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error_descripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error_descripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
