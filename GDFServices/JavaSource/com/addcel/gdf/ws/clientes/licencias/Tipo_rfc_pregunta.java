/**
 * Tipo_rfc_pregunta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.licencias;

public class Tipo_rfc_pregunta  implements java.io.Serializable {
    private java.lang.String nombre;

    private java.lang.String ap_paterno;

    private java.lang.String ap_materno;

    private java.math.BigDecimal dia;

    private java.math.BigDecimal mes;

    private java.math.BigDecimal anio;

    private java.lang.String usuario;

    private java.lang.String password;

    public Tipo_rfc_pregunta() {
    }

    public Tipo_rfc_pregunta(
           java.lang.String nombre,
           java.lang.String ap_paterno,
           java.lang.String ap_materno,
           java.math.BigDecimal dia,
           java.math.BigDecimal mes,
           java.math.BigDecimal anio,
           java.lang.String usuario,
           java.lang.String password) {
           this.nombre = nombre;
           this.ap_paterno = ap_paterno;
           this.ap_materno = ap_materno;
           this.dia = dia;
           this.mes = mes;
           this.anio = anio;
           this.usuario = usuario;
           this.password = password;
    }


    /**
     * Gets the nombre value for this Tipo_rfc_pregunta.
     * 
     * @return nombre
     */
    public java.lang.String getNombre() {
        return nombre;
    }


    /**
     * Sets the nombre value for this Tipo_rfc_pregunta.
     * 
     * @param nombre
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }


    /**
     * Gets the ap_paterno value for this Tipo_rfc_pregunta.
     * 
     * @return ap_paterno
     */
    public java.lang.String getAp_paterno() {
        return ap_paterno;
    }


    /**
     * Sets the ap_paterno value for this Tipo_rfc_pregunta.
     * 
     * @param ap_paterno
     */
    public void setAp_paterno(java.lang.String ap_paterno) {
        this.ap_paterno = ap_paterno;
    }


    /**
     * Gets the ap_materno value for this Tipo_rfc_pregunta.
     * 
     * @return ap_materno
     */
    public java.lang.String getAp_materno() {
        return ap_materno;
    }


    /**
     * Sets the ap_materno value for this Tipo_rfc_pregunta.
     * 
     * @param ap_materno
     */
    public void setAp_materno(java.lang.String ap_materno) {
        this.ap_materno = ap_materno;
    }


    /**
     * Gets the dia value for this Tipo_rfc_pregunta.
     * 
     * @return dia
     */
    public java.math.BigDecimal getDia() {
        return dia;
    }


    /**
     * Sets the dia value for this Tipo_rfc_pregunta.
     * 
     * @param dia
     */
    public void setDia(java.math.BigDecimal dia) {
        this.dia = dia;
    }


    /**
     * Gets the mes value for this Tipo_rfc_pregunta.
     * 
     * @return mes
     */
    public java.math.BigDecimal getMes() {
        return mes;
    }


    /**
     * Sets the mes value for this Tipo_rfc_pregunta.
     * 
     * @param mes
     */
    public void setMes(java.math.BigDecimal mes) {
        this.mes = mes;
    }


    /**
     * Gets the anio value for this Tipo_rfc_pregunta.
     * 
     * @return anio
     */
    public java.math.BigDecimal getAnio() {
        return anio;
    }


    /**
     * Sets the anio value for this Tipo_rfc_pregunta.
     * 
     * @param anio
     */
    public void setAnio(java.math.BigDecimal anio) {
        this.anio = anio;
    }


    /**
     * Gets the usuario value for this Tipo_rfc_pregunta.
     * 
     * @return usuario
     */
    public java.lang.String getUsuario() {
        return usuario;
    }


    /**
     * Sets the usuario value for this Tipo_rfc_pregunta.
     * 
     * @param usuario
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }


    /**
     * Gets the password value for this Tipo_rfc_pregunta.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this Tipo_rfc_pregunta.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_rfc_pregunta)) return false;
        Tipo_rfc_pregunta other = (Tipo_rfc_pregunta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nombre==null && other.getNombre()==null) || 
             (this.nombre!=null &&
              this.nombre.equals(other.getNombre()))) &&
            ((this.ap_paterno==null && other.getAp_paterno()==null) || 
             (this.ap_paterno!=null &&
              this.ap_paterno.equals(other.getAp_paterno()))) &&
            ((this.ap_materno==null && other.getAp_materno()==null) || 
             (this.ap_materno!=null &&
              this.ap_materno.equals(other.getAp_materno()))) &&
            ((this.dia==null && other.getDia()==null) || 
             (this.dia!=null &&
              this.dia.equals(other.getDia()))) &&
            ((this.mes==null && other.getMes()==null) || 
             (this.mes!=null &&
              this.mes.equals(other.getMes()))) &&
            ((this.anio==null && other.getAnio()==null) || 
             (this.anio!=null &&
              this.anio.equals(other.getAnio()))) &&
            ((this.usuario==null && other.getUsuario()==null) || 
             (this.usuario!=null &&
              this.usuario.equals(other.getUsuario()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNombre() != null) {
            _hashCode += getNombre().hashCode();
        }
        if (getAp_paterno() != null) {
            _hashCode += getAp_paterno().hashCode();
        }
        if (getAp_materno() != null) {
            _hashCode += getAp_materno().hashCode();
        }
        if (getDia() != null) {
            _hashCode += getDia().hashCode();
        }
        if (getMes() != null) {
            _hashCode += getMes().hashCode();
        }
        if (getAnio() != null) {
            _hashCode += getAnio().hashCode();
        }
        if (getUsuario() != null) {
            _hashCode += getUsuario().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_rfc_pregunta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://10.1.65.12/fut/vehicular/licencias/lic_ws_secure_server.php", "tipo_rfc_pregunta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ap_paterno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ap_paterno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ap_materno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ap_materno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "anio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
