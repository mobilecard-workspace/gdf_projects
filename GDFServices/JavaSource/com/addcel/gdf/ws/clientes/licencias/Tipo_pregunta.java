/**
 * Tipo_pregunta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.licencias;

public class Tipo_pregunta  implements java.io.Serializable {
    private java.lang.String rfc;

    private java.math.BigDecimal clave;

    private java.math.BigDecimal ejercicio;

    private java.lang.String tipo_licencia;

    private java.lang.String usuario;

    private java.lang.String password;

    public Tipo_pregunta() {
    }

    public Tipo_pregunta(
           java.lang.String rfc,
           java.math.BigDecimal clave,
           java.math.BigDecimal ejercicio,
           java.lang.String tipo_licencia,
           java.lang.String usuario,
           java.lang.String password) {
           this.rfc = rfc;
           this.clave = clave;
           this.ejercicio = ejercicio;
           this.tipo_licencia = tipo_licencia;
           this.usuario = usuario;
           this.password = password;
    }


    /**
     * Gets the rfc value for this Tipo_pregunta.
     * 
     * @return rfc
     */
    public java.lang.String getRfc() {
        return rfc;
    }


    /**
     * Sets the rfc value for this Tipo_pregunta.
     * 
     * @param rfc
     */
    public void setRfc(java.lang.String rfc) {
        this.rfc = rfc;
    }


    /**
     * Gets the clave value for this Tipo_pregunta.
     * 
     * @return clave
     */
    public java.math.BigDecimal getClave() {
        return clave;
    }


    /**
     * Sets the clave value for this Tipo_pregunta.
     * 
     * @param clave
     */
    public void setClave(java.math.BigDecimal clave) {
        this.clave = clave;
    }


    /**
     * Gets the ejercicio value for this Tipo_pregunta.
     * 
     * @return ejercicio
     */
    public java.math.BigDecimal getEjercicio() {
        return ejercicio;
    }


    /**
     * Sets the ejercicio value for this Tipo_pregunta.
     * 
     * @param ejercicio
     */
    public void setEjercicio(java.math.BigDecimal ejercicio) {
        this.ejercicio = ejercicio;
    }


    /**
     * Gets the tipo_licencia value for this Tipo_pregunta.
     * 
     * @return tipo_licencia
     */
    public java.lang.String getTipo_licencia() {
        return tipo_licencia;
    }


    /**
     * Sets the tipo_licencia value for this Tipo_pregunta.
     * 
     * @param tipo_licencia
     */
    public void setTipo_licencia(java.lang.String tipo_licencia) {
        this.tipo_licencia = tipo_licencia;
    }


    /**
     * Gets the usuario value for this Tipo_pregunta.
     * 
     * @return usuario
     */
    public java.lang.String getUsuario() {
        return usuario;
    }


    /**
     * Sets the usuario value for this Tipo_pregunta.
     * 
     * @param usuario
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }


    /**
     * Gets the password value for this Tipo_pregunta.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this Tipo_pregunta.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_pregunta)) return false;
        Tipo_pregunta other = (Tipo_pregunta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rfc==null && other.getRfc()==null) || 
             (this.rfc!=null &&
              this.rfc.equals(other.getRfc()))) &&
            ((this.clave==null && other.getClave()==null) || 
             (this.clave!=null &&
              this.clave.equals(other.getClave()))) &&
            ((this.ejercicio==null && other.getEjercicio()==null) || 
             (this.ejercicio!=null &&
              this.ejercicio.equals(other.getEjercicio()))) &&
            ((this.tipo_licencia==null && other.getTipo_licencia()==null) || 
             (this.tipo_licencia!=null &&
              this.tipo_licencia.equals(other.getTipo_licencia()))) &&
            ((this.usuario==null && other.getUsuario()==null) || 
             (this.usuario!=null &&
              this.usuario.equals(other.getUsuario()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRfc() != null) {
            _hashCode += getRfc().hashCode();
        }
        if (getClave() != null) {
            _hashCode += getClave().hashCode();
        }
        if (getEjercicio() != null) {
            _hashCode += getEjercicio().hashCode();
        }
        if (getTipo_licencia() != null) {
            _hashCode += getTipo_licencia().hashCode();
        }
        if (getUsuario() != null) {
            _hashCode += getUsuario().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_pregunta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://10.1.65.12/fut/vehicular/licencias/lic_ws_secure_server.php", "tipo_pregunta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clave");
        elemField.setXmlName(new javax.xml.namespace.QName("", "clave"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ejercicio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ejercicio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo_licencia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipo_licencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
