/**
 * ServicesPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.licencias;

public interface ServicesPortType extends java.rmi.Remote {

    /**
     * Calcula solo la linea de captura
     */
    public com.addcel.gdf.ws.clientes.licencias.Tipo_respuesta solicitar_lc(com.addcel.gdf.ws.clientes.licencias.Tipo_pregunta pregunta) throws java.rmi.RemoteException;

    /**
     * Calcula el RFC para los datos de nombre, paterno, materno y
     * fec. nacimiento separada
     */
    public com.addcel.gdf.ws.clientes.licencias.Tipo_rfc_respuesta solicitar_rfc(com.addcel.gdf.ws.clientes.licencias.Tipo_rfc_pregunta pregunta_rfc) throws java.rmi.RemoteException;

    /**
     * Calcula los datos completos de una linea de captura
     */
    public com.addcel.gdf.ws.clientes.licencias.Tipo_respuesta[] solicitar_lc_multiple(com.addcel.gdf.ws.clientes.licencias.Tipo_pregunta[] preguntas) throws java.rmi.RemoteException;
}
