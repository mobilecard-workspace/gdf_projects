package com.addcel.gdf.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.ProsaPagoVO;
import com.addcel.gdf.vo.UsuarioVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.vo.bitacoras.TransactionProcomVO;
import com.addcel.gdf.ws.clientes.finanzas.Tipo_respuesta;
import com.addcel.utils.ConstantesGDF;


/**
 * Servlet implementation class ComercioConGDF
 */
public class ProcomAproviGDFService{
	private static final Logger logger = LoggerFactory.getLogger(ProcomAproviGDFService.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProcomAproviGDFService() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void processProcom(TransactionProcomVO transactionProcomVO, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	logger.debug("Procesando peticion");
    	ProcomService ps=new ProcomService();
    	TransactionProcomService tranProcomService = new TransactionProcomService();
    	PagoProsaService prosaService = new PagoProsaService();
    	
    	Tipo_respuesta resp = null;
    	AbstractVO abstractPagoVO = null;
    	StringBuffer paramEmail = null;
    	UsuarioVO usuario = null;
//    	HashMap<String, String> result = new HashMap<String, String>();
    	TBitacoraVO tbitacoraVO = new TBitacoraVO();
//    	TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
    	ProsaPagoVO prosaPagoVO = new ProsaPagoVO();
    	int statusExito = 1;
		String tipoTDC = "NI";
    	
		//Insercion en ecommerce        
		logger.info("Inserccion en ecommerce");
		tranProcomService.insertTransactionProcom(transactionProcomVO);
        
        logger.info("Inicio digestRegresoProsa.");
        //Valida digest
        String digest = ps.digestRegresoProsa(transactionProcomVO);
        usuario = ps.selectUsuario(String.valueOf(transactionProcomVO.getId_usuario()));
		tipoTDC = ps.tipoTDC(usuario.getUsrTdcNumero());
        
//        if(transactionProcomVO.getEm_Digest() != null && transactionProcomVO.getEm_Digest().equals(digest)){
        	 if (!transactionProcomVO.getEm_Auth().equals("000000")) {
        		 //Aprovicionamineto gdf
        		 logger.info("Aprovisionamiento GDF");    
        		 prosaService.updateBitacoraDetalle(transactionProcomVO.getEm_Auth(), Long.parseLong(transactionProcomVO.getEm_OrderID()), transactionProcomVO.getId_producto(), 1, tipoTDC);
        		 abstractPagoVO = getDetallePago(transactionProcomVO);
        		 
        		 // (Linea de Captura, Referencia, Total)
        		 try{
	        		 resp = ps.aprovisionamientoGdf(
	        				 abstractPagoVO.getLinea_captura(), transactionProcomVO.getEm_RefNum(), 
	        				 new BigDecimal(abstractPagoVO.getTotalPago()));
	        		 if(resp!=null){        		 
//	        			 result.put("respgdf", resp.getSecuencia_trans().toString());   
		        		 logger.info("Respuesta de gdf (Confirmacion de Pago)");
		        		 logger.info(resp.getSecuencia_trans()+"\t"+resp.getError()+"\t"+resp.getError_descripcion());
	        		 }
        		 }catch(Exception e){
        			 logger.error("Error en Aprovisionamiento GDF: " + abstractPagoVO.getId_bitacora(), e);   
        			 resp = new Tipo_respuesta();
        			 resp.setError(new BigDecimal(3));
        			 resp.setError_descripcion(e.getMessage().length() > 50?e.getMessage().substring(0, 59):e.getMessage());
        		 }
        		 if(resp!= null){	        			
        			 statusExito = (resp.getError().intValue() == 0) ? 1: 2;
        		 }else
        			statusExito = 2; //2
        		 
        		 prosaPagoVO.setStatus(1);
				 prosaPagoVO.setMsg("EXITO PAGO " + prosaService.descIdProduto(transactionProcomVO.getId_producto()) + " 3D Secure AUTORIZADO");
        		 prosaPagoVO.setAutorizacion(transactionProcomVO.getEm_Auth());
        		 
        		 tbitacoraVO.setId_bitacora(abstractPagoVO.getId_bitacora());
        		 tbitacoraVO.setId_usuario(abstractPagoVO.getId_usuario());
        		 
        		 logger.info("statusExito : " + statusExito);
//        		 prosaService.updateBitacoraDetalle(transactionProcomVO.getEm_Auth(), abstractPagoVO.getId_bitacora(), transactionProcomVO.getId_producto(), statusExito, tipoTDC);
        		 prosaService.updateBitacoras(tbitacoraVO, prosaPagoVO, transactionProcomVO.getId_producto(), usuario.getUsrTdcNumero(), tipoTDC);
        		 
        		 String jsonObject = ps.jsonObject( abstractPagoVO.getId_bitacora(), (int)transactionProcomVO.getId_producto(), ps.getUltimos4TDC(usuario.getUsrTdcNumero()));

        		 //envio de mail
        		 if(usuario != null && usuario.getEmail() != null){
//        			 correo="jesus.lopez@addcel.com";
        			 paramEmail = new StringBuffer()
        					 .append("correo=").append(usuario.getEmail())
//        					 .append("correo=").append("jesus.lopez@addcel.com")
        					 .append("&json=").append(jsonObject);
        			 
//        			 logger.info("Invio de Emai Parametros: " + paramEmail);
        			 ps.peticionUrlPostParams(ConstantesGDF.URL_MAIL + transactionProcomVO.getId_producto(), paramEmail.toString()); 
        		 }else{
        			 logger.error("ERROR imposible enviar email: el correo es nulo");
        		 }        		 
        		 request.setAttribute("monto", abstractPagoVO.getTotalPago());
        		 request.setAttribute("respuesta", transactionProcomVO.getEm_Response());
        		 request.setAttribute("autorizacion", transactionProcomVO.getEm_Auth());
        		 request.setAttribute("referencia", abstractPagoVO.getId_bitacora()+"");
        		 
				 request.getRequestDispatcher("/procom/pago.jsp").forward(request, response);
        	 }else{
        		 prosaPagoVO.setStatus(0);
				 prosaPagoVO.setMsg("ERROR PAGO " + prosaService.descIdProduto(transactionProcomVO.getId_producto()) + " " + 
						 transactionProcomVO.getEm_Response() +
        				 (transactionProcomVO.getEm_RefNum() != null? " - " + transactionProcomVO.getEm_RefNum(): ""));
        		 prosaPagoVO.setAutorizacion(null);
        		 
        		 tbitacoraVO.setId_bitacora(Long.parseLong(transactionProcomVO.getEm_OrderID()));
        		 tbitacoraVO.setId_usuario(transactionProcomVO.getId_usuario());
        		 
        		 logger.info("statusExito : " + statusExito);
//        		 prosaService.updateBitacoraDetalle(transactionProcomVO.getEm_Auth(), abstractPagoVO.getId_bitacora(), transactionProcomVO.getId_producto(), statusExito, tipoTDC);
        		 prosaService.updateBitacoras(tbitacoraVO, prosaPagoVO, transactionProcomVO.getId_producto(), usuario.getUsrTdcNumero(), tipoTDC);
        		 logger.error("Transaccion no autorizada por 3D Secure");
        		 logger.error("Respuesta: "+transactionProcomVO.getEm_Response());
        		 logger.error("RefNum: "+ transactionProcomVO.getEm_RefNum());
        		 
        		 request.setAttribute("descError", "Respuesta: " + transactionProcomVO.getEm_Response() +
        				 (transactionProcomVO.getEm_RefNum() != null? " - " + transactionProcomVO.getEm_RefNum(): ""));
        		 request.getRequestDispatcher("/procom/error.jsp").forward(request, response);
        	 }
//        }else{        	
//        	 logger.error("Digest incorrecto: "+ transactionProcomVO.getEm_Digest()+ "!=" +digest);
//        	 logger.error("Digest Prosa: "+transactionProcomVO.getEm_Digest());
//        	 logger.error("Digest Calculado: "+digest);
//        	 request.setAttribute("descError", "Datos Invalidos en respuesta de prosa: DIGEST");
//        	 request.getRequestDispatcher("/procom/error.jsp").forward(request, response);
//        }
    }

    public TransactionProcomVO getProcomRequest(HttpServletRequest request) throws ServletException, IOException {
    	//logger.debug("Creando ProcomRespuestaVO.");
    	TransactionProcomVO transactionProcomVO = new TransactionProcomVO();

    	if(request.getParameter("id_usuario") != null){
    		transactionProcomVO.setId_usuario(Long.parseLong(request.getParameter("id_usuario")));
    	}
    	if(request.getParameter("id_producto") != null){
    		transactionProcomVO.setId_producto(Integer.parseInt(request.getParameter("id_producto")));
    	}
    	if(request.getParameter("EM_Response") != null){
    		transactionProcomVO.setEm_Response(request.getParameter("EM_Response"));
    	}
    	if(request.getParameter("EM_Total") != null){
    		transactionProcomVO.setEm_Total(Long.parseLong(request.getParameter("EM_Total")));
    		transactionProcomVO.setTransactionProcomMnt(Long.parseLong(request.getParameter("EM_Total")));
    	}
    	if(request.getParameter("EM_OrderID") != null){
    		transactionProcomVO.setEm_OrderID(request.getParameter("EM_OrderID"));
    		transactionProcomVO.setTransactionProcomMrchId(Long.parseLong(request.getParameter("EM_OrderID")));
    		transactionProcomVO.setTransactionProsaId(Long.parseLong(request.getParameter("EM_OrderID")));
    	}
    	if(request.getParameter("EM_Merchant") != null){
    		transactionProcomVO.setEm_Merchant(Long.parseLong(request.getParameter("EM_Merchant")));
    	}
    	if(request.getParameter("EM_Store") != null){
    		transactionProcomVO.setEm_Store(Long.parseLong(request.getParameter("EM_Store")));
    	}
    	if(request.getParameter("EM_Term") != null){
    		transactionProcomVO.setEm_Term(request.getParameter("EM_Term"));
    	}
    	if(request.getParameter("EM_RefNum") != null){
    		transactionProcomVO.setEm_RefNum(request.getParameter("EM_RefNum"));
    	}
    	if(request.getParameter("EM_Auth") != null){
    		transactionProcomVO.setEm_Auth(request.getParameter("EM_Auth"));
    	}
    	if(request.getParameter("EM_Digest") != null){
    		transactionProcomVO.setEm_Digest(request.getParameter("EM_Digest"));
    	}
    	
    	return transactionProcomVO;
    }
    
    protected AbstractVO getDetallePago(TransactionProcomVO transactionProcomVO) throws Exception {

		AbstractVO abstractVO = null;
		Dependency dependency = null;
		HashMap<String, String> consulta = new HashMap<String, String>();
		
		switch (transactionProcomVO.getId_producto()) {
			case ConstantesGDF.idProd_Nomina:
				dependency = new NominaService();
				break;
	
			case ConstantesGDF.idProd_Tenencia:
				dependency = new TenenciaServices();
				break;
				
			case ConstantesGDF.idProd_Infraccion:
				dependency = new InfraccionService();
				break;
				
			case ConstantesGDF.idProd_Predial:
				dependency = new PredialServices();
				break;
			
			case ConstantesGDF.idProd_Agua:
				dependency = new AguaService();
				break;
			
			case ConstantesGDF.idProd_PredialVencido:
				dependency = new PredialVencidoServices();
				break;
				
			case ConstantesGDF.ID_PRODUCTO_TARJETA_CIRCULACION:
				dependency = new TarjetaCirculacionServices();
				break;
				
			case ConstantesGDF.ID_PRODUCTO_LICENCIA_CONDUCTOR:
				dependency = new LicenciaConductorServices();
				break;
			
			case ConstantesGDF.ID_PRODUCTO_LICENCIA_PERMANENTE:
				dependency = new LicenciaPermanenteServices();
				break;
				
			default:
				break;
		}
		
		consulta.put("id_bitacora", transactionProcomVO.getEm_OrderID());
		
		abstractVO = dependency.getDetallePagoProcom(consulta);
		
		return abstractVO;
	}
	
    protected String getTicket(TransactionProcomVO transactionProcomVO) throws Exception {
    	StringBuffer ticket= new StringBuffer();
    	if(!transactionProcomVO.getEm_Response().equals("000000")){
    		ticket.append("Excepcion: ").append(transactionProcomVO.getEm_Auth());
    	}else{
    		ticket.append("Pago GDF ").append(
        			descIdProduto((int)transactionProcomVO.getId_producto()));
    	}
    	return ticket.toString();
    }
    
    protected String getConcepto(TransactionProcomVO transactionProcomVO) throws Exception {
    	StringBuffer concepto = new StringBuffer();
    	
    		concepto.append("Pago GDF ").append(
        			descIdProduto((int)transactionProcomVO.getId_producto()));
    	return concepto.toString();
    }
    
    private String descIdProduto(int id_producto){
    	String desc = "";
    	switch (id_producto) {
			case ConstantesGDF.idProd_Nomina:
				desc = "Nomina";
				break;
	
			case ConstantesGDF.idProd_Tenencia:
				desc = "Tenencia";
				break;
				
			case ConstantesGDF.idProd_Infraccion:
				desc = "Infraccion";
				break;
				
			case ConstantesGDF.idProd_Predial:
				desc = "Predial";
				break;
			
			case ConstantesGDF.idProd_Agua:
				desc = "Agua";
				break;
				
			default:
				break;
		}
    	return desc;
    }
}
