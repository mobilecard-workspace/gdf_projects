package com.addcel.gdf.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.model.dao.TerminosCondicionesDao;
import com.addcel.gdf.vo.terminos.TerminosCondicionesVO;
import com.addcel.utils.ibatis.service.AbstractService;
import com.google.gson.Gson;

public class TerminosCondicionesService extends AbstractService {
	private static final Logger log = LoggerFactory.getLogger(TerminosCondicionesService.class);
	
	public String insertTerminosXUsuario(TerminosCondicionesVO termVO) throws Exception{
		String json = null;
		TerminosCondicionesDao dao = null;
		try {
			termVO.setStatus("1");
			dao = (TerminosCondicionesDao) getBean("TerminosCondicionesDao");
			dao.insertTerminosXUsuario(termVO);
			json = "{\"error\":\"Exito en el guardado\",\"numError\":\"0\"}";
		} catch (Exception e) {
			log.error("Ocurrio un error el TerminosCondicionesService.insertTerminosXUsuario:" , e);
			throw new Exception(e);
		}
		
		return json;
	}
	
	public String selectTerminosXUsuario(TerminosCondicionesVO termVO) throws Exception{
		List<TerminosCondicionesVO> lisTerminos = new ArrayList<TerminosCondicionesVO>();
		String json = null;
		TerminosCondicionesDao dao = null;
		Gson gson = new Gson();
		try {
			dao = (TerminosCondicionesDao) getBean("TerminosCondicionesDao");
			lisTerminos = dao.selectTerminosXUsuario(termVO);
			
			for(int i = 0; i <lisTerminos.size(); i++){
				if(lisTerminos.get(i).getId_usuario() != null){
					lisTerminos.remove(i);
					i--;
				}
			}
			json = "{\"consultaTerminosCondiciones\":" + gson.toJson(lisTerminos) + "}";
		} catch (Exception e) {
			log.error("Ocurrio un error el TerminosCondicionesService.selectTerminosXUsuario:" , e);
			throw new Exception(e);
		}
		
		return json;
	}

	public String selectTerminosXAplicacion(TerminosCondicionesVO termVO) throws Exception{
		String json = null;
		TerminosCondicionesDao dao = null;
		Gson gson = new Gson();
		try {
			dao = (TerminosCondicionesDao) getBean("TerminosCondicionesDao");
			json = "{\"consultaTerminosCondiciones\":" + gson.toJson(dao.selectTerminosXAplicacion(termVO)) + "}";
		} catch (Exception e) {
			log.error("Ocurrio un error el TerminosCondicionesService.selectTerminosXAplicacion:" , e);
			throw new Exception(e);
		}
		
		return json;
	}
	
}
