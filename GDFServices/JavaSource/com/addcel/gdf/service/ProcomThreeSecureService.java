package com.addcel.gdf.service;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.UsuarioVO;
import com.addcel.gdf.vo.ProcomProsa.ProcomVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.Utilerias;
import com.google.gson.Gson;

public class ProcomThreeSecureService {
	private static final Logger logger = LoggerFactory.getLogger(ProcomThreeSecureService.class);
	
		
	public void inicioProceso(String json, HttpServletRequest request, HttpServletResponse response) {    	
		logger.debug("Procesando peticion 3D Secure");
		HashMap<String, Object> resBitacora = new HashMap<String, Object>();
        AbstractVO abstractVO = null;
        AbstractVO resp = null;
		TBitacoraVO tbitacoraVO = null;
		ProcomVO procomVO = null;
		
		ProcomService ps = new ProcomService();
		PagoProsaService pagoProsa = new PagoProsaService();
		TBitacoraService tbSer = new TBitacoraService();
		UsuarioVO usuario = null;
		Gson gson = new Gson();
		String afiliacion = null;
		
        try{
        	if(pagoProsa.validMaintenanceHours(ConstantesGDF.lTime, ConstantesGDF.uTime,ConstantesGDF.dayFlag)){
        		resp = new AbstractVO("1", "El sistema de pagos se encuentra en mantenimiento, intente después de las: " + ConstantesGDF.uTime + " Hrs.");
    			logger.error("Dentro de periodo de mantenimiento");
        	} else {
        		abstractVO = gson.fromJson(json, AbstractVO.class);
        		if(abstractVO.getToken() == null){
        			resp = new AbstractVO("2", "El parametro TOKEN no puede ser NULL.");
        			logger.error("El parametro TOKEN no puede ser NULL");
        		}else{
        			abstractVO.setToken(AddcelCrypto.decryptSensitive(abstractVO.getToken()));
        			logger.info("token ==> " + abstractVO.getToken());
    			
        			if((tbSer.difFechaMin(abstractVO.getToken())) > 30){
//        			if(false){
        				resp = new AbstractVO("3", "La Transaccion no es valida.");
        				json = "{\"numError\":2,\"error\":\"La Transaccion no es valida\"}";
        				logger.info("La Transacción no es válida");
					
        			}else{
//        				abstractVO.setPassword(AddcelCrypto.encryptPassword(abstractVO.getPassword()));
        				usuario = ps.selectUsuario(String.valueOf(abstractVO.getId_usuario()));
    				
        				double total = Double.parseDouble(ps.montoMaxProducto((new Integer(abstractVO.getId_producto()))));
        				
        				if(tbSer.selectTBitacoraXLC(abstractVO.getLinea_captura() + "%")){
        					resp = new AbstractVO("10", "La Linea de Captura ya fue pagada.");
        					logger.error("La linea de captura ya esta pagada: " + abstractVO.getLinea_captura());
        				}else if(Double.parseDouble(abstractVO.getTotalPago()) > total){
        					resp = new AbstractVO("4", "El limite maximo para una transaccion es $" + Utilerias.formatoImporte(total) + ".");
        					logger.error("El limite maximo para una transaccion es: " + total);
        				}else if(usuario == null){
        					resp = new AbstractVO("5", "El usuario no Existe.");
        					logger.error("El usuario no Existe, id_user: " + abstractVO.getId_usuario());
        				}else if(usuario.getIdUsrStatus() == 0){
        					resp = new AbstractVO("6", "Usuario Inactivo. Consulte con el Administrador.");
	    					logger.error("Usuario Inactivo. Consulte con el Administrador, id_user: " + abstractVO.getId_usuario());
    		            }else if(usuario.getIdUsrStatus() == 99){
    		            	resp = new AbstractVO("7", "Modifique su Informacion por seguridad, antes de reaizar un pago.");
       						logger.error("Modifique su Informacion por seguridad, id_user: " + abstractVO.getId_usuario());
    		            }else if(usuario.getIdUsrStatus() == 98){
    		            	resp = new AbstractVO("8", "Modifique su clave de acceso por seguridad.");
       						logger.error("Modifique su Informacion por seguridad, id_user: " + abstractVO.getId_usuario());
    		            }else if(usuario.getIdUsrStatus() != 1){
    		            	resp = new AbstractVO("9", "Estatus de Usuario Invalido. Consulte con el Administrador.");
	    					logger.error("Estatus de Usuario Invalido, id_user: " + abstractVO.getId_usuario());
//        				}else if(!usuario.getUsrPwd().equals(abstractVO.getPassword())){
//        					json = "{\"numError\":11,\"error\":\"Password incorrecto.\"}";
//        					logger.error("El password es incorrecto, id_user: " + abstractVO.getId_usuario());
        				}else{
    						afiliacion = ps.getAfiliacion();
        					resBitacora = pagoProsa.insertaBitcoras(json, afiliacion);
			        	
        					if(resBitacora.containsKey("tbitacoraVO")){
        						tbitacoraVO = (TBitacoraVO) resBitacora.get("tbitacoraVO");
        					}
        					if(resBitacora.containsKey("guardaVO")){
        						abstractVO = (AbstractVO) resBitacora.get("guardaVO");
        					}
//            					json = swichAbierto(tbitacoraVO , abstractVO  , usuario, afiliacion );
        					procomVO = new ProcomService().comercioFin( // user, referencia, monto, idTramite, afiliacion
        			    			String.valueOf(abstractVO.getId_usuario()), String.valueOf(tbitacoraVO.getId_bitacora()) , 
        			    			abstractVO.getTotalPago(), String.valueOf(abstractVO.getId_producto()), afiliacion);
        					
        					request.setAttribute("prosa", procomVO);
        					request.getRequestDispatcher("/procom/comercioThreeSecure.jsp").forward(request, response);
        				}
        			}
        		}
        	}
        	if(resp != null){
        		request.setAttribute("descError", resp.getDescError());
				request.getRequestDispatcher("/procom/error.jsp").forward(request, response);
        	}
        	
        }catch(Exception e){
        	request.setAttribute("descError", "Error General: " + e.getMessage());
        	logger.error("Ocurrio un error al ProcomThreeSecureService.inicioProceso: {}" , e);
        }
    }
}
