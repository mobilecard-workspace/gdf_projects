package com.addcel.gdf.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.model.dao.TBitacoraProsaDao;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.utils.ibatis.service.AbstractService;

public class TBitacoraProsaService extends AbstractService {
	private static final Logger log = LoggerFactory.getLogger(TBitacoraProsaService.class);

	public int insertTBitacora(TBitacoraProsaVO bitacora) throws Exception {
		int idBitacora = 0; 
		TBitacoraProsaDao dao = null;
		try {
			dao = (TBitacoraProsaDao) getBean("TBitacoraProsaDao");
			idBitacora = dao.insertTBitacoraProsa(bitacora);
			
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Comunicados Generales." , e);
			throw new Exception(e);
		}
		return idBitacora;
	}
	

	public int updateTBitacoraProsa(TBitacoraProsaVO bitacora) throws Exception {
		int idBitacora = 0; 
		TBitacoraProsaDao dao = null;
		try {
			dao = (TBitacoraProsaDao) getBean("TBitacoraProsaDao");
			idBitacora = dao.updateTBitacoraProsa(bitacora);
			
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Comunicados Generales." , e);
			throw new Exception(e);
		}
		return idBitacora;
	}
}
