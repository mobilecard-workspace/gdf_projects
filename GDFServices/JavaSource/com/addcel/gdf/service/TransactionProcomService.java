package com.addcel.gdf.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.model.dao.TransactionProcomDao;
import com.addcel.gdf.vo.bitacoras.TransactionProcomVO;
import com.addcel.utils.ibatis.service.AbstractService;

public class TransactionProcomService extends AbstractService {
	private static final Logger log = LoggerFactory.getLogger(TransactionProcomService.class);

	public int insertTransactionProcom(TransactionProcomVO bitacora) {
		int idBitacora = 0; 
		TransactionProcomDao dao = null;
		try {
			dao = (TransactionProcomDao) getBean("TransactionProcomDao");
			idBitacora = dao.insertTransactionProcom(bitacora);
			
		} catch (Exception e) {
			log.error("Ocurrio un error al Insertar en TransactionProcom: {}" , e);
//			throw new Exception(e);
		}
		return idBitacora;
	}
	

}
