package com.addcel.gdf.service;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.axis.AxisFault;
import org.apache.axis.AxisProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.model.dao.TarjetaCirculacionBitacoraDao;
import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.ProsaPagoVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.vo.tarjetaCirculacion.TarjetaCirculacionResponseVO;
import com.addcel.gdf.vo.tarjetaCirculacion.TarjetaCirculacionVO;
import com.addcel.gdf.ws.clientes.tarjetaCirculacion.ServicesPortTypeProxy;
import com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_pregunta_emi;
import com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_respuesta;
import com.addcel.utils.AxisSSLSocketFactory;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.ibatis.service.AbstractService;
import com.addcel.utils.json.me.JSONObject;
import com.google.gson.Gson;

public class TarjetaCirculacionServices extends AbstractService implements Dependency {

	private static final Logger log = LoggerFactory.getLogger(TarjetaCirculacionServices.class);


	@Override
	public String consumeWS(String json) {
		ServicesPortTypeProxy proxy = null;
		TarjetaCirculacionVO tarjetaCirculacionVO = null;
		TarjetaCirculacionResponseVO tarjetaCirculacionResponseVO = null;
		Gson gson = new Gson();

		try {
			
			BigDecimal monto_max = new BigDecimal(new ProcomService().montoMaxProducto(ConstantesGDF.ID_PRODUCTO_TARJETA_CIRCULACION));
			
			tarjetaCirculacionVO = gson.fromJson(json, TarjetaCirculacionVO.class);

			Tipo_pregunta_emi pregunta = new Tipo_pregunta_emi();
			pregunta.setClave(new BigDecimal(36));
			pregunta.setSubconcepto(new BigDecimal(15));
			pregunta.setPlaca(tarjetaCirculacionVO.getPlaca());
			pregunta.setId_marca(tarjetaCirculacionVO.getMarca());
			pregunta.setModelo(tarjetaCirculacionVO.getModelo());
			pregunta.setVencimiento(Calendar.getInstance().get(Calendar.YEAR)+"-12-31");
			pregunta.setUsuario(ConstantesGDF.USUARIO_WS_VEHICULAR);
			pregunta.setPassword(ConstantesGDF.PASSWORD_WS_VEHICULAR);
			
			log.info("usuario: "+pregunta.getUsuario());
			log.info("Password: "+pregunta.getPassword());
			log.info("Clave: "+pregunta.getClave());
			log.info("SubConcepto: "+pregunta.getSubconcepto());
			log.info("Placa: "+pregunta.getPlaca());
			log.info("Id Marca: "+pregunta.getId_marca());
			log.info("Modelo: "+pregunta.getModelo());
			log.info("Vencimiento: "+pregunta.getVencimiento());
			
			proxy = new ServicesPortTypeProxy();
			
			AxisSSLSocketFactory.setKeystorePassword("gdfkeypass");
			AxisSSLSocketFactory.setResourcePathToKeystore("com/addcel/utils/GDFkey.jks");
			AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.utils.AxisSSLSocketFactory");
			
			Tipo_respuesta respuesta = null;
			respuesta = proxy.solicitar_lc_emi(pregunta);
			
			tarjetaCirculacionResponseVO = new TarjetaCirculacionResponseVO();	
			tarjetaCirculacionResponseVO.setConcepto(respuesta.getConcepto());
			tarjetaCirculacionResponseVO.setNombreSubConcepto(respuesta.getNombreSubconcepto());
			tarjetaCirculacionResponseVO.setPlaca(respuesta.getPlaca());
			tarjetaCirculacionResponseVO.setMarca(respuesta.getId_marca());
			tarjetaCirculacionResponseVO.setModelo(respuesta.getModelo());
			tarjetaCirculacionResponseVO.setImporte(respuesta.getImporte());
			tarjetaCirculacionResponseVO.setTotalPago(String.valueOf(respuesta.getImporte()));
			tarjetaCirculacionResponseVO.setVigencia(respuesta.getVigencia());
			tarjetaCirculacionResponseVO.setLinea_captura(respuesta.getLc());
			tarjetaCirculacionResponseVO.setImporteInicial(respuesta.getImporte_inicial());
			tarjetaCirculacionResponseVO.setImporteFinal(respuesta.getImporte_final());
			tarjetaCirculacionResponseVO.setTotalPago(String.valueOf(respuesta.getImporte_final()));
			tarjetaCirculacionResponseVO.setDescuento(respuesta.getDescuento());
			tarjetaCirculacionResponseVO.setError(String.valueOf(respuesta.getError()));
			tarjetaCirculacionResponseVO.setDescError(respuesta.getError_descripcion());		
			tarjetaCirculacionResponseVO.setMonto_maximo_operacion(monto_max);
			
			log.info("Clave: "+respuesta.getClave());
			log.info("Nombre Sub Concepto: "+respuesta.getNombreSubconcepto());
			log.info("Placa: "+respuesta.getPlaca());
			log.info("Marca: "+respuesta.getId_marca());
			log.info("Modelo: "+respuesta.getModelo());
			log.info("Importe: "+respuesta.getImporte());
			log.info("Vigencia: "+respuesta.getVigencia());
			log.info("Linea Captura: "+respuesta.getLc());
			log.info("Error: "+respuesta.getError());
			log.info("Descripcion Error: "+respuesta.getError_descripcion());
			
			if(!"0".equals(tarjetaCirculacionResponseVO.getError())){
				int testError = Integer.valueOf(tarjetaCirculacionResponseVO.getError());
				if(testError > 0){
					json = "{\"error\":\"" + tarjetaCirculacionResponseVO.getDescError() + "\",\"numError\":\""+ tarjetaCirculacionResponseVO.getError()+"\"}";
				}
				else
					json = gson.toJson(tarjetaCirculacionResponseVO);
			}
			else{
				tarjetaCirculacionResponseVO.setNumError("0");
				json = gson.toJson(tarjetaCirculacionResponseVO);
			}
				
			
		} catch (AxisFault aF) {
			json = "{\"error\":" + aF.getMessage() + ",\"numError\":\"A3001\"}";

		} catch (RemoteException rE) {
			json = "{\"error\":" + rE.getMessage() + ",\"numError\":\"A2001\"}";
			
		} catch (Exception E) {
			json = "{\"error\":" + E.getMessage() + ",\"numError\":\"Z1001\"}";
		}
		return json;
	}
	
	@Override
	public HashMap<String, Object> consume3DSecure(String json, String afiliacion) throws Exception{
		TBitacoraService tbitacoraService = new TBitacoraService();
		TBitacoraProsaService tbitacoraProsaService = new TBitacoraProsaService();
		TBitacoraVO tbitacoraVO = null;
		TBitacoraProsaVO tbitacoraProsaVO = null;
		
		TarjetaCirculacionResponseVO tarjetaCirculacionResponseVO = null;
		
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Gson gson = new Gson();
		long id_bitacora = 0;
		try {
			
			tarjetaCirculacionResponseVO = gson.fromJson(json, TarjetaCirculacionResponseVO.class);
			tbitacoraVO = gson.fromJson(json, TBitacoraVO.class);
			tbitacoraProsaVO = gson.fromJson(json, TBitacoraProsaVO.class);
			
			tbitacoraVO.setId_proveedor(16);
			tbitacoraVO.setBit_status(0);
			tbitacoraVO.setBit_codigo_error(0);
			tbitacoraVO.setBit_cargo(String.valueOf(tarjetaCirculacionResponseVO.getImporteInicial()));
			tbitacoraVO.setDestino(tarjetaCirculacionResponseVO.getLinea_captura());
			
			id_bitacora = tbitacoraService.insertTBitacora(tbitacoraVO);
			if(id_bitacora > 0){				
				tarjetaCirculacionResponseVO.setId_bitacora(id_bitacora);
				tarjetaCirculacionResponseVO.setError("0");
				tarjetaCirculacionResponseVO.setAfiliacion(afiliacion);
				tbitacoraVO.setId_bitacora(id_bitacora);
				tbitacoraProsaVO.setId_bitacora(new Long(id_bitacora));
				tbitacoraProsaService.insertTBitacora(tbitacoraProsaVO);
				insertBitacoraTarjetaCirculacion(tarjetaCirculacionResponseVO);
			}
			tarjetaCirculacionResponseVO.setId_producto(ConstantesGDF.ID_PRODUCTO_TARJETA_CIRCULACION);
			resp.put("tbitacoraVO", tbitacoraVO);
			resp.put("guardaVO", tarjetaCirculacionResponseVO);
			
			log.info("Exito TarjetaCirculacionServices.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora());
		} catch (Exception e) {
//			log.error("Error TarjetaCirculacionServices.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora()+"", e);
			log.info(e.getMessage());
			throw new Exception(e);
		}
		return resp;
	}
	
	public int insertBitacoraTarjetaCirculacion(TarjetaCirculacionResponseVO bitacora) throws Exception{
		TarjetaCirculacionBitacoraDao dao = null;
		int idTenencia = 0;
		try {
			dao = (TarjetaCirculacionBitacoraDao) getBean("TarjetaCirculacionDao");
			dao.insertGDFTarjetaCirculacion(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al insert Detalle GDF Tarjeta Circulacion." , e);
			throw new Exception(e);
		}
		
		return idTenencia;
	}
	
	public int updateBitacoraarjetaCirculacion(TarjetaCirculacionResponseVO bitacora) throws Exception{
		TarjetaCirculacionBitacoraDao dao = null;
		int idTenencia = 0;
		try {
			dao = (TarjetaCirculacionBitacoraDao) getBean("TarjetaCirculacionDao");
			dao.updateGDFTarjetaCirculacion(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Tarjeta Circulacion." , e);
			throw new Exception(e);
		}
		
		return idTenencia;
	}
	
	@Override
	public int updateBitacoraDetalle(String autorizacion, long idBitacora, int status, String tipoTDC) throws Exception{
		TarjetaCirculacionBitacoraDao dao = null;
		TarjetaCirculacionResponseVO bitacora; 
		int idAgua = 0;
		try {
			bitacora = new TarjetaCirculacionResponseVO();
			
			bitacora.setId_bitacora(idBitacora);
			bitacora.setNo_autorizacion(autorizacion);
			bitacora.setTipoTarjeta(tipoTDC);
			bitacora.setStatusPago(status);
			dao = (TarjetaCirculacionBitacoraDao) getBean("TarjetaCirculacionDao");
			dao.updateGDFTarjetaCirculacion(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Tarjeta Circulacion ." , e);
			throw new Exception(e);
		}
		
		return idAgua;
	}
	
	@Override
	public String consumeConsultas(String json) throws Exception{
		HashMap<String, String> consulta = new HashMap<String, String>();
		Gson gson = new Gson();
		try {
			JSONObject jsObject = new JSONObject(json);
			
			if (jsObject.has("id_usuario")){
				consulta.put("id_usuario",jsObject.getString("id_usuario"));
				log.error("id_usuario: " + jsObject.getString("id_usuario") );
			}
			if (jsObject.has("mes")){
				consulta.put("mes",jsObject.getString("mes"));
				log.error("mes: " + jsObject.getString("mes") );
			}
			if (jsObject.has("anio")){
				consulta.put("anio",jsObject.getString("anio"));
				log.error("anio: " + jsObject.getString("anio") );
			}
			
			json = "{\"consultaTarjetaCirculacion\":" + gson.toJson(getDetallePagos(consulta)) + "}";
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Consulta Tarjeta Circulacion." , e);
			throw new Exception(e);
		}
		
		return json;
	}
	
	
	
	public List<TarjetaCirculacionResponseVO> getDetallePagos(HashMap<String, String> consulta) throws Exception{
		List<TarjetaCirculacionResponseVO> listaTenencia = new ArrayList<TarjetaCirculacionResponseVO>();
		TarjetaCirculacionBitacoraDao dao = null;
		try {
			dao = (TarjetaCirculacionBitacoraDao) getBean("TarjetaCirculacionDao");
			
			listaTenencia = dao.selectGDFTarjetaCirculacion(consulta);
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los TarjetaCirculacionServices.getDetallePagos." , e);
			throw new Exception(e);
		}
		
		return listaTenencia;
	}
	
	@Override
	public AbstractVO getDetallePagoProcom(HashMap<String, String> consulta) throws Exception{
		List<TarjetaCirculacionResponseVO> listaTarjetaCirculacion = new ArrayList<TarjetaCirculacionResponseVO>();
		AbstractVO abstractVO = null;
		try {
			listaTarjetaCirculacion = getDetallePagos(consulta);
			
			if(listaTarjetaCirculacion != null && listaTarjetaCirculacion.size() > 0){
				abstractVO = (AbstractVO) listaTarjetaCirculacion.get(0);
			}
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los TarjetaCirculacionServices.getDetallePagoProcom." , e);
			throw new Exception(e);
		}
		
		return abstractVO;
	}
	
}
