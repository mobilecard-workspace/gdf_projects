package com.addcel.gdf.service;

import java.math.BigDecimal;
import java.util.Calendar;

import org.apache.axis.AxisProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.ws.clientes.tarjetaCirculacion.ServicesPortTypeProxy;
import com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_catalogo_marcas;
import com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_catalogo_marcas_pregunta;
import com.addcel.utils.AxisSSLSocketFactory;
import com.google.gson.Gson;


public class CatalogoMarcasService {
	private static final Logger log = LoggerFactory.getLogger(CatalogoMarcasService.class);

	public String getMarcas(){
		ServicesPortTypeProxy proxy = null;	
		String jsonResponse = null; 
		Gson gson = new Gson();
		try {
			Tipo_catalogo_marcas_pregunta pregunta = new Tipo_catalogo_marcas_pregunta();
			Tipo_catalogo_marcas marcasArray[] = new Tipo_catalogo_marcas[1];
			Tipo_catalogo_marcas marca = new Tipo_catalogo_marcas();
//			marca.setId_marca("1");
//			marca.setMarca("Volwskwagen");
//			marcasArray[0] = marca;
			proxy = new ServicesPortTypeProxy();
			
			AxisSSLSocketFactory.setKeystorePassword("gdfkeypass");
			AxisSSLSocketFactory.setResourcePathToKeystore("com/addcel/utils/GDFkey.jks");
			AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.utils.AxisSSLSocketFactory");
			
			pregunta.setEjercicio(new BigDecimal(Calendar.YEAR));
			marcasArray = proxy.solicitar_catalogo_marcas(pregunta);
			jsonResponse = gson.toJson(marcasArray);
			
			jsonResponse = "{\"marcas\": "+jsonResponse+", \"numError\":\"0\", \"error\":\"Consulta exitosa.\"}";
		} catch (Exception e) {
			jsonResponse = "{\"numError\":\"2\",\"error\":\"Ocurrio un error al consultar el catalogo de marcas.\"}";
			log.error("Error: {}", e);
		}
		
		return jsonResponse;
	}
	
}
