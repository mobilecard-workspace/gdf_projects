package com.addcel.gdf.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.model.dao.TBitacoraDao;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.utils.ibatis.service.AbstractService;

public class TBitacoraService extends AbstractService {
	private static final Logger log = LoggerFactory.getLogger(TBitacoraService.class);

	public long insertTBitacora(TBitacoraVO bitacora) throws Exception {
		int idBitacora = 0; 
		TBitacoraDao dao = null;
		try {
			dao = (TBitacoraDao) getBean("TBitacoraDao");
			idBitacora = dao.insertTBitacora(bitacora);
			
		} catch (Exception e) {
			log.error("Ocurrio un error en TBitacoraService.insertTBitacora." , e);
			throw new Exception(e);
		}
		return idBitacora;
	}
	

	public int updateTBitacora(TBitacoraVO bitacora) throws Exception {
		int idBitacora = 0; 
		TBitacoraDao dao = null;
		try {
			dao = (TBitacoraDao) getBean("TBitacoraDao");
			idBitacora = dao.updateTBitacora(bitacora);
			
		} catch (Exception e) {
			log.error("Ocurrio un error en TBitacoraService.updateTBitacora." , e);
			throw new Exception(e);
		}
		return idBitacora;
	}
	
	public boolean selectTBitacoraXLC(String lineaCaptura) throws Exception {
		boolean existe = false; 
		TBitacoraDao dao = null;
		TBitacoraVO data = null;
		try {
			dao = (TBitacoraDao) getBean("TBitacoraDao");
			data = dao.selectTBitacoraXLC(lineaCaptura);
			if(data != null && data.getDestino() != null ){
				existe = true;
			}
		} catch (Exception e) {
			log.error("Ocurrio un error en TBitacoraService.updateTBitacora." , e);
			throw new Exception(e);
		}
		return existe;
	}
	
	public String getFechaActual() throws Exception {
		TBitacoraDao dao = null;
		String fecha = null;
		try {
			dao = (TBitacoraDao) getBean("TBitacoraDao");
			fecha = dao.getFechaActual();
			
		} catch (Exception e) {
			log.error("Ocurrio un error en TBitacoraService.getFechaActual." , e);
			throw new Exception(e);
		}
		return fecha;
	}
	
	public int difFechaMin(String fechaToken) throws Exception {
		int seg = 0; 
		TBitacoraDao dao = null;
		try {
			dao = (TBitacoraDao) getBean("TBitacoraDao");
			seg = dao.difFechaMin(fechaToken);
			
		} catch (Exception e) {
			log.error("Ocurrio un error en TBitacoraService.difFechaMin." , e);
			throw new Exception(e);
		}
		return seg;
	}
	
	
}
