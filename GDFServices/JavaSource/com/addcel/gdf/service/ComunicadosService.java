package com.addcel.gdf.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.gdf.model.dao.ComunicadosDao;
import com.addcel.gdf.vo.comunicados.ListaComunicadosVO;
import com.addcel.utils.ibatis.service.AbstractService;

public class ComunicadosService extends AbstractService {
	private static final Logger log = LoggerFactory.getLogger(ComunicadosService.class);

	public ListaComunicadosVO consultaComunicados(int idAplicacion) {
		ListaComunicadosVO comunicados = new ListaComunicadosVO(); 
		ComunicadosDao dao = null;
		try {
			dao = (ComunicadosDao) getBean("ComunicadosDao");
	        comunicados = dao.consultaComunicados(idAplicacion);
			
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Comunicados Generales." , e);
		}
		return comunicados;
	}
}
