package com.addcel.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Formatter;

import org.apache.axis.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utilerias {
	
	private static final Logger logger = LoggerFactory.getLogger(Utilerias.class);
	private static final String patronImp = "########0.00";
//	private static final String patronImpCom = "###,###,##0.00";
	private static DecimalFormat formato;
	private static final String patronNum = "########0";
	private static DecimalFormat formatoNum;
	private static DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	private static final SimpleDateFormat sdf2 = new SimpleDateFormat("ddMMyyyy");
	private static final SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private static final SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
	
	private static final String CERO = "0";

	static{
		simbolos.setDecimalSeparator('.');
		formato = new DecimalFormat(patronImp,simbolos);
		formatoNum = new DecimalFormat(patronNum,simbolos);
	}
	
	public static String formatoFecha(String fecha){		
		String fechafmt="";
		try {
			if(fecha.indexOf("-") > 0){
				fechafmt=sdf2.format(sdf.parse(fecha));
			}else{
				fechafmt=sdf2.format(sdf1.parse(fecha));
			}
		} catch (ParseException e) {			
			logger.error("Formato datos: ", e);
		}				
		return fechafmt;
	}
	
	public static String formatoFechaCorta(String fecha){		
		String fechafmt="";
		try {
			fechafmt=sdf4.format(sdf3.parse(fecha.substring(0,fecha.length() -5)));
		} catch (ParseException e) {			
			logger.error("Formato datos: ", e);
		}				
		return fechafmt;
	}
	
	public static String formatoImporte(BigDecimal numDecimal){
		if(numDecimal == null){
			return "0.00";
		}
		return formatoImporte(numDecimal.doubleValue());
	}
	
	public static String formatoImporte(String numString){
		String total = null;
		if(!StringUtils.isEmpty(numString)){
			total = formatoImporte(Double.parseDouble(numString));
		}
		return total;
	}
	
	public static String formatoImporte(double numDouble){
		String total = null;
		try{
			total = formato.format(numDouble);
		}catch(Exception e){
			logger.error("Error en formatoImporte: "+ e.getMessage());
		}
		return total;
	}
	
	public static String formatoNumero(BigDecimal totalPago){
		return formatoNumero(totalPago.doubleValue());
	}
	
	public static String formatoNumero(String totalPago){
		String total = null;
		if(!StringUtils.isEmpty(totalPago)){
			total = formatoNumero(Double.parseDouble(totalPago));
		}
		return total;
	}
	
	public static String formatoNumero(double totalPago){
		String total = null;
		try{
			total = formatoNum.format(totalPago);
		}catch(Exception e){
			logger.error("Error en formatoImporte: "+ e.getMessage());
		}
		return total;
	}
	
	public static String formatoImporteProsa(String totalFmtProsa){
		String total = null;
		Formatter fmt = null;
		if(!StringUtils.isEmpty(totalFmtProsa)){
			fmt = new Formatter();
			total = fmt.format("%1$015d", Integer.parseInt(totalFmtProsa)).toString();
			fmt.close();
		}
		return total;
	}
	
	public static String formatoMontoProsa(String monto){
		String varTotal="000";
		if(!StringUtils.isEmpty(monto)){
			monto = formatoImporte(monto);
			varTotal = monto.replace(".", "");
			logger.info("Monto formatoMontoProsa: "+varTotal);
		}
		return varTotal;		
	}
	
	public static String formatAddCeros(String cadena, int tamaño, boolean leftRigth){
		StringBuffer respuesta = new StringBuffer();
		int size = 0;
		try{
			size = tamaño - cadena.length(); 
			for(int i = 0; i < size; i++){
				respuesta.append(CERO);
			}
			
			if(leftRigth){
				cadena = respuesta.toString() + cadena;
			}else{
				cadena += respuesta.toString();
			}
				
		}catch(Exception e){
			logger.error("Error en formatAddCeros: "+ e.getMessage());
		}
		
		return cadena;
	}
	
	public static boolean validaEntero(String numString){
		boolean numero = true;
		try{
			Integer.parseInt(numString);
		}catch(NumberFormatException e){
			numero = false;
		}
		return numero;
	}
	
	
}
