package com.addcel.utils;

import java.text.SimpleDateFormat;

public class ConstantesGDF {
	
	public static final String URL_AUT_PROSA = "https://localhost:8443/ProsaWeb/ProsaAuth?";
	public static final String URL_MAIL = "http://50.57.192.214:8080/MailSenderAddcel/envia-recibo-gdf/";
	public static final String URL_MAIL_GENERA = "http://50.57.192.214:8080/MailSenderAddcel/genera-recibo-gdf/";
	
	private static final SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private static final SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
	
	public static final int idProd_Nomina = 1;
	public static final int idProd_Tenencia = 2;
	public static final int idProd_Infraccion = 3;
	public static final int idProd_Predial = 4;
	public static final int idProd_Agua = 5;
	public static final int idProd_PredialVencido= 6; 
	public static final int ID_PRODUCTO_TARJETA_CIRCULACION = 7;
	public static final int ID_PRODUCTO_LICENCIA_CONDUCTOR = 8;
	public static final int ID_PRODUCTO_LICENCIA_PERMANENTE = 9;
	public static final int ID_PAGO_LINEA_CAPTURA = 10;
	
	public static final String USUARIO_WS_VEHICULAR = "kioskos";
	public static final String PASSWORD_WS_VEHICULAR = "bf2579b6ccd26082aaacf2b8e0774ff5";
	
	public static final int LLAVE_SECRETA = 12357113;
	public static final int MODULO_30 = 24300000;
	
	public static final String lTime = "22:57:00";
	public static final String uTime = "23:48:00";
	public static final String dayFlag = "0";
}
