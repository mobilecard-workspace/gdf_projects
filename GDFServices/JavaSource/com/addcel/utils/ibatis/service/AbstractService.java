package com.addcel.utils.ibatis.service;
/**
 * 
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.addcel.utils.ibatis.core.SessionApplicationContextBuilderImpl;


/**
 * @author ELopez
 *
 */
public abstract class AbstractService {
	private static final Logger log = LoggerFactory.getLogger(AbstractService.class);

	public Object getBean(String bean) throws Exception{
		ClassPathXmlApplicationContext ctxt = null;
		Object obj = null;
		try{
			ctxt = getSqlSessionInstance();
			obj =  ctxt.getBean(bean);
		}catch(Exception e){
			log.error("Ocurrio un error al obtener el DAOBean.", e);
			throw new Exception(e);
		}
		return obj;
	}
	
	public ClassPathXmlApplicationContext getSqlSessionInstance(){
		return SessionApplicationContextBuilderImpl.getApplicationContexInstance();
	}

}
