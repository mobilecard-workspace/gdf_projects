package com.addcel.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.GeneralSecurityException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UtilsService {
	private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	
	public String peticionHttpsUrlParams(String urlServicio, String parametros) {
		InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpURLConnection conn = null;
		URL url = null;
		String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        
		HostnameVerifier hv = new HostnameVerifier() {
			public boolean verify(String urlHostName, SSLSession session) {
				logger.debug("Warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost());
				return true;
			}
		};	        
	    
        try {
        	trustAllHttpsCertificates();
    	    HttpsURLConnection.setDefaultHostnameVerifier(hv);
            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                if (parametros != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write(parametros);
                    writer.flush();
                    writer.close();
                    os.flush();
                    os.close();
                    conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
//                logger.debug("Respuesta notificacion: {}", sbf);
            }
        } catch (GeneralSecurityException e) {
        	logger.info("Error Al generar certificados: {}", e);
        }catch (MalformedURLException e) {
        	logger.info("error al conectar con la URL: "+urlServicio + "\n {}", e);
        }catch (ProtocolException e) {
        	logger.info("Error al enviar datos: {}", e);
		} catch (IOException e) {
			logger.info("Error al algo: {}", e);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
        }
        return sbf.toString(); 
	}
	
	public static class miTM implements javax.net.ssl.TrustManager,
			javax.net.ssl.X509TrustManager {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public boolean isServerTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public boolean isClientTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public void checkServerTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}

		public void checkClientTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}
	}

	private static void trustAllHttpsCertificates() throws Exception {
		// Create a trust manager that does not validate certificate chains:
		javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(
		sc.getSocketFactory());

	}
}
