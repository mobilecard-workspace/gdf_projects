<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width"/>
	<title>Error</title>
	    <style type="text/css">
        root { 
            display: block;
        }

        html
        {
            font-family: arial;
            font-size: 12px;
            font-weight: bold;
            color: black;
            background-color: white;
        }

        td{
            font-family: arial;
            font-size: 12px;
        }

        .title
        {
            font-family: arial;
            font-size: 12px;

        }

        .title2
        {
            font-family: arial;
            font-size: 12px;
        }

        input{
            width: 240px;
            height: 35px;
            font-family: arial;
            font-size: 14px;    
        }                

        .styled-select select {
            background: transparent;
            width: 268px;
            padding: 5px;
            font-size: 16px;
            border: 1px solid #ccc;
            height: 34px;
        }        
    </style>       
</head>
<body>
	
	<table cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="2" align="center">
				<h2>Ocurrio un error al procesar la transacción</h2>
			</td>
		</tr>
		<tr>
			<td>Error:</td>
		</tr>
		<tr>
			<td>${descError}</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<br><br>
			</td>
		</tr>
		<tr>
			<td colspan="2">Presione el Boton Atras para terminar su transacci&oacute;n.</td>
		</tr>
	</table>
</body>
</html>