<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width"/>
	<title>Respuesta GDF Pago</title>
	    <style type="text/css">
        root { 
            display: block;
        }

        html
        {
            font-family: arial;
            font-size: 12px;
            font-weight: bold;
            color: black;
            background-color: white;
        }

        td{
            font-family: arial;
            font-size: 12px;
        }

        .title
        {
            font-family: arial;
            font-size: 12px;

        }

        .title2
        {
            font-family: arial;
            font-size: 12px;
        }

        input{
            width: 240px;
            height: 35px;
            font-family: arial;
            font-size: 14px;    
        }                

        .styled-select select {
            background: transparent;
            width: 268px;
            padding: 5px;
            font-size: 16px;
            border: 1px solid #ccc;
            height: 34px;
        }        
    </style>       
	
</head>
<body>

	<input type="hidden" name="EM_Response" value="${prosa.EM_Response}" /> 
	<table cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="2" align="center">
				<h1>Transaccion exitosa</h1>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				Su pago fue procesado correctamente.
				</br></br>
			</td>
		</tr>
		<tr>
			<td>Respuesta:</td>
			<td>${respuesta}</td>
		</tr>
		<tr>
			<td>Monto:</td>
			<td>${monto}</td>
		</tr>
		<tr>
			<td>N&uacute;mero Autorización:</td>
			<td>${autorizacion}</td>
		</tr>
		<tr>
			<td>N&uacute;mero Referencia MC:</td>
			<td>${referencia}</td>
		</tr>
		<tr>
			<td colspan="2">
					</br></br>
					Se ha enviado un correo a la dirección registrada en
					Mobilecard con los detalles del su pago.
					</br></br>
			</td>
		</tr>
		<tr>
			<td colspan="2">Presione el Boton Atras para terminar su transacci&oacute;n.</td>
		</tr>
	</table>
</body>
</html>