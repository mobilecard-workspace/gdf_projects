<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Upload Result</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<%
	response.setHeader("Expires", "0");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", -1);
%>

<style type="text/css">
* {
	padding: 0;
	margin: 0;
}

body {
	font-size: 62.5%;
	background-color: rgb(255, 255, 255);
	font-family: verdana, arial, sans-serif;
}

.page-container {
	width: 1458px;
	margin: 0px auto;
	margin-top: 10px;
	margin-bottom: 10px;
	border: solid 1px rgb(150, 150, 150);
	font-size: 1.0em;
}
/* HEADER */
.header {
	width: 1458px;
	height: 50px;
	font-family: "trebuchet ms", arial, sans-serif;
	overflow: visible !important /*Firefox*/;
	overflow: hidden /*IE6*/;
}

.main-content {
	display: inline; /*Fix IE floating margin bug*/;
	float: left;
	width: 1400px;
	margin: 0 0 0 30px;
	overflow: visible !important /*Firefox*/;
	overflow: hidden /*IE6*/;
	margin-bottom: 10px !important /*Non-IE6*/;
	margin-bottom: 5px /*IE6*/;
}
/* MAIN CONTENT */
.main-content h1.pagetitle {
	margin: 0 0 0.4em 0;
	padding: 0 0 2px 0;
	font-family: Verdana, Geneva, sans-serif;
	color: #000000;
	font-weight: bold;
	font-size: 180%;
}

.main-content p {
	margin: 0 0 1.0em 0;
	line-height: 1.5em;
	font-size: 120%;
	text-align: justify;
	color: #000000;
}

.main-content table {
	clear: both;
	/*width: 100%;*/
	margin: 0 0 0 0;
	table-layout: fixed;
	border-collapse: collapse;
	empty-cells: show;
	background-color: rgb(233, 232, 244);
	text-align: justify;
}

.main-content table td {
	height: 2.5em;
	padding: 2px 5px 2px 5px;
	border-left: solid 2px rgb(255, 255, 255);
	border-right: solid 2px rgb(255, 255, 255);
	border-top: solid 2px rgb(255, 255, 255);
	border-bottom: solid 2px rgb(255, 255, 255);
	background-color: rgb(225, 225, 225);
	text-align: center;
	font-weight: normal;
	color: #000000;
	font-size: 11.5px;
}
/*  FOOTER SECTION  */
.footer {
	clear: both;
	width: 1458px;
	height: 7em;
	padding: 1.1em 0 0;
	background: rgb(225, 225, 225);
	font-size: 1.0em;
	overflow: visible !important /*Firefox*/;
	overflow: hidden /*IE6*/;
}

.footer p {
	line-height: 1.3em;
	text-align: center;
	color: rgb(125, 125, 125);
	font-weight: bold;
	font-size: 104%;
}
</style>
<script>
	/**
	 * Crea una selección que engloba el nodo indicado
	 *
	 * @param {Node} nodo Elemento de la página
	 * @version 2013-11-27
	 */
	function seleccionarNodo(nodo) {
		var rango, seleccion;

		// Creamos un rango y le asignamos el nodo
		if (document.createRange) { // W3C
			rango = document.createRange();
			rango.selectNode(nodo);
		} else if (document.body.createTextRange) { // IE<9
			rango = document.body.createTextRange();
			rango.moveToElementText(nodo);
		} else {
			return;
		}
		// Seleccionamos el rango
		if (window.getSelection) { // W3C
			seleccion = window.getSelection();
			seleccion.removeAllRanges(); // Eliminar selección actual
			seleccion.addRange(rango);
		} else if (rango.moveToElementText) { // IE<9
			rango.select();
		}
	}
</script>

</head>
<body>
	<div class="page-container">
		<div class="header">
			<img src="../Repository/Mobilecard_header.png" alt="" />
		</div>
		<br>

		<div class="main-content">
			<h1 class="pagetitle">Reporte generado </h1>
			<h2>${message}</h2>
			<br><br>
            <p>
                Haz doble clic sobre la tabla para seleccionar    Ó    <input type="submit"  width="200px" height="40px" value="  Seleccionar Texto  " onclick="seleccionarNodo(document.getElementById('respuesta'))"></input> 
                <br>
                Después se puede copiar la selección y pegar en una Hoja de cálculo (Excel). 
            </p>
            <br>
			<table align="center" cellpadding="2" cellspacing="2" id="respuesta" 
				ondblclick="seleccionarNodo(document.getElementById('respuesta'))">
				<tr>
					<td><strong>ID Bitacora</strong></td>
					<td><strong>Linea Captura</strong></td>
					<td><strong>Status</strong></td>
				</tr>
				${data}
			</table>
		</div>
		<div class="footer">
			<p>MobileCard - ABC Capital © 2014 | Todos los derechos
				reservados.</p>
			<p>Powered by Addcel.</p>
		</div>
	</div>
</body>
</html>