<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Recibo Tenencia</title>
<style type="text/css">
@page {
	size: 21.59cm 27.94cm; /* width height */
	margin: 2cm 1.53cm 2cm 1.53cm;
}

#contenido {
	position: absolute;
	margin: 0 auto;
	width: 18.5cm;
	height: 20cm;
	font-family: serif;
}

.titulo-18pt {
	font-size: 18pt;
	font-weight: bold;
	margin: 0px;
	height: 1cm;
}

.titulo-16pt {
	font-size: 16pt;
	font-weight: bold;
	height: 1cm;
}

.titulo-10pt {
	font-size: 10pt;
	width: 200px;
	font-weight: bold;
	margin-left: 0.5cm;
}

#encabezado {
	position: absolute;
	margin-top: 0px;
	height: 6cm;
	width: 100%;
}

#datos1 {
	position: absolute;
	height: 5cm;
	border: 1px solid;
	width: 9cm;
	margin-top: 6cm;
}

#datos2 {
	position: absolute;
	height: 5cm;
	border: 1px solid;
	width: 9cm;
	margin-top: 6cm;
	margin-left: 9.5cm;
}

#datos3 {
	position: absolute;
	height: 3cm;
	border: 1px solid;
	width: 100%;
	margin-top: 11.5cm;
}

#datos4 {
	position: absolute;
	height: 1.5cm;
	border: 1px solid;
	width: 9cm;
	margin-top: 15cm;
	margin-left: 4.75cm;
	text-align: center;
}

#datos5 {
	position: absolute;
	height: 3cm;
	background-color: silver;
	width: 11cm;
	margin-top: 17cm;
}

#datos6 {
	position: absolute;
	height: 1.5cm;
	border: 1px solid;
	width: 7cm;
	margin-top: 18.5cm;
	margin-left: 11.5cm;
	text-align: center;
}

.titulo {
	height: 0.5cm;
	background-color: rgb(40%, 40%, 40%);
	color: white;
	font-weight: bold;
	font-size: 9pt;
	text-align: center;
	line-height: 0.5cm;
}

#logodf {
	position: absolute;
	height: 3.5cm;
	width: 3.5cm;
}

#logosefin {
	position: absolute;
	height: 3.0cm;
	width: 3.0cm;
	margin-left: 4cm;
	margin-top: 0.5cm;
}

#logohsbc {
	height: 2cm;
	width: 4.5cm;
	float: right;
	margin-top: 0.3cm;
}

#texto1 {
	position: absolute;
	font-size: 10pt;
	margin-left: 7.5cm;
	margin-top: 1cm;
	width: 6cm;
	height: 0.2cm;
}

#texto2 {
	position: absolute;
	font-size: 10pt;
	font-weight: bold;
	margin-left: 14cm;
	margin-top: 0cm;
	width: 3cm;
	height: 0.2cm;
}

#titulo-principal {
	position: absolute;
	margin-top: 3.5cm;
	text-align: center;
	width: 100%;
	font-weight: bold;
}

#certificado-digital {
	font-size: 22pt;
	text-align: center;
	font-family: monospace;
	font-weight: bold;
}

#logo-escudo {
	float: right;
	margin-right: 5px;
	width: 1.4cm;
}

table.table-sin-borde {
	border-collapse: collapse;
	border: none;
	margin-left: 10px;
	font-weight: bold;
	font-size: 10pt;
}

.titulo-dos {
	height: 0.5cm;
	font-weight: bold;
	font-size: 9pt;
	text-align: center;
	line-height: 0.5cm;
}

.titulo-14pt {
	font-size: 14pt;
	font-weight: bold;
}

td.td-sin-borde {
	padding: 0;
	height: 0.6cm;
}
</style>
</head>
<body>
	<div id="contenido">
		<div id="encabezado">
			<img id="logodf"
				src="http://localhost:8080/MailSenderAddcel/resources/images/gdf/logoDfBn.png"
				alt="logodf" /> <img id="logosefin"
				src="http://localhost:8080/MailSenderAddcel/resources/images/gdf/logoSEFIN.png"
				alt="logosefin" />
			<div id="texto1">
				SECRETARÍA DE <b>FINANZAS TESORERÍA</b>
			</div>
			<div>
				<div id="texto2"></div>
				<div>
					<img id="logohsbc"
						src="http://localhost:8080/MailSenderAddcel/resources/images/gdf/logo_inbursa.png"
						alt="logohsbc" />
				</div>
			</div>
			<div id="titulo-principal">
				<div>
					<span class="titulo-18pt">RECIBO DE PAGO A LA TESORERÍA</span>
				</div>
				<div>
					<span class="titulo-18pt">${obj.concepto}</span>
				</div>
				<div>
					<span class="titulo-16pt">Realizado por Tesorería Móvil</span>
				</div>
			</div>
		</div>
		<div id="datos1">
			<div class="titulo">DATOS ADMINISTRATIVOS</div>
			<table class="table-sin-borde">
				<tbody>
					<#-- Datos para el recibo de nomina --> <#if obj.id_producto==1>
					<tr>
						<td class="td-sin-borde">RFC:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.rfc ! ''}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Periodo de Pago:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.anio_pago ! ''}-${obj.mes_pago
							! ''}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Tipo de Declaración:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.tipo_declaracion ! ''}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">No. de Trabajadores:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.num_trabajadores ! ''}</td>
					</tr>
					</#if> <#-- Datos para del recibo de tenencia --> <#if
					obj.id_producto==2>
					<tr>
						<td class="td-sin-borde">Placa:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.placa ! ''}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Ej. Fiscal:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.ejercicio ! ''}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Modelo:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.modelo ! ''}</td>
					</tr>
					</#if> <#-- Datos para el recibo de infracción --> <#if
					obj.id_producto==3>
					<tr>
						<td class="td-sin-borde">Folio:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.folio ! ''}</td>
					</tr>
					</#if> <#-- Datos para el recibo de predial --> <#if
					obj.id_producto==4>
					<tr>
						<td class="td-sin-borde">Cuenta Predial:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.cuentaP ! ''}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Periodo:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.bimestre ! ''}</td>
					</tr>
					</#if> <#-- Datos para el recibo de agua --> <#if
					obj.id_producto==5>
					<tr>
						<td class="td-sin-borde">Cuenta Agua:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.cuenta ! ''}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Año:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.vanio ! ''}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Bimestre:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.vbimestre ! ''}</td>
					</tr>
					</#if> <#-- Datos para el recibo de predial vencido --> <#if
					obj.id_producto==6>
					<tr>
						<td class="td-sin-borde">Cuenta Predial:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.cuenta ! ''}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Periodos:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.bimestres ! ''}</td>
					</tr>
					</#if>
					<#-- Datos para el recibo de tarjeta de circulacion --> 
					<#if obj.id_producto==7>
						<tr>
							<td class="td-sin-borde">Placa:</td>
							<td width="10px"></td>
							<td class="td-sin-borde">${obj.placa ! ''}</td>
						</tr>
						<tr>
							<td class="td-sin-borde">Modelo:</td>
							<td width="10px"></td>
							<td class="td-sin-borde">${obj.modelo?string("0000") ! ''}</td>
						</tr>
					</#if>
					<#-- Datos para el recibo de licencia --> 
					<#if obj.id_producto==8>
						<tr>
							<td class="td-sin-borde">RFC:</td>
							<td width="10px"></td>
							<td class="td-sin-borde">${obj.rfc ! ''}</td>
						</tr>
					</#if>
					<#-- Datos para el recibo de licencia Permanente --> 
					<#if obj.id_producto==9>
						<tr>
							<td class="td-sin-borde">RFC:</td>
							<td width="10px"></td>
							<td class="td-sin-borde">${obj.rfc ! ''}</td>
						</tr>
					</#if>
				</tbody>
			</table>
		</div>
		<div id="datos2">
			<div class="titulo">LIQUIDACIÓN DE PAGO</div>
			<table class="table-sin-borde">
				<tbody>
					<#-- <#-- Si existe una propiedad del tipo de recibo se asume que
					se trata de un recibo de ese tipo por lo que no se revisan los
					demas parametros. --> <#-- Datos para el recibo de nomina --> <#if
					obj.id_producto==1> <#if obj.remuneraciones != '0.00'>
					<tr>
						<td class="td-sin-borde">Rem Gravadas:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.remuneraciones ! ''}</td>
					</tr>
					</#if> <#if obj.impuesto != '0.00'>
					<tr>
						<td class="td-sin-borde">Impuesto:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.impuesto ! ''}</td>
					</tr>
					</#if> <#if obj.impuesto_actualizado?has_content> <#if
					obj.impuesto_actualizado != '0.00'>
					<tr>
						<td class="td-sin-borde">Impuesto Actualizado:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.impuesto_actualizado ! ''}</td>
					</tr>
					</#if> </#if> <#if obj.recargos != '0.00'>
					<tr>
						<td class="td-sin-borde">Recargos:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.recargos ! ''}</td>
					</tr>
					</#if> </#if> <#-- Datos para el recibo de tenencia --> <#if
					obj.id_producto==2> <#if obj.tenencia != '0.00'>
					<tr>
						<td class="td-sin-borde">Tenencia:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.tenencia ! ''}</td>
					</tr>
					</#if><#if obj.tenSubsidio?has_content> <#if obj.tenSubsidio !=
					'0.00'>
					<tr>
						<td class="td-sin-borde">Subsidio Tenencia:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">-${obj.tenSubsidio ! ''}</td>
					</tr>
					</#if> </#if><#if obj.tenActualizacion?has_content><#if
					obj.tenActualizacion != '0.00'>
					<tr>
						<td class="td-sin-borde">Actualización Tenencia:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.tenActualizacion ! ''}</td>
					</tr>
					</#if></#if><#if obj.tenRecargo?has_content><#if obj.tenRecargo !=
					'0.00'>
					<tr>
						<td class="td-sin-borde">Recargos Tenencia:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.tenRecargo ! ''}</td>
					</tr>
					</#if></#if><#if obj.derechos != '0.00'>
					<tr>
						<td class="td-sin-borde">Derechos:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.derechos ! ''}</td>
					</tr>
					</#if><#if obj.derActualizacion?has_content><#if
					obj.derActualizacion != '0.00'>
					<tr>
						<td class="td-sin-borde">Actualización Derechos:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.derActualizacion ! ''}</td>
					</tr>
					</#if></#if><#if obj.derRecargo?has_content><#if obj.derRecargo !=
					'0.00'>
					<tr>
						<td class="td-sin-borde">Recargos Derechos:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.derRecargo ! ''}</td>
					</tr>
					</#if></#if><#if obj.condRecargos?has_content><#if obj.condRecargos
					!= '0.00'>
					<tr>
						<td class="td-sin-borde">Condonacion Recargos:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">-${obj.condRecargos ! ''}</td>
					</tr>
					</#if></#if></#if> <#-- Datos para el recibo de infracción --> <#if
					obj.id_producto==3> <#if obj.importe != '0.00'>
					<tr>
						<td class="td-sin-borde">Multa:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.importe ! ''}</td>
					</tr>
					</#if> <#if obj.dias_multa != '0.00'>
					<tr>
						<td class="td-sin-borde">Dias multa:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.dias_multa ! ''}</td>
					</tr>
					</#if> <#if obj.actualizacion?has_content><#if obj.actualizacion !=
					'0.00'>
					<tr>
						<td class="td-sin-borde">Actualización:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.actualizacion ! ''}</td>
					</tr>
					</#if></#if><#if obj.recargos != '0.00'>
					<tr>
						<td class="td-sin-borde">Recargos:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.recargos ! ''}</td>
					</tr>
					</#if> </#if> <#-- Datos para el recibo de predial --> <#if
					obj.id_producto==4> <#if obj.importe != '0.00'>
					<tr>
						<td class="td-sin-borde">Impuesto:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.importe ! ''}</td>
					</tr>
					</#if> <#if obj.reduccion != '0.00'>
					<tr>
						<td class="td-sin-borde">Reducción:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.reduccion ! ''}</td>
					</tr>
					</#if> </#if> <#-- Datos para el recibo de agua --> <#if
					obj.id_producto==5> <#if obj.vderecho != '0.00'>
					<tr>
						<td class="td-sin-borde">Derecho:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.vderecho ! ''}</td>
					</tr>
					</#if> <#if obj.viva != '0.00'>
					<tr>
						<td class="td-sin-borde">IVA:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.viva ! ''}</td>
					</tr>
					</#if> <#if obj.totalPago != '0.00'>
					<tr>
						<td class="td-sin-borde">Total a pagar:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">${obj.totalPago ! ''}</td>
					</tr>
					</#if></#if><#-- Datos para el recibo de predial vencido--> 
					<#if obj.id_producto==6> 
						<#if obj.total_cuenta != '0.00'>
					<tr>
						<td class="td-sin-borde">Importe:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">$ ${obj.totalPago ! ''}</td>
					</tr>
					</#if>
					<tr>
						<td class="td-sin-borde">Detalle:</td>
						<td width="10px"></td>
						<td class="td-sin-borde">Consultar detalle de pago en:
							http://www.finanzas.df.gob.mx</td>
					</tr>
					</#if>
					<#-- Datos para el recibo de tarjeta de circulacion --> 
					<#if obj.id_producto==7>
						<#if obj.importeFinal != 0.00>
							<tr>
								<td class="td-sin-borde">Importe:</td>
								<td width="10px"></td>
								<td class="td-sin-borde">${obj.importeFinal?string.currency ! ''}</td>
							</tr>
						</#if>
					</#if>
					<#-- Datos para el recibo de licencia --> 
					<#if obj.id_producto==8>
						<#if obj.importeFinal != 0.00>
							<tr>
								<td class="td-sin-borde">Importe:</td>
								<td width="10px"></td>
								<td class="td-sin-borde">${obj.importeFinal?string.currency ! ''}</td>
							</tr>
						</#if>
					</#if>
					<#-- Datos para el recibo de licencia permanente --> 
					<#if obj.id_producto==9>
						<#if obj.importeFinal != 0.00>
							<tr>
								<td class="td-sin-borde">Importe:</td>
								<td width="10px"></td>
								<td class="td-sin-borde">${obj.importeFinal?string.currency ! ''}</td>
							</tr>
						</#if>
					</#if>
				</tbody>
			</table>
		</div>
		<div id="datos3">
			<div class="titulo">DATOS DEL PAGO</div>
			<table class="table-sin-borde">
				<tbody>
					<tr>
						<td cliass="td-sin-borde">Adquiriente:</td>
						<td width="10px"></td>
						<td>Addcel S.A. de C.V.</td>
					</tr>
					<tr>
						<td class="td-sin-borde">No. de Autorización:</td>
						<td width="10px"></td>
						<td>${obj.no_autorizacion ! ''}</td>
					</tr>
					<tr>
						<td class="td-sin-borde">Fecha de pago:</td>
						<td width="10px"></td>
						<td>${obj.fechaPago ! ''}</td>
					</tr>
					<#if obj.ultimos4TDC?has_content>
					<tr>
						<td class="td-sin-borde">Tarjeta:</td>
						<td width="10px"></td>
						<td>${obj.ultimos4TDC ! ''}</td>
					</tr>
					</#if>
				</tbody>
			</table>
		</div>
		<div id="datos4">
			<div class="titulo">LÍNEA DE CAPTURA</div>
			<div class="titulo-14pt">${obj.linea_captura ! ''}</div>
		</div>
		<div id="datos5">
			<div class="titulo-dos">CERTIFICACIÓN DIGITAL DE LA TESORERÍA</div>
			<div id="certificado-digital">${obj.certificado ! ''}</div>
			<img id="logo-escudo"
				src="http://localhost:8080/MailSenderAddcel/resources/images/gdf/logoGdfEscudo.png"
				alt="logoescudo" />
		</div>
		<div id="datos6">
			<div class="titulo-dos">TOTAL PAGADO</div>
			<div style="text-decoration: underline;" class="titulo-14pt">$
				${obj.totalPago ! ''}</div>
		</div>
	</div>
</body>
</html>