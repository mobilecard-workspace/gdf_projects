package com.ironbit.mega.widget.contextmenu;

public interface OnContextMenuIconItemSelectedListener {
	public void onContextMenuIconItemSelected(int itemId);
}