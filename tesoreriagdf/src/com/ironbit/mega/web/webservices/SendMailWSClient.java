/**
 * 
 */
package com.ironbit.mega.web.webservices;

import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

/**
 * @author mans
 *
 */
public class SendMailWSClient extends WebServiceClient {
	
	protected String destino;
	protected String subject;
	protected String mensaje;
	protected String tipo;
	
	public SendMailWSClient(Activity ctx){
		super(ctx);
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public String getTipo(){
		return tipo;
	}
	
	public void setTipo(String tipo){
		this.tipo = tipo;
	}

	/* (non-Javadoc)
	 * @see com.ironbit.mega.web.webservices.WebServiceClient#getWebServiceUrl()
	 */
	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_SEND_MAIL;
	}

	/* (non-Javadoc)
	 * @see com.ironbit.mega.web.webservices.WebServiceClient#getHiloId()
	 */
	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_ENVIA_MAIL;
	}
	
	public WebServiceClient execute(final OnResponseJSONReceivedListener listener){
		addGetParameter("para", getDestino());
		addGetParameter("tipo", getTipo());
		
		if(!"".equals(getSubject())){
			addGetParameter("sub", getSubject());
		}
		
		if( getTipo( ) == null || "".equals(getTipo())){
			addGetParameter("msg", getMensaje());
		}
		
		return executeClean(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if(listener != null){
					try{
						System.out.println(":::EXITO EN ENVIO DE MAIL:::");
					}catch(Exception e){
						Sys.log(e);
					}
					
					listener.onResponseJSONReceived(jsonResponse);
				}
				
			}
		});
	}

}
