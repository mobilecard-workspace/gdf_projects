package com.ironbit.mega.web.webservices;


import java.util.Hashtable;

import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.conexion.ConexionHttp;
import com.ironbit.mega.conexion.http.HttpConexion.Charset;
import com.ironbit.mega.conexion.http.OnRespuestaHttpRecibidaListener;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.system.crypto.Crypto;
import com.ironbit.mega.web.util.Url;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;
import com.ironbit.mega.web.webservices.events.OnResponseReceivedListener;

public abstract class WebServiceClient {
	protected DataFormat formatResponse = DataFormat.json;
	protected final Hashtable<String, String> postParameters = new Hashtable<String, String>();
	protected final Hashtable<String, String> getParameters = new Hashtable<String, String>();
	protected Activity ctx = null;
	
	
	public WebServiceClient(Activity ctx){
		this.ctx = ctx;
	}
	
	protected abstract String getWebServiceUrl();
	
	
	protected abstract ConexionHttp.Hilo getHiloId();
	
	
	public WebServiceClient addPostParameters(Hashtable<String, String> postParameters){
		this.postParameters.putAll(postParameters);
		return this;
	}
	
	
	public WebServiceClient addPostParameter(String name, JSONObject jsonValue){
		return addPostParameter(name, jsonValue.toString());
	}
	
	
	public WebServiceClient addPostParameter(String name, String value){
		postParameters.put(name, value);
		return this;
	}
	
	
	public WebServiceClient addGetParameters(Hashtable<String, String> getParameters){
		this.getParameters.putAll(getParameters);
		return this;
	}
	
	
	public WebServiceClient addGetParameter(String name, JSONObject jsonValue){
		return addGetParameter(name, jsonValue.toString());
	}
	
	
	public WebServiceClient addGetParameter(String name, String value){
		getParameters.put(name, value);
		return this;
	}
	
	
	private String getWebServiceUrlWithGetParameters(){
		String url = null;
		
		try{
			url = Url.setUrl(getWebServiceUrl()).agregarParametros(getParameters).toString();
		}catch(Exception e){
			url = getWebServiceUrl();
			Sys.log(e);
		}
		
		return url;
	}
	
	public boolean isPostRequest(){
		return (postParameters.size() > 0);
	}
	
	
	public WebServiceClient cancel(){
		ConexionHttp.cancelarConexionAsync(getHiloId());
		return this;
	}
	
	
	protected WebServiceClient execute(final OnResponseReceivedListener listener){
		try{
			OnRespuestaHttpRecibidaListener onRespuestaHttpRecibidaListener = new OnRespuestaHttpRecibidaListener(){
				public void onRespuestaHttpRecibida(int idRespuesta, String respuesta) {
					if (listener != null){
						/**
						 * @NOTE: por default todas los HttpResponces se están desencriptando 
						 */
						respuesta = Crypto.aesDecrypt(Sys.getKey(), respuesta);
						
						Sys.log("HttpResponse", respuesta);
						listener.onResponseReceived(respuesta);
					}
				}
			};
			
			if (isPostRequest()){
				ConexionHttp.getInstancia().postAsync(
					onRespuestaHttpRecibidaListener, 
					getHiloId(), 
					getWebServiceUrlWithGetParameters(), 
					postParameters,
					Charset.ISO_8859_1,
					Charset.ISO_8859_1
				);
			}else{
				ConexionHttp.getInstancia().getAsync(
					onRespuestaHttpRecibidaListener, 
					getHiloId(), 
					getWebServiceUrlWithGetParameters(),
					Charset.ISO_8859_1,
					Charset.ISO_8859_1
				);
			}
		}catch(Exception e){
			Sys.log(e);
		}
		
		return this;
	}
	
	
	protected WebServiceClient execute(final OnResponseJSONReceivedListener listener){
		return execute(new OnResponseReceivedListener(){
			public void onResponseReceived(String response) {
				JSONObject jsonObj = null;
				
				try{
					jsonObj = new JSONObject(response);
				}catch(Exception e){
					jsonObj = new JSONObject();
					Sys.log(e);
				}
				
				if (listener != null){
					listener.onResponseJSONReceived(jsonObj);
				}
			}
		});
	}
}