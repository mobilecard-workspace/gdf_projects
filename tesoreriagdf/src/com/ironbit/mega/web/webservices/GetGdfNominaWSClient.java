package com.ironbit.mega.web.webservices;

import org.addcel.gdf.constants.URL;
import org.addcel.gdf.dto.NominaResponse;
import org.addcel.gdf.util.JSONUtil;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.events.OnGdfNominaResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetGdfNominaWSClient extends WebServiceClient {
	
	private String rfc;
	private String mes;
	private String anio;
	private int remuneraciones;
	private int numTrabajadores;
	
	public GetGdfNominaWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetGdfNominaWSClient setRfc(String rfc) {
		if (rfc != null) {
			this.rfc = rfc;
		}
		
		return this;
	}
	
	public GetGdfNominaWSClient setMes(String mes) {
		if (mes != null) {
			this.mes = mes;
		}
		
		return this;
	}
	
	public GetGdfNominaWSClient setAnio(String anio) {
		if (anio != null) {
			this.anio = anio;
		}
		
		return this;
	}
	
	public GetGdfNominaWSClient setRemuneraciones(int remuneraciones) {
		this.remuneraciones = remuneraciones;
		return this;
	}
	
	public GetGdfNominaWSClient setNumTrabajadores(int numTrabajadores) {
		this.numTrabajadores = numTrabajadores;
		return this;
	}
	
	public WebServiceClient execute(final OnGdfNominaResponseReceivedListener listener) {
		String json = JSONUtil.getNominaRequestAsString(Usuario.getPass(ctx), rfc, mes, anio, remuneraciones, numTrabajadores);
		
		Sys.setComposedKey(Usuario.getPass(ctx));
		addPostParameter("json", json);
		
		return executeNew(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (listener != null) {
					int error = jsonResponse.optInt("error");
					if (error == 0) {
						double impuesto = jsonResponse.optDouble("impuesto");
						double impuestoActualizado = jsonResponse.optDouble("impuesto_actualizado");
						String clave = jsonResponse.optString("clave");
						String rfc = jsonResponse.optString("rfc");
						String anio = jsonResponse.optString("anio_pago");
						String mes = jsonResponse.optString("mes_pago");
						int remuneraciones = jsonResponse.optInt("remuneraciones");
						double recargos = jsonResponse.optDouble("recargos");
//						double recargosCondonado = jsonResponse.optDouble("recargos_condonado");
//						int interes = jsonResponse.optInt("interes");
						String total = jsonResponse.optString("totalPago");
						String vigencia = jsonResponse.optString("vigencia");
						String lineaCaptura = jsonResponse.optString("linea_captura");
						
						NominaResponse response = null;
						
						if (impuesto == impuestoActualizado) {
							response = new NominaResponse(clave, rfc, anio, mes,  remuneraciones, impuesto, recargos, total, vigencia, lineaCaptura, 0);
						} else {
							response = new NominaResponse(clave, rfc, anio, mes, remuneraciones, impuesto, impuestoActualizado, recargos, total, vigencia, lineaCaptura, 0);
						}						
						listener.onGdfNominaResponseReceived(response, jsonResponse);	
					} else {						
						String errorDescripcion = jsonResponse.optString("error_descripcion");
						listener.onGdfNominaErrorReceived(error, errorDescripcion);			
					}
				}
			}
		});
		
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return URL.CONSUMIDOR;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GDF_GET_NOMINA;
	}

}
