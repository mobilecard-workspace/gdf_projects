package com.ironbit.mega.web.webservices.data;

public class DataOperation {
	protected String fecha = "";
	protected String concepto = "";
	protected String monto = "";
	protected String numAutorizacion = "";
	protected int codeError = 0;
	protected int status = 0;
	protected int proveedor = 0;
	protected int producto = 0;
	
	
	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getConcepto() {
		return concepto;
	}


	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}


	public String getMonto() {
		return monto;
	}


	public void setMonto(String monto) {
		this.monto = monto;
	}


	public String getNumAutorizacion() {
		return numAutorizacion;
	}


	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}


	public int getCodeError() {
		return codeError;
	}


	public void setCodeError(int codeError) {
		this.codeError = codeError;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public int getProveedor() {
		return proveedor;
	}


	public void setProveedor(int proveedor) {
		this.proveedor = proveedor;
	}


	public int getProducto() {
		return producto;
	}


	public void setProducto(int producto) {
		this.producto = producto;
	}
	
	
	public DataOperation(String fecha, String concepto, String monto, String numAutorizacion, 
			int codeError, int status, int proveedor, int producto){
		setFecha(fecha);
		setConcepto(concepto);
		setMonto(monto);
		setNumAutorizacion(numAutorizacion);
		setCodeError(codeError);
		setStatus(status);
		setProveedor(proveedor);
		setProducto(producto);
	}
}