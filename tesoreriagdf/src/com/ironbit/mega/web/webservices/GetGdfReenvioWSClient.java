package com.ironbit.mega.web.webservices;

import org.addcel.gdf.constants.URL;
import org.addcel.gdf.util.JSONUtil;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.system.crypto.AddcelCrypto;
import com.ironbit.mega.system.crypto.Crypto;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetGdfReenvioWSClient extends WebServiceClient {
	
	private String idBitacora;
	private int idProducto;

	public GetGdfReenvioWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetGdfReenvioWSClient setIdBitacora(String idBitacora) {
		if (null != idBitacora) {
			this.idBitacora = idBitacora;
		}
		
		return this;
	}
	
	public GetGdfReenvioWSClient setIdProducto(int idProducto) {
		this.idProducto = idProducto;
		
		return this;
	}
	

	
	public WebServiceClient executeClient(final OnResponseJSONReceivedListener listener) {
		
		addPostParameter("json", JSONUtil.getReenvioRequestAsString(ctx,idBitacora, idProducto));
		
		return execute(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				listener.onResponseJSONReceived(jsonResponse);
			}
		});
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return URL.REENVIAR_RECIBO;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GDF_REENVIAR_RECIBO;
	}

}
