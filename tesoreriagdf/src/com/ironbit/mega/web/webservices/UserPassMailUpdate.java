package com.ironbit.mega.web.webservices;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.system.crypto.Crypto;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;
import com.ironbit.mega.web.webservices.events.OnUserPassUpdatedResponseReceivedListener;

public class UserPassMailUpdate extends WebServiceClient {
	protected String password = "";
	protected String newPassword = "";
	protected String newPassword2 = "";
	protected String mail = "";
	
	public UserPassMailUpdate(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_PASS_MAIL_UPDATE;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_USER_PASS_MAIL;
	}

	/**
	 * @param newPassword
	 * Password nuevo (sin encriptar)
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	public void setNewPassword2(String newPassword2) {
		this.newPassword2 = newPassword2;
	}
	
	public void setEmail(String mail) {
		this.mail = mail;
	}
	
	/**
	 * @param password
	 * Password antiguo (sin encriptar)
	 */
	public void setActualPassword(String password) {
		this.password = password;
	}
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("login", Usuario.getLogin(ctx));
			jsonObj.put("passwordS", Crypto.sha1(password));
			jsonObj.put("password", newPassword);
			jsonObj.put("newPassword", newPassword2);
			//jsonObj.put("mail", mail);
		}catch(Exception e){
			Sys.log(e);
		}
		
		return jsonObj;
	}
	
	public WebServiceClient execute(final OnUserPassUpdatedResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(newPassword), postData);
		String newEnc = Text.mergeStr(jEnc, newPassword);
		addPostParameter("json", newEnc);
		Sys.setPTelefono(Text.parsePass(newPassword));
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){		
					try{
						listener.onUserPassUpdatedResponseReceived(
							jsonResponse.getString("resultado"), 
							jsonResponse.getString("mensaje")
						);
					}catch(Exception e){
						Sys.log(e);
						listener.onUserPassUpdatedResponseReceived("", "");
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
