package com.ironbit.mega.web.webservices.events;

public interface OnResponseReceivedListener {
	public void onResponseReceived(String response);
}