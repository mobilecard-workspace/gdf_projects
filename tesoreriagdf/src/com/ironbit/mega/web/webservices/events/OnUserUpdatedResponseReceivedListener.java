package com.ironbit.mega.web.webservices.events;

public interface OnUserUpdatedResponseReceivedListener {
	public void onUserUpdatedResponseReceived(String resultado, String mensaje);
}