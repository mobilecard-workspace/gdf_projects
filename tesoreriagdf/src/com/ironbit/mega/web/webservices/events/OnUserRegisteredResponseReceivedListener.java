package com.ironbit.mega.web.webservices.events;

public interface OnUserRegisteredResponseReceivedListener {
	public void onUserRegisteredResponseReceived(String resultado, String mensaje);
}