package com.ironbit.mega.web.webservices.events;


import java.util.ArrayList;

import com.ironbit.mega.web.webservices.data.DataPromotion;

public interface OnPromotionsResponseReceivedListener {
	public void onPromotionsResponseReceived(ArrayList<DataPromotion> promotions);
}