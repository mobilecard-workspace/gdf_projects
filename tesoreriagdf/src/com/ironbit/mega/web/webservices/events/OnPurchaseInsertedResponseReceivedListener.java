package com.ironbit.mega.web.webservices.events;

public interface OnPurchaseInsertedResponseReceivedListener {
	public void onPurchaseInsertedResponseReceived(String folio, String resultado, String mensaje);
}