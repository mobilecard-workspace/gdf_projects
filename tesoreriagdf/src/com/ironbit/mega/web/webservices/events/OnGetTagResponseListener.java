package com.ironbit.mega.web.webservices.events;

import java.util.ArrayList;

import com.ironbit.mega.web.webservices.data.DataTag;

public interface OnGetTagResponseListener {
	public void onGetTagResponseListener(ArrayList<DataTag> tags);
}
