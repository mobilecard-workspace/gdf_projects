package com.ironbit.mega.web.webservices.events;

import org.addcel.gdf.dto.TenenciaResponse;
import org.json.JSONObject;

public interface OnGdfCapturaResponseReceivedListener {
	public void onGdfCapturaResponseReceived(TenenciaResponse t, JSONObject j);
	public void onGdfErrorResponseReceived(String error, String numError);
}
