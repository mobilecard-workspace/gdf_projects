package com.ironbit.mega.web.webservices.events;


import org.json.JSONArray;


public interface OnResponseJSONAReceivedListener {

	public void onResponseJSONReceived(JSONArray jsonResponse);

	
}
