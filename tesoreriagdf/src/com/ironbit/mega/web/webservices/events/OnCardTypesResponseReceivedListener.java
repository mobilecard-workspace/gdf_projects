package com.ironbit.mega.web.webservices.events;

import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;

public interface OnCardTypesResponseReceivedListener {
	public void onCardTypesResponseReceived(ArrayList<BasicNameValuePair> cardTypes);
}