package com.ironbit.mega.web.webservices.events;

import java.util.List;

import org.addcel.gdf.dto.Comunicado;

public interface OnComunicadosResponseReceivedListener {
	
	public void OnComunicadosResponseReceived(List<Comunicado> comunicados);

}
