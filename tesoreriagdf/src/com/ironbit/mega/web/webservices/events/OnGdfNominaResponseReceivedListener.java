package com.ironbit.mega.web.webservices.events;

import org.addcel.gdf.dto.NominaResponse;
import org.json.JSONObject;

public interface OnGdfNominaResponseReceivedListener {
	public void onGdfNominaErrorReceived(int error, String errorDescripcion);
	public void onGdfNominaResponseReceived(NominaResponse response, JSONObject j);
}
