package com.ironbit.mega.web.webservices.events;

import java.util.List;

import org.addcel.gdf.dto.Infraccion;
import org.json.JSONObject;


public interface OnGdfInfraccionesResponseReceivedListener {
	public void onGdfInfraccionesErrorReceived(String error, String errorDesc);
	public void onGdfInfraccionesResponseReceived(String importeTotal, String adeudo, List<Infraccion> infracciones, boolean hashCodeCalc);
}
