package com.ironbit.mega.web.webservices.events;


import org.json.JSONObject;


public interface OnResponseJSONReceivedListener {

	public void onResponseJSONReceived(JSONObject jsonResponse);
	
}