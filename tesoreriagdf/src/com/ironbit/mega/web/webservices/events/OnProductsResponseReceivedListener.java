package com.ironbit.mega.web.webservices.events;


import java.util.ArrayList;

import com.ironbit.mega.web.webservices.data.DataProduct;

public interface OnProductsResponseReceivedListener {
	public void onProductsResponseReceived(ArrayList<DataProduct> products);
}