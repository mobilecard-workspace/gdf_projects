package com.ironbit.mega.web.webservices.events;


import java.util.ArrayList;

import com.ironbit.mega.web.webservices.data.DataCategory;

public interface OnCategoriesResponseReceivedListener {
	public void onCategoriesResponseReceived(ArrayList<DataCategory> categories);
}