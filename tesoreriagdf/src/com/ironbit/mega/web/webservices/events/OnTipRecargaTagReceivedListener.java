package com.ironbit.mega.web.webservices.events;

import java.util.ArrayList;

import com.ironbit.mega.web.webservices.data.DataTipoRecargaTag;

public interface OnTipRecargaTagReceivedListener {
	public void onTipoRecargaTagReceivedListener(ArrayList<DataTipoRecargaTag> listaTipos);
}
