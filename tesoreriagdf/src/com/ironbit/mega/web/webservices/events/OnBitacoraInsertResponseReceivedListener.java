package com.ironbit.mega.web.webservices.events;

public interface OnBitacoraInsertResponseReceivedListener {
	public void onBitacoraInsertResponseReceived(long idBitacora);
}
