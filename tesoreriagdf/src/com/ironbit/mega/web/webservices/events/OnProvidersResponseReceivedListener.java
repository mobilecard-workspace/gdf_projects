package com.ironbit.mega.web.webservices.events;


import java.util.ArrayList;

import com.ironbit.mega.web.webservices.data.DataProvider;

public interface OnProvidersResponseReceivedListener {
	public void onProvidersResponseReceived(ArrayList<DataProvider> providers);
}