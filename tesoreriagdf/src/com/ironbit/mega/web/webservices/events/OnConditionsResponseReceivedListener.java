package com.ironbit.mega.web.webservices.events;

import com.ironbit.mega.web.webservices.data.DataCondition;


public interface OnConditionsResponseReceivedListener {
	public void onConditionsResponseReceived(DataCondition condition);
}