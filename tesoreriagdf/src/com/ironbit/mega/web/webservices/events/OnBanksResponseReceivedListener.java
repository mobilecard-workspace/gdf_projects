package com.ironbit.mega.web.webservices.events;

import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;

public interface OnBanksResponseReceivedListener {
	public void onBanksResponseReceived(ArrayList<BasicNameValuePair> banks);
}