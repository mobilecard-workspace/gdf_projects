package com.ironbit.mega.web.webservices.events;


import java.util.ArrayList;

import com.ironbit.mega.web.webservices.data.DataOperation;

public interface OnOperationsResponseReceivedListener {
	public void onOperationsResponseReceived(ArrayList<DataOperation> operations);
}