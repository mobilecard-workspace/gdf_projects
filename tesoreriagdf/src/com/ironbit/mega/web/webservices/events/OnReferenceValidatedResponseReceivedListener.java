package com.ironbit.mega.web.webservices.events;

public interface OnReferenceValidatedResponseReceivedListener {
	public void onReferenceValidatedResponseReceived(String mensaje);
}
