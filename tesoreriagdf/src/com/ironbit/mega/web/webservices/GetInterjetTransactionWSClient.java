package com.ironbit.mega.web.webservices;

import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetInterjetTransactionWSClient extends WebServiceClient {
	
	protected String user;
	protected String pnr;
	protected String monto;

	public GetInterjetTransactionWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetInterjetTransactionWSClient setUser(String user){
		if(user != null){
			this.user = user;
		}
		
		return this;
	}
	
	public GetInterjetTransactionWSClient setPnr(String pnr){
		if(pnr != null){
			this.pnr = pnr;
		}
		
		return this;
	}
	
	public GetInterjetTransactionWSClient setMonto(String monto){
		if(monto != null){
			this.monto = monto;
		}
		
		return this;
	}
	
	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_INTERJET_SET_TRAN +"?user="+this.user
								+"&pnr="+this.pnr+"&monto="+this.monto+"%2E0";
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_TRANSACCION_INTERJET;
	}
	
	public WebServiceClient execute(final OnGeneralWSResponseListener listener){
		return executeClean(new OnResponseJSONReceivedListener() {
			public void onResponseJSONReceived(JSONObject jsonResponse) {

				if(listener != null){
					try{
						long id;
						try{
							id = jsonResponse.optLong("id");
							System.out.println("***MONTO***" + id);
							
						}catch(Exception ee){
							Sys.log(ee);
							id = 0;
						}				
						listener.onGeneralWSErrorListener(Long.toString(id));
					}catch(Exception e){
						Sys.log(e);
						
						String message = "Hubo problemas con su referencia. Intenta m�s tarde";
						if(jsonResponse != null){
							String msj = jsonResponse.optString("id");
							if (msj != null && msj.trim().length() > 0){
								message = msj;
							}
						}
						listener.onGeneralWSErrorListener(message);
					}
				}			
			}
		});
	}

}
