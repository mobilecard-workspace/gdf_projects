package com.ironbit.mega.web.webservices;

import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.system.Sys;
import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.web.webservices.events.OnBitacoraInsertResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class InsertPagoBitacoraWSClient extends WebServiceClient {

	private long usuario;
	private double cargo;
	private String proveedor;
	private String producto;
	private int noAutorizacion;
	private String ticket;
	private int codigoError;
	private String concepto;
	private int status;
	private String destino;

	public InsertPagoBitacoraWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public InsertPagoBitacoraWSClient setUsuario(long usuario) {
		this.usuario = usuario;
		return this;
	}
	
	public InsertPagoBitacoraWSClient setCargo(double cargo) {
		this.cargo = cargo;
		return this;
	}
	
	public InsertPagoBitacoraWSClient setProveedor(String proveedor) {
		if (proveedor != null) {
			this.proveedor = proveedor;
		}
		
		return this;
	}
	
	public InsertPagoBitacoraWSClient setProducto(String producto) {
		if (producto != null) {
			this.producto = producto;
		}
		
		return this;
	}
	
	public InsertPagoBitacoraWSClient setNoAutorizacion(int noAutorizacion) {
		this.noAutorizacion = noAutorizacion;
		return this;
	}
	
	public InsertPagoBitacoraWSClient setTicket(String ticket) {
		if (ticket != null) {
			this.ticket = ticket;
		}
		
		return this;
	}
	
	public InsertPagoBitacoraWSClient setCodigoError(int codigoError) {
		this.codigoError = codigoError;
		return this;
	}
	
	public InsertPagoBitacoraWSClient setConcepto(String concepto) {
		if (concepto != null) {
			this.concepto = concepto;
		}
		
		return this;
	}
	
	public InsertPagoBitacoraWSClient setStatus(int status) {
		this.status = status;
		return this;
	}
	
	public InsertPagoBitacoraWSClient setDestino(String destino) {
		if (destino != null) {
			this.destino = destino;
		}
		
		return this;
	}
	
	public WebServiceClient execute(final OnBitacoraInsertResponseReceivedListener listener) {
		String idUsuario = Long.toString(usuario);
		String monto = Double.toString(cargo);
		String autorizacion = Integer.toString(noAutorizacion);
		String codError = Integer.toString(codigoError);
		String estatus = Integer.toString(status);
		
		addPostParameter("idUsuario", idUsuario);
		addPostParameter("monto", monto);
		addPostParameter("proveedor", proveedor);
		addPostParameter("producto", producto);
		addPostParameter("autorizacion", autorizacion);
		addPostParameter("folioETN", ticket);
		addPostParameter("codError",codError);
		addPostParameter("detError", concepto);
		addPostParameter("estatus", estatus);
		addPostParameter("detalle", destino);
		
		
//		Par�metros de �quipo
		
		addPostParameter("imei", Sys.getIMEI(ctx));
		addPostParameter("modelo", Sys.getModel());
		addPostParameter("software", Sys.getSWVersion());
		addPostParameter("tipoProducto", Sys.getTipo());
		addPostParameter("wkey", Sys.getIMEI(ctx));
		
		System.out.println("idUsuario: " + idUsuario);
		System.out.println("monto: " + monto);
		System.out.println("proveedor: " + proveedor);
		System.out.println("producto: " + producto);
		System.out.println("autorizacion: " + autorizacion);
		System.out.println("folioETN: " + ticket);
		System.out.println("codError: " + codError);
		System.out.println("detError: " + concepto);
		System.out.println("estatus: " + estatus);
		System.out.println("detalle: " + destino);
		
		return executeClean(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (listener != null) {
					long idBitacora = jsonResponse.optLong("registrado", 0);
					listener.onBitacoraInsertResponseReceived(idBitacora);
				}
			}
		});
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_GDF_BITACORA_INSERT;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GDF_INSERT_BITACORA;
	}

}
