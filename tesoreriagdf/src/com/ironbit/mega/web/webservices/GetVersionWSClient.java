package com.ironbit.mega.web.webservices;

import org.addcel.gdf.constants.URL;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetVersionWSClient extends WebServiceClient {

	public GetVersionWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public WebServiceClient execute(final OnResponseJSONReceivedListener listener) {
		
		addPostParameter("idApp", "4");
		addPostParameter("idPlataforma", "4");
		
		return executeClean(new OnResponseJSONReceivedListener() {

			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (null != listener) {
					listener.onResponseJSONReceived(jsonResponse);
				}
			}

		});
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return URL.GET_VERSION;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GET_VERSION;
	}
	
	

}
