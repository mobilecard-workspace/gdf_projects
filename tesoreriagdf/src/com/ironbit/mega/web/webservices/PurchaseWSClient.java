package com.ironbit.mega.web.webservices;


import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Fecha;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.system.Fecha.FormatoFecha;
import com.ironbit.mega.system.crypto.Crypto;
import com.ironbit.mega.system.errores.ErrorSys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.events.OnPurchaseInsertedResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class PurchaseWSClient extends WebServiceClient{
	protected String cvv2 = "";
	protected String producto = "";
	protected String telefono = "";
	protected String password = "";
	protected int fechaVencimientoMes = 0;
	protected int fechaVencimientoAnio = 0;
	protected double X = 0.0;
	protected double Y = 0.0;
	
	
	public PurchaseWSClient(Activity ctx){
		super(ctx);
	}
	
	
	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_PURCHASES_INSERT;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_PURCHASES_INSERT;
	}
	

	public PurchaseWSClient setCvv2(String cvv2) {
		if (cvv2 != null){
			this.cvv2 = cvv2;
		}
		
		return this;
	}
	
	
	public PurchaseWSClient setPassword(String password) {
		if (password != null){
			this.password = password;
		}
		
		return this;
	}
	
	
	public PurchaseWSClient setFechaVencimientoMes(int fechaVencimientoMes) {
		this.fechaVencimientoMes = fechaVencimientoMes;
		return this;
	}

	
	public PurchaseWSClient setFechaVencimientoAnio(int fechaVencimientoAnio) {
		this.fechaVencimientoAnio = fechaVencimientoAnio;
		return this;
	}
	
	
	protected String getFechaVencimientoFormateada(){
		String f = "";
		
		try{
			Fecha fecha = new Fecha();
			fecha.setMes(fechaVencimientoMes);
			fecha.setAnio(fechaVencimientoAnio);
			f = fecha.toFechaFormateada(FormatoFecha.mesNumero + FormatoFecha.anioCorto);
		}catch(ErrorSys e){
			Sys.log(e);
		}
		
		return f;
	}

	public PurchaseWSClient setX(double X) {
		if (X != 0.0){
			this.X = X;
		}
		
		return this;
	}
	
	public PurchaseWSClient setY(double Y) {
		if (Y != 0.0){
			this.Y = Y;
		}
		
		return this;
	}

	/**
	 * Clave WS del producto a comprar
	 * @param producto
	 */
	public PurchaseWSClient setProducto(String producto) {
		if (producto != null){
			this.producto = producto;
		}
		
		return this;
	}
	
	
	public PurchaseWSClient setTelefono(String telefono){
		if (telefono != null){
			this.telefono = telefono;
		}
		
		return this;
	}
	
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("login", Usuario.getLogin(ctx));
			//jsonObj.put("password", Crypto.sha1(password));
			jsonObj.put("password", password);
			jsonObj.put("cvv2", cvv2);
			jsonObj.put("telefono", telefono);
			jsonObj.put("vigencia", getFechaVencimientoFormateada());
			jsonObj.put("producto", producto);
			jsonObj.put("imei", Sys.getIMEI(ctx));
			jsonObj.put("cx", Double.toString(X));
			jsonObj.put("cy", Double.toString(Y));
			
			jsonObj.put("tipo", Sys.getTipo());
			jsonObj.put("software", Sys.getSWVersion());
			jsonObj.put("modelo", Sys.getModel());
			jsonObj.put("key", Sys.getIMEI(ctx));
			System.out.println(jsonObj.toString());
		}catch(Exception e){
			Sys.log(e);
		}
		
		return jsonObj;
	}
	
	
	public WebServiceClient execute(final OnPurchaseInsertedResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(password), postData);
		String newEnc = Text.mergeStr(jEnc, password);
		addPostParameter("json", newEnc);
		Sys.setPTelefono(Text.parsePass(password));
				
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){	
					try{
						String folio;
						try{
							folio = jsonResponse.getString("folio");
						}catch(Exception ee){
							Sys.log(ee);
							folio = "";
						}
						
						listener.onPurchaseInsertedResponseReceived(
							folio, 
							jsonResponse.getString("resultado"), 
							jsonResponse.getString("mensaje")
						);
					}catch(Exception e){
						Sys.log(e);
						
						String message = "hubo problemas al tratar de hacer la compra. Intenta m�s tarde";
						if (jsonResponse != null){
							String msj = jsonResponse.optString("mensaje");
							if (msj != null && msj.trim().length() > 0){
								message = msj;
							}
						}
						listener.onPurchaseInsertedResponseReceived("", "", message);
					}
				}
			}

			
		});
	}
}