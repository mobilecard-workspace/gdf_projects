package com.ironbit.mega.web.webservices;

import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.system.crypto.Crypto;
import com.ironbit.mega.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mega.web.webservices.events.OnResponseReceivedListener;

public class SetInvitaWS extends WebServiceClient {
	protected String tuNombre = "", 
	nombreAmigo = "", 
	email = "";

	public SetInvitaWS(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_INVITA;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_INVITA_AMIGO;
	}
	
	public SetInvitaWS setTuNombre(String tuNombre) {
		if (tuNombre != null){
			this.tuNombre = tuNombre;
		}
		
		return this;
	}
	
	public SetInvitaWS setNombreAmigo(String nombreAmigo) {
		if (nombreAmigo != null){
			this.nombreAmigo = nombreAmigo;
		}
		
		return this;
	}
	
	public SetInvitaWS setEmail(String email) {
		if (email != null){
			this.email = email;
		}
		
		return this;
	}
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			//jsonObj.put("cadena", tuNombre+"|"+nombreAmigo+"|"+email);
			jsonObj.put("Aplicacion", "MobileCard");
			jsonObj.put("Login", tuNombre);
			jsonObj.put("Nombre", nombreAmigo);
			jsonObj.put("Telefono", email);
		
		}catch(Exception e){
			Sys.log(e);
		}
		/*"{\"login\":\""+MainClass.userBean.getLogin()
		+"\",\"password\":"+"\""+Utils.sha1(basicPassword.getText().trim())+
		"\",\"cvv2\":\""+basicCvv2.getText().trim()+
		"\",\"vigencia\":\""+stringMonth+vigency+
		"\",\"producto\":\""+this.generalBean.getClave()+
		"\",\"tarjeta\":\""+basicClave.getText().trim()+
		"\",\"pin\":\""+basicPin.getText().trim()+"\"}";	*/	
		return jsonObj;
	}
	
	public WebServiceClient execute(final OnGeneralWSResponseListener listener) {
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
				
		addPostParameter("json", Crypto.aesEncrypt(Sys.getKey(), postData));
		
		return execute(new OnResponseReceivedListener(){
			public void onResponseReceived(String jsonResponse) {
				if (listener != null){	
					try{						
						listener.onGeneralWSResponseListener(jsonResponse);
					}catch(Exception e){
						Sys.log(e);
						
						listener.onGeneralWSErrorListener(jsonResponse);
					}
				}
			}
		});
	}

}
