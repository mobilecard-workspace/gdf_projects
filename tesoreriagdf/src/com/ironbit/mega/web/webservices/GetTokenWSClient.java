package com.ironbit.mega.web.webservices;

import org.addcel.gdf.constants.URL;
import org.addcel.gdf.util.JSONUtil;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetTokenWSClient extends WebServiceClient {
	
	private String usuario;
	private String password;
	private int producto;

	public GetTokenWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetTokenWSClient setUsuario(String usuario) {
		if (null != usuario) {
			this.usuario = usuario;
		}
		
		return this;
	}
	
	public GetTokenWSClient setPassword(String password) {
		if (null != password) {
			this.password = password;
		}
		
		return this;
	}
	
	public GetTokenWSClient setProducto(int producto) {

		this.producto = producto;
		return this;
	}
	
	public WebServiceClient execute(final OnResponseJSONReceivedListener listener) {
		String json = JSONUtil.getTokenRequest(usuario, password, producto);
		Sys.setComposedKey(password);
		addPostParameter("json", json);
		
		return executeNew(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				listener.onResponseJSONReceived(jsonResponse);
			}
		});
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return URL.GET_TOKEN;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GDF_GET_PREDIAL;
	}

}
