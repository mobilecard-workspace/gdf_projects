package com.ironbit.mega.web.webservices;

import java.util.ArrayList;
import java.util.List;

import org.addcel.gdf.constants.URL;
import org.addcel.gdf.dto.Comunicado;
import org.addcel.gdf.util.JSONUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.web.webservices.events.OnComunicadosResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetComunicadosWSClient extends WebServiceClient {

	public GetComunicadosWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public WebServiceClient execute(final OnComunicadosResponseReceivedListener listener) {
		addPostParameter("json", JSONUtil.getComunicadosRequestAsString());
		return execute(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (null != jsonResponse) {
					try {
						JSONArray comunicados = jsonResponse.getJSONArray("comunicados");
						
						if (0 < comunicados.length()) {

							List<Comunicado> comunicadosList = new ArrayList<Comunicado>();
							
							for (int i = 0; i < comunicados.length(); i++) {
								JSONObject comunicadoJSON = comunicados.getJSONObject(i);
								
								Comunicado comunicado = new Comunicado();
								comunicado.setBgColor(comunicadoJSON.optString("bgColor"));
								comunicado.setContenido(comunicadoJSON.optString("contenido"));
								comunicado.setFgColor(comunicadoJSON.optString("fgColor"));
								comunicado.setIdAplicacion(comunicadoJSON.optInt("idAplicacion"));
								comunicado.setIdComunicado(comunicadoJSON.optInt("idComunicado"));
								comunicado.setIdEmpresa(comunicadoJSON.optInt("idEmpresa"));
								comunicado.setPrioridad(comunicadoJSON.optInt("idPrioridad"));
								comunicado.setTamFuente(comunicadoJSON.optInt("tamFuente"));
								
								comunicadosList.add(comunicado);
								
							}
							
							listener.OnComunicadosResponseReceived(comunicadosList);
							
						}
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Log.e("JSONException", e.getMessage());
					}
				}
			}
		});
		
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return URL.BUSCA_COMUNICADOS;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GDF_GET_COMUNICADOS;
	}

}
