package com.ironbit.mega.web.webservices;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.web.webservices.data.DataTipoRecargaTag;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;
import com.ironbit.mega.web.webservices.events.OnTipRecargaTagReceivedListener;

public class GetTipoRecargaTagWS extends WebServiceClient {

	public GetTipoRecargaTagWS(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getWebServiceUrl() {
		return UrlWebServices.URL_WS_GET_TIPO_RECARGA_TAG;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_TIPO_RECARGA_TAG;
	}
	
	public WebServiceClient execute(final OnTipRecargaTagReceivedListener listener) {
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
					if (listener != null){
						
						ArrayList<DataTipoRecargaTag> tipos = new ArrayList<DataTipoRecargaTag>();
						
						try{
							JSONArray jsonProviders = jsonResponse.getJSONArray("tipoRecargaTag");
							JSONObject jsonProvider = null;
							
							for (int a = 0; a < jsonProviders.length(); a++){
								jsonProvider = jsonProviders.getJSONObject(a);
								System.out.println(jsonProvider.toString());
								//if(!jsonProvider.getString("clave").equals("3")){
									tipos.add(new DataTipoRecargaTag(
										jsonProvider.getString("clave"), 
										
										jsonProvider.getString("nombre")
									));
								//}
							}
						}catch(Exception e){
							Sys.log(e);
						}
						listener.onTipoRecargaTagReceivedListener(tipos);
					}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
