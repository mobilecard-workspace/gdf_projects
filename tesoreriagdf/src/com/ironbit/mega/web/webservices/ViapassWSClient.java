package com.ironbit.mega.web.webservices;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Fecha;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.system.Fecha.FormatoFecha;
import com.ironbit.mega.system.crypto.Crypto;
import com.ironbit.mega.system.errores.ErrorSys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.events.OnPurchaseInsertedResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class ViapassWSClient extends WebServiceClient {
	protected String password;
	protected String producto;
    protected String imei;
    protected String pin;
    protected String tag;
    protected String cvv2;
    protected String vigencia;
    protected double X = 0.0;
    protected double Y = 0.0;
    protected int fechaVencimientoMes = 0;
	protected int fechaVencimientoAnio = 0;
	
	public ViapassWSClient(Activity ctx) {
		super(ctx);

	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_PURCASE_VIAPASS;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_PURCHASE_VIAPASS;
	}
	
	public ViapassWSClient setCvv2(String cvv2) {
		if (cvv2 != null){
			this.cvv2 = cvv2;
		}
		
		return this;
	}
	
	
	public ViapassWSClient setPassword(String password) {
		if (password != null){
			this.password = password;
		}
		
		return this;
	}
	
	
	public ViapassWSClient setFechaVencimientoMes(int fechaVencimientoMes) {
		this.fechaVencimientoMes = fechaVencimientoMes;
		return this;
	}

	
	public ViapassWSClient setFechaVencimientoAnio(int fechaVencimientoAnio) {
		this.fechaVencimientoAnio = fechaVencimientoAnio;
		return this;
	}
	
	
	protected String getFechaVencimientoFormateada(){
		String f = "";
		
		try{
			Fecha fecha = new Fecha();
			fecha.setMes(fechaVencimientoMes);
			fecha.setAnio(fechaVencimientoAnio);
			f = fecha.toFechaFormateada(FormatoFecha.mesNumero + FormatoFecha.anioCorto);
		}catch(ErrorSys e){
			Sys.log(e);
		}
		
		return f;
	}


	/**
	 * Clave WS del producto a comprar
	 * @param producto
	 */
	public ViapassWSClient setProducto(String producto) {
		if (producto != null){
			this.producto = producto;
		}
		
		return this;
	}
	
	
	public ViapassWSClient setTag(String tag){
		if (tag != null){
			this.tag = tag;
		}
		
		return this;
	}
	
	public ViapassWSClient setPin(String pin){
		if (pin != null){
			this.pin = pin;
		}
		
		return this;
	}
	
	public ViapassWSClient setX(double X) {
		if (X != 0.0){
			this.X = X;
		}
		
		return this;
	}
	
	public ViapassWSClient setY(double Y) {
		if (Y != 0.0){
			this.Y = Y;
		}
		
		return this;
	}
	
	
	protected JSONObject getJsonParam(){
		JSONObject jsonObj = new JSONObject();
		
		try{
			jsonObj.put("login", Usuario.getLogin(ctx));
			//jsonObj.put("password", Crypto.sha1(password));
			jsonObj.put("password", password);
			jsonObj.put("cvv2", cvv2);
			jsonObj.put("pin", pin);
			jsonObj.put("tarjeta", tag);
			jsonObj.put("vigencia", getFechaVencimientoFormateada());
			jsonObj.put("producto", producto);
			jsonObj.put("imei", Sys.getIMEI(ctx));
			jsonObj.put("cx", Double.toString(X));
			jsonObj.put("cy", Double.toString(Y));
			
			jsonObj.put("tipo", Sys.getTipo());
			jsonObj.put("software", Sys.getSWVersion());
			jsonObj.put("modelo", Sys.getModel());
			jsonObj.put("key", Sys.getIMEI(ctx));
			System.out.println(jsonObj.toString());
		}catch(Exception e){
			Sys.log(e);
		}
		/*
protected String login;
	protected String password;
	protected String producto;
	protected String id_order;
	protected String id_tag;
    protected String amount;
    protected String imei;
    protected String cvv2;
    protected String vigencia;
    protected String cx;
    protected String cy;
    protected String tarjeta;
		 */	
		return jsonObj;
	}
	
	public WebServiceClient execute(final OnPurchaseInsertedResponseReceivedListener listener) {
		String postData = getJsonParam().toString();
		Sys.log("json=" + postData);
		String jEnc =  Crypto.aesEncrypt(Text.parsePass(password), postData);
		String newEnc = Text.mergeStr(jEnc, password);
		addPostParameter("json", newEnc);
		Sys.setPTelefono(Text.parsePass(password));
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){	
					try{
						String folio;
						try{
							folio = jsonResponse.getString("folio");
						}catch(Exception ee){
							Sys.log(ee);
							folio = "";
						}
						
						listener.onPurchaseInsertedResponseReceived(
							folio, 
							jsonResponse.getString("resultado"), 
							jsonResponse.getString("mensaje")
						);
					}catch(Exception e){
						Sys.log(e);
						
						String message = "hubo problemas al tratar de hacer la compra. Intenta m�s tarde";
						if (jsonResponse != null){
							String msj = jsonResponse.optString("mensaje");
							if (msj != null && msj.trim().length() > 0){
								message = msj;
							}
						}
						listener.onPurchaseInsertedResponseReceived("", "", message);
					}
				}
			}

			public void onResponseJSONAReceived(JSONArray jsonResponse) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
