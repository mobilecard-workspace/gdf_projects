package com.ironbit.mega.web.webservices;

import java.util.Date;

import org.addcel.gdf.constants.URL;
import org.addcel.gdf.util.JSONUtil;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetGdfConsultaWSClient extends WebServiceClient {
	
	private int idProducto;
	private String mes;
	private String anio;
	
	//{"id_producto":1,"id_usuario":1375920819509,"mes":"08","anio":"2013"}


	public GetGdfConsultaWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetGdfConsultaWSClient setIdProducto(int idProducto) {
		this.idProducto = idProducto;
		
		return this;
	}
	
	public GetGdfConsultaWSClient setMes(String mes) {
		if (null != mes) {
			this.mes = mes;
		}
		
		return this;
	}
	
	public GetGdfConsultaWSClient setAnio(String anio) {
		if (null != anio) {
			this.anio = anio;
		}
		
		return this;
	}
	
	public WebServiceClient execute(final OnResponseJSONReceivedListener listener) {
		String json = JSONUtil.getConsultaRequestAsString(ctx, idProducto, mes, anio);
		Sys.setComposedKey(Usuario.getPass(ctx));
		
		Log.d("JSON QUE ENVIO", json);
		addGetParameter("json", json);
		
		return executeNew(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				listener.onResponseJSONReceived(jsonResponse);
			}
		});
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return URL.CONSULTA_PAGOS;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GDF_GET_CONSULTA;
	}

}
