package com.ironbit.mega.web.webservices;

import org.addcel.gdf.constants.URL;
import org.addcel.gdf.util.JSONUtil;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GdfPurchaseWSClient extends WebServiceClient {
	
	private JSONObject json;
	private int idProducto;

	public GdfPurchaseWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GdfPurchaseWSClient setJson(JSONObject json) {
		if (null != json) {
			this.json = json;
		}
		
		return this;
	}
	
	public GdfPurchaseWSClient setIdProducto(int idProducto) {
		this.idProducto = idProducto;
		
		return this;
	}
	
	public WebServiceClient execute(final OnResponseJSONReceivedListener listener) {
		String request = JSONUtil.getProcomRequestAsString(ctx,json,Usuario.getIdUser(ctx),idProducto);
		addPostParameter("json", request);
		
		return executeNew(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				listener.onResponseJSONReceived(jsonResponse);
			}
		});
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return URL.PAGO_PROSA;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GDF_GET_PURCHASE;
	}

}
