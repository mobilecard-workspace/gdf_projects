package com.ironbit.mega.web.webservices;

import java.util.ArrayList;
import java.util.List;

import org.addcel.gdf.constants.URL;
import org.addcel.gdf.dto.Infraccion;
import org.addcel.gdf.util.JSONUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.events.OnGdfInfraccionesResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetGdfInfraccionesClient extends WebServiceClient {
	
	private String placa;

	public GetGdfInfraccionesClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetGdfInfraccionesClient setPlaca(String placa) {
		if (placa != null) {
			this.placa = placa;
		}
		
		return this;
	}
	
//	{"toDO":3,"placa":"0010926","usuario":"addcel","password":"25b6f919837ece5aab57930fcb1ee09e"}


	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return URL.CONSUMIDOR;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GDF_GET_INFRACCIONES;
	}
	
	public WebServiceClient execute(final OnGdfInfraccionesResponseReceivedListener listener) {
		Log.d("Password", Usuario.getPass(ctx));
		
		
		String json = JSONUtil.getInfraccionesRequestAsString(Usuario.getPass(ctx), placa);
		Log.d("JSON infraccion", json);
		
		Sys.setComposedKey(Usuario.getPass(ctx));
		addPostParameter("json", json);
		
		return executeNew(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if (listener != null) {
					String errorNo = jsonResponse.optString("error");
					String error = jsonResponse.optString("error_desc");
					
					if (!errorNo.equals("")) {						
						listener.onGdfInfraccionesErrorReceived(errorNo, error);
					} else {
						String adeudo = jsonResponse.optString("adeudo", "0");
						if (adeudo.equals("0")) {
							listener.onGdfInfraccionesErrorReceived("", "Placa sin adeudos");
						} else {
							
							String importeTotal = jsonResponse.optString("importe_total");
							boolean hashCodeCalc = jsonResponse.optBoolean("__hashCodeCalc");
						
							List<Infraccion> infracciones = new ArrayList<Infraccion>();
							JSONArray infraccionesArray = jsonResponse.optJSONArray("arreglo_folios");
							for (int i = 0; i < infraccionesArray.length(); i++) {
								Infraccion infraccion = new Infraccion();
								
								infraccion.setFolio(infraccionesArray.optJSONObject(i).optString("folio"));
								infraccion.setHashCodeCalc(infraccionesArray.optJSONObject(i).optInt("codeCalc"));
								infraccion.setImporte(infraccionesArray.optJSONObject(i).optString("totalPago"));
								infraccion.setLineaCaptura(infraccionesArray.optJSONObject(i).optString("linea_captura"));
								
//								NUEVOS PARÁMETROS
								infraccion.setPlaca(infraccionesArray.optJSONObject(i).optString("placa"));
								infraccion.setFechaInfraccion(infraccionesArray.optJSONObject(i).optString("fechainfraccion"));
								infraccion.setActualizacion(infraccionesArray.optJSONObject(i).optString("actualizacion", "0.0"));
								infraccion.setRecargos(infraccionesArray.optJSONObject(i).optString("recargos"));
								infraccion.setDiasMulta(infraccionesArray.optJSONObject(i).optString("dias_multa"));
								
								infracciones.add(infraccion);
							}
							
							listener.onGdfInfraccionesResponseReceived(importeTotal, adeudo, infracciones, hashCodeCalc);
						}
						
					}
				}
				
			}
		});
	}

}
