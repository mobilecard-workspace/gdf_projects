package com.ironbit.mega.web.webservices;


import org.addcel.gdf.constants.URL;
import org.addcel.gdf.util.JSONUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.web.webservices.data.DataCondition;
import com.ironbit.mega.web.webservices.events.OnConditionsResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetConditionsWSClient extends WebServiceClient{
	
	@Override
	protected String getWebServiceUrl() {
		return URL.CONSUMIDOR_TERMINOS;
	}
	
	
	@Override
	protected Hilo getHiloId() {
		return Hilo.DESCARGA_WS_CONDITIONS;
	}
	
	
	public GetConditionsWSClient(Activity ctx){
		super(ctx);
	}
	
	
	public WebServiceClient execute(final OnConditionsResponseReceivedListener listener) {
		
		addPostParameter("json", JSONUtil.getCondicionesRequestAsString());
		
		return execute(new OnResponseJSONReceivedListener(){
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				if (listener != null){		
					try{
						
						JSONArray consultaTerminosCondiciones = jsonResponse.getJSONArray("consultaTerminosCondiciones");
						
						JSONObject terminos = consultaTerminosCondiciones.getJSONObject(0);
						
						String descTermino = terminos.optString("desc_termino");
						
						
						listener.onConditionsResponseReceived(new DataCondition(
							terminos.getString("id_termino"), 
							terminos.getString("desc_termino")
						));
					}catch(Exception e){
						Sys.log(e);
					}
				}
			}
		});
	}
}