package com.ironbit.mega.web.webservices;

import org.addcel.gdf.constants.URL;
import org.addcel.gdf.util.JSONUtil;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetGdfPredialWSClient extends WebServiceClient {
	
	private String cuenta;

	public GetGdfPredialWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetGdfPredialWSClient setCuenta(String cuenta) {
		if (null != cuenta) {
			this.cuenta = cuenta;
		}
		
		return this;
	}
	
	public WebServiceClient execute(final OnResponseJSONReceivedListener listener) {
		
		String json = JSONUtil.getPredialRequestAsString(Usuario.getPass(ctx), cuenta);
		Sys.setComposedKey(Usuario.getPass(ctx));
		addPostParameter("json", json);
		
		
		return executeNew(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				listener.onResponseJSONReceived(jsonResponse);
			}
		});
		
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return URL.CONSUMIDOR;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GDF_GET_PREDIAL;
	}

}
