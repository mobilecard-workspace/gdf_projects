package com.ironbit.mega.web.webservices;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.web.webservices.events.OnGeneralWSResponseListener;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetInterjetWSClient extends WebServiceClient {
	
	protected String referencia;
	
	public GetInterjetWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetInterjetWSClient setReferencia(String referencia){
		if(referencia != null){
			this.referencia = referencia;
		}
		
		return this;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_INTERJET_GET + "?pnr=" + this.referencia;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_DATOS_INTERJET;
	}
	
	public WebServiceClient execute(final OnGeneralWSResponseListener listener){
		
		return executeClean(new OnResponseJSONReceivedListener() {
			public void onResponseJSONReceived(JSONObject jsonResponse) {

				if(listener != null){
					try{
						String monto;
						try{
							monto = jsonResponse.optString("Monto");
							System.out.println("***MONTO***" + monto);
							
						}catch(Exception ee){
							Sys.log(ee);
							monto = "";
						}				
						listener.onGeneralWSErrorListener(monto);
					}catch(Exception e){
						Sys.log(e);
						
						String message = "Hubo problemas con su referencia. Intenta m�s tarde";
						if(jsonResponse != null){
							String msj = jsonResponse.optString("Monto");
							if (msj != null && msj.trim().length() > 0){
								message = msj;
							}
						}
						listener.onGeneralWSErrorListener(message);
					}
				}			
			}
		});
	}
	
	

}
