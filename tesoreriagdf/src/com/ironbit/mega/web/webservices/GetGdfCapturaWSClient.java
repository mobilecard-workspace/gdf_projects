package com.ironbit.mega.web.webservices;

import org.addcel.gdf.constants.URL;
import org.addcel.gdf.dto.TenenciaResponse;
import org.addcel.gdf.util.JSONUtil;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;

import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.events.OnGdfCapturaResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetGdfCapturaWSClient extends WebServiceClient {
	
	private String placa;
	private String ejercicio;

	public GetGdfCapturaWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}
	
	public GetGdfCapturaWSClient setPlaca(String placa){
		if (placa != null) {
			this.placa = placa;
		}
		
		return this;
	}
	
	public GetGdfCapturaWSClient setEjercicio(String ejercicio){
		if (ejercicio != null) {
			this.ejercicio = ejercicio;
		}
		
		return this;
	}
	
	protected JSONObject getJsonParam(){
		
		System.out.println("PLACA: " + placa);
		System.out.println("EJERCICIO: " + ejercicio);
		
		JSONObject json = new JSONObject();
		
		try {
//			FALTA AGREGAR OTRO PARAMETRO, PREGUNTAR A ALBERTO.
			json.putOpt("toDO", 2);
			json.putOpt("placa", placa);
			json.putOpt("ejercicio2", ejercicio);
			json.putOpt("condonacion", false);
			json.putOpt("interes", false);
			json.putOpt("subsidio", true);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return json;
	}
	
	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return URL.CONSUMIDOR;
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_GDF_GET_CAPTURA;
	}
	
	public WebServiceClient execute(final OnGdfCapturaResponseReceivedListener listener){
		
		String json = JSONUtil.getTenenciaRequestAsString(Usuario.getIdUser(ctx), Usuario.getPass(ctx), placa, ejercicio);
		Sys.setComposedKey(Usuario.getPass(ctx));
		Log.d("JSON QUE ENV�O",json);
		
		addPostParameter("json", json);
		return executeNew(new OnResponseJSONReceivedListener() {
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
				// TODO Auto-generated method stub
				if(listener != null){
					
					if(jsonResponse.has("numError")){
						
						System.out.println("HAY NUM_ERROR");
						
						listener.onGdfErrorResponseReceived(jsonResponse.optString("error"), jsonResponse.optString("numError"));
					}
					else{
						
						System.out.println("NO HAY NUM_ERROR");
						
						if(jsonResponse.optString("error").equals("1"))
							listener.onGdfErrorResponseReceived(jsonResponse.optString("error"), jsonResponse.optString("descrip"));
						else{
							
							TenenciaResponse response = new TenenciaResponse();
							response.setTenencia(jsonResponse.optDouble("tenencia", 0.0));
							response.setActualizacion(jsonResponse.optDouble("tenActualizacion", 0.0));
							response.setRecargos(jsonResponse.optDouble("tenRecargo", 0.0));
							response.setRefrendo(jsonResponse.optDouble("derecho", 0.0));
							response.setActRefrendo(jsonResponse.optDouble("derActualizacion", 0.0));
							response.setRecRefrendo(jsonResponse.optDouble("derRecargo", 0.0));
							response.setTotal(jsonResponse.optString("totalPago"));
							response.setVigencia(jsonResponse.optString("vigencia"));
							response.setLineaCaptura(jsonResponse.optString("linea_captura"));
							response.setModelo(jsonResponse.optInt("modelo"));
						
							
							listener.onGdfCapturaResponseReceived(response, jsonResponse);
						}
							
					}										
				}
			}
		});
	}

}
