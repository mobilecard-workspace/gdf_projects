package com.ironbit.mega.web.webservices;

import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;

import com.ironbit.mega.UrlWebServices;
import com.ironbit.mega.conexion.ConexionHttp.Hilo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.web.webservices.events.OnBanksResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class GetParentescoWSClient extends WebServiceClient {

	public GetParentescoWSClient(Activity ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Hilo getHiloId() {
		// TODO Auto-generated method stub
		return Hilo.DESCARGA_WS_PARENTESCO_GET;
	}

	@Override
	protected String getWebServiceUrl() {
		// TODO Auto-generated method stub
		return UrlWebServices.URL_WS_PARENTESCO_GET;
	}

	public WebServiceClient execute(final OnBanksResponseReceivedListener listener) {
		return execute(new OnResponseJSONReceivedListener(){
			
			public void onResponseJSONReceived(JSONObject jsonResponse) {
					if (listener != null){
						ArrayList<BasicNameValuePair> estados = new ArrayList<BasicNameValuePair>();
						
						try{
							JSONArray jsonEsts = jsonResponse.getJSONArray("parentescos");
							JSONObject jsonBank = null;
							
							for (int a = 0; a < jsonEsts.length(); a++){
								jsonBank = jsonEsts.getJSONObject(a);
								
								estados.add(new BasicNameValuePair(
									jsonBank.getString("descripcion"), 
									jsonBank.getString("clave")
								));
							}
						}catch(Exception e){
							Sys.log(e);
						}
						
						listener.onBanksResponseReceived(estados);
					}
			}
		
			
		});
	}
}
