package com.ironbit.mega;


import org.addcel.gdf.interfaces.DialogOkListener;
import org.addcel.gdftesoreria.R;
import org.addcel.mobilecard.activities.MenuAppActivity;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.system.contador.Contador;
import com.ironbit.mega.system.contador.OnContadorTerminadoListener;
import com.ironbit.mega.web.webservices.GetVersionWSClient;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class InicioActivity extends MenuActivity{
    public static final int WAIT_TIME = 30000;
    private GetVersionWSClient getVersion;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.inicio);
	}
	
	
	@Override
	protected void onStart() {
		super.onStart();
		new Contador(new Handler(), new OnContadorTerminadoListener(){
			public void onContadorTerminado() {
				new LongProcess(InicioActivity.this, true, 30, false, "getVersion", new LongProcessListener() {
					
					public void stopProcess() {
						// TODO Auto-generated method stub
						if (null != getVersion) {
							getVersion.cancel();
							getVersion = null;
						}
					}
					
					public void doProcess() {
						// TODO Auto-generated method stub
						getVersion = new GetVersionWSClient(InicioActivity.this);
						getVersion.execute(new OnResponseJSONReceivedListener() {
							
							public void onResponseJSONReceived(JSONObject jsonResponse) {
								// TODO Auto-generated method stub
								
								String version = getVersionName(getApplicationContext(), InicioActivity.class);
								
								try {
									String[] servVersion = jsonResponse.getString("version").split("[.]");
									String[] currVersion = version.split("[.]");
									
									boolean pasa = true;
									
									for (int i = 0; i < servVersion.length; i++) {
										if (i < currVersion.length) {
											int max = Integer.parseInt(servVersion[i]);
											int min = Integer.parseInt(currVersion[i]);
											if (max > min) {
												pasa = false;
												break;
											}
										} else {
											break;
										}
									}
									
									if (pasa) {
										startActivity(new Intent(InicioActivity.this, MenuAppActivity.class));
									} else {
										String prior = jsonResponse.getString("tipo");
										final String url = jsonResponse.getString("url");
										
										if ("1".equals(prior)) {
											makeOkAlert(InicioActivity.this, "Hay nueva nueva versi�n importante. \n"+
													" su versi�n: "+version+"\n"+
													" nueva versi�n: "+jsonResponse.getString("version"), 
													new DialogOkListener(){

														public void cancel(DialogInterface dialog,int id) {}

														public void ok(DialogInterface dialog, int id) {
															// TODO Auto-generated method stub
															Intent intent = new Intent(Intent.ACTION_VIEW);
															intent.setData(Uri.parse("market://details?id=com.ironbit.etn"));
															startActivity(intent);
														}
												
											});
										} else {
											makeYesNoAlert(InicioActivity.this, "Hay nueva nueva versi�n disponible. \n "+
													" su version: "+version+"\n"+
													" nueva version: "+jsonResponse.getString("version")+"\n"+
													" �Desea Descargarla?", new DialogOkListener() {
														
														public void ok(DialogInterface dialog, int id) {
															// TODO Auto-generated method stub

															Intent intent = new Intent(Intent.ACTION_VIEW);
															intent.setData(Uri.parse("market://details?id=com.ironbit.etn"));
															startActivity(intent);
														}
														
														public void cancel(DialogInterface dialog, int id) {
															// TODO Auto-generated method stub
															Intent intent = new Intent(InicioActivity.this, MenuAppActivity.class);
															startActivity(intent);
														}
													});
										}
									}
								} catch (JSONException e) {
									Intent intent = new Intent(InicioActivity.this, MenuAppActivity.class);
									startActivity(intent);
								} catch (Exception e) {
									Intent intent = new Intent(InicioActivity.this, MenuAppActivity.class);
									startActivity(intent);
								}
								
								stopProcess();
							}
						});
						
					}
				}).start();
			}
		},2, "splashInicial");
		
	}
	
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.PRINCIPAL;
	}
	
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	public String getVersionName(Context context, Class cls) 
	    {
	      try {
	        ComponentName comp = new ComponentName(context, cls);
	        PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
	        return pinfo.versionName;
	      } catch (android.content.pm.PackageManager.NameNotFoundException e) {
	        return null;
	      }
    }
	
	public static void makeOkAlert(Context con, String msj, final DialogOkListener listener){
		AlertDialog.Builder builder = new AlertDialog.Builder(con);
		builder.setMessage(msj)
		       .setCancelable(false)
		       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		        	   if(listener != null)
		        		   listener.ok(dialog, id);
		           }
		       })
		       ;
		
		AlertDialog alert = builder.create();
		
		alert.show();
	}
	
	public static void makeYesNoAlert(Context con, String msj, final DialogOkListener listener){
		AlertDialog.Builder builder = new AlertDialog.Builder(con);
		builder.setMessage(msj)
		       .setCancelable(false)
		       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                listener.ok(dialog, id);
		           }
		       })
		       .setNegativeButton("No", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		        	   dialog.cancel();
		        	   if(listener != null)
		                listener.cancel(dialog, id);
		           }
		       });
		AlertDialog alert = builder.create();
		alert.show();
	}
}