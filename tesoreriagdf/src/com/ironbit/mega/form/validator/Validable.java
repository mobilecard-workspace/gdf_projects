package com.ironbit.mega.form.validator;

import com.ironbit.mega.system.errores.ErrorSys;

public interface Validable {
	public void validate() throws ErrorSys;
}
