package com.ironbit.mega.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;

import com.ironbit.mega.activities.events.OnPressBackListener;
import com.ironbit.mega.form.validator.FormValidable;
import com.ironbit.mega.widget.imageloader.ImageLoader;

public abstract class MainActivity extends Activity implements FormValidable{
	/**
	 * Propiedades
	 */
	public static final String ID_APLICACION = "MobileCard";
	public ProgressDialog progressDialog = null;
	public AlertDialog alertProgressDialog = null;
	protected ImageLoader imageLoader = null;
	
	
	
	
	/**
	 * Enums
	 */
	
	public enum Actividades {
		SIN_ID,
		PRINCIPAL,
		INICIO_SESION,
		MENU,
		REGISTRO,
		MODIFICAR_DATOS,
		MODIFICAR_PASSWORD,
		CATEGORIAS,
		PROVEEDORES,
		PRODUCTOS,
		RESUMEN,
		FINAL_COMPRA,
		PROMOCIONES,
		CONDICIONES,
		COMPRAS,
		CONFIGURACIONES,
		COMPRA_FALLIDA,
		ABOUT,
		REC_PASS,
		INVITA_AMIGO,
		MODIFICAR_PASS_MAIL,
		AGREGA_TAG,
		INSERT_TAG,
		CONS_TAG,
		OHL_COMPRA,
		CALIENTE_WEB,
		CALIENTE_MOVIL,
		REENVIA_SMS,
		IAVE2,
		VITAMEDICA_INDIVIDUAL,
		VITAMEDICA_COMPRA,
		VITAMEDICA_BENEFICIARIO,
		TENECIA_PLACAS, 
		TENENCIA_COMPRA,
		INTERJET_REFERENCIA,
		INTERJET_REFERENCIA_AMEX,
		CFE_REFERENCIA,
		CFE_CATEGORIES,
		MEGA_REFERENCIA,
		MEGA_CUENTA,
		MEGA_CATEGORIES, 
		GDF_DEPTOS,
		GDF_TRAMITES,
		GDF_PAGO_TENENCIA,
		GDF_PAGO_INFRACCIONES,
		GDF_PAGO_NOMINA,
		GDF_PAGO_PREDIAL,
		GDF_PAGO_AGUA,
		GDF_CONSULTA,
		GDF_CONSULTA_TENENCIA,
		GDF_CONSULTA_INFRACCION,
		GDF_CONSULTA_NOMINA,
		GDF_CONSULTA_PREDIAL,
		GDF_CONSULTA_AGUA,
		GDF_COMPRA,
		GDF_COMPRA_WEB,
		GDF_DETALLE;
	}
	
	
	/**
	 * Metodos
	 */
	
	abstract public Actividades getIdActividad();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	
	private AlertDialog getAlertProgressDialog(){
		ImageView im = null;
		
		if (alertProgressDialog == null){
			Builder builder = new AlertDialog.Builder(this);
			
			//creamos imagenes
			im = new ImageView(this);
			im.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			builder.setView(im);
			
			alertProgressDialog = builder.create();
			alertProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		}
		
		imageLoader = new ImageLoader(this, im, "", 8, true);
		return alertProgressDialog;
	}
	
	
	public void mostrarProgressDialog(){
		getAlertProgressDialog().show();
		//progressDialog = ProgressDialog.show(this, "", getString(R.string.app_progress_dialog), true);
	}
	
	
	public void mostrarProgressDialog(final OnPressBackListener listener){
		getAlertProgressDialog().setOnKeyListener(new OnKeyListener() {
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK){
					listener.onPressBack();
					return true;
				}
				
				return false;
			}
		});
		
		getAlertProgressDialog().show();
		
//		progressDialog = ProgressDialog.show(this, "", getString(R.string.app_progress_dialog), true);
//		progressDialog.setOnKeyListener(new OnKeyListener() {
//			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
//				if (keyCode == KeyEvent.KEYCODE_BACK){
//					listener.onPressBack();
//					return true;
//				}
//				
//				return false;
//			}
//		});
	}
	
	
	public void ocultarProgressDialog(){
		if (alertProgressDialog != null){
			alertProgressDialog.dismiss();
			
			if (imageLoader != null){
				imageLoader.detener();
				imageLoader = null;
			}
		}
		
//		if (progressDialog != null){
//			progressDialog.dismiss();
//		}
	}
	
	
	public static void setAppConf(Context ctx, String nombre, String valor){
		ConfigActivity.setAppConf(ctx, nombre, valor);
	}
	
	
	public static String getAppConf(Context ctx, String nombre){
		return ConfigActivity.getAppConf(ctx, nombre);
	}
	
	
	public static String getAppConf(Context ctx, String nombre, String defaultVal){
		return ConfigActivity.getAppConf(ctx, nombre, defaultVal);
	}
	
	
	/**
	 * @param layoutResID. id del recurso del layour a mostrar
	 */
	protected void inicializarGUI(int layoutResID){
		setContentView(layoutResID);
		configValidations();
	}
	
	
	/**
	 * Override this to config a series of validations for a form
	 */
	public void configValidations() {}
}
