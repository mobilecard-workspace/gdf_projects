package com.ironbit.mega.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.ironbit.mega.R;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.form.validator.FormValidator;
import com.ironbit.mega.system.errores.ErrorSys;
import com.ironbit.mega.usuario.OnUserRegistradoListener;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.UserLoginWSClient;

public class IniciarSesionActivity extends MenuActivity{
	protected Button btnOk = null;
	protected Button btnRegistrar = null;
	protected EditText txtUsuario = null; 
	protected EditText txtPassword = null; 
	protected final FormValidator formValidator = new FormValidator(this, true);
	protected UserLoginWSClient login = null;
	protected LongProcess iniciarSesionProcess = null;
	
	
	protected Button getBtnOk(){
		if (btnOk == null){
			btnOk = (Button)findViewById(R.id.btnOk);
		}
		
		return btnOk;
	}
	
	
	protected Button getBtnRegistrar(){
		if (btnRegistrar == null){
			btnRegistrar = (Button)findViewById(R.id.btnRegistrarse);
		}
		
		return btnRegistrar;
	}
	
	
	protected EditText getTxtUsuario(){
		if (txtUsuario == null){
			txtUsuario = (EditText)findViewById(R.id.txtUsuario);
		}
		
		return txtUsuario;
	}
	
	
	protected EditText getTxtPassword(){
		if (txtPassword == null){
			txtPassword = (EditText)findViewById(R.id.txtPassword);
		}
		
		return txtPassword;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.inicio_sesion);
		Usuario.reset(this);
		
		iniciarSesionProcess = new LongProcess(this, true, 30, true, "iniciarSesion", new LongProcessListener(){
			public void doProcess() {
				Usuario.estaRegistrado(IniciarSesionActivity.this, getTxtUsuario().getText().toString(), getTxtPassword().getText().toString(), new OnUserRegistradoListener(){
					public void onUserRegistradoListener(boolean isRegistrado, String mensaje) {
						if (isRegistrado){
							//Guardamos los datos de inicio de sesión para futuras peticiones a los WS
							Usuario.setLogin(IniciarSesionActivity.this, getTxtUsuario().getText().toString());
							Usuario.setPass(IniciarSesionActivity.this, getTxtPassword().getText().toString());
							
							//abrimos menu y cerramos esta pantalla
							startActivity(new Intent(IniciarSesionActivity.this, MenuAppActivity.class));
							finish();
						}else{
							ErrorSys.showMsj(IniciarSesionActivity.this, mensaje);
						}
						
						iniciarSesionProcess.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				Usuario.cancelarChequeoRegistro(IniciarSesionActivity.this);
			}
		});
	}
	
	
	@Override
	protected void onStop() {
		iniciarSesionProcess.stop();
		super.onStop();
	}
	
	
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
		getBtnOk().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				if (formValidator.validate()){
					iniciarSesionProcess.start();
				}
			}
		});
		
		getBtnRegistrar().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				Intent intent = new Intent(IniciarSesionActivity.this, RegistroActivity.class);
				startActivity(intent);
			}
		});
	}
	
	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtUsuario(), "Usuario")
			.canBeEmpty(false)
			.maxLength(10)
			.minLength(3);
		
		formValidator.addItem(getTxtPassword(), "Contraseña")
			.canBeEmpty(false)
			.maxLength(12)
			.minLength(8);
	}
	
	
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.INICIO_SESION;
	}
}