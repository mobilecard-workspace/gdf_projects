package com.ironbit.mega.conexion;


import java.util.Hashtable;

import com.ironbit.mega.conexion.http.HttpConexion;
import com.ironbit.mega.conexion.http.OnRespuestaHttpRecibidaListener;
import com.ironbit.mega.conexion.http.HttpConexion.Charset;
import com.ironbit.mega.conexion.http.HttpConexion.Metodo;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.system.errores.ErrorSys;
import com.ironbit.mega.web.util.WebUtil;

import android.os.Handler;

public class ConexionHttp{
	
	public enum Hilo{
		DESCARGA_WS_USER_LOGIN(0, "Descarga WS User Login"),
		DESCARGA_WS_USER_INSERT(1, "Descarga WS User Register"),
		DESCARGA_WS_USER_UPDATE(2, "Descarga WS User Edit"),
		DESCARGA_WS_USER_UPDATE_PASS(3, "Descarga WS User Edit Pass"),
		DESCARGA_WS_USER_GET(4, "Descarga WS User Get"),
		DESCARGA_WS_BANKS(5, "Descarga WS Banks"),
		DESCARGA_WS_PROVIDERS(6, "Descarga WS Providers"),
		DESCARGA_WS_CARD_TYPES(7, "Descarga WS Card Types"),
		DESCARGA_WS_CATEGORIES(8, "Descarga WS Categories"),
		DESCARGA_WS_PRODUCTS(9, "Descarga WS Products"),
		DESCARGA_WS_PURCHASES(10, "Descarga WS Purchases"),
		DESCARGA_WS_PURCHASES_INSERT(11, "Descarga WS Purchases Insert"),
		DESCARGA_WS_PROMOTIONS(12, "Descarga WS Promotions"),
		DESCARGA_WS_CONDITIONS(13, "Descarga WS Conditions"),
		DESCARGA_WS_IAVE(14, "Descarga WS IAVE"),
		DESCARGA_WS_COMOSION(15, "Descarga WS Comision"),
		DESCARGA_WS_RECOVERY(16, "Descarga WS Recovery"),
		DESCARGA_WS_INVITA(17, "Descarga WS Invita Amigo"),
		DESCARGA_WS_PASS_MAIL_UPDATE(18, "Deascarga WS User Pass Mail Update"),
		DESCARGA_WS_TIPO_RECARGA_TAG(19, "Deascarga WS Tipo Recarga Tag"),
		DESCARGA_WS_SET_TAG(20, "Deascarga WS Set Tag"),
		DESCARGA_WS_GET_TAG(21, "Deascarga WS Get Tag"),
		DESCARGA_WS_REMOVE_TAG(22, "Deascarga WS Remove Tag"),
		DESCARGA_WS_PURCASE_OHL(23, "Deascarga WS Compra OHL"), 
		DESCARGA_WS_PURCASE_VIAPASS(24, "Deascarga WS Compra VIAPASS"),
		DESCARGA_WS_SET_SMS(25, "Deascarga WS Set SMS"),
		DESCARGA_WS_IAVE2(26, "Deascarga WS IAVE"),
		DESCARGA_WS_ESTADOS_GET(27, "Deascarga Estados"),
		DESCARGA_WS_EDOCIVIL_GET(28, "Deascarga Estado Civil"),
		DESCARGA_WS_PARENTESCO_GET(29, "Deascarga Parentesco"),
		DESCARGA_WS_PURCHASE_VITA(30, "Deascarga VITAMEDICA compra") ,
		
		DESCARGA_WS_TENENCIA(31, "Deascarga Tenencia compra"),
		
		DESCARGA_WS_ORIGEN_ETN(32, "Descarga Origen"),
		DESCARGA_WS_DESTINO_ETN(33, "Descarga destino"),
		

		DESCARGA_WS_DATOS_INTERJET(34, "Descarga Interjet"),
		DESCARGA_WS_TRANSACCION_INTERJET(35, "Transaccion Interjet"),
		
		DESCARGA_WS_ENVIA_MAIL(36, "Envia Mail"),
		
		DESCARGA_WS_GDF_GET_CAPTURA(37, "Get Gdf Captura"),
		DESCARGA_WS_GDF_GET_INFRACCIONES(38, "Get Gdf Infracciones"),
		DESCARGA_WS_GDF_GET_NOMINA(39, "Get Gdf Nomina"),
		DESCARGA_WS_GDF_GET_PREDIAL(40, "Get Gdf Predial"),
		DESCARGA_WS_GDF_GET_AGUA(41, "Get Gdf Agua"),
		DESCARGA_WS_GDF_INSERT_BITACORA(42, "Insert Gdf Bitacora"),
		DESCARGA_WS_GDF_GET_COMUNICADOS(43, "Busca GdF Comunicados"),
		DESCARGA_WS_GDF_GET_CONSULTA(44, "Get Gdf Consulta"),
		DESCARGA_WS_GDF_REENVIAR_RECIBO(45, "Reenviar Recibo"),
		DESCARGA_WS_GET_VERSION(46, "Get Version"),
		DESCARGA_WS_GET_TOKEN(47, "Get Token"),
		DESCARGA_WS_GDF_GET_PURCHASE(48, "Purchase GDF");
		
		
		private int id = 0;
		private String nombre = "";
		
		Hilo(int id, String nombre){
			this.id = id;
			this.nombre = nombre;
		}
		
		public int getId(){
			return id;
		}
		
		public String getNombre(){
			return nombre;
		}
	}
	
	/**
	 * Propiedades
	 */
	
	protected static ConexionHttp instancia = null;
	private static Hashtable<Integer, Thread> threads = null;
	
	
	/**
	 * Constructor
	 */

	/**
	 * no se pueden instanciar objectos de esta clase desde fuera de la misma.
	 */
	private ConexionHttp(){}

	
	/**
	 * M�todos
	 */
	
	public static ConexionHttp getInstancia(){
		if (instancia == null){
			instancia = new ConexionHttp();
		}
		
		return instancia;
	}
	
	
	public static Hashtable<Integer, Thread> getThreads(){
		if (threads == null){
			threads = new Hashtable<Integer, Thread>();
		}
		
		return threads;
	}
	
	
	/**
	 * Realiza una petición POST sincrona al servidor especificado en la url
	 * @param parametrosPost. String con las variables a enviar mediante POST. Ejemplo: var1=valor1&var2=valor2<br/>
	 * Si se desea enviar parametros mediante GET, debem venir adjuntos al parametro @param url. <br/>
	 * <br/>NOTA: se asume que estos valores ya viene encriptado con WebUtil.encriptarParaQuery()
	 * @throws ErrorSys
	 */
	public String post(String url, String parametrosPost) throws ErrorSys{
		try{
			return HttpConexion.post(url, WebUtil.queryStringToHashtable(parametrosPost));
		}catch(ErrorSys e){
			throw e;
		}catch(Exception ee){
			throw new ErrorSys(ee.getMessage());
		}
	}
	
	
	public String post(String url, Hashtable<String, String> parametrosPost) throws ErrorSys{
		try{
			return HttpConexion.post(url, parametrosPost);
		}catch(ErrorSys e){
			throw e;
		}catch(Exception ee){
			throw new ErrorSys(ee.getMessage());
		}
	}
	
	
	public ConexionHttp getAsync(OnRespuestaHttpRecibidaListener listener, Hilo hiloId, String url, Charset charsetRequest, Charset charsetResponse) throws ErrorSys{
		try{
			getThreads().put(hiloId.getId(), new HttpConexion(new Handler(), listener, hiloId.getId(), url, Metodo.get, null, charsetRequest, charsetResponse));
			getThreads().get(hiloId.getId()).start();
		}catch(Exception e){
			Sys.log(e);
			throw new ErrorSys(e.getMessage());
		}
		
		return this;
	}
	
	
	/**
	 * Realiza una petición POST asincrona al servidor especificado en la url
	 * @param parametrosPost. String con las variables a enviar mediante POST. Ejemplo: var1=valor1&var2=valor2<br/>
	 * Si se desea enviar parametros mediante GET, debem venir adjuntos al parametro @param url. <br/>
	 * <br/>NOTA: se asume que estos valores ya viene encriptado con WebUtil.encriptarParaQuery()
	 * @throws ErrorSys
	 */
	public ConexionHttp postAsync(OnRespuestaHttpRecibidaListener listener, Hilo hiloId, String url, String parametrosPost) throws ErrorSys{
		try{
			getThreads().put(hiloId.getId(), new HttpConexion(new Handler(), listener, hiloId.getId(), url, Metodo.post, WebUtil.queryStringToHashtable(parametrosPost)));
			getThreads().get(hiloId.getId()).start();
		}catch(Exception e){
			Sys.log(e);
			throw new ErrorSys(e.getMessage());
		}
		
		return this;
	}
	
	
	public ConexionHttp postAsync(OnRespuestaHttpRecibidaListener listener, Hilo hiloId, String url, String parametrosPost, Hashtable<String, String> filesPost) throws ErrorSys{
		try{
			getThreads().put(hiloId.getId(), new HttpConexion(new Handler(), listener, hiloId.getId(), url, Metodo.post, WebUtil.queryStringToHashtable(parametrosPost), filesPost));
			getThreads().get(hiloId.getId()).start();
		}catch(Exception e){
			Sys.log(e);
			throw new ErrorSys(e.getMessage());
		}
		
		return this;
	}
	
	
	public ConexionHttp postAsync(OnRespuestaHttpRecibidaListener listener, Hilo hiloId, String url, Hashtable<String, String> parametrosPost, Charset charsetRequest, Charset charsetResponse) throws ErrorSys{
		try{
			getThreads().put(hiloId.getId(), new HttpConexion(new Handler(), listener, hiloId.getId(), url, Metodo.post, parametrosPost, charsetRequest, charsetResponse));
			getThreads().get(hiloId.getId()).start();
		}catch(Exception e){
			Sys.log(e);
			throw new ErrorSys(e.getMessage());
		}
		
		return this;
	}
	
	
	/**
	 * @description
	 * Cancela la recepción de datos http que se estaba realizando con algun metodo asincrono como postAsync()
	 */
	public static void cancelarConexionAsync(Hilo hiloId){
		if (getThreads().get(hiloId.getId()) != null){
			try{
				getThreads().get(hiloId.getId()).interrupt();
				getThreads().remove(hiloId.getId());
			}catch(Exception e){
				Sys.log(e);
			}
			
			Sys.log("cancelamos hilo: "+hiloId.getNombre());
		}
	}
}
