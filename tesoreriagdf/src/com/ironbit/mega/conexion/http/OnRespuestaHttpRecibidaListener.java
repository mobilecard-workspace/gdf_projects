package com.ironbit.mega.conexion.http;

import com.ironbit.mega.widget.ProgressBarHandler;

public interface OnRespuestaHttpRecibidaListener {
	/**
	 * Propiedades
	 */
	public ProgressBarHandler barraHandler = new ProgressBarHandler();
	
	
	/**
	 * Metodos
	 */
	public void onRespuestaHttpRecibida(int idRespuesta, String respuesta);
}