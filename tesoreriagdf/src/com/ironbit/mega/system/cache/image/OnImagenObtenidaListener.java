package com.ironbit.mega.system.cache.image;

import android.graphics.Bitmap;

public interface OnImagenObtenidaListener {
	 public void onImagenObtenida(Bitmap imagen);
}