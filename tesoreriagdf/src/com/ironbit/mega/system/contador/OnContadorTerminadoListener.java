package com.ironbit.mega.system.contador;

public interface OnContadorTerminadoListener {
	public void onContadorTerminado();
}
