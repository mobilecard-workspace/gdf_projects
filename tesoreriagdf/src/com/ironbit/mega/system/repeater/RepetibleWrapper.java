package com.ironbit.mega.system.repeater;


public class RepetibleWrapper implements Runnable{
	protected Repetible listener = null;
	
	public RepetibleWrapper(Repetible listener){
		this.listener = listener;
	}
	
	
	public void run(){
		listener.repetir();
	}
}
