package com.ironbit.mega.system.events;

public interface OnTouchSlideListener {
	public void onTouchSlideLeft();
	
	public void onTouchSlideRight();
}