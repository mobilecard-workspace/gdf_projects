package com.ironbit.mega;

public class UrlWebServices {
	
	private final static String URL_WS_BASE = "http://50.57.192.213:8080/AddCelBridge/Servicios/";
//	private final static String URL_WS_BASE = "http://www.mobilecard.mx:8080/AddCelBridge/Servicios/";
//	private final static String URL_WS_BASE = "http://201.161.23.42:8080/AddCelBridge/Servicios/";
	public final static String URL_WS_USER_LOGIN = 			URL_WS_BASE + "adc_userLogin.jsp";
	public final static String URL_WS_USER_INSERT = 		URL_WS_BASE + "adc_userInsertGobierno.jsp";
	public final static String URL_WS_USER_UPDATE = 		URL_WS_BASE + "adc_userUpdate.jsp";
	public final static String URL_WS_USER_UPDATE_PASS = 	URL_WS_BASE + "adc_userPasswordUpdate.jsp";
	public final static String URL_WS_USER_GET = 			URL_WS_BASE + "adc_getUserData.jsp";
	public final static String URL_WS_CARD_TYPE_GET = 		URL_WS_BASE + "adc_getCardType.jsp";
	public final static String URL_WS_BANKS_GET = 			URL_WS_BASE + "adc_getBanks.jsp";
	public final static String URL_WS_PROVIDERS_GET = 		URL_WS_BASE + "adc_getProviders.jsp";
	public final static String URL_WS_CATEGORIAS_GET = 		URL_WS_BASE + "adc_getCategoris.jsp";
	public final static String URL_WS_PRODUCTOS_GET = 		URL_WS_BASE + "adc_getProducts.jsp";
	public final static String URL_WS_PURCHASES_GET = 		URL_WS_BASE + "adc_getUserPurchases.jsp";
	public final static String URL_WS_PURCHASES_INSERT = 	URL_WS_BASE + "adc_purchase.jsp";
	public final static String URL_WS_IAVE_INSERT = 		URL_WS_BASE + "adc_purchase_iave.jsp";
	public final static String URL_WS_PROMOTIONS_GET = 		URL_WS_BASE + "adc_getPromotions.jsp";
	public final static String URL_WS_CONDITIONS_GET = 		URL_WS_BASE + "adc_getConditions.jsp";
	
	public final static String URL_WS_ESTADOS_GET = 		URL_WS_BASE + "adc_getEstados.jsp";
	public final static String URL_WS_EDO_CIVIL_GET = 		URL_WS_BASE + "adc_getEstadoCivil.jsp";
	public final static String URL_WS_PARENTESCO_GET = 		URL_WS_BASE + "adc_getParentesco.jsp";
	
	public final static String URL_WS_COMOSION_GET = 		URL_WS_BASE + "adc_getComision.jsp";
	public final static String URL_WS_RECOVERY_GET = 		URL_WS_BASE + "adc_RecoveryUP.jsp";
	public final static String URL_WS_INVITA_AMIGO = 		URL_WS_BASE + "adc_setInvita.jsp";
	public static final String URL_WS_USER_PASS_MAIL = 		URL_WS_BASE + "adc_userPasswordUpdateMail.jsp";
	public static final String URL_WS_GET_TIPO_RECARGA_TAG =URL_WS_BASE + "adc_getTipoRecargaTag.jsp";
	public static final String URL_WS_SET_TAG =				URL_WS_BASE + "adc_setTag.jsp";
	public static final String URL_WS_GET_TAG =				URL_WS_BASE + "adc_getTags.jsp";
	public static final String URL_WS_REMOVE_TAG =			URL_WS_BASE + "adc_removeTag.jsp";
	public static final String URL_WS_PURCHASE_OHL =		URL_WS_BASE + "adc_purchase_ohl.jsp";
	public static final String URL_WS_PURCHASE_VIAPASS = 	URL_WS_BASE + "adc_purchase_viapass.jsp";
	public static final String URL_WS_PURCHASE_VITA = 		URL_WS_BASE + "adc_purchase_vitamedica.jsp";
	public static final String URL_WS_SET_SMS = 			URL_WS_BASE + "adc_setSMS.jsp";
	public final static String URL_WS_IAVE_PASE = 			URL_WS_BASE + "adc_purchase_pase.jsp";
	public final static String URL_WS_TENENCIA = 			URL_WS_BASE + "adc_purchase_tenencia.jsp";
	
	public final static String URL_WS_ORIGEN_GET = 		    "http://201.161.23.42:8080/PruebasETN/etn/terminales";
	public final static String URL_WS_DESTINO_GET = 		"http://201.161.23.42:8080/PruebasETN/etn/destinos";
	
	public final static String URL_WS_INTERJET_GET = 		"http://www.addcelapp.com:12345/getPNR2.aspx"; 
	public final static String URL_WS_INTERJET_SET_TRAN =   "http://www.addcelapp.com:12345/getInterjetTransaction.aspx";  
	
	public final static String URL_WS_SCAN_PAY = 			 URL_WS_BASE + "prosaScanAndPay.jsp";
	public final static String URL_WS_SEND_MAIL = 			 "http://201.161.23.42:15500/mail.aspx";
	
//	http://mobilecard.mx:8595/GatewayGDF/Consumer
//	http://mobilecard.mx:8595/WsGDF/GdfWebServiceConsumer
	public final static String URL_WS_GDF_CAPTURA_GET = 	"http://mobilecard.mx:8595/GatewayGDF/Consumer";
	public final static String URL_WS_GDF_BITACORA_INSERT = "http://www.mobilecard.mx:8080/AddCelBridge/ClienteBitacoraAgrega";
	//"http://192.168.1.21:8080/WsGDF/GdfWebServiceConsumer";
	//
}