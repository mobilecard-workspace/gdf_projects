package com.ironbit.mega;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class UtilsGdf {

	public static String getLineaCaptura(String placa){
		return "84112XX" + placa.toUpperCase() + "1NFYEV8";
	}
	
	public static void showCenterToast(Context context, String mensaje, int duracion) {
		Toast t = Toast.makeText(context, mensaje, duracion);
		t.setGravity(Gravity.CENTER, 0, 0);
		t.show();
	}
}
