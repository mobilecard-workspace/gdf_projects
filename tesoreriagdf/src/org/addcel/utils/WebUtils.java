package org.addcel.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

public class WebUtils {
	
	public static UrlEncodedFormEntity getParamsEntity(String[]params, String[] vals){
		 List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        for(int x=0; x<params.length; x++){
	        	//System.out.println(params[x]+" = "+vals[x]);
	        	 nameValuePairs.add(new BasicNameValuePair(params[x], vals[x]));
	        }
	        try {
				return new UrlEncodedFormEntity(nameValuePairs);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
	}
}
