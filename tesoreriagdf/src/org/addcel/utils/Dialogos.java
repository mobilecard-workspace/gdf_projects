package org.addcel.utils;

import org.addcel.gdf.sql.PlacaDataSource;
import org.addcel.gdf.sql.PredialDataSource;
import org.addcel.gdftesoreria.R;
import org.addcel.mobilecard.activities.UpdatePassMailActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.Toast;

import com.ironbit.mega.activities.MainActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.system.errores.ErrorSys;
import com.ironbit.mega.usuario.OnUserRegistradoListener;
import com.ironbit.mega.usuario.Usuario;

public class Dialogos {
private static ProgressDialog progress;
private static LongProcess loginProcess;
	
	public static ProgressDialog makeDialog(Context con, String title, String msj){
		progress = new ProgressDialog(con);
		progress.setTitle(title);
		progress.setMessage(msj);
		progress.setIndeterminate(true);
		progress.show();
		return progress;
	}
	
	public static void closeDialog(){
		if(progress.isShowing()){
			progress.dismiss();
		}
	}
	
	public static void showInitialDialog(final Activity ctx) {
		AlertDialog.Builder alertDiaBuilder = new AlertDialog.Builder(ctx);
		alertDiaBuilder.setTitle(ctx.getResources().getText(R.string.dialog_title))
			.setMessage(ctx.getResources().getText(R.string.dialog_message))
			.setCancelable(false)
			.setPositiveButton(R.string.dialog_ok_button, new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
		
		AlertDialog alertDialog = alertDiaBuilder.create();
		alertDialog.show();
	}	
	
	public static void showPlacaDialog(final MainActivity ctx, final PlacaDataSource data) {
		

		
		EditText placaEdit = new EditText(ctx);
		placaEdit.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		placaEdit.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
		
		final EditText finalPlaca = placaEdit;
		
		AlertDialog.Builder alertDiaBuilder = new AlertDialog.Builder(ctx);
		alertDiaBuilder.setTitle(ctx.getResources().getString(R.string.placa_dialog_title))
		.setMessage(ctx.getResources().getString(R.string.placa_dialog_message))
		.setView(placaEdit)
		.setNegativeButton(R.string.placa_dialog_cancel_button, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		})
		.setPositiveButton(R.string.placa_dialog_add_button, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				final String placa = finalPlaca.getText().toString().trim().toUpperCase();
				
				if (null == placa || "".equals(placa)) {
					Toast.makeText(ctx, "Introduzca una placa", Toast.LENGTH_SHORT).show();
				} else {
					Log.i("showPlacaDialog", "abrimos dataSource");
					data.open();
				
					if (data.insertaNuevo(placa) > 0) {
						Log.i("showPlacaDialog", "Exito en inserci�n");
						Toast.makeText(ctx, "Su placa se ha insertado con �xito.", Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(ctx, "Error en inserci�n de placa. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
					}
					Log.i("showPlacaDialog", "cerramos dataSource");
					data.close();
					Log.i("showPlacaDialog", "despedimos dialog");
					dialog.dismiss();
				}
			}
		});
		
		AlertDialog alertDialog = alertDiaBuilder.create();
		alertDialog.show();
	}
	
	public static void showPredialDialog(final MainActivity ctx, final PredialDataSource data) {
		

		
		EditText placaEdit = new EditText(ctx);
		placaEdit.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		placaEdit.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
		
		final EditText finalPlaca = placaEdit;
		
		AlertDialog.Builder alertDiaBuilder = new AlertDialog.Builder(ctx);
		alertDiaBuilder.setTitle(ctx.getResources().getString(R.string.predial_dialog_title))
		.setMessage(ctx.getResources().getString(R.string.predial_dialog_message))
		.setView(placaEdit)
		.setNegativeButton(R.string.placa_dialog_cancel_button, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		})
		.setPositiveButton(R.string.placa_dialog_add_button, new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				final String cuenta = finalPlaca.getText().toString().trim().toUpperCase();
				
				if (null == cuenta || "".equals(cuenta)) {
					Toast.makeText(ctx, "Introduzca una cuenta de predial", Toast.LENGTH_SHORT).show();
				} else {
					Log.i("showPlacaDialog", "abrimos dataSource");
					data.open();
				
					if (data.insertaNuevo(cuenta) > 0) {
						Log.i("showPlacaDialog", "Exito en inserci�n");
						Toast.makeText(ctx, "Su cuenta predial se ha insertado con �xito.", Toast.LENGTH_LONG).show();
					
					} else {
						Toast.makeText(ctx, "Error en inserci�n de cuenta predial. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
					}
					Log.i("showPlacaDialog", "cerramos dataSource");
					data.close();
					Log.i("showPlacaDialog", "despedimos dialog");
					dialog.dismiss();
				}
			}
		});
		
		AlertDialog alertDialog = alertDiaBuilder.create();
		alertDialog.show();
	}
	
	public static void loginAlert(final MainActivity con, final LongProcess process) {

		LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View loginDialog = inflater.inflate(R.layout.dialog_login, null);
		
		final EditText userEdit = (EditText) loginDialog.findViewById(R.id.edit_dialog_user);
		final EditText passEdit = (EditText) loginDialog.findViewById(R.id.edit_dialog_password);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(con);
		builder.setTitle(con.getString(R.string.dialog_login_title))
		.setMessage(con.getString(R.string.dialog_login_message))
		.setView(loginDialog)
		.setCancelable(false)
		.setNegativeButton(con.getResources().getString(R.string.placa_dialog_cancel_button), new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		})
		.setPositiveButton(con.getResources().getString(R.string.button_login), new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
			
				loginProcess = new LongProcess(con, true, 30, true, "login", new LongProcessListener() {
					
					public void stopProcess() {
						// TODO Auto-generated method stub
						Usuario.cancelarChequeoRegistro(con);
					}
					
					public void doProcess() {
						// TODO Auto-generated method stub

						final String user = userEdit.getText().toString().trim();
						final String pass = passEdit.getText().toString().trim();
						
						Usuario.estaRegistrado(con,user,pass, new OnUserRegistradoListener() {
							
							public void onUserRegistradoListener(boolean isRegistrado, String mensaje) {
								// TODO Auto-generated method stub
								if (isRegistrado) {
									
									String[] arrayMensaje = mensaje.split("\\|");
									String id = arrayMensaje[1];
									Usuario.setLogin(con, user);
									Usuario.setPass(con, pass);
									Usuario.setIdUser(con, id);
									
									if (null != process ) {
										process.start();
									} else {
										Toast.makeText(con, arrayMensaje[0], Toast.LENGTH_SHORT).show();
									}
									
								} else {
									ErrorSys.showMsj(con, mensaje);
								}
								loginProcess.processFinished();
							}
							
							public void onUserPassUpdate(String mensaje) {
								// TODO Auto-generated method stub
								Usuario.setLogin(con, user);
								Usuario.setPass(con, pass);
								con.startActivity(new Intent(con, UpdatePassMailActivity.class));
								loginProcess.processFinished();
							}
							
							public void onUserDataUpdate(String mensaje) {
								// TODO Auto-generated method stub
								Usuario.setLogin(con, user);
								Usuario.setPass(con, pass);
								Intent intent = new Intent(con, UpdatePassMailActivity.class);
								intent.putExtra("source", 0);
								con.startActivity(intent);
								loginProcess.processFinished();
							}
						});
					
					}
				});
				
				validateAndStartLoginProcess(userEdit, passEdit);
			}
		});
		
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private static void validateAndStartLoginProcess(EditText user, EditText pass) {
		if (user.getText().toString().length() == 0) {
			user.setError("Usuario Vac�o");
		} else if (user.getText().toString().length() < 8) {
			user.requestFocus();
			user.setError("Usuario debe ser mayor a 7 caracteres");
		} else if (user.getText().toString().length() > 16) {
			user.requestFocus();
			user.setError("Usuario debe ser menor a 13 caracteres");
		} else if (pass.getText().toString().length() == 0) {
			user.requestFocus();
			pass.setError("Password Vac�o");
		} else if (pass.getText().toString().length() < 8) {
			user.requestFocus();
			pass.setError("Password debe ser mayor a 7 caracteres");
		} else if (pass.getText().toString().length() > 12) {
			user.requestFocus();
			pass.setError("Password debe ser menor a 13 caracteres");
		} else {
			loginProcess.start();
		}
	}
	

	
	
	

}
