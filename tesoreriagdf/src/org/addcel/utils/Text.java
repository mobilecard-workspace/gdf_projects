package org.addcel.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.util.Log;

public class Text {
	public static final String SHARED_FILE = "MUNDOE_RECORD";
	public static final String SHARED_NAME = "USER_NAME";
	public static final String SHARED_MAIL = "USER_MAIL";
	
	public static String[] SECCIONES = {
		/*"Actuales",
		"Econom�a y Negocios",
		"Mundo Pyme",
		"Mujer Ejecutiva",
		"LifeStyle",
		"Responsabilidad Social",
		"Politica y Sociedad",
		"Salud",
		"Mundo Express",
		*/
	};
	
	
	 public static String getVersionName(Context context, Class<?> cls) 
	    {
	      try {
	        ComponentName comp = new ComponentName(context, cls);
	        PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
	        return pinfo.versionName;
	      } catch (android.content.pm.PackageManager.NameNotFoundException e) {
	        return null;
	      }
	    }
	 
	 public static boolean esCorreo(String correo){
			return (correo.matches("^[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\\.)*[a-zA-Z0-9-]{2,200}\\.[a-zA-Z]{2,6}$"));
	 }
	 
	public static Integer[] getYearsArray() {
		Calendar c = Calendar.getInstance();
		Integer[] years = new Integer[5];
		
		int i = 0;
		int year = c.get(Calendar.YEAR);
		
		while (i < 5) {
			
			years[i] = year;
			year--;
			i++;
		}
		
		Log.i("years array", Arrays.toString(years));
		
		return years;
		
	}
		
	 public static String formatDate(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		return format.format(date);
	 }	
	 
	 public static Date getDateFromString(String s) {
		 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
		 try {
			 Date fecha = format.parse(s);
			 Log.i("getDateFromString", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
		 
	 }
	 
	 public static String getKey() { 
		 return String.valueOf(new Date().getTime()).substring(0, 8);
		 
	 }
}
