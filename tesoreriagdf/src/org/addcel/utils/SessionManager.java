package org.addcel.utils;

import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
	SharedPreferences pref;
	Editor editor;
	Context _context;
	
	int PRIVATE_MODE = 0;
	
	private static final String PREF_NAME = "Data";
	private static final String IS_LOGIN = "IsLoggedIn";
	public static final String USR_ID = "usr_id";
	public static final String USR_LOGIN = "usr_login";
	public static final String USR_PWD = "usr_pwd";
	
	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	
	public void createLoginSession(String id, String login, String pwd) {
		editor.putBoolean(IS_LOGIN, true);
		editor.putString(USR_ID, id);
		editor.putString(USR_LOGIN, login);
		editor.putString(USR_PWD, pwd);
		editor.commit();
	}
	
	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		
		user.put(USR_ID, pref.getString(USR_ID, null));
		user.put(USR_LOGIN, pref.getString(USR_LOGIN, null));
		user.put(USR_PWD, pref.getString(USR_PWD, null));
		
		return user;
	}
	
	public void logoutUser() {
		editor.clear();
		editor.commit();
	}
	
	public boolean isLoggedIn() {
		return pref.getBoolean(IS_LOGIN, false);
	}
	
}
