package org.addcel.gdf.activities;

import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.dto.Detalle;
import org.addcel.gdftesoreria.R;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ironbit.mega.activities.MenuActivity;

public class DetalleActivity extends MenuActivity {
	
	private static final String TAG = "DetalleActivity";
	
	private TextView headerText, mensajeView, folioLabel, folioView;
	private TextView autorizacionLabel, autorizacionView, totalLabel;
	private TextView totalView, aclaracionesView;
	
	private Button finalizarButton, reintentarButton;
	
	private Detalle detalle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_detalle);
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		initData();
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		headerText = (TextView) findViewById(R.id.text_header);
		mensajeView = (TextView) findViewById(R.id.view_mensaje);
		folioLabel = (TextView) findViewById(R.id.label_folio);
		folioView = (TextView) findViewById(R.id.view_folio);
		autorizacionLabel = (TextView) findViewById(R.id.label_autorizacion);
		autorizacionView = (TextView) findViewById(R.id.view_autorizacion);
		totalLabel = (TextView) findViewById(R.id.label_total);
		totalView = (TextView) findViewById(R.id.view_total);
		aclaracionesView = (TextView) findViewById(R.id.view_aclaraciones);
		
		finalizarButton = (Button) findViewById(R.id.button_finalizar);
		reintentarButton = (Button) findViewById(R.id.button_reintentar);
		
		headerText.setTypeface(gothamBold);
		mensajeView.setTypeface(gothamBook);
		folioLabel.setTypeface(gothamBold);
		folioView.setTypeface(gothamBold);
		autorizacionLabel.setTypeface(gothamBold);
		autorizacionView.setTypeface(gothamBold);
		totalLabel.setTypeface(gothamBold);
		totalView.setTypeface(gothamBold);
		aclaracionesView.setTypeface(gothamBook);
		reintentarButton.setTypeface(gothamBook);
		
		finalizarButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		reintentarButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		setTexts();
	}
	
	private void initData() {
		detalle = getIntent().getParcelableExtra("detalle");
	}
	
	private void setTexts() {
		mensajeView.setText(detalle.getMensaje());
		folioView.setText(detalle.getFolio());
		
		if (detalle.isExitoso()) {
			autorizacionView.setText(detalle.getAutorizacion());
			totalView.setText(detalle.getMonto());
		} else {
			autorizacionLabel.setVisibility(View.GONE);
			autorizacionView.setVisibility(View.GONE);
			totalLabel.setVisibility(View.GONE);
			totalView.setVisibility(View.GONE);
			reintentarButton.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_DETALLE;
	}
	
	

}
