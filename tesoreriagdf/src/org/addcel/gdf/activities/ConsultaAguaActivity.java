package org.addcel.gdf.activities;

import org.addcel.gdf.constants.Fonts;
import org.addcel.gdftesoreria.R;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.web.webservices.GetGdfReenvioWSClient;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class ConsultaAguaActivity extends MenuActivity {
	
	private JSONObject json;
	private LongProcess reenvioProcess;
	private GetGdfReenvioWSClient reenvioClient;
	
	public JSONObject getJson()  {
		if (null == json) {
			try {
				json = new JSONObject(getIntent().getStringExtra("json"));
			} catch (JSONException e) {
				return null;
			}
		}
		
		return json;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_agua_consulta);
		
		
		
		String bimestre = getJson().optString("vbimestre");
		String anioBimestre = getJson().optString("vanio");
		String cuenta = getJson().optString("cuenta");
		String captura = getJson().optString("linea_captura");
		double derecho = getJson().optDouble("vderdom", 0.0) + getJson().optDouble("vderndom", 0.0);
		double iva = getJson().optDouble("viva");
		String total = getJson().optString("totalPago");
		
		((TextView) findViewById(R.id.text_bimestre)).setText(bimestre);
		((TextView) findViewById(R.id.text_anio_bimestre)).setText(anioBimestre);
		((TextView) findViewById(R.id.text_cuenta)).setText(cuenta);
		((TextView) findViewById(R.id.text_captura)).setText(captura);
		((TextView) findViewById(R.id.text_derecho)).setText(Text.formatCurrency(derecho, true));
//		((TextView) findViewById(R.id.text_iva)).setText(iva);
		((TextView) findViewById(R.id.text_total)).setText("$ " + total);
	
		reenvioProcess = new LongProcess(this, true, 30, true, "reenvio", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != reenvioClient) {
					reenvioClient.cancel();
					reenvioClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				reenvioClient = new GetGdfReenvioWSClient(ConsultaAguaActivity.this);
				reenvioClient.setIdBitacora(getJson().optString("id_bitacora"));
				reenvioClient.setIdProducto(getJson().optInt("id_producto"));
				reenvioClient.executeClient(new OnResponseJSONReceivedListener() {
					
					public void onResponseJSONReceived(JSONObject jsonResponse) {
						// TODO Auto-generated method stub
						String error = jsonResponse.optString("error");
						Toast.makeText(ConsultaAguaActivity.this, error, Toast.LENGTH_SHORT).show();
						reenvioProcess.processFinished();
					}
				});
			}
		});
		

		findViewById(R.id.button_reenviar).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				reenvioProcess.start();
			}
		});
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_CONSULTA_AGUA;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		
		((TextView) findViewById(R.id.label_bimestre)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_bimestre)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_anio_bimestre)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_anio_bimestre)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_cuenta)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_cuenta)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_captura)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_captura)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_derecho)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_derecho)).setTypeface(gothamBold);
		
//		((TextView) findViewById(R.id.label_iva)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_iva)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.text_total)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.label_total)).setTypeface(gothamBold);
		
		findViewById(R.id.button_atras).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	
	}

}
