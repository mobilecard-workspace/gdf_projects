package org.addcel.gdf.activities;

import java.util.Arrays;
import java.util.List;

import org.addcel.gdf.adapters.GdfListAdapter;
import org.addcel.gdf.constants.ConstantesGdf;
import org.addcel.gdf.constants.Fonts;
import org.addcel.gdftesoreria.R;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.system.contador.Contador;
import com.ironbit.mega.system.contador.OnContadorTerminadoListener;
import com.ironbit.mega.usuario.Usuario;

public class DepartamentoActivity extends MenuActivity {
	public static final int WAIT = 1000;
	
	private TextView welcomeTextView;
	private TextView secretariaText;
	private TextView finanzasText;
	private List<String> departamentos;
	
	public TextView getSecretariaText() {
		if (secretariaText == null) {
			secretariaText = (TextView) findViewById(R.id.text_secretaria);
		}
		
		return secretariaText;
	}
	
	public TextView getFinanzasText() {
		if (finanzasText == null) {
			finanzasText = (TextView) findViewById(R.id.text_finanzas);
		}
		
		return finanzasText;
	}
	
	public TextView getWelcomeTextView() {
		if (welcomeTextView == null) {
			welcomeTextView = (TextView) findViewById(R.id.texto_login);
		}
		
		return welcomeTextView;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.gdf_departamentos);
		
		if (ConstantesGdf.IS_LOGGED) {
			getWelcomeTextView().setText(getWelcomeTextView().getText().toString() + Usuario.getLogin(DepartamentoActivity.this));
			getWelcomeTextView().setVisibility(View.VISIBLE);
		}
	
		new Contador(new Handler(), new OnContadorTerminadoListener() {
			
			public void onContadorTerminado() {
				// TODO Auto-generated method stub
				departamentos = Arrays.asList(getResources().getStringArray(R.array.arr_deptos));
				getTxtList().setAdapter(new GdfListAdapter(DepartamentoActivity.this, departamentos));
				getTxtList().setOnItemClickListener(new OnItemClickListener() {

					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(DepartamentoActivity.this, TramiteActivity.class);
						intent.putExtra("departamento", arg2);
						startActivity(intent);
					}
				});
			}
		}, 2);
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_DEPTOS;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID){
		super.inicializarGUI(layoutResID);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		getSecretariaText().setTypeface(gothamBook);
		getFinanzasText().setTypeface(gothamBold);
		
		findViewById(R.id.button_atras).setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
	}
	
	protected ListView getTxtList(){
		return (ListView)findViewById(R.id.departamentos_lista);
	}
	
	@Override
	public void onBackPressed(){
		super.onBackPressed();
	}

}
