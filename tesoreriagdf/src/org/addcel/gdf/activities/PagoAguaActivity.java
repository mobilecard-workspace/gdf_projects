package org.addcel.gdf.activities;

import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.constants.Servicios;
import org.addcel.gdftesoreria.R;
import org.addcel.utils.Dialogos;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.GetGdfAguaWSClient;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class PagoAguaActivity extends MenuActivity {
	
	private Button consultarButton;
	private LongProcess consultarProcess;
	private GetGdfAguaWSClient consultarClient;
	private EditText cuentaEdit;
	private double derechoSum;
	
	private JSONObject requestPago;
	
	public EditText getCuentaEdit() {
		if (null == cuentaEdit) {
			cuentaEdit = (EditText) findViewById(R.id.edit_cuenta);
		}
		
		return cuentaEdit;
	}
	
	public Button getConsultarButton() {
		if (null == consultarButton) {
			consultarButton = (Button) findViewById(R.id.button_consultar);
		}
		
		return consultarButton;
	}
	
	public double getDerechoSum() {
		return derechoSum;
	}
	
	public void setDerechoSum(double derechoSum) {
		this.derechoSum = derechoSum;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.gdf_agua_pago);
		
		consultarProcess = new LongProcess(this, true, 30, true, "consulta", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub	
				if (null != consultarClient) {
					consultarClient.cancel();
					consultarClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				consultarClient = new GetGdfAguaWSClient(PagoAguaActivity.this);
				consultarClient.setCuenta(getCuentaEdit().getText().toString().trim());
				consultarClient.execute(new OnResponseJSONReceivedListener() {
					
					public void onResponseJSONReceived(JSONObject jsonResponse) {
						// TODO Auto-generated method stub
						
						if (null != jsonResponse) {
							
							requestPago = jsonResponse;
							String numErrorString = jsonResponse.optString("numError", "-1");
							String error = jsonResponse.optString("error", "Error de conexi�n. Int�nte de nuevo m�s tarde");
							int numError = -1;
							
							try {
								numError = Integer.valueOf(numErrorString);
								
								switch (numError) {
									case 0:
										setDerechoSum(jsonResponse.optInt("vderdom") + jsonResponse.optInt("vderndom"));
										
										String bimestre = jsonResponse.optString("vbimestre");
										String anioBimestre = jsonResponse.optString("vanio");
										String cuenta = jsonResponse.optString("cuenta");
										String captura = jsonResponse.optString("linea_captura");
										double derecho = getDerechoSum();
										double iva = jsonResponse.optDouble("viva", 0.0);
										String total = jsonResponse.optString("totalPago");
									
										
										((TextView) findViewById(R.id.text_bimestre)).setText(bimestre);
										((TextView) findViewById(R.id.text_anio_bimestre)).setText(anioBimestre);
										((TextView) findViewById(R.id.text_cuenta)).setText(cuenta);
										((TextView) findViewById(R.id.text_captura)).setText(captura);
										((TextView) findViewById(R.id.text_derecho)).setText(Text.formatCurrency(derecho, true));
//										((TextView) findViewById(R.id.text_iva)).setText(Text.formatCurrency(iva, true));
										((TextView) findViewById(R.id.text_total)).setText("$ " + total);
										
										findViewById(R.id.layout_datos).setVisibility(View.VISIBLE);
										break;
	
									default:
										Toast.makeText(PagoAguaActivity.this, error, Toast.LENGTH_SHORT).show();
										break;
								}
								
							} catch (NumberFormatException e) {
								Toast.makeText(PagoAguaActivity.this, error, Toast.LENGTH_SHORT).show();
								Log.e("PagoAguaActivity", "Error en formato de error", e);
							} finally {
								consultarProcess.processFinished();
							}
								
						}
						
						consultarProcess.processFinished();
					}
				});
			}
		});
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_PAGO_AGUA;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		
		((TextView) findViewById(R.id.label_bimestre)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_bimestre)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_anio_bimestre)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_anio_bimestre)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_cuenta)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_cuenta)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_captura)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_captura)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_derecho)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_derecho)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_iva)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_iva)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.text_total)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.label_total)).setTypeface(gothamBold);
		
		findViewById(R.id.button_atras).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		getConsultarButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String idUser = Usuario.getIdUser(PagoAguaActivity.this);
				
				if (cuentaValida()) {
					if (null == idUser || "".equals(idUser)) {
						Dialogos.loginAlert(PagoAguaActivity.this, consultarProcess);
					} else {
						consultarProcess.start();
					}
				}
				
			}
		});
		
		findViewById(R.id.pagar_button).setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (null != requestPago) {
					
					putDerechoSum(getDerechoSum());
					
					Intent i = new Intent(PagoAguaActivity.this, PagoActivity.class);
					i.putExtra("datos", requestPago.toString());
					i.putExtra("servicio", Servicios.AGUA);
					startActivity(i);
					finish();
				} else {
					Toast.makeText(PagoAguaActivity.this, "Consulta tus datos nuevamente.", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		getCuentaEdit().addTextChangedListener(new TextWatcher() {
			
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				((TextView) findViewById(R.id.text_contador)).setText(String.valueOf(16 - arg0.length()));
			}
			
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	private void putDerechoSum(double sum) {
		if (null != requestPago) {
			try {
				requestPago.put("vderecho", Double.toString(sum));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private boolean cuentaValida() {
		
		String cuenta = getCuentaEdit().getText().toString().trim();
		
		if (null == cuenta|| "".equals(cuenta)) {
			getCuentaEdit().setError("Ingresa una cuenta v�lida");
			return false;
		}
		
		return true;
	}

}
