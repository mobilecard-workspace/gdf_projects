package org.addcel.gdf.activities;

import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.dto.TenenciaResponse;
import org.addcel.gdftesoreria.R;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.web.webservices.GetGdfReenvioWSClient;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class ConsultaTenenciaActivity extends MenuActivity {
	
	private JSONObject json;
	private TenenciaResponse tenencia;
	private LongProcess reenvioProcess;
	private GetGdfReenvioWSClient reenvioClient;
	
	public JSONObject getJson()  {
		if (null == json) {
			try {
				json = new JSONObject(getIntent().getStringExtra("json"));
			} catch (JSONException e) {
				return null;
			}
		}
		
		return json;
	}
	
	public TenenciaResponse getTenencia() {
		
		if (null == tenencia) {
			JSONObject jsonResponse = getJson();
			
			tenencia = new TenenciaResponse();
			tenencia.setTenencia(jsonResponse.optDouble("tenencia", 0.0));
			tenencia.setActualizacion(jsonResponse.optDouble("tenActualizacion", 0.0));
			tenencia.setRecargos(jsonResponse.optDouble("recargos", 0.0));
			tenencia.setRefrendo(jsonResponse.optDouble("derecho", 0.0));
			tenencia.setActRefrendo(jsonResponse.optDouble("derActualizacion", 0.0));
			tenencia.setRecRefrendo(jsonResponse.optDouble("derRecargo", 0.0));
			tenencia.setTotal(jsonResponse.optString("totalPago"));
			tenencia.setVigencia(jsonResponse.optString("vigencia"));
			tenencia.setLineaCaptura(jsonResponse.optString("linea_captura"));
			tenencia.setModelo(jsonResponse.optInt("modelo"));
		}
		
		return tenencia;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_tenencia_consulta);
		
		((TextView) findViewById(R.id.gdf_tenencia_text)).setText(Text.formatCurrency(getTenencia().getTenencia(), true));
		((TextView) findViewById(R.id.gdf_recargos_tenencia_text)).setText(Text.formatCurrency(getTenencia().getRecargos(), true));
		((TextView) findViewById(R.id.gdf_refrendo_text)).setText(Text.formatCurrency(getTenencia().getRefrendo(), true));
		((TextView) findViewById(R.id.gdf_recargo_refrendo_text)).setText(Text.formatCurrency(getTenencia().getRecRefrendo(), true));
		((TextView) findViewById(R.id.gdf_lineacaptura_edit)).setText(getTenencia().getLineaCaptura());
		((TextView) findViewById(R.id.gdf_monto_edit)).setText("$" + getTenencia().getTotal());
		
		((TextView) findViewById(R.id.gdf_actualizacion_text)).setText(Text.formatCurrency(getTenencia().getActualizacion(), true));
		findViewById(R.id.tenencia_actualizacion_layout).setVisibility(View.VISIBLE);
		
		((TextView) findViewById(R.id.gdf_actualizacion_refrendo_text)).setText(Text.formatCurrency(getTenencia().getActRefrendo(),true));

		findViewById(R.id.refrendo_actualizacion_layout).setVisibility(View.VISIBLE);
		
		reenvioProcess = new LongProcess(this, true, 30, true, "reenvio", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != reenvioClient) {
					reenvioClient.cancel();
					reenvioClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				reenvioClient = new GetGdfReenvioWSClient(ConsultaTenenciaActivity.this);
				reenvioClient.setIdBitacora(getJson().optString("id_bitacora"));
				reenvioClient.setIdProducto(getJson().optInt("id_producto"));
				reenvioClient.executeClient(new OnResponseJSONReceivedListener() {
					
					public void onResponseJSONReceived(JSONObject jsonResponse) {
						// TODO Auto-generated method stub
						String error = jsonResponse.optString("error");
						Toast.makeText(ConsultaTenenciaActivity.this, error, Toast.LENGTH_SHORT).show();
						reenvioProcess.processFinished();
					}
				});
			}
		});
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_CONSULTA_TENENCIA;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);	
		
		((TextView) findViewById(R.id.label_tenencia)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_tenencia_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_actualizacion)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_actualizacion_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_recargos_tenencia)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_recargos_tenencia_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_refrendo)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_refrendo_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_actualizacion_refrendo)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_actualizacion_refrendo_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_recargo_refrendo)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_recargo_refrendo_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_lineacaptura)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_lineacaptura_edit)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_monto_edit)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.label_monto)).setTypeface(gothamBold);
		
		findViewById(R.id.button_atras).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		findViewById(R.id.button_reenviar).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				reenvioProcess.start();
			}
		});
		
	}

}
