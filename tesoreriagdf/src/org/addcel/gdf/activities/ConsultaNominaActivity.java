package org.addcel.gdf.activities;

import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.dto.NominaResponse;
import org.addcel.gdftesoreria.R;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.web.webservices.GetGdfReenvioWSClient;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class ConsultaNominaActivity extends MenuActivity {
	
	private JSONObject json;
	private NominaResponse nomina;
	private LongProcess reenvioProcess;
	private GetGdfReenvioWSClient reenvioClient;
	
	public JSONObject getJson()  {
		if (null == json) {
			try {
				json = new JSONObject(getIntent().getStringExtra("json"));
			} catch (JSONException e) {
				return null;
			}
		}
		
		return json;
	}
	
	public NominaResponse getNomina(){
		if (null == nomina) {
			
			double impuesto = getJson().optDouble("impuesto");
			double impuestoActualizado = getJson().optDouble("impuesto_actualizado");
			String clave = getJson().optString("clave");
			String rfc = getJson().optString("rfc");
			String anio = getJson().optString("anio_pago");
			String mes = getJson().optString("mes_pago");
			int numTrabajadores = getJson().optInt("num_trabajadores");
			int remuneraciones = getJson().optInt("remuneraciones");
			double recargos = getJson().optDouble("recargos");
			String total = getJson().optString("totalPago");
			String vigencia = getJson().optString("vigencia");
			String lineaCaptura = getJson().optString("linea_captura");
			
			if (impuesto == impuestoActualizado) {
				nomina = new NominaResponse(clave, rfc,anio, mes, remuneraciones, impuesto, recargos, total, vigencia, lineaCaptura, numTrabajadores);
			} else {
				nomina = new NominaResponse(clave, rfc, anio, mes, remuneraciones, impuesto, impuestoActualizado, recargos, total, vigencia, lineaCaptura, numTrabajadores);
			}
		}
		
		return nomina;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_nomina_consulta);

		((TextView) findViewById(R.id.gdf_rfc_text)).setText(getNomina().getRfc());
		((TextView) findViewById(R.id.gdf_anio_text)).setText(getNomina().getAnio());
		((TextView) findViewById(R.id.gdf_mes_text)).setText(getNomina().getMes());
		((TextView) findViewById(R.id.gdf_trabajadores_text)).setText(""+  getNomina().getNumTrabajadores());
		((TextView) findViewById(R.id.gdf_nomina_gravadas_text)).setText("" + getNomina().getRemuneraciones());
		((TextView) findViewById(R.id.gdf_nomina_impuesto_text)).setText("" + getNomina().getImpuesto());
		((TextView) findViewById(R.id.gdf_nomina_recargos_text)).setText("" + getNomina().getRecargos());
		((TextView) findViewById(R.id.gdf_nomina_total_text)).setText("$ " + getNomina().getTotal());
		((TextView) findViewById(R.id.gdf_nomina_captura_text)).setText(getNomina().getLineaCaptura());
		
		if (getNomina().getImpuestoActualizado() > getNomina().getImpuesto()) {
			((TextView) findViewById(R.id.gdf_nomina_actualizacion_text)).setText("" + getNomina().getImpuestoActualizado());
			findViewById(R.id.gdf_actualizacion_layout).setVisibility(View.VISIBLE);
		} else {
			if (findViewById(R.id.gdf_actualizacion_layout).getVisibility() == View.VISIBLE) {
				findViewById(R.id.gdf_actualizacion_layout).setVisibility(View.GONE);
			}
		}
		
		reenvioProcess = new LongProcess(this, true, 30, true, "reenvio", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != reenvioClient) {
					reenvioClient.cancel();
					reenvioClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				reenvioClient = new GetGdfReenvioWSClient(ConsultaNominaActivity.this);
				reenvioClient.setIdBitacora(getJson().optString("id_bitacora"));
				reenvioClient.setIdProducto(getJson().optInt("id_producto"));
				reenvioClient.executeClient(new OnResponseJSONReceivedListener() {
					
					public void onResponseJSONReceived(JSONObject jsonResponse) {
						// TODO Auto-generated method stub
						String error = jsonResponse.optString("error");
						Toast.makeText(ConsultaNominaActivity.this, error, Toast.LENGTH_SHORT).show();
						reenvioProcess.processFinished();
					}
				});
			}
		});
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_CONSULTA_NOMINA;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		
		((TextView) findViewById(R.id.label_rfc)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_rfc_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_anio)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_anio_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_mes)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_mes_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_trabajadores)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_trabajadores_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_gravadas)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_gravadas_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_impuesto)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_impuesto_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_actualizacion)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_actualizacion_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_recargos)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_recargos_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_total)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_total_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_captura)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_captura_text)).setTypeface(gothamBold);
		
		findViewById(R.id.button_atras).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		findViewById(R.id.button_reenviar).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				reenvioProcess.start();
			}
		});
	}

}
