 package org.addcel.gdf.activities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.addcel.gdf.adapters.GdfListAdapter;
import org.addcel.gdf.constants.Departamento;
import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.constants.Tramite;
import org.addcel.gdftesoreria.R;
import org.addcel.utils.Dialogos;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.usuario.Usuario;

public class TramiteActivity extends MenuActivity {
	
	private TextView headerText;
	
//	GUI POPUP
	private EditText loginEditText;
	private EditText passwordEditText;
	
//	LISTA TRAMITES
	private List<String> tramites = new ArrayList<String>();
	
//	DEPARTAMENTO
	private int departamento;
	
//	TRAMITE SELECCIONADO
	private String tramiteSeleccionado;
	
	public TextView getHeaderText() {
		if (headerText == null) {
			headerText = (TextView) findViewById(R.id.header_tramites_text);
		}
		
		return headerText;
	}
	
	public EditText getLoginEditText(View popup) {
		if (loginEditText == null) {
			loginEditText = (EditText) popup.findViewById(R.id.login_edit);
		}
		
		return loginEditText;
	}
	
	public EditText getPasswordEditText(View popup) {
		if (passwordEditText == null) {
			passwordEditText = (EditText) popup.findViewById(R.id.password_edit);
		}
		
		return passwordEditText;
	}
	
	public int getDepartamento() {
			departamento = getIntent().getIntExtra("departamento", -1);
		
		
		return departamento;
	}
	
	public List<String> getTramites() {
		if (Departamento.VEHICULOS == getDepartamento()) {
			tramites = Arrays.asList(getResources().getStringArray(R.array.arr_vehiculos));
		} else if (Departamento.EMPRESAS == getDepartamento()) {
			tramites = Arrays.asList(getResources().getStringArray(R.array.arr_nomina));
		} else if (Departamento.INMUEBLES == getDepartamento()) {
			tramites = Arrays.asList(getResources().getStringArray(R.array.arr_otros));
		}
		
		return tramites;
	}

	public String getTramiteSeleccionado() {
		return tramiteSeleccionado;
	}

	public void setTramiteSeleccionado(String tramiteSeleccionado) {
		this.tramiteSeleccionado = tramiteSeleccionado;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.gdf_tramites);
		
		if (null == Usuario.getIdUser(TramiteActivity.this) || "".equals(Usuario.getIdUser(TramiteActivity.this))) {
			Dialogos.showInitialDialog(TramiteActivity.this);
		}
		
		getTxtList().setAdapter(new GdfListAdapter(TramiteActivity.this, getTramites()));
		getTxtList().setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				
				if (Departamento.INMUEBLES == getDepartamento()) {
					if (arg2 == Tramite.PREDIAL) {
						Intent intent = new Intent(TramiteActivity.this, PagoPredialActivity.class);
						startActivity(intent);
						
					} else if (arg2 == Tramite.AGUA) {
						Intent intent = new Intent(TramiteActivity.this, PagoAguaActivity.class);
						startActivity(intent);}
				}

				if (Departamento.VEHICULOS == getDepartamento()) {
					if (arg2 == Tramite.TENENCIA) {
						Intent intent = new Intent(TramiteActivity.this, PagoTenenciaActivity.class);
						startActivity(intent);
						
					} else if (arg2 == Tramite.INFRACCIONES) {
						Intent intent = new Intent(TramiteActivity.this, PagoInfraccionesActivity.class);
						startActivity(intent);
					}
				}
					
				if (Departamento.EMPRESAS == getDepartamento()) {
					if (arg2 == Tramite.NOMINA) {
						Intent intent = new Intent(TramiteActivity.this, PagoNominaActivity.class);
						startActivity(intent);
					}
				}
				
			}
		});
			
	}
	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_TRAMITES;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID){
		super.inicializarGUI(layoutResID);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		
		findViewById(R.id.button_atras).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		String deptoFromArray = "";
		
		if (Departamento.ERROR == getDepartamento()) {
			
			Toast.makeText(TramiteActivity.this, "Tr�mites no disponibles", Toast.LENGTH_LONG).show();
			onBackPressed();
			
		} else {
			deptoFromArray = getResources().getStringArray(R.array.arr_deptos)[getDepartamento()];

			String departamentoNew = getHeaderText().getText().toString().replace("Deptos", deptoFromArray);
			getHeaderText().setText(departamentoNew);
			getHeaderText().setTypeface(gothamBook);
		}
	}
	
	protected ListView getTxtList(){
		return (ListView)findViewById(R.id.tramites_lista);
	}
	
	@Override
	public void onBackPressed(){
		super.onBackPressed();
	}
	
}
