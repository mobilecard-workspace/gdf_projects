package org.addcel.gdf.activities;

import java.util.Calendar;

import org.addcel.gdf.constants.Fonts;
import org.addcel.gdftesoreria.R;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.web.webservices.GetGdfReenvioWSClient;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class ConsultaPredialActivity extends MenuActivity {
	
	private JSONObject json;
	private LongProcess reenvioProcess;
	private GetGdfReenvioWSClient reenvioClient;
	
	public JSONObject getJson()  {
		if (null == json) {
			try {
				json = new JSONObject(getIntent().getStringExtra("json"));
			} catch (JSONException e) {
				return null;
			}
		}
		
		return json;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_predial_consulta);
		
		String anio = getAnioActual();
		
		String periodo = getJson().optString("bimestre");
		String cuentaP = getJson().optString("cuentaP");
		double importe = getJson().optDouble("importe", 0.0);
		int intImpuesto = getJson().optInt("intImpuesto");
		String linCap = getJson().optString("linea_captura");
//		String reduccion = getJson().optString("reduccion");
		String total = getJson().optString("totalPago");
//		String vencimiento = getJson().optString("vencimiento");
		
		
//		((TextView) findViewById(R.id.text_anio)).setText(anio);
		((TextView) findViewById(R.id.text_bimestre)).setText(periodo);
		((TextView) findViewById(R.id.text_derecho)).setText(Text.formatCurrency(importe, true));
//		((TextView) findViewById(R.id.text_iva)).setText("" + intImpuesto);
//		((TextView) findViewById(R.id.text_concepto)).setText(concepto);
		((TextView) findViewById(R.id.text_predial)).setText(cuentaP);
//		((TextView) findViewById(R.id.text_importe)).setText(importe);
		((TextView) findViewById(R.id.text_captura)).setText(linCap);
//		((TextView) findViewById(R.id.text_reduccion)).setText(reduccion);
		((TextView) findViewById(R.id.text_total)).setText("$" + total);
//		((TextView) findViewById(R.id.text_vencimiento)).setText(vencimiento);
	
		reenvioProcess = new LongProcess(this, true, 30, true, "reenvio", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != reenvioClient) {
					reenvioClient.cancel();
					reenvioClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				reenvioClient = new GetGdfReenvioWSClient(ConsultaPredialActivity.this);
				reenvioClient.setIdBitacora(getJson().optString("id_bitacora"));
				reenvioClient.setIdProducto(getJson().optInt("id_producto"));
				reenvioClient.executeClient(new OnResponseJSONReceivedListener() {
					
					public void onResponseJSONReceived(JSONObject jsonResponse) {
						// TODO Auto-generated method stub
						String error = jsonResponse.optString("error");
						Toast.makeText(ConsultaPredialActivity.this, error, Toast.LENGTH_SHORT).show();
						reenvioProcess.processFinished();
					}
				});
			}
		});
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_CONSULTA_PREDIAL;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);

//		((TextView) findViewById(R.id.label_anio)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_anio)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_bimestre)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_bimestre)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_derecho)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_derecho)).setTypeface(gothamBold);
		

//		((TextView) findViewById(R.id.label_iva)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_iva)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_predial)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_predial)).setTypeface(gothamBold);
		
//		((TextView) findViewById(R.id.label_vencimiento)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_vencimiento)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_captura)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_captura)).setTypeface(gothamBold);
		
//		((TextView) findViewById(R.id.label_importe)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_importe)).setTypeface(gothamBold);
		
//		((TextView) findViewById(R.id.label_reduccion)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_reduccion)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.text_total)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.label_total)).setTypeface(gothamBold);

		findViewById(R.id.button_atras).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		findViewById(R.id.button_reenviar).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				reenvioProcess.start();
			}
		});
	}
	
	private String getAnioActual() {
		Calendar calendar = Calendar.getInstance();
		int anio = calendar.get(Calendar.YEAR);
		
		return Integer.toString(anio);
	}

}
