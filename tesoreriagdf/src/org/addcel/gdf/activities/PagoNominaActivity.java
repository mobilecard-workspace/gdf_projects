package org.addcel.gdf.activities;

import java.util.LinkedList;
import java.util.List;

import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.constants.Servicios;
import org.addcel.gdf.constants.URL;
import org.addcel.gdf.dto.NominaResponse;
import org.addcel.gdf.util.JSONUtil;
import org.addcel.gdftesoreria.R;
import org.addcel.utils.Dialogos;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.form.validator.FormValidator;
import com.ironbit.mega.form.validator.Validable;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.system.Validador;
import com.ironbit.mega.system.errores.ErrorSys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.GetGdfNominaWSClient;
import com.ironbit.mega.web.webservices.InsertPagoBitacoraWSClient;
import com.ironbit.mega.web.webservices.events.OnGdfNominaResponseReceivedListener;

public class PagoNominaActivity extends MenuActivity {

	private TextView headerText;

	private EditText rfcEdit;
	private EditText confRfcEdit;
	private Spinner mesSpinner;
	private Spinner anioSpinner;
	private EditText remuneracionesEdit;
	private EditText numTrabajadoresEdit;
	private Button verificaButton;
	private Button pagoButton;

	// LAYOUT NOMINA
	private LinearLayout nominaLayout;
	private LinearLayout actualizacionLayout;

	// TEXTVIEWS DESGLOSE
	private TextView gravadasText;
	private TextView impuestoText;
	private TextView actualizacionText;
	private TextView recargosText;
	private TextView totalText;
	private TextView capturaText;
	// PROCESOS
	private LongProcess verificaProcess;
	private LongProcess pagoProcess;

	// SERVICIOS
	private GetGdfNominaWSClient verificaClient;
	private InsertPagoBitacoraWSClient pagoClient;

	// NOMINA RESPONSE
	private NominaResponse response;
	private JSONObject RequestJSON;
	private long idBitacora;

	// VALIDADOR FORM
	private FormValidator validator;

	public NominaResponse getResponse() {
		return response;
	}

	public void setResponse(NominaResponse response) {
		this.response = response;
	}

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}

	public FormValidator getFormValidator() {
		if (validator == null) {
			validator = new FormValidator(this, true);
		}

		return validator;
	}

	public TextView getHeaderTextView() {
		if (headerText == null) {
			headerText = (TextView) findViewById(R.id.header_text);
		}

		return headerText;
	}

	public EditText getRfcEdit() {
		if (rfcEdit == null) {
			rfcEdit = (EditText) findViewById(R.id.gdf_rfc_edit);
		}

		return rfcEdit;
	}
	
	public EditText getConfRfcEdit() {
		if (null == confRfcEdit) {
			confRfcEdit = (EditText) findViewById(R.id.gdf_conf_rfc_edit);
		}
		
		return confRfcEdit;
	}

	public Spinner getMesSpinner() {
		if (mesSpinner == null) {
			mesSpinner = (Spinner) findViewById(R.id.mes_spinner);
		}

		return mesSpinner;
	}

	public Spinner getAnioSpinner() {
		if (anioSpinner == null) {
			anioSpinner = (Spinner) findViewById(R.id.anios_spinner);
		}

		return anioSpinner;
	}

	public EditText getRemuneracionesEdit() {
		if (remuneracionesEdit == null) {
			remuneracionesEdit = (EditText) findViewById(R.id.gdf_nomina_remuneraciones_edit);
		}

		return remuneracionesEdit;
	}

	public EditText getNumTrabajadoresEdit() {
		if (numTrabajadoresEdit == null) {
			numTrabajadoresEdit = (EditText) findViewById(R.id.gdf_nomina_trabajadores_edit);
		}

		return numTrabajadoresEdit;
	}

	public Button getVerificarButton() {
		if (verificaButton == null) {
			verificaButton = (Button) findViewById(R.id.gdf_verificar_button);
		}

		return verificaButton;
	}

	public LinearLayout getNominaLayout() {
		if (nominaLayout == null) {
			nominaLayout = (LinearLayout) findViewById(R.id.gdf_nomina_layout);
		}

		return nominaLayout;
	}

	public LinearLayout getActualizacionLayout() {
		if (actualizacionLayout == null) {
			actualizacionLayout = (LinearLayout) findViewById(R.id.gdf_actualizacion_layout);
		}

		return actualizacionLayout;
	}

	public TextView getGravadasText() {
		if (gravadasText == null) {
			gravadasText = (TextView) findViewById(R.id.gdf_nomina_gravadas_text);
		}

		return gravadasText;
	}

	public TextView getImpuestoText() {
		if (impuestoText == null) {
			impuestoText = (TextView) findViewById(R.id.gdf_nomina_impuesto_text);
		}

		return impuestoText;
	}

	public TextView getActualizacionText() {
		if (actualizacionText == null) {
			actualizacionText = (TextView) findViewById(R.id.gdf_nomina_actualizacion_text);
		}

		return actualizacionText;
	}

	public TextView getRecargosText() {
		if (recargosText == null) {
			recargosText = (TextView) findViewById(R.id.gdf_nomina_recargos_text);
		}

		return recargosText;
	}

	public TextView getTotalText() {
		if (totalText == null) {
			totalText = (TextView) findViewById(R.id.gdf_nomina_total_text);
		}

		return totalText;
	}

	public TextView getCapturaText() {
		if (capturaText == null) {
			capturaText = (TextView) findViewById(R.id.gdf_nomina_captura_text);
		}

		return capturaText;
	}

	public Button getPagoButton() {
		if (pagoButton == null) {
			pagoButton = (Button) findViewById(R.id.gdf_ok_button);
		}

		return pagoButton;
	}

	@Override
	public void configValidations() {
		getFormValidator().addItem(getRfcEdit(), "RFC", new Validable() {

			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub

				String rfc = getRfcEdit().getText().toString().trim();

				if (!Validador.esRFC(rfc)) {
					getRfcEdit().requestFocus();
					throw new ErrorSys("Formato de RFC Inv�lido.");
				}
			}
		}).canBeEmpty(false).isRequired(true).minLength(12);
		
		getFormValidator().addItem(getConfRfcEdit(), "Confirmaci�n RFC", new Validable() {
			
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				
				String rfc = getRfcEdit().getText().toString().trim();
				String confRfc = getConfRfcEdit().getText().toString().trim();
				
				if (! rfc.equals(confRfc)) {
					throw new ErrorSys("No coinciden RFCs, favor de validar.");
				}
			}
		}).canBeEmpty(false).isRequired(true).minLength(12);

		getFormValidator().addItem(getRemuneracionesEdit(), "Remuneraciones", new Validable() {
			
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				try {
					int numTrabajadores = Integer.valueOf(getRemuneracionesEdit().getText().toString().trim());
					
					if (numTrabajadores <= 0) {
						throw new ErrorSys("Las remuneraciones debe ser mayores a 0");
					}
				} catch (NumberFormatException e) {
					throw new ErrorSys("Las remuneraciones debe ser mayores a 0");
				}
			}
		}).canBeEmpty(false).isRequired(true);

		getFormValidator().addItem(getNumTrabajadoresEdit(), "Num. Trabajadores", new Validable() {
			
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				
				try {
					int numTrabajadores = Integer.valueOf(getNumTrabajadoresEdit().getText().toString().trim());
					
					if (numTrabajadores <= 0) {
						throw new ErrorSys("El n�mero de trabajadores debe ser mayor a 0");
					}
				} catch (NumberFormatException e) {
					throw new ErrorSys("El n�mero de trabajadores debe ser mayor a 0");
				}
			}
		}).canBeEmpty(false).isRequired(true);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.gdf_nomina_pago);

		verificaProcess = new LongProcess(this, true, 30, true, "consulta",
				new LongProcessListener() {

					public void stopProcess() {
						// TODO Auto-generated method stub
						if (verificaClient != null) {
							verificaClient.cancel();
							verificaClient = null;
						}
					}

					public void doProcess() {
						// TODO Auto-generated method stub
						verificaClient = new GetGdfNominaWSClient(PagoNominaActivity.this);
						verificaClient.setRfc(getRfcEdit().getText().toString().trim());
						verificaClient.setAnio((String) getAnioSpinner().getSelectedItem());
						verificaClient.setMes((String) getMesSpinner().getSelectedItem());
						verificaClient.setNumTrabajadores(Integer.parseInt(getNumTrabajadoresEdit().getText().toString().trim()));
						verificaClient.setRemuneraciones(Integer.parseInt(getRemuneracionesEdit().getText().toString().trim()));
						verificaClient.execute(new OnGdfNominaResponseReceivedListener() {

									public void onGdfNominaResponseReceived(NominaResponse response,JSONObject jsonResponse) {
										// TODO Auto-generated method stub
										setResponse(response);
										
										RequestJSON = jsonResponse;
										
										((TextView) findViewById(R.id.gdf_rfc_text)).setText(response.getRfc());
										((TextView) findViewById(R.id.gdf_anio_text)).setText(response.getAnio());
										((TextView) findViewById(R.id.gdf_mes_text)).setText(response.getMes());
										
										getGravadasText().setText(""+ response.getRemuneraciones());
										getImpuestoText().setText("" + response.getImpuesto());
										getRecargosText().setText("" + response.getRecargos());
										getTotalText().setText("" + response.getTotal());
										getCapturaText().setText(response.getLineaCaptura());

										System.out.println("IMPUESTO: " + response.getImpuesto());
										System.out.println("IMPUESTO ACTUALIZADO: " + response.getImpuestoActualizado());

										getActualizacionText().setText(""+ response.getImpuestoActualizado());
										
										getNominaLayout().setVisibility(View.VISIBLE);

										verificaProcess.processFinished();
									}

									public void onGdfNominaErrorReceived(
											int error, String errorDescripcion) {
										// TODO Auto-generated method stub
										String errorAMostrar = "Lo sentimos, "+ errorDescripcion+ ". Int�ntelo m�s tarde.";
										Toast.makeText(PagoNominaActivity.this,errorAMostrar,Toast.LENGTH_LONG).show();
										verificaProcess.processFinished();
									}
								});
					}
				});

		pagoProcess = new LongProcess(this, true, 60, true, "pago",
				new LongProcessListener() {

					public void stopProcess() {
						// TODO Auto-generated method stub
						if (pagoClient != null) {
							pagoClient.cancel();
							pagoClient = null;
						}
					}

					public void doProcess() {
						// TODO Auto-generated method stub

						Intent intent = new Intent(PagoNominaActivity.this, PagoWebGdfActivity.class);

						Context con = PagoNominaActivity.this;
						String idUsuario = Usuario.getIdUser(PagoNominaActivity.this);
						String password = Usuario.getPass(PagoNominaActivity.this);
						int idProducto = Servicios.NOMINA;

						BasicNameValuePair param = new BasicNameValuePair("json", JSONUtil.getProcomRequestAsString(con,RequestJSON, idUsuario, idProducto));
						List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
						params.add(param);

						String url = Text.addParamsToUrl(URL.PROCOM_3D_SECURE_GDF, params);
						Log.i("url procom nomina", url);

						if (url != null && !url.equals("")) {
							intent.putExtra("url", url);
							intent.putExtra("tramite", "N�mina");
							startActivity(intent);
						} else {
							Toast.makeText(PagoNominaActivity.this,"Intente de nuevo m�s tarde.",Toast.LENGTH_SHORT).show();
						}

						pagoProcess.processFinished();

					}
				});

	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_PAGO_NOMINA;
	}

	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		Typeface gothamBook = Typeface.createFromAsset(getAssets(),Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(),Fonts.GOTHAM_BOLD);
		getHeaderTextView().setTypeface(gothamBook);

		((TextView) findViewById(R.id.label_desglose)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_rfc)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_rfc_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_anio)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_anio_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_mes)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_mes_text)).setTypeface(gothamBold);

		((TextView) findViewById(R.id.label_gravadas)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_gravadas_text)).setTypeface(gothamBold);

		((TextView) findViewById(R.id.label_impuesto)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_impuesto_text)).setTypeface(gothamBold);

		((TextView) findViewById(R.id.label_actualizacion)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_actualizacion_text)).setTypeface(gothamBold);

		((TextView) findViewById(R.id.label_recargos)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_recargos_text)).setTypeface(gothamBold);

		((TextView) findViewById(R.id.label_total)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_total_text)).setTypeface(gothamBold);

		((TextView) findViewById(R.id.label_captura)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_nomina_captura_text)).setTypeface(gothamBold);

		getVerificarButton().setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (getFormValidator().validate()) {
					if (!Usuario.getLogin(PagoNominaActivity.this).equals("")) {

						verificaProcess.start();

					} else {
						Dialogos.loginAlert(PagoNominaActivity.this, verificaProcess);
					}
				}
			}
		});

		getPagoButton().setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(PagoNominaActivity.this, PagoActivity.class);
				i.putExtra("datos", RequestJSON.toString());
				i.putExtra("servicio", Servicios.NOMINA);
				startActivity(i);
				finish();
			}
		});

		findViewById(R.id.button_atras).setOnClickListener(
				new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						onBackPressed();
					}
				});
	}

}
