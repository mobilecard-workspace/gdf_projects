 package org.addcel.gdf.activities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.addcel.gdf.adapters.ConsultaAdapter;
import org.addcel.gdf.constants.Servicios;
import org.addcel.gdf.dto.Operacion;
import org.addcel.gdftesoreria.R;
import org.addcel.utils.Dialogos;
import org.addcel.utils.Text;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.GetGdfConsultaWSClient;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class ConsultaActivity extends MenuActivity{
	
	private Spinner operacionSpinner;
	private Spinner mesSpinner;
	private Spinner anioSpinner;
	private List<JSONObject> consultas;
	
	private Operacion[] operaciones = { new Operacion(Servicios.TENENCIA, "Tenencia"), new Operacion(Servicios.INFRACCION, "Infracciones"),
									    new Operacion(Servicios.NOMINA, "N�mina"), new Operacion(Servicios.PREDIAL, "Predial"),
										new Operacion(Servicios.AGUA, "Agua")
									};
	
	private ArrayAdapter<Operacion> adapter;
	private ConsultaAdapter consultaAdapter;
	
	private LongProcess consultarProcess;
	private GetGdfConsultaWSClient consultarClient;
	
	private ListView consultaListView;
	private int servicioSeleccionado;
	
	public Spinner getOperacionSpinner() {
		if (null == operacionSpinner) {
			operacionSpinner = (Spinner) findViewById(R.id.spinner_operacion);
		}
		
		return operacionSpinner;
	}
	
	public ArrayAdapter<Operacion> getAdapter() {
		if (null == adapter) {
			adapter = new ArrayAdapter<Operacion>(ConsultaActivity.this, android.R.layout.simple_spinner_item, operaciones);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		}
		
		return adapter;
	}
	
	public Spinner getMesesSpinner() {
		if (mesSpinner == null) {
			mesSpinner = (Spinner) findViewById(R.id.cmbFechaVencimientoMes);
			ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.meses, android.R.layout.simple_spinner_item);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mesSpinner.setAdapter(adapter);
		}
		
		return mesSpinner;
		
	}
	
	public Spinner getAnioSpinner() {
		if (anioSpinner == null) {
			anioSpinner = (Spinner) findViewById(R.id.cmbFechaVencimientoAnio);
			ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, Text.getYearsArray());
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			anioSpinner.setAdapter(adapter);
		}
		
		return anioSpinner;
	}
	

	
	public ListView getConsultasListView() {
		if (null == consultaListView) {
			consultaListView = (ListView) findViewById(R.id.list_consulta);
		}
		
		return consultaListView;
	}
	
	public List<JSONObject> getConsultas() {
		if (null == consultas) {
			consultas = new ArrayList<JSONObject>();
		}
		
		return consultas;
	}
	
	public ConsultaAdapter getConsultaAdapter(int tipoServicio) {
		consultaAdapter = new ConsultaAdapter(ConsultaActivity.this, getConsultas(), tipoServicio);
		return consultaAdapter;
			
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_CONSULTA;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.gdf_consulta);
		
		consultarProcess = new LongProcess(this, true, 30, true, "consulta", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != consultarClient) {
					consultarClient.cancel();
					consultarClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				
				consultarClient = new GetGdfConsultaWSClient(ConsultaActivity.this);
				consultarClient.setIdProducto(((Operacion) getOperacionSpinner().getSelectedItem()).getValue());
				consultarClient.setMes("" + (getMesesSpinner().getSelectedItemPosition() + 1));
				consultarClient.setAnio(((Integer) getAnioSpinner().getSelectedItem()).toString());
				consultarClient.execute(new OnResponseJSONReceivedListener() {
					
					public void onResponseJSONReceived(JSONObject jsonResponse) {
						// TODO Auto-generated method stub
						try {
							Log.i("RESPUESTA CONSULTA", jsonResponse.toString(1));
							
							if (jsonResponse.has("consultaTenencia")) {
								JSONArray consultaTenencia = jsonResponse.optJSONArray("consultaTenencia");
								
								if (consultaTenencia.length() > 0) {
									consultas = new ArrayList<JSONObject>();
									
									for (int i = 0; i < consultaTenencia.length(); i++) {
										consultas.add(consultaTenencia.getJSONObject(i));
									}
									
									getConsultasListView().setAdapter(getConsultaAdapter(Servicios.TENENCIA));
									consultaAdapter.notifyDataSetChanged();
									
									servicioSeleccionado = Servicios.TENENCIA;
									
									consultarProcess.processFinished();
									
									
								} else {
									Toast.makeText(ConsultaActivity.this, "Sin datos", Toast.LENGTH_SHORT).show();
									consultarProcess.processFinished();
								}
								
							} else if (jsonResponse.has("consultaInfraccion")) {
								JSONArray consultaInfraccion = jsonResponse.optJSONArray("consultaInfraccion");
								
								if (consultaInfraccion.length() > 0) {
									consultas = new ArrayList<JSONObject>();
									
									for (int i = 0; i < consultaInfraccion.length(); i++) {
										consultas.add(consultaInfraccion.getJSONObject(i));
									}
									
									getConsultasListView().setAdapter(getConsultaAdapter(Servicios.INFRACCION));
									consultaAdapter.notifyDataSetChanged();
									

									servicioSeleccionado = Servicios.INFRACCION;
									
									consultarProcess.processFinished();
									
								} else {
									Toast.makeText(ConsultaActivity.this, "Sin datos", Toast.LENGTH_SHORT).show();
									consultarProcess.processFinished();
								}
								
							} else if (jsonResponse.has("consultaNomina")) {
								JSONArray consultaNomina = jsonResponse.optJSONArray("consultaNomina");
								
								if (consultaNomina.length() > 0) {
									consultas = new ArrayList<JSONObject>();
									
									for (int i = 0; i < consultaNomina.length(); i++) {
										consultas.add(consultaNomina.getJSONObject(i));
									}
									
									getConsultasListView().setAdapter(getConsultaAdapter(Servicios.NOMINA));
									consultaAdapter.notifyDataSetChanged();

									servicioSeleccionado = Servicios.NOMINA;
									
									consultarProcess.processFinished();
									
								} else {
									Toast.makeText(ConsultaActivity.this, "Sin datos", Toast.LENGTH_SHORT).show();
									consultarProcess.processFinished();
								}
								
							} else if (jsonResponse.has("consultaPredial")) {
								
								JSONArray consultaPredial = jsonResponse.optJSONArray("consultaPredial");
								
								if (consultaPredial.length() > 0) {
									consultas = new ArrayList<JSONObject>();
									
									for (int i = 0; i < consultaPredial.length(); i++) {
										consultas.add(consultaPredial.getJSONObject(i));
									}
									
									getConsultasListView().setAdapter(getConsultaAdapter(Servicios.PREDIAL));
									consultaAdapter.notifyDataSetChanged();
									
									servicioSeleccionado = Servicios.PREDIAL;
									
									consultarProcess.processFinished();
									
								} else {
									Toast.makeText(ConsultaActivity.this, "Sin datos", Toast.LENGTH_SHORT).show();
									consultarProcess.processFinished();
								}
							
							} else if (jsonResponse.has("consultaAgua")) {
								
								JSONArray consultaAgua = jsonResponse.optJSONArray("consultaAgua");
								
								if (consultaAgua.length() > 0) {
									consultas = new ArrayList<JSONObject>();
									
									for (int i = 0; i < consultaAgua.length(); i++) {
										consultas.add(consultaAgua.getJSONObject(i));
									}
									
									getConsultasListView().setAdapter(getConsultaAdapter(Servicios.AGUA));
									consultaAdapter.notifyDataSetChanged();
									
									servicioSeleccionado = Servicios.AGUA;
									
									consultarProcess.processFinished();
									
								} else {
									Toast.makeText(ConsultaActivity.this, "Sin datos", Toast.LENGTH_SHORT).show();
									consultarProcess.processFinished();
								}
							}
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							Toast.makeText(ConsultaActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
						} finally {
							consultarProcess.processFinished();
						}
					}
				});
			}
		});
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		getOperacionSpinner().setAdapter(getAdapter());
		getMesesSpinner();
		getAnioSpinner();
		

		
		getConsultasListView().setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				JSONObject json = (JSONObject) arg0.getAdapter().getItem(arg2);
				Intent i = null;
				
				switch (servicioSeleccionado) {
					case Servicios.TENENCIA:
						i = new Intent(ConsultaActivity.this, ConsultaTenenciaActivity.class);
						i.putExtra("json", json.toString());
						startActivity(i);
						break;
					case Servicios.INFRACCION:
						i = new Intent(ConsultaActivity.this, ConsultaInfraccionActivity.class);
						i.putExtra("json", json.toString());
						startActivity(i);
						break;
					case Servicios.NOMINA:
						i = new Intent(ConsultaActivity.this, ConsultaNominaActivity.class);
						i.putExtra("json", json.toString());
						startActivity(i);
						break;
					case Servicios.PREDIAL:
						i = new Intent(ConsultaActivity.this, ConsultaPredialActivity.class);
						i.putExtra("json", json.toString());
						startActivity(i);
						break;
					case Servicios.AGUA:
						i = new Intent(ConsultaActivity.this, ConsultaAguaActivity.class);
						i.putExtra("json", json.toString());
						startActivity(i);
						break;
					default:
						Toast.makeText(ConsultaActivity.this, "Intente de nuevo m�s tarde", Toast.LENGTH_LONG).show();
						break;
				}
			}
		});
		
		findViewById(R.id.button_atras).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		findViewById(R.id.button_consultar).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String usrId = Usuario.getIdUser(ConsultaActivity.this);
				
				if (null != usrId && ! "".equals(usrId)) {
					consultarProcess.start();
				} else {
					Dialogos.loginAlert(ConsultaActivity.this, consultarProcess);
				}
			}
		});
		
	}

}
