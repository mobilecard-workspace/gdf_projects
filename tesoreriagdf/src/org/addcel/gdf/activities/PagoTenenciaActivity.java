package org.addcel.gdf.activities;

import java.util.LinkedList;
import java.util.List;

import org.addcel.gdf.constants.ConstantesGdf;
import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.constants.Servicios;
import org.addcel.gdf.constants.URL;
import org.addcel.gdf.dto.TenenciaResponse;
import org.addcel.gdf.sql.Placa;
import org.addcel.gdf.sql.PlacaDataSource;
import org.addcel.gdf.util.JSONUtil;
import org.addcel.gdftesoreria.R;
import org.addcel.utils.Dialogos;
import org.addcel.utils.SessionManager;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.UtilsGdf;
import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.form.validator.FormValidator;
import com.ironbit.mega.form.validator.Validable;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.system.errores.ErrorSys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.GetGdfCapturaWSClient;
import com.ironbit.mega.web.webservices.InsertPagoBitacoraWSClient;
import com.ironbit.mega.web.webservices.events.OnGdfCapturaResponseReceivedListener;

/**
 * <p>ACTIVIDAD EN LA QUE SE OBTIENEN LOS DATOS NECESARIOS PARA LLEVAR A CABO EL PAGO DE TENENCIA</p>
 * @author CARLOS GARCIA
 * @version 1.0
 */

public class PagoTenenciaActivity extends MenuActivity {
	
//	COMPONENTES GUI
	private TextView headerText;
	private Spinner placaSpinner;
	private TextView placaText;
	private EditText placaEdit;
	private TextView confPlacaText;
	private EditText confPlacaEdit;
	private Spinner eventoEdit;
	
	//	VIEWS PARA DATOS NUEVOS
	private LinearLayout datosLayout;
	private LinearLayout tenenciaActualizacionLayout;
	private LinearLayout refrendoActualizacionLayout;

	private TextView tenenciaText;
	private TextView actualizacionText;
	private TextView recargosTenenciaText;
	private TextView refrendoText;
	private TextView actualizacionRefrendoText;
	private TextView recargoRefrendoText;
	
	private TextView capturaEdit;
	private TextView montoEdit;
	private ArrayAdapter<Placa> placaAdapter;
	private Button atrasButton;
	private Button addPlacaButton;
	private Button verificarButton;
	private Button pagarButton;
	
	private LongProcess capturaProcess;
	private LongProcess insertaSqliteProcess;
	private LongProcess insertaBitacoraProcess;
	
//	WEB-SERVICES
	private GetGdfCapturaWSClient capturaClient;
	private InsertPagoBitacoraWSClient pagoClient;
	
//	OBJETO DATOS
	private TenenciaResponse response;
	
//	BANDERA PARA VALIDAR BOTON
	private boolean activado = false;
	
//	STRINGS LOGIN
	private String usuarioString;
	private String passwordString;
	
	private EditText loginEditText;
	private EditText passwordEditText;
	
//	COMPONENTES POP-UP
	private PopupWindow placaWindow;
	private EditText placaWindowEdit;
	private JSONObject requestPago;
	
	private String placaAInsertar;
	private PlacaDataSource placaDataSource;
	private long idBitacora;
	
//	BANDERA LOGIN
	private boolean logged;
	
	SessionManager session;
	
	protected final FormValidator formValidator = new FormValidator(this, true);
	
	public TextView getHeaderTextView() {
		if (headerText == null) {
			headerText = (TextView) findViewById(R.id.header_text);
		}
		
		return headerText;
	}
	
	public EditText getLoginEditText(View popup) {
		if (loginEditText == null) {
			loginEditText = (EditText) popup.findViewById(R.id.login_edit);
		}
		
		return loginEditText;
	}
	
	public EditText getPasswordEditText(View popup) {
		if (passwordEditText == null) {
			passwordEditText = (EditText) popup.findViewById(R.id.password_edit);
		}
		
		return passwordEditText;
	}
	
	public PlacaDataSource getPlacaDataSource() {
		if (placaDataSource == null) {
			placaDataSource = new PlacaDataSource(PagoTenenciaActivity.this);
		}
		
		return placaDataSource;
	}
	
	public String getUsuarioString() {
		return usuarioString;
	}
	
	public void setUsuarioString(String usuarioString) {
		this.usuarioString = usuarioString;
	}
	
	public String getPasswordString() {
		return passwordString;
	}
	
	public void setPasswordString(String passwordString) {
		this.passwordString = passwordString;
	}
	
	/**
	 * 
	 * @param popup View en el cual se buscar� el componente
	 * @return
	 */
	protected EditText getPlacaWindowEdit(View popup) {
		if (placaWindowEdit == null) {
			placaWindowEdit = (EditText) popup.findViewById(R.id.placa_edit);
		}
		
		return placaWindowEdit;
	}
	
	protected TextView getPlacaText() {
		if (placaText == null) {
			placaText = (TextView) findViewById(R.id.gdf_placa_text);
		}
		
		return placaText;
	}
	
	protected EditText getPlacaEdit() {
		if (placaEdit == null) {
			placaEdit = (EditText) findViewById(R.id.gdf_placa_edit);
		}
		
		return placaEdit;
	}
	
	protected TextView getConfPlacaText() {
		if (confPlacaText == null) {
			confPlacaText = (TextView) findViewById(R.id.gdf_conf_placa_text);
		}
		
		return confPlacaText;
	}
	
	protected EditText getConfPlacaEdit() {
		if (confPlacaEdit == null) {
			confPlacaEdit = (EditText) findViewById(R.id.gdf_conf_placa_edit);
		}
		
		return confPlacaEdit;
	}
	
	protected Spinner getPlacaSpinner(){
		if(placaSpinner == null){
			placaSpinner = (Spinner)findViewById(R.id.gdf_placa_spinner);
		}
		
		return placaSpinner;
	}
	
	protected ArrayAdapter<Placa> getPlacaAdapter(){
		

		Log.i("getPlacaAdapter()", "Entramos...");
		
		getPlacaDataSource();
		getPlacaDataSource().open();		
		List<Placa> placas = getPlacaDataSource().getAllPlacas();
		
		placaAdapter = new ArrayAdapter<Placa>(PagoTenenciaActivity.this,android.R.layout.simple_spinner_item,placas);
		placaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		Log.i("getPlacaAdapter()", "cerramos dataSource");
		getPlacaDataSource().close();
		
		Log.i("getPlacaAdapter()", "Terminamos...");
		return placaAdapter;
	}
	
	protected Spinner getEventoSpinner(){
		if(eventoEdit == null){
			eventoEdit = (Spinner)findViewById(R.id.anios_spinner);
		}
		return eventoEdit;
	}
	
	protected TextView getcapturaEdit(){
		if(capturaEdit == null){
			capturaEdit = (TextView)findViewById(R.id.gdf_lineacaptura_edit);
		}
		return capturaEdit;
	}
	
	
	protected TextView getmontoEdit(){
		if(montoEdit == null){
			montoEdit = (TextView)findViewById(R.id.gdf_monto_edit);
		}
		return montoEdit;
	}
	
	protected Button getAtrasButton() {
		if (atrasButton == null) {
			atrasButton = (Button) findViewById(R.id.button_atras);
		}
		
		return atrasButton;
	}
	
	protected Button getAddPlacaButton() {
		if (addPlacaButton == null) {
			addPlacaButton = (Button) findViewById(R.id.button_addplaca);
		}
		
		return addPlacaButton;
	}
	
	protected Button getVerificarButton(){
		if(verificarButton == null){
			verificarButton = (Button)findViewById(R.id.gdf_verificar_button);
		}
		
		return verificarButton;
	}
	
	protected LinearLayout getDatosLayout() {
		if (datosLayout == null) {
			datosLayout = (LinearLayout) findViewById(R.id.datos_pago_layout);
		}
		
		return datosLayout;
	}
	
	protected LinearLayout getTenenciaActualizacionLayout() {
		if (tenenciaActualizacionLayout == null) {
			tenenciaActualizacionLayout = (LinearLayout) findViewById(R.id.tenencia_actualizacion_layout);
		}
		
		return tenenciaActualizacionLayout;
	}
	
	protected LinearLayout getRefrendoActualizacionLayout() {
		if (refrendoActualizacionLayout == null) {
			refrendoActualizacionLayout = (LinearLayout) findViewById(R.id.refrendo_actualizacion_layout);
		}
		
		return refrendoActualizacionLayout;
	}
	
	protected TextView getTenenciaText() {
		if (tenenciaText == null) {
			tenenciaText = (TextView) findViewById(R.id.gdf_tenencia_text);
		}
		
		return tenenciaText;
	}
	
	protected TextView getActualizacionText() {
		if (actualizacionText == null) {
			actualizacionText = (TextView) findViewById(R.id.gdf_actualizacion_text);
		}
		
		return actualizacionText;
	}
	
	protected TextView getRecargosTenenciaText() {
		if (recargosTenenciaText == null) {
			recargosTenenciaText = (TextView) findViewById(R.id.gdf_recargos_tenencia_text);
		}
		
		return recargosTenenciaText;
	}
	
	
	protected TextView getRefrendoText() {
		if (refrendoText == null) {
			refrendoText = (TextView) findViewById(R.id.gdf_refrendo_text);
		}
		
		return refrendoText;
	}
	
	protected TextView getActualizacionRefrendoText() {
		if (actualizacionRefrendoText == null) {
			actualizacionRefrendoText = (TextView) findViewById(R.id.gdf_actualizacion_refrendo_text);
		}
		
		return actualizacionRefrendoText;
	}
	
	protected TextView getRecargoRefrendoText() {
		if (recargoRefrendoText == null) {
			recargoRefrendoText = (TextView) findViewById(R.id.gdf_recargo_refrendo_text);
		}
		
		return recargoRefrendoText;
	}
	
	protected Button getOkButton(){
		if(pagarButton == null){
			pagarButton = (Button)findViewById(R.id.gdf_ok_button);
		}
		
		return pagarButton;
	}
	
	public String getPlacaAInsertar() {
		return placaAInsertar;
	}
	
	public void setPlacaAInsertar(String placaAInsertar) {
		this.placaAInsertar = placaAInsertar;
	}
	
	public TenenciaResponse getResponse() {
		return response;
	}

	public void setResponse(TenenciaResponse response) {
		this.response = response;
	}
	
	public boolean isLogged() {
		return logged;
	}

	public void setLogged(boolean logged) {
		this.logged = logged;
	}

	public boolean isActivado() {
		return activado;
	}

	public void setActivado(boolean activado) {
		this.activado = activado;
	}
	
	public long getIdBitacora() {
		return idBitacora;
	}
	
	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.gdf_pago);
		
		session = new SessionManager(PagoTenenciaActivity.this);
		
		getPlacaSpinner().setAdapter(getPlacaAdapter());
		
		getPlacaSpinner().setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Placa p = (Placa) getPlacaSpinner().getSelectedItem();
				
				if (p.getPlaca().contains(ConstantesGdf.OTRA_PLACA)) {
					getPlacaText().setVisibility(View.VISIBLE);
					getPlacaEdit().setVisibility(View.VISIBLE);
					getConfPlacaText().setVisibility(View.VISIBLE);
					getConfPlacaEdit().setVisibility(View.VISIBLE);
				} else {
					if (getPlacaEdit().getVisibility() == View.VISIBLE) {
						getPlacaText().setVisibility(View.GONE);
						getPlacaEdit().setVisibility(View.GONE);	
						getConfPlacaText().setVisibility(View.GONE);
						getConfPlacaEdit().setVisibility(View.GONE);					
					}
				}
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		Placa p = (Placa) getPlacaSpinner().getSelectedItem();
		
		if (p.getPlaca().contains(ConstantesGdf.OTRA_PLACA)) {
			getPlacaText().setVisibility(View.VISIBLE);
			getPlacaEdit().setVisibility(View.VISIBLE);
			getConfPlacaText().setVisibility(View.VISIBLE);
			getConfPlacaEdit().setVisibility(View.VISIBLE);
		} else {
			if (getPlacaEdit().getVisibility() == View.VISIBLE) {
				getPlacaText().setVisibility(View.GONE);
				getPlacaEdit().setVisibility(View.GONE);
				getConfPlacaText().setVisibility(View.GONE);
				getConfPlacaEdit().setVisibility(View.GONE);
				
			}
		}
		
		getVerificarButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Placa placa = (Placa) getPlacaSpinner().getSelectedItem();
				String placaString = placa.getPlaca();
				
				if (placaString.contains(ConstantesGdf.OTRA_PLACA)) {
					if (formValidator.validate()) {
						
						/**
						 * if (session.isLoggedIn()) {
						 * 	capturaProcess.start();
						 * } else {
						 * 	Dialogos.loginAlert(PagoTenenciaActivity.this, capturaProcess);
						 * }
						 */
						
						if(null != Usuario.getIdUser(PagoTenenciaActivity.this) && ! "".equals(Usuario.getIdUser(PagoTenenciaActivity.this))) {
							capturaProcess.start();
						} else {
							Dialogos.loginAlert(PagoTenenciaActivity.this, capturaProcess);
						}
					}
				} else {
					if(null != Usuario.getIdUser(PagoTenenciaActivity.this) && ! "".equals(Usuario.getIdUser(PagoTenenciaActivity.this))) {
						capturaProcess.start();
					} else {
						Dialogos.loginAlert(PagoTenenciaActivity.this, capturaProcess);
					}
				}
				
			}
		});
		
		getOkButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub			
				if (isActivado()) {				
					
					Intent i = new Intent(PagoTenenciaActivity.this, PagoActivity.class);
					i.putExtra("datos", requestPago.toString());
					i.putExtra("servicio", Servicios.TENENCIA);
					startActivity(i);
					finish();
					
				} else {
					Toast.makeText(PagoTenenciaActivity.this, "Para hacer tu pago debes generar una l�nea de captura v�lida.", Toast.LENGTH_SHORT).show();
					
				}
			}
		});
		
		capturaProcess = new LongProcess(this, true, 60, true, "getLinea", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if(capturaClient != null){
					capturaClient.cancel();
					capturaClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				capturaClient = new GetGdfCapturaWSClient(PagoTenenciaActivity.this);
				capturaClient.setPlaca(getPlacaAEnviar());
				capturaClient.setEjercicio((String)getEventoSpinner().getSelectedItem());
				capturaClient.execute(new OnGdfCapturaResponseReceivedListener() {
					
					public void onGdfErrorResponseReceived(String error, String numError) {
						// TODO Auto-generated method stub
						getDatosLayout().setVisibility(View.GONE);
						setActivado(false);
						UtilsGdf.showCenterToast(getApplicationContext(), error, Toast.LENGTH_LONG);
 						capturaProcess.processFinished();
					}
					
					public void onGdfCapturaResponseReceived(TenenciaResponse t, JSONObject j) {
						// TODO Auto-generated method stub
//						Asignamos objeto que regresa la linea de captura a atributo dataCaptura de la actividad
						requestPago = j;
						setResponse(t);
						
//						LLenamos campos del GUI
						getTenenciaText().setText(Text.formatCurrency(t.getTenencia(), true));
						getRecargosTenenciaText().setText(Text.formatCurrency(t.getRecargos(), true));
						getRefrendoText().setText(Text.formatCurrency(t.getRefrendo(), true));
						getRecargoRefrendoText().setText(Text.formatCurrency(t.getRecRefrendo(), true));
						getmontoEdit().setText("$" + t.getTotal());
						getActualizacionText().setText(Text.formatCurrency(t.getActualizacion(), true));
						getActualizacionRefrendoText().setText(Text.formatCurrency(t.getActRefrendo(),true));
						getcapturaEdit().setText(t.getLineaCaptura());

						
						getDatosLayout().setVisibility(View.VISIBLE);
						
						setActivado(true);
						
						capturaProcess.processFinished();
					}
				});
			}
		});
		
		insertaSqliteProcess = new LongProcess(this, true, 20, true, "setPlaca", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (getPlacaDataSource().isOpen()) {
					getPlacaDataSource().close();
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				getPlacaDataSource().open();
				
				if (getPlacaDataSource().insertaNuevo(getPlacaAInsertar()) > 0) {
					getPlacaSpinner().setAdapter(getPlacaAdapter());
					Toast.makeText(PagoTenenciaActivity.this, "Su placa se ha insertado con �xito.", Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(PagoTenenciaActivity.this, "Error en inserci�n de placa. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				}
				
				getPlacaDataSource().close();
				placaWindow.dismiss();
				insertaSqliteProcess.processFinished();
			}
		});
		
		insertaBitacoraProcess = new LongProcess(this, true, 60, true, "insertPagoBitacora", new LongProcessListener() {

			public void doProcess() {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PagoTenenciaActivity.this, PagoWebGdfActivity.class);
				
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				
				params.add(new BasicNameValuePair("json", JSONUtil.getProcomRequestAsString(getApplicationContext(), requestPago, Usuario.getIdUser(PagoTenenciaActivity.this), Servicios.TENENCIA))
				);
				
				String url = Text.addParamsToUrl(URL.PROCOM_3D_SECURE_GDF, params);
				
				Log.i("url procom", url);
				
				if (url != null && !url.equals("")) {				
					intent.putExtra("url", url);
					intent.putExtra("tramite", "Tenencia");
					startActivity(intent);						
				} else {
					Toast.makeText(PagoTenenciaActivity.this, "Url de pago no es v�lida. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				}	
				
				insertaBitacoraProcess.processFinished();
				
			}

			public void stopProcess() {
				// TODO Auto-generated method stub
				if (pagoClient != null) {
					pagoClient.cancel();
					pagoClient = null;
				}
			}
			
		});
	}
	
	private String getPlacaAEnviar() {
		
		Placa selectedPlaca = ((Placa) getPlacaSpinner().getSelectedItem());
		
		if (selectedPlaca.getPlaca().contains(ConstantesGdf.OTRA_PLACA)) {
			return getPlacaEdit().getText().toString().trim().toUpperCase();
		}
		
		return selectedPlaca.getPlaca().toUpperCase();
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_PAGO_TENENCIA;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID){
		super.inicializarGUI(layoutResID);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		getHeaderTextView().setTypeface(gothamBook);	
		
		((TextView) findViewById(R.id.label_tenencia)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_tenencia_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_actualizacion)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_actualizacion_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_recargos_tenencia)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_recargos_tenencia_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_refrendo)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_refrendo_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_actualizacion_refrendo)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_actualizacion_refrendo_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_recargo_refrendo)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_recargo_refrendo_text)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_lineacaptura)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.gdf_lineacaptura_edit)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.gdf_monto_edit)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.label_monto)).setTypeface(gothamBold);
		
		((Button) findViewById(R.id.gdf_ok_button)).setTypeface(gothamBook);
		
		getAtrasButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		getAddPlacaButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Dialogos.showPlacaDialog(PagoTenenciaActivity.this, getPlacaDataSource());
			}
		});
	}
	
	@Override
	public void configValidations() {
		// TODO Auto-generated method stub
		formValidator.addItem(getPlacaEdit(), "Placa")
		.canBeEmpty(false)
		.isRequired(true)
		.minLength(4);
		
		formValidator.addItem(getConfPlacaEdit(), "Confirmacion Placa", new Validable() {
			
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				if (!getPlacaEdit().getText().toString().trim().equals(getConfPlacaEdit().getText().toString().trim())) {
					throw new ErrorSys("No coinciden Placas, favor de validar.");
				}
			}
		}).canBeEmpty(false).isRequired(true).minLength(4);
		
	}
}
