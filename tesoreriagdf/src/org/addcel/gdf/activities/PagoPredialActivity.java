package org.addcel.gdf.activities;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.addcel.gdf.constants.ConstantesGdf;
import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.constants.Servicios;
import org.addcel.gdf.constants.URL;
import org.addcel.gdf.sql.CuentaPredial;
import org.addcel.gdf.sql.Placa;
import org.addcel.gdf.sql.PlacaDataSource;
import org.addcel.gdf.sql.PredialDataSource;
import org.addcel.gdf.util.JSONUtil;
import org.addcel.gdftesoreria.R;
import org.addcel.utils.Dialogos;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.GetGdfPredialWSClient;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class PagoPredialActivity extends MenuActivity{
	
	private Button consultarButton;
	private LongProcess consultarProcess;
	private GetGdfPredialWSClient consultarClient;
	private Spinner cuentasSpinner;
	private EditText cuentaEdit;
	
//	VARIABLES DB LOCAL
	private PredialDataSource dataSource;
	private ArrayAdapter<CuentaPredial> adapter;
	
	private JSONObject requestPago;
	
	public Spinner getCuentasSpinner() {
		if (null == cuentasSpinner) {
			cuentasSpinner = (Spinner) findViewById(R.id.predial_spinner);
		}
		
		return cuentasSpinner;
	}
	
	public EditText getCuentaEdit() {
		if (null == cuentaEdit) {
			cuentaEdit = (EditText) findViewById(R.id.edit_cuenta);
		}
		
		return cuentaEdit;
	}
	
	public Button getConsultarButton() {
		if (null == consultarButton) {
			consultarButton = (Button) findViewById(R.id.gdf_consultar_button);
		}
		
		return consultarButton;
	}
	
	public PredialDataSource getDataSource() {
		if (null == dataSource) {
			dataSource = new PredialDataSource(PagoPredialActivity.this);
		}
		
		return dataSource;
	}
	
	protected ArrayAdapter<CuentaPredial> getAdapter() {
		
//		if (adapter == null) {

			getDataSource().open();
			
			List<CuentaPredial> cuentas = getDataSource().getAllCuentas();
			
			adapter = new ArrayAdapter<CuentaPredial>(PagoPredialActivity.this, android.R.layout.simple_spinner_item, cuentas);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
			getDataSource().close();
//		}
		
		return adapter;
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_PAGO_PREDIAL;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.gdf_predial_pago);
		
		consultarProcess = new LongProcess(this, true, 30, true, "consulta", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != consultarClient) {
					consultarClient.cancel();
					consultarClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				consultarClient = new GetGdfPredialWSClient(PagoPredialActivity.this);
				consultarClient.setCuenta(getCuentaAConsultar());
				consultarClient.execute(new OnResponseJSONReceivedListener() {
					
					public void onResponseJSONReceived(JSONObject jsonResponse) {
						// TODO Auto-generated method stub
						String error = jsonResponse.optString("error");
						
						if ("".equals(error)) {
							
							requestPago = jsonResponse;
							
							String periodo = jsonResponse.optString("bimestre");
//							String concepto = jsonResponse.optString("concepto");
							String cuentaP = jsonResponse.optString("cuentaP");
							double importe = jsonResponse.optDouble("importe", 0.0);
							int intImpuesto = jsonResponse.optInt("intImpuesto");
							String linCap = jsonResponse.optString("linea_captura");
//							String reduccion = jsonResponse.optString("reduccion");
							String total = jsonResponse.optString("totalPago");
//							String vencimiento = jsonResponse.optString("vencimiento");
							
							
//							((TextView) findViewById(R.id.text_anio)).setText(anio);
							((TextView) findViewById(R.id.text_periodo)).setText(periodo);
							((TextView) findViewById(R.id.text_derecho)).setText(Text.formatCurrency(importe, true));
//							((TextView) findViewById(R.id.text_iva)).setText("" + intImpuesto);
//							((TextView) findViewById(R.id.text_concepto)).setText(concepto);
							((TextView) findViewById(R.id.text_predial)).setText(cuentaP);
//							((TextView) findViewById(R.id.text_importe)).setText(importe);
							((TextView) findViewById(R.id.text_captura)).setText(linCap);
//							((TextView) findViewById(R.id.text_reduccion)).setText(reduccion);
							((TextView) findViewById(R.id.text_total)).setText("$" + total);
//							((TextView) findViewById(R.id.text_vencimiento)).setText(vencimiento);
							
							
							findViewById(R.id.layout_datos).setVisibility(View.VISIBLE);
							
							consultarProcess.processFinished();
						
						} else {
							Toast.makeText(PagoPredialActivity.this, jsonResponse.optString("error_descripcion"),Toast.LENGTH_SHORT).show();
						

							consultarProcess.processFinished();
						}
					}
				});
			}
		});
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		
//		((TextView) findViewById(R.id.label_concepto)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_concepto)).setTypeface(gothamBold);

//		((TextView) findViewById(R.id.label_anio)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_anio)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_periodo)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_periodo)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_derecho)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_derecho)).setTypeface(gothamBold);
		

//		((TextView) findViewById(R.id.label_iva)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_iva)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_predial)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_predial)).setTypeface(gothamBold);
		
//		((TextView) findViewById(R.id.label_vencimiento)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_vencimiento)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_captura)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.text_captura)).setTypeface(gothamBold);
		
//		((TextView) findViewById(R.id.label_importe)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_importe)).setTypeface(gothamBold);
		
//		((TextView) findViewById(R.id.label_reduccion)).setTypeface(gothamBold);
//		((TextView) findViewById(R.id.text_reduccion)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.text_total)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.label_total)).setTypeface(gothamBold);
		
		getCuentasSpinner().setAdapter(getAdapter());
		getCuentasSpinner().setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				CuentaPredial c = (CuentaPredial) getCuentasSpinner().getSelectedItem();
				
				if (c.getCuenta().contains(ConstantesGdf.OTRA_PLACA)) {

					findViewById(R.id.layout_cuenta).setVisibility(View.VISIBLE);
				} else {
					if (View.VISIBLE == findViewById(R.id.layout_cuenta).getVisibility()) {
						findViewById(R.id.layout_cuenta).setVisibility(View.GONE);
					}
				}
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		getCuentaEdit().addTextChangedListener(new TextWatcher() {
			
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				CuentaPredial c = (CuentaPredial) getCuentasSpinner().getSelectedItem();
				
				if (c.getCuenta().contains(ConstantesGdf.OTRA_PLACA)) {
					int count = 12 - arg0.length();
					((TextView)findViewById(R.id.text_contador)).setText("" + count);
					
				}
			}
			
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		findViewById(R.id.button_atras).setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		findViewById(R.id.button_addplaca).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Dialogos.showPredialDialog(PagoPredialActivity.this, getDataSource());
			}
		});
		
		getConsultarButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				String idUser = Usuario.getIdUser(PagoPredialActivity.this);	
				
				if (cuentaValida()) {
				
					if (null == idUser || "".equals(idUser)) {
						Dialogos.loginAlert(PagoPredialActivity.this, consultarProcess);
					} else {
						consultarProcess.start();
					}
				}
			}
		});
		
		findViewById(R.id.pagar_button).setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent i = new Intent(PagoPredialActivity.this, PagoActivity.class);
				i.putExtra("datos", requestPago.toString());
				i.putExtra("servicio", Servicios.PREDIAL);
				startActivity(i);
				finish();
			}
		});
	}
	
	private String getCuentaAConsultar() {
		
		CuentaPredial selectedCuenta = ((CuentaPredial) getCuentasSpinner().getSelectedItem());
		
		if (selectedCuenta.getCuenta().contains(ConstantesGdf.OTRA_PLACA)) {
			return getCuentaEdit().getText().toString().trim().toUpperCase();
		}
		
		return selectedCuenta.getCuenta().toUpperCase();
	}
	
	private boolean cuentaValida() {
		if (null == getCuentaAConsultar() || "".equals(getCuentaAConsultar())) {
			getCuentaEdit().setError("Ingresa una cuenta predial v�lida");
			return false;
		}
		
		return true;
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getAdapter().notifyDataSetChanged();
		getCuentasSpinner().setAdapter(getAdapter());
		findViewById(R.id.layout_datos).setVisibility(View.GONE);
	}
	
	private String getAnioActual() {
		Calendar calendar = Calendar.getInstance();
		int anio = calendar.get(Calendar.YEAR);
		
		return Integer.toString(anio);
	}



}
