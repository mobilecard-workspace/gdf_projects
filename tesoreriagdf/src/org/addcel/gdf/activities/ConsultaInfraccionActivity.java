package org.addcel.gdf.activities;

import org.addcel.gdf.constants.ConstantesGdf;
import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.dto.Infraccion;
import org.addcel.gdftesoreria.R;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.web.webservices.GetGdfReenvioWSClient;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class ConsultaInfraccionActivity extends MenuActivity {
	
	private JSONObject json;
	private Infraccion infraccion;
	private LongProcess reenvioProcess;
	private GetGdfReenvioWSClient reenvioClient;
	
	public JSONObject getJson()  {
		if (null == json) {
			try {
				json = new JSONObject(getIntent().getStringExtra("json"));
			} catch (JSONException e) {
				return null;
			}
		}
		
		return json;
	}
	
	public Infraccion getInfraccion() {
		if (null == infraccion) {
			infraccion = new Infraccion();
			
			infraccion.setFolio(getJson().optString("folio"));
			infraccion.setHashCodeCalc(getJson().optInt("codeCalc"));
			infraccion.setImporte(getJson().optString("totalPago"));
			infraccion.setLineaCaptura(getJson().optString("linea_captura"));
			infraccion.setFechaInfraccion(getJson().optString("fechainfraccion"));
			infraccion.setActualizacion(getJson().optString("actualizacion", "0.0"));
			infraccion.setRecargos(getJson().optString("recargos"));
			infraccion.setDiasMulta(getJson().optString("importe"));
		}
		
		return infraccion;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_infraccion_consulta);
		
		((TextView) findViewById(R.id.infraccion_folio)).setText(getInfraccion().getFolio());
		((TextView) findViewById(R.id.infraccion_importe)).setText(getInfraccion().formatImporte());
		((TextView) findViewById(R.id.infraccion_linea)).setText(getInfraccion().getLineaCaptura());
		((TextView) findViewById(R.id.infraccion_fecha)).setText(getInfraccion().getFechaInfraccion());
		((TextView) findViewById(R.id.infraccion_recargos)).setText(getInfraccion().getRecargos());
		((TextView) findViewById(R.id.infraccion_multa)).setText(getInfraccion().getDiasMulta());
		
		if (! getInfraccion().getActualizacion().equals(ConstantesGdf.DEFAULT_ACTUALIZACION)) {
			((TextView) findViewById(R.id.infraccion_actualizacion)).setText(getInfraccion().getActualizacion());
			findViewById(R.id.infraccion_actualizacion_layout).setVisibility(View.VISIBLE);
		} else {
			if (findViewById(R.id.infraccion_actualizacion_layout).getVisibility() == View.VISIBLE) {
				findViewById(R.id.infraccion_actualizacion_layout).setVisibility(View.GONE);
			}
		}
		
		reenvioProcess = new LongProcess(this, true, 30, true, "reenvio", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != reenvioClient) {
					reenvioClient.cancel();
					reenvioClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				reenvioClient = new GetGdfReenvioWSClient(ConsultaInfraccionActivity.this);
				reenvioClient.setIdBitacora(getJson().optString("id_bitacora"));
				reenvioClient.setIdProducto(getJson().optInt("id_producto"));
				reenvioClient.executeClient(new OnResponseJSONReceivedListener() {
					
					public void onResponseJSONReceived(JSONObject jsonResponse) {
						// TODO Auto-generated method stub
						String error = jsonResponse.optString("error");
						Toast.makeText(ConsultaInfraccionActivity.this, error, Toast.LENGTH_SHORT).show();
						reenvioProcess.processFinished();
					}
				});
			}
		});
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_CONSULTA_INFRACCION;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		
		((TextView) findViewById(R.id.label_datos)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_folio)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_folio)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_fecha)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_fecha)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_actualizacion)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_actualizacion)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_recargos)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_recargos)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_multa)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_multa)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_linea)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_linea)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_importe)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_importe)).setTypeface(gothamBold);
		
		findViewById(R.id.button_reenviar).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				reenvioProcess.start();
			}
		});
	}

}
