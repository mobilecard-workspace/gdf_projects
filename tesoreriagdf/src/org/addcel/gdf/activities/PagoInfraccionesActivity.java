package org.addcel.gdf.activities;

import java.util.LinkedList;
import java.util.List;

import org.addcel.gdf.constants.ConstantesGdf;
import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.constants.Servicios;
import org.addcel.gdf.constants.URL;
import org.addcel.gdf.dto.Infraccion;
import org.addcel.gdf.sql.Placa;
import org.addcel.gdf.sql.PlacaDataSource;
import org.addcel.gdf.util.JSONUtil;
import org.addcel.gdftesoreria.R;
import org.addcel.utils.Dialogos;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.UtilsGdf;
import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.form.validator.FormValidator;
import com.ironbit.mega.form.validator.Validable;
import com.ironbit.mega.system.Text;
import com.ironbit.mega.system.errores.ErrorSys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.GetGdfInfraccionesClient;
import com.ironbit.mega.web.webservices.InsertPagoBitacoraWSClient;
import com.ironbit.mega.web.webservices.events.OnGdfInfraccionesResponseReceivedListener;

public class PagoInfraccionesActivity extends MenuActivity {
	
//	COMPONENTES GUI
	private TextView headerText;
	private Spinner placaSpinner;
	private TextView placaText;
	private EditText placaEdit;
	private TextView confPlacaText;
	private EditText confPlacaEdit;
	private Button consultarButton;
	private Button addPlacaButton;
	
//	PROCESOS
	private LongProcess consultaProcess;
	private LongProcess insertaProcess;
	private LongProcess pagoProcess;
	
//	WEB-SERVICES
	private GetGdfInfraccionesClient consultaClient;
	private InsertPagoBitacoraWSClient pagoClient;
			
//	VIEWS PARA DATOS NUEVOS
	private LinearLayout folioLayout;
	private Spinner folioSpinner;
	private ArrayAdapter<Infraccion> infraccionAdapter;
	
	private LinearLayout infraccionLayout;
	private LinearLayout actualizacionLayout;
	
	private TextView folioText;
	private TextView importeText;
	private TextView capturaText;
	private TextView fechaInfraccionText;
	private TextView actualizacionText;
	private TextView recargosText;
	private TextView diasMultaText;
	
	private Button pagarButton;
	
//	COMPONENENTES POP-UP
	private PopupWindow placaWindow;
	private EditText placaWindowEdit;
	private int x;
	private int y;
	
	private String placaAInsertar;
	private PlacaDataSource placaDataSource;
	private ArrayAdapter<Placa> placaAdapter;
	
	private long idBitacora;
	
//	BANDERA LOGIN
	private boolean logged;
	
//	BANDERA PARA VALIDAR BOT�N
	private boolean activado = false;
	
//	INFRACCION ACTUAL
	private Infraccion infraccionActual;
	private JSONObject infraccionRequestJSON;
	
	protected final FormValidator formValidator = new FormValidator(this, true);
	
//	GETTERS & SETTERS
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getPlacaAInsertar() {
		return placaAInsertar;
	}

	public void setPlacaAInsertar(String placaAInsertar) {
		this.placaAInsertar = placaAInsertar;
	}

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}

	public boolean isLogged() {
		return logged;
	}

	public void setLogged(boolean logged) {
		this.logged = logged;
	}
	
	public Infraccion getInfraccionActual() {
		return infraccionActual;
	}

	public void setInfraccionActual(Infraccion infraccionActual) {
		this.infraccionActual = infraccionActual;
	}

	public boolean isActivado() {
		return activado;
	}

	public void setActivado(boolean activado) {
		this.activado = activado;
	}
	
	private void showPopupPlacas(final Activity context) {
		LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup_placa);
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		final View layout = layoutInflater.inflate(R.layout.popup_placa, viewGroup);
		
		placaWindow = new PopupWindow(context);
		placaWindow.setContentView(layout);
		getSize();
		placaWindow.setWidth(x - (x / 3));
		placaWindow.setHeight(y - (y / 3));
		placaWindow.setFocusable(true);
		
		placaWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
		
		final Button placaButton = (Button) layout.findViewById(R.id.agregar_button);
		
		placaButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final String placaString = getPlacaWindowEdit(layout).getText().toString().trim();
				
				if (placaString != "") {
					setPlacaAInsertar(placaString.toUpperCase());
					insertaProcess.start();
				}
			}
		});
	}

	public PlacaDataSource getPlacaDataSource() {
		if (placaDataSource == null) {
			placaDataSource = new PlacaDataSource(PagoInfraccionesActivity.this);
		}
		
		return placaDataSource;
	}
	
	/**
	 * @param view en el cual buscamos el componente
	 * @return
	 */
	protected EditText getPlacaWindowEdit(View popup) {
		if (placaWindowEdit == null) {
			placaWindowEdit = (EditText) popup.findViewById(R.id.placa_edit);
		}
		
		return placaWindowEdit;
	}
	
	protected Button getAddPlacaButton() {
		if (addPlacaButton == null) {
			addPlacaButton = (Button) findViewById(R.id.button_addplaca);
		}
		
		return addPlacaButton;
	}
	
	public TextView getHeaderText() {
		if (headerText == null) {
			headerText = (TextView) findViewById(R.id.header_text);
		}
		
		return headerText;
	}
	
	public Spinner getPlacaSpinner() {
		if (placaSpinner == null) {
			placaSpinner = (Spinner) findViewById(R.id.gdf_placa_spinner);
		}
		
		return placaSpinner;
	}
	
	public ArrayAdapter<Placa> getPlacaAdapter() {
		getPlacaDataSource().open();
		List<Placa> placas = getPlacaDataSource().getAllPlacas();
		
		placaAdapter = new ArrayAdapter<Placa>(PagoInfraccionesActivity.this, android.R.layout.simple_spinner_item, placas);
		placaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		getPlacaDataSource().close();
		
		return placaAdapter;
	}
	
	public TextView getPlacaTextView() {
		if (placaText == null) {
			placaText = (TextView) findViewById(R.id.gdf_placa_text);
		}
		
		return placaText;
	}
	
	public EditText getPlacaEdit() {
		if (placaEdit == null) {
			placaEdit = (EditText) findViewById(R.id.gdf_placa_edit);
		}
		
		return placaEdit;
	}
	
	protected TextView getConfPlacaText() {
		if (confPlacaText == null) {
			confPlacaText = (TextView) findViewById(R.id.gdf_conf_placa_text);
		}
		
		return confPlacaText;
	}
	
	protected EditText getConfPlacaEdit() {
		if (confPlacaEdit == null) {
			confPlacaEdit = (EditText) findViewById(R.id.gdf_conf_placa_edit);
		}
		
		return confPlacaEdit;
	}
	
	public void MostrarUOcultarPlaca(Placa p) {
		System.out.println(p.getPlaca());
		if (p.getPlaca().contains(ConstantesGdf.OTRA_PLACA)) {
			getPlacaTextView().setVisibility(View.VISIBLE);
			getPlacaEdit().setVisibility(View.VISIBLE);
			getConfPlacaText().setVisibility(View.VISIBLE);
			getConfPlacaEdit().setVisibility(View.VISIBLE);
		} else {
			if (getPlacaEdit().getVisibility() == View.VISIBLE) {
				getPlacaTextView().setVisibility(View.GONE);
				getPlacaEdit().setVisibility(View.GONE);		
				getConfPlacaText().setVisibility(View.GONE);
				getConfPlacaEdit().setVisibility(View.GONE);					
			}
		}
	}
	
	public Button getConsultarButton() {
		if (consultarButton == null) {
			consultarButton = (Button) findViewById(R.id.gdf_consultar_button);
		}
		
		return consultarButton;
	}
	
	public LinearLayout getFolioLayout(){
		if (folioLayout == null) {
			folioLayout = (LinearLayout) findViewById(R.id.layout_folio);
		}
		
		return folioLayout;
	}
	
	public Spinner getFolioSpinner() {
		if (folioSpinner == null) {
			folioSpinner = (Spinner) findViewById(R.id.gdf_folio_spinner);
		}
		
		return folioSpinner;
	}
	
	public ArrayAdapter<Infraccion> getFolioAdapter(List<Infraccion> infracciones) {
		
		infraccionAdapter = new ArrayAdapter<Infraccion>(PagoInfraccionesActivity.this, android.R.layout.simple_spinner_item, infracciones);
		infraccionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		return infraccionAdapter;
	}
	
	public LinearLayout getInfraccionLayout() {
		if (infraccionLayout == null) {
			infraccionLayout = (LinearLayout) findViewById(R.id.gdf_infraccion_layout);
		}
		
		return infraccionLayout;
	}
	
	public LinearLayout getActualizacionLayout() {
		if (actualizacionLayout == null) {
			actualizacionLayout = (LinearLayout) findViewById(R.id.infraccion_actualizacion_layout);
		}
		
		return actualizacionLayout;
	}
	
	public TextView getFolioText() {
		if (folioText == null) {
			folioText = (TextView) findViewById(R.id.infraccion_folio);
		}
		
		return folioText;
	}
	
	public TextView getImporteText() {
		if (importeText == null) {
			importeText = (TextView) findViewById(R.id.infraccion_importe);
		}
		
		return importeText;
	}
	
	public TextView getCapturaText() {
		if (capturaText == null) {
			capturaText = (TextView) findViewById(R.id.infraccion_linea);
		}
		
		return capturaText;
	}
	
	public TextView getFechaInfraccionText() {
		if (fechaInfraccionText == null) {
			fechaInfraccionText = (TextView) findViewById(R.id.infraccion_fecha);
		}
		
		return fechaInfraccionText;
	}
	
	public TextView getActualizacionText() {
		if (actualizacionText == null) {
			actualizacionText = (TextView) findViewById(R.id.infraccion_actualizacion);
		}
		
		return actualizacionText;
	}
	
	public TextView getRecargosText() {
		if (recargosText == null) {
			recargosText = (TextView) findViewById(R.id.infraccion_recargos);
		}
		
		return recargosText;
	}
	
	public TextView getDiasMultaText() {
		if (diasMultaText == null) {
			diasMultaText = (TextView) findViewById(R.id.infraccion_multa);
		}
		
		return diasMultaText;
	}
	
	public Button getPagarButton() {
		if (pagarButton == null) {
			pagarButton = (Button) findViewById(R.id.gdf_ok_button);
		}
		
		return pagarButton;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.gdf_infraccion_pago);
		
		getPlacaSpinner().setAdapter(getPlacaAdapter());
		
		getPlacaSpinner().setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Spinner selected = (Spinner) arg0;
				Placa selectedPlaca = (Placa) selected.getSelectedItem();
				MostrarUOcultarPlaca(selectedPlaca);
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		Placa placaInicial = (Placa) getPlacaSpinner().getSelectedItem();
		System.out.println(placaInicial.getPlaca());
		
		MostrarUOcultarPlaca(placaInicial);
		
		getFolioSpinner().setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Spinner spinner = (Spinner) arg0;
				setInfraccionActual((Infraccion) spinner.getSelectedItem());
				infraccionRequestJSON = getInfraccionActual().toJSON();
				
				
				if (getInfraccionLayout().getVisibility() == View.GONE) {
					getInfraccionLayout().setVisibility(View.VISIBLE);
				}
				
				getFolioText().setText(getInfraccionActual().getFolio());
				getImporteText().setText(getInfraccionActual().formatImporte());
				getCapturaText().setText(getInfraccionActual().getLineaCaptura());
				
//				NUEVOS PAR�METROS EN EL RESPONSE
				getFechaInfraccionText().setText(getInfraccionActual().getFechaInfraccion());
				getRecargosText().setText(getInfraccionActual().getRecargos());
				getDiasMultaText().setText(getInfraccionActual().getDiasMulta());
				getActualizacionText().setText(getInfraccionActual().getActualizacion());

				
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				getFolioLayout().setVisibility(View.GONE);
			}
		});
		
		
		
		consultaProcess = new LongProcess(this, true, 60, true, "getInfraccion", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (consultaClient != null) {
					consultaClient.cancel();
					consultaClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				consultaClient = new GetGdfInfraccionesClient(PagoInfraccionesActivity.this);
				consultaClient.setPlaca(getPlacaAEnviar());
				consultaClient.execute(new OnGdfInfraccionesResponseReceivedListener() {
					
					public void onGdfInfraccionesResponseReceived(String importeTotal,
							String adeudo, List<Infraccion> infracciones, boolean hashCodeCalc) {
						// TODO Auto-generated method stub
						System.out.println("TOTAL: " + importeTotal);
						System.out.println("ADEUDO: " + adeudo);
						System.out.println("HASH_CODE: " + hashCodeCalc);
						System.out.println(infracciones);
						
						getFolioSpinner().setAdapter(getFolioAdapter(infracciones));
						getFolioLayout().setVisibility(View.VISIBLE);
						
						setActivado(true);
						
						consultaProcess.processFinished();
					}
					
					public void onGdfInfraccionesErrorReceived(String error, String errorDesc) {
						// TODO Auto-generated method stub
						UtilsGdf.showCenterToast(getApplicationContext(), errorDesc, Toast.LENGTH_LONG);
						consultaProcess.processFinished();
					}
				});
			}
		});
		
		
//		Process que ejectuta la incersi�n en bd local
		 
		insertaProcess = new LongProcess(this, true, 20, true, "setPlaca", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (getPlacaDataSource().isOpen()) {
					getPlacaDataSource().close();
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				getPlacaDataSource().open();
				
				if (getPlacaDataSource().insertaNuevo(getPlacaAInsertar()) > 0) {
					getPlacaSpinner().setAdapter(getPlacaAdapter());
					UtilsGdf.showCenterToast(PagoInfraccionesActivity.this, "Su placa se ha insertado con �xito", Toast.LENGTH_LONG);
				} else {
					UtilsGdf.showCenterToast(PagoInfraccionesActivity.this, "Error en inseci�n de placa. Intente m�s tarde", Toast.LENGTH_SHORT);
				}
				
				getPlacaDataSource().close();
				placaWindow.dismiss();
				insertaProcess.processFinished();
			}
		});
		
		pagoProcess = new LongProcess(this, true, 60, true, "insertaPago", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (pagoClient != null) {
					pagoClient.cancel();
					pagoClient = null;
				}
				
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PagoInfraccionesActivity.this, PagoWebGdfActivity.class);
				BasicNameValuePair param = new BasicNameValuePair("json", JSONUtil.getProcomRequestAsString(PagoInfraccionesActivity.this, infraccionRequestJSON, Usuario.getIdUser(PagoInfraccionesActivity.this), Servicios.INFRACCION));
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(param);
				
				String url = Text.addParamsToUrl(URL.PROCOM_3D_SECURE_GDF, params);
				
				Log.i("url procom infracciones", url);
				
				if (url != null && !url.equals("")) {					
					intent.putExtra("url", url);
					intent.putExtra("tramite", "Infracciones");
					startActivity(intent);						
				} else {
					Toast.makeText(PagoInfraccionesActivity.this, "Url de pago no es v�lida. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				}	
				
				pagoProcess.processFinished();
			}
		});
		
		
		
		getConsultarButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Placa placa = (Placa) getPlacaSpinner().getSelectedItem();
				String placaString = placa.getPlaca();
				
				if (placaString.contains(ConstantesGdf.OTRA_PLACA)) {
					if (formValidator.validate()) {
						if(null != Usuario.getIdUser(PagoInfraccionesActivity.this) && ! "".equals(Usuario.getIdUser(PagoInfraccionesActivity.this))) {
							consultaProcess.start();
						} else {
							Dialogos.loginAlert(PagoInfraccionesActivity.this, consultaProcess);
						}
					}
				} else {
					if(null != Usuario.getIdUser(PagoInfraccionesActivity.this) && ! "".equals(Usuario.getIdUser(PagoInfraccionesActivity.this))) {
						consultaProcess.start();
					} else {
						Dialogos.loginAlert(PagoInfraccionesActivity.this, consultaProcess);
					}
				}
			}
		});
		
		getPagarButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isActivado()) {
					Intent i = new Intent(PagoInfraccionesActivity.this, PagoActivity.class);
					i.putExtra("datos", infraccionRequestJSON.toString());
					i.putExtra("servicio", Servicios.INFRACCION);
					startActivity(i);
					finish();
				} else {
					Toast.makeText(PagoInfraccionesActivity.this, "Para hacer tu pago debes generar una l�nea de captura v�lida", Toast.LENGTH_SHORT).show();
				}
				
			}
		});
		
		findViewById(R.id.button_atras).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		getAddPlacaButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Dialogos.showPlacaDialog(PagoInfraccionesActivity.this, getPlacaDataSource());
			}
		});
	}
	
	private String getPlacaAEnviar() {
		
		Placa selectedPlaca = ((Placa) getPlacaSpinner().getSelectedItem());
		
		if (selectedPlaca.getPlaca().contains(ConstantesGdf.OTRA_PLACA)) {
			return getPlacaEdit().getText().toString().trim().toUpperCase();
		}
		
		return selectedPlaca.getPlaca().toUpperCase();
	}

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_PAGO_INFRACCIONES;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		getHeaderText().setTypeface(gothamBook);
		
		((TextView) findViewById(R.id.label_datos)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_folio)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_folio)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_fecha)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_fecha)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_actualizacion)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_actualizacion)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_recargos)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_recargos)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_multa)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_multa)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_linea)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_linea)).setTypeface(gothamBold);
		
		((TextView) findViewById(R.id.label_importe)).setTypeface(gothamBold);
		((TextView) findViewById(R.id.infraccion_importe)).setTypeface(gothamBold);
		
		((Button) findViewById(R.id.gdf_ok_button)).setTypeface(gothamBook);
	}
	
	public void getSize() {
		Display display = getWindowManager().getDefaultDisplay();
		x = display.getWidth();
		y = display.getHeight();
	}
	
	@Override
		public void configValidations() {
			// TODO Auto-generated method stub
			formValidator.addItem(getPlacaEdit(), "Placa")
			.canBeEmpty(false)
			.isRequired(true)
			.minLength(4);
			
			formValidator.addItem(getConfPlacaEdit(), "Confirmacion Placa", new Validable() {
				
				public void validate() throws ErrorSys {
					// TODO Auto-generated method stub
					if (!getPlacaEdit().getText().toString().trim().equals(getConfPlacaEdit().getText().toString().trim())) {
						throw new ErrorSys("No coinciden Placas, favor de validar.");
					}
				}
			}).canBeEmpty(false).isRequired(true).minLength(4);
		}
	
	

}
