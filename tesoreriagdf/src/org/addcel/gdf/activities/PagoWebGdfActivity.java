package org.addcel.gdf.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.YuvImage;

import org.addcel.gdf.constants.ConstantesGdf;
import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.constants.URL;
import org.addcel.gdftesoreria.R;
import org.addcel.mobilecard.activities.MenuAppActivity;

import com.ironbit.mega.activities.MenuActivity;

public class PagoWebGdfActivity extends MenuActivity {
	
	private Button finalizarButton;
	private WebView webView;
	private String url;
	private ProgressDialog progressDialog;
	private String tramite;
	private TextView headerText;
	
	protected TextView getHeaderText() {
		if (headerText == null) {
			headerText = (TextView) findViewById(R.id.textView1);
		}
		
		return headerText;
	}
	
	protected WebView getWebView() {
		if(webView == null){
			webView = (WebView)findViewById(R.id.gdf_pago_webview);
		}
		
		return webView;
	}
	
	protected String getUrl() {
		if(url == null){
			url = getIntent().getStringExtra("url");
		}
		
		return url;
	}
	
	protected String getTramite() {
		if (tramite == null) {
			tramite = getIntent().getStringExtra("tramite");
		}
		
		return tramite;
	}
	
	protected Button getFinalizarButton() {
		if (finalizarButton == null) {
			finalizarButton = (Button)findViewById(R.id.gdf_button_finalizar);
		}
		
		return finalizarButton;
	}
	
	protected ProgressDialog getProgressDialog() {
		return progressDialog;
	}
	
	protected void setProgressDialog(ProgressDialog progressDialog) {
		this.progressDialog = progressDialog;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.gdf_web_pago);
		
		getFinalizarButton().setVisibility(View.GONE);
		getFinalizarButton().setEnabled(false);
		setProgressDialog(ProgressDialog.show(PagoWebGdfActivity.this, "", "Cargando...", true));
		
		getFinalizarButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PagoWebGdfActivity.this, MenuAppActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
		
		getWebView().getSettings().setBuiltInZoomControls(true);
		getWebView().getSettings().setJavaScriptEnabled(true);
		getWebView().getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		getWebView().getSettings().setLoadsImagesAutomatically(true);
		getWebView().getSettings().setPluginsEnabled(true);
		
		getWebView().setWebViewClient(new WebViewClient(){
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				
				if (!getProgressDialog().isShowing() && getProgressDialog() != null) {
					getProgressDialog().show();				
				}			
				
				if (url.equals(URL.PROCOM_RETURN)) {
					getFinalizarButton().setVisibility(View.VISIBLE);
				}	
				
			}
			
			@Override
			public void onPageFinished(WebView view, String url) {
				
				Log.i("url final procom", url);
				
				if (getProgressDialog().isShowing() && getProgressDialog() != null) {
					getProgressDialog().dismiss();
				}
				
				if (url.equals(URL.PROCOM_RETURN)) {
					getFinalizarButton().setEnabled(true);
				}
			}
			
			@Override
			public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
				super.onReceivedSslError(view, handler, error);
				handler.proceed();
			}
			
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
//				return super.shouldOverrideUrlLoading(view, url);
				return false;
			}
		});
		
		getWebView().clearSslPreferences();
		getWebView().loadUrl(getUrl());
		
	}
	
	

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_COMPRA_WEB;
	}
	
	@Override
	protected void inicializarGUI(int layoutResID){
		super.inicializarGUI(layoutResID);
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		getHeaderText().setTypeface(gothamBook);
		getHeaderText().setText(getTramite() + " > Pago 3D-Secure");
	}

}
