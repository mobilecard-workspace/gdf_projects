package org.addcel.gdf.activities;

import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.constants.Servicios;
import org.addcel.gdf.dto.Detalle;
import org.addcel.gdf.util.JSONUtil;
import org.addcel.gdftesoreria.R;
import org.addcel.gdftesoreria.R.id;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.system.errores.ErrorSys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.GdfPurchaseWSClient;
import com.ironbit.mega.web.webservices.GetTokenWSClient;
import com.ironbit.mega.web.webservices.events.OnResponseJSONReceivedListener;

public class PagoActivity extends MenuActivity {
	
	private TextView headerText, infoText, servicioLabel, montoLabel; 
	private TextView capturaLabel, cvvLabel, passLabel, servicioText;
	private TextView montoText, capturaText;
	private EditText cvvEdit, passEdit;
	private Button cancelarButton, pagarButton;
	private String datosString;
	private int servicio;
	private JSONObject datos;
	private LongProcess compraProcess;
	private GetTokenWSClient tokenClient;
	
	private static final String TAG = "PagoActivity";
	

	@Override
	public Actividades getIdActividad() {
		// TODO Auto-generated method stub
		return Actividades.GDF_COMPRA;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_pago);
//		this, true, 60, true,
		compraProcess = new LongProcess(PagoActivity.this, true, 60, true, "Compra", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != tokenClient) {
					tokenClient.cancel();
					tokenClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				tokenClient = new GetTokenWSClient(PagoActivity.this);
				tokenClient.setUsuario(getResources().getString(R.string.usr_token));
				tokenClient.setPassword(getResources().getString(R.string.pass_token));
				tokenClient.setProducto(Integer.parseInt(getResources().getString(R.string.id_proveedor)));
				tokenClient.execute(new OnResponseJSONReceivedListener() {
					
					public void onResponseJSONReceived(JSONObject jsonResponse) {
						// TODO Auto-generated method stub
						Log.d(TAG, jsonResponse.toString());
						
						int idError = jsonResponse.optInt("numError", -1);
						String mensajeError = jsonResponse.optString("error", "Error de Conexi�n.");
						
						switch (idError) {
							case 0:
								
								String cvv2 = cvvEdit.getText().toString().trim();
								String token = jsonResponse.optString("token");
								String password = passEdit.getText().toString().trim();
								
								GdfPurchaseWSClient compraClient = new GdfPurchaseWSClient(PagoActivity.this);
								compraClient.setIdProducto(servicio);
								setNewJSONData(cvv2, token, password);
								compraClient.setJson(getDatos());
								compraClient.execute(new OnResponseJSONReceivedListener() {
									
									public void onResponseJSONReceived(JSONObject jsonResponse) {
										// TODO Auto-generated method stub
										int idError = jsonResponse.optInt("numError", -1);
										
										String mensajeError = jsonResponse.optString("error", "Error de conexi�n. Intente de nuevo m�s tarde");
										String referencia = jsonResponse.optString("referencia", "");
										String autorizacion = jsonResponse.optString("autorizacion", "");
										String monto = getDatos().optString("totalPago", "");
										
										Detalle detalle = new Detalle(mensajeError, referencia, autorizacion, monto);
										
										Intent intent = new Intent(PagoActivity.this, DetalleActivity.class);

										switch (idError) {
											case 0:
												detalle.setExitoso(true);										
												intent.putExtra("detalle", detalle);
												startActivity(intent);
												finish();
												compraProcess.processFinished();
												break;
	
											default:
												detalle.setExitoso(false);										
												intent.putExtra("detalle", detalle);
												startActivity(intent);
												compraProcess.processFinished();
												break;
										}										
									}
								});
								
								break;
	
							default:
								Toast.makeText(PagoActivity.this, mensajeError, Toast.LENGTH_SHORT).show();
								compraProcess.processFinished();
								break;
							}
					}
				});
			}
		});
		
		
	}
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		// TODO Auto-generated method stub
		super.inicializarGUI(layoutResID);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		initData();
		
		headerText = (TextView) findViewById(R.id.text_header);
		infoText = (TextView) findViewById(R.id.text_info);
		
		servicioLabel = (TextView) findViewById(R.id.label_servicio);
		montoLabel = (TextView) findViewById(R.id.label_monto);
		capturaLabel = (TextView) findViewById(R.id.label_captura);
		cvvLabel = (TextView) findViewById(R.id.label_cvv2);
		passLabel = (TextView) findViewById(R.id.text_password);
		
		servicioText = (TextView) findViewById(R.id.text_servicio);
		montoText = (TextView) findViewById(R.id.text_monto);
		capturaText = (TextView) findViewById(R.id.text_captura);
		
		cvvEdit = (EditText) findViewById(R.id.edit_cvv2);
		passEdit = (EditText) findViewById(R.id.edit_password);
		
		cancelarButton = (Button) findViewById(R.id.button_cancelar);
		pagarButton = (Button) findViewById(R.id.button_pagar);
		
		headerText.setTypeface(gothamBook);
		infoText.setTypeface(gothamBook);
		servicioLabel.setTypeface(gothamBook);
		montoLabel.setTypeface(gothamBook);
		capturaLabel.setTypeface(gothamBook);
		cvvLabel.setTypeface(gothamBook);
		passLabel.setTypeface(gothamBook);
		
		servicioText.setTypeface(gothamBook);
		montoText.setTypeface(gothamBook);
		capturaText.setTypeface(gothamBook);
		
		setTexts();
		
		cancelarButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		pagarButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				compraProcess.start();
				
			}
		});
	}
	
	private void initData() {
		datosString = getIntent().getStringExtra("datos");
		Log.i(TAG, datosString);
		servicio = getIntent().getIntExtra("servicio", 0);
	}
	
	private JSONObject getDatos() {
		if (datos == null) {
			
			try {
				datos = new JSONObject(datosString);
			} catch (JSONException e) {
				return null;
			}
		}
		
		return datos;
	}
	
	private void setServicioText() {
		switch (servicio) {
			case Servicios.AGUA:
				servicioText.setText("Agua");
				break;
			case Servicios.INFRACCION:
				servicioText.setText("Infracciones");
				break;
			case Servicios.NOMINA:
				servicioText.setText("N�mina");
				break;
			case Servicios.PREDIAL:
				servicioText.setText("Predial");
				break;
			case Servicios.TENENCIA:
				servicioText.setText("Tenencia");
	
			default:
				break;
		}
	}
	
	private void setTexts() {
		setServicioText();
		
		String monto = getDatos().optString("totalPago", "No disponible");
		String captura = getDatos().optString("linea_captura", "No disponible");
		
		montoText.setText(monto);
		capturaText.setText(captura);
	}
	
	private void setNewJSONData(String cvv2, String token, String password) {
		try {
			getDatos().put("cvv2", cvv2);
			getDatos().put("token", token);
			getDatos().put("password", password);
		} catch (JSONException e) {
			Log.e(TAG, e.getClass().toString(), e);
		}
	}
	

}
