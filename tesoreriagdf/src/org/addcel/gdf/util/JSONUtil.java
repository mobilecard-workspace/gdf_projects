package org.addcel.gdf.util;

import org.addcel.gdf.constants.Servicios;
import org.addcel.gdf.constants.Terminos;
import org.addcel.utils.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.ironbit.mega.system.Sys;
import com.ironbit.mega.system.crypto.AddcelCrypto;
import com.ironbit.mega.usuario.Usuario;

public class JSONUtil {
	
	public static String getTokenRequest(String usuario, String password, int producto) {

		JSONObject json = new JSONObject();
		
		try {
			json.put("idProveedor", producto);
			json.put("usuario", usuario);
			json.put("password", password);
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptSensitive(Text.getKey(), json.toString());
	}
	
	public static String getProcomRequestAsString(Context con, JSONObject json, String idUsuario, int idProducto) {
		try {
			json.put("id_usuario", idUsuario);
			json.put("id_producto", idProducto);
			json.put("imei", DeviceUtils.getIMEI(con));
			json.put("tipo", DeviceUtils.getTipo());
			json.put("software", DeviceUtils.getSWVersion());
			json.put("modelo_procom", DeviceUtils.getModel());
			json.put("wkey", DeviceUtils.getIMEI(con));
			json.put("cx", "0.0");
			json.put("cy", "0.0");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		
		String key = Text.getKey();
		Sys.setComposedKey(key);
		
		return AddcelCrypto.encryptSensitive(key, json.toString().replace("\\", ""));
	}
	
	public static String getComunicadosRequestAsString() {
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.put("idAplicacion", 1);
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptHard(JSON.toString());
	}
	
	public static String getReenvioRequestAsString(Activity ctx,String idBitacora, int idProducto) {
		JSONObject j = new JSONObject();
		
		try {
			j.put("id_bitacora", idBitacora);
			j.put("id_producto", idProducto);
			j.put("id_usuario", Long.parseLong(Usuario.getIdUser(ctx)));
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptHard(j.toString());
	}
	
	public static String getTenenciaRequestAsString(String userId, String password, String placa, String ejercicio) {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.putOpt("toDO", Servicios.TENENCIA);
			JSON.putOpt("usuario", userId);
			JSON.putOpt("placa", placa);
			JSON.putOpt("ejercicio", ejercicio);
			JSON.putOpt("condonacion", false);
			JSON.putOpt("interes", false);
			JSON.putOpt("subsidio", true);
		} catch (JSONException e) {
			return "";
		}
		
		String cifrado =  AddcelCrypto.encryptSensitive(password, JSON.toString());
		
		Log.i("cifrado tenencia", cifrado);
		
		return cifrado;
	}
	
	public static String getInfraccionesRequestAsString(String password, String placa) {
		
		JSONObject JSON = new JSONObject();
		
		Log.i("password infraccion", password);
		
		try {
			JSON.putOpt("toDO", Servicios.INFRACCION);
			JSON.putOpt("placa", placa);
			
			Log.i("json infracciones", JSON.toString());
		} catch ( JSONException e) {
			return "";	
		}
		
		String cifrado =  AddcelCrypto.encryptSensitive(password, JSON.toString());
		
		Log.i("cifrado infracciones", cifrado);
		
		return cifrado;
	}
	
	public static String getNominaRequestAsString(String password, String rfc, String mes, String anio, int remuneraciones, int numTrabajadores) {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.put("toDO", Servicios.NOMINA);
			JSON.put("rfc", rfc);
			JSON.put("mes_pago_nomina", mes);
			JSON.put("anio_pago_nomina", anio);
			JSON.put("remuneraciones", remuneraciones);
			JSON.put("tipo_declaracion", 1);
			JSON.put("num_trabajadores", numTrabajadores);
			JSON.put("interes", true);
		} catch (JSONException e) {
			return "";
		}
		
		String cifrado =  AddcelCrypto.encryptSensitive(password, JSON.toString());
		
		Log.i("cifrado nomina", cifrado);
		
		return cifrado;
	}
	
	public static String getPredialRequestAsString(String password, String cuentaPredial) {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.put("toDO", Servicios.PREDIAL);
			JSON.put("cuenta", cuentaPredial);
		} catch (JSONException e) {
			return "";
		}
		
		String cifrado =  AddcelCrypto.encryptSensitive(password, JSON.toString());
		
		Log.i("cifrado predial", cifrado);
		
		return cifrado;
	}
	
	public static String getAguaRequestAsString(String password, String cuentaAgua) {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.put("toDO", Servicios.AGUA );
			JSON.put("cuenta", cuentaAgua);
		} catch (JSONException e){
			return "";
		}
		
		String cifrado = AddcelCrypto.encryptSensitive(password, JSON.toString());
		
		Log.i("cifrado agua", cifrado);
		
		return cifrado;
	}
	

	
	public static String getConsultaRequestAsString(Activity con, int idProducto, String mes, String anio) {
		
		JSONObject JSON = new JSONObject();
		long idUsuario = Long.parseLong(Usuario.getIdUser(con));
		String pass = Usuario.getPass(con);
		
		try {
			JSON.put("id_producto", idProducto);
			JSON.put("id_usuario", idUsuario);
			JSON.put("mes", mes);
			JSON.put("anio", anio);
			
			Log.i("consulta JSON", JSON.toString());
		} catch (JSONException e) {
			return "";
		}
		
		String cifrado = AddcelCrypto.encryptSensitive(pass, JSON.toString());
		
		Log.i("cifrado consulta", cifrado);
		
		return cifrado;
	}
	
	public static String getCondicionesRequestAsString() {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.put("id_aplicacion", Terminos.ID_APLICACION);
			JSON.put("id_producto", Terminos.ID_PRODUCTO);
		} catch (JSONException e) {
			return "";
		}
		
		String cifrado = AddcelCrypto.encryptHard(JSON.toString());
		
		Log.i("cifrado terminos", cifrado);
		
		return cifrado;
		
		
	}

}
