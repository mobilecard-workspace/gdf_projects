package org.addcel.gdf.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class Mysqlite extends SQLiteOpenHelper {
	
	public static final String TABLE_PLACAS = "placas";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_PLACA = "placa";
	
	public static final String TABLE_PREDIAL = "rfcs";
	public static final String COLUMN_ID_CUENTA = "_id";
	public static final String COLUMN_CUENTA = "_rfc";
	
	public static final String DATABASE = "placas.db";
	private static final int VERSION = 1;


	private static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS "
		      	+ TABLE_PLACAS + "(" + COLUMN_ID
		      	+ " integer primary key autoincrement, " + COLUMN_PLACA
		      	+ " text not null);";
	
	private static final String DATABASE_PREDIAL_CREATE = "CREATE TABLE IF NOT EXISTS "
			+TABLE_PREDIAL + "(" + COLUMN_ID_CUENTA
			+ " integer primary key autoincrement, " + COLUMN_CUENTA
			+ " text not null);";
	
	private static final String INITIAL_INSERT = "INSERT INTO "+ TABLE_PLACAS + 
												" ("+ COLUMN_PLACA +" ) " +
												"VALUES ('Ingreso Directo de Placa')";
	
	private static final String INITIAL_PREDIAL_INSERT = "INSERT INTO "+ TABLE_PREDIAL + 
													" ("+COLUMN_CUENTA+" ) " +
													"VALUES ('Ingreso Directo de Cuenta')";
	
	public Mysqlite(Context context) {
	    	super(context, DATABASE, null, VERSION);	    	
	  	}//constructor

	  @Override
	  public void onCreate(SQLiteDatabase database) {
		  database.execSQL(DATABASE_CREATE);
		  database.execSQL(DATABASE_PREDIAL_CREATE);
		  database.execSQL(INITIAL_INSERT);
		  database.execSQL(INITIAL_PREDIAL_INSERT);
		  
	  }//OnCreate para crear si no existe

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    Log.w(Mysqlite.class.getName(),
	        "Upgrading database from version " + oldVersion + " to "
	            + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLACAS);
	    onCreate(db);
	  }//actualizador de tabla


}
