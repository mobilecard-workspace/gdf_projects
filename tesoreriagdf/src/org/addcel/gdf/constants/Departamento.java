package org.addcel.gdf.constants;

public class Departamento {
	public static final int ERROR = -1;
	public static final int INMUEBLES = 0;
	public static final int VEHICULOS = 1;
	public static final int EMPRESAS = 2;
}
