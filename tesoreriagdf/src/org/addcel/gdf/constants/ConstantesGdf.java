package org.addcel.gdf.constants;

import java.util.ArrayList;

import android.graphics.Typeface;

public class ConstantesGdf {
	public static ArrayList<String> DEPARTAMENTOS = new ArrayList<String>();
	public static ArrayList<String> TRAMITES = new ArrayList<String>();
	public static String OTRA_PLACA = "Ingreso Directo";
	public static String URL_PAGO = "http://50.57.192.210:8080/ProComGDF/prosa_gdf.jsp";
	public static String URL_REGRESO = "http://50.57.192.210:8080/ProComGDF/regreso.jsp";
	public static boolean IS_LOGGED = false;
	
//	DEFAULT VALUES
	public static final String DEFAULT_ACTUALIZACION = "0.0";
	


}

