package org.addcel.gdf.constants;

public class URL {
	
	public static final String TESTING = "http://50.57.192.213:8080/GDFServicios/";
	public static final String PRODUCTION = "http://50.57.192.210:8080/GDFServicios/";
	public static String BASE_URL = TESTING;
	public static final String VERSION_BASE_URL = "http://www.mobilecard.mx:8080/Vitamedica/";

	public static final String CONSUMIDOR_TERMINOS = BASE_URL + "ConsumidorTerminos";
	public static final String BUSCA_COMUNICADOS = BASE_URL + "BuscaComunicados";
	public static final String CONSUMIDOR = BASE_URL + "Consumidor";
	public static final String CONSULTA_PAGOS = BASE_URL + "ConsultaPagos";
	public static final String REENVIAR_RECIBO = BASE_URL + "ReenvioReciboGDF";
	public static final String GET_TOKEN = BASE_URL + "getToken";
	public static final String PROCOM_3D_SECURE_GDF = BASE_URL + "Procom3DSecureGDF"; 
	public static final String PAGO_PROSA = BASE_URL + "ProsaPago";
	public static final String PROCOM_RETURN = BASE_URL + "ProcomAproviGDF";
	public static final String GET_VERSION = VERSION_BASE_URL + "getVersion";
}
