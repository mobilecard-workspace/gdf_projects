package org.addcel.gdf.constants;

public class Servicios {

	public static final int NOMINA = 1;
	public static final int TENENCIA = 2;
	public static final int INFRACCION = 3;
	public static final int PREDIAL = 4;
	public static final int AGUA = 5;
}
