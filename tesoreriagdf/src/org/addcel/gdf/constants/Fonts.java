package org.addcel.gdf.constants;

public class Fonts {

	public static final String GOTHAM_BOOK = "fonts/Gotham-Book.ttf";
	public static final String GOTHAM_BOLD = "fonts/Gotham-Bold.ttf";
}
