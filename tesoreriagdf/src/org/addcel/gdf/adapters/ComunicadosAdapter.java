package org.addcel.gdf.adapters;

import java.util.List;

import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.dto.Comunicado;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ComunicadosAdapter extends BaseAdapter {
	
	private Context con;
	private List<Comunicado> comunicados;
	private LayoutInflater inflater;
	private Typeface typeface;
	
	public ComunicadosAdapter(Context con, List<Comunicado> comunicados) {
		this.con = con;
		this.comunicados = comunicados;
		this.inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.typeface = Typeface.createFromAsset(con.getAssets(), Fonts.GOTHAM_BOOK);
	}
	
	public Context getCon() {
		return con;
	}

	public void setCon(Context con) {
		this.con = con;
	}

	public List<Comunicado> getComunicados() {
		return comunicados;
	}

	public void setComunicados(List<Comunicado> comunicados) {
		this.comunicados = comunicados;
	}

	public LayoutInflater getInflater() {
		if (null == inflater) {
			inflater = (LayoutInflater) getCon().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		return inflater;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return getComunicados().size();
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return getComunicados().get(arg0);
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return getComunicados().get(arg0).getIdComunicado();
	}

	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		Comunicado comunicado = getComunicados().get(arg0);
		
		View view = inflater.inflate(org.addcel.gdftesoreria.R.layout.item_comunicado, null);
		TextView texto = (TextView) view.findViewById(org.addcel.gdftesoreria.R.id.id_comunicado);
		texto.setText(comunicado.getContenido());
		texto.setTextSize(comunicado.getTamFuente());
		texto.setTextColor(Color.parseColor("#"+comunicado.getFgColor()));
		texto.setTypeface(typeface);
		
		return view;
	}
	

}
