/**
 * 
 */
package org.addcel.gdf.adapters;

import java.util.List;

import org.addcel.gdf.constants.Fonts;
import org.addcel.gdftesoreria.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * @author ADDCEL14
 *
 */
public class GdfListAdapter extends BaseAdapter {
	List<String> data;
	Activity act;
	LayoutInflater inflater;
	Typeface gothamBook;
	View v = null;
	
	public GdfListAdapter(Activity act, List<String> data){
		this.act = act;
		this.data = data;
		gothamBook = Typeface.createFromAsset(act.getAssets(), Fonts.GOTHAM_BOOK);
		inflater = (LayoutInflater)act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		String datoString = data.get(position);
		
		View v = null;		
		v = inflater.inflate(R.layout.gdf_list_item, null);
		TextView tv = (TextView) v.findViewById(R.id.nombre_tramite);
		tv.setText(tv.getText() + datoString);
		tv.setTypeface(gothamBook);
		
		return v;
	}

}
