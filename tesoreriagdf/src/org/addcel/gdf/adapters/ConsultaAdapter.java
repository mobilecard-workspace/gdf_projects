package org.addcel.gdf.adapters;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.addcel.gdf.constants.Servicios;
import org.addcel.gdftesoreria.R;
import org.addcel.utils.Text;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ConsultaAdapter extends BaseAdapter {
	
	private Context con;
	private List<JSONObject> data;
	private LayoutInflater inflater;
	private int servicio;
	
	public ConsultaAdapter(Context con, List<JSONObject> data, int servicio) {
		this.con = con;
		this.data = data;
		this.servicio = servicio;
		this.inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return data.get(arg0);
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		JSONObject consulta = data.get(arg0);
		View view = inflater.inflate(R.layout.item_consulta, null);
		
		Date d = Text.getDateFromString(consulta.optString("fechaPago"));
		Calendar c = Calendar.getInstance();
		
		c.setTime(d);
		
		TextView dia = (TextView) view.findViewById(R.id.text_dia);
		dia.setText("" + c.get(Calendar.DAY_OF_MONTH));
		
		TextView mes = (TextView) view.findViewById(R.id.text_mes);
		
		String mesString = new DateFormatSymbols().getMonths()[c.get(Calendar.MONTH)];
		mes.setText(mesString);
		
		TextView anio = (TextView) view.findViewById(R.id.text_anio);
		anio.setText("" + c.get(Calendar.YEAR));	
		
		TextView lineaCaptura = (TextView) view.findViewById(R.id.text_referencia);
		lineaCaptura.setText(consulta.optString("linea_captura"));

		TextView total = (TextView) view.findViewById(R.id.text_total);
		total.setText("$" + consulta.optInt("totalPago"));
		
		if (servicio == Servicios.TENENCIA) {
			TextView referencia = (TextView) view.findViewById(R.id.text_periodo);
			referencia.setText("Pago Tenencia");
			
		} else if (servicio == Servicios.INFRACCION) {
			TextView referencia = (TextView) view.findViewById(R.id.text_periodo);
			referencia.setText("Pago Infracciones");
			
		} else if (servicio == Servicios.NOMINA) {
			TextView referencia = (TextView) view.findViewById(R.id.text_periodo);
			referencia.setText("Pago N�mina");
			
		} else if (servicio == Servicios.PREDIAL) {
			TextView referencia = (TextView) view.findViewById(R.id.text_periodo);
			referencia.setText("Pago Predial");
			
		} else if (servicio == Servicios.AGUA) {
			TextView referencia = (TextView) view.findViewById(R.id.text_periodo);
			referencia.setText("Pago Agua");
		}
		return view;
	}

}
