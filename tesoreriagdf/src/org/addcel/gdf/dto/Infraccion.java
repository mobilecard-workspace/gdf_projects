package org.addcel.gdf.dto;

import org.json.JSONException;
import org.json.JSONObject;

public class Infraccion {
	
	private String placa;
	private String folio;
	private String lineaCaptura;
	private String importe;
	private String fechaInfraccion;
	private String actualizacion;
	private String recargos;
	private String diasMulta;
	private int hashCodeCalc;
	
	public Infraccion() {
		super();
	}
	
	public Infraccion(String placa, String folio, String lineaCaptura, String importe, int hashCodeCalc) {
		this.placa = placa;
		this.folio = folio;
		this.lineaCaptura = lineaCaptura;
		this.importe = importe;
		this.hashCodeCalc = hashCodeCalc;
	}
	
	public Infraccion(String placa, String folio, String lineaCaptura, String importe, String fechaInfraccion, String actualizacion, String recargos, String diasMulta, int hashCodeCalc) {
		this.placa = placa;
		this.folio = folio;
		this.lineaCaptura = lineaCaptura;
		this.importe = importe;
		this.fechaInfraccion = fechaInfraccion;
		this.actualizacion = actualizacion;
		this.recargos = recargos;
		this.diasMulta = diasMulta;
		this.hashCodeCalc = hashCodeCalc;
	}
	
	public String getPlaca() {
		return placa;
	}
	
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	public String getFolio() {
		return folio;
	}
	
	public void setFolio(String folio) {
		this.folio = folio;
	}
	
	public String getLineaCaptura() {
		return lineaCaptura;
	}
	
	public void setLineaCaptura(String lineaCaptura) {
		this.lineaCaptura = lineaCaptura;
	}
	
	public String getImporte() {
		return importe;
	}
	
	public void setImporte(String importe) {
		this.importe = importe;
	}
	

	
	public String getFechaInfraccion() {
		return fechaInfraccion;
	}

	public void setFechaInfraccion(String fechaInfraccion) {
		this.fechaInfraccion = fechaInfraccion;
	}

	public String getActualizacion() {
		return actualizacion;
	}

	public void setActualizacion(String actualizacion) {
		this.actualizacion = actualizacion;
	}

	public String getRecargos() {
		return recargos;
	}

	public void setRecargos(String recargos) {
		this.recargos = recargos;
	}

	public String getDiasMulta() {
		return diasMulta;
	}

	public void setDiasMulta(String diasMulta) {
		this.diasMulta = diasMulta;
	}

	public int getHashCodeCalc() {
		return hashCodeCalc;
	}
	
	public void setHashCodeCalc(int hashCodeCalc) {
		this.hashCodeCalc = hashCodeCalc;
	}
	
	public String formatImporte() {
		
		return "$" + importe;
	}
	
	public JSONObject toJSON() {
		JSONObject j = new JSONObject();
		
		try {
			j.put("placa", placa);
			j.put("folio", folio);
			j.put("codeCalc", hashCodeCalc);
			j.put("totalPago", importe);
			j.put("linea_captura", lineaCaptura);
			j.put("fechainfraccion", fechaInfraccion);
			j.put("actualizacion", actualizacion);
			j.put("recargos", recargos);
			j.put("dias_multa", diasMulta);
		} catch (JSONException e) {
			return null;
		}
		
		return j;
	}
	
	@Override
	public String toString() {
		return folio + " - "+formatImporte();
	}
}
