package org.addcel.gdf.dto;

public class TenenciaResponse {
	
	private double tenencia;
	private double actualizacion;
	private double recargos;
	private double refrendo; //derecho en json
	private double actRefrendo; //derActualizacion en json
	private double recRefrendo; //derRecargo en json
	private String total;
	private String vigencia;
	private String lineaCaptura;
	private int modelo;
	
	public double getTenencia() {
		return tenencia;
	}
	public void setTenencia(double tenencia) {
		this.tenencia = tenencia;
	}
	public double getActualizacion() {
		return actualizacion;
	}
	public void setActualizacion(double actualizacion) {
		this.actualizacion = actualizacion;
	}
	public double getRecargos() {
		return recargos;
	}
	public void setRecargos(double recargos) {
		this.recargos = recargos;
	}
	public double getRefrendo() {
		return refrendo;
	}
	public void setRefrendo(double refrendo) {
		this.refrendo = refrendo;
	}
	public double getActRefrendo() {
		return actRefrendo;
	}
	public void setActRefrendo(double actRefrendo) {
		this.actRefrendo = actRefrendo;
	}
	public double getRecRefrendo() {
		return recRefrendo;
	}
	public void setRecRefrendo(double recRefrendo) {
		this.recRefrendo = recRefrendo;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getLineaCaptura() {
		return lineaCaptura;
	}
	public void setLineaCaptura(String lineaCaptura) {
		this.lineaCaptura = lineaCaptura;
	}
	public int getModelo() {
		return modelo;
	}
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}
	@Override
	public String toString() {
		return "TenenciaResponse [total=" + total + ", lineaCaptura="
				+ lineaCaptura + "]";
	}
	
}
