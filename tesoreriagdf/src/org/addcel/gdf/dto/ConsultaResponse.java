package org.addcel.gdf.dto;

public class ConsultaResponse {
	
	private long idBitacora;
	private String noAutorizacion;
	private String lineaCaptura;
	private String fechaPago;
	
	public ConsultaResponse(long idBitacora, String noAutorizacion, String lineaCaptura, String fechaPago) {
		this.idBitacora = idBitacora;
		this.noAutorizacion = noAutorizacion;
		this.lineaCaptura = lineaCaptura;
		this.fechaPago = fechaPago;
	}

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}

	public String getNoAutorizacion() {
		return noAutorizacion;
	}

	public void setNoAutorizacion(String noAutorizacion) {
		this.noAutorizacion = noAutorizacion;
	}

	public String getLineaCaptura() {
		return lineaCaptura;
	}

	public void setLineaCaptura(String lineaCaptura) {
		this.lineaCaptura = lineaCaptura;
	}

	public String getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
}
