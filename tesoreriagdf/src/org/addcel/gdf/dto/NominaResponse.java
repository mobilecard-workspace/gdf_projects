package org.addcel.gdf.dto;

public class NominaResponse {
	
	private String clave;
	private String rfc;
	private String mes;
	private String anio;
	private int numTrabajadores;
	private int remuneraciones;
	private double impuesto;
	private double impuestoActualizado;
	private double recargos;
	private double recargosCondonado;
	private double interes;
	private String total;
	private String vigencia;
	private String lineaCaptura;
	private int error;
	private String errorDescripcion;
	
	public NominaResponse(String clave, String rfc, String anio, String mes, int remuneraciones, double impuesto,
			double recargos, String total, String vigencia, String lineaCaptura, int numTrabajadores) {
		super();
		this.clave = clave;
		this.rfc = rfc;
		this.anio = anio;
		this.mes = mes;
		this.numTrabajadores = numTrabajadores;
		this.remuneraciones = remuneraciones;
		this.impuesto = impuesto;
		this.impuestoActualizado = impuesto;
		this.recargos = recargos;
		this.total = total;
		this.vigencia = vigencia;
		this.lineaCaptura = lineaCaptura;
	}
	
	public NominaResponse(String clave, String rfc, String anio, String mes, int remuneraciones, double impuesto,
			double impuestoActualizado, double recargos, String total,
			String vigencia, String lineaCaptura, int numTrabajadores) {
		super();
		this.clave = clave;
		this.rfc = rfc;
		this.anio = anio;
		this.mes = mes;
		this.numTrabajadores = numTrabajadores;
		this.remuneraciones = remuneraciones;
		this.impuesto = impuesto;
		this.impuestoActualizado = impuestoActualizado;
		this.recargos = recargos;
		this.total = total;
		this.vigencia = vigencia;
		this.lineaCaptura = lineaCaptura;
	}



	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	
	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}
	
	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public int getRemuneraciones() {
		return remuneraciones;
	}

	public void setRemuneraciones(int remuneraciones) {
		this.remuneraciones = remuneraciones;
	}

	public double getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(double impuesto) {
		this.impuesto = impuesto;
	}

	public double getImpuestoActualizado() {
		return impuestoActualizado;
	}

	public void setImpuestoActualizado(double impuestoActualizado) {
		this.impuestoActualizado = impuestoActualizado;
	}

	public double getRecargos() {
		return recargos;
	}

	public void setRecargos(double recargos) {
		this.recargos = recargos;
	}

	public double getRecargosCondonado() {
		return recargosCondonado;
	}

	public void setRecargosCondonado(double recargosCondonado) {
		this.recargosCondonado = recargosCondonado;
	}

	public double getInteres() {
		return interes;
	}

	public void setInteres(double interes) {
		this.interes = interes;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getLineaCaptura() {
		return lineaCaptura;
	}

	public void setLineaCaptura(String lineaCaptura) {
		this.lineaCaptura = lineaCaptura;
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}

	public String getErrorDescripcion() {
		return errorDescripcion;
	}

	public void setErrorDescripcion(String errorDescripcion) {
		this.errorDescripcion = errorDescripcion;
	}
	
	public int getNumTrabajadores() {
		return numTrabajadores;
	}
	
	public void setNumTrabajadores(int numTrabajadores) {
		this.numTrabajadores = numTrabajadores;
	}
	
	@Override
	public String toString() {
		return clave + " - " + total;
	}
	
}
