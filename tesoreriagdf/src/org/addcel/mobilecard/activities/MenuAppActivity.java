package org.addcel.mobilecard.activities;

import java.util.List;

import org.addcel.gdf.activities.ConsultaActivity;
import org.addcel.gdf.activities.DepartamentoActivity;
import org.addcel.gdf.adapters.ComunicadosAdapter;
import org.addcel.gdf.constants.ConstantesGdf;
import org.addcel.gdf.constants.Fonts;
import org.addcel.gdf.dto.Comunicado;
import org.addcel.gdftesoreria.R;
import org.addcel.utils.Dialogos;
import org.addcel.utils.SessionManager;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.system.errores.ErrorSys;
import com.ironbit.mega.usuario.OnUserRegistradoListener;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.GetComunicadosWSClient;
import com.ironbit.mega.web.webservices.events.OnComunicadosResponseReceivedListener;

public class MenuAppActivity extends MenuActivity{
		
	private LongProcess loginProcess;
	private LongProcess comunicadoProcess;
	private String usuarioString;
	private String passwordString;
	public static boolean termina;
	private PopupWindow loginWindow;
	
	private TextView welcomeTextView;
	private EditText loginEditText;
	private EditText passwordEditText;
	private TextView secretariaText;
	private TextView finanzasText;
	private TextView avisoText;
		
	private Button registroButton;
	private Button loginButton;
	private Button pagoButton;
	
	private ListView avisosList;
	private ComunicadosAdapter adapter;
	private List<Comunicado> comunicados;
	
	private SessionManager session;
	
	private GetComunicadosWSClient comunicadoClient;
	
	public TextView getSecretariaText() {
		if (secretariaText == null) {
			secretariaText = (TextView) findViewById(R.id.text_secretaria);
		}
		
		return secretariaText;
	}
	
	public TextView getFinanzasText() {
		if (finanzasText == null) {
			finanzasText = (TextView) findViewById(R.id.text_finanzas);
		}
		
		return finanzasText;
	}
	
	public TextView getWelcomeTextView() {
		if (welcomeTextView == null) {
			welcomeTextView = (TextView) findViewById(R.id.texto_login);
		}
		
		return welcomeTextView;
	}
	
	public Button getRegistroButton() {
		if (registroButton == null) {
			registroButton = (Button) findViewById(R.id.button_registro);
		}
		
		return registroButton;
	}
	
	public Button getLoginButton() {
		if (loginButton == null) {
			loginButton = (Button) findViewById(R.id.button_login);
		}
		
		return loginButton;
	}
	
	public Button getPagoButton() {
		if (pagoButton == null) {
			pagoButton = (Button) findViewById(R.id.btn_tramites_gdf);
		}
		return pagoButton;
	}
	
	public TextView getAvisoText() {
		if (avisoText == null) {
			avisoText = (TextView) findViewById(R.id.texto_contribuyentes);
		}
		
		return avisoText;
	}
	
	public ListView getAvisosList() {
		if (null == avisosList) {
			avisosList = (ListView) findViewById(R.id.list_avisos);
		}
		
		return avisosList;
	}
	
	public ComunicadosAdapter getAdapter() {
		if (null == adapter) {
			adapter = new ComunicadosAdapter(this, comunicados);
		}
		
		return adapter;
	}
	
	public List<Comunicado> getComunicados() {
		return comunicados;
	}
	
	public void setComunicados(List<Comunicado> comunicados) {
		this.comunicados = comunicados;
	}
	
	
	/**
	 * 
	 * @param popup PopupWindow donde se encuentra el recurso login_edit
	 * @return EditText inicializado, despu�s de encontrar el view correcto
	 */
	public EditText getLoginEditText(View popup) {
		if (loginEditText == null) {
			loginEditText = (EditText) popup.findViewById(R.id.login_edit);
		}
		
		return loginEditText;
	}
	
	/**
	 * 
	 * @param popup PopupWindow donde se encuentra el recurso password_edit
	 * @return EditText inicializado, despu�s de encontrar el view correcto
	 */
	public EditText getPasswordEditText(View popup) {
		if (passwordEditText == null) {
			passwordEditText = (EditText) popup.findViewById(R.id.password_edit);
		}
		
		return passwordEditText;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		inicializarGUI(R.layout.gdf_principal);
		
		session = new SessionManager(MenuAppActivity.this);
		
		loginProcess = new LongProcess(this, true, 30, true, "login", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				Usuario.cancelarChequeoRegistro(MenuAppActivity.this);
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				Usuario.estaRegistrado(MenuAppActivity.this, usuarioString, passwordString, new OnUserRegistradoListener() {
					

					public void onUserRegistradoListener(boolean isRegistrado, String mensaje) {
						// TODO Auto-generated method stub
						if (isRegistrado) {
							String id = mensaje.substring(mensaje.indexOf("|") + 1, mensaje.length());
							Usuario.setLogin(MenuAppActivity.this, usuarioString);
							Usuario.setPass(MenuAppActivity.this, passwordString);
							Usuario.setIdUser(MenuAppActivity.this, id);
							
							ConstantesGdf.IS_LOGGED = true;
							getWelcomeTextView().setText(getWelcomeTextView().getText().toString() + Usuario.getLogin(MenuAppActivity.this));
							getWelcomeTextView().setVisibility(View.VISIBLE);
							Toast.makeText(MenuAppActivity.this, "Bienvenido a GDF, " + Usuario.getLogin(MenuAppActivity.this) , Toast.LENGTH_LONG).show();
							loginWindow.dismiss();
							
							termina = true;	
						} else {
							if (mensaje.equals("")) {
								mensaje = "Intentelo m�s tarde";
							}

							ErrorSys.showMsj(MenuAppActivity.this, mensaje);
						}
						
						loginProcess.processFinished();
					}
					
					public void onUserPassUpdate(String mensaje) {
						// TODO Auto-generated method stub
						Usuario.setLogin(MenuAppActivity.this, usuarioString);
						Usuario.setPass(MenuAppActivity.this, passwordString);
						startActivity(new Intent(MenuAppActivity.this, UpdatePassMailActivity.class));
						usuarioString = new String();
						passwordString =  new String();
						loginProcess.processFinished();
					}
					
					public void onUserDataUpdate(String mensaje) {
						// TODO Auto-generated method stub
						Usuario.setLogin(MenuAppActivity.this, usuarioString);
						Usuario.setPass(MenuAppActivity.this, passwordString);
						Intent intent = new Intent(MenuAppActivity.this, ModificarPassActivity.class);
						intent.putExtra("source", 0);
						startActivity(intent);
						usuarioString = new String();
						passwordString =  new String();
						loginProcess.processFinished();
					}
				});
			}
		});
		
		comunicadoProcess = new LongProcess(this, true, 60, true, "Comunicados", new LongProcessListener() {
			
			public void stopProcess() {
				// TODO Auto-generated method stub
				if (null != comunicadoClient) {
					comunicadoClient.cancel();
					comunicadoClient = null;
				}
			}
			
			public void doProcess() {
				// TODO Auto-generated method stub
				comunicadoClient = new GetComunicadosWSClient(MenuAppActivity.this);
				comunicadoClient.execute(new OnComunicadosResponseReceivedListener() {
					
					public void OnComunicadosResponseReceived(List<Comunicado> comunicados) {
						// TODO Auto-generated method stub
						getAvisosList().setAdapter(new ComunicadosAdapter(MenuAppActivity.this, comunicados));
						comunicadoProcess.processFinished();
					}
				});
			}
		});
		
		comunicadoProcess.start();
	}
	
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.MENU;
	}
	
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		
//		CREAMOS LAS TYPEFACE UTILIZANDO LOS PATHS QUE EST�N DECLARADOS COMO CONSTANTES EN LA CLASE ConstantesGdf
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		getSecretariaText().setTypeface(gothamBook);
		getFinanzasText().setTypeface(gothamBold);
		getWelcomeTextView().setTypeface(gothamBook);
		getPagoButton().setTypeface(gothamBook);
		getAvisoText().setTypeface(gothamBold);
		((Button)findViewById(R.id.btn_consulta_gdf)).setTypeface(gothamBook);
		
		if (Usuario.getLogin(MenuAppActivity.this) != null) {
			getWelcomeTextView().setText(getWelcomeTextView().getText().toString() + Usuario.getLogin(MenuAppActivity.this));
			getWelcomeTextView().setVisibility(View.VISIBLE);
		}
		
		getRegistroButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MenuAppActivity.this,RegistroActivity.class));
			}
		});
		
		getLoginButton().setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String userId = Usuario.getIdUser(MenuAppActivity.this);
				
				if (null == userId || "".equals(userId)) {
					Dialogos.loginAlert(MenuAppActivity.this, null);
				} else {
					getWelcomeTextView().setVisibility(View.GONE);
					Usuario.reset(MenuAppActivity.this);
					Toast.makeText(MenuAppActivity.this, "Gracias por hacer pagos con GDF. Hasta pronto.", Toast.LENGTH_SHORT).show();

				}
			}
		});
		
		findViewById(R.id.btn_consulta_gdf).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MenuAppActivity.this, ConsultaActivity.class));
			}
		});
		
		findViewById(R.id.btn_tramites_gdf).setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				Intent intent = new Intent(MenuAppActivity.this, DepartamentoActivity.class);
				startActivity(intent);
			}
		});
		
	}
	
	@Override
	public void onBackPressed(){
		super.onBackPressed();
		session.logoutUser();
		finish();
		
	}
}