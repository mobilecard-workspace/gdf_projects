package org.addcel.mobilecard.activities;

import java.util.ArrayList;

import org.addcel.gdf.constants.Fonts;
import org.addcel.gdftesoreria.R;
import org.apache.http.message.BasicNameValuePair;

import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ironbit.mega.activities.MenuActivity;
import com.ironbit.mega.activities.process.LongProcess;
import com.ironbit.mega.activities.process.LongProcessListener;
import com.ironbit.mega.activities.widget.TerminosWidget;
import com.ironbit.mega.form.validator.FormValidator;
import com.ironbit.mega.form.validator.Validable;
import com.ironbit.mega.system.Fecha;
import com.ironbit.mega.system.Fecha.FormatoFecha;
import com.ironbit.mega.system.Fecha.TipoFecha;
import com.ironbit.mega.system.Sys;
import com.ironbit.mega.system.Validador;
import com.ironbit.mega.system.errores.ErrorSys;
import com.ironbit.mega.system.errores.InfoSys;
import com.ironbit.mega.usuario.Usuario;
import com.ironbit.mega.web.webservices.GetBanksWSClient;
import com.ironbit.mega.web.webservices.GetCardTypesWSClient;
import com.ironbit.mega.web.webservices.GetEstadosWSClient;
import com.ironbit.mega.web.webservices.GetProvidersWSClient;
import com.ironbit.mega.web.webservices.UserInsertWSClient;
import com.ironbit.mega.web.webservices.data.DataProvider;
import com.ironbit.mega.web.webservices.events.OnBanksResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnCardTypesResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnProvidersResponseReceivedListener;
import com.ironbit.mega.web.webservices.events.OnUserRegisteredResponseReceivedListener;
import com.ironbit.mega.widget.SpinnerValues;
import com.ironbit.mega.widget.datepickerdialog.MyDatePickerDialog;

public class RegistroActivity extends MenuActivity{
	
	protected TextView headerText;
	protected TextView finanzasText;
	
	protected EditText txtUsuario = null;
	protected EditText txtNombre = null;
	protected EditText txtAPat = null;
	protected EditText txtAMat= null;
	protected Button btnFechaNacimiento = null;
	protected EditText txtCelular = null;
	protected EditText txtCelular2 = null;
	protected EditText txtMail = null;
	protected EditText txtMail2 = null;
	protected EditText curp = null;
	protected EditText rfc = null;
	protected EditText txtTelCasa = null;
	protected EditText txtTelOficina = null;
	protected EditText txtCiudad = null;
	protected EditText txtCalle= null;
	protected EditText txtNumExt = null;
	protected EditText txtNumInt= null;
	protected EditText txtColonia= null;
	protected EditText txtCP= null;
	
	protected Spinner cmbCtrlProveedor = null;
	protected SpinnerValues cmbProveedor = null;
	protected EditText txtTarjeta = null;
	protected Spinner cmbCtrlTipoTarjeta = null;
	protected SpinnerValues cmbTipoTarjeta = null;
	protected Spinner cmbCtrlBanco = null;
	protected SpinnerValues cmbBanco = null;
	protected Spinner cmbFechaVencimientoMes = null;
	protected Spinner cmbFechaVencimientoAnio = null;
	
	protected Spinner cmbSexo= null;
	protected Spinner cmbEstados= null;
	protected SpinnerValues cmbSVEstados= null;
	
	protected Button btnRegistrar = null;
	protected Button btnAtras;
	protected TextView btnCondiciones = null;
	protected CheckBox checkCondiciones = null;
	protected static final int FECHA_NACIMIENTO_DIALOG_ID = 0;
	protected MyDatePickerDialog fechaNacimientoDialog = null;
	protected Fecha fechaNacimiento = null;
	protected Fecha fechaVencimiento = null;
	protected GetProvidersWSClient providersWS = null;
	protected GetEstadosWSClient estadosWS = null;
	protected GetBanksWSClient banksWS = null;
	protected GetCardTypesWSClient cardTypesWS = null;
	protected final FormValidator formValidator = new FormValidator(this, true);
	protected LongProcess sendDataProcess = null;
	protected LongProcess loadDataProcess = null;
	protected UserInsertWSClient userRegisterWS = null;
	protected TerminosWidget terminosWidget = null;
	
	protected String tagTipo = "", tagNick = "", tagNum = "";
	int tagTipoId=0, tagVerif = 0;
	
	protected TextView getSecretariaText() {
		if (headerText == null) {
			headerText = (TextView) findViewById(R.id.text_secretaria);
		}
		
		return headerText;
	}
	
	protected TextView getFinanzasText() {
		if (finanzasText == null) {
			finanzasText = (TextView) findViewById(R.id.text_finanzas);
		}
		
		return finanzasText;
	}
	
	protected MyDatePickerDialog getFechaNacimientoDialog(){
		if (fechaNacimientoDialog == null){
			fechaNacimientoDialog = new MyDatePickerDialog(this, new DatePickerDialog.OnDateSetListener(){
				public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
					try{
						fechaNacimiento.setAnio(year);
						fechaNacimiento.setMesBase0(monthOfYear);
						fechaNacimiento.setDia(dayOfMonth);
						fechaNacimiento.actualizar();
						
						updateFechaNacimiento();
					}catch (ErrorSys e) {
						e.showMsj(RegistroActivity.this);
					}
				}
			},fechaNacimiento.getAnio() ,fechaNacimiento.getMesBase0(), fechaNacimiento.getDia());
		}
		
		return fechaNacimientoDialog;
	}
	
	
	protected CheckBox getCheckCondiciones(){
		if (checkCondiciones == null){
			checkCondiciones = (CheckBox)findViewById(R.id.checkCondiciones);
		}
		
		return checkCondiciones;
	}
	
	
	protected EditText getTxtUsuario(){
		if (txtUsuario == null){
			txtUsuario = (EditText)findViewById(R.id.txtUsuario);
		}
		
		return txtUsuario;
	}
	
	protected EditText getTxtMail(){
		if (txtMail == null){
			txtMail= (EditText)findViewById(R.id.txtEmail);
		}
		
		return txtMail;
	}
	
	/*	protected EditText curp = null;
	protected EditText rfc = null;
	protected EditText placas = null;
	protected EditText cuentaPredial;*/
	
	protected EditText getTxtCurp(){
		if (curp == null){
			curp =(EditText)findViewById(R.id.txtCurp);
		}
		
		return curp;
	}
	
	protected EditText getTxtRfc(){
		if (rfc == null){
			rfc =(EditText)findViewById(R.id.txtRfc);
		}
		
		return rfc;
	}
	
	protected EditText getTxtTelCasa(){
		if (txtTelCasa == null){
			txtTelCasa= (EditText)findViewById(R.id.txtTelCasa);
		}
		
		return txtTelCasa;
	}
	
	protected EditText getTxtTelOficina(){
		if (txtTelOficina == null){
			txtTelOficina= (EditText)findViewById(R.id.txtTelOficina);
		}
		
		return txtTelOficina;
	}
	
	protected EditText getTxtCiudad(){
		if (txtCiudad == null){
			txtCiudad= (EditText)findViewById(R.id.txtCiudad);
		}
		
		return txtCiudad;
	}
	
	protected EditText getTxtCalle(){
		if (txtCalle == null){
			txtCalle = (EditText)findViewById(R.id.txtCalle);
		}
		
		return txtCalle;
	}
	
	protected EditText getTxtColonia(){
		if (txtColonia == null){
			txtColonia = (EditText)findViewById(R.id.txtColonia);
		}
		
		return txtColonia;
	}
	
	protected EditText getTxtCP(){
		if (txtCP == null){
			txtCP = (EditText)findViewById(R.id.txtCP);
		}
		
		return txtCP;
	}
	
	protected EditText getTxtNumExt(){
		if (txtNumExt== null){
			txtNumExt= (EditText)findViewById(R.id.txtNumExt);
		}
		
		return txtNumExt;
	}
	
	protected EditText getTxtNumInt(){
		if (txtNumInt== null){
			txtNumInt= (EditText)findViewById(R.id.txtNumInt);
		}
		
		return txtNumInt;
	}
	
	protected EditText getTxtMail2(){
		if (txtMail2 == null){
			txtMail2 = (EditText)findViewById(R.id.txtEmail2);
		}
		
		return txtMail2;
	}
	
	protected EditText getTxtNombre(){
		if (txtNombre == null){
			txtNombre = (EditText)findViewById(R.id.txtNombre);
		}
		
		return txtNombre;
	}
	
	
	protected EditText getTxtApellidoPaterno(){
		if (txtAPat == null){
			txtAPat = (EditText)findViewById(R.id.txtApellidoP);
		}
		
		return txtAPat;
	}
	
	protected EditText getTxtApellidoMaterno(){
		if (txtAMat == null){
			txtAMat = (EditText)findViewById(R.id.txtApellidoM);
		}
		
		return txtAMat;
	}
	
	protected Button getBtnFechaNacimiento(){
		if (btnFechaNacimiento == null){
			btnFechaNacimiento = (Button)findViewById(R.id.btnFechaNacimiento);
		}
		
		return btnFechaNacimiento;
	}
	
	
	protected EditText getTxtCelular(){
		if (txtCelular == null){
			txtCelular = (EditText)findViewById(R.id.txtNoCelular);
		}
		
		return txtCelular;
	}
	
	protected EditText getTxtCelular2(){
		if (txtCelular2 == null){
			txtCelular2 = (EditText)findViewById(R.id.txtNoCelular2);
		}
		
		return txtCelular2;
	}
	
	protected Spinner getCmbSexo(){
		if (cmbSexo == null){
			cmbSexo = (Spinner)findViewById(R.id.spin_sexo);
			ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
		            this, R.array.sexo, android.R.layout.simple_spinner_item);
		    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    cmbSexo.setAdapter(adapter);
		}
		
		return cmbSexo;
	}
	
	protected Spinner getCmbEstados(){
		if (cmbEstados== null){
			cmbEstados = (Spinner)findViewById(R.id.spin_estados);
			/*ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
		            this, R.array.estados, android.R.layout.simple_spinner_item);
		    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    cmbEstados.setAdapter(adapter);*/
		}
		
		return cmbEstados;
	}
	
	protected SpinnerValues getCmbSVEstados(){
		if (cmbSVEstados== null){
			cmbSVEstados =  new SpinnerValues(this, getCmbEstados());
			/*ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
		            this, R.array.estados, android.R.layout.simple_spinner_item);
		    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    cmbEstados.setAdapter(adapter);*/
		}
		
		return cmbSVEstados;
	}
	
	protected Spinner getCmbCtrlProveedor(){
		if (cmbCtrlProveedor == null){
			cmbCtrlProveedor = (Spinner)findViewById(R.id.cmbProveedor);
		}
		
		return cmbCtrlProveedor;
	}
	
	
	protected SpinnerValues getCmbProveedor(){
		if (cmbProveedor == null){
			cmbProveedor = new SpinnerValues(this, getCmbCtrlProveedor());
		}
		
		return cmbProveedor;
	}
	
	
	/*protected Spinner getCmbCtrlBanco(){
		if (cmbCtrlBanco == null){
			cmbCtrlBanco = (Spinner)findViewById(R.id.cmbBanco);
		}
		
		return cmbCtrlBanco;
	}*/
	
	
	/*protected SpinnerValues getCmbBanco(){
		if (cmbBanco == null){
			cmbBanco = new SpinnerValues(this, getCmbCtrlBanco());
		}
		
		return cmbBanco;
	}*/
	
	
	protected Spinner getCmbCtrlTipoTarjeta(){
		if (cmbCtrlTipoTarjeta == null){
			cmbCtrlTipoTarjeta = (Spinner)findViewById(R.id.cmbTipoTarjeta);
		}
		
		return cmbCtrlTipoTarjeta;
	}
	
	
	protected SpinnerValues getCmbTipoTarjeta(){
		if (cmbTipoTarjeta == null){
			cmbTipoTarjeta = new SpinnerValues(this, getCmbCtrlTipoTarjeta());
		}
		
		return cmbTipoTarjeta;
	}
	
	
	protected EditText getTxtTarjeta(){
		if (txtTarjeta == null){
			txtTarjeta = (EditText)findViewById(R.id.txtNoTarjetaCredito);
		}
		
		return txtTarjeta;
	}
	
	
	protected Spinner getCmbFechaVencimientoAnio(){
		if (cmbFechaVencimientoAnio == null){
			cmbFechaVencimientoAnio = (Spinner)findViewById(R.id.cmbFechaVencimientoAnio);
		}
		
		return cmbFechaVencimientoAnio;
	}
	
	
	protected Spinner getCmbFechaVencimientoMes(){
		if (cmbFechaVencimientoMes == null){
			cmbFechaVencimientoMes = (Spinner)findViewById(R.id.cmbFechaVencimientoMes);
		}
		
		return cmbFechaVencimientoMes;
	}
	
	
	protected TextView getBtnCondiciones(){
		if (btnCondiciones == null){
			btnCondiciones = (TextView)findViewById(R.id.btnCondiciones);
		}
		
		return btnCondiciones;
	}
	
	
	protected Button getBtnRegistrar(){
		if (btnRegistrar == null){
			btnRegistrar = (Button)findViewById(R.id.button_registrar);
			
			System.out.println("ResName: "+getResources().getResourceName(R.id.btnRegistrarse));
		}
		
		return btnRegistrar;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inicializarGUI(R.layout.activity_mobilecard_registro);
		
		loadDataProcess = new LongProcess(this, true, 60, true, "LoadUserData", new LongProcessListener(){
			public void doProcess() {
				//inicializamos proveedores
				providersWS = new GetProvidersWSClient(RegistroActivity.this);
				providersWS.execute(new OnProvidersResponseReceivedListener(){
					public void onProvidersResponseReceived(ArrayList<DataProvider> providers) {
						ArrayList<BasicNameValuePair> data = new ArrayList<BasicNameValuePair>();
						
						for (DataProvider dp : providers){
							data.add(new BasicNameValuePair(
								dp.getDescription(),
								dp.getClave()+""
							));
						}
						
						getCmbProveedor().setDatos(data);
						
						//inicializamos bancos
						banksWS = new GetBanksWSClient(RegistroActivity.this);
						banksWS.execute(new OnBanksResponseReceivedListener(){
							public void onBanksResponseReceived(ArrayList<BasicNameValuePair> bancos) {
								//getCmbBanco().setDatos(bancos);
								
								//inicializamos tipos de tarjetas
								cardTypesWS = new GetCardTypesWSClient(RegistroActivity.this);
								cardTypesWS.execute(new OnCardTypesResponseReceivedListener(){
									public void onCardTypesResponseReceived(ArrayList<BasicNameValuePair> cardTypes) {
										getCmbTipoTarjeta().setDatos(cardTypes);
										
										//desbloqueamos boton de registro
										getBtnRegistrar().setEnabled(true);
										
										estadosWS = new GetEstadosWSClient(RegistroActivity.this);
										estadosWS.execute(new OnBanksResponseReceivedListener(){

											
											public void onBanksResponseReceived(
													ArrayList<BasicNameValuePair> ests) {
												// TODO Auto-generated method stu
												getCmbSVEstados().setDatos(ests);
												loadDataProcess.processFinished();
												
											}
											
										});
										//terminamos el proceso
										
									}
								});
							}
						});
					}
				});
			}
			
			public void stopProcess() {
				if (providersWS != null){
					providersWS.cancel();
					providersWS = null;
				}
				
				if (banksWS != null){
					banksWS.cancel();
					banksWS = null;
				}
				
				if (cardTypesWS != null){
					cardTypesWS.cancel();
					cardTypesWS = null;
				}
				
				if(estadosWS!=null){
					estadosWS.cancel();
					estadosWS = null;
				}
			}
		});
		
		sendDataProcess = new LongProcess(this, true, 40, true, "processRegistro", new LongProcessListener(){
			public void doProcess() {
				userRegisterWS = new UserInsertWSClient(RegistroActivity.this);
				userRegisterWS.setLogin(getTxtUsuario().getText().toString());
				//userRegisterWS.setPassword(getTxtPassword().getText().toString());
				userRegisterWS.setPassword("mCL8m39sJ1");
				userRegisterWS.setNombre(getTxtNombre().getText().toString());
				userRegisterWS.setApellido(getTxtApellidoPaterno().getText().toString());
				userRegisterWS.setMaterno(getTxtApellidoMaterno().getText().toString());
				userRegisterWS.setSexo(""+getCmbSexo().getSelectedItem().toString().charAt(0));
				userRegisterWS.setTelCasa(getTxtTelCasa().getText().toString());
				userRegisterWS.setTelOficina(getTxtTelOficina().getText().toString());
				userRegisterWS.setCiudad(getTxtCiudad().getText().toString());
				userRegisterWS.setCalle(getTxtCalle().getText().toString());
				userRegisterWS.setNumExt(Integer.parseInt(getTxtNumExt().getText().toString()));
				userRegisterWS.setNumInt("");
				userRegisterWS.setColonia(getTxtColonia().getText().toString());
				
				
				userRegisterWS.setCP(Integer.parseInt( getTxtCP().getText().toString()));
				
				userRegisterWS.setEstado(Integer.parseInt(getCmbSVEstados().getValorSeleccionado()));
				userRegisterWS.setFechaNacimiento(fechaNacimiento);
				userRegisterWS.setDireccion("");
				userRegisterWS.setEmail(getTxtMail().getText().toString());
				userRegisterWS.setCelular(getTxtCelular().getText().toString());
				userRegisterWS.setProveedor(Integer.parseInt(getCmbProveedor().getValorSeleccionado()));
				userRegisterWS.setTarjeta(getTxtTarjeta().getText().toString());
				userRegisterWS.setTipoTarjeta(Integer.parseInt(getCmbTipoTarjeta().getValorSeleccionado()));
				//userRegisterWS.setBanco(Integer.parseInt(getCmbBanco().getValorSeleccionado()));
				userRegisterWS.setBanco(1);
				userRegisterWS.setFechaVencimientoMes(Fecha.mesStringToInt(getCmbFechaVencimientoMes().getSelectedItem().toString()));
				userRegisterWS.setFechaVencimientoAnio(Integer.parseInt(getCmbFechaVencimientoAnio().getSelectedItem().toString()));
				
				userRegisterWS.setEtiqueta(tagNick);
				userRegisterWS.setIdtiporecargatag(tagTipoId);
				userRegisterWS.setNumeroTag(tagNum);
				userRegisterWS.setDv(tagVerif);
				
				
				userRegisterWS.execute(new OnUserRegisteredResponseReceivedListener(){
					public void onUserRegisteredResponseReceived(String resultado, String mensaje) {
						if (resultado.trim().equals("1")){
							//Guardamos los datos de inicio de sesión para futuras peticiones a los WS
							Usuario.setLogin(RegistroActivity.this, getTxtUsuario().getText().toString());
							//Usuario.setPass(RegistroActivity.this, getTxtPassword().getText().toString());
							//System.out.println("res: "+resultado+" msj: "+mensaje);
							InfoSys.showMsj(RegistroActivity.this, mensaje);
							
							//abrimos pantalla de menú y borramos esta actividad
							//startActivity(new Intent(RegistroActivity.this, MenuAppActivity.class));
							finish();
						}else{
							ErrorSys.showMsj(RegistroActivity.this, mensaje);
						}
						
						//avisamos que el proceso ya terminó
						sendDataProcess.processFinished();
					}
				});
			}
			
			public void stopProcess() {
				if (userRegisterWS != null){
					userRegisterWS.cancel();
					userRegisterWS = null;
				}
			}
		});
		
		
		loadDataProcess.start();
	}
	
	
	@Override
	protected void onStop() {
		if (terminosWidget != null){
			terminosWidget.onStop();
		}
		
		sendDataProcess.stop();
		super.onStop();
	}
	
	
	@Override
	protected void inicializarGUI(int layoutResID) {
		super.inicializarGUI(layoutResID);
		terminosWidget = new TerminosWidget(this);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		getSecretariaText().setTypeface(gothamBook);
		getFinanzasText().setTypeface(gothamBold);
		
		//inicializamos fechas
		try{
			fechaNacimiento = new Fecha();
			fechaNacimiento.restarAnios(18);
			fechaVencimiento = new Fecha("1990-01-01", TipoFecha.date);
		}catch (ErrorSys e) {
			e.showMsj(this);
		}
		
		findViewById(R.id.button_atras).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		
		//Botón Fecha Nacimiento
		getBtnFechaNacimiento().setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				//showDialog(FECHA_NACIMIENTO_DIALOG_ID);
				getFechaNacimientoDialog().show();
			}
		});
		
		
		//Boton Registrarse
		getBtnRegistrar().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				if (formValidator.validate()){
					sendDataProcess.start();
				}
			}
		});
		
		
		//Boton Terminos y Condiciones
		getBtnCondiciones().setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				terminosWidget.show();
			}
		});
		
		
		//fecha de nacimiento
		getFechaNacimientoDialog().setMinDate(1, 0, 1911);
		getFechaNacimientoDialog().setMaxDate(fechaNacimiento.getDia(), fechaNacimiento.getMesBase0(), fechaNacimiento.getAnio());
		
		
		
		//Año vencimiento
		String[] aniosVencimiento = new String[13];
		try{
			Fecha fechaNow = new Fecha();
			int anioActual = fechaNow.getAnio();
			
			for (int a = anioActual, i = 0; a <= anioActual+12; a++, i++){
				aniosVencimiento[i] = a+"";
			}
			
			//metemos array al Spinner
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				this, 
				android.R.layout.simple_spinner_item, 
				aniosVencimiento
			);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		    getCmbFechaVencimientoAnio().setAdapter(adapter);
		}catch(Exception e){
			Sys.log(e);
		}
		
		// getTxtDireccion().set
		System.out.println("setListener");
		 
		updateFechaNacimiento();
	}
	

	
	@Override
	public void configValidations() {
		formValidator.addItem(getTxtUsuario(), "Usuario")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(16)
			.minLength(4);
		
		formValidator.addItem(getTxtMail(), "Correo Electr�nico", new Validable(){
			public void validate() throws ErrorSys {
				if (!getTxtMail().getText().toString().equals(getTxtMail2().getText().toString())){
					getTxtMail2().requestFocus();
					throw new ErrorSys("Los correos electr�nicos deben coincidir entre s�");
				}
			}
		}).canBeEmpty(false)
		.maxLength(60)
		.isEmail(true);
		
		formValidator.addItem(getTxtNombre(), "Nombre")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(50)
			.minLength(3);
		
		
		
		formValidator.addItem(getTxtApellidoPaterno(), "Apellido Paterno")
			.canBeEmpty(false)
			.isRequired(true)
			.maxLength(50);
		formValidator.addItem(getTxtApellidoPaterno(), "Apellido Materno")
			.canBeEmpty(true)
			.isRequired(false)
			.maxLength(50);
		
		formValidator.addItem(getTxtCelular(), "N�mero de celular", new Validable(){
			public void validate() throws ErrorSys {
				if (!getTxtCelular().getText().toString().equals(getTxtCelular2().getText().toString())){
					getTxtCelular().requestFocus();
					throw new ErrorSys("N�meros de celular no coinciden");
				}

				if (!Validador.esNumeroCelularMX(getTxtCelular().getText().toString())){
					getTxtCelular().requestFocus();
					throw new ErrorSys("El n�" +
							"mero de celular debe ser de 10 d�gitos");
				}
			}
		}).canBeEmpty(false)
		.isRequired(true);
		
		
		
		formValidator.addItem(getTxtTarjeta(), "Tarjeta de cr�dito")
			.canBeEmpty(false)
			.isRequired(true)
			.isNumeric(true)
			.maxLength(20)
			.minLength(13);
		
		formValidator.addItem(getCmbSexo(), "Sexo", new Validable(){

			
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				//System.out.println("Sex item pos: "+getCmbSexo().getSelectedItemPosition());
				
				if(getCmbSexo().getSelectedItemPosition()==0){
					throw new ErrorSys("Debe seleccionar su sexo");
				}
			}
			
		});
		
		/*formValidator.addItem(getCmbEstados(), "Estado", new Validable(){

			@Override
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				//System.out.println("Estado item pos: "+getCmbEstados().getSelectedItemPosition());
				if(getCmbEstados().getSelectedItemPosition()==0){
					throw new ErrorSys("Debe seleccionar su estado");
				}
			}
			
		});*/
		
		formValidator.addItem(getTxtTelCasa(), "Tel�fono Casa")
		.canBeEmpty(true)
		.isRequired(false)
		.isPhoneNumber(true)
		.maxLength(10);
		
		formValidator.addItem(getTxtTelOficina(), "Tel�fono Oficina")
		.canBeEmpty(true)
		.isRequired(false)
		.isPhoneNumber(true)
		.maxLength(10);
		
		formValidator.addItem(getTxtCiudad(), "Ciudad")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(50);
		
		formValidator.addItem(getTxtCalle(), "Calle")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(50);
		
		formValidator.addItem(getTxtNumExt(), "N�mero exterior")
		.canBeEmpty(false)
		.isRequired(true)
		.isNumeric(true)
		.maxLength(5);
		
		formValidator.addItem(getTxtNumInt(), "N�mero interior")
		.canBeEmpty(true)
		.isRequired(false)
		.maxLength(20);
		
		formValidator.addItem(getTxtColonia(), "Colonia")
		.canBeEmpty(false)
		.isRequired(true)
		.maxLength(50);
		
		formValidator.addItem(getTxtCP(), "C�digo Postal")
		.canBeEmpty(true)
		.isRequired(false)
		.isNumeric(true)
		.maxLength(6);
		
		
		formValidator.addItem(getTxtRfc(), "RFC", new Validable(){
			public void validate() throws ErrorSys {
				if (!Validador.esRFC(getTxtRfc().getText().toString().trim())){
					getTxtRfc().requestFocus();
					throw new ErrorSys("RFC no v�lido");
				}
			}
		}).canBeEmpty(false)
		.isRequired(true);
		
		formValidator.addItem(getTxtCurp(), "CURP", new Validable() {
			
			public void validate() throws ErrorSys {
				// TODO Auto-generated method stub
				if (!Validador.esCurp(getTxtCurp().getText().toString().trim())) {
					getTxtCurp().requestFocus();
					throw new ErrorSys("Curp no v�lido");
				}
			}
		}).canBeEmpty(false)
		.isRequired(true);
		
		formValidator.addItem(getCmbFechaVencimientoMes(), "Fecha de Vencimiento", new Validable(){
			public void validate() throws ErrorSys {
				Fecha now = new Fecha();
				now.setHora(0);
				now.setMinuto(0);
				now.setSegundo(0);
				now.actualizar();
				
				Fecha fVencimiento = new Fecha();
				fVencimiento.setDia(1);
				fVencimiento.setMes(Fecha.mesStringToInt(getCmbFechaVencimientoMes().getSelectedItem().toString()));
				fVencimiento.setAnio(Integer.parseInt(getCmbFechaVencimientoAnio().getSelectedItem().toString()));
				fVencimiento.setHora(0);
				fVencimiento.setMinuto(0);
				fVencimiento.setSegundo(0);
				fVencimiento.actualizar();
				
				if (fVencimiento.getTimeStampSeg() <= now.getTimeStampSeg()){
					getCmbFechaVencimientoMes().requestFocus();
					throw new ErrorSys("La fecha de vencimiento debe ser mayor a la fecha actual");
				}
			}
		});
		
		formValidator.addItem(getCheckCondiciones(), "T�rminos y condiciones", new Validable(){
			public void validate() throws ErrorSys {
				if (!getCheckCondiciones().isChecked()){
					getCheckCondiciones().requestFocus();
					throw new ErrorSys("Es necesario Aceptar los Terminos y Condiciones de Uso");
				}
			}
		});
		
	}
	
	
	protected void updateFechaNacimiento(){
		if (fechaNacimiento != null){
			getBtnFechaNacimiento().setText("Seleccionar Fecha [" + fechaNacimiento.toFechaFormateada(FormatoFecha.fechaEsp_DDguionMMguionYYYY) + "]");
		}
	}
	
	
	@Override
	public Actividades getIdActividad() {
		return Actividades.REGISTRO;
	}
	

}