<%-- 
    Document   : comercio_con
    Created on : 25/06/2013, 01:50:13 PM
    Author     : JORGE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>3D Secure GDF</title>
    </head>
    <body>          
        <table>
            <tbody>
                <tr><td colspan="2">==* Transacción Bancaria *==</td></tr>
                <tr>
                    <td>Response:</td>
                    <td><c:out value="${param.EM_Response}"/></td>
                </tr>  
                <tr>
                    <td># Referencia:</td>
                    <td><c:out value="${param.EM_RefNum}"/></td>
                </tr>
                <tr>
                    <td># Autorización:</td>
                    <td><c:out value="${param.EM_Auth}"/></td>
                </tr>
                <tr><td colspan="2">==* Status GDF *==</td></tr>
                <tr><td colspan="2">Transacción exitosa</td></tr>
            </tbody>
        </table>                                                      
    </body>
</html>
