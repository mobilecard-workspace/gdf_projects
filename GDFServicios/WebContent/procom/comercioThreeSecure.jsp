<%-- 
    Document   : comercio_fin
    Created on : 25/06/2013, 01:49:53 PM
    Author     : JORGE
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>GDF</title>
        <script type="text/javascript">
            function sendform() {
                if (self.name.length === 0) {
                    self.name = "gotoProsa";
                }
                //var twnd = window.open("","wnd","toolbar=0,location=0,directories=0,status=0,menubar=0,resizable=1,copyhistory=0,width=760,height=750");
                document.form1.return_target.value = self.name.toString();
                //document.form1.target = "wnd";
                document.form1.submit();
            }
        </script>
    </head>
    <body onload="sendform();">
        <form method="post" name="form1" action="procom/pagina-prosa.jsp">                 
            <input type="hidden" name="total" value="${prosa.total}">
            <input type="hidden" name="currency" value="${prosa.currency}">
            <input type="hidden" name="address" value="${prosa.address}">
            <input type="hidden" name="order_id" value="${prosa.orderId}">
            <input type="hidden" name="merchant" value="${prosa.merchant}">
            <input type="hidden" name="store" value="${prosa.store}">
            <input type="hidden" name="term" value="${prosa.term}">
            <input type="hidden" name="digest" value="${prosa.digest}">
            <input type="hidden" name="return_target" value="">
            <input type="hidden" name="id_usuario" value="${prosa.usuario}">    
            <input type="hidden" name="id_producto" value="${prosa.idTramite}">
            <input type="hidden" name="urlBack" value="http://localhost:8080/GDFServices/ProsaGDFCcon">                        
        </form>
    </body>
</html>
