<%@page import="java.io.PrintWriter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">-->
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
<%--
#java/jsp/html
################################################################################
# Nombre del Programa :prosa_comercio_validaciones.jsp                         #
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :N/A                                   Fecha: N/A        #
# Descripcion General :Pagina para comercio electronico                        # 
# Programa Dependiente:N/A                                                     #
# Programa Subsecuente:N/A                                                     #
# Cond. de ejecucion  :N/A                                                     #
# Dias de ejecucion   :N/A                                      Horario:N/A    #
#                              MODIFICACIONES                                  #
#------------------------------------------------------------------------------#
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :C-04-2761-10                             Fecha:14/10/10 #
# Modificacion        :Nivelacion de Procom                                    #
# Marca de cambio     :C-04-2761-10 Acriter NAC                                #
#------------------------------------------------------------------------------#
# Autor               :Noe Albarran Ceron                                      #
# Compania            :Acriter S.A. de C.V.                                    #
# Proyecto/Procliente :C-04-2761-10 Fase2                       Fecha:20/01/11 #
# Modificacion        :Nivelacion de Procom Fase2                              #
# Marca de cambio     :Acriter NAC C-04-2761-10 Fase2                          #
#------------------------------------------------------------------------------#
# Numero de Parametros:N/A                                                     #
# Parametros Entrada  :N/A                                      Formato:N/A    #
# Parametros Salida   :N/A                                      Formato:N/A    #
################################################################################
--%>
<head>
    <title>Purchase Verification</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="HandheldFriendly" content="true"/>
    <style type="text/css">
        root { 
            display: block;
        }

        html
        {
            font-family: arial;
            font-size: 12px;
            font-weight: bold;
            color: black;
            background-color: white;
        }

        td{
            font-family: arial;
            font-size: 12px;
        }

        .title
        {
            font-family: arial;
            font-size: 12px;

        }

        .title2
        {
            font-family: arial;
            font-size: 12px;
        }

        input{
            width: 240px;
            height: 35px;
            font-family: arial;
            font-size: 14px;    
        }                

        .styled-select select {
            background: transparent;
            width: 268px;
            padding: 5px;
            font-size: 16px;
            border: 1px solid #ccc;
            height: 34px;
        }        
    </style>       
</head>
<%@page import="java.util.Enumeration"%>
<%//@page import="com.acriter.abi.procom.utils.StringHelper"%>
<%//@page import="com.acriter.abi.procom.model.constants.RequestParam"%>
<body>

<% 
String host = request.getParameter("host");
String sessionid = request.getParameter("sessionid");
Enumeration en = request.getParameterNames();


if (host != null && !host.equals("null") && !host.equals("")  && sessionid != null && !sessionid.equals("null") && !sessionid.equals("")) { %>
<link rel=stylesheet href="http://<%= host %>/clear.png?session=<%= sessionid %>">
<object type="application/x-shockwave-flash" data="https://<%= host %>/fp.swf" width="1" height="1" id="thm_fp"><param name="movie" value="https://<%= host %>/fp.swf"/><param name="FlashVars" value="session=<%= sessionid %>" /></object>
<script src="https://<%= host %>/check.js?session=<%= sessionid %>" type="text/javascript"></script>
<% } %>

<center>
    <div class="title">
        Portal 3D Secure Dummy GDF
    </div>
</center>
<br/>
<%-- Checar cambiar el action por el que esta a continuacion --%>
<%-- Invalidando Session --%>
<% session.invalidate(); %>
<!-- Modificacion: Marca de inicio Acriter NAC C-04-2761-10 Fase2 --> 
<form method="post" autocomplete="off" action="/GDFServices/ProcomAproviGDF">
<!--<FORM METHOD="POST" AUTOCOMPLETE="OFF" ACTION="./validaciones/valida.do">-->
<!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->

<input type="hidden" name="data_sent" value="1">

  
<%-- ------------------------------ Variables de Mas para el nuevo Procom ------------------------------- --%>
<input type="hidden" name="returnContext" value="<%=request.getContextPath()%>"/>
<input type="hidden" name="urlMerchant" value="<%=request.getServletPath()%>"/>
<!-- Modificacion: Marca de inicio Acriter NAC C-04-2761-10 Fase2 -->
<input type="hidden" name="urlpost" value="/urlpost.jsp"/>
<input type="hidden" name="urlerror" value="/urlpost.jsp"/>
<!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->
<input type="hidden" name="acquirer" value="83">
<input type="hidden" name="source" value="100">


<%
	
		String name = null;
		String value = null;

		while(en.hasMoreElements()){
		  name = (String)en.nextElement();
		  value = request.getParameter(name);
		  
		  %>
		  <input type="hidden" name="<%=name%>" value="<%=value%>">
		  <%
				
		}
                				
 %>
 <input type="hidden" name="EM_Response" value="approved"/>
 <input type="hidden" name="EM_Total" value="<%=request.getParameter("total")%>"/>
 <input type="hidden" name="EM_OrderID" value="<%=request.getParameter("order_id")%>"/>
 <input type="hidden" name="EM_Merchant" value="7454431"/>
 <input type="hidden" name="EM_Store" value="1234"/>
 <input type="hidden" name="EM_Term" value="001"/>
 <input type="hidden" name="EM_RefNum" value="000000000035"/>
 <input type="hidden" name="EM_Auth" value="057504"/>
 <input type="hidden" name="EM_Digest" value="070c6faf727825e24849c91b5429757e4979fd2e"/>
<%-- --------------------------------------------------------------------- --%>
<center>   
   Por favor proporcione la siguiente información:
</center>

<center>
    <table border="0">
        <thead>
            <tr>
                <td class="title2">
                    Información de la Tarjeta de Crédito.
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Nombre:
                </td>
            </tr>
            <tr>
                <td>
                    <INPUT TYPE="TEXT" NAME="cc_name" SIZE="40,1" MAXLENGTH="30" VALUE="" required="true">
                </td>
            </tr>
            <tr>
                <td>
                    Número de Tarjeta:
                </td>
            </tr>
            <tr>
                <td>
                    <INPUT TYPE="TEXT" NAME="cc_number" MAXLENGTH="19" SIZE="40,1" VALUE="" required="true">
                </td>
            </tr>
            <tr>
                <td>
                    Tipo:
                </td>
            </tr>
            <tr>
                <td>
                    <div class="styled-select">
                        <select name="cc_type">
                            <!-- Modificacion: Marca de inicio C-04-2761-10 Acriter NAC -->
                            <option value="Visa">VISA</option>
                            <option value="Mastercard">MasterCard</option>                           
                            <!-- Modificacion: Marca de inicio C-04-2761-10 Acriter NAC -->
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Fecha de vencimiento:
                </td>
            </tr>
            <tr>
                <td>
                    <div class="styled-select">
                        <select name="_cc_expmonth">
                            <option  value="01">1</option>
                            <option  value="02">2</option>
                            <option  value="03">3</option>
                            <option  value="04">4</option>
                            <option  value="05">5</option>
                            <option  value="06">6</option>
                            <option  value="07">7</option>
                            <option  value="08">8</option>
                            <option  value="09">9</option>
                            <option  value="10">10</option>
                            <option  value="11">11</option>
                            <option  value="12">12</option>
                        </select>    
                    </div>
                    <div class="styled-select">
                        <select name="_cc_expyear">
                            <%
                                java.util.Calendar C= java.util.Calendar.getInstance();
                                int anio=C.get(java.util.Calendar.YEAR);
                                out.println("<option selected>"+anio+"</option>");
                                anio++;
                                for(int i=1;i<15;i++)
                                {
                                    out.println("<option>"+anio+"</option>");
                                    anio++;
                                }
                            %>                
                        </select> 
                        </div>
                </td>
            </tr>
            
            <tr>
                <td>
                    Código de seguridad (CVV2/CVC2):
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="cc_cvv2" size="3,1" maxlength="3" value="" required="true" />
                </td>
            </tr>
            
                                    
       
            <tr>
                <td>
                    <input type="submit" value="Pagar"/>
                </td>
            </tr>
        
    </table>
</center>
</br>
</form>
</body>
</html>
