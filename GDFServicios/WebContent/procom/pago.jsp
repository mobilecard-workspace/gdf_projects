<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Pago de Servicios GDF</title>
</head>
<body>
<h1>Detalles del Pago</h1>

<div># Autorización Bancaria</div>
<div>${autoBan}</div>
<div># Referencia Bancaria</div>
<div>${refeBan}</div>
<div># Confirmación de Servicio GDF</div>
<div>${respgdf}</div>
<div># Monto pagado</div>
<div>${monto}</div>
<div>Se ha enviado un correo a la dirección registrada en Mobilecard con los detalles
	del su pago.
</div>

<%-- <table cellpadding="2" cellspacing="2" width="250px" height:"100%">
		<h1>Detalles del Pago</h1>
		<tbody>
			<tr>
				<td># Autorización Bancaria</td>
			</tr>
			<tr>
				<td>${autoBan}</td>
			</tr>
			<tr>
				<td># Referencia Bancaria</td>
			</tr>
			<tr>
				<td>${refeBan}</td>
			</tr>
			<tr>
				<td># Confirmación de Servicio GDF</td>
			</tr>
			<tr>
				<td>${respgdf}</td>
			</tr>
			<tr>
				<td># Monto pagado</td>
			</tr>
			<tr>
				<td>${monto}</td>
			</tr>
			<tr>
				<td colspan="2">Se ha enviado un correo a la dirección
					registrada en Mobilecard con los detalles del su pago.
			</tr>
		</tbody>
	</table> --%>
</body>
</html>