<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.Enumeration"%>
<%
	//@page import="com.acriter.abi.procom.utils.StringHelper"
%>
<%
	//@page import="com.acriter.abi.procom.model.constants.RequestParam"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Pago de Servicios GDF</title>
<style type="text/css">
#contenedor {
	width: 275px;
	margin: 0 auto;
}

html {
	font-family: arial;
	font-size: 12px;
	font-weight: bold;
	color: white;
	background-color: #414040;
}

td {
	font-family: arial;
	font-size: 12px;
}

.title {
	font-family: arial;
	font-size: 12px;
}

.title2 {
	font-family: arial;
	font-size: 12px;
}

input,select {
	width: 240px;
	height: 35px;
	font-family: arial;
	font-size: 14px;
}

select.mes {
	width: 120px;
}

.anio {
	width: 120px;
}

p.info {
	margin-top: 5px;
	margin-bottom: 5px;
}
</style>
</head>
<body>
	<!-- Lineas de procom -->
	<%
		String host = request.getParameter("host");
		String sessionid = request.getParameter("sessionid");
		Enumeration en = request.getParameterNames();

		if (host != null && !host.equals("null") && !host.equals("")
				&& sessionid != null && !sessionid.equals("null")
				&& !sessionid.equals("")) {
	%>
	<link rel=stylesheet
		href="http://<%=host%>/clear.png?session=<%=sessionid%>">
		<object type="application/x-shockwave-flash"
			data="https://<%=host%>/fp.swf" width="1" height="1" id="thm_fp">
			<param name="movie" value="https://<%=host%>/fp.swf" />
			<param name="FlashVars" value="session=<%=sessionid%>" />
		</object>
		<script src="https://<%=host%>/check.js?session=<%=sessionid%>"
			type="text/javascript"></script>
		<%
			}
		%>
		<!-- fin lineas de procom -->
	<div id="contenedor">
		<p style="text-align: center;">Portal 3D Secure GDF</p>
		<p class="info">Por favor proporcione la siguiente información:
			Información de la tarjeta de Crédito</p>
		<!-- lineas procom -->
		<%-- Checar cambiar el action por el que esta a continuacion --%>
		<%-- Invalidando Session --%>
		<%
			session.invalidate();
		%>
		<!-- Modificacion: Marca de inicio Acriter NAC C-04-2761-10 Fase2 -->
		<!--  -->
		<form autocomplete="off" action="comercio_con.jsp" method="post">
			<!-- lineas procom -->

			<!--<FORM METHOD="POST" AUTOCOMPLETE="OFF" ACTION="./validaciones/valida.do">-->
			<!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->

			<input type="hidden" name="data_sent" value="1"> <%-- ------------------------------ Variables de Mas para el nuevo Procom ------------------------------- --%>
				<input type="hidden" name="returnContext"
				value="<%=request.getContextPath()%>" /> <input type="hidden"
				name="urlMerchant" value="<%=request.getServletPath()%>" /> <!-- Modificacion: Marca de inicio Acriter NAC C-04-2761-10 Fase2 -->
				<input type="hidden" name="urlpost" value="/urlpost.jsp" /> <input
				type="hidden" name="urlerror" value="/urlpost.jsp" /> <!-- Modificacion: Marca de fin Acriter NAC C-04-2761-10 Fase2 -->
				<input type="hidden" name="acquirer" value="83"> <input
					type="hidden" name="source" value="100"> <%
 	String name = null;
 	String value = null;

 	while (en.hasMoreElements()) {
 		name = (String) en.nextElement();
 		value = request.getParameter(name);
 %> <input type="hidden" name="<%=name%>" value="<%=value%>">
							<%
								}
							%> <!--  -->
			<table>
				<tbody>
					<tr>
						<td>Nombre:</td>
					</tr>
					<tr>
						<td><input type="text" name="cc_name" size="40,1"
							maxlength="40" value="" required="true" /></td>
					</tr>
					<tr>
						<td>Número de Tarjeta:</td>
					</tr>
					<tr>
						<td><input type="text" name="cc_number" size="40,1"
							maxlength="19" value="" required="true" /></td>
					</tr>
					<tr>
						<td>Tipo:</td>
					</tr>
					<tr>
						<td><select name="cc_type">
								<option value="Visa">VISA</option>
								<option value="Mastercard">MasterCard</option>
						</select></td>
					</tr>
					<tr>
						<td>Fecha de Vencimiento (mes-año):</td>
					</tr>
					<tr>
						<td><select class="mes" name="_cc_expmonth">
								<option value="01">1</option>
								<option value="02">2</option>
								<option value="03">3</option>
								<option value="04">4</option>
								<option value="05">5</option>
								<option value="06">6</option>
								<option value="07">7</option>
								<option value="08">8</option>
								<option value="09">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
						</select> <select class="anio" name="_cc_expyear">
								<%
									java.util.Calendar C = java.util.Calendar.getInstance();
									int anio = C.get(java.util.Calendar.YEAR);
									out.println("<option selected>" + anio + "</option>");
									anio++;
									for (int i = 1; i < 15; i++) {
										out.println("<option>" + anio + "</option>");
										anio++;
									}
								%>
						</select></td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td>Código de seguridad(CVV2/CVC2):</td>
					</tr>
					<tr>
						<td><input type="text" name="cc_cvv2" size="3,1"
							maxlength="3" value="" required="true" /></td>
					</tr>
					<tr>
						<td><input type="submit" value="Pagar" /></td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
</body>
</html>