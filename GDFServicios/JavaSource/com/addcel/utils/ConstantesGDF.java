package com.addcel.utils;

import java.text.SimpleDateFormat;

public class ConstantesGDF {
	
	private static final SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private static final SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
	
	public static final int idProd_Nomina = 1;
	public static final int idProd_Tenencia = 2;
	public static final int idProd_Infraccion = 3;
	public static final int idProd_Predial = 4;
	public static final int idProd_Agua = 5;
	// MLS Predial vencido
	public static final int idProd_PredialVencido= 6; 
	
	public static final int ID_PRODUCTO_TARJETA_CIRCULACION = 7;
	
	public static final int ID_PRODUCTO_LICENCIA_CONDUCTOR = 8;
	
	public static final int ID_PRODUCTO_LICENCIA_PERMANENTE = 9;
	
	public static final String USUARIO_WS_VEHICULAR = "kioskos";
	
	public static final String PASSWORD_WS_VEHICULAR = "bf2579b6ccd26082aaacf2b8e0774ff5";
}
