package com.addcel.gdf.ws.clientes.infraccion;

public class ServicesPortTypeProxy implements com.addcel.gdf.ws.clientes.infraccion.ServicesPortType {
  private String _endpoint = null;
  private com.addcel.gdf.ws.clientes.infraccion.ServicesPortType servicesPortType = null;
  
  public ServicesPortTypeProxy() {
    _initServicesPortTypeProxy();
  }
  
  public ServicesPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initServicesPortTypeProxy();
  }
  
  private void _initServicesPortTypeProxy() {
    try {
      servicesPortType = (new com.addcel.gdf.ws.clientes.infraccion.ServicesLocator()).getServicesPort();
      if (servicesPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)servicesPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)servicesPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (servicesPortType != null)
      ((javax.xml.rpc.Stub)servicesPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.gdf.ws.clientes.infraccion.ServicesPortType getServicesPortType() {
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType;
  }
  
  public com.addcel.gdf.ws.clientes.infraccion.Arreglo_respuesta_wrapper solicitar_datos(com.addcel.gdf.ws.clientes.infraccion.Tipo_pregunta tipo_pregunta) throws java.rmi.RemoteException{
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType.solicitar_datos(tipo_pregunta);
  }
  
  
}