/**
 * Tipo_respuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.finanzas;

public class Tipo_respuesta  implements java.io.Serializable {
    private java.math.BigDecimal secuencia_trans;

    private java.math.BigDecimal error;

    private java.lang.String error_descripcion;

    public Tipo_respuesta() {
    }

    public Tipo_respuesta(
           java.math.BigDecimal secuencia_trans,
           java.math.BigDecimal error,
           java.lang.String error_descripcion) {
           this.secuencia_trans = secuencia_trans;
           this.error = error;
           this.error_descripcion = error_descripcion;
    }


    /**
     * Gets the secuencia_trans value for this Tipo_respuesta.
     * 
     * @return secuencia_trans
     */
    public java.math.BigDecimal getSecuencia_trans() {
        return secuencia_trans;
    }


    /**
     * Sets the secuencia_trans value for this Tipo_respuesta.
     * 
     * @param secuencia_trans
     */
    public void setSecuencia_trans(java.math.BigDecimal secuencia_trans) {
        this.secuencia_trans = secuencia_trans;
    }


    /**
     * Gets the error value for this Tipo_respuesta.
     * 
     * @return error
     */
    public java.math.BigDecimal getError() {
        return error;
    }


    /**
     * Sets the error value for this Tipo_respuesta.
     * 
     * @param error
     */
    public void setError(java.math.BigDecimal error) {
        this.error = error;
    }


    /**
     * Gets the error_descripcion value for this Tipo_respuesta.
     * 
     * @return error_descripcion
     */
    public java.lang.String getError_descripcion() {
        return error_descripcion;
    }


    /**
     * Sets the error_descripcion value for this Tipo_respuesta.
     * 
     * @param error_descripcion
     */
    public void setError_descripcion(java.lang.String error_descripcion) {
        this.error_descripcion = error_descripcion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_respuesta)) return false;
        Tipo_respuesta other = (Tipo_respuesta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.secuencia_trans==null && other.getSecuencia_trans()==null) || 
             (this.secuencia_trans!=null &&
              this.secuencia_trans.equals(other.getSecuencia_trans()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.error_descripcion==null && other.getError_descripcion()==null) || 
             (this.error_descripcion!=null &&
              this.error_descripcion.equals(other.getError_descripcion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSecuencia_trans() != null) {
            _hashCode += getSecuencia_trans().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getError_descripcion() != null) {
            _hashCode += getError_descripcion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_respuesta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://wsbancos.finanzas.df.gob.mx/fut/webserv_trans_pagos/informacion_ws_server.php", "tipo_respuesta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("secuencia_trans");
        elemField.setXmlName(new javax.xml.namespace.QName("", "secuencia_trans"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error_descripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error_descripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
