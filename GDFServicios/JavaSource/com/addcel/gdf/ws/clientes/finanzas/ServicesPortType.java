/**
 * ServicesPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.finanzas;

public interface ServicesPortType extends java.rmi.Remote {

    /**
     * Registra pago de banco
     */
    public com.addcel.gdf.ws.clientes.finanzas.Tipo_respuesta registrar_pago(com.addcel.gdf.ws.clientes.finanzas.Tipo_pregunta pregunta) throws java.rmi.RemoteException;

    /**
     * Registra pago de banco
     */
    public com.addcel.gdf.ws.clientes.finanzas.Tipo_respuesta_cancela cancelar_pago(com.addcel.gdf.ws.clientes.finanzas.Tipo_pregunta_cancela pregunta) throws java.rmi.RemoteException;
}
