/**
 * ServicesPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.nomina;

public interface ServicesPortType extends java.rmi.Remote {

    /**
     * Calcula la linea de captura de nomina
     */
    public com.addcel.gdf.ws.clientes.nomina.Tipo_respuesta solicitar_lc(com.addcel.gdf.ws.clientes.nomina.Tipo_pregunta pregunta) throws java.rmi.RemoteException;
}
