/**
 * Tipo_pregunta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.nomina;

public class Tipo_pregunta  implements java.io.Serializable {
    private java.lang.String rfc;

    private java.lang.String mes_pago_nomina;

    private java.lang.String anio_pago_nomina;

    private java.math.BigDecimal remuneraciones;

    private java.math.BigDecimal tipo_declaracion;

    private java.math.BigDecimal num_trabajadores;

    private boolean interes;

    private java.lang.String usuario;

    private java.lang.String password;

    public Tipo_pregunta() {
    }

    public Tipo_pregunta(
           java.lang.String rfc,
           java.lang.String mes_pago_nomina,
           java.lang.String anio_pago_nomina,
           java.math.BigDecimal remuneraciones,
           java.math.BigDecimal tipo_declaracion,
           java.math.BigDecimal num_trabajadores,
           boolean interes,
           java.lang.String usuario,
           java.lang.String password) {
           this.rfc = rfc;
           this.mes_pago_nomina = mes_pago_nomina;
           this.anio_pago_nomina = anio_pago_nomina;
           this.remuneraciones = remuneraciones;
           this.tipo_declaracion = tipo_declaracion;
           this.num_trabajadores = num_trabajadores;
           this.interes = interes;
           this.usuario = usuario;
           this.password = password;
    }


    /**
     * Gets the rfc value for this Tipo_pregunta.
     * 
     * @return rfc
     */
    public java.lang.String getRfc() {
        return rfc;
    }


    /**
     * Sets the rfc value for this Tipo_pregunta.
     * 
     * @param rfc
     */
    public void setRfc(java.lang.String rfc) {
        this.rfc = rfc;
    }


    /**
     * Gets the mes_pago_nomina value for this Tipo_pregunta.
     * 
     * @return mes_pago_nomina
     */
    public java.lang.String getMes_pago_nomina() {
        return mes_pago_nomina;
    }


    /**
     * Sets the mes_pago_nomina value for this Tipo_pregunta.
     * 
     * @param mes_pago_nomina
     */
    public void setMes_pago_nomina(java.lang.String mes_pago_nomina) {
        this.mes_pago_nomina = mes_pago_nomina;
    }


    /**
     * Gets the anio_pago_nomina value for this Tipo_pregunta.
     * 
     * @return anio_pago_nomina
     */
    public java.lang.String getAnio_pago_nomina() {
        return anio_pago_nomina;
    }


    /**
     * Sets the anio_pago_nomina value for this Tipo_pregunta.
     * 
     * @param anio_pago_nomina
     */
    public void setAnio_pago_nomina(java.lang.String anio_pago_nomina) {
        this.anio_pago_nomina = anio_pago_nomina;
    }


    /**
     * Gets the remuneraciones value for this Tipo_pregunta.
     * 
     * @return remuneraciones
     */
    public java.math.BigDecimal getRemuneraciones() {
        return remuneraciones;
    }


    /**
     * Sets the remuneraciones value for this Tipo_pregunta.
     * 
     * @param remuneraciones
     */
    public void setRemuneraciones(java.math.BigDecimal remuneraciones) {
        this.remuneraciones = remuneraciones;
    }


    /**
     * Gets the tipo_declaracion value for this Tipo_pregunta.
     * 
     * @return tipo_declaracion
     */
    public java.math.BigDecimal getTipo_declaracion() {
        return tipo_declaracion;
    }


    /**
     * Sets the tipo_declaracion value for this Tipo_pregunta.
     * 
     * @param tipo_declaracion
     */
    public void setTipo_declaracion(java.math.BigDecimal tipo_declaracion) {
        this.tipo_declaracion = tipo_declaracion;
    }


    /**
     * Gets the num_trabajadores value for this Tipo_pregunta.
     * 
     * @return num_trabajadores
     */
    public java.math.BigDecimal getNum_trabajadores() {
        return num_trabajadores;
    }


    /**
     * Sets the num_trabajadores value for this Tipo_pregunta.
     * 
     * @param num_trabajadores
     */
    public void setNum_trabajadores(java.math.BigDecimal num_trabajadores) {
        this.num_trabajadores = num_trabajadores;
    }


    /**
     * Gets the interes value for this Tipo_pregunta.
     * 
     * @return interes
     */
    public boolean isInteres() {
        return interes;
    }


    /**
     * Sets the interes value for this Tipo_pregunta.
     * 
     * @param interes
     */
    public void setInteres(boolean interes) {
        this.interes = interes;
    }


    /**
     * Gets the usuario value for this Tipo_pregunta.
     * 
     * @return usuario
     */
    public java.lang.String getUsuario() {
        return usuario;
    }


    /**
     * Sets the usuario value for this Tipo_pregunta.
     * 
     * @param usuario
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }


    /**
     * Gets the password value for this Tipo_pregunta.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this Tipo_pregunta.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_pregunta)) return false;
        Tipo_pregunta other = (Tipo_pregunta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rfc==null && other.getRfc()==null) || 
             (this.rfc!=null &&
              this.rfc.equals(other.getRfc()))) &&
            ((this.mes_pago_nomina==null && other.getMes_pago_nomina()==null) || 
             (this.mes_pago_nomina!=null &&
              this.mes_pago_nomina.equals(other.getMes_pago_nomina()))) &&
            ((this.anio_pago_nomina==null && other.getAnio_pago_nomina()==null) || 
             (this.anio_pago_nomina!=null &&
              this.anio_pago_nomina.equals(other.getAnio_pago_nomina()))) &&
            ((this.remuneraciones==null && other.getRemuneraciones()==null) || 
             (this.remuneraciones!=null &&
              this.remuneraciones.equals(other.getRemuneraciones()))) &&
            ((this.tipo_declaracion==null && other.getTipo_declaracion()==null) || 
             (this.tipo_declaracion!=null &&
              this.tipo_declaracion.equals(other.getTipo_declaracion()))) &&
            ((this.num_trabajadores==null && other.getNum_trabajadores()==null) || 
             (this.num_trabajadores!=null &&
              this.num_trabajadores.equals(other.getNum_trabajadores()))) &&
            this.interes == other.isInteres() &&
            ((this.usuario==null && other.getUsuario()==null) || 
             (this.usuario!=null &&
              this.usuario.equals(other.getUsuario()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRfc() != null) {
            _hashCode += getRfc().hashCode();
        }
        if (getMes_pago_nomina() != null) {
            _hashCode += getMes_pago_nomina().hashCode();
        }
        if (getAnio_pago_nomina() != null) {
            _hashCode += getAnio_pago_nomina().hashCode();
        }
        if (getRemuneraciones() != null) {
            _hashCode += getRemuneraciones().hashCode();
        }
        if (getTipo_declaracion() != null) {
            _hashCode += getTipo_declaracion().hashCode();
        }
        if (getNum_trabajadores() != null) {
            _hashCode += getNum_trabajadores().hashCode();
        }
        _hashCode += (isInteres() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getUsuario() != null) {
            _hashCode += getUsuario().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_pregunta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://wsbancos.finanzas.df.gob.mx/fut/nomina/nom_ws_secure_server.php", "tipo_pregunta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rfc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rfc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mes_pago_nomina");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mes_pago_nomina"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anio_pago_nomina");
        elemField.setXmlName(new javax.xml.namespace.QName("", "anio_pago_nomina"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remuneraciones");
        elemField.setXmlName(new javax.xml.namespace.QName("", "remuneraciones"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo_declaracion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipo_declaracion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("num_trabajadores");
        elemField.setXmlName(new javax.xml.namespace.QName("", "num_trabajadores"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "interes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
