/**
 * Tipo_pregunta_emi.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.tarjetaCirculacion;

public class Tipo_pregunta_emi  implements java.io.Serializable {
    private java.math.BigDecimal clave;

    private java.math.BigDecimal subconcepto;

    private java.lang.String id_marca;

    private java.math.BigDecimal modelo;

    private java.lang.String placa;

    private java.lang.String vencimiento;

    private java.lang.String usuario;

    private java.lang.String password;

    public Tipo_pregunta_emi() {
    }

    public Tipo_pregunta_emi(
           java.math.BigDecimal clave,
           java.math.BigDecimal subconcepto,
           java.lang.String id_marca,
           java.math.BigDecimal modelo,
           java.lang.String placa,
           java.lang.String vencimiento,
           java.lang.String usuario,
           java.lang.String password) {
           this.clave = clave;
           this.subconcepto = subconcepto;
           this.id_marca = id_marca;
           this.modelo = modelo;
           this.placa = placa;
           this.vencimiento = vencimiento;
           this.usuario = usuario;
           this.password = password;
    }


    /**
     * Gets the clave value for this Tipo_pregunta_emi.
     * 
     * @return clave
     */
    public java.math.BigDecimal getClave() {
        return clave;
    }


    /**
     * Sets the clave value for this Tipo_pregunta_emi.
     * 
     * @param clave
     */
    public void setClave(java.math.BigDecimal clave) {
        this.clave = clave;
    }


    /**
     * Gets the subconcepto value for this Tipo_pregunta_emi.
     * 
     * @return subconcepto
     */
    public java.math.BigDecimal getSubconcepto() {
        return subconcepto;
    }


    /**
     * Sets the subconcepto value for this Tipo_pregunta_emi.
     * 
     * @param subconcepto
     */
    public void setSubconcepto(java.math.BigDecimal subconcepto) {
        this.subconcepto = subconcepto;
    }


    /**
     * Gets the id_marca value for this Tipo_pregunta_emi.
     * 
     * @return id_marca
     */
    public java.lang.String getId_marca() {
        return id_marca;
    }


    /**
     * Sets the id_marca value for this Tipo_pregunta_emi.
     * 
     * @param id_marca
     */
    public void setId_marca(java.lang.String id_marca) {
        this.id_marca = id_marca;
    }


    /**
     * Gets the modelo value for this Tipo_pregunta_emi.
     * 
     * @return modelo
     */
    public java.math.BigDecimal getModelo() {
        return modelo;
    }


    /**
     * Sets the modelo value for this Tipo_pregunta_emi.
     * 
     * @param modelo
     */
    public void setModelo(java.math.BigDecimal modelo) {
        this.modelo = modelo;
    }


    /**
     * Gets the placa value for this Tipo_pregunta_emi.
     * 
     * @return placa
     */
    public java.lang.String getPlaca() {
        return placa;
    }


    /**
     * Sets the placa value for this Tipo_pregunta_emi.
     * 
     * @param placa
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }


    /**
     * Gets the vencimiento value for this Tipo_pregunta_emi.
     * 
     * @return vencimiento
     */
    public java.lang.String getVencimiento() {
        return vencimiento;
    }


    /**
     * Sets the vencimiento value for this Tipo_pregunta_emi.
     * 
     * @param vencimiento
     */
    public void setVencimiento(java.lang.String vencimiento) {
        this.vencimiento = vencimiento;
    }


    /**
     * Gets the usuario value for this Tipo_pregunta_emi.
     * 
     * @return usuario
     */
    public java.lang.String getUsuario() {
        return usuario;
    }


    /**
     * Sets the usuario value for this Tipo_pregunta_emi.
     * 
     * @param usuario
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }


    /**
     * Gets the password value for this Tipo_pregunta_emi.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this Tipo_pregunta_emi.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_pregunta_emi)) return false;
        Tipo_pregunta_emi other = (Tipo_pregunta_emi) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.clave==null && other.getClave()==null) || 
             (this.clave!=null &&
              this.clave.equals(other.getClave()))) &&
            ((this.subconcepto==null && other.getSubconcepto()==null) || 
             (this.subconcepto!=null &&
              this.subconcepto.equals(other.getSubconcepto()))) &&
            ((this.id_marca==null && other.getId_marca()==null) || 
             (this.id_marca!=null &&
              this.id_marca.equals(other.getId_marca()))) &&
            ((this.modelo==null && other.getModelo()==null) || 
             (this.modelo!=null &&
              this.modelo.equals(other.getModelo()))) &&
            ((this.placa==null && other.getPlaca()==null) || 
             (this.placa!=null &&
              this.placa.equals(other.getPlaca()))) &&
            ((this.vencimiento==null && other.getVencimiento()==null) || 
             (this.vencimiento!=null &&
              this.vencimiento.equals(other.getVencimiento()))) &&
            ((this.usuario==null && other.getUsuario()==null) || 
             (this.usuario!=null &&
              this.usuario.equals(other.getUsuario()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClave() != null) {
            _hashCode += getClave().hashCode();
        }
        if (getSubconcepto() != null) {
            _hashCode += getSubconcepto().hashCode();
        }
        if (getId_marca() != null) {
            _hashCode += getId_marca().hashCode();
        }
        if (getModelo() != null) {
            _hashCode += getModelo().hashCode();
        }
        if (getPlaca() != null) {
            _hashCode += getPlaca().hashCode();
        }
        if (getVencimiento() != null) {
            _hashCode += getVencimiento().hashCode();
        }
        if (getUsuario() != null) {
            _hashCode += getUsuario().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_pregunta_emi.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://10.1.65.12/fut/vehicular/tvehicular/veh_ws_secure_server.php", "tipo_pregunta_emi"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clave");
        elemField.setXmlName(new javax.xml.namespace.QName("", "clave"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subconcepto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subconcepto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_marca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id_marca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modelo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modelo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("placa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "placa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vencimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vencimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
