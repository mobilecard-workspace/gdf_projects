/**
 * Tipo_respuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.tarjetaCirculacion;

public class Tipo_respuesta  implements java.io.Serializable {
    private java.lang.String concepto;

    private java.lang.String nombreTramite;

    private java.lang.String nombreSubconcepto;

    private int clave;

    private java.lang.String placa;

    private java.lang.String id_marca;

    private java.lang.String marca;

    private java.math.BigDecimal modelo;

    private float importe;

    private java.lang.String vigencia;

    private java.lang.String lc;

    private float importe_inicial;

    private float importe_final;

    private float descuento;

    private java.math.BigDecimal error;

    private java.lang.String error_descripcion;

    public Tipo_respuesta() {
    }

    public Tipo_respuesta(
           java.lang.String concepto,
           java.lang.String nombreTramite,
           java.lang.String nombreSubconcepto,
           int clave,
           java.lang.String placa,
           java.lang.String id_marca,
           java.lang.String marca,
           java.math.BigDecimal modelo,
           float importe,
           java.lang.String vigencia,
           java.lang.String lc,
           float importe_inicial,
           float importe_final,
           float descuento,
           java.math.BigDecimal error,
           java.lang.String error_descripcion) {
           this.concepto = concepto;
           this.nombreTramite = nombreTramite;
           this.nombreSubconcepto = nombreSubconcepto;
           this.clave = clave;
           this.placa = placa;
           this.id_marca = id_marca;
           this.marca = marca;
           this.modelo = modelo;
           this.importe = importe;
           this.vigencia = vigencia;
           this.lc = lc;
           this.importe_inicial = importe_inicial;
           this.importe_final = importe_final;
           this.descuento = descuento;
           this.error = error;
           this.error_descripcion = error_descripcion;
    }


    /**
     * Gets the concepto value for this Tipo_respuesta.
     * 
     * @return concepto
     */
    public java.lang.String getConcepto() {
        return concepto;
    }


    /**
     * Sets the concepto value for this Tipo_respuesta.
     * 
     * @param concepto
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }


    /**
     * Gets the nombreTramite value for this Tipo_respuesta.
     * 
     * @return nombreTramite
     */
    public java.lang.String getNombreTramite() {
        return nombreTramite;
    }


    /**
     * Sets the nombreTramite value for this Tipo_respuesta.
     * 
     * @param nombreTramite
     */
    public void setNombreTramite(java.lang.String nombreTramite) {
        this.nombreTramite = nombreTramite;
    }


    /**
     * Gets the nombreSubconcepto value for this Tipo_respuesta.
     * 
     * @return nombreSubconcepto
     */
    public java.lang.String getNombreSubconcepto() {
        return nombreSubconcepto;
    }


    /**
     * Sets the nombreSubconcepto value for this Tipo_respuesta.
     * 
     * @param nombreSubconcepto
     */
    public void setNombreSubconcepto(java.lang.String nombreSubconcepto) {
        this.nombreSubconcepto = nombreSubconcepto;
    }


    /**
     * Gets the clave value for this Tipo_respuesta.
     * 
     * @return clave
     */
    public int getClave() {
        return clave;
    }


    /**
     * Sets the clave value for this Tipo_respuesta.
     * 
     * @param clave
     */
    public void setClave(int clave) {
        this.clave = clave;
    }


    /**
     * Gets the placa value for this Tipo_respuesta.
     * 
     * @return placa
     */
    public java.lang.String getPlaca() {
        return placa;
    }


    /**
     * Sets the placa value for this Tipo_respuesta.
     * 
     * @param placa
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }


    /**
     * Gets the id_marca value for this Tipo_respuesta.
     * 
     * @return id_marca
     */
    public java.lang.String getId_marca() {
        return id_marca;
    }


    /**
     * Sets the id_marca value for this Tipo_respuesta.
     * 
     * @param id_marca
     */
    public void setId_marca(java.lang.String id_marca) {
        this.id_marca = id_marca;
    }


    /**
     * Gets the marca value for this Tipo_respuesta.
     * 
     * @return marca
     */
    public java.lang.String getMarca() {
        return marca;
    }


    /**
     * Sets the marca value for this Tipo_respuesta.
     * 
     * @param marca
     */
    public void setMarca(java.lang.String marca) {
        this.marca = marca;
    }


    /**
     * Gets the modelo value for this Tipo_respuesta.
     * 
     * @return modelo
     */
    public java.math.BigDecimal getModelo() {
        return modelo;
    }


    /**
     * Sets the modelo value for this Tipo_respuesta.
     * 
     * @param modelo
     */
    public void setModelo(java.math.BigDecimal modelo) {
        this.modelo = modelo;
    }


    /**
     * Gets the importe value for this Tipo_respuesta.
     * 
     * @return importe
     */
    public float getImporte() {
        return importe;
    }


    /**
     * Sets the importe value for this Tipo_respuesta.
     * 
     * @param importe
     */
    public void setImporte(float importe) {
        this.importe = importe;
    }


    /**
     * Gets the vigencia value for this Tipo_respuesta.
     * 
     * @return vigencia
     */
    public java.lang.String getVigencia() {
        return vigencia;
    }


    /**
     * Sets the vigencia value for this Tipo_respuesta.
     * 
     * @param vigencia
     */
    public void setVigencia(java.lang.String vigencia) {
        this.vigencia = vigencia;
    }


    /**
     * Gets the lc value for this Tipo_respuesta.
     * 
     * @return lc
     */
    public java.lang.String getLc() {
        return lc;
    }


    /**
     * Sets the lc value for this Tipo_respuesta.
     * 
     * @param lc
     */
    public void setLc(java.lang.String lc) {
        this.lc = lc;
    }


    /**
     * Gets the importe_inicial value for this Tipo_respuesta.
     * 
     * @return importe_inicial
     */
    public float getImporte_inicial() {
        return importe_inicial;
    }


    /**
     * Sets the importe_inicial value for this Tipo_respuesta.
     * 
     * @param importe_inicial
     */
    public void setImporte_inicial(float importe_inicial) {
        this.importe_inicial = importe_inicial;
    }


    /**
     * Gets the importe_final value for this Tipo_respuesta.
     * 
     * @return importe_final
     */
    public float getImporte_final() {
        return importe_final;
    }


    /**
     * Sets the importe_final value for this Tipo_respuesta.
     * 
     * @param importe_final
     */
    public void setImporte_final(float importe_final) {
        this.importe_final = importe_final;
    }


    /**
     * Gets the descuento value for this Tipo_respuesta.
     * 
     * @return descuento
     */
    public float getDescuento() {
        return descuento;
    }


    /**
     * Sets the descuento value for this Tipo_respuesta.
     * 
     * @param descuento
     */
    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }


    /**
     * Gets the error value for this Tipo_respuesta.
     * 
     * @return error
     */
    public java.math.BigDecimal getError() {
        return error;
    }


    /**
     * Sets the error value for this Tipo_respuesta.
     * 
     * @param error
     */
    public void setError(java.math.BigDecimal error) {
        this.error = error;
    }


    /**
     * Gets the error_descripcion value for this Tipo_respuesta.
     * 
     * @return error_descripcion
     */
    public java.lang.String getError_descripcion() {
        return error_descripcion;
    }


    /**
     * Sets the error_descripcion value for this Tipo_respuesta.
     * 
     * @param error_descripcion
     */
    public void setError_descripcion(java.lang.String error_descripcion) {
        this.error_descripcion = error_descripcion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_respuesta)) return false;
        Tipo_respuesta other = (Tipo_respuesta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.concepto==null && other.getConcepto()==null) || 
             (this.concepto!=null &&
              this.concepto.equals(other.getConcepto()))) &&
            ((this.nombreTramite==null && other.getNombreTramite()==null) || 
             (this.nombreTramite!=null &&
              this.nombreTramite.equals(other.getNombreTramite()))) &&
            ((this.nombreSubconcepto==null && other.getNombreSubconcepto()==null) || 
             (this.nombreSubconcepto!=null &&
              this.nombreSubconcepto.equals(other.getNombreSubconcepto()))) &&
            this.clave == other.getClave() &&
            ((this.placa==null && other.getPlaca()==null) || 
             (this.placa!=null &&
              this.placa.equals(other.getPlaca()))) &&
            ((this.id_marca==null && other.getId_marca()==null) || 
             (this.id_marca!=null &&
              this.id_marca.equals(other.getId_marca()))) &&
            ((this.marca==null && other.getMarca()==null) || 
             (this.marca!=null &&
              this.marca.equals(other.getMarca()))) &&
            ((this.modelo==null && other.getModelo()==null) || 
             (this.modelo!=null &&
              this.modelo.equals(other.getModelo()))) &&
            this.importe == other.getImporte() &&
            ((this.vigencia==null && other.getVigencia()==null) || 
             (this.vigencia!=null &&
              this.vigencia.equals(other.getVigencia()))) &&
            ((this.lc==null && other.getLc()==null) || 
             (this.lc!=null &&
              this.lc.equals(other.getLc()))) &&
            this.importe_inicial == other.getImporte_inicial() &&
            this.importe_final == other.getImporte_final() &&
            this.descuento == other.getDescuento() &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.error_descripcion==null && other.getError_descripcion()==null) || 
             (this.error_descripcion!=null &&
              this.error_descripcion.equals(other.getError_descripcion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConcepto() != null) {
            _hashCode += getConcepto().hashCode();
        }
        if (getNombreTramite() != null) {
            _hashCode += getNombreTramite().hashCode();
        }
        if (getNombreSubconcepto() != null) {
            _hashCode += getNombreSubconcepto().hashCode();
        }
        _hashCode += getClave();
        if (getPlaca() != null) {
            _hashCode += getPlaca().hashCode();
        }
        if (getId_marca() != null) {
            _hashCode += getId_marca().hashCode();
        }
        if (getMarca() != null) {
            _hashCode += getMarca().hashCode();
        }
        if (getModelo() != null) {
            _hashCode += getModelo().hashCode();
        }
        _hashCode += new Float(getImporte()).hashCode();
        if (getVigencia() != null) {
            _hashCode += getVigencia().hashCode();
        }
        if (getLc() != null) {
            _hashCode += getLc().hashCode();
        }
        _hashCode += new Float(getImporte_inicial()).hashCode();
        _hashCode += new Float(getImporte_final()).hashCode();
        _hashCode += new Float(getDescuento()).hashCode();
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getError_descripcion() != null) {
            _hashCode += getError_descripcion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_respuesta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://10.1.65.12/fut/vehicular/tvehicular/veh_ws_secure_server.php", "tipo_respuesta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("concepto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "concepto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombreTramite");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombreTramite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombreSubconcepto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombreSubconcepto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clave");
        elemField.setXmlName(new javax.xml.namespace.QName("", "clave"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("placa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "placa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_marca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id_marca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "marca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modelo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modelo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "importe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vigencia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vigencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importe_inicial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "importe_inicial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importe_final");
        elemField.setXmlName(new javax.xml.namespace.QName("", "importe_final"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descuento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descuento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error_descripcion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error_descripcion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
