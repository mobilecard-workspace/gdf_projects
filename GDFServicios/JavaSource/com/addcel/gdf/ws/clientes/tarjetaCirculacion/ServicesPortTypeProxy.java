package com.addcel.gdf.ws.clientes.tarjetaCirculacion;

public class ServicesPortTypeProxy implements com.addcel.gdf.ws.clientes.tarjetaCirculacion.ServicesPortType {
  private String _endpoint = null;
  private com.addcel.gdf.ws.clientes.tarjetaCirculacion.ServicesPortType servicesPortType = null;
  
  public ServicesPortTypeProxy() {
    _initServicesPortTypeProxy();
  }
  
  public ServicesPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initServicesPortTypeProxy();
  }
  
  private void _initServicesPortTypeProxy() {
    try {
      servicesPortType = (new com.addcel.gdf.ws.clientes.tarjetaCirculacion.ServicesLocator()).getServicesPort();
      if (servicesPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)servicesPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)servicesPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (servicesPortType != null)
      ((javax.xml.rpc.Stub)servicesPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.gdf.ws.clientes.tarjetaCirculacion.ServicesPortType getServicesPortType() {
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType;
  }
  
  public com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_respuesta solicitar_lc(com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_pregunta pregunta) throws java.rmi.RemoteException{
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType.solicitar_lc(pregunta);
  }
  
  public com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_respuesta solicitar_lc_emi(com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_pregunta_emi pregunta_emi) throws java.rmi.RemoteException{
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType.solicitar_lc_emi(pregunta_emi);
  }
  
  public com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_catalogo_marcas[] solicitar_catalogo_marcas(com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_catalogo_marcas_pregunta pregunta) throws java.rmi.RemoteException{
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType.solicitar_catalogo_marcas(pregunta);
  }
  
  public com.addcel.gdf.ws.clientes.tarjetaCirculacion.Catalogo_subconceptos[] solicitar_subconceptos(com.addcel.gdf.ws.clientes.tarjetaCirculacion.Tipo_catalogo_subconceptos_pregunta pregunta) throws java.rmi.RemoteException{
    if (servicesPortType == null)
      _initServicesPortTypeProxy();
    return servicesPortType.solicitar_subconceptos(pregunta);
  }
  
  
}