/**
 * Tipo_catalogo_marcas.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.tarjetaCirculacion;

public class Tipo_catalogo_marcas  implements java.io.Serializable {
    private java.lang.String id_marca;

    private java.lang.String marca;

    public Tipo_catalogo_marcas() {
    }

    public Tipo_catalogo_marcas(
           java.lang.String id_marca,
           java.lang.String marca) {
           this.id_marca = id_marca;
           this.marca = marca;
    }


    /**
     * Gets the id_marca value for this Tipo_catalogo_marcas.
     * 
     * @return id_marca
     */
    public java.lang.String getId_marca() {
        return id_marca;
    }


    /**
     * Sets the id_marca value for this Tipo_catalogo_marcas.
     * 
     * @param id_marca
     */
    public void setId_marca(java.lang.String id_marca) {
        this.id_marca = id_marca;
    }


    /**
     * Gets the marca value for this Tipo_catalogo_marcas.
     * 
     * @return marca
     */
    public java.lang.String getMarca() {
        return marca;
    }


    /**
     * Sets the marca value for this Tipo_catalogo_marcas.
     * 
     * @param marca
     */
    public void setMarca(java.lang.String marca) {
        this.marca = marca;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_catalogo_marcas)) return false;
        Tipo_catalogo_marcas other = (Tipo_catalogo_marcas) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id_marca==null && other.getId_marca()==null) || 
             (this.id_marca!=null &&
              this.id_marca.equals(other.getId_marca()))) &&
            ((this.marca==null && other.getMarca()==null) || 
             (this.marca!=null &&
              this.marca.equals(other.getMarca())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId_marca() != null) {
            _hashCode += getId_marca().hashCode();
        }
        if (getMarca() != null) {
            _hashCode += getMarca().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_catalogo_marcas.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("https://10.1.65.12/fut/vehicular/tvehicular/veh_ws_secure_server.php", "tipo_catalogo_marcas"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_marca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id_marca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "marca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
