/**
 * Server.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.tenencia;

public interface Server extends javax.xml.rpc.Service {
    public java.lang.String getServerPortAddress();

    public com.addcel.gdf.ws.clientes.tenencia.ServerPortType getServerPort() throws javax.xml.rpc.ServiceException;

    public com.addcel.gdf.ws.clientes.tenencia.ServerPortType getServerPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
