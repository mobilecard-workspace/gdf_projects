/**
 * ServerBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.tenencia;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.*;
import java.util.*;
import com.addcel.gdf.ws.clientes.handler.SimpleHandler;

public class ServerBindingStub extends org.apache.axis.client.Stub implements com.addcel.gdf.ws.clientes.tenencia.ServerPortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[1];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("calculoTenencia");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "placa"), org.apache.axis.description.ParameterDesc.INOUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ejercicio"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "modelo"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ambito"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "valor_fact"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "cve_vehi"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "fech_factura"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "num_cilindros"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "procedencia"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "rfc"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "funcion_cobro"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "numeroError"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "mensaje"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "subsidio"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "depresiacion"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "tenencia"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "actualiza_ten"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "recargo_ten"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "total_tenencia"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "derecho"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "actuliza_derecho"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "recargo_derecho"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "total_derechos"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "total"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "dagid"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "lineacaptura"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "vigencia"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "lineacapturaCB"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[0] = oper;

    }

    public ServerBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public ServerBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
    	this(service);
         super.cachedEndpoint = endpointURL;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public ServerBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
        try{
            HandlerRegistry hr = service.getHandlerRegistry();

            QName  portName = new QName("http://www.finanzas.df.gob.mx/formato_lc/lc/tenenciaWSC/tenenciaWS", "ServerPort");

            List handlerChain = hr.getHandlerChain(portName);

            HandlerInfo hi = new HandlerInfo();
            hi.setHandlerClass(SimpleHandler.class);
            handlerChain.add(hi);
        	
        }catch(Exception e) {
        	e.printStackTrace();
        }
    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public void calculoTenencia(javax.xml.rpc.holders.StringHolder placa, java.lang.String ejercicio, javax.xml.rpc.holders.StringHolder modelo, javax.xml.rpc.holders.StringHolder ambito, javax.xml.rpc.holders.StringHolder valor_fact, javax.xml.rpc.holders.StringHolder cve_vehi, javax.xml.rpc.holders.StringHolder fech_factura, javax.xml.rpc.holders.StringHolder num_cilindros, javax.xml.rpc.holders.StringHolder procedencia, javax.xml.rpc.holders.StringHolder rfc, javax.xml.rpc.holders.StringHolder funcion_cobro, javax.xml.rpc.holders.StringHolder numeroError, javax.xml.rpc.holders.StringHolder mensaje, javax.xml.rpc.holders.StringHolder subsidio, javax.xml.rpc.holders.StringHolder depresiacion, javax.xml.rpc.holders.StringHolder tenencia, javax.xml.rpc.holders.StringHolder actualiza_ten, javax.xml.rpc.holders.StringHolder recargo_ten, javax.xml.rpc.holders.StringHolder total_tenencia, javax.xml.rpc.holders.StringHolder derecho, javax.xml.rpc.holders.StringHolder actuliza_derecho, javax.xml.rpc.holders.StringHolder recargo_derecho, javax.xml.rpc.holders.StringHolder total_derechos, javax.xml.rpc.holders.StringHolder total, javax.xml.rpc.holders.StringHolder dagid, javax.xml.rpc.holders.StringHolder lineacaptura, javax.xml.rpc.holders.StringHolder vigencia, javax.xml.rpc.holders.StringHolder lineacapturaCB) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:http://www.finanzas.df.gob.mx/formato_lc/lc/tenenciaWSC/tenenciaWS/calculoTenencia");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:tenenciaWSWSDL", "calculoTenencia"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {placa.value, ejercicio});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                placa.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "placa"));
            } catch (java.lang.Exception _exception) {
                placa.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "placa")), java.lang.String.class);
            }
            try {
                modelo.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "modelo"));
            } catch (java.lang.Exception _exception) {
                modelo.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "modelo")), java.lang.String.class);
            }
            try {
                ambito.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "ambito"));
            } catch (java.lang.Exception _exception) {
                ambito.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "ambito")), java.lang.String.class);
            }
            try {
                valor_fact.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "valor_fact"));
            } catch (java.lang.Exception _exception) {
                valor_fact.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "valor_fact")), java.lang.String.class);
            }
            try {
                cve_vehi.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "cve_vehi"));
            } catch (java.lang.Exception _exception) {
                cve_vehi.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "cve_vehi")), java.lang.String.class);
            }
            try {
                fech_factura.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "fech_factura"));
            } catch (java.lang.Exception _exception) {
                fech_factura.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "fech_factura")), java.lang.String.class);
            }
            try {
                num_cilindros.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "num_cilindros"));
            } catch (java.lang.Exception _exception) {
                num_cilindros.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "num_cilindros")), java.lang.String.class);
            }
            try {
                procedencia.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "procedencia"));
            } catch (java.lang.Exception _exception) {
                procedencia.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "procedencia")), java.lang.String.class);
            }
            try {
                rfc.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "rfc"));
            } catch (java.lang.Exception _exception) {
                rfc.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "rfc")), java.lang.String.class);
            }
            try {
                funcion_cobro.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "funcion_cobro"));
            } catch (java.lang.Exception _exception) {
                funcion_cobro.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "funcion_cobro")), java.lang.String.class);
            }
            try {
                numeroError.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "numeroError"));
            } catch (java.lang.Exception _exception) {
                numeroError.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "numeroError")), java.lang.String.class);
            }
            try {
                mensaje.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "mensaje"));
            } catch (java.lang.Exception _exception) {
                mensaje.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "mensaje")), java.lang.String.class);
            }
            try {
                subsidio.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "subsidio"));
            } catch (java.lang.Exception _exception) {
                subsidio.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "subsidio")), java.lang.String.class);
            }
            try {
                depresiacion.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "depresiacion"));
            } catch (java.lang.Exception _exception) {
                depresiacion.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "depresiacion")), java.lang.String.class);
            }
            try {
                tenencia.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "tenencia"));
            } catch (java.lang.Exception _exception) {
                tenencia.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "tenencia")), java.lang.String.class);
            }
            try {
                actualiza_ten.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "actualiza_ten"));
            } catch (java.lang.Exception _exception) {
                actualiza_ten.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "actualiza_ten")), java.lang.String.class);
            }
            try {
                recargo_ten.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "recargo_ten"));
            } catch (java.lang.Exception _exception) {
                recargo_ten.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "recargo_ten")), java.lang.String.class);
            }
            try {
                total_tenencia.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "total_tenencia"));
            } catch (java.lang.Exception _exception) {
                total_tenencia.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "total_tenencia")), java.lang.String.class);
            }
            try {
                derecho.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "derecho"));
            } catch (java.lang.Exception _exception) {
                derecho.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "derecho")), java.lang.String.class);
            }
            try {
                actuliza_derecho.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "actuliza_derecho"));
            } catch (java.lang.Exception _exception) {
                actuliza_derecho.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "actuliza_derecho")), java.lang.String.class);
            }
            try {
                recargo_derecho.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "recargo_derecho"));
            } catch (java.lang.Exception _exception) {
                recargo_derecho.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "recargo_derecho")), java.lang.String.class);
            }
            try {
                total_derechos.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "total_derechos"));
            } catch (java.lang.Exception _exception) {
                total_derechos.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "total_derechos")), java.lang.String.class);
            }
            try {
                total.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "total"));
            } catch (java.lang.Exception _exception) {
                total.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "total")), java.lang.String.class);
            }
            try {
                dagid.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "dagid"));
            } catch (java.lang.Exception _exception) {
                dagid.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "dagid")), java.lang.String.class);
            }
            try {
                lineacaptura.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "lineacaptura"));
            } catch (java.lang.Exception _exception) {
                lineacaptura.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "lineacaptura")), java.lang.String.class);
            }
            try {
                vigencia.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "vigencia"));
            } catch (java.lang.Exception _exception) {
                vigencia.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "vigencia")), java.lang.String.class);
            }
            try {
                lineacapturaCB.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "lineacapturaCB"));
            } catch (java.lang.Exception _exception) {
                lineacapturaCB.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "lineacapturaCB")), java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
