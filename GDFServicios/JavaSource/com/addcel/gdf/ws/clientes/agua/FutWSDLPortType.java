/**
 * FutWSDLPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

public interface FutWSDLPortType extends java.rmi.Remote {

    /**
     * Regresa los datos pertenecientes a una cuenta
     */
    public com.addcel.gdf.ws.clientes.agua.OutputFolio obtenerFolioControl(com.addcel.gdf.ws.clientes.agua.InputDatosConsulta input) throws java.rmi.RemoteException;

    /**
     * Regresa los datos de detalles de pagos
     */
    public com.addcel.gdf.ws.clientes.agua.OutputDatosFut obtenerFut(com.addcel.gdf.ws.clientes.agua.InputDatosConsulta input) throws java.rmi.RemoteException;

    /**
     * Regresa los datos de detalles de pagos
     */
    public com.addcel.gdf.ws.clientes.agua.OutputDatosFut obtenerBimestres(com.addcel.gdf.ws.clientes.agua.InputDatosConsulta input) throws java.rmi.RemoteException;

    /**
     * Obtiene los gastos de ejecución
     */
    public com.addcel.gdf.ws.clientes.agua.OutputGastosEjecucion obtenerGastosEjecucion(com.addcel.gdf.ws.clientes.agua.InputVoid input) throws java.rmi.RemoteException;

    /**
     * Inserta los datos del formato en maestro y detalle banco, y
     * devuelve los que integrarán el formato.
     */
    public com.addcel.gdf.ws.clientes.agua.OutputDatosFormato guardarFut(com.addcel.gdf.ws.clientes.agua.InputFut input) throws java.rmi.RemoteException;

    /**
     * Inserta los datos del formato en la tabla de pagos vigentes
     */
    public com.addcel.gdf.ws.clientes.agua.OutputDatosVigente guardarFutVigente(com.addcel.gdf.ws.clientes.agua.InputFutVigente input) throws java.rmi.RemoteException;
}
