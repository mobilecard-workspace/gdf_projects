/**
 * FutWSDLBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.HandlerRegistry;

import com.addcel.gdf.ws.clientes.handler.SimpleHandler;

public class FutWSDLBindingStub extends org.apache.axis.client.Stub implements com.addcel.gdf.ws.clientes.agua.FutWSDLPortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[6];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("obtenerFolioControl");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:futWSDL", "inputDatosConsulta"), com.addcel.gdf.ws.clientes.agua.InputDatosConsulta.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:futWSDL", "OutputFolio"));
        oper.setReturnClass(com.addcel.gdf.ws.clientes.agua.OutputFolio.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("obtenerFut");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:futWSDL", "inputDatosConsulta"), com.addcel.gdf.ws.clientes.agua.InputDatosConsulta.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:futWSDL", "OutputDatosFut"));
        oper.setReturnClass(com.addcel.gdf.ws.clientes.agua.OutputDatosFut.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("obtenerBimestres");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:futWSDL", "inputDatosConsulta"), com.addcel.gdf.ws.clientes.agua.InputDatosConsulta.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:futWSDL", "OutputDatosFut"));
        oper.setReturnClass(com.addcel.gdf.ws.clientes.agua.OutputDatosFut.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("obtenerGastosEjecucion");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:futWSDL", "inputVoid"), com.addcel.gdf.ws.clientes.agua.InputVoid.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:futWSDL", "OutputGastosEjecucion"));
        oper.setReturnClass(com.addcel.gdf.ws.clientes.agua.OutputGastosEjecucion.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("guardarFut");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:futWSDL", "inputFut"), com.addcel.gdf.ws.clientes.agua.InputFut.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:futWSDL", "OutputDatosFormato"));
        oper.setReturnClass(com.addcel.gdf.ws.clientes.agua.OutputDatosFormato.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("guardarFutVigente");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "input"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:futWSDL", "inputFutVigente"), com.addcel.gdf.ws.clientes.agua.InputFutVigente.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:futWSDL", "OutputDatosVigente"));
        oper.setReturnClass(com.addcel.gdf.ws.clientes.agua.OutputDatosVigente.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "return"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[5] = oper;

    }

    public FutWSDLBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public FutWSDLBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public FutWSDLBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:futWSDL", "Bimestre");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.Bimestre.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "BimestresArray");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.Bimestre[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:futWSDL", "Bimestre");
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:futWSDL", "DatosFut");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.DatosFut.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "DatosFutArray");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.DatosFut[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:futWSDL", "DatosFut");
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:futWSDL", "DatosGenerales");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.DatosGenerales.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "DatosGeneralesArray");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.DatosGenerales[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:futWSDL", "DatosGenerales");
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:futWSDL", "Gastos");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.Gastos.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "GastosEjecArray");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.Gastos[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:futWSDL", "Gastos");
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:futWSDL", "GastosEjecucion");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.GastosEjecucion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "GastosEjecucionArray");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.GastosEjecucion[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:futWSDL", "GastosEjecucion");
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:futWSDL", "inputDatosConsulta");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.InputDatosConsulta.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "inputFut");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.InputFut.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "inputFutVigente");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.InputFutVigente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "inputVoid");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.InputVoid.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "OutputDatosFormato");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.OutputDatosFormato.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "OutputDatosFut");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.OutputDatosFut.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "OutputDatosVigente");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.OutputDatosVigente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "OutputFolio");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.OutputFolio.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "OutputGastosEjecucion");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.OutputGastosEjecucion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "Totales");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.Totales.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:futWSDL", "TotalesArray");
            cachedSerQNames.add(qName);
            cls = com.addcel.gdf.ws.clientes.agua.Totales[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:futWSDL", "Totales");
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());
            try{
                HandlerRegistry hr = service.getHandlerRegistry();

                QName  portName = new QName("urn:futWSDL", "futWSDLPort");

                List handlerChain = hr.getHandlerChain(portName);

                HandlerInfo hi = new HandlerInfo();
                hi.setHandlerClass(SimpleHandler.class);
                handlerChain.add(hi);
            	
            }catch(Exception e) {
            	e.printStackTrace();
            }


    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
                    _call.setEncodingStyle(org.apache.axis.Constants.URI_SOAP11_ENC);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.addcel.gdf.ws.clientes.agua.OutputFolio obtenerFolioControl(com.addcel.gdf.ws.clientes.agua.InputDatosConsulta input) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("soapdemo_wsdl#obtenerFolioControl");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("soapdemo_wsdl", "obtenerFolioControl"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {input});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.gdf.ws.clientes.agua.OutputFolio) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.gdf.ws.clientes.agua.OutputFolio) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.gdf.ws.clientes.agua.OutputFolio.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.gdf.ws.clientes.agua.OutputDatosFut obtenerFut(com.addcel.gdf.ws.clientes.agua.InputDatosConsulta input) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("soapdemo_wsdl#obtenerFut");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("soapdemo_wsdl", "obtenerFut"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {input});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.gdf.ws.clientes.agua.OutputDatosFut) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.gdf.ws.clientes.agua.OutputDatosFut) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.gdf.ws.clientes.agua.OutputDatosFut.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.gdf.ws.clientes.agua.OutputDatosFut obtenerBimestres(com.addcel.gdf.ws.clientes.agua.InputDatosConsulta input) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("soapdemo_wsdl#obtenerBimestres");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("soapdemo_wsdl", "obtenerBimestres"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {input});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.gdf.ws.clientes.agua.OutputDatosFut) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.gdf.ws.clientes.agua.OutputDatosFut) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.gdf.ws.clientes.agua.OutputDatosFut.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.gdf.ws.clientes.agua.OutputGastosEjecucion obtenerGastosEjecucion(com.addcel.gdf.ws.clientes.agua.InputVoid input) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("soapdemo_wsdl#obtenerGastosEjecucion");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("soapdemo_wsdl", "obtenerGastosEjecucion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {input});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.gdf.ws.clientes.agua.OutputGastosEjecucion) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.gdf.ws.clientes.agua.OutputGastosEjecucion) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.gdf.ws.clientes.agua.OutputGastosEjecucion.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.gdf.ws.clientes.agua.OutputDatosFormato guardarFut(com.addcel.gdf.ws.clientes.agua.InputFut input) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("soapdemo_wsdl#guardarFut");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("soapdemo_wsdl", "guardarFut"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {input});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.gdf.ws.clientes.agua.OutputDatosFormato) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.gdf.ws.clientes.agua.OutputDatosFormato) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.gdf.ws.clientes.agua.OutputDatosFormato.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.gdf.ws.clientes.agua.OutputDatosVigente guardarFutVigente(com.addcel.gdf.ws.clientes.agua.InputFutVigente input) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("soapdemo_wsdl#guardarFut");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("soapdemo_wsdl", "guardarFutVigente"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {input});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.gdf.ws.clientes.agua.OutputDatosVigente) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.gdf.ws.clientes.agua.OutputDatosVigente) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.gdf.ws.clientes.agua.OutputDatosVigente.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
