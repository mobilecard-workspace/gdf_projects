/**
 * OutputDatosFut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

public class OutputDatosFut  implements java.io.Serializable {
    private com.addcel.gdf.ws.clientes.agua.DatosFut[] fut;

    private java.lang.String folio;

    private com.addcel.gdf.ws.clientes.agua.DatosGenerales[] datosGenerales;

    private java.lang.String error;

    private java.lang.String origen;

    private java.lang.String sql;

    public OutputDatosFut() {
    }

    public OutputDatosFut(
           com.addcel.gdf.ws.clientes.agua.DatosFut[] fut,
           java.lang.String folio,
           com.addcel.gdf.ws.clientes.agua.DatosGenerales[] datosGenerales,
           java.lang.String error,
           java.lang.String origen,
           java.lang.String sql) {
           this.fut = fut;
           this.folio = folio;
           this.datosGenerales = datosGenerales;
           this.error = error;
           this.origen = origen;
           this.sql = sql;
    }


    /**
     * Gets the fut value for this OutputDatosFut.
     * 
     * @return fut
     */
    public com.addcel.gdf.ws.clientes.agua.DatosFut[] getFut() {
        return fut;
    }


    /**
     * Sets the fut value for this OutputDatosFut.
     * 
     * @param fut
     */
    public void setFut(com.addcel.gdf.ws.clientes.agua.DatosFut[] fut) {
        this.fut = fut;
    }


    /**
     * Gets the folio value for this OutputDatosFut.
     * 
     * @return folio
     */
    public java.lang.String getFolio() {
        return folio;
    }


    /**
     * Sets the folio value for this OutputDatosFut.
     * 
     * @param folio
     */
    public void setFolio(java.lang.String folio) {
        this.folio = folio;
    }


    /**
     * Gets the datosGenerales value for this OutputDatosFut.
     * 
     * @return datosGenerales
     */
    public com.addcel.gdf.ws.clientes.agua.DatosGenerales[] getDatosGenerales() {
        return datosGenerales;
    }


    /**
     * Sets the datosGenerales value for this OutputDatosFut.
     * 
     * @param datosGenerales
     */
    public void setDatosGenerales(com.addcel.gdf.ws.clientes.agua.DatosGenerales[] datosGenerales) {
        this.datosGenerales = datosGenerales;
    }


    /**
     * Gets the error value for this OutputDatosFut.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this OutputDatosFut.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }


    /**
     * Gets the origen value for this OutputDatosFut.
     * 
     * @return origen
     */
    public java.lang.String getOrigen() {
        return origen;
    }


    /**
     * Sets the origen value for this OutputDatosFut.
     * 
     * @param origen
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }


    /**
     * Gets the sql value for this OutputDatosFut.
     * 
     * @return sql
     */
    public java.lang.String getSql() {
        return sql;
    }


    /**
     * Sets the sql value for this OutputDatosFut.
     * 
     * @param sql
     */
    public void setSql(java.lang.String sql) {
        this.sql = sql;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OutputDatosFut)) return false;
        OutputDatosFut other = (OutputDatosFut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fut==null && other.getFut()==null) || 
             (this.fut!=null &&
              java.util.Arrays.equals(this.fut, other.getFut()))) &&
            ((this.folio==null && other.getFolio()==null) || 
             (this.folio!=null &&
              this.folio.equals(other.getFolio()))) &&
            ((this.datosGenerales==null && other.getDatosGenerales()==null) || 
             (this.datosGenerales!=null &&
              java.util.Arrays.equals(this.datosGenerales, other.getDatosGenerales()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.origen==null && other.getOrigen()==null) || 
             (this.origen!=null &&
              this.origen.equals(other.getOrigen()))) &&
            ((this.sql==null && other.getSql()==null) || 
             (this.sql!=null &&
              this.sql.equals(other.getSql())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFut() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFut());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFut(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFolio() != null) {
            _hashCode += getFolio().hashCode();
        }
        if (getDatosGenerales() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDatosGenerales());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDatosGenerales(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getOrigen() != null) {
            _hashCode += getOrigen().hashCode();
        }
        if (getSql() != null) {
            _hashCode += getSql().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OutputDatosFut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "OutputDatosFut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fut");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fut"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "DatosFut"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "folio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datosGenerales");
        elemField.setXmlName(new javax.xml.namespace.QName("", "datosGenerales"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "DatosGenerales"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("origen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "origen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sql");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sql"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
