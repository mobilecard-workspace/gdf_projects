/**
 * OutputDatosVigente.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

public class OutputDatosVigente  implements java.io.Serializable {
    private java.lang.String vanio;

    private java.lang.String vbimestre;

    private java.lang.String vderdom;

    private java.lang.String vderndom;

    private java.lang.String vuso;

    private java.lang.String vderdife;

    private java.lang.String viva;

    private java.lang.String vtotal;

    private java.lang.String lc;

    private java.lang.String lcCB;

    private java.lang.String dag_id;

    private java.lang.String origen;

    private java.lang.String cuenta;

    private java.lang.String error;

    private java.lang.String vigencia;

    private java.lang.String sql;

    private java.lang.String id_pago;

    public OutputDatosVigente() {
    }

    public OutputDatosVigente(
           java.lang.String vanio,
           java.lang.String vbimestre,
           java.lang.String vderdom,
           java.lang.String vderndom,
           java.lang.String vuso,
           java.lang.String vderdife,
           java.lang.String viva,
           java.lang.String vtotal,
           java.lang.String lc,
           java.lang.String lcCB,
           java.lang.String dag_id,
           java.lang.String origen,
           java.lang.String cuenta,
           java.lang.String error,
           java.lang.String vigencia,
           java.lang.String sql,
           java.lang.String id_pago) {
           this.vanio = vanio;
           this.vbimestre = vbimestre;
           this.vderdom = vderdom;
           this.vderndom = vderndom;
           this.vuso = vuso;
           this.vderdife = vderdife;
           this.viva = viva;
           this.vtotal = vtotal;
           this.lc = lc;
           this.lcCB = lcCB;
           this.dag_id = dag_id;
           this.origen = origen;
           this.cuenta = cuenta;
           this.error = error;
           this.vigencia = vigencia;
           this.sql = sql;
           this.id_pago = id_pago;
    }


    /**
     * Gets the vanio value for this OutputDatosVigente.
     * 
     * @return vanio
     */
    public java.lang.String getVanio() {
        return vanio;
    }


    /**
     * Sets the vanio value for this OutputDatosVigente.
     * 
     * @param vanio
     */
    public void setVanio(java.lang.String vanio) {
        this.vanio = vanio;
    }


    /**
     * Gets the vbimestre value for this OutputDatosVigente.
     * 
     * @return vbimestre
     */
    public java.lang.String getVbimestre() {
        return vbimestre;
    }


    /**
     * Sets the vbimestre value for this OutputDatosVigente.
     * 
     * @param vbimestre
     */
    public void setVbimestre(java.lang.String vbimestre) {
        this.vbimestre = vbimestre;
    }


    /**
     * Gets the vderdom value for this OutputDatosVigente.
     * 
     * @return vderdom
     */
    public java.lang.String getVderdom() {
        return vderdom;
    }


    /**
     * Sets the vderdom value for this OutputDatosVigente.
     * 
     * @param vderdom
     */
    public void setVderdom(java.lang.String vderdom) {
        this.vderdom = vderdom;
    }


    /**
     * Gets the vderndom value for this OutputDatosVigente.
     * 
     * @return vderndom
     */
    public java.lang.String getVderndom() {
        return vderndom;
    }


    /**
     * Sets the vderndom value for this OutputDatosVigente.
     * 
     * @param vderndom
     */
    public void setVderndom(java.lang.String vderndom) {
        this.vderndom = vderndom;
    }


    /**
     * Gets the vuso value for this OutputDatosVigente.
     * 
     * @return vuso
     */
    public java.lang.String getVuso() {
        return vuso;
    }


    /**
     * Sets the vuso value for this OutputDatosVigente.
     * 
     * @param vuso
     */
    public void setVuso(java.lang.String vuso) {
        this.vuso = vuso;
    }


    /**
     * Gets the vderdife value for this OutputDatosVigente.
     * 
     * @return vderdife
     */
    public java.lang.String getVderdife() {
        return vderdife;
    }


    /**
     * Sets the vderdife value for this OutputDatosVigente.
     * 
     * @param vderdife
     */
    public void setVderdife(java.lang.String vderdife) {
        this.vderdife = vderdife;
    }


    /**
     * Gets the viva value for this OutputDatosVigente.
     * 
     * @return viva
     */
    public java.lang.String getViva() {
        return viva;
    }


    /**
     * Sets the viva value for this OutputDatosVigente.
     * 
     * @param viva
     */
    public void setViva(java.lang.String viva) {
        this.viva = viva;
    }


    /**
     * Gets the vtotal value for this OutputDatosVigente.
     * 
     * @return vtotal
     */
    public java.lang.String getVtotal() {
        return vtotal;
    }


    /**
     * Sets the vtotal value for this OutputDatosVigente.
     * 
     * @param vtotal
     */
    public void setVtotal(java.lang.String vtotal) {
        this.vtotal = vtotal;
    }


    /**
     * Gets the lc value for this OutputDatosVigente.
     * 
     * @return lc
     */
    public java.lang.String getLc() {
        return lc;
    }


    /**
     * Sets the lc value for this OutputDatosVigente.
     * 
     * @param lc
     */
    public void setLc(java.lang.String lc) {
        this.lc = lc;
    }


    /**
     * Gets the lcCB value for this OutputDatosVigente.
     * 
     * @return lcCB
     */
    public java.lang.String getLcCB() {
        return lcCB;
    }


    /**
     * Sets the lcCB value for this OutputDatosVigente.
     * 
     * @param lcCB
     */
    public void setLcCB(java.lang.String lcCB) {
        this.lcCB = lcCB;
    }


    /**
     * Gets the dag_id value for this OutputDatosVigente.
     * 
     * @return dag_id
     */
    public java.lang.String getDag_id() {
        return dag_id;
    }


    /**
     * Sets the dag_id value for this OutputDatosVigente.
     * 
     * @param dag_id
     */
    public void setDag_id(java.lang.String dag_id) {
        this.dag_id = dag_id;
    }


    /**
     * Gets the origen value for this OutputDatosVigente.
     * 
     * @return origen
     */
    public java.lang.String getOrigen() {
        return origen;
    }


    /**
     * Sets the origen value for this OutputDatosVigente.
     * 
     * @param origen
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }


    /**
     * Gets the cuenta value for this OutputDatosVigente.
     * 
     * @return cuenta
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }


    /**
     * Sets the cuenta value for this OutputDatosVigente.
     * 
     * @param cuenta
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }


    /**
     * Gets the error value for this OutputDatosVigente.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this OutputDatosVigente.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }


    /**
     * Gets the vigencia value for this OutputDatosVigente.
     * 
     * @return vigencia
     */
    public java.lang.String getVigencia() {
        return vigencia;
    }


    /**
     * Sets the vigencia value for this OutputDatosVigente.
     * 
     * @param vigencia
     */
    public void setVigencia(java.lang.String vigencia) {
        this.vigencia = vigencia;
    }


    /**
     * Gets the sql value for this OutputDatosVigente.
     * 
     * @return sql
     */
    public java.lang.String getSql() {
        return sql;
    }


    /**
     * Sets the sql value for this OutputDatosVigente.
     * 
     * @param sql
     */
    public void setSql(java.lang.String sql) {
        this.sql = sql;
    }


    /**
     * Gets the id_pago value for this OutputDatosVigente.
     * 
     * @return id_pago
     */
    public java.lang.String getId_pago() {
        return id_pago;
    }


    /**
     * Sets the id_pago value for this OutputDatosVigente.
     * 
     * @param id_pago
     */
    public void setId_pago(java.lang.String id_pago) {
        this.id_pago = id_pago;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OutputDatosVigente)) return false;
        OutputDatosVigente other = (OutputDatosVigente) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.vanio==null && other.getVanio()==null) || 
             (this.vanio!=null &&
              this.vanio.equals(other.getVanio()))) &&
            ((this.vbimestre==null && other.getVbimestre()==null) || 
             (this.vbimestre!=null &&
              this.vbimestre.equals(other.getVbimestre()))) &&
            ((this.vderdom==null && other.getVderdom()==null) || 
             (this.vderdom!=null &&
              this.vderdom.equals(other.getVderdom()))) &&
            ((this.vderndom==null && other.getVderndom()==null) || 
             (this.vderndom!=null &&
              this.vderndom.equals(other.getVderndom()))) &&
            ((this.vuso==null && other.getVuso()==null) || 
             (this.vuso!=null &&
              this.vuso.equals(other.getVuso()))) &&
            ((this.vderdife==null && other.getVderdife()==null) || 
             (this.vderdife!=null &&
              this.vderdife.equals(other.getVderdife()))) &&
            ((this.viva==null && other.getViva()==null) || 
             (this.viva!=null &&
              this.viva.equals(other.getViva()))) &&
            ((this.vtotal==null && other.getVtotal()==null) || 
             (this.vtotal!=null &&
              this.vtotal.equals(other.getVtotal()))) &&
            ((this.lc==null && other.getLc()==null) || 
             (this.lc!=null &&
              this.lc.equals(other.getLc()))) &&
            ((this.lcCB==null && other.getLcCB()==null) || 
             (this.lcCB!=null &&
              this.lcCB.equals(other.getLcCB()))) &&
            ((this.dag_id==null && other.getDag_id()==null) || 
             (this.dag_id!=null &&
              this.dag_id.equals(other.getDag_id()))) &&
            ((this.origen==null && other.getOrigen()==null) || 
             (this.origen!=null &&
              this.origen.equals(other.getOrigen()))) &&
            ((this.cuenta==null && other.getCuenta()==null) || 
             (this.cuenta!=null &&
              this.cuenta.equals(other.getCuenta()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.vigencia==null && other.getVigencia()==null) || 
             (this.vigencia!=null &&
              this.vigencia.equals(other.getVigencia()))) &&
            ((this.sql==null && other.getSql()==null) || 
             (this.sql!=null &&
              this.sql.equals(other.getSql()))) &&
            ((this.id_pago==null && other.getId_pago()==null) || 
             (this.id_pago!=null &&
              this.id_pago.equals(other.getId_pago())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVanio() != null) {
            _hashCode += getVanio().hashCode();
        }
        if (getVbimestre() != null) {
            _hashCode += getVbimestre().hashCode();
        }
        if (getVderdom() != null) {
            _hashCode += getVderdom().hashCode();
        }
        if (getVderndom() != null) {
            _hashCode += getVderndom().hashCode();
        }
        if (getVuso() != null) {
            _hashCode += getVuso().hashCode();
        }
        if (getVderdife() != null) {
            _hashCode += getVderdife().hashCode();
        }
        if (getViva() != null) {
            _hashCode += getViva().hashCode();
        }
        if (getVtotal() != null) {
            _hashCode += getVtotal().hashCode();
        }
        if (getLc() != null) {
            _hashCode += getLc().hashCode();
        }
        if (getLcCB() != null) {
            _hashCode += getLcCB().hashCode();
        }
        if (getDag_id() != null) {
            _hashCode += getDag_id().hashCode();
        }
        if (getOrigen() != null) {
            _hashCode += getOrigen().hashCode();
        }
        if (getCuenta() != null) {
            _hashCode += getCuenta().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getVigencia() != null) {
            _hashCode += getVigencia().hashCode();
        }
        if (getSql() != null) {
            _hashCode += getSql().hashCode();
        }
        if (getId_pago() != null) {
            _hashCode += getId_pago().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OutputDatosVigente.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "OutputDatosVigente"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vanio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vanio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vbimestre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vbimestre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vderdom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vderdom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vderndom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vderndom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vuso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vuso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vderdife");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vderdife"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viva");
        elemField.setXmlName(new javax.xml.namespace.QName("", "viva"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vtotal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vtotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lcCB");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lcCB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dag_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dag_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("origen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "origen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vigencia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vigencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sql");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sql"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_pago");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id_pago"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
