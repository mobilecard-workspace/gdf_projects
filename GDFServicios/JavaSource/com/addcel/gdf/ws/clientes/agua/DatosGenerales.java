/**
 * DatosGenerales.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

public class DatosGenerales  implements java.io.Serializable {
    private java.lang.String nombre;

    private java.lang.String calle;

    private java.lang.String num_ext;

    private java.lang.String num_int;

    private java.lang.String colonia;

    private java.lang.String cod_pos;

    private java.lang.String medidor;

    private java.lang.String diametro;

    private java.lang.String delegacion;

    private java.lang.String empresa;

    private java.lang.String uso;

    private java.lang.String folio;

    private java.lang.String status;

    private java.lang.String banderaCondonacion;

    public DatosGenerales() {
    }

    public DatosGenerales(
           java.lang.String nombre,
           java.lang.String calle,
           java.lang.String num_ext,
           java.lang.String num_int,
           java.lang.String colonia,
           java.lang.String cod_pos,
           java.lang.String medidor,
           java.lang.String diametro,
           java.lang.String delegacion,
           java.lang.String empresa,
           java.lang.String uso,
           java.lang.String folio,
           java.lang.String status,
           java.lang.String banderaCondonacion) {
           this.nombre = nombre;
           this.calle = calle;
           this.num_ext = num_ext;
           this.num_int = num_int;
           this.colonia = colonia;
           this.cod_pos = cod_pos;
           this.medidor = medidor;
           this.diametro = diametro;
           this.delegacion = delegacion;
           this.empresa = empresa;
           this.uso = uso;
           this.folio = folio;
           this.status = status;
           this.banderaCondonacion = banderaCondonacion;
    }


    /**
     * Gets the nombre value for this DatosGenerales.
     * 
     * @return nombre
     */
    public java.lang.String getNombre() {
        return nombre;
    }


    /**
     * Sets the nombre value for this DatosGenerales.
     * 
     * @param nombre
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }


    /**
     * Gets the calle value for this DatosGenerales.
     * 
     * @return calle
     */
    public java.lang.String getCalle() {
        return calle;
    }


    /**
     * Sets the calle value for this DatosGenerales.
     * 
     * @param calle
     */
    public void setCalle(java.lang.String calle) {
        this.calle = calle;
    }


    /**
     * Gets the num_ext value for this DatosGenerales.
     * 
     * @return num_ext
     */
    public java.lang.String getNum_ext() {
        return num_ext;
    }


    /**
     * Sets the num_ext value for this DatosGenerales.
     * 
     * @param num_ext
     */
    public void setNum_ext(java.lang.String num_ext) {
        this.num_ext = num_ext;
    }


    /**
     * Gets the num_int value for this DatosGenerales.
     * 
     * @return num_int
     */
    public java.lang.String getNum_int() {
        return num_int;
    }


    /**
     * Sets the num_int value for this DatosGenerales.
     * 
     * @param num_int
     */
    public void setNum_int(java.lang.String num_int) {
        this.num_int = num_int;
    }


    /**
     * Gets the colonia value for this DatosGenerales.
     * 
     * @return colonia
     */
    public java.lang.String getColonia() {
        return colonia;
    }


    /**
     * Sets the colonia value for this DatosGenerales.
     * 
     * @param colonia
     */
    public void setColonia(java.lang.String colonia) {
        this.colonia = colonia;
    }


    /**
     * Gets the cod_pos value for this DatosGenerales.
     * 
     * @return cod_pos
     */
    public java.lang.String getCod_pos() {
        return cod_pos;
    }


    /**
     * Sets the cod_pos value for this DatosGenerales.
     * 
     * @param cod_pos
     */
    public void setCod_pos(java.lang.String cod_pos) {
        this.cod_pos = cod_pos;
    }


    /**
     * Gets the medidor value for this DatosGenerales.
     * 
     * @return medidor
     */
    public java.lang.String getMedidor() {
        return medidor;
    }


    /**
     * Sets the medidor value for this DatosGenerales.
     * 
     * @param medidor
     */
    public void setMedidor(java.lang.String medidor) {
        this.medidor = medidor;
    }


    /**
     * Gets the diametro value for this DatosGenerales.
     * 
     * @return diametro
     */
    public java.lang.String getDiametro() {
        return diametro;
    }


    /**
     * Sets the diametro value for this DatosGenerales.
     * 
     * @param diametro
     */
    public void setDiametro(java.lang.String diametro) {
        this.diametro = diametro;
    }


    /**
     * Gets the delegacion value for this DatosGenerales.
     * 
     * @return delegacion
     */
    public java.lang.String getDelegacion() {
        return delegacion;
    }


    /**
     * Sets the delegacion value for this DatosGenerales.
     * 
     * @param delegacion
     */
    public void setDelegacion(java.lang.String delegacion) {
        this.delegacion = delegacion;
    }


    /**
     * Gets the empresa value for this DatosGenerales.
     * 
     * @return empresa
     */
    public java.lang.String getEmpresa() {
        return empresa;
    }


    /**
     * Sets the empresa value for this DatosGenerales.
     * 
     * @param empresa
     */
    public void setEmpresa(java.lang.String empresa) {
        this.empresa = empresa;
    }


    /**
     * Gets the uso value for this DatosGenerales.
     * 
     * @return uso
     */
    public java.lang.String getUso() {
        return uso;
    }


    /**
     * Sets the uso value for this DatosGenerales.
     * 
     * @param uso
     */
    public void setUso(java.lang.String uso) {
        this.uso = uso;
    }


    /**
     * Gets the folio value for this DatosGenerales.
     * 
     * @return folio
     */
    public java.lang.String getFolio() {
        return folio;
    }


    /**
     * Sets the folio value for this DatosGenerales.
     * 
     * @param folio
     */
    public void setFolio(java.lang.String folio) {
        this.folio = folio;
    }


    /**
     * Gets the status value for this DatosGenerales.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this DatosGenerales.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the banderaCondonacion value for this DatosGenerales.
     * 
     * @return banderaCondonacion
     */
    public java.lang.String getBanderaCondonacion() {
        return banderaCondonacion;
    }


    /**
     * Sets the banderaCondonacion value for this DatosGenerales.
     * 
     * @param banderaCondonacion
     */
    public void setBanderaCondonacion(java.lang.String banderaCondonacion) {
        this.banderaCondonacion = banderaCondonacion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DatosGenerales)) return false;
        DatosGenerales other = (DatosGenerales) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nombre==null && other.getNombre()==null) || 
             (this.nombre!=null &&
              this.nombre.equals(other.getNombre()))) &&
            ((this.calle==null && other.getCalle()==null) || 
             (this.calle!=null &&
              this.calle.equals(other.getCalle()))) &&
            ((this.num_ext==null && other.getNum_ext()==null) || 
             (this.num_ext!=null &&
              this.num_ext.equals(other.getNum_ext()))) &&
            ((this.num_int==null && other.getNum_int()==null) || 
             (this.num_int!=null &&
              this.num_int.equals(other.getNum_int()))) &&
            ((this.colonia==null && other.getColonia()==null) || 
             (this.colonia!=null &&
              this.colonia.equals(other.getColonia()))) &&
            ((this.cod_pos==null && other.getCod_pos()==null) || 
             (this.cod_pos!=null &&
              this.cod_pos.equals(other.getCod_pos()))) &&
            ((this.medidor==null && other.getMedidor()==null) || 
             (this.medidor!=null &&
              this.medidor.equals(other.getMedidor()))) &&
            ((this.diametro==null && other.getDiametro()==null) || 
             (this.diametro!=null &&
              this.diametro.equals(other.getDiametro()))) &&
            ((this.delegacion==null && other.getDelegacion()==null) || 
             (this.delegacion!=null &&
              this.delegacion.equals(other.getDelegacion()))) &&
            ((this.empresa==null && other.getEmpresa()==null) || 
             (this.empresa!=null &&
              this.empresa.equals(other.getEmpresa()))) &&
            ((this.uso==null && other.getUso()==null) || 
             (this.uso!=null &&
              this.uso.equals(other.getUso()))) &&
            ((this.folio==null && other.getFolio()==null) || 
             (this.folio!=null &&
              this.folio.equals(other.getFolio()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.banderaCondonacion==null && other.getBanderaCondonacion()==null) || 
             (this.banderaCondonacion!=null &&
              this.banderaCondonacion.equals(other.getBanderaCondonacion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNombre() != null) {
            _hashCode += getNombre().hashCode();
        }
        if (getCalle() != null) {
            _hashCode += getCalle().hashCode();
        }
        if (getNum_ext() != null) {
            _hashCode += getNum_ext().hashCode();
        }
        if (getNum_int() != null) {
            _hashCode += getNum_int().hashCode();
        }
        if (getColonia() != null) {
            _hashCode += getColonia().hashCode();
        }
        if (getCod_pos() != null) {
            _hashCode += getCod_pos().hashCode();
        }
        if (getMedidor() != null) {
            _hashCode += getMedidor().hashCode();
        }
        if (getDiametro() != null) {
            _hashCode += getDiametro().hashCode();
        }
        if (getDelegacion() != null) {
            _hashCode += getDelegacion().hashCode();
        }
        if (getEmpresa() != null) {
            _hashCode += getEmpresa().hashCode();
        }
        if (getUso() != null) {
            _hashCode += getUso().hashCode();
        }
        if (getFolio() != null) {
            _hashCode += getFolio().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getBanderaCondonacion() != null) {
            _hashCode += getBanderaCondonacion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DatosGenerales.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "DatosGenerales"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("num_ext");
        elemField.setXmlName(new javax.xml.namespace.QName("", "num_ext"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("num_int");
        elemField.setXmlName(new javax.xml.namespace.QName("", "num_int"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("colonia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "colonia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cod_pos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cod_pos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medidor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "medidor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("diametro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "diametro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("delegacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "delegacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empresa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "empresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "folio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("banderaCondonacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "banderaCondonacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
