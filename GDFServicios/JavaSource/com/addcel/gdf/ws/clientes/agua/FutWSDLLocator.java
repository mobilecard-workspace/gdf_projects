/**
 * FutWSDLLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

public class FutWSDLLocator extends org.apache.axis.client.Service implements com.addcel.gdf.ws.clientes.agua.FutWSDL {

    public FutWSDLLocator() {
    }


    public FutWSDLLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FutWSDLLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for futWSDLPort
    private java.lang.String futWSDLPort_address = "http://10.11.10.28/ws/fut_ws.php";

    public java.lang.String getfutWSDLPortAddress() {
        return futWSDLPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String futWSDLPortWSDDServiceName = "futWSDLPort";

    public java.lang.String getfutWSDLPortWSDDServiceName() {
        return futWSDLPortWSDDServiceName;
    }

    public void setfutWSDLPortWSDDServiceName(java.lang.String name) {
        futWSDLPortWSDDServiceName = name;
    }

    public com.addcel.gdf.ws.clientes.agua.FutWSDLPortType getfutWSDLPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(futWSDLPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getfutWSDLPort(endpoint);
    }

    public com.addcel.gdf.ws.clientes.agua.FutWSDLPortType getfutWSDLPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.gdf.ws.clientes.agua.FutWSDLBindingStub _stub = new com.addcel.gdf.ws.clientes.agua.FutWSDLBindingStub(portAddress, this);
            _stub.setPortName(getfutWSDLPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setfutWSDLPortEndpointAddress(java.lang.String address) {
        futWSDLPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.gdf.ws.clientes.agua.FutWSDLPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.gdf.ws.clientes.agua.FutWSDLBindingStub _stub = new com.addcel.gdf.ws.clientes.agua.FutWSDLBindingStub(new java.net.URL(futWSDLPort_address), this);
                _stub.setPortName(getfutWSDLPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("futWSDLPort".equals(inputPortName)) {
            return getfutWSDLPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:futWSDL", "futWSDL");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:futWSDL", "futWSDLPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("futWSDLPort".equals(portName)) {
            setfutWSDLPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
