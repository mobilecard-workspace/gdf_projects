/**
 * InputFut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.agua;

public class InputFut  implements java.io.Serializable {
    private int folio_control;

    private java.lang.String cuenta;

    private com.addcel.gdf.ws.clientes.agua.Bimestre[] bimestres;

    private java.lang.String folio;

    private java.lang.String programa;

    private java.lang.String uso;

    private java.lang.String empresa;

    private java.lang.String ip;

    private java.lang.String id_aplicacion;

    private java.lang.String passwd;

    public InputFut() {
    }

    public InputFut(
           int folio_control,
           java.lang.String cuenta,
           com.addcel.gdf.ws.clientes.agua.Bimestre[] bimestres,
           java.lang.String folio,
           java.lang.String programa,
           java.lang.String uso,
           java.lang.String empresa,
           java.lang.String ip,
           java.lang.String id_aplicacion,
           java.lang.String passwd) {
           this.folio_control = folio_control;
           this.cuenta = cuenta;
           this.bimestres = bimestres;
           this.folio = folio;
           this.programa = programa;
           this.uso = uso;
           this.empresa = empresa;
           this.ip = ip;
           this.id_aplicacion = id_aplicacion;
           this.passwd = passwd;
    }


    /**
     * Gets the folio_control value for this InputFut.
     * 
     * @return folio_control
     */
    public int getFolio_control() {
        return folio_control;
    }


    /**
     * Sets the folio_control value for this InputFut.
     * 
     * @param folio_control
     */
    public void setFolio_control(int folio_control) {
        this.folio_control = folio_control;
    }


    /**
     * Gets the cuenta value for this InputFut.
     * 
     * @return cuenta
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }


    /**
     * Sets the cuenta value for this InputFut.
     * 
     * @param cuenta
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }


    /**
     * Gets the bimestres value for this InputFut.
     * 
     * @return bimestres
     */
    public com.addcel.gdf.ws.clientes.agua.Bimestre[] getBimestres() {
        return bimestres;
    }


    /**
     * Sets the bimestres value for this InputFut.
     * 
     * @param bimestres
     */
    public void setBimestres(com.addcel.gdf.ws.clientes.agua.Bimestre[] bimestres) {
        this.bimestres = bimestres;
    }


    /**
     * Gets the folio value for this InputFut.
     * 
     * @return folio
     */
    public java.lang.String getFolio() {
        return folio;
    }


    /**
     * Sets the folio value for this InputFut.
     * 
     * @param folio
     */
    public void setFolio(java.lang.String folio) {
        this.folio = folio;
    }


    /**
     * Gets the programa value for this InputFut.
     * 
     * @return programa
     */
    public java.lang.String getPrograma() {
        return programa;
    }


    /**
     * Sets the programa value for this InputFut.
     * 
     * @param programa
     */
    public void setPrograma(java.lang.String programa) {
        this.programa = programa;
    }


    /**
     * Gets the uso value for this InputFut.
     * 
     * @return uso
     */
    public java.lang.String getUso() {
        return uso;
    }


    /**
     * Sets the uso value for this InputFut.
     * 
     * @param uso
     */
    public void setUso(java.lang.String uso) {
        this.uso = uso;
    }


    /**
     * Gets the empresa value for this InputFut.
     * 
     * @return empresa
     */
    public java.lang.String getEmpresa() {
        return empresa;
    }


    /**
     * Sets the empresa value for this InputFut.
     * 
     * @param empresa
     */
    public void setEmpresa(java.lang.String empresa) {
        this.empresa = empresa;
    }


    /**
     * Gets the ip value for this InputFut.
     * 
     * @return ip
     */
    public java.lang.String getIp() {
        return ip;
    }


    /**
     * Sets the ip value for this InputFut.
     * 
     * @param ip
     */
    public void setIp(java.lang.String ip) {
        this.ip = ip;
    }


    /**
     * Gets the id_aplicacion value for this InputFut.
     * 
     * @return id_aplicacion
     */
    public java.lang.String getId_aplicacion() {
        return id_aplicacion;
    }


    /**
     * Sets the id_aplicacion value for this InputFut.
     * 
     * @param id_aplicacion
     */
    public void setId_aplicacion(java.lang.String id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }


    /**
     * Gets the passwd value for this InputFut.
     * 
     * @return passwd
     */
    public java.lang.String getPasswd() {
        return passwd;
    }


    /**
     * Sets the passwd value for this InputFut.
     * 
     * @param passwd
     */
    public void setPasswd(java.lang.String passwd) {
        this.passwd = passwd;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InputFut)) return false;
        InputFut other = (InputFut) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.folio_control == other.getFolio_control() &&
            ((this.cuenta==null && other.getCuenta()==null) || 
             (this.cuenta!=null &&
              this.cuenta.equals(other.getCuenta()))) &&
            ((this.bimestres==null && other.getBimestres()==null) || 
             (this.bimestres!=null &&
              java.util.Arrays.equals(this.bimestres, other.getBimestres()))) &&
            ((this.folio==null && other.getFolio()==null) || 
             (this.folio!=null &&
              this.folio.equals(other.getFolio()))) &&
            ((this.programa==null && other.getPrograma()==null) || 
             (this.programa!=null &&
              this.programa.equals(other.getPrograma()))) &&
            ((this.uso==null && other.getUso()==null) || 
             (this.uso!=null &&
              this.uso.equals(other.getUso()))) &&
            ((this.empresa==null && other.getEmpresa()==null) || 
             (this.empresa!=null &&
              this.empresa.equals(other.getEmpresa()))) &&
            ((this.ip==null && other.getIp()==null) || 
             (this.ip!=null &&
              this.ip.equals(other.getIp()))) &&
            ((this.id_aplicacion==null && other.getId_aplicacion()==null) || 
             (this.id_aplicacion!=null &&
              this.id_aplicacion.equals(other.getId_aplicacion()))) &&
            ((this.passwd==null && other.getPasswd()==null) || 
             (this.passwd!=null &&
              this.passwd.equals(other.getPasswd())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getFolio_control();
        if (getCuenta() != null) {
            _hashCode += getCuenta().hashCode();
        }
        if (getBimestres() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBimestres());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBimestres(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFolio() != null) {
            _hashCode += getFolio().hashCode();
        }
        if (getPrograma() != null) {
            _hashCode += getPrograma().hashCode();
        }
        if (getUso() != null) {
            _hashCode += getUso().hashCode();
        }
        if (getEmpresa() != null) {
            _hashCode += getEmpresa().hashCode();
        }
        if (getIp() != null) {
            _hashCode += getIp().hashCode();
        }
        if (getId_aplicacion() != null) {
            _hashCode += getId_aplicacion().hashCode();
        }
        if (getPasswd() != null) {
            _hashCode += getPasswd().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InputFut.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "inputFut"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folio_control");
        elemField.setXmlName(new javax.xml.namespace.QName("", "folio_control"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bimestres");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bimestres"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:futWSDL", "Bimestre"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "folio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("programa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "programa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uso");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("empresa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "empresa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ip");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_aplicacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id_aplicacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passwd");
        elemField.setXmlName(new javax.xml.namespace.QName("", "passwd"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
