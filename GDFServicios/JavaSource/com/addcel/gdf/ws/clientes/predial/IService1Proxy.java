package com.addcel.gdf.ws.clientes.predial;

public class IService1Proxy implements com.addcel.gdf.ws.clientes.predial.IService1 {
  private String _endpoint = null;
  private com.addcel.gdf.ws.clientes.predial.IService1 iService1 = null;
  
  public IService1Proxy() {
    _initIService1Proxy();
  }
  
  public IService1Proxy(String endpoint) {
    _endpoint = endpoint;
    _initIService1Proxy();
  }
  
  private void _initIService1Proxy() {
    try {
      iService1 = (new com.addcel.gdf.ws.clientes.predial.Service1Locator()).getBasicHttpBinding_IService1();
      if (iService1 != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iService1)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iService1)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iService1 != null)
      ((javax.xml.rpc.Stub)iService1)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.gdf.ws.clientes.predial.IService1 getIService1() {
    if (iService1 == null)
      _initIService1Proxy();
    return iService1;
  }
  
  public com.addcel.gdf.ws.clientes.predial.vo.Tipo_respuesta_adeudo[] solicitar_adeudo_vigente(com.addcel.gdf.ws.clientes.predial.vo.Tipo_pregunta_adeudo pregunta) throws java.rmi.RemoteException{
    if (iService1 == null)
      _initIService1Proxy();
    return iService1.solicitar_adeudo_vigente(pregunta);
  }
  
  public com.addcel.gdf.ws.clientes.predial.vo.Tipo_respuesta_adeudo_vencido[] solicita_adeudo(com.addcel.gdf.ws.clientes.predial.vo.Tipo_pregunta_adeudo pregunta) throws java.rmi.RemoteException{
    if (iService1 == null)
      _initIService1Proxy();
    return iService1.solicita_adeudo(pregunta);
  }
  
  public com.addcel.gdf.ws.clientes.predial.vo.Envio_adeudo_vencido_periodo[] solicita_seleccionados2(com.addcel.gdf.ws.clientes.predial.vo.Recibo_adeudo_vencido_periodo pregunta) throws java.rmi.RemoteException{
    if (iService1 == null)
      _initIService1Proxy();
    return iService1.solicita_seleccionados2(pregunta);
  }
  
  public com.addcel.gdf.ws.clientes.predial.vo.Envio_adeudo_vencido_periodo[] solicita_seleccionados(com.addcel.gdf.ws.clientes.predial.vo.Recibo_adeudo_vencido_periodo pregunta) throws java.rmi.RemoteException{
    if (iService1 == null)
      _initIService1Proxy();
    return iService1.solicita_seleccionados(pregunta);
  }
  
  public com.addcel.gdf.ws.clientes.predial.vo.Tipo_respuesta_adeudo[] solicitar_adeudo_anticipado(com.addcel.gdf.ws.clientes.predial.vo.Tipo_pregunta_adeudo pregunta) throws java.rmi.RemoteException{
    if (iService1 == null)
      _initIService1Proxy();
    return iService1.solicitar_adeudo_anticipado(pregunta);
  }
  
  
}