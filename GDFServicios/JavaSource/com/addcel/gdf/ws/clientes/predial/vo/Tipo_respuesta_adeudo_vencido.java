/**
 * Tipo_respuesta_adeudo_vencido.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.predial.vo;

public class Tipo_respuesta_adeudo_vencido  implements java.io.Serializable {
    private java.lang.String cadbc;

    private java.lang.String cuenta;

    private java.lang.String descripcion_error;

    private com.addcel.gdf.ws.clientes.predial.vo.Detalle_periodos_vencidos[] detalle;

    private java.lang.String error;

    private java.math.BigDecimal gastos_ejecucion;

    private java.math.BigDecimal multa_incumplimiento;

    public Tipo_respuesta_adeudo_vencido() {
    }

    public Tipo_respuesta_adeudo_vencido(
           java.lang.String cadbc,
           java.lang.String cuenta,
           java.lang.String descripcion_error,
           com.addcel.gdf.ws.clientes.predial.vo.Detalle_periodos_vencidos[] detalle,
           java.lang.String error,
           java.math.BigDecimal gastos_ejecucion,
           java.math.BigDecimal multa_incumplimiento) {
           this.cadbc = cadbc;
           this.cuenta = cuenta;
           this.descripcion_error = descripcion_error;
           this.detalle = detalle;
           this.error = error;
           this.gastos_ejecucion = gastos_ejecucion;
           this.multa_incumplimiento = multa_incumplimiento;
    }


    /**
     * Gets the cadbc value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @return cadbc
     */
    public java.lang.String getCadbc() {
        return cadbc;
    }


    /**
     * Sets the cadbc value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @param cadbc
     */
    public void setCadbc(java.lang.String cadbc) {
        this.cadbc = cadbc;
    }


    /**
     * Gets the cuenta value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @return cuenta
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }


    /**
     * Sets the cuenta value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @param cuenta
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }


    /**
     * Gets the descripcion_error value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @return descripcion_error
     */
    public java.lang.String getDescripcion_error() {
        return descripcion_error;
    }


    /**
     * Sets the descripcion_error value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @param descripcion_error
     */
    public void setDescripcion_error(java.lang.String descripcion_error) {
        this.descripcion_error = descripcion_error;
    }


    /**
     * Gets the detalle value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @return detalle
     */
    public com.addcel.gdf.ws.clientes.predial.vo.Detalle_periodos_vencidos[] getDetalle() {
        return detalle;
    }


    /**
     * Sets the detalle value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @param detalle
     */
    public void setDetalle(com.addcel.gdf.ws.clientes.predial.vo.Detalle_periodos_vencidos[] detalle) {
        this.detalle = detalle;
    }


    /**
     * Gets the error value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }


    /**
     * Gets the gastos_ejecucion value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @return gastos_ejecucion
     */
    public java.math.BigDecimal getGastos_ejecucion() {
        return gastos_ejecucion;
    }


    /**
     * Sets the gastos_ejecucion value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @param gastos_ejecucion
     */
    public void setGastos_ejecucion(java.math.BigDecimal gastos_ejecucion) {
        this.gastos_ejecucion = gastos_ejecucion;
    }


    /**
     * Gets the multa_incumplimiento value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @return multa_incumplimiento
     */
    public java.math.BigDecimal getMulta_incumplimiento() {
        return multa_incumplimiento;
    }


    /**
     * Sets the multa_incumplimiento value for this Tipo_respuesta_adeudo_vencido.
     * 
     * @param multa_incumplimiento
     */
    public void setMulta_incumplimiento(java.math.BigDecimal multa_incumplimiento) {
        this.multa_incumplimiento = multa_incumplimiento;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Tipo_respuesta_adeudo_vencido)) return false;
        Tipo_respuesta_adeudo_vencido other = (Tipo_respuesta_adeudo_vencido) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cadbc==null && other.getCadbc()==null) || 
             (this.cadbc!=null &&
              this.cadbc.equals(other.getCadbc()))) &&
            ((this.cuenta==null && other.getCuenta()==null) || 
             (this.cuenta!=null &&
              this.cuenta.equals(other.getCuenta()))) &&
            ((this.descripcion_error==null && other.getDescripcion_error()==null) || 
             (this.descripcion_error!=null &&
              this.descripcion_error.equals(other.getDescripcion_error()))) &&
            ((this.detalle==null && other.getDetalle()==null) || 
             (this.detalle!=null &&
              java.util.Arrays.equals(this.detalle, other.getDetalle()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.gastos_ejecucion==null && other.getGastos_ejecucion()==null) || 
             (this.gastos_ejecucion!=null &&
              this.gastos_ejecucion.equals(other.getGastos_ejecucion()))) &&
            ((this.multa_incumplimiento==null && other.getMulta_incumplimiento()==null) || 
             (this.multa_incumplimiento!=null &&
              this.multa_incumplimiento.equals(other.getMulta_incumplimiento())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCadbc() != null) {
            _hashCode += getCadbc().hashCode();
        }
        if (getCuenta() != null) {
            _hashCode += getCuenta().hashCode();
        }
        if (getDescripcion_error() != null) {
            _hashCode += getDescripcion_error().hashCode();
        }
        if (getDetalle() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDetalle());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDetalle(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getGastos_ejecucion() != null) {
            _hashCode += getGastos_ejecucion().hashCode();
        }
        if (getMulta_incumplimiento() != null) {
            _hashCode += getMulta_incumplimiento().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Tipo_respuesta_adeudo_vencido.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "tipo_respuesta_adeudo_vencido"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cadbc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Cadbc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cuenta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Cuenta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descripcion_error");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Descripcion_error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("detalle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Detalle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "detalle_periodos_vencidos"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "detalle_periodos_vencidos"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gastos_ejecucion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Gastos_ejecucion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("multa_incumplimiento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/WcfSecretaria.Negocio", "Multa_incumplimiento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
