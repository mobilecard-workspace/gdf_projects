/**
 * IService1.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.ws.clientes.predial;

public interface IService1 extends java.rmi.Remote {
    public com.addcel.gdf.ws.clientes.predial.vo.Tipo_respuesta_adeudo[] solicitar_adeudo_vigente(com.addcel.gdf.ws.clientes.predial.vo.Tipo_pregunta_adeudo pregunta) throws java.rmi.RemoteException;
    public com.addcel.gdf.ws.clientes.predial.vo.Tipo_respuesta_adeudo_vencido[] solicita_adeudo(com.addcel.gdf.ws.clientes.predial.vo.Tipo_pregunta_adeudo pregunta) throws java.rmi.RemoteException;
    public com.addcel.gdf.ws.clientes.predial.vo.Envio_adeudo_vencido_periodo[] solicita_seleccionados2(com.addcel.gdf.ws.clientes.predial.vo.Recibo_adeudo_vencido_periodo pregunta) throws java.rmi.RemoteException;
    public com.addcel.gdf.ws.clientes.predial.vo.Envio_adeudo_vencido_periodo[] solicita_seleccionados(com.addcel.gdf.ws.clientes.predial.vo.Recibo_adeudo_vencido_periodo pregunta) throws java.rmi.RemoteException;
    public com.addcel.gdf.ws.clientes.predial.vo.Tipo_respuesta_adeudo[] solicitar_adeudo_anticipado(com.addcel.gdf.ws.clientes.predial.vo.Tipo_pregunta_adeudo pregunta) throws java.rmi.RemoteException;
}
