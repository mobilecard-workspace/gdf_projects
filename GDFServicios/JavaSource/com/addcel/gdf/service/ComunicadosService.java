package com.addcel.gdf.service;

import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.ComunicadosDao;
import com.addcel.gdf.vo.comunicados.ListaComunicadosVO;
import com.addcel.utils.ibatis.service.AbstractService;

public class ComunicadosService extends AbstractService {
	private static Logger log = Logger.getLogger(ComunicadosService.class);

	public ListaComunicadosVO consultaComunicados(int idAplicacion) {
		ListaComunicadosVO comunicados = new ListaComunicadosVO(); 
		ComunicadosDao dao = null;
		try {
			dao = (ComunicadosDao) getBean("ComunicadosDao");
	        comunicados = dao.consultaComunicados(idAplicacion);
			
//			TBitacoraService tbitacoraService = new TBitacoraService();
//			tbitacoraService .pruebaTbitacora();
			
//	        TBitacoraProsaService tbitacoraProsaService = new TBitacoraProsaService();
//	        tbitacoraProsaService .pruebaTbitacora();
	        
//	        TransactionProcomService transactionProcomService = new TransactionProcomService();
//	        transactionProcomService .pruebaTbitacora();
	        
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Comunicados Generales." , e);
		}
		return comunicados;
	}
}
