package com.addcel.gdf.service;

import java.util.HashMap;

import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.ProsaPagoVO;

public interface Dependency {

	public String consumeWS(String json);
	public String consumeConsultas(String json) throws Exception;
	public HashMap<String, Object> consume3DSecure(String json) throws Exception;
	public int updateBitacoraDetalle(ProsaPagoVO prosaPagoVO, long idBitacora, int status, String tipoTDC) throws Exception;
	public AbstractVO getDetallePagoProcom(HashMap<String, String> consulta) throws Exception;
}
