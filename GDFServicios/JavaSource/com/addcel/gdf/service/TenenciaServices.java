package com.addcel.gdf.service;

import java.math.BigDecimal;
import java.rmi.RemoteException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;

import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.TenenciaDao;
import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.ProsaPagoVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.vo.tenencia.TenenciaPreguntaVO;
import com.addcel.gdf.vo.tenencia.TenenciaRespuestaVO;
import com.addcel.gdf.ws.clientes.tenencia.ServerPortTypeProxy;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.Utilerias;
import com.addcel.utils.ibatis.service.AbstractService;
import com.addcel.utils.json.me.JSONObject;
import com.google.gson.Gson;

public class TenenciaServices extends AbstractService implements Dependency{
	private static Logger log = Logger.getLogger(TenenciaServices.class);


	@Override
	public String consumeWS(String json) {
		ServerPortTypeProxy proxy = null;
		TenenciaPreguntaVO tenenciaPreguntaVO = null;
		TenenciaRespuestaVO tenenciaVO = null;
		Gson gson = new Gson();

		try {

			ProcomService ps = new ProcomService();
			int productoId = 2;
			BigDecimal monto_max = new BigDecimal(ps.montoMaxProducto(new Integer(productoId)));
			log.info("monto max tenencia :" + monto_max.toString());

			tenenciaPreguntaVO = gson.fromJson(json, TenenciaPreguntaVO.class);

			proxy = new ServerPortTypeProxy();
			javax.xml.rpc.holders.StringHolder placa = new javax.xml.rpc.holders.StringHolder(tenenciaPreguntaVO.getPlaca());
			java.lang.String ejercicio = tenenciaPreguntaVO.getEjercicio();
			javax.xml.rpc.holders.StringHolder modelo = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder ambito= new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder valor_fact = new javax.xml.rpc.holders.StringHolder(); 
			javax.xml.rpc.holders.StringHolder cve_vehi = new javax.xml.rpc.holders.StringHolder(); 
			javax.xml.rpc.holders.StringHolder fech_factura = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder num_cilindros = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder procedencia = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder rfc = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder funcion_cobro = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder numeroError = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder mensaje = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder subsidio = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder depresiacion = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder tenencia = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder actualiza_ten = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder recargo_ten = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder total_tenencia = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder derecho = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder actuliza_derecho = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder recargo_derecho = new javax.xml.rpc.holders.StringHolder(); 
			javax.xml.rpc.holders.StringHolder total_derechos = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder total = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder dagid = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder lineacaptura = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder vigencia = new javax.xml.rpc.holders.StringHolder();
			javax.xml.rpc.holders.StringHolder lineacapturaCB = new javax.xml.rpc.holders.StringHolder();
			
			proxy.calculoTenencia(placa, ejercicio, modelo, ambito, valor_fact, cve_vehi, fech_factura, num_cilindros, procedencia, rfc, funcion_cobro, numeroError, mensaje, subsidio, depresiacion, tenencia, actualiza_ten, recargo_ten, total_tenencia, derecho, actuliza_derecho, recargo_derecho, total_derechos, total, dagid, lineacaptura, vigencia, lineacapturaCB);
			
			tenenciaVO = new TenenciaRespuestaVO();

			tenenciaVO.setTenencia(Utilerias.formatoImporte(tenencia.value));
			tenenciaVO.setTenSubsidio(Utilerias.formatoImporte(subsidio.value));
			tenenciaVO.setTenActualizacion(Utilerias.formatoImporte(actualiza_ten.value));
			tenenciaVO.setTenRecargo(Utilerias.formatoImporte(recargo_ten.value));

			tenenciaVO.setDerechos(Utilerias.formatoImporte(derecho.value));
			tenenciaVO.setDerRecargo(Utilerias.formatoImporte(recargo_derecho.value));
			tenenciaVO.setDerActualizacion(Utilerias.formatoImporte(actuliza_derecho.value));
			tenenciaVO.setTotalDerecho(Utilerias.formatoImporte(total_derechos.value));
			tenenciaVO.setTotalPago(Utilerias.formatoImporte(total.value));
			tenenciaVO.setLinea_captura(lineacaptura.value);

			tenenciaVO.setCve_vehi(cve_vehi.value);
			tenenciaVO.setTipoServicio(ambito.value);
			tenenciaVO.setFech_factura(fech_factura.value);
			tenenciaVO.setValor_fact(valor_fact.value);

			tenenciaVO.setPlaca(placa.value);
			tenenciaVO.setDescrip(mensaje.value);
			tenenciaVO.setError(numeroError.value);
			tenenciaVO.setEjercicio(Utilerias.formatoNumero(ejercicio));
			tenenciaVO.setModelo(Utilerias.formatoNumero(modelo.value));
			try{
				SimpleDateFormat vigenciaFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date vigenciaDate = vigenciaFormat.parse(vigencia.value);
				tenenciaVO.setVigenciaD(vigenciaDate);
			} catch(Exception e){
				log.error("Error al parsear fecha de vigencia: " + e.getMessage());
			}
			tenenciaVO.setRecargos(Utilerias.formatoImporte("0"));
			tenenciaVO.setCondRecargos(Utilerias.formatoImporte("0"));
			
			tenenciaVO.setTenCondRecargo(Utilerias.formatoImporte("0"));
			tenenciaVO.setActualizacion(Utilerias.formatoImporte("0"));
			tenenciaVO.setMonto_maximo_operacion(monto_max);
			
			if(tenenciaVO.getError()!=null){
				int testError = Integer.parseInt(tenenciaVO.getError());
				if(testError > 0){
					json = "{\"error\":\"" + tenenciaVO.getDescrip() + "\",\"numError\":\""+ tenenciaVO.getError()+"\"}";
				}
				else
					json = gson.toJson(tenenciaVO);
			}
			else
				json = gson.toJson(tenenciaVO);
			
		} catch (AxisFault aF) {
			json = "{\"error\":" + aF.getMessage() + ",\"numError\":\"A3001\"}";

		} catch (RemoteException rE) {
			json = "{\"error\":" + rE.getMessage() + ",\"numError\":\"A2001\"}";
			
		} catch (Exception E) {
			json = "{\"error\":" + E.getMessage() + ",\"numError\":\"Z1001\"}";
		}
		return json;
	}
	
	@Override
	public HashMap<String, Object> consume3DSecure(String json) throws Exception{
		TBitacoraService tbitacoraService = new TBitacoraService();
		TBitacoraProsaService tbitacoraProsaService = new TBitacoraProsaService();
		TenenciaRespuestaVO tenenciaRespuestaVO = null;
		TBitacoraVO tbitacoraVO = null;
		TBitacoraProsaVO tbitacoraProsaVO = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Gson gson = new Gson();
		long id_bitacora = 0;
		try {
			
			tenenciaRespuestaVO = gson.fromJson(json, TenenciaRespuestaVO.class);
			tbitacoraVO = gson.fromJson(json, TBitacoraVO.class);
			tbitacoraProsaVO = gson.fromJson(json, TBitacoraProsaVO.class);
			
			tbitacoraVO.setId_proveedor(16);
			tbitacoraVO.setBit_status(0);
			tbitacoraVO.setBit_codigo_error(0);
			tbitacoraVO.setBit_cargo(tenenciaRespuestaVO.getTotalPago());
			tbitacoraVO.setDestino(tenenciaRespuestaVO.getLinea_captura());
			
			id_bitacora = tbitacoraService.insertTBitacora(tbitacoraVO);
			if(id_bitacora > 0){
				tbitacoraVO.setId_bitacora(id_bitacora);
				tenenciaRespuestaVO.setId_bitacora(id_bitacora);
				tenenciaRespuestaVO.setError("0");
				tbitacoraProsaVO.setId_bitacora(new Long(id_bitacora));
				tbitacoraProsaService.insertTBitacora(tbitacoraProsaVO);
				insertBitacoraTenencia(tenenciaRespuestaVO);
			}
			tenenciaRespuestaVO.setId_producto(ConstantesGDF.idProd_Tenencia);
			resp.put("tbitacoraVO", tbitacoraVO);
			resp.put("guardaVO", tenenciaRespuestaVO);
			
			log.info("Exito TenenciaServices.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora());
		} catch (Exception e) {
			log.error("Error TenenciaServices.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora(), e);
			throw new Exception(e);
		}
		return resp;
	}
	
	public int insertBitacoraTenencia(TenenciaRespuestaVO bitacora) throws Exception{
		TenenciaDao dao = null;
		int idTenencia = 0;
		try {
			dao = (TenenciaDao) getBean("TenenciaDao");
			dao.insertGDFTenencia(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al insert Detalle GDF Tenencia ." , e);
			throw new Exception(e);
		}
		
		return idTenencia;
	}
	
	public int updateBitacoraTenencia(TenenciaRespuestaVO bitacora) throws Exception{
		TenenciaDao dao = null;
		int idTenencia = 0;
		try {
			dao = (TenenciaDao) getBean("TenenciaDao");
			dao.updateGDFTenencia(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Tenencia ." , e);
			throw new Exception(e);
		}
		
		return idTenencia;
	}
	
	@Override
	public int updateBitacoraDetalle(ProsaPagoVO prosaPagoVO, long idBitacora, int status, String tipoTDC) throws Exception{
		TenenciaDao dao = null;
		TenenciaRespuestaVO bitacora = null;
		int idAgua = 0;
		try {
			bitacora = new TenenciaRespuestaVO();
			
			bitacora.setId_bitacora(idBitacora);
			bitacora.setNo_autorizacion(prosaPagoVO.getAutorizacion());
//			bitacora.setError(transactionProcomVO.getError());
//			bitacora.setDescError(transactionProcomVO.getDescError());
			bitacora.setTipoTarjeta(tipoTDC);
			bitacora.setStatusPago(status);
			dao = (TenenciaDao) getBean("TenenciaDao");
			dao.updateGDFTenencia(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Agua ." , e);
			throw new Exception(e);
		}
		
		return idAgua;
	}
	
	@Override
	public String consumeConsultas(String json) throws Exception{
		HashMap<String, String> consulta = new HashMap<String, String>();
//		TenenciaDao dao = null;
		Gson gson = new Gson();
		try {
			JSONObject jsObject = new JSONObject(json);
			
			if (jsObject.has("id_usuario")){
				consulta.put("id_usuario",jsObject.getString("id_usuario"));
				log.error("id_usuario: " + jsObject.getString("id_usuario") );
			}
			if (jsObject.has("mes")){
				consulta.put("mes",jsObject.getString("mes"));
				log.error("mes: " + jsObject.getString("mes") );
			}
			if (jsObject.has("anio")){
				consulta.put("anio",jsObject.getString("anio"));
				log.error("anio: " + jsObject.getString("anio") );
			}
			
//			dao = (TenenciaDao) getBean("TenenciaDao");
			
//			TenenciaRespuestaVO tenenciaVO = new TenenciaRespuestaVO();
//			tenenciaVO = crearObjeto();
			
//			dao.insertGDFTenencia(tenenciaVO);
			
//			dao.updateGDFTenencia(tenenciaVO);
			
			json = "{\"consultaTenencia\":" + gson.toJson(getDetallePagos(consulta)) + "}";
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Consulta Tenencia." , e);
			throw new Exception(e);
		}
		
		return json;
	}
	
	
	
	public List<TenenciaRespuestaVO> getDetallePagos(HashMap<String, String> consulta) throws Exception{
		List<TenenciaRespuestaVO> listaTenencia = new ArrayList<TenenciaRespuestaVO>();
		TenenciaDao dao = null;
		try {
			dao = (TenenciaDao) getBean("TenenciaDao");
			
			listaTenencia = dao.selectGDFTenencia(consulta);
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los TenenciaServices.getDetallePagos." , e);
			throw new Exception(e);
		}
		
		return listaTenencia;
	}
	
	@Override
	public AbstractVO getDetallePagoProcom(HashMap<String, String> consulta) throws Exception{
		List<TenenciaRespuestaVO> listaTenencia = new ArrayList<TenenciaRespuestaVO>();
		AbstractVO abstractVO = null;
		try {
			listaTenencia = getDetallePagos(consulta);
			
			if(listaTenencia != null && listaTenencia.size() > 0){
				abstractVO = (AbstractVO) listaTenencia.get(0);
			}
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los TenenciaServices.getDetallePagoProcom." , e);
			throw new Exception(e);
		}
		
		return abstractVO;
	}
	
	private static TenenciaRespuestaVO crearObjeto(){
		TenenciaRespuestaVO tenenciaVO = new TenenciaRespuestaVO();
		tenenciaVO.setId_bitacora(110302);
		tenenciaVO.setId_usuario(13709053);
		tenenciaVO.setNo_autorizacion("024510");
		tenenciaVO.setPlaca("100ABC");
		tenenciaVO.setCondonacion(false);
//		tenenciaVO.setTenencia(new BigDecimal(1872.69));
//		tenenciaVO.setDerechos(new BigDecimal(411.00));
//		tenenciaVO.setTenSubsidio(new BigDecimal(0.00));
//		tenenciaVO.setTotal(new BigDecimal(2411.00));
//		tenenciaVO.setVigenciaD(new Date(1376110800000L));
//		tenenciaVO.setInteres(false);
//		tenenciaVO.setSubsidio(false);
//		tenenciaVO.setEjercicio(new BigDecimal(2013));
//		tenenciaVO.setModelo(new BigDecimal(2005));
//		tenenciaVO.setLineacaptura("84105XX100ABC8DK64AH");
//		tenenciaVO.setTipoServicio("PARTICULAR");
//		tenenciaVO.setCve_vehi("0040720");
//		tenenciaVO.setFechaPago("2005-04-22");
//		tenenciaVO.setTenActualizacion(new BigDecimal(0));
//		tenenciaVO.setTenRecargo(new BigDecimal(104.68));
//		tenenciaVO.setDerActualizacion(new BigDecimal(0));
//		tenenciaVO.setDerRecargo(new BigDecimal(22.97));
//		tenenciaVO.setTotalDerecho(new BigDecimal(433.97));
//		tenenciaVO.setDerecho(new BigDecimal(411));
//		tenenciaVO.setActualizacion(new BigDecimal(0));
//		tenenciaVO.setRecargos(new BigDecimal(126.65));
//		tenenciaVO.setTenSubsidio(new BigDecimal(0));
		tenenciaVO.setError("0");
		tenenciaVO.setStatusPago(1);
		return tenenciaVO; 
	}

	
}
