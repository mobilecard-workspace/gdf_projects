package com.addcel.gdf.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.axis.AxisFault;
import org.apache.axis.AxisProperties;
import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.NominaDao;
import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.ProsaPagoVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.vo.nomina.NominaPreguntaVO;
import com.addcel.gdf.vo.nomina.NominaRespuestaVO;
import com.addcel.gdf.ws.clientes.nomina.ServicesPortTypeProxy;
import com.addcel.gdf.ws.clientes.nomina.Tipo_pregunta;
import com.addcel.gdf.ws.clientes.nomina.Tipo_respuesta;
import com.addcel.utils.AxisSSLSocketFactory;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.Utilerias;
import com.addcel.utils.ibatis.service.AbstractService;
import com.addcel.utils.json.me.JSONObject;
import com.google.gson.Gson;

public class NominaService extends AbstractService implements Dependency{
	private static Logger log = Logger.getLogger(NominaService.class);

	@Override
	public String consumeWS(String json) {
		ServicesPortTypeProxy servicesPortTypeProxy = null;
		NominaPreguntaVO nominaPregunta = null;
		Tipo_pregunta tipo_pregunta = null;
		Tipo_respuesta tipo_respuesta = null;
		NominaRespuestaVO nominaRespuesta = null;
		Gson gson = new Gson();
		
		try {

			ProcomService ps = new ProcomService();
			int productoId = 1;
			BigDecimal monto_max = new BigDecimal(ps.montoMaxProducto(new Integer(productoId)));
			log.info("monto max nomina :" + monto_max.toString());

			nominaPregunta = gson.fromJson(json, NominaPreguntaVO.class);
			
			servicesPortTypeProxy = new ServicesPortTypeProxy();

			tipo_pregunta = new Tipo_pregunta();
			tipo_pregunta.setRfc(nominaPregunta.getRfc());
			tipo_pregunta.setMes_pago_nomina(nominaPregunta.getMes_pago_nomina());
			tipo_pregunta.setAnio_pago_nomina(nominaPregunta.getAnio_pago_nomina());
			tipo_pregunta.setRemuneraciones(nominaPregunta.getRemuneraciones());
			tipo_pregunta.setTipo_declaracion(nominaPregunta.getTipo_declaracion());
			tipo_pregunta.setNum_trabajadores(nominaPregunta.getNum_trabajadores());
			tipo_pregunta.setInteres(nominaPregunta.isInteres());
			tipo_pregunta.setUsuario("addcel");
			tipo_pregunta.setPassword("25b6f919837ece5aab57930fcb1ee09e");
			
			AxisSSLSocketFactory.setKeystorePassword("gdfkeypass");
			AxisSSLSocketFactory.setResourcePathToKeystore("com/addcel/utils/GDFkey.jks");
			AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.utils.AxisSSLSocketFactory");
			
			tipo_respuesta = servicesPortTypeProxy.solicitar_lc(tipo_pregunta);

			nominaRespuesta = new NominaRespuestaVO();
			
			nominaRespuesta.setClave(tipo_respuesta.getClave());
			nominaRespuesta.setRfc(tipo_respuesta.getRfc());
			nominaRespuesta.setMes_pago(tipo_respuesta.getMes_pago());
			nominaRespuesta.setAnio_pago(tipo_respuesta.getAnio_pago());
			
			nominaRespuesta.setRemuneraciones(Utilerias.formatoImporte(tipo_respuesta.getRemuneraciones()));
			nominaRespuesta.setImpuesto(Utilerias.formatoImporte(tipo_respuesta.getImpuesto()));
			nominaRespuesta.setImpuesto_actualizado(Utilerias.formatoImporte(tipo_respuesta.getImpuesto_actualizado()));
			nominaRespuesta.setRecargos(Utilerias.formatoImporte(tipo_respuesta.getRecargos()));
			nominaRespuesta.setRecargos_condonado(Utilerias.formatoImporte(tipo_respuesta.getRecargos_condonado()));
			nominaRespuesta.setInteres(Utilerias.formatoImporte(tipo_respuesta.getInteres()));
			nominaRespuesta.setTotalPago(Utilerias.formatoImporte(tipo_respuesta.getTotal()));
			
			nominaRespuesta.setVigencia(tipo_respuesta.getVigencia());
			nominaRespuesta.setLinea_captura(tipo_respuesta.getLineacaptura());
			nominaRespuesta.setLineacapturaCB(tipo_respuesta.getLineacapturaCB());
			nominaRespuesta.setError(tipo_respuesta.getError() != null?tipo_respuesta.getError().toString():"0");
			nominaRespuesta.setError_descripcion(tipo_respuesta.getError_descripcion());
			
			nominaRespuesta.setNum_trabajadores(nominaPregunta.getNum_trabajadores().intValue());
			nominaRespuesta.setTipo_declaracion(tipo_pregunta.getTipo_declaracion().intValue());
			nominaRespuesta.setMonto_maximo_operacion(monto_max);

			json = gson.toJson(nominaRespuesta);

		} catch (AxisFault aF) {

			json = "{\"error\":\"1\",\"numError\":\"A3001\"}";

			aF.printStackTrace();
			
		} catch (RemoteException rE) {

			json = "{\"error\":\"1\",\"numError\":\"A2001\"}";
			rE.printStackTrace();
		} catch (IOException ioE) {

			json = "{\"error\":\"1\",\"numError\":\"A1001\"}";
			ioE.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}

		return json;
	}
	
	@Override
	public HashMap<String, Object> consume3DSecure(String json) throws Exception {
		TBitacoraService tbitacoraService = new TBitacoraService();
		TBitacoraProsaService tbitacoraProsaService = new TBitacoraProsaService();
		NominaRespuestaVO nominaBitacoraVO = null;
		TBitacoraVO tbitacoraVO = null;
		TBitacoraProsaVO tbitacoraProsaVO = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Gson gson = new Gson();
		long id_bitacora = 0;
		try {
			
			nominaBitacoraVO = gson.fromJson(json, NominaRespuestaVO.class);
			tbitacoraVO = gson.fromJson(json, TBitacoraVO.class);
			tbitacoraProsaVO = gson.fromJson(json, TBitacoraProsaVO.class);
			
			tbitacoraVO.setId_proveedor(16);
			tbitacoraVO.setBit_status(0);
			tbitacoraVO.setBit_codigo_error(0);
			tbitacoraVO.setBit_cargo(nominaBitacoraVO.getTotalPago());
			tbitacoraVO.setDestino(nominaBitacoraVO.getLinea_captura());
			
			id_bitacora = tbitacoraService.insertTBitacora(tbitacoraVO);
			if(id_bitacora > 0){
				tbitacoraVO.setId_bitacora(id_bitacora);
				nominaBitacoraVO.setId_bitacora(id_bitacora);
				nominaBitacoraVO.setError("0");
				tbitacoraProsaVO.setId_bitacora(new Long(id_bitacora));
				tbitacoraProsaService.insertTBitacora(tbitacoraProsaVO);
				insertBitacoraNomina(nominaBitacoraVO);
			}
			nominaBitacoraVO.setId_producto(ConstantesGDF.idProd_Nomina);
			resp.put("tbitacoraVO", tbitacoraVO);
			resp.put("guardaVO", nominaBitacoraVO);
			resp.put("monto", String.valueOf(tbitacoraVO.getBit_cargo()));
			
			log.info("Exito NominaService.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora());
			
		} catch (Exception e) {
			log.error("Error TenenciaServices.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora(), e);
			throw new Exception(e);
		}
		return resp;
	}
	
	public int insertBitacoraNomina(NominaRespuestaVO bitacora) throws Exception{
		NominaDao dao = null;
		int idAgua = 0;
		try {
			dao = (NominaDao) getBean("NominaDao");
			dao.insertGDFNomina(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al insert Detalle GDF Agua ." , e);
			throw new Exception(e);
		}
		
		return idAgua;
	}
	
	public int updateBitacoraNomina(NominaRespuestaVO bitacora) throws Exception{
		NominaDao dao = null;
		int idAgua = 0;
		try {
			dao = (NominaDao) getBean("NominaDao");
			dao.updateGDFNomina(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Agua ." , e);
			throw new Exception(e);
		}
		
		return idAgua;
	}
	
	@Override
	public int updateBitacoraDetalle(ProsaPagoVO prosaPagoVO, long idBitacora, int status, String tipoTDC) throws Exception{
		NominaDao dao = null;
		NominaRespuestaVO bitacora = null;
		int idAgua = 0;
		try {
			bitacora = new NominaRespuestaVO();
			
			bitacora.setId_bitacora(idBitacora);
			bitacora.setNo_autorizacion(prosaPagoVO.getAutorizacion());
//			bitacora.setError(transactionProcomVO.getError());
//			bitacora.setError_descripcion(transactionProcomVO.getDescError());
			bitacora.setTipoTarjeta(tipoTDC);
			bitacora.setStatusPago(status);
			dao = (NominaDao) getBean("NominaDao");
			dao.updateGDFNomina(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Agua ." , e);
			throw new Exception(e);
		}
		
		return idAgua;
	}


	public String consumeConsultas(String json) throws Exception{
		HashMap<String, String> consulta = new HashMap<String, String>();
//		NominaDao dao = null;
		Gson gson = new Gson();
		try {
			JSONObject jsObject = new JSONObject(json);
			
			if (jsObject.has("id_usuario")){
				consulta.put("id_usuario",jsObject.getString("id_usuario"));
				log.info("id_usuario: " + jsObject.getString("id_usuario") );
			}
			if (jsObject.has("mes")){
				consulta.put("mes",jsObject.getString("mes"));
				log.info("mes: " + jsObject.getString("mes") );
			}
			if (jsObject.has("anio")){
				consulta.put("anio",jsObject.getString("anio"));
				log.info("anio: " + jsObject.getString("anio") );
			}
			
//			dao = (NominaDao) getBean("NominaDao");
			
//			NominaRespuestaVO nominaRespuestaVO = new NominaRespuestaVO();
//			nominaRespuestaVO = crearObjeto();
			
//			dao.insertGDFNomina(nominaRespuestaVO);
//			
//			dao.updateGDFNomina(nominaRespuestaVO);
			
			json = "{\"consultaNomina\":" + gson.toJson(getDetallePagos(consulta)) + "}";
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Consulta Nomina." , e);
			throw new Exception(e);
		}
		
		return json;
	}
	
	
	public List<NominaRespuestaVO> getDetallePagos(HashMap<String, String> consulta) throws Exception{
		List<NominaRespuestaVO> listaNomina = new ArrayList<NominaRespuestaVO>();
		NominaDao dao = null;
		try {
			dao = (NominaDao) getBean("NominaDao");
			
			listaNomina = dao.selectGDFNomina(consulta);
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los NominaService.getDetallePagos." , e);
			throw new Exception(e);
		}
		
		return listaNomina;
	}
	
	@Override
	public AbstractVO getDetallePagoProcom(HashMap<String, String> consulta) throws Exception{
		List<NominaRespuestaVO> listaNomina = new ArrayList<NominaRespuestaVO>();
		AbstractVO abstractVO = null;
		try {
			listaNomina = getDetallePagos(consulta);
			
			if(listaNomina.size() > 0){
				abstractVO = (AbstractVO) listaNomina.get(0);
			}
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los NominaService.getDetallePagoProcom." , e);
			throw new Exception(e);
		}
		
		return abstractVO;
	}
	
	private static NominaRespuestaVO crearObjeto(){
		NominaRespuestaVO nominaRespuestaVO = new NominaRespuestaVO();
		nominaRespuestaVO.setId_bitacora(110202);
		nominaRespuestaVO.setId_usuario(13709053);
		nominaRespuestaVO.setNo_autorizacion("071201");
		nominaRespuestaVO.setClave("96");
		nominaRespuestaVO.setRfc("CTE0411241R7");
		nominaRespuestaVO.setMes_pago("01");
		nominaRespuestaVO.setAnio_pago("2013");
//		nominaRespuestaVO.setRemuneraciones(new BigDecimal(900));
//		nominaRespuestaVO.setImpuesto(new BigDecimal(225));
//		nominaRespuestaVO.setImpuesto_actualizado(new BigDecimal(227));
//		nominaRespuestaVO.setRecargos(new BigDecimal(17.14));
//		nominaRespuestaVO.setRecargos_condonado(new BigDecimal(0));
//		nominaRespuestaVO.setInteres(new BigDecimal(0));
//		nominaRespuestaVO.setTotal(new BigDecimal(244));
//		nominaRespuestaVO.setVigencia("2013-08-10");
//		nominaRespuestaVO.setLineacaptura("96CTE04BOX1R78DFRTTY");
		nominaRespuestaVO.setLineacapturaCB("96CTE04BOX1R78DFRTTY000000244386");
		nominaRespuestaVO.setTipo_declaracion(0);
		nominaRespuestaVO.setNum_trabajadores(0);
		nominaRespuestaVO.setError("0");
		nominaRespuestaVO.setStatusPago(1);
		return nominaRespuestaVO; 
	}

}


