package com.addcel.gdf.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.UsuarioVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.vo.bitacoras.TransactionProcomVO;
import com.addcel.gdf.ws.clientes.finanzas.Tipo_respuesta;
import com.addcel.utils.ConstantesGDF;


/**
 * Servlet implementation class ComercioConGDF
 */
public class ProcomAproviGDFService{
	private static final Logger logger = Logger.getLogger(ProcomAproviGDFService.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProcomAproviGDFService() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public HashMap<String, String> processProcom(TransactionProcomVO transactionProcomVO) throws Exception {
    	logger.debug("Procesando peticion");
    	ProcomService ps=new ProcomService();
    	RequestDispatcher rd;
    	TransactionProcomService tranProcomService = new TransactionProcomService();
    	Tipo_respuesta resp = null;
    	AbstractVO abstractPagoVO = null;
    	StringBuffer paramEmail = null;
    	UsuarioVO usuario = null;
    	HashMap<String, String> result = new HashMap<String, String>();
    	TBitacoraVO tbitacoraVO = new TBitacoraVO();
    	TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
    	
//    	String correo = null;
    	String patron = "000000";
    	String patron2 = "000000000000";
    	DecimalFormat formato = new DecimalFormat(patron);
    	DecimalFormat formato2 = new DecimalFormat(patron2);
    	
    	Random  rnd = new Random();
    	
    	int aut = (int)(rnd.nextDouble() * 100000);
    	long ref = (long)(rnd.nextDouble() * 100000000);
    	
    	transactionProcomVO.setEm_Auth(formato.format(aut));
    	transactionProcomVO.setEm_RefNum(formato2.format(ref));
    	
    	try{
    		//Insercion en ecommerce        
    		logger.info("Inserccion en ecommerce");
    		tranProcomService.insertTransactionProcom(transactionProcomVO);
    	}catch(Exception e){
    		logger.error("Error durante el insert en TransactionProcom: \n"+ transactionProcomVO, e);
    	}
        
        logger.info("Inicio digestRegresoProsa.");
      //Valida digest
        String digest=ps.digestRegresoProsa(transactionProcomVO);
        
//        if(transactionProcomVO.getEm_Digest() != null && transactionProcomVO.getEm_Digest().equals(digest)){
        	 if (!transactionProcomVO.getEm_Auth().equals("000000")) {
        		 //Aprovicionamineto gdf
        		 logger.info("Aprovisionamiento GDF");    
//        		 updateBitacoraDetalle(transactionProcomVO,1);
        		 abstractPagoVO = getDetallePago(transactionProcomVO);
        		 
        		 		// (Linea de Captura, Referencia, Total)
        		 try{
	        		 resp = ps.aprovisionamientoGdf(
	        				 abstractPagoVO.getLinea_captura(), transactionProcomVO.getEm_RefNum(), 
	        				 new BigDecimal(abstractPagoVO.getTotalPago()));
        		 }catch(Exception e){
        			 logger.error("Error en Aprovisionamiento GDF: " + abstractPagoVO.getId_bitacora(), e);   
        			 resp = new Tipo_respuesta();
        			 resp.setError(new BigDecimal(3));
        			 resp.setError_descripcion(e.getMessage().length() > 50?e.getMessage().substring(0, 59):e.getMessage());
        		 }
        		 if(resp!= null && resp.getError().intValue() == 0){
//        			 updateBitacoraDetalle(transactionProcomVO, 2); 
        		 }else{
//        			 updateBitacoraDetalle(transactionProcomVO,-1); 
        		 }
        		 
        		 tbitacoraVO.setBit_status(resp.getError().intValue() == 0? 1: -1);
        		 tbitacoraVO.setBit_codigo_error(resp.getError().intValue());
        		// inicio MLS imprimir numero de tarjeta en pdf 
        		 usuario = ps.selectUsuario(String.valueOf(transactionProcomVO.getId_usuario()));
        		 //Recuperar datos para generar el recibo        		         		        		 
        		 
        		 String jsonObject = ps.jsonObject(  abstractPagoVO.getId_bitacora(), (int)transactionProcomVO.getId_producto(),ps.getUltimos4TDC(usuario.getUsrTdcNumero()));
        		// fin MLS imprimir numero de tarjeta en pdf
        		 //envio de mail
        		 logger.info("Envio de correo GDF");
        		 
        		 if(usuario != null && usuario.getEmail() != null){
//        			 correo="jesus.lopez@addcel.com";
        			 paramEmail = new StringBuffer()
        					 .append("correo=").append(usuario.getEmail())
//        					 .append("correo=").append("jesus.lopez@addcel.com")
        					 .append("&json=").append(jsonObject);
        			 
        			 logger.info("Invio de Emai Parametros: " + paramEmail);
        			 ps.peticionUrlPostParams("http://50.57.192.214:8080/MailSenderAddcel/envia-recibo-gdf/" 
        					 + transactionProcomVO.getId_producto(),paramEmail.toString()); 
        		 }else{
        			 logger.error("ERROR imposible enviar email: el correo es nulo");
        		 }        		 
        		 //Pametro de la vista
        		 if(resp!=null){        		 
        			 result.put("respgdf", resp.getSecuencia_trans().toString());   
	        		 logger.info("Respuesta de gdf (Confirmacion de Pago)");
	        		 logger.info(resp.getSecuencia_trans()+"\t"+resp.getError()+"\t"+resp.getError_descripcion());
        		 }
        		 result.put("monto", abstractPagoVO.getTotalPago());
        		 result.put("autoBan", transactionProcomVO.getEm_Auth());
        		 result.put("refeBan", transactionProcomVO.getEm_RefNum());
        		 result.put("Dispatcher","/procom/pago.jsp");        		 
        	 }else{
//        		 updateBitacoraDetalle(transactionProcomVO,-1);
        		 logger.error("Transaccion no autorizada por 3D Secure");
        		 logger.error("Autorizacion: "+ transactionProcomVO.getEm_Auth());
        		 logger.error("Respuesta: "+transactionProcomVO.getEm_Response());
        		 result.put("Dispatcher","/procom/error.jsp");
        	 }
//        }else{        	
//        	logger.error("Digest incorrecto: "+ transactionProcomVO.getEm_Digest()+ "!=" +digest);
//        	logger.error("Digest Prosa: "+transactionProcomVO.getEm_Digest());
//        	logger.error("Digest Calculado: "+digest);
//        	result.put("Dispatcher","/procom/error.jsp");
//        	
//        }
        //Actualizacion de bitacoras
//        rd.forward(request, response);
        try{
        	logger.info("Inicio Update TBitacora.");
        	tbitacoraVO.setId_bitacora(Long.parseLong(transactionProcomVO.getEm_RefNum()));
        	tbitacoraVO.setBit_concepto(getConcepto(transactionProcomVO));
        	tbitacoraVO.setBit_ticket(getTicket(transactionProcomVO));
        	
        	TBitacoraService tbitacoraService = new TBitacoraService();
        	tbitacoraService.updateTBitacora(tbitacoraVO);
        	logger.info("Fin Update TBitacora.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacora: ", e);
        }
        
        try{
        	logger.info("Inicio Update TBitacoraProsa.");
        	tbitacoraProsaVO.setId_bitacora(Long.parseLong(transactionProcomVO.getEm_RefNum()));
        	tbitacoraProsaVO.setAutorizacion(transactionProcomVO.getEm_Response());
        	
        	TBitacoraProsaService tbitacoraProsaService = new TBitacoraProsaService();
        	tbitacoraProsaService.updateTBitacoraProsa(tbitacoraProsaVO);
        	logger.info("Fin Update TBitacora.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacoraProsa: ", e);
        }
        
        logger.info("Actualizacion de bitacoras");
        return result;
    }

    public TransactionProcomVO getProcomRequest(HttpServletRequest request) throws ServletException, IOException {
    	logger.debug("Creando ProcomRespuestaVO.");
    	TransactionProcomVO transactionProcomVO = new TransactionProcomVO();

    	if(request.getParameter("id_usuario") != null){
    		transactionProcomVO.setId_usuario(Long.parseLong(request.getParameter("id_usuario")));
    	}
    	if(request.getParameter("id_producto") != null){
    		transactionProcomVO.setId_producto(Integer.parseInt(request.getParameter("id_producto")));
    	}
    	if(request.getParameter("EM_Response") != null){
    		transactionProcomVO.setEm_Response(request.getParameter("EM_Response"));
    	}
    	if(request.getParameter("EM_Total") != null){
    		transactionProcomVO.setEm_Total(Long.parseLong(request.getParameter("EM_Total")));
    		transactionProcomVO.setTransactionProcomMnt(Long.parseLong(request.getParameter("EM_Total")));
    	}
    	if(request.getParameter("EM_OrderID") != null){
    		transactionProcomVO.setEm_OrderID(request.getParameter("EM_OrderID"));
    		transactionProcomVO.setTransactionProcomMrchId(Long.parseLong(request.getParameter("EM_OrderID")));
    		transactionProcomVO.setTransactionProsaId(Long.parseLong(request.getParameter("EM_OrderID")));
    	}
    	if(request.getParameter("EM_Merchant") != null){
    		transactionProcomVO.setEm_Merchant(Long.parseLong(request.getParameter("EM_Merchant")));
    	}
    	if(request.getParameter("EM_Store") != null){
    		transactionProcomVO.setEm_Store(Long.parseLong(request.getParameter("EM_Store")));
    	}
    	if(request.getParameter("EM_Term") != null){
    		transactionProcomVO.setEm_Term(request.getParameter("EM_Term"));
    	}
    	if(request.getParameter("EM_RefNum") != null){
    		transactionProcomVO.setEm_RefNum(request.getParameter("EM_RefNum"));
    	}
    	if(request.getParameter("EM_Auth") != null){
    		transactionProcomVO.setEm_Auth(request.getParameter("EM_Auth"));
    	}
    	if(request.getParameter("EM_Digest") != null){
    		transactionProcomVO.setEm_Digest(request.getParameter("EM_Digest"));
    	}
    	
    	return transactionProcomVO;
    }
    
    protected int updateBitacoraDetalle(TransactionProcomVO transactionProcomVO, long idBitacora, int status) throws Exception {
		Dependency dependency = null;
		int respuesta = 0; 
		
		switch (transactionProcomVO.getId_producto()) {
			case ConstantesGDF.idProd_Nomina:
				dependency = new NominaService();
				break;
	
			case ConstantesGDF.idProd_Tenencia:
				dependency = new TenenciaServices();
				break;
				
			case ConstantesGDF.idProd_Infraccion:
				dependency = new InfraccionService();
				break;
				
			case ConstantesGDF.idProd_Predial:
				dependency = new PredialServices();
				break;
			
			case ConstantesGDF.idProd_Agua:
				dependency = new AguaService();
				break;
				
			default:
				break;
		}
		
//		respuesta = dependency.updateBitacoraDetalle(transactionProcomVO, idBitacora, status);
		
		return respuesta;
	}
    
    protected AbstractVO getDetallePago(TransactionProcomVO transactionProcomVO) throws Exception {

		AbstractVO abstractVO = new AbstractVO();
		Dependency dependency = null;
		HashMap<String, String> consulta = new HashMap<String, String>();
		
		switch ((int)transactionProcomVO.getId_producto()) {
			case ConstantesGDF.idProd_Nomina:
				dependency = new NominaService();
				break;
	
			case ConstantesGDF.idProd_Tenencia:
				dependency = new TenenciaServices();
				break;
				
			case ConstantesGDF.idProd_Infraccion:
				dependency = new InfraccionService();
				break;
				
			case ConstantesGDF.idProd_Predial:
				dependency = new PredialServices();
				break;
			
			case ConstantesGDF.idProd_Agua:
				dependency = new AguaService();
				break;
				
			default:
				break;
		}
		
		consulta.put("id_bitacora", transactionProcomVO.getEm_OrderID());
		
		abstractVO = dependency.getDetallePagoProcom(consulta);
		
		return abstractVO;
	}
	
    protected String getTicket(TransactionProcomVO transactionProcomVO) throws Exception {
    	StringBuffer ticket= new StringBuffer();
    	if(!transactionProcomVO.getEm_Response().equals("000000")){
    		ticket.append("Excepcion: ").append(transactionProcomVO.getEm_Auth());
    	}else{
    		ticket.append("Pago GDF ").append(
        			descIdProduto((int)transactionProcomVO.getId_producto()));
    	}
    	return ticket.toString();
    }
    
    protected String getConcepto(TransactionProcomVO transactionProcomVO) throws Exception {
    	StringBuffer concepto = new StringBuffer();
    	
    		concepto.append("Pago GDF ").append(
        			descIdProduto((int)transactionProcomVO.getId_producto()));
    	return concepto.toString();
    }
    
    private String descIdProduto(int id_producto){
    	String desc = "";
    	switch (id_producto) {
			case ConstantesGDF.idProd_Nomina:
				desc = "Nomina";
				break;
	
			case ConstantesGDF.idProd_Tenencia:
				desc = "Tenencia";
				break;
				
			case ConstantesGDF.idProd_Infraccion:
				desc = "Infraccion";
				break;
				
			case ConstantesGDF.idProd_Predial:
				desc = "Predial";
				break;
			
			case ConstantesGDF.idProd_Agua:
				desc = "Agua";
				break;
				
			default:
				break;
		}
    	return desc;
    }
}
