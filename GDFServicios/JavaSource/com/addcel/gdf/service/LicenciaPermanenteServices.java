package com.addcel.gdf.service;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.axis.AxisFault;
import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.LicenciaPermanenteBitacoraDao;
import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.ProsaPagoVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.vo.licencia.LicenciaConductorResponseVO;
import com.addcel.gdf.vo.licencia.LicenciaConductorVO;
import com.addcel.gdf.ws.clientes.licencias.ServicesPortTypeProxy;
import com.addcel.gdf.ws.clientes.licencias.Tipo_pregunta;
import com.addcel.gdf.ws.clientes.licencias.Tipo_respuesta;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.ibatis.service.AbstractService;
import com.addcel.utils.json.me.JSONObject;
import com.google.gson.Gson;

public class LicenciaPermanenteServices  extends AbstractService implements Dependency {

	private static Logger log = Logger.getLogger(LicenciaPermanenteServices.class);


	@Override
	public String consumeWS(String json) {
		ProcomService ps = new ProcomService();
		ServicesPortTypeProxy proxy = null;
		LicenciaConductorVO licenciaVO = null;
		LicenciaConductorResponseVO licenciaConductorResponseVO = null;
		
		Gson gson = new Gson();

		try {
			BigDecimal monto_max = new BigDecimal(ps.montoMaxProducto(new Integer(9)));
			
			licenciaVO = gson.fromJson(json, LicenciaConductorVO.class);
			
			Tipo_pregunta pregunta = new Tipo_pregunta();
			pregunta.setClave(new BigDecimal(3));
			pregunta.setUsuario(ConstantesGDF.USUARIO_WS_VEHICULAR);
			pregunta.setPassword(ConstantesGDF.PASSWORD_WS_VEHICULAR);
			pregunta.setRfc(licenciaVO.getRfc());
			pregunta.setEjercicio(new BigDecimal(Calendar.getInstance().get(Calendar.YEAR)));
			pregunta.setTipo_licencia("A");
			pregunta.setUsuario(ConstantesGDF.USUARIO_WS_VEHICULAR);
			pregunta.setPassword(ConstantesGDF.PASSWORD_WS_VEHICULAR);
			
			log.info("Clave: "+pregunta.getClave());
			log.info("usuario: "+pregunta.getUsuario());
			log.info("Password: "+pregunta.getPassword());
			log.info("RFC: "+pregunta.getRfc());
			log.info("Ejercicio: "+pregunta.getEjercicio());
			log.info("Tipo Licencia: "+pregunta.getTipo_licencia());
			
			proxy = new ServicesPortTypeProxy();			
			
			Tipo_respuesta respuesta = proxy.solicitar_lc(pregunta);
			licenciaConductorResponseVO = new LicenciaConductorResponseVO();
			licenciaConductorResponseVO.setClave(respuesta.getClave().intValue());
			licenciaConductorResponseVO.setConcepto(respuesta.getConcepto());
			licenciaConductorResponseVO.setRfc(respuesta.getRfc());
			licenciaConductorResponseVO.setCostoUnitario(respuesta.getCosto_unitario());
			licenciaConductorResponseVO.setImporteFinal(respuesta.getImporte());
			licenciaConductorResponseVO.setTotalPago(String.valueOf(respuesta.getImporte()));
			licenciaConductorResponseVO.setVigencia(respuesta.getVigencia());
			licenciaConductorResponseVO.setEjercicio(respuesta.getEjercicio());
			licenciaConductorResponseVO.setFolio(respuesta.getFolio());
			licenciaConductorResponseVO.setLinea_captura(respuesta.getLc());
			licenciaConductorResponseVO.setError(respuesta.getError().toString());
			licenciaConductorResponseVO.setDescError(respuesta.getError_descripcion());
			licenciaConductorResponseVO.setMonto_maximo_operacion(monto_max);
			
			log.info("Clave: "+respuesta.getClave());
			log.info("Concepto: "+respuesta.getConcepto());
			log.info("RFC: "+respuesta.getRfc());
			log.info("Costo Unitario: "+respuesta.getCosto_unitario());
			log.info("Importe: "+respuesta.getImporte());
			log.info("Vigencia: "+respuesta.getVigencia());
			log.info("Ejercicio: "+respuesta.getEjercicio());
			log.info("Folio: "+respuesta.getFolio());
			log.info("Linea Captura: "+respuesta.getLc());
			log.info("Error: "+respuesta.getError());
			log.info("Descripcion Error: "+respuesta.getError_descripcion());
			
//			licenciaConductorResponseVO = new LicenciaConductorResponseVO();
//			licenciaConductorResponseVO.setClave(1);
//			licenciaConductorResponseVO.setConcepto("Licencia");
//			licenciaConductorResponseVO.setRfc("OANJ830926");
//			licenciaConductorResponseVO.setCostoUnitario(300);
//			licenciaConductorResponseVO.setImporteFinal(335);
//			licenciaConductorResponseVO.setVigencia("2014-02-02");
//			licenciaConductorResponseVO.setEjercicio(new BigDecimal(2014));
//			licenciaConductorResponseVO.setFolio("11112345678");
//			licenciaConductorResponseVO.setLinea_captura("WERTYUIO34567DFGHJ");
//			licenciaConductorResponseVO.setError("0");
//			licenciaConductorResponseVO.setDescError("Exito!");

			if(!"0".equals(licenciaConductorResponseVO.getError())){
				int testError = Integer.valueOf(licenciaConductorResponseVO.getError());
				if(testError > 0){
					json = "{\"error\":\"" + licenciaConductorResponseVO.getDescError() + "\",\"numError\":\""+ licenciaConductorResponseVO.getError()+"\"}";
				}
				else
					json = gson.toJson(licenciaConductorResponseVO);
			}
			else{
				licenciaConductorResponseVO.setNumError("0");
				json = gson.toJson(licenciaConductorResponseVO);
			}
				
		} catch (AxisFault aF) {
			json = "{\"error\":" + aF.getMessage() + ",\"numError\":\"A3001\"}";

		} catch (RemoteException rE) {
			json = "{\"error\":" + rE.getMessage() + ",\"numError\":\"A2001\"}";
			
		} catch (Exception E) {
			json = "{\"error\":" + E.getMessage() + ",\"numError\":\"Z1001\"}";
		}
		return json;
	}
	
	@Override
	public HashMap<String, Object> consume3DSecure(String json) throws Exception{
		TBitacoraService tbitacoraService = new TBitacoraService();
		TBitacoraProsaService tbitacoraProsaService = new TBitacoraProsaService();
		TBitacoraVO tbitacoraVO = null;
		TBitacoraProsaVO tbitacoraProsaVO = null;
		
		LicenciaConductorResponseVO licenciaConductorResponseVO = null;
		
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Gson gson = new Gson();
		long id_bitacora = 0;
		try {
			
			licenciaConductorResponseVO = gson.fromJson(json, LicenciaConductorResponseVO.class);
			tbitacoraVO = gson.fromJson(json, TBitacoraVO.class);
			tbitacoraProsaVO = gson.fromJson(json, TBitacoraProsaVO.class);
			
			licenciaConductorResponseVO.setPermanente(true);
			
			tbitacoraVO.setId_proveedor(16);
			tbitacoraVO.setBit_status(0);
			tbitacoraVO.setBit_codigo_error(0);
			tbitacoraVO.setBit_cargo(String.valueOf(licenciaConductorResponseVO.getImporteFinal()));
			tbitacoraVO.setDestino(licenciaConductorResponseVO.getLinea_captura());
			
			id_bitacora = tbitacoraService.insertTBitacora(tbitacoraVO);
			if(id_bitacora > 0){				
				licenciaConductorResponseVO.setId_bitacora(id_bitacora);
				licenciaConductorResponseVO.setError("0");
				
				tbitacoraVO.setId_bitacora(id_bitacora);
				tbitacoraProsaVO.setId_bitacora(new Long(id_bitacora));
				tbitacoraProsaService.insertTBitacora(tbitacoraProsaVO);
				insertBitacoraLicenciaConductor(licenciaConductorResponseVO);
			}
			licenciaConductorResponseVO.setId_producto(ConstantesGDF.ID_PRODUCTO_LICENCIA_PERMANENTE);
			resp.put("tbitacoraVO", tbitacoraVO);
			resp.put("guardaVO", licenciaConductorResponseVO);
			
			log.info("Exito TarjetaCirculacionServices.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora());
		} catch (Exception e) {
			log.error("Error TarjetaCirculacionServices.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora(), e);
			throw new Exception(e);
		}
		return resp;
	}
	
	public int insertBitacoraLicenciaConductor(LicenciaConductorResponseVO bitacora) throws Exception{
		LicenciaPermanenteBitacoraDao dao = null;
		int idTenencia = 0;
		try {
			dao = (LicenciaPermanenteBitacoraDao) getBean("LicenciaPermanenteDao");
			dao.insertGDFLicenciaPermanente(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al insert Detalle GDF Licencia Conductor." , e);
			throw new Exception(e);
		}
		
		return idTenencia;
	}
	
	public int updateBitacoraLicenciaConductor(LicenciaConductorResponseVO bitacora) throws Exception{
		LicenciaPermanenteBitacoraDao dao = null;
		int idTenencia = 0;
		try {
			dao = (LicenciaPermanenteBitacoraDao) getBean("LicenciaPermanenteDao");
			dao.updateGDFLicenciaPermanente(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Tarjeta Circulacion." , e);
			throw new Exception(e);
		}
		
		return idTenencia;
	}
	
	@Override
	public int updateBitacoraDetalle(ProsaPagoVO prosaPagoVO, long idBitacora, int status, String tipoTDC) throws Exception{
		LicenciaPermanenteBitacoraDao dao = null;
		LicenciaConductorResponseVO bitacora; 
		int idAgua = 0;
		try {
			bitacora = new LicenciaConductorResponseVO();
			
			bitacora.setId_bitacora(idBitacora);
			bitacora.setNo_autorizacion(prosaPagoVO.getAutorizacion());
			bitacora.setTipoTarjeta(tipoTDC);
			bitacora.setStatusPago(status);
			dao = (LicenciaPermanenteBitacoraDao) getBean("LicenciaPermanenteDao");
			dao.updateGDFLicenciaPermanente(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Tarjeta Circulacion ." , e);
			throw new Exception(e);
		}
		
		return idAgua;
	}
	
	@Override
	public String consumeConsultas(String json) throws Exception{
		HashMap<String, String> consulta = new HashMap<String, String>();
		Gson gson = new Gson();
		try {
			JSONObject jsObject = new JSONObject(json);
			
			if (jsObject.has("id_usuario")){
				consulta.put("id_usuario",jsObject.getString("id_usuario"));
				log.error("id_usuario: " + jsObject.getString("id_usuario") );
			}
			if (jsObject.has("mes")){
				consulta.put("mes",jsObject.getString("mes"));
				log.error("mes: " + jsObject.getString("mes") );
			}
			if (jsObject.has("anio")){
				consulta.put("anio",jsObject.getString("anio"));
				log.error("anio: " + jsObject.getString("anio") );
			}
			
			json = "{\"licenciaPagos\":" + gson.toJson(getDetallePagos(consulta)) + ", \"error\":\"Consulta Exitosa\" ,\"numError\":\"0\"}";
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener la consulta de licencia permanente." , e);
			throw new Exception(e);
		}
		
		return json;
	}
	
	
	
	public List<LicenciaConductorResponseVO> getDetallePagos(HashMap<String, String> consulta) throws Exception{
		List<LicenciaConductorResponseVO> listaTenencia = new ArrayList<LicenciaConductorResponseVO>();
		LicenciaPermanenteBitacoraDao dao = null;
		try {
			dao = (LicenciaPermanenteBitacoraDao) getBean("LicenciaPermanenteDao");
			
			listaTenencia = dao.selectGDFLicenciaPermanente(consulta);
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los TarjetaCirculacionServices.getDetallePagos." , e);
			throw new Exception(e);
		}
		
		return listaTenencia;
	}
	
	@Override
	public AbstractVO getDetallePagoProcom(HashMap<String, String> consulta) throws Exception{
		List<LicenciaConductorResponseVO> listaTarjetaCirculacion = new ArrayList<LicenciaConductorResponseVO>();
		AbstractVO abstractVO = null;
		try {
			listaTarjetaCirculacion = getDetallePagos(consulta);
			
			if(listaTarjetaCirculacion != null && listaTarjetaCirculacion.size() > 0){
				abstractVO = (AbstractVO) listaTarjetaCirculacion.get(0);
			}
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los TarjetaCirculacionServices.getDetallePagoProcom." , e);
			throw new Exception(e);
		}
		
		return abstractVO;
	}
	
}