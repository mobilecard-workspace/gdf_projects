package com.addcel.gdf.service;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;

import org.apache.axis.AxisFault;
import org.apache.axis.AxisProperties;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.TUsuarioDao;
import com.addcel.gdf.model.dao.TBitacoraDao;
import com.addcel.gdf.vo.TProveedorVO;
import com.addcel.gdf.vo.UsuarioVO;
import com.addcel.gdf.vo.ProcomProsa.ProcomVO;
import com.addcel.gdf.vo.agua.AguaRespuestaGuardaVO;
import com.addcel.gdf.vo.bitacoras.TransactionProcomVO;
import com.addcel.gdf.vo.bitacoras.CatalogoBinVO;
import com.addcel.gdf.vo.bitacoras.SubStrTarjetasVO;
import com.addcel.gdf.vo.infraccion.InfraccionRespuestaVO;
import com.addcel.gdf.vo.licencia.LicenciaConductorResponseVO;
import com.addcel.gdf.vo.nomina.NominaRespuestaVO;
import com.addcel.gdf.vo.predial.PredialRespuestaAdeudoVO;
import com.addcel.gdf.vo.predial.PredialRespuestaSeleccionadoVencidoVO;
import com.addcel.gdf.vo.tenencia.TenenciaRespuestaVO;
import com.addcel.gdf.ws.clientes.finanzas.ServicesPortTypeProxy;
import com.addcel.gdf.vo.tarjetaCirculacion.TarjetaCirculacionResponseVO;
import com.addcel.gdf.ws.clientes.finanzas.Tipo_pregunta;
import com.addcel.gdf.ws.clientes.finanzas.Tipo_respuesta;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.AxisSSLSocketFactory;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.ibatis.service.AbstractService;
import com.google.gson.Gson;

public class ProcomService extends AbstractService{
	private static final Logger logger=Logger.getLogger(ProcomService.class);
	private static final String afiliacion = "1462795";
	
	public ProcomVO comercioFin(String user,String referencia,String monto,String idTramite) {    	        
        String varTotal=formatoMontoProsa(monto);                                        
        String digest=digestEnvioProsa("7414025", "1234", "001", varTotal, "484", referencia);   
        logger.info("Digest gdf: "+digest);
        							//(total, currency, addres, orderId, merchant, store, term, digest, urlBack, usuario, idTramite)        
    	ProcomVO procomObj=new ProcomVO(varTotal, "484", "PROSA", referencia, "7414025", "1234", "001", digest, "", user, idTramite);
    	
    	logger.info("user gdf: "+user);
    	logger.info("referencia gdf: "+referencia);
    	logger.info("idTramite gdf: "+idTramite);
    	
    	return procomObj;
    }
	
	private String formatoMontoProsa(String monto){
		String varTotal = "000";
		String pesos = null;
        String centavos = null;
		if(monto.contains(".")){
            pesos=monto.substring(0, monto.indexOf("."));
            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
            if(centavos.length()<2){
                centavos = centavos.concat("0");
            }else{
                centavos=centavos.substring(0, 2);
            }            
            varTotal=pesos+centavos;
        }else{
            varTotal=monto.concat("00");
        } 
		logger.info("Monto a cobrar 3dSecure: "+varTotal);
		return varTotal;		
	}
	
	private String digest(String text){
		String digest="";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes(),0,text.length());
			byte[] sha1=md.digest();
			digest = convertToHex(sha1);
		} catch (NoSuchAlgorithmException e) {
			logger.info("Error al encriptar - digest", e);
		}
		return digest;
	}
	
	private String convertToHex(byte[] data) { 
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));    
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    } 
	
	public String digestEnvioProsa(String varMerchant,String varStore,String varTerm,String varTotal,String varCurrency,String varOrderId){
		return digest(varMerchant+varStore+varTerm+varTotal+varCurrency+varOrderId);		
	}
	
	public String digestRegresoProsa(TransactionProcomVO transactionProcomV) {
		StringBuffer digestCad = new StringBuffer()
				.append(transactionProcomV.getEm_Total())
				.append(transactionProcomV.getEm_OrderID())
				.append(transactionProcomV.getEm_Merchant())
				.append(transactionProcomV.getEm_Store())
				.append(transactionProcomV.getEm_Term())
				.append(transactionProcomV.getEm_RefNum())
				.append("-")
				.append(transactionProcomV.getEm_Auth());
				
		return digest(digestCad.toString());		
	}
	
	public Tipo_respuesta aprovisionamientoGdf(String lineaCaptura,String referencia,BigDecimal importe){		
		Tipo_respuesta resp = new Tipo_respuesta();
		ServicesPortTypeProxy servicesPortTypeProxy = null;
		
		try {
			logger.info("Inicia aprovisionamientoGdf: ");
			logger.info("lineaCaptura: " + lineaCaptura);
			logger.info("referencia: " + referencia);
			logger.info("importe: " + importe);
			
			servicesPortTypeProxy = new ServicesPortTypeProxy();
			Tipo_pregunta pregunta = new Tipo_pregunta();
			pregunta.setLc(lineaCaptura);
			pregunta.setRef(referencia);
			pregunta.setImporte(importe);
			pregunta.setTipopago(new BigDecimal(3));
			pregunta.setSucursal(new BigDecimal(3));
			pregunta.setUsuario("addcel");
			pregunta.setPassword("25b6f919837ece5aab57930fcb1ee09e");
			
			AxisSSLSocketFactory.setKeystorePassword("gdfkeypass");
			AxisSSLSocketFactory.setResourcePathToKeystore("com/addcel/utils/GDFkey.jks");
			AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.utils.AxisSSLSocketFactory");
			
			resp = servicesPortTypeProxy.registrar_pago(pregunta);
			logger.info("Respuesta de GDF: \n secuencia: " + resp.getSecuencia_trans().toString() + 
						"\n error : " + resp.getError().toString()+
						"\n descripcion: " + resp.getError_descripcion());
			
		} catch (AxisFault e) {
			logger.error("Error Confirmar Pago gdf: "+e);
			resp.setError(new BigDecimal(3));
			resp.setError_descripcion(e.getMessage().substring(0,50));
		} catch (Exception e) {
			logger.error("Error Confirmar Pago gdf: "+e);
			resp.setError(new BigDecimal(3));
			resp.setError_descripcion(e.getMessage().substring(0,50));
		}		
		return resp;
	}
	
	public String peticionUrlPostParams(String urlRemota,String parametros) throws IOException {
		String respuesta = "Sin respuesta";
		URL url = new URL(urlRemota);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		// Se indica que se escribira en la conexi�n
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		// Se escribe los parametros enviados a la url
		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		logger.info("antes de escribir parametros en la petici�n");
		wr.write(parametros);
		wr.close();			
		if(con.getResponseCode() == HttpURLConnection.HTTP_OK ||
				con.getResponseCode() == HttpURLConnection.HTTP_ACCEPTED){
			logger.info("Respuesta correcta ");
			respuesta = "OK";
		}else{
			respuesta = "ERROR";
		}
		con.disconnect();
		
		return respuesta;
	}
	
	public UsuarioVO selectUsuario(String idUsuario){
		TUsuarioDao usrDao = null;
		UsuarioVO usuario = null;
		try {
			usrDao = (TUsuarioDao) getBean("tUsuarioDao");
			usuario = usrDao.selectUsuario(idUsuario);
			if(usuario != null){
				logger.info("selectUsuario.getUsrLogin : " + usuario.getUsrLogin());
				logger.info("selectUsuario.getEmail : " + usuario.getEmail());
			}else{
				logger.info("selectUsuario No existe el usuario: " + idUsuario);
			}
			
		} catch (Exception e) {
			logger.error("Ocurrio un error al obtener el correo.", e);
		}					
		return usuario;
	}
	public String getAfiliacion(){
		String response = afiliacion;
		try{
			TProveedorVO proveedor = getTProveedor("16");
			if(proveedor!=null){
				response = proveedor.getAfiliacion();
			}
		} catch(Exception e){
			logger.error("Ocurrio un error al obtener la afiliacion.", e);
			response = afiliacion;
		}
		return response;
	}
	
	public TProveedorVO getTProveedor(String idUsuario){
		TUsuarioDao usrDao = null;
		TProveedorVO tproveedorVO = null;
		try {
			usrDao = (TUsuarioDao) getBean("tUsuarioDao");
			tproveedorVO = usrDao.getTProveedor(idUsuario);
			if(tproveedorVO != null){
				logger.info("getTProveedor.getIdProveedor : " + tproveedorVO.getIdProveedor());
				logger.info("getTProveedor.getAfiliacion  : " + tproveedorVO.getAfiliacion());
				logger.info("getTProveedor.getMaxTransacc : " + tproveedorVO.getMaxTransaccion());
			}else{
				logger.info("getTProveedor No existe el getTProveedor: " + idUsuario);
			}
			
		} catch (Exception e) {
			logger.error("Ocurrio un error al obtener el getTProveedor.", e);
		}					
		return tproveedorVO;
	}
	
	// MLS imprimir numero de tarjeta en pdf
	public String jsonObject(long id_bitacora, int id_producto, String ultimos4TDC) throws Exception{
		String json = null;
		
		NominaService nominaService = null;
		TenenciaServices tenenciaServices = null;
		InfraccionService infraccionService = null;
		PredialServices predialServices = null;
		AguaService aguaService = null;
		PredialVencidoServices predialVencidoService = null;
		TarjetaCirculacionServices tarjetaCirculacionService = null;
		LicenciaConductorServices licenciaConductorServices = null;
		LicenciaPermanenteServices licenciaPermanenteServices = null;
		try {			
			HashMap<String,String> consulta=new HashMap<String, String>(1);
			consulta.put("id_bitacora", String.valueOf(id_bitacora));
			Gson gson=new Gson();
			Object object = null;
			switch ( id_producto) {
				case ConstantesGDF.idProd_Nomina:
					nominaService = new NominaService();
					object = nominaService.getDetallePagos(consulta).get(0);
					((NominaRespuestaVO) object).setUltimos4TDC(ultimos4TDC);
					((NominaRespuestaVO) object).generaCertificado();					
					json = gson.toJson((NominaRespuestaVO)object);
					break;
		
				case ConstantesGDF.idProd_Tenencia:
					tenenciaServices = new TenenciaServices();
					object = tenenciaServices.getDetallePagos(consulta).get(0);
					((TenenciaRespuestaVO) object).setUltimos4TDC(ultimos4TDC);
					((TenenciaRespuestaVO) object).generaCertificado();
					json = gson.toJson((TenenciaRespuestaVO) object);
					break;
					
				case ConstantesGDF.idProd_Infraccion:
					infraccionService = new InfraccionService();
					object = infraccionService.getDetallePagos(consulta).get(0);
					((InfraccionRespuestaVO) object).setUltimos4TDC(ultimos4TDC);
					((InfraccionRespuestaVO) object).generaCertificado();
					json = gson.toJson((InfraccionRespuestaVO)object);
					break;
					
				case ConstantesGDF.idProd_Predial:
					predialServices = new PredialServices();
					object = predialServices.getDetallePagos(consulta).get(0);
					((PredialRespuestaAdeudoVO) object).setUltimos4TDC(ultimos4TDC);
					((PredialRespuestaAdeudoVO) object).generaCertificado();
					json = gson.toJson(object);
					break;
				
				case ConstantesGDF.idProd_Agua:
					aguaService = new AguaService();
					object = aguaService.getDetallePagos(consulta).get(0);
					((AguaRespuestaGuardaVO) object).setUltimos4TDC(ultimos4TDC);
					((AguaRespuestaGuardaVO) object).generaCertificado();
					json = gson.toJson((AguaRespuestaGuardaVO)object);
					break;
				case ConstantesGDF.idProd_PredialVencido:
					predialVencidoService = new PredialVencidoServices();
					object = predialVencidoService.getDetallePagos(consulta).get(0);
					((PredialRespuestaSeleccionadoVencidoVO) object).setUltimos4TDC(ultimos4TDC);
					((PredialRespuestaSeleccionadoVencidoVO) object).generaCertificado();
					json = gson.toJson((PredialRespuestaSeleccionadoVencidoVO)object);
					break;
				case ConstantesGDF.ID_PRODUCTO_TARJETA_CIRCULACION:
					tarjetaCirculacionService = new TarjetaCirculacionServices();
					object = tarjetaCirculacionService.getDetallePagos(consulta).get(0);
					((TarjetaCirculacionResponseVO) object).setUltimos4TDC(ultimos4TDC);
					((TarjetaCirculacionResponseVO) object).generaCertificado();
					json = gson.toJson((TarjetaCirculacionResponseVO)object);
					break;
				case ConstantesGDF.ID_PRODUCTO_LICENCIA_CONDUCTOR:
					licenciaConductorServices = new LicenciaConductorServices();
					object = licenciaConductorServices.getDetallePagos(consulta).get(0);
					((LicenciaConductorResponseVO) object).setUltimos4TDC(ultimos4TDC);
					((LicenciaConductorResponseVO) object).generaCertificado();
					json = gson.toJson((LicenciaConductorResponseVO)object);
					break;
				case ConstantesGDF.ID_PRODUCTO_LICENCIA_PERMANENTE:
					licenciaPermanenteServices = new LicenciaPermanenteServices();
					object = licenciaPermanenteServices.getDetallePagos(consulta).get(0);
					((LicenciaConductorResponseVO) object).setUltimos4TDC(ultimos4TDC);
					((LicenciaConductorResponseVO) object).generaCertificado();
					json = gson.toJson((LicenciaConductorResponseVO)object);
					break;
				default:
					break;
			}
		} catch (Exception e) {			
			logger.error("Ocurrio un error al obtener los datos.", e);
			throw e;
		}		
		return json;
	}
	
	public String generaCertificado(String numAuto,String fechaPago,String lineaCaptura,String importe){
		//String llaveBanco="��6��	pP;E5\n�";		
		String claveBanco="986";
		String importeFmt=formatoMontoProsa(importe);
		importe=formatoImporte(importeFmt);
		String cPago=claveBanco+numAuto+formatoFecha(fechaPago)+lineaCaptura+importe;
		byte[] llaveBanco = {3,29,-105,-9,54,-121,-101,9,112,80,59,69,53,20,10,-121}; 		
		byte[] cadenaPago= cPago.getBytes();		
		byte[] certificado=new byte[llaveBanco.length+cadenaPago.length];
		
		System.arraycopy(cadenaPago, 0, certificado, 0, cadenaPago.length);
		System.arraycopy(llaveBanco, 0, certificado, cadenaPago.length,llaveBanco.length);		
		
		byte[] md5c = DigestUtils.md5(certificado);
		byte[] base64 = Base64.encodeBase64(md5c);
		String selloDigital=new String(base64).substring(0, 22);
		
		return selloDigital;
	}
	
	public String formatoFecha(String fecha){		
		String fechafmt="";
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date dia=sdf.parse(fecha);
			sdf=new SimpleDateFormat("ddMMyyyy");
			fechafmt=sdf.format(dia);
		} catch (ParseException e) {			
			logger.error("Formato datos.", e);
		}				
		return fechafmt;
	}
	
	public String formatoImporte(String totalFmtProsa){
		int importe=0;		
		importe=Integer.parseInt(totalFmtProsa);		
		Formatter fmt= new Formatter();
		String d = fmt.format("%1$015d", importe).toString();		
		fmt.close();
		return d;
	}
	//MLS Método para obtener topes de pago por producto
	public String montoMaxProducto(Integer idProducto){
		TUsuarioDao usrDao = null;
		String totalResult = null;
		try {
			usrDao = (TUsuarioDao) getBean("tUsuarioDao");
			totalResult = usrDao.getMontoMaxProducto(idProducto);
			if(totalResult != null){
				logger.info("total por producto: " + totalResult);
			}else{
				logger.info("No existe el total por producto: " + idProducto);
			}
			
		} catch (Exception e) {
			logger.error("Ocurrio un error al obtener el total por producto.", e);
		}
		return totalResult;
	}
	// inicio MLS imprimir numero de tarjeta en pdf
	/**
	 * Método para regresar la cadena de la tarjeta de credito con solo los cuatro
	 * ultimos digitos visibles
	 * @param encryptedTDC Numero de tarjeta de credito encriptada
	 * @return 
	 */
	public String getUltimos4TDC(String encryptedTDC){
		String ultimos4TDC = null;
		try{
			String TDC = AddcelCrypto.decryptTarjeta(encryptedTDC);
			if(TDC!=null){
				if(TDC.length()==16)
					ultimos4TDC = "XXXX XXXX XXXX " + TDC.substring(12);
				else
					ultimos4TDC = "XXXX XXXXXX X " + TDC.substring(11);
			}
		} catch(Exception e){
			logger.error("No se pudo obtener la TDC enmascarada: " +e.getMessage());
		}
		return ultimos4TDC;
	}
	// fin MLS imprimir numero de tarjeta en pdf
	
	public String tipoTDC(String encryptedTDC){
		String tipoTDC = "NI";
		TBitacoraDao tBitacoraDao = null;
		List<CatalogoBinVO> bines = null;		
		try{
			tBitacoraDao = (TBitacoraDao) getBean("TBitacoraDao");
			
			SubStrTarjetasVO subStr = new SubStrTarjetasVO();
			
			String TDC = AddcelCrypto.decryptTarjeta(encryptedTDC);
			
			subStr.setSubStr6(TDC.substring(0, 6));
			subStr.setSubStr7(TDC.substring(0, 7));
			subStr.setSubStr8(TDC.substring(0, 8));
			subStr.setSubStr9(TDC.substring(0, 9));
			subStr.setSubStr10(TDC.substring(0, 10));
			
			bines = tBitacoraDao.getTipoTarjeta(subStr);
			if(bines!=null && bines.size()>0){
				tipoTDC = bines.get(0).getTipo();					
			}else{
				CatalogoBinVO binDesconocido = new CatalogoBinVO(subStr.getSubStr6(),"NI");
				int resultado = tBitacoraDao.insertCatalogoBin(binDesconocido);
				logger.info("No se encontro bin, añadiendolo: " + resultado);
			}
			
		} catch(Exception e){
			logger.error("No se pudo obtener el tipo de TDC: " +e.getMessage());
		}
		return tipoTDC;
	}
}
