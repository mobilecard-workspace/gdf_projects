package com.addcel.gdf.service;

import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.ComunicadosDao;
import com.addcel.gdf.model.dao.TBitacoraDao;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.vo.comunicados.ListaComunicadosVO;
import com.addcel.utils.ibatis.service.AbstractService;

public class TBitacoraService extends AbstractService {
	public void pruebaTbitacora(){
		
		TBitacoraVO bitacora = new TBitacoraVO();
		bitacora.setId_bitacora(110241);
		bitacora.setId_producto(1000);
		bitacora.setId_proveedor(2);
		bitacora.setId_usuario(1234);
//		bitacora.setBit_cargo(200.13);
		bitacora.setBit_codigo_error(1);
		bitacora.setBit_concepto("904SFG");
//		bitacora.setBit_no_autorizacion(1);
		bitacora.setBit_status(10);
		bitacora.setBit_ticket("Pago exitoso de Tenencia. Placas: 	904sfg, Periodo: 2013");
		bitacora.setImei("77A5E1F3-CB91-479B-A95F-CC88C4FB395B");
		bitacora.setDestino("12");
		bitacora.setTarjeta_compra("eknoinjnu231d3yb");
		bitacora.setTipo("Phone 4 GSM");
		bitacora.setSoftware("6.1.3");
		bitacora.setModelo("iPhone");
		bitacora.setWkey("77A5E1F3-CB91-479B-A95F-CC88C4FB395B");
		try{
			insertTBitacora(bitacora);
		}catch(Exception e){
			
		}
		
		
	}
	private static Logger log = Logger.getLogger(TBitacoraService.class);

	public long insertTBitacora(TBitacoraVO bitacora) throws Exception {
		int idBitacora = 0; 
		TBitacoraDao dao = null;
		try {
			dao = (TBitacoraDao) getBean("TBitacoraDao");
			idBitacora = dao.insertTBitacora(bitacora);
			
		} catch (Exception e) {
			log.error("Ocurrio un error en TBitacoraService.insertTBitacora." , e);
			throw new Exception(e);
		}
		return idBitacora;
	}
	

	public int updateTBitacora(TBitacoraVO bitacora) throws Exception {
		int idBitacora = 0; 
		TBitacoraDao dao = null;
		try {
			dao = (TBitacoraDao) getBean("TBitacoraDao");
			idBitacora = dao.updateTBitacora(bitacora);
			
		} catch (Exception e) {
			log.error("Ocurrio un error en TBitacoraService.updateTBitacora." , e);
			throw new Exception(e);
		}
		return idBitacora;
	}
	
	public String getFechaActual() throws Exception {
		TBitacoraDao dao = null;
		String fecha = null;
		try {
			dao = (TBitacoraDao) getBean("TBitacoraDao");
			fecha = dao.getFechaActual();
			
		} catch (Exception e) {
			log.error("Ocurrio un error en TBitacoraService.getFechaActual." , e);
			throw new Exception(e);
		}
		return fecha;
	}
	
	public int difFechaMin(String fechaToken) throws Exception {
		int seg = 0; 
		TBitacoraDao dao = null;
		try {
			dao = (TBitacoraDao) getBean("TBitacoraDao");
			seg = dao.difFechaMin(fechaToken);
			
		} catch (Exception e) {
			log.error("Ocurrio un error en TBitacoraService.difFechaMin." , e);
			throw new Exception(e);
		}
		return seg;
	}
	
	
}
