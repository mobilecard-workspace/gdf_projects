package com.addcel.gdf.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.AguaDao;
import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.ProsaPagoVO;
import com.addcel.gdf.vo.agua.AguaPreguntaFutVO;
import com.addcel.gdf.vo.agua.AguaRespuestaGuardaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.ws.clientes.agua.FutWSDLPortTypeProxy;
import com.addcel.gdf.ws.clientes.agua.InputDatosConsulta;
import com.addcel.gdf.ws.clientes.agua.InputFutVigente;
import com.addcel.gdf.ws.clientes.agua.OutputDatosFut;
import com.addcel.gdf.ws.clientes.agua.OutputDatosVigente;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.Utilerias;
import com.addcel.utils.ibatis.service.AbstractService;
import com.addcel.utils.json.me.JSONObject;
import com.google.gson.Gson;

public class AguaService extends AbstractService implements Dependency{
	private static Logger log = Logger.getLogger(AguaService.class);

	@Override
	public String consumeWS(String json) {
		FutWSDLPortTypeProxy futWSDLPortTypeProxy = null;
		AguaPreguntaFutVO aguaPreguntaFutVO = null;
		AguaRespuestaGuardaVO aguaRespuestaGuardaVO = null;
		
		InputDatosConsulta inputDatosConsulta = null;
		OutputDatosFut tipo_respuesta = null;
		
		InputFutVigente inputFut = null;
		OutputDatosVigente outputDatosFormato = null;
//		Bimestre[] bimestres = new Bimestre[1];
//		bimestres[0] = new Bimestre();
		
		Gson gson = new Gson();

		try {

			aguaPreguntaFutVO = gson.fromJson(json, AguaPreguntaFutVO.class);

			ProcomService ps = new ProcomService();
			int productoId = 5;
			BigDecimal monto_max = new BigDecimal(ps.montoMaxProducto(new Integer(productoId)));
			log.info("monto max agua :" + monto_max.toString());

			futWSDLPortTypeProxy = new FutWSDLPortTypeProxy();
	
			inputDatosConsulta = new InputDatosConsulta();
			inputDatosConsulta.setCuenta(aguaPreguntaFutVO.getCuenta());
			inputDatosConsulta.setTipo("VIG");
			inputDatosConsulta.setId_aplicacion("10002");
			inputDatosConsulta.setPasswd("07yuKU1PFbsKlqUYTdAM");

			tipo_respuesta = futWSDLPortTypeProxy.obtenerFut(inputDatosConsulta);
			log.info("tipo_respuesta.getError: " + tipo_respuesta.getError() );
			json = null;

			if(tipo_respuesta != null && tipo_respuesta.getError() != null){
				json = "{\"error\":\""+ tipo_respuesta.getError()  + "\",\"numError\":\"C0000\"}";
			}else if(tipo_respuesta != null && tipo_respuesta.getFut() == null){
					json = "{\"error\":\"No existen adeudos vigentes por pagar.\",\"numError\":\"C0001\"}";
			}else if(tipo_respuesta != null && tipo_respuesta.getError() == null 
					&& tipo_respuesta.getDatosGenerales().length > 0){
				log.info("Existen datos para la cuenta: " + aguaPreguntaFutVO.getCuenta() +
						"\t\tFolio: " +tipo_respuesta.getFolio() );
				
//				log.info("tipo_respuesta.getFut()[0].getVanio(): " + (tipo_respuesta.getFut() != null ?tipo_respuesta.getFut()[0].getVanio():"getFut :  null") );
//				bimestres[0].setAnio( tipo_respuesta.getFut()[0].getVanio() );
//				bimestres[0].setBimestre( tipo_respuesta.getFut()[0].getVbimestre() );
				
				
				inputFut = new InputFutVigente();
				inputFut.setCuenta(aguaPreguntaFutVO.getCuenta());
				inputFut.setFolio_control(Integer.parseInt(tipo_respuesta.getFolio()));
				inputFut.setAnio(tipo_respuesta.getFut()[0].getVanio());
				inputFut.setBimestre(tipo_respuesta.getFut()[0].getVbimestre());
				inputFut.setFolio(tipo_respuesta.getDatosGenerales()[0].getFolio());
				inputFut.setPrograma("0");
				inputFut.setUso(tipo_respuesta.getFut()[0].getVuso());
				inputFut.setEmpresa(empresa.get(tipo_respuesta.getDatosGenerales()[0].getCod_pos().substring(0, 2)));
				inputFut.setIp("192.168.112.210");
				inputFut.setId_aplicacion("10002");
				inputFut.setPasswd("07yuKU1PFbsKlqUYTdAM");
				
				outputDatosFormato = futWSDLPortTypeProxy.guardarFutVigente(inputFut);
				log.info("outputDatosFormato.getError(): " + outputDatosFormato.getError());
				if(outputDatosFormato != null && outputDatosFormato.getError() != null){
					json = "{\"error\":\""+ outputDatosFormato.getError()  + "\",\"numError\":\"C0002\"}";
				}else if(outputDatosFormato != null && outputDatosFormato.getLc() != null){
					aguaRespuestaGuardaVO = new AguaRespuestaGuardaVO();
					aguaRespuestaGuardaVO.setVanio(outputDatosFormato.getVanio());
					aguaRespuestaGuardaVO.setVbimestre(outputDatosFormato.getVbimestre());
					aguaRespuestaGuardaVO.setVderdom(Utilerias.formatoImporte(outputDatosFormato.getVderdom()));
					aguaRespuestaGuardaVO.setVderndom(Utilerias.formatoImporte(outputDatosFormato.getVderndom()));
					aguaRespuestaGuardaVO.setVuso(outputDatosFormato.getVuso());
					aguaRespuestaGuardaVO.setViva(Utilerias.formatoImporte(outputDatosFormato.getViva()));
					aguaRespuestaGuardaVO.setVtotal(Utilerias.formatoImporte(outputDatosFormato.getVtotal()));
					aguaRespuestaGuardaVO.setLinea_captura(outputDatosFormato.getLc());
					aguaRespuestaGuardaVO.setCuenta(outputDatosFormato.getCuenta());
					aguaRespuestaGuardaVO.setVigencia(outputDatosFormato.getVigencia());
					aguaRespuestaGuardaVO.setVderecho(Utilerias.formatoImporte(
							Double.parseDouble( outputDatosFormato.getVderdom()) +
							Double.parseDouble( outputDatosFormato.getVderndom())
							));
					aguaRespuestaGuardaVO.setTotalPago(aguaRespuestaGuardaVO.getVtotal());
					aguaRespuestaGuardaVO.setError("");
					aguaRespuestaGuardaVO.setNumError("0");
					aguaRespuestaGuardaVO.setMonto_maximo_operacion(monto_max);
					json = gson.toJson(aguaRespuestaGuardaVO);
				}
			}
			if(json == null){
				json = "{\"error\":\"Ocurrio un error en la obtencion de la Linea de Captura\",\"numError\":\"C0003\"}";
			}

//			aguaRespuestaGuardaVO = new AguaRespuestaGuardaVO();
//			aguaRespuestaGuardaVO.setVanio("2013");
//			aguaRespuestaGuardaVO.setVbimestre("03");
//			aguaRespuestaGuardaVO.setVderecho(Utilerias.formatoImporte("200"));
//			aguaRespuestaGuardaVO.setVderdom(Utilerias.formatoImporte("200"));
//			aguaRespuestaGuardaVO.setVderndom(Utilerias.formatoImporte("0"));
//			aguaRespuestaGuardaVO.setVuso("0");
//			aguaRespuestaGuardaVO.setViva(Utilerias.formatoImporte("32.00"));
//			aguaRespuestaGuardaVO.setVtotal(Utilerias.formatoImporte("100"));
//			aguaRespuestaGuardaVO.setLinea_captura("80033109120029J9AN0N");
//			aguaRespuestaGuardaVO.setCuenta("1234567890123456");
//			aguaRespuestaGuardaVO.setVigencia("2013-09-01");
//			aguaRespuestaGuardaVO.setTotalPago(Utilerias.formatoImporte("100"));
		
//			json = gson.toJson(aguaRespuestaGuardaVO);
			
//		} catch (AxisFault aF) {
//			json = "{\"error\":\"1\",\"numError\":\"A3001\"}";
//
//		} catch (RemoteException rE) {
//			json = "{\"error\":\"1\",\"numError\":\"A2001\"}";
			
		} catch (Exception e) {
			log.error("Ocurrio un error al consultar WS de Agua. ", e);
			if (aguaRespuestaGuardaVO == null) {
				json = "{\"error\":\""+ e.getMessage() + "\",\"numError\":\"A0001\"}";
			} else {
				json = "{\"error\":\"1\",\"numError\":\"Z1001\"}";
			}
		}
		return json;
	}
	
	@Override
	public HashMap<String, Object> consume3DSecure(String json) throws Exception {
		TBitacoraService tbitacoraService = new TBitacoraService();
		TBitacoraProsaService tbitacoraProsaService = new TBitacoraProsaService();
		AguaRespuestaGuardaVO aguaRespuestaGuardaVO = null;
		TBitacoraVO tbitacoraVO = null;
		TBitacoraProsaVO tbitacoraProsaVO = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Gson gson = new Gson();
		long id_bitacora = 0;
		try {
			
			aguaRespuestaGuardaVO = gson.fromJson(json, AguaRespuestaGuardaVO.class);
			tbitacoraVO = gson.fromJson(json, TBitacoraVO.class);
			tbitacoraProsaVO = gson.fromJson(json, TBitacoraProsaVO.class);
			
			tbitacoraVO.setId_proveedor(16);
			tbitacoraVO.setBit_status(0);
			tbitacoraVO.setBit_codigo_error(0);
			tbitacoraVO.setBit_cargo(aguaRespuestaGuardaVO.getTotalPago());
			tbitacoraVO.setDestino(aguaRespuestaGuardaVO.getLinea_captura());
			
			id_bitacora = tbitacoraService.insertTBitacora(tbitacoraVO);
			if(id_bitacora > 0){
				tbitacoraVO.setId_bitacora(id_bitacora);
				aguaRespuestaGuardaVO.setId_bitacora(id_bitacora);
				aguaRespuestaGuardaVO.setError("0");
				tbitacoraProsaVO.setId_bitacora(new Long(id_bitacora));
				tbitacoraProsaService.insertTBitacora(tbitacoraProsaVO);
				insertBitacoraAgua(aguaRespuestaGuardaVO);
			}
			aguaRespuestaGuardaVO.setId_producto(ConstantesGDF.idProd_Agua);
			resp.put("tbitacoraVO", tbitacoraVO);
			resp.put("guardaVO", aguaRespuestaGuardaVO);
			
			log.info("Exito AguaService.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora());
			
		} catch (Exception e) {
			log.error("Error AguaService.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora(), e);
			throw new Exception(e);
		}
		return resp;
	}
	
	public int insertBitacoraAgua(AguaRespuestaGuardaVO bitacora) throws Exception{
		AguaDao dao = null;
		int idAgua = 0;
		try {
			dao = (AguaDao) getBean("AguaDao");
			dao.insertGDFAgua(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al insert Detalle GDF Agua ." , e);
			throw new Exception(e);
		}
		
		return idAgua;
	}
	
	public int updateBitacoraAgua(AguaRespuestaGuardaVO bitacora) throws Exception{
		AguaDao dao = null;
		int idAgua = 0;
		try {
			dao = (AguaDao) getBean("AguaDao");
			dao.updateGDFAgua(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Agua ." , e);
			throw new Exception(e);
		}
		
		return idAgua;
	}

	@Override
	public int updateBitacoraDetalle(ProsaPagoVO prosaPagoVO, long idBitacora, int status, String tipoTDC) throws Exception{
		AguaDao dao = null;
		AguaRespuestaGuardaVO bitacora = null;
		int idAgua = 0;
		try {
			bitacora = new AguaRespuestaGuardaVO();
			
			bitacora.setId_bitacora(idBitacora);
			bitacora.setNo_autorizacion(prosaPagoVO.getAutorizacion());
//			bitacora.setError(transactionProcomVO.getError());
//			bitacora.setDescError(transactionProcomVO.getDescError());
			bitacora.setStatusPago(status);
			bitacora.setTipoTarjeta(tipoTDC);
			dao = (AguaDao) getBean("AguaDao");
			dao.updateGDFAgua(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Agua ." , e);
			throw new Exception(e);
		}
		
		return idAgua;
	}
	public String consumeConsultas(String json) throws Exception{
		HashMap<String, String> consulta = new HashMap<String, String>();
//		AguaDao dao = null;
		Gson gson = new Gson();
		try {
			JSONObject jsObject = new JSONObject(json);
			
			if (jsObject.has("id_usuario")){
				consulta.put("id_usuario",jsObject.getString("id_usuario"));
				log.info("id_usuario: " + jsObject.getString("id_usuario") );
			}
			if (jsObject.has("mes")){
				consulta.put("mes",jsObject.getString("mes"));
				log.info("mes: " + jsObject.getString("mes") );
			}
			if (jsObject.has("anio")){
				consulta.put("anio",jsObject.getString("anio"));
				log.info("anio: " + jsObject.getString("anio") );
			}
			
//			dao = (AguaDao) getBean("AguaDao");
			
//			AguaRespuestaGuardaVO aguaRespuestaGuardaVO = new AguaRespuestaGuardaVO();
//			aguaRespuestaGuardaVO = crearObjeto();
			
//			dao.insertGDFAgua(aguaRespuestaGuardaVO);
			
//			dao.updateGDFAgua(aguaRespuestaGuardaVO);
			
			json = "{\"consultaAgua\":" + gson.toJson(getDetallePagos(consulta)) + "}";
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Comunicados Generales." , e);
			throw new Exception(e);
		}
		
		return json;
	}
	
	public List<AguaRespuestaGuardaVO> getDetallePagos(HashMap<String, String> consulta) throws Exception{
		List<AguaRespuestaGuardaVO> listaAgua = new ArrayList<AguaRespuestaGuardaVO>();
		AguaDao dao = null;
		try {
			dao = (AguaDao) getBean("AguaDao");
			
			listaAgua = dao.selectGDFAgua(consulta);
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los AguaService.getDetallePagos." , e);
			throw new Exception(e);
		}
		
		return listaAgua;
	}
	
	@Override
	public AbstractVO getDetallePagoProcom(HashMap<String, String> consulta) throws Exception{
		List<AguaRespuestaGuardaVO> listaAgua = new ArrayList<AguaRespuestaGuardaVO>();
		AbstractVO abstractVO = null;
		try {
			listaAgua = getDetallePagos(consulta);
			
			if(listaAgua.size() > 0){
				abstractVO = (AbstractVO) listaAgua.get(0);
			}
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los AguaService.getDetallePagoProcom." , e);
			throw new Exception(e);
		}
		
		return abstractVO;
	}
	
	private static AguaRespuestaGuardaVO crearObjeto(){
		AguaRespuestaGuardaVO aguaRespuestaGuardaVO = new AguaRespuestaGuardaVO();
		aguaRespuestaGuardaVO.setId_bitacora(110102);
		aguaRespuestaGuardaVO.setId_usuario(13709053);
		aguaRespuestaGuardaVO.setNo_autorizacion("098001");
		aguaRespuestaGuardaVO.setVanio("2013");
		aguaRespuestaGuardaVO.setVbimestre("01");
		aguaRespuestaGuardaVO.setVderdom("12346");
		aguaRespuestaGuardaVO.setVderndom("67891");
		aguaRespuestaGuardaVO.setVuso("0");
		aguaRespuestaGuardaVO.setViva("16.0");
		aguaRespuestaGuardaVO.setVtotal("2450");
		aguaRespuestaGuardaVO.setLinea_captura("5110012ABC2002J9AEFG");
		aguaRespuestaGuardaVO.setCuenta("1234567890123456");
		aguaRespuestaGuardaVO.setVigencia("2013-09-01");
		aguaRespuestaGuardaVO.setError("0");
		aguaRespuestaGuardaVO.setStatusPago(1);
		return aguaRespuestaGuardaVO; 
	}
	
	
	private static final HashMap<String,String> empresa = new HashMap<String,String>();
	
	static {
		empresa.put("01","4"); 
		empresa.put("02","1");
		empresa.put("03","2");
		empresa.put("04","2"); 
		empresa.put("05","2");
		empresa.put("06","1");
		empresa.put("07","1"); 
		empresa.put("08","2");
		empresa.put("09","3");
		empresa.put("10","4");
		empresa.put("11","4");
		empresa.put("12","3");
		empresa.put("13","3");
		empresa.put("14","4");
		empresa.put("15","2"); 
		empresa.put("16","3");
    }


}
