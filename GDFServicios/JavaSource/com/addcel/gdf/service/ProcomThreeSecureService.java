package com.addcel.gdf.service;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.addcel.gdf.vo.ProcomProsa.ProcomVO;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.json.me.JSONObject;

public class ProcomThreeSecureService {
	private static final Logger logger=Logger.getLogger(ProcomThreeSecureService.class);
	
		
	public  ProcomVO inicioProceso(String json) {    	
		logger.debug("Procesando peticion");
        int index = 0;
        HashMap<String, String> datosProcom = new HashMap<String, String>();
        try{
        
			JSONObject jsObject = new JSONObject(json);
	        
			if (jsObject.has("id_producto")){
				index = Integer.parseInt(jsObject.getString("id_producto"));
				Dependency dependency = null;
				switch (index) {
				case ConstantesGDF.idProd_Nomina:
					dependency = new NominaService();
					break;
	
				case ConstantesGDF.idProd_Tenencia:
					dependency = new TenenciaServices();
					break;
					
				case ConstantesGDF.idProd_Infraccion:
					dependency = new InfraccionService();
					break;
					
				case ConstantesGDF.idProd_Predial:
					dependency = new PredialServices();
					break;
				
				case ConstantesGDF.idProd_Agua:
					dependency = new AguaService();
					break;
					
				default:
					break;
				}
//				datosProcom = dependency.consume3DSecure(json);
				
			} else {
	//			throw new Exception("Falta el parametro id_producto.");
				// no existe el parametro id_producto
			}
        }catch(Exception e){
        	logger.error("Ocurrio un error al ProcomThreeSecureService.inicioProceso: " , e);
        }
        	
        return new ProcomService().comercioFin( //String user,String referencia,String monto,String idTramite
    			datosProcom.get("id_user"), datosProcom.get("referencia") , datosProcom.get("monto"), String.valueOf(index));
    }
	
}
