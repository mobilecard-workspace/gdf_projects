package com.addcel.gdf.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.ProsaPagoVO;
import com.addcel.gdf.vo.TProveedorVO;
import com.addcel.gdf.vo.UsuarioVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.ws.clientes.finanzas.Tipo_respuesta;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.Utilerias;
import com.addcel.utils.json.me.JSONObject;
import com.google.gson.Gson;

public class PagoProsaService {
	private static final Logger logger=Logger.getLogger(PagoProsaService.class);
	
	//QA
//	private static final String URL_AUT_PROSA = "http://localhost:8080/ProsaWeb/ProsaAuth?";
//	private static final String URL_MAIL = "http://localhost:8080/MailSenderAddcel/envia-recibo-gdf/";
	
	//PROD
	private static final String URL_AUT_PROSA = "http://localhost:8080/ProsaWeb/ProsaAuth?";
	private static final String URL_MAIL = "http://50.57.192.214:8080/MailSenderAddcel/envia-recibo-gdf/";
	private static final String lTime = "22:57:00";
	private static final String uTime = "23:48:00";
	private static final String dayFlag = "0";
		
	public  String inicioProceso(String json) {    	
		logger.debug("Procesando peticion Pago Visa...");
        HashMap<String, Object> resBitacora = new HashMap<String, Object>();
        AbstractVO abstractVO = null;
		TBitacoraVO tbitacoraVO = null;
		
		ProcomService ps = new ProcomService();
		TBitacoraService tbSer = new TBitacoraService();
		UsuarioVO usuario = null;
		TProveedorVO proveedor = null;
		Gson gson = new Gson();
		
        try{
        	if(validMaintenanceHours(lTime,uTime,dayFlag)){
    			json = "{\"numError\":1,\"error\":\"El sistema de pagos se encuentra en mantenimiento, intente después de las: " + uTime + " Hrs.\"}";
    			logger.error("Dentro de periodo de mantenimiento");
        	} else {
        		abstractVO = gson.fromJson(json, AbstractVO.class);
        		if(abstractVO.getToken() == null){
        			json = "{\"numError\":1,\"error\":\"El parametro TOKEN no puede ser NULL\"}";
        			logger.error("El parametro TOKEN no puede ser NULL");
        		}else{
        			abstractVO.setToken(AddcelCrypto.decryptSensitive(abstractVO.getToken()));
        			logger.info("token ==> " + abstractVO.getToken());
    			
        			if((tbSer.difFechaMin(abstractVO.getToken())) > 30){
        				json = "{\"numError\":2,\"error\":\"La Transaccion no es valida\"}";
        				logger.info("La Transacción no es válida");
					
        			}else{
        				abstractVO.setPassword(AddcelCrypto.encryptPassword(abstractVO.getPassword()));
        				usuario = ps.selectUsuario(String.valueOf(abstractVO.getId_usuario()));
    				
        				// inicio MLS Cambiar validación de tope de transacción por producto en vez de por usuario 
        				double total = Double.parseDouble(ps.montoMaxProducto((new Integer(abstractVO.getId_producto()))));
        				// fin MLS Cambiar validación de tope de transacción por producto en vez de por usuario
        				double totalPago = Double.parseDouble(abstractVO.getTotalPago());
        				if(totalPago > total){
        					json = "{\"numError\":12,\"error\":\"El limite maximo para una transaccion es $" + Utilerias.formatoImporte(total) + ".\"}";
        					logger.error("El limite maximo para una transaccion es: " + total);
        				}else if(usuario == null){
        					json = "{\"numError\":10,\"error\":\"El usuario no Existe.\"}";
        					logger.error("El usuario no Existe., id_user: " + abstractVO.getId_usuario());
        				}else if(!usuario.getUsrPwd().equals(abstractVO.getPassword())){
        					json = "{\"numError\":11,\"error\":\"Password incorrecto.\"}";
        					logger.error("El password es incorrecto, id_user: " + abstractVO.getId_usuario());
        				}else{
        					resBitacora = insertaBitcoras(json);
			        	
        					if(resBitacora.containsKey("tbitacoraVO")){
        						tbitacoraVO = (TBitacoraVO) resBitacora.get("tbitacoraVO");
        					}
        					if(resBitacora.containsKey("guardaVO")){
        						abstractVO = (AbstractVO) resBitacora.get("guardaVO");
        					}
        					String afiliacion = ps.getAfiliacion();
        					json = swichAbierto(tbitacoraVO , abstractVO  , usuario, afiliacion );
        				}
        			}
        		}
        	}        	
        }catch(Exception e){
        	logger.error("Ocurrio un error al PagoProsaService.inicioProceso: " , e);
        	json = "{\"numError\":100,\"error\":\"Ocurrio un error al realizar el pago :"+e.getMessage()+" \"}";
        }
        	
        return json;
    }
	
	private HashMap<String, Object> insertaBitcoras(String json)throws Exception{
        int index = 0;
        HashMap<String, Object> resp = new HashMap<String, Object>();
		try{
			JSONObject jsObject = new JSONObject(json);
			
			if (jsObject.has("id_producto")){
				index = Integer.parseInt(jsObject.getString("id_producto"));
				logger.info("id_producto: " + index);
				Dependency dependency = null;
				switch (index) {
				case ConstantesGDF.idProd_Nomina:
					dependency = new NominaService();
					break;

				case ConstantesGDF.idProd_Tenencia:
					dependency = new TenenciaServices();
					break;
					
				case ConstantesGDF.idProd_Infraccion:
					dependency = new InfraccionService();
					break;
					
				case ConstantesGDF.idProd_Predial:
					dependency = new PredialServices();
					break;
				
				case ConstantesGDF.idProd_Agua:
					dependency = new AguaService();
					break;
					
				case ConstantesGDF.idProd_PredialVencido:
					dependency = new PredialVencidoServices();
					break;
					
				case ConstantesGDF.ID_PRODUCTO_TARJETA_CIRCULACION:
					dependency = new TarjetaCirculacionServices();
					break;
					
				case ConstantesGDF.ID_PRODUCTO_LICENCIA_CONDUCTOR:
					dependency = new LicenciaConductorServices();
					break;
				
				case ConstantesGDF.ID_PRODUCTO_LICENCIA_PERMANENTE:
					dependency = new LicenciaPermanenteServices();
					break;
					
				default:
					logger.info("No se encontro un id_producto valido.");
					break;
				}
				resp = dependency.consume3DSecure(json);
			} else {
				//			throw new Exception("Falta el parametro id_producto.");
							// no existe el parametro id_producto
			}
		}catch(Exception e){
			logger.error("Ocurrio un error al PagoProsaService.insertaBitcoras: " , e);
			throw new Exception(e);
		}
		
		return resp;
		
	}
	
	private String swichAbierto(TBitacoraVO tbitacoraVO , AbstractVO abstractVO  , UsuarioVO usuario, String afiliacion ){
		ProsaPagoVO prosaPagoVO = null;
		String json = null;
		StringBuffer paramEmail = null;
		Gson gson = new Gson();
		String jsonObject = null;
		ProcomService ps = new ProcomService();
		// Variable para guardar el status de confirmación de GDF: 1 Exitoso, 3 Pendiente
		int statusExito = 1;
		String tipoTDC = "NI";
		
		try{
	
			
			StringBuffer data = new StringBuffer() 
					.append( "card="      ).append( URLEncoder.encode(AddcelCrypto.decryptTarjeta(usuario.getUsrTdcNumero()), "UTF-8") )
					.append( "&vigencia=" ).append( URLEncoder.encode(usuario.getUsrTdcVigencia(), "UTF-8") )
					.append( "&nombre="   ).append( URLEncoder.encode(usuario.getUsrNombre() + " " + usuario.getUsrApellido(), "UTF-8") )
					.append( "&cvv2="     ).append( URLEncoder.encode(abstractVO.getCvv2(), "UTF-8") )
				    .append( "&monto="    ).append( URLEncoder.encode(abstractVO.getTotalPago() + "", "UTF-8") )
					//.append( "&monto="    ).append( URLEncoder.encode("1.00" + "", "UTF-8") )
					.append( "&afiliacion=" ).append( URLEncoder.encode(afiliacion, "UTF-8") );
//					.append( "&moneda="   ).append( URLEncoder.encode(datosPago.getMoneda(), "UTF-8")) ;
					
			logger.info("Envío de datos VISA: " + data);
			tipoTDC = ps.tipoTDC(usuario.getUsrTdcNumero());
			
			URL url = new URL(URL_AUT_PROSA);
			URLConnection urlConnection = url.openConnection();
			
			urlConnection.setDoOutput(true);
			
			OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
			wr.write(data.toString());
			wr.flush();
			logger.info("Datos enviados, esperando respuesta de PROSA");
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			StringBuilder sb = new StringBuilder();
			
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			
			wr.close();
			rd.close();
			
			prosaPagoVO = (ProsaPagoVO) gson.fromJson(sb.toString(), ProsaPagoVO.class);
			
			if(prosaPagoVO != null){
				
				if(prosaPagoVO.isAuthorized()){
					prosaPagoVO.setStatus(1);
					prosaPagoVO.setMsg("EXITO PAGO " + descIdProduto(abstractVO.getId_producto()) + " VISA AUTORIZADA");
					json = "{\"numError\":0,\"error\":\"El pago fue exitoso. El comprobante ha sido enviado al correo electrónico registrado.\",\"transaccion\":\"" + prosaPagoVO.getTransactionId() + 
							"\",\"autorizacion\":\"" + prosaPagoVO.getAutorizacion() +
							"\",\"referencia\":" + abstractVO.getId_bitacora() + "}";
					Tipo_respuesta resp = null;
					try{
		        		 resp = ps.aprovisionamientoGdf(
		        				 abstractVO.getLinea_captura(), prosaPagoVO.getAutorizacion(), 
		        				 new BigDecimal(abstractVO.getTotalPago()));
	        		}catch(Exception e){
	        			 logger.error("Error en Aprovisionamiento GDF: " + abstractVO.getId_bitacora(), e);   
	        			 resp = new Tipo_respuesta();
	        			 resp.setError(new BigDecimal(3));
	        			 resp.setError_descripcion(e.getMessage().length() > 50?e.getMessage().substring(0, 59):e.getMessage());
	        			 statusExito = 2; // 2
	        		}
	        		if(resp!= null){	        			
	        			if(resp.getError().intValue() == 0){
	        			 statusExito = 1;
	        			}else{
	        				statusExito = 2; //2 
	        			}
	        		}else
	        			statusExito = 2; //2
	        			
					logger.info("statusExito : " + statusExito);
					updateBitacoraDetalle(prosaPagoVO, tbitacoraVO.getId_bitacora(), abstractVO.getId_producto(), statusExito, tipoTDC);
					updateBitacoras(tbitacoraVO, prosaPagoVO, abstractVO.getId_producto(),usuario.getUsrTdcNumero(), tipoTDC);
					try{
						// MLS imprimir numero de tarjeta en pdf
						jsonObject = ps.jsonObject(  abstractVO.getId_bitacora(),(int)abstractVO.getId_producto(), ps.getUltimos4TDC(usuario.getUsrTdcNumero()));
					
						if(usuario.getEmail() != null){
							paramEmail = new StringBuffer()
								.append("correo=").append(usuario.getEmail())
								.append("&json=").append(jsonObject);
	        			 
							logger.info("Invio de Emai Parametros: " + paramEmail);
							ps.peticionUrlPostParams( URL_MAIL + abstractVO.getId_producto(), paramEmail.toString()); 
						}else{
							logger.error("ERROR imposible enviar email: el correo es nulo");
						}
					} catch(Exception e1){
						logger.error("ERROR imposible enviar email: "+ e1.getMessage()); 
					}					
				}else if(prosaPagoVO.isRejected()){
					prosaPagoVO.setStatus(0);
					prosaPagoVO.setMsg("PAGO " + descIdProduto(abstractVO.getId_producto()) + " VISA RECHAZADA");
					json = "{\"numError\":4,\"error\":\"El pago fue rechazado.\",\"referencia\":" + prosaPagoVO.getTransactionId() + "}";
					updateBitacoras(tbitacoraVO, prosaPagoVO, abstractVO.getId_producto(),usuario.getUsrTdcNumero(), tipoTDC);
					
				}else if(prosaPagoVO.isProsaError()){
					prosaPagoVO.setStatus(0);
					prosaPagoVO.setMsg("PAGO " + descIdProduto(abstractVO.getId_producto()) + " VISA ERROR");
					json = "{\"numError\":5,\"error\":\"Ocurrio un error durante el pago Banco.\",\"referencia\":" + prosaPagoVO.getTransactionId() + "}";
					updateBitacoras(tbitacoraVO, prosaPagoVO, abstractVO.getId_producto(),usuario.getUsrTdcNumero(), tipoTDC);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Ocurrio un error durante el pago al banco: {}", e);
			prosaPagoVO.setStatus(0);
			prosaPagoVO.setMsg("Ocurrio un error durante el pago al banco");
			json = "{\"numError\":6,\"error\":\"Ocurrio un error durante el pago.\"}";
			updateBitacoras(tbitacoraVO, prosaPagoVO, abstractVO.getId_producto(),usuario.getUsrTdcNumero(),tipoTDC);
		}
		return json;
	}
		
	private void updateBitacoras(TBitacoraVO tbitacoraVO, ProsaPagoVO prosaPagoVO, int id_producto, String tarjeta, String tipoTDC){
		TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
		try{
			updateBitacoraDetalle(prosaPagoVO, tbitacoraVO.getId_bitacora(), id_producto, prosaPagoVO.getStatus(),tipoTDC);
			
        	logger.info("Inicio Update TBitacora.");
        	tbitacoraVO.setBit_concepto(prosaPagoVO.getMsg());
        	tbitacoraVO.setBit_ticket(prosaPagoVO.getMsg());
        	tbitacoraVO.setBit_no_autorizacion(prosaPagoVO.getAutorizacion());
        	tbitacoraVO.setBit_status(prosaPagoVO.getStatus());
        	tbitacoraVO.setTarjeta_compra(tarjeta);
        	tbitacoraVO.setBit_codigo_error(prosaPagoVO.getError());
        	tbitacoraVO.setDestino("-"+tipoTDC);
        	
        	TBitacoraService tbitacoraService = new TBitacoraService();
        	tbitacoraService.updateTBitacora(tbitacoraVO);
        	logger.info("Fin Update TBitacora.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacora: ", e);
        }
        
        try{
        	logger.info("Inicio Update TBitacoraProsa.");
        	tbitacoraProsaVO.setId_bitacora(tbitacoraVO.getId_bitacora());
        	tbitacoraProsaVO.setAutorizacion(prosaPagoVO.getAutorizacion());
        	
        	TBitacoraProsaService tbitacoraProsaService = new TBitacoraProsaService();
        	tbitacoraProsaService.updateTBitacoraProsa(tbitacoraProsaVO);
        	logger.info("Fin Update TBitacora.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacoraProsa: ", e);
        }
	}
	
	private int updateBitacoraDetalle(ProsaPagoVO prosaPagoVO, long idBitacora, int id_producto, int status, String tipoTDC) throws Exception {
		Dependency dependency = null;
		int respuesta = 0; 
		
		switch (id_producto) {
			case ConstantesGDF.idProd_Nomina:
				dependency = new NominaService();
				break;
	
			case ConstantesGDF.idProd_Tenencia:
				dependency = new TenenciaServices();
				break;
				
			case ConstantesGDF.idProd_Infraccion:
				dependency = new InfraccionService();
				break;
				
			case ConstantesGDF.idProd_Predial:
				dependency = new PredialServices();
				break;
			
			case ConstantesGDF.idProd_Agua:
				dependency = new AguaService();
				break;
			case ConstantesGDF.idProd_PredialVencido:
				dependency = new PredialVencidoServices();
				break;
			case ConstantesGDF.ID_PRODUCTO_TARJETA_CIRCULACION:
				dependency = new TarjetaCirculacionServices();
				break;
				
			case ConstantesGDF.ID_PRODUCTO_LICENCIA_CONDUCTOR:
				dependency = new LicenciaConductorServices();
				break;
			
			case ConstantesGDF.ID_PRODUCTO_LICENCIA_PERMANENTE:
				dependency = new LicenciaPermanenteServices();
				break;
			default:
				break;
		}
		
		respuesta = dependency.updateBitacoraDetalle(prosaPagoVO, idBitacora, status, tipoTDC);
		
		return respuesta;
	}
	
	private String descIdProduto(int id_producto){
    	String desc = "";
    	switch (id_producto) {
			case ConstantesGDF.idProd_Nomina:
				desc = "Nomina";
				break;
	
			case ConstantesGDF.idProd_Tenencia:
				desc = "Tenencia";
				break;
				
			case ConstantesGDF.idProd_Infraccion:
				desc = "Infraccion";
				break;
				
			case ConstantesGDF.idProd_Predial:
				desc = "Predial";
				break;
			
			case ConstantesGDF.idProd_Agua:
				desc = "Agua";
				break;
			case ConstantesGDF.idProd_PredialVencido:
				desc = "PredialVencido";
				break;
			case ConstantesGDF.ID_PRODUCTO_TARJETA_CIRCULACION:
				desc = "Tarjeta Circulacion";
				break;
				
			case ConstantesGDF.ID_PRODUCTO_LICENCIA_CONDUCTOR:
				desc = "Licencia";
				break;
			
			case ConstantesGDF.ID_PRODUCTO_LICENCIA_PERMANENTE:
				desc = "Licencia Permanente";
				break;
			default:
				break;
		}
    	return desc;
    }
	boolean validMaintenanceHours(String lowerTime, String UpperTime, String days){
		boolean isValid = false;
		try{
			String[] parts = lowerTime.split(":");
			Calendar lowCal = Calendar.getInstance();
			lowCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
			lowCal.set(Calendar.MINUTE, Integer.parseInt(parts[1]));
			lowCal.set(Calendar.SECOND, Integer.parseInt(parts[2]));

			parts = UpperTime.split(":");
			Calendar upperCal = Calendar.getInstance();
			upperCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
			upperCal.set(Calendar.MINUTE, Integer.parseInt(parts[1]));
			upperCal.set(Calendar.SECOND, Integer.parseInt(parts[2]));
			
			int Days = Integer.parseInt(days);
			if(Days>0)
				upperCal.add(Calendar.DATE, Days);
			
			Calendar now = Calendar.getInstance();
			
			if(now.after(lowCal)&&now.before(upperCal))
				isValid = true;
			else 
				isValid = false;
		} catch(Exception e){
			isValid = false;
		}
		return isValid;
	}
}
