package com.addcel.gdf.service;

import java.math.BigDecimal;


import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.PredialDao;
import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.ProsaPagoVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.vo.predial.PredialPreguntaAdeudoVO;
import com.addcel.gdf.vo.predial.PredialRespuestaAdeudoVO;
import com.addcel.gdf.vo.predial.PredialRespuestaAdeudoVencidoDetalleVO;
import com.addcel.gdf.vo.predial.PredialRespuestaAdeudoVencidoVO;
import com.addcel.gdf.vo.predial.PredialRespuestaSeleccionadoVencidoVO;
import com.addcel.gdf.ws.clientes.predial.IService1Proxy;
import com.addcel.gdf.ws.clientes.predial.vo.Tipo_pregunta_adeudo;
import com.addcel.gdf.ws.clientes.predial.vo.Tipo_respuesta_adeudo_vencido;
import com.addcel.gdf.ws.clientes.predial.vo.Recibo_adeudo_vencido_periodo;
import com.addcel.gdf.ws.clientes.predial.vo.Envio_adeudo_vencido_periodo;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.ibatis.service.AbstractService;
import com.addcel.utils.json.me.JSONObject;
import com.addcel.utils.Utilerias;
import com.google.gson.Gson;

public class PredialVencidoServices extends AbstractService  implements Dependency{
	private static Logger log = Logger.getLogger(PredialVencidoServices.class);

	@Override
	public String consumeWS(String json) {
		IService1Proxy servicesPortTypeProxy = null;
		PredialPreguntaAdeudoVO predialPreguntaVO = null;
		PredialRespuestaAdeudoVencidoVO predialRespuestaVO = new PredialRespuestaAdeudoVencidoVO();
		PredialRespuestaSeleccionadoVencidoVO predialRespuestaSelVO = new PredialRespuestaSeleccionadoVencidoVO();
	    BigDecimal sumActualizacion = new BigDecimal(0);
	    BigDecimal sumRecargos = new BigDecimal(0);
	    BigDecimal sumMultas = new BigDecimal(0);
	    BigDecimal impuestoEmitido = new BigDecimal(0);

		Tipo_pregunta_adeudo tipo_pregunta = null;
		Tipo_respuesta_adeudo_vencido[] tipo_respuesta = null;
		String tipoConsulta=null;
		List <PredialRespuestaAdeudoVencidoDetalleVO> detalle = null;
		String bimestres = "";
		Gson gson = new Gson();
		int idError = 0;
		String mensajeError=null;
		try {
			
			JSONObject jsObject = new JSONObject(json);
			ProcomService ps = new ProcomService();
			int productoId = 6;
			BigDecimal monto_max = new BigDecimal(ps.montoMaxProducto(new Integer(productoId)));
			log.info("monto max predial vencido :" + monto_max.toString());
			
			if (jsObject.has("tipo_consulta_pv")){
				tipoConsulta = jsObject.getString("tipo_consulta_pv");
				if(tipoConsulta.equals("Adeudos")){
					predialPreguntaVO = gson.fromJson(json, PredialPreguntaAdeudoVO.class);
					servicesPortTypeProxy = new IService1Proxy();
					tipo_pregunta = new Tipo_pregunta_adeudo();
					tipo_pregunta.setCuenta(predialPreguntaVO.getCuenta());
					tipo_pregunta.setTipo_adeudo("Vencido");
					tipo_pregunta.setUsuario("ovica");
					tipo_pregunta.setPassword("contraseña");
					
					tipo_respuesta = servicesPortTypeProxy.solicita_adeudo(tipo_pregunta);
					if(tipo_respuesta.length > 0 && "0".equals(tipo_respuesta[0].getError())){					
						predialRespuestaVO.setCadbc(tipo_respuesta[0].getCadbc());
						predialRespuestaVO.setCuenta(tipo_respuesta[0].getCuenta());						
						detalle = new ArrayList<PredialRespuestaAdeudoVencidoDetalleVO>();
						for(int i=0; i<tipo_respuesta[0].getDetalle().length;i++){
							PredialRespuestaAdeudoVencidoDetalleVO predialRespuestaDetalleVO = new PredialRespuestaAdeudoVencidoDetalleVO();
							predialRespuestaDetalleVO.setActualizacion(tipo_respuesta[0].getDetalle()[i].getActualización());							 
							predialRespuestaDetalleVO.setCond_Gast_Ejec(tipo_respuesta[0].getDetalle()[i].getCond_Gast_Ejec());
							predialRespuestaDetalleVO.setCond_mult_omi(tipo_respuesta[0].getDetalle()[i].getCond_mult_omi());
							predialRespuestaDetalleVO.setCond_rec(tipo_respuesta[0].getDetalle()[i].getCond_rec());
							predialRespuestaDetalleVO.setImpuesto_bimestral(tipo_respuesta[0].getDetalle()[i].getImpuesto_bimestral());
							predialRespuestaDetalleVO.setMulta_omision(tipo_respuesta[0].getDetalle()[i].getMulta_omision());
							predialRespuestaDetalleVO.setPeriodo(tipo_respuesta[0].getDetalle()[i].getPeriodo());							
							predialRespuestaDetalleVO.setRecargos(tipo_respuesta[0].getDetalle()[i].getRecargos());
							predialRespuestaDetalleVO.setRequerido(tipo_respuesta[0].getDetalle()[i].getRequerido());
							if(tipo_respuesta[0].getDetalle()[i].getSubtotal()!=null)
								predialRespuestaDetalleVO.setSubtotal(tipo_respuesta[0].getDetalle()[i].getSubtotal());
							else{
								BigDecimal Subtotal = (new BigDecimal(tipo_respuesta[0].getDetalle()[i].getImpuesto_bimestral())).add(
													   (new BigDecimal(tipo_respuesta[0].getDetalle()[i].getActualización()).add(
													   (new BigDecimal(tipo_respuesta[0].getDetalle()[i].getMulta_omision()).add(
													   (new BigDecimal(tipo_respuesta[0].getDetalle()[i].getRecargos())))))));
								predialRespuestaDetalleVO.setSubtotal(Subtotal.toString());
							}
								
							detalle.add(predialRespuestaDetalleVO);							
						}
						predialRespuestaVO.setDetalle(detalle);
						predialRespuestaVO.setGastos_ejecucion(tipo_respuesta[0].getGastos_ejecucion());
						predialRespuestaVO.setMulta_incumplimiento(tipo_respuesta[0].getMulta_incumplimiento());
						predialRespuestaVO.setMonto_maximo_operacion(monto_max);
						idError=0;
						
					}else if(tipo_respuesta.length > 0 && !"0".equals(tipo_respuesta[0].getError())){
						idError = -1;						
						mensajeError = tipo_respuesta[0].getDescripcion_error();
						log.error("Error en servicio de GDF: " + tipo_respuesta[0].getDescripcion_error());
					} else if (tipo_respuesta.length==0){
						idError = -1;
						mensajeError = "La cuenta "+ predialPreguntaVO.getCuenta() +" no presenta adeudos";
						log.error("La cuenta "+ predialPreguntaVO.getCuenta() +" no presenta adeudos");
					}
				}else if(tipoConsulta.equals("Periodos")){
					Recibo_adeudo_vencido_periodo request = new Recibo_adeudo_vencido_periodo();
					request.setCadbc(jsObject.getString("cadbc"));
					request.setCuenta(jsObject.getString("cuenta"));
					request.setInteres(new Boolean(false));
					request.setPassword("contraseña");
					request.setPeriodo(jsObject.getString("periodo"));
					request.setUsuario("ovica");
					
					servicesPortTypeProxy = new IService1Proxy();
					
					Envio_adeudo_vencido_periodo[] response = servicesPortTypeProxy.solicita_seleccionados(request);
					
					if(response.length > 0 && "0".equals(response[0].getError())){
						BigDecimal total_cuenta = new BigDecimal(response[0].getTotal_cuenta());
						if(monto_max.compareTo(total_cuenta)==-1)
							throw new Exception("El Total a pagar es mayor al limite máximo de la operacion de $ "+Utilerias.formatoImporte(monto_max.toString()));
						predialRespuestaSelVO.setCadbc(response[0].getCadbc());
						predialRespuestaSelVO.setCuenta(response[0].getCuenta());
						predialRespuestaSelVO.setCond_gastos_ejecucion(response[0].getCond_gastos_ejecución());
						predialRespuestaSelVO.setCond_multa_incumplimiento(response[0].getCond_multa_incumplimiento());
						detalle = new ArrayList<PredialRespuestaAdeudoVencidoDetalleVO>();
						for(int i=0; i<response[0].getDetalle().length;i++){
							PredialRespuestaAdeudoVencidoDetalleVO predialRespuestaDetalleVO = new PredialRespuestaAdeudoVencidoDetalleVO();
							predialRespuestaDetalleVO.setActualizacion(response[0].getDetalle()[i].getActualización());
							sumActualizacion = sumActualizacion.add((new BigDecimal(response[0].getDetalle()[i].getActualización())));
							predialRespuestaDetalleVO.setCond_Gast_Ejec(response[0].getDetalle()[i].getCond_Gast_Ejec());
							predialRespuestaDetalleVO.setCond_mult_omi(response[0].getDetalle()[i].getCond_mult_omi());
							predialRespuestaDetalleVO.setCond_rec(response[0].getDetalle()[i].getCond_rec());
							predialRespuestaDetalleVO.setImpuesto_bimestral(response[0].getDetalle()[i].getImpuesto_bimestral());
							predialRespuestaDetalleVO.setImpuesto_emitido(response[0].getDetalle()[i].getImpuesto_bimestral());
							impuestoEmitido = impuestoEmitido.add((new BigDecimal(response[0].getDetalle()[i].getImpuesto_bimestral())));
							predialRespuestaDetalleVO.setMulta_omision(response[0].getDetalle()[i].getMulta_omision());
							sumMultas = sumMultas.add((new BigDecimal(response[0].getDetalle()[i].getMulta_omision())));
							predialRespuestaDetalleVO.setPeriodo(response[0].getDetalle()[i].getPeriodo());
							if(i==0)
								bimestres = response[0].getDetalle()[i].getPeriodo();
							else
								bimestres = bimestres+","+response[0].getDetalle()[i].getPeriodo();
							predialRespuestaDetalleVO.setRecargos(response[0].getDetalle()[i].getRecargos());
							sumRecargos = sumRecargos.add((new BigDecimal(response[0].getDetalle()[i].getRecargos())));
							predialRespuestaDetalleVO.setRequerido(response[0].getDetalle()[i].getRequerido());
							predialRespuestaDetalleVO.setSubtotal(response[0].getDetalle()[i].getSubtotal());
							detalle.add(predialRespuestaDetalleVO);							
						}
						predialRespuestaSelVO.setDetalle(detalle);
						predialRespuestaSelVO.setBimestres(bimestres);
						predialRespuestaSelVO.setGastos_ejecucion(new BigDecimal(response[0].getGastos_ejecución()));
						predialRespuestaSelVO.setInteres(response[0].getInteres());
						predialRespuestaSelVO.setLinea_captura(response[0].getLinea());
						predialRespuestaSelVO.setTotal_cuenta(response[0].getTotal_cuenta());
						predialRespuestaSelVO.setTotalPago(response[0].getTotal_cuenta());
						predialRespuestaSelVO.setSumActualizacion(sumActualizacion);
						predialRespuestaSelVO.setSumMultas(sumMultas);
						predialRespuestaSelVO.setSumRecargos(sumRecargos);
						predialRespuestaSelVO.setImpuestoEmitido(impuestoEmitido);
						idError=0;
					}else if(response.length > 0 && !"0".equals(response[0].getError())){
						idError = -1;
						mensajeError = "El servicio de consulta tuvo un error, favor de reintentar, el error fue: " + tipo_respuesta[0].getDescripcion_error();
						log.error("Error en servicio de GDF: " + response[0].getDescripcion_error());
					}	
				}else{
					idError = -1;
					mensajeError = "El parámetro tipo_consulta_pv = " + tipoConsulta + " no es reconocible";
					log.error("El parámetro tipo_consulta_pv = " + tipoConsulta + " no es reconocible");
				}
			} else{
				idError = -1;
				mensajeError = "El parámetro tipo_consulta_pv no esta definido";
				log.error("El parámetro tipo_consulta_pv no esta definido");
			}
		} catch (Exception e) {
			idError = -1;
			mensajeError = "Ocurrio un error al consultar adeudos anteriores: " + e.getMessage();
			log.error("Ocurrio un error al consultar adeudos anteriores: " + e.getMessage());
		} finally{
			if(tipoConsulta.equals("Adeudos")){
				predialRespuestaVO.setIdError(idError);
				predialRespuestaVO.setMensajeError(mensajeError);
				json = gson.toJson(predialRespuestaVO);
				log.info("json:"+json);
			}else if(tipoConsulta.equals("Periodos")){
				predialRespuestaSelVO.setIdError(idError);
				predialRespuestaSelVO.setMensajeError(mensajeError);
				json = gson.toJson(predialRespuestaSelVO);
				log.info("json:"+json);
			}
		}
		return json;
	}
	
	@Override
	public HashMap<String, Object> consume3DSecure(String json) throws Exception {
		TBitacoraService tbitacoraService = new TBitacoraService();
		TBitacoraProsaService tbitacoraProsaService = new TBitacoraProsaService();
		PredialRespuestaSeleccionadoVencidoVO predialRespuestaSelVO = null;
		TBitacoraVO tbitacoraVO = null;
		TBitacoraProsaVO tbitacoraProsaVO = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Gson gson = new Gson();
		long id_bitacora = 0;
		try {
			
			predialRespuestaSelVO = gson.fromJson(json, PredialRespuestaSeleccionadoVencidoVO.class);
			
			tbitacoraVO = gson.fromJson(json, TBitacoraVO.class);
			tbitacoraProsaVO = gson.fromJson(json, TBitacoraProsaVO.class);
			
			tbitacoraVO.setId_proveedor(16);
			tbitacoraVO.setBit_status(0);
			tbitacoraVO.setBit_codigo_error(0);
			tbitacoraVO.setBit_cargo(predialRespuestaSelVO.getTotal_cuenta());
			tbitacoraVO.setDestino(predialRespuestaSelVO.getLinea_captura());
			
			id_bitacora = tbitacoraService.insertTBitacora(tbitacoraVO);
			if(id_bitacora > 0){
				tbitacoraVO.setId_bitacora(id_bitacora);
				predialRespuestaSelVO.setId_bitacora(id_bitacora);
				predialRespuestaSelVO.setError("0");
				tbitacoraProsaVO.setId_bitacora(new Long(id_bitacora));
				tbitacoraProsaService.insertTBitacora(tbitacoraProsaVO);
				insertBitacoraPredial(predialRespuestaSelVO);
			}
			predialRespuestaSelVO.setId_producto(ConstantesGDF.idProd_PredialVencido);
			resp.put("tbitacoraVO", tbitacoraVO);
			resp.put("guardaVO", predialRespuestaSelVO);
			
			log.info("Exito InfraccionService.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora());
			
		} catch (Exception e) {
			log.error("Error TenenciaServices.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora(), e);
			throw new Exception(e);
		}
		return resp;
	}

	public int insertBitacoraPredial(PredialRespuestaSeleccionadoVencidoVO bitacora) throws Exception{
		PredialDao dao = null;
		int idPredial = 0;
		try {
			dao = (PredialDao) getBean("PredialDao");
			dao.insertGDFPredialVencido(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al insert Detalle GDF Predial ." , e);
			throw new Exception(e);
		}
		
		return idPredial;
	}
	
	public int updateBitacoraPredial(PredialRespuestaAdeudoVO bitacora) throws Exception{
		PredialDao dao = null;
		int idPredial = 0;
		try {
			dao = (PredialDao) getBean("PredialDao");
			dao.updateGDFPredial(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Predial ." , e);
			throw new Exception(e);
		}
		
		return idPredial;
	}
	
	@Override
	public int updateBitacoraDetalle(ProsaPagoVO prosaPagoVO, long idBitacora, int status, String tipoTDC) throws Exception{
		PredialDao dao = null;
		PredialRespuestaSeleccionadoVencidoVO bitacora = null;
		int idPredial = 0;
		try {
//			log.info("updateBitacoraDetalle - transactionProcomVO: " +transactionProcomVO);
			bitacora = new PredialRespuestaSeleccionadoVencidoVO();
			
			bitacora.setId_bitacora(idBitacora);
			bitacora.setNo_autorizacion(prosaPagoVO.getAutorizacion());
//			bitacora.setError(transactionProcomVO.getError());
//			bitacora.setDescError(transactionProcomVO.getDescError());
			bitacora.setTipoTarjeta(tipoTDC);
			bitacora.setStatusPago(status);
			dao = (PredialDao) getBean("PredialDao");
			dao.updateGDFPredialVencido(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Predial ." , e);
			throw new Exception(e);
		}
		
		return idPredial;
	}

	public String consumeConsultas(String json) throws Exception{
		HashMap<String, String> consulta = new HashMap<String, String>();
//		PredialDao dao = null;
		Gson gson = new Gson();
		try {
			JSONObject jsObject = new JSONObject(json);
			
			if (jsObject.has("id_usuario")){
				consulta.put("id_usuario",jsObject.getString("id_usuario"));
				log.info("id_usuario: " + jsObject.getString("id_usuario") );
			}
			if (jsObject.has("mes")){
				consulta.put("mes",jsObject.getString("mes"));
				log.info("mes: " + jsObject.getString("mes") );
			}
			if (jsObject.has("anio")){
				consulta.put("anio",jsObject.getString("anio"));
				log.info("anio: " + jsObject.getString("anio") );
			}
//			dao = (PredialDao) getBean("PredialDao");
			
//			PredialRespuestaAdeudoVO PredialRespuestaGuardaVO = new PredialRespuestaAdeudoVO();
//			PredialRespuestaGuardaVO = crearObjeto();
			
//			dao.insertGDFPredial(PredialRespuestaGuardaVO);
			
//			dao.updateGDFPredial(PredialRespuestaGuardaVO);
			
			json = "{\"consultaPredialVencido\":" + gson.toJson(getDetallePagos(consulta)) + "}";
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Consulta Predial." , e);
			throw new Exception(e);
		}
		
		return json;
	}
	
	
	
	public List<PredialRespuestaSeleccionadoVencidoVO> getDetallePagos(HashMap<String, String> consulta) throws Exception{
		List<PredialRespuestaSeleccionadoVencidoVO> listaPredial = new ArrayList<PredialRespuestaSeleccionadoVencidoVO>();
		PredialDao dao = null;
		try {
			dao = (PredialDao) getBean("PredialDao");
			
			listaPredial = dao.selectGDFPredialVencido(consulta);
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los PredialServices.getDetallePagos." , e);
			throw new Exception(e);
		}
		
		return listaPredial;
	}
	
	@Override
	public AbstractVO getDetallePagoProcom(HashMap<String, String> consulta) throws Exception{
		List<PredialRespuestaSeleccionadoVencidoVO> listaPredial = new ArrayList<PredialRespuestaSeleccionadoVencidoVO>();
		AbstractVO abstractVO = null;
		try {
			listaPredial = getDetallePagos(consulta);
			
			if(listaPredial.size() > 0){
				abstractVO = (AbstractVO) listaPredial.get(0);
			}
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los PredialServices.getDetallePagoProcom." , e);
			throw new Exception(e);
		}
		
		return abstractVO;
	}
	
	private static PredialRespuestaAdeudoVO crearObjeto(){
		PredialRespuestaAdeudoVO predialRespuestaVO = new PredialRespuestaAdeudoVO();
		predialRespuestaVO.setId_bitacora(110002);
		predialRespuestaVO.setId_usuario(13709053);
		predialRespuestaVO.setStatusPago(1);
		predialRespuestaVO.setNo_autorizacion("008002");
		predialRespuestaVO.setIntImpuesto(2543 );
		predialRespuestaVO.setConcepto("Impuesto Predial:001");
		predialRespuestaVO.setCuentaP("033109120023");
		predialRespuestaVO.setBimestre("04");
		predialRespuestaVO.setVencimiento("2013-09-02");
//		predialRespuestaVO.setLinCap("00000109120029J9AAA");
//		predialRespuestaVO.setImporte("50.15");
//		predialRespuestaVO.setReduccion("15.80");
//		predialRespuestaVO.setTotal("35.02");
		predialRespuestaVO.setError_cel("");
		predialRespuestaVO.setError_descripcion("");	
		return predialRespuestaVO;
	}
}
