package com.addcel.gdf.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.PredialDao;
import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.ProsaPagoVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.vo.predial.PredialPreguntaAdeudoVO;
import com.addcel.gdf.vo.predial.PredialRespuestaAdeudoVO;
import com.addcel.gdf.ws.clientes.predial.IService1Proxy;
import com.addcel.gdf.ws.clientes.predial.vo.Tipo_pregunta_adeudo;
import com.addcel.gdf.ws.clientes.predial.vo.Tipo_respuesta_adeudo;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.Utilerias;
import com.addcel.utils.ibatis.service.AbstractService;
import com.addcel.utils.json.me.JSONObject;
import com.google.gson.Gson;

public class PredialServices extends AbstractService  implements Dependency{
	private static Logger log = Logger.getLogger(PredialServices.class);

	@Override
	public String consumeWS(String json) {
		IService1Proxy servicesPortTypeProxy = null;
		PredialPreguntaAdeudoVO predialPreguntaVO = null;
		PredialRespuestaAdeudoVO predialRespuestaVO = null;
		List<PredialRespuestaAdeudoVO> predialRespuestas = new ArrayList<PredialRespuestaAdeudoVO>();
		boolean error1 = false;
		Tipo_pregunta_adeudo tipo_pregunta = null;
		Tipo_respuesta_adeudo[] tipo_respuesta = null;
		Gson gson = new Gson();

		try {
			
			ProcomService ps = new ProcomService();
			int productoId = 4;
			BigDecimal monto_max = new BigDecimal(ps.montoMaxProducto(new Integer(productoId)));
			log.info("monto max predial vigente :" + monto_max.toString());

			predialPreguntaVO = gson.fromJson(json, PredialPreguntaAdeudoVO.class);

			servicesPortTypeProxy = new IService1Proxy();
	
			tipo_pregunta = new Tipo_pregunta_adeudo();
			tipo_pregunta.setCuenta(predialPreguntaVO.getCuenta());
			tipo_pregunta.setTipo_adeudo("Vigente");
			tipo_pregunta.setUsuario("ovica");
			tipo_pregunta.setPassword("contraseña");

			tipo_respuesta = servicesPortTypeProxy.solicitar_adeudo_vigente(tipo_pregunta);

			if(tipo_respuesta.length > 0 && "0".equals(tipo_respuesta[0].getError())){
				predialRespuestaVO = new PredialRespuestaAdeudoVO();
				predialRespuestaVO.setIntImpuesto(tipo_respuesta[0].getIntImpuesto());
				predialRespuestaVO.setConcepto(tipo_respuesta[0].getConcepto());
				predialRespuestaVO.setCuentaP(tipo_respuesta[0].getCuentaP());
				predialRespuestaVO.setBimestre(tipo_respuesta[0].getBimestre());
				predialRespuestaVO.setVencimiento(
						tipo_respuesta[0].getVencimiento() != null?tipo_respuesta[0].getVencimiento().substring(0, 10):"");
				predialRespuestaVO.setLinea_captura(tipo_respuesta[0].getLinCap());
				predialRespuestaVO.setImporte(Utilerias.formatoImporte(tipo_respuesta[0].getImporte()));
				predialRespuestaVO.setReduccion(tipo_respuesta[0].getReduccion());
				predialRespuestaVO.setTotalPago(Utilerias.formatoImporte(tipo_respuesta[0].getTotal()));
				predialRespuestaVO.setError_cel(tipo_respuesta[0].getError_cel()==null?"":tipo_respuesta[0].getError_cel());
				predialRespuestaVO.setError_descripcion(
						tipo_respuesta[0].getError_descripcion()==null?"":tipo_respuesta[0].getError_descripcion());
				predialRespuestaVO.setMonto_maximo_operacion(monto_max);
				predialRespuestas.add(predialRespuestaVO);
			}else if(tipo_respuesta.length > 0 && !"0".equals(tipo_respuesta[0].getError())){
				String mensajeErr="Error al consultar";
				if(tipo_respuesta[0].getMensaje()!=null)
					mensajeErr = tipo_respuesta[0].getMensaje();
				else if (tipo_respuesta[0].getError_descripcion()!=null)
					mensajeErr = tipo_respuesta[0].getError_descripcion();				
				json = "{\"error\":\""+ mensajeErr +"\",\"numError\":\"S00"+ tipo_respuesta[0].getError() +"\"}";
			}else if(tipo_respuesta.length == 0){
				json = "{\"error\":\"La cuenta no presenta adeudos\",\"numError\":\"S001\"}";
				error1=true;
			}			

			if(!error1){
				tipo_pregunta.setTipo_adeudo("Anticipado");

				tipo_respuesta = servicesPortTypeProxy.solicitar_adeudo_anticipado(tipo_pregunta);

				if(tipo_respuesta.length > 0 && "0".equals(tipo_respuesta[0].getError())){
					predialRespuestaVO = new PredialRespuestaAdeudoVO();
					predialRespuestaVO.setIntImpuesto(tipo_respuesta[0].getIntImpuesto());
					predialRespuestaVO.setConcepto(tipo_respuesta[0].getConcepto());
					predialRespuestaVO.setCuentaP(tipo_respuesta[0].getCuentaP());
					predialRespuestaVO.setBimestre(tipo_respuesta[0].getBimestre());
					predialRespuestaVO.setVencimiento(
						tipo_respuesta[0].getVencimiento() != null?tipo_respuesta[0].getVencimiento().substring(0, 10):"");
					predialRespuestaVO.setLinea_captura(tipo_respuesta[0].getLinCap());
					predialRespuestaVO.setImporte(Utilerias.formatoImporte(tipo_respuesta[0].getImporte()));
					predialRespuestaVO.setReduccion(tipo_respuesta[0].getReduccion());
					predialRespuestaVO.setTotalPago(Utilerias.formatoImporte(tipo_respuesta[0].getTotal()));
					predialRespuestaVO.setError_cel(tipo_respuesta[0].getError_cel()==null?"":tipo_respuesta[0].getError_cel());
					predialRespuestaVO.setError_descripcion(
						tipo_respuesta[0].getError_descripcion()==null?"":tipo_respuesta[0].getError_descripcion());
					
					predialRespuestaVO.setMonto_maximo_operacion(monto_max);
					predialRespuestas.add(predialRespuestaVO);
					json = "{\"predialRespuestas\":" + gson.toJson(predialRespuestas) + "}";
					
				}else if(tipo_respuesta.length > 0 && !"0".equals(tipo_respuesta[0].getError())){
					String errorMsg = "";
					if(tipo_respuesta[0].getMensaje()!=null)
						errorMsg = tipo_respuesta[0].getMensaje();
					else if (tipo_respuesta[0].getError_descripcion()!=null)
						errorMsg = tipo_respuesta[0].getError_descripcion();
					json = "{\"error\":\""+ errorMsg +"\",\"numError\":\"S00"+ tipo_respuesta[0].getError() +"\"}";
				}else if(tipo_respuesta.length == 0){
					json = "{\"error\":\"La cuenta no presenta adeudos\",\"numError\":\"S001\"}";
				}
			}

		} catch (Exception e) {
			if (predialRespuestaVO == null) {
				json = "{\"error\":\"El servicio de consulta de adeudos de predial no se encuentra disponible, por favor reintente más tarde\",\"numError\":\"A0001\"}";
			} else {
				json = "{\"error\":\"" + e.getMessage() + "\",\"numError\":\"Z1001\"}";
			}
		}
		return json;
	}
	
	@Override
	public HashMap<String, Object> consume3DSecure(String json) throws Exception {
		TBitacoraService tbitacoraService = new TBitacoraService();
		TBitacoraProsaService tbitacoraProsaService = new TBitacoraProsaService();
		PredialRespuestaAdeudoVO predialRespuestaAdeudoVO = null;
		TBitacoraVO tbitacoraVO = null;
		TBitacoraProsaVO tbitacoraProsaVO = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Gson gson = new Gson();
		long id_bitacora = 0;
		try {
			
			predialRespuestaAdeudoVO = gson.fromJson(json, PredialRespuestaAdeudoVO.class);
			tbitacoraVO = gson.fromJson(json, TBitacoraVO.class);
			tbitacoraProsaVO = gson.fromJson(json, TBitacoraProsaVO.class);
			
			tbitacoraVO.setId_proveedor(16);
			tbitacoraVO.setBit_status(0);
			tbitacoraVO.setBit_codigo_error(0);
			tbitacoraVO.setBit_cargo(predialRespuestaAdeudoVO.getTotalPago());
			tbitacoraVO.setDestino(predialRespuestaAdeudoVO.getLinea_captura());
			
			id_bitacora = tbitacoraService.insertTBitacora(tbitacoraVO);
			if(id_bitacora > 0){
				tbitacoraVO.setId_bitacora(id_bitacora);
				predialRespuestaAdeudoVO.setId_bitacora(id_bitacora);
				predialRespuestaAdeudoVO.setError("0");
				tbitacoraProsaVO.setId_bitacora(new Long(id_bitacora));
				tbitacoraProsaService.insertTBitacora(tbitacoraProsaVO);
				insertBitacoraPredial(predialRespuestaAdeudoVO);
			}
			predialRespuestaAdeudoVO.setId_producto(ConstantesGDF.idProd_Predial);
			resp.put("tbitacoraVO", tbitacoraVO);
			resp.put("guardaVO", predialRespuestaAdeudoVO);
			
			log.info("Exito InfraccionService.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora());
			
		} catch (Exception e) {
			log.error("Error TenenciaServices.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora(), e);
			throw new Exception(e);
		}
		return resp;
	}

	public int insertBitacoraPredial(PredialRespuestaAdeudoVO bitacora) throws Exception{
		PredialDao dao = null;
		int idPredial = 0;
		try {
			dao = (PredialDao) getBean("PredialDao");
			dao.insertGDFPredial(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al insert Detalle GDF Predial ." , e);
			throw new Exception(e);
		}
		
		return idPredial;
	}
	
	public int updateBitacoraPredial(PredialRespuestaAdeudoVO bitacora) throws Exception{
		PredialDao dao = null;
		int idPredial = 0;
		try {
			dao = (PredialDao) getBean("PredialDao");
			dao.updateGDFPredial(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Predial ." , e);
			throw new Exception(e);
		}
		
		return idPredial;
	}
	
	@Override
	public int updateBitacoraDetalle(ProsaPagoVO prosaPagoVO, long idBitacora, int status, String tipoTDC) throws Exception{
		PredialDao dao = null;
		PredialRespuestaAdeudoVO bitacora = null;
		int idPredial = 0;
		try {
//			log.info("updateBitacoraDetalle - transactionProcomVO: " +transactionProcomVO);
			bitacora = new PredialRespuestaAdeudoVO();
			
			bitacora.setId_bitacora(idBitacora);
			bitacora.setNo_autorizacion(prosaPagoVO.getAutorizacion());
//			bitacora.setError(transactionProcomVO.getError());
//			bitacora.setDescError(transactionProcomVO.getDescError());
			bitacora.setTipoTarjeta(tipoTDC);
			bitacora.setStatusPago(status);
			dao = (PredialDao) getBean("PredialDao");
			dao.updateGDFPredial(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Predial ." , e);
			throw new Exception(e);
		}
		
		return idPredial;
	}

	public String consumeConsultas(String json) throws Exception{
		HashMap<String, String> consulta = new HashMap<String, String>();
//		PredialDao dao = null;
		Gson gson = new Gson();
		try {
			JSONObject jsObject = new JSONObject(json);
			
			if (jsObject.has("id_usuario")){
				consulta.put("id_usuario",jsObject.getString("id_usuario"));
				log.info("id_usuario: " + jsObject.getString("id_usuario") );
			}
			if (jsObject.has("mes")){
				consulta.put("mes",jsObject.getString("mes"));
				log.info("mes: " + jsObject.getString("mes") );
			}
			if (jsObject.has("anio")){
				consulta.put("anio",jsObject.getString("anio"));
				log.info("anio: " + jsObject.getString("anio") );
			}
//			dao = (PredialDao) getBean("PredialDao");
			
//			PredialRespuestaAdeudoVO PredialRespuestaGuardaVO = new PredialRespuestaAdeudoVO();
//			PredialRespuestaGuardaVO = crearObjeto();
			
//			dao.insertGDFPredial(PredialRespuestaGuardaVO);
			
//			dao.updateGDFPredial(PredialRespuestaGuardaVO);
			
			json = "{\"consultaPredial\":" + gson.toJson(getDetallePagos(consulta)) + "}";
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Consulta Predial." , e);
			throw new Exception(e);
		}
		
		return json;
	}
	
	
	
	public List<PredialRespuestaAdeudoVO> getDetallePagos(HashMap<String, String> consulta) throws Exception{
		List<PredialRespuestaAdeudoVO> listaPredial = new ArrayList<PredialRespuestaAdeudoVO>();
		PredialDao dao = null;
		try {
			dao = (PredialDao) getBean("PredialDao");
			
			listaPredial = dao.selectGDFPredial(consulta);
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los PredialServices.getDetallePagos." , e);
			throw new Exception(e);
		}
		
		return listaPredial;
	}
	
	@Override
	public AbstractVO getDetallePagoProcom(HashMap<String, String> consulta) throws Exception{
		List<PredialRespuestaAdeudoVO> listaPredial = new ArrayList<PredialRespuestaAdeudoVO>();
		AbstractVO abstractVO = null;
		try {
			listaPredial = getDetallePagos(consulta);
			
			if(listaPredial.size() > 0){
				abstractVO = (AbstractVO) listaPredial.get(0);
			}
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los PredialServices.getDetallePagoProcom." , e);
			throw new Exception(e);
		}
		
		return abstractVO;
	}
	
	private static PredialRespuestaAdeudoVO crearObjeto(){
		PredialRespuestaAdeudoVO predialRespuestaVO = new PredialRespuestaAdeudoVO();
		predialRespuestaVO.setId_bitacora(110002);
		predialRespuestaVO.setId_usuario(13709053);
		predialRespuestaVO.setStatusPago(1);
		predialRespuestaVO.setNo_autorizacion("008002");
		predialRespuestaVO.setIntImpuesto(2543 );
		predialRespuestaVO.setConcepto("Impuesto Predial:001");
		predialRespuestaVO.setCuentaP("033109120023");
		predialRespuestaVO.setBimestre("04");
		predialRespuestaVO.setVencimiento("2013-09-02");
//		predialRespuestaVO.setLinCap("00000109120029J9AAA");
//		predialRespuestaVO.setImporte("50.15");
//		predialRespuestaVO.setReduccion("15.80");
//		predialRespuestaVO.setTotal("35.02");
		predialRespuestaVO.setError_cel("");
		predialRespuestaVO.setError_descripcion("");	
		return predialRespuestaVO;
	}
}
