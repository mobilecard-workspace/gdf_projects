package com.addcel.gdf.service;

import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.TBitacoraProsaDao;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.utils.ibatis.service.AbstractService;

public class TBitacoraProsaService extends AbstractService {
	public void pruebaTbitacora(){
		
		TBitacoraProsaVO bitacora = new TBitacoraProsaVO();
		bitacora.setId_bitacoraProsa(110241);
		bitacora.setId_bitacora(110241);
		bitacora.setId_usuario(1234);
		bitacora.setTarjeta("niu3un2i3u?==");
		bitacora.setAutorizacion("0123456");
		bitacora.setConcepto("904SFG");
		bitacora.setCargo(1);
		bitacora.setComision(10);
		bitacora.setCx("0.0000");
		bitacora.setCy("0.0000");
		try{
			updateTBitacoraProsa(bitacora);
		}catch(Exception e){
			
		}
		
		
	}
	private static Logger log = Logger.getLogger(TBitacoraProsaService.class);

	public int insertTBitacora(TBitacoraProsaVO bitacora) throws Exception {
		int idBitacora = 0; 
		TBitacoraProsaDao dao = null;
		try {
			dao = (TBitacoraProsaDao) getBean("TBitacoraProsaDao");
			idBitacora = dao.insertTBitacoraProsa(bitacora);
			
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Comunicados Generales." , e);
			throw new Exception(e);
		}
		return idBitacora;
	}
	

	public int updateTBitacoraProsa(TBitacoraProsaVO bitacora) throws Exception {
		int idBitacora = 0; 
		TBitacoraProsaDao dao = null;
		try {
			dao = (TBitacoraProsaDao) getBean("TBitacoraProsaDao");
			idBitacora = dao.updateTBitacoraProsa(bitacora);
			
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los Comunicados Generales." , e);
			throw new Exception(e);
		}
		return idBitacora;
	}
}
