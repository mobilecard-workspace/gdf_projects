package com.addcel.gdf.service;

import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.TransactionProcomDao;
import com.addcel.gdf.vo.bitacoras.TransactionProcomVO;
import com.addcel.utils.ibatis.service.AbstractService;

public class TransactionProcomService extends AbstractService {
	public void pruebaTbitacora(){
		
		TransactionProcomVO bitacora = new TransactionProcomVO();
		bitacora.setTransactionProcomId(0);
		bitacora.setTransactionProcomMnt( 	220500);
		bitacora.setTransactionProcomMrchId(7454431);
		bitacora.setTransactionProsaId(110241);
		bitacora.setEm_Response("approved");
		bitacora.setEm_Total(220500);
		bitacora.setEm_OrderID("851");
		bitacora.setEm_Merchant(7454431);
		bitacora.setEm_Store(1234);
		bitacora.setEm_Term("001");
		bitacora.setEm_RefNum("000123456782");
		bitacora.setEm_Auth("018286");
		bitacora.setEm_Digest("bc89583kjsoe92847a450wk4c1b541ce7c03ae18");
		
		try{
			insertTransactionProcom(bitacora);
		}catch(Exception e){
			
		}
		
		
	}
	private static Logger log = Logger.getLogger(TransactionProcomService.class);

	public int insertTransactionProcom(TransactionProcomVO bitacora) throws Exception {
		int idBitacora = 0; 
		TransactionProcomDao dao = null;
		try {
			dao = (TransactionProcomDao) getBean("TransactionProcomDao");
			idBitacora = dao.insertTransactionProcom(bitacora);
			
		} catch (Exception e) {
			log.error("Ocurrio un error al Insertar en TransactionProcom." , e);
			throw new Exception(e);
		}
		return idBitacora;
	}
	

}
