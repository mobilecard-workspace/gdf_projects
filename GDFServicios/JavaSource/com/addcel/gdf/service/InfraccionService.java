package com.addcel.gdf.service;


import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.axis.AxisProperties;
import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.InfraccionDao;
import com.addcel.gdf.vo.AbstractVO;
import com.addcel.gdf.vo.ProsaPagoVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.vo.infraccion.InfraccionPreguntaVO;
import com.addcel.gdf.vo.infraccion.InfraccionRespuestaVO;
import com.addcel.gdf.vo.infraccion.InfraccionWrapperRespuestaVO;
import com.addcel.gdf.ws.clientes.infraccion.Arreglo_respuesta_wrapper;
import com.addcel.gdf.ws.clientes.infraccion.ServicesPortTypeProxy;
import com.addcel.gdf.ws.clientes.infraccion.Tipo_pregunta;
import com.addcel.gdf.ws.clientes.infraccion.Tipo_respuesta;
import com.addcel.utils.AxisSSLSocketFactory;
import com.addcel.utils.ConstantesGDF;
import com.addcel.utils.ibatis.service.AbstractService;
import com.addcel.utils.json.me.JSONObject;
import com.google.gson.Gson;

public class InfraccionService extends AbstractService implements Dependency{
	private static Logger log = Logger.getLogger(InfraccionService.class);

	@Override
	public String consumeWS(String json) {

		ServicesPortTypeProxy servicesPortTypeProxy = null;
		InfraccionPreguntaVO infraccionPregunta = null;
		Tipo_pregunta tipo_pregunta = null;
		Arreglo_respuesta_wrapper wrapper = null;
		Gson gson = new Gson();
		Tipo_respuesta[] tipo_respuestas = null;
		InfraccionWrapperRespuestaVO infraccionWrapperRespVO = null;
		InfraccionRespuestaVO infraccionRespuestaVO =  null;
		InfraccionRespuestaVO[] infraccionArrayVO =  null;
		
		
		try {
			ProcomService ps = new ProcomService();
			int productoId = 3;
			BigDecimal monto_max = new BigDecimal(ps.montoMaxProducto(new Integer(productoId)));
			log.info("monto max infracciones :" + monto_max.toString());

			infraccionPregunta = gson.fromJson(json, InfraccionPreguntaVO.class);
			tipo_pregunta = new Tipo_pregunta();
			
			tipo_pregunta.setPlaca(infraccionPregunta.getPlaca());
			tipo_pregunta.setUsuario("addcel");
			tipo_pregunta.setPassword("25b6f919837ece5aab57930fcb1ee09e");

			servicesPortTypeProxy = new ServicesPortTypeProxy();
			
			AxisSSLSocketFactory.setKeystorePassword("gdfkeypass");
			AxisSSLSocketFactory.setResourcePathToKeystore("com/addcel/utils/GDFkey.jks");
			AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.utils.AxisSSLSocketFactory");
			
			wrapper = servicesPortTypeProxy.solicitar_datos(tipo_pregunta);
			
			infraccionWrapperRespVO = new InfraccionWrapperRespuestaVO();
			
			
			if(wrapper!= null && !"0".endsWith(wrapper.getAdeudo())){
				infraccionWrapperRespVO.setAdeudo(wrapper.getAdeudo());
				infraccionWrapperRespVO.setError(wrapper.getError());
				infraccionWrapperRespVO.setError_desc(wrapper.getError_desc());
				infraccionWrapperRespVO.setImporte_total(wrapper.getImporte_total());
//				infraccionWrapperRespVO.set
				
				if(wrapper.getArreglo_folios() != null){
					tipo_respuestas = wrapper.getArreglo_folios();
					infraccionArrayVO = new InfraccionRespuestaVO[ tipo_respuestas.length];
					for(int index = 0; index < tipo_respuestas.length ; index++){
						infraccionRespuestaVO = new InfraccionRespuestaVO();
						infraccionRespuestaVO.setPlaca(infraccionPregunta.getPlaca());
						infraccionRespuestaVO.setFolio(tipo_respuestas[index].getFolio());
						infraccionRespuestaVO.setImporte(tipo_respuestas[index].getImporte());
						infraccionRespuestaVO.setLinea_captura(tipo_respuestas[index].getLinea_captura());
						infraccionRespuestaVO.setTotalPago(tipo_respuestas[index].getImporte());
						infraccionRespuestaVO.setFechainfraccion(tipo_respuestas[index].getFechainfraccion());
						infraccionRespuestaVO.setActualizacion(tipo_respuestas[index].getActualizacion());
						infraccionRespuestaVO.setRecargos(tipo_respuestas[index].getRecargos());
						infraccionRespuestaVO.setDias_multa(tipo_respuestas[index].getDias_multa());
						infraccionRespuestaVO.setMulta(tipo_respuestas[index].getMulta());
						infraccionRespuestaVO.setMonto_maximo_operacion(monto_max);
						
						infraccionArrayVO[index] = infraccionRespuestaVO;
						
					}
				}
				infraccionWrapperRespVO.setArreglo_folios(infraccionArrayVO);

				json = gson.toJson(infraccionWrapperRespVO);
			}else{
				json = "{\"error\":\"La Placa no tiene adeudos\",\"numError\":\"I001\"}";
			}
			
			
		} catch (RemoteException e) {
			json = "{\"error\":\"Servicio no disponible, intente más tarde\",\"numError\":\"A2001\"}";
			e.printStackTrace();
		}

		return json;
	}
	
	@Override
	public HashMap<String, Object> consume3DSecure(String json) throws Exception {
		TBitacoraService tbitacoraService = new TBitacoraService();
		TBitacoraProsaService tbitacoraProsaService = new TBitacoraProsaService();
		InfraccionRespuestaVO infraccionRespuestaVO = null;
		TBitacoraVO tbitacoraVO = null;
		TBitacoraProsaVO tbitacoraProsaVO = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Gson gson = new Gson();
		long id_bitacora = 0;
		try {
			
			infraccionRespuestaVO = gson.fromJson(json, InfraccionRespuestaVO.class);
			tbitacoraVO = gson.fromJson(json, TBitacoraVO.class);
			tbitacoraProsaVO = gson.fromJson(json, TBitacoraProsaVO.class);
			
			tbitacoraVO.setId_proveedor(16);
			tbitacoraVO.setBit_status(0);
			tbitacoraVO.setBit_codigo_error(0);
			tbitacoraVO.setBit_cargo(infraccionRespuestaVO.getTotalPago());
			tbitacoraVO.setDestino(infraccionRespuestaVO.getLinea_captura());
			
			id_bitacora = tbitacoraService.insertTBitacora(tbitacoraVO);
			if(id_bitacora > 0){
				tbitacoraVO.setId_bitacora(id_bitacora);
				infraccionRespuestaVO.setId_bitacora(id_bitacora);
				infraccionRespuestaVO.setError("0");
				tbitacoraProsaVO.setId_bitacora(new Long(id_bitacora));
				tbitacoraProsaService.insertTBitacora(tbitacoraProsaVO);
				insertBitacoraInfraccion(infraccionRespuestaVO);
			}
			infraccionRespuestaVO.setId_producto(ConstantesGDF.idProd_Infraccion);
			resp.put("tbitacoraVO", tbitacoraVO);
			resp.put("guardaVO", infraccionRespuestaVO);
			
			log.info("Exito InfraccionService.consume3DSecure,  id_bitacora: " + tbitacoraProsaVO.getId_bitacora());
			
		} catch (Exception e) {
			log.error("Error TenenciaServices.consume3DSecure,  id_bitacora: " + 
					(tbitacoraProsaVO != null? tbitacoraProsaVO.getId_bitacora(): 0) , e);
			throw new Exception(e);
		}
		return resp;
	}
	
	public int insertBitacoraInfraccion(InfraccionRespuestaVO bitacora) throws Exception{
		InfraccionDao dao = null;
		int idInfraccion = 0;
		try {
			dao = (InfraccionDao) getBean("InfraccionDao");
			dao.insertGDFInfraccion(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al insert Detalle GDF Infraccion ." , e);
			throw new Exception(e);
		}
		
		return idInfraccion;
	}
	
	public int updateBitacoraInfraccion(InfraccionRespuestaVO bitacora) throws Exception{
		InfraccionDao dao = null;
		int idInfraccion = 0;
		try {
			dao = (InfraccionDao) getBean("InfraccionDao");
			dao.updateGDFInfraccion(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Infraccion ." , e);
			throw new Exception(e);
		}
		
		return idInfraccion;
	}
	
	@Override
	public int updateBitacoraDetalle(ProsaPagoVO prosaPagoVO, long idBitacora, int status, String tipoTDC) throws Exception{
		InfraccionDao dao = null;
		InfraccionRespuestaVO bitacora = null;
		int idAgua = 0;
		try {
//			log.info("updateBitacoraDetalle - transactionProcomVO: " +transactionProcomVO);
			bitacora = new InfraccionRespuestaVO();
			
			bitacora.setId_bitacora(idBitacora);
			bitacora.setNo_autorizacion(prosaPagoVO.getAutorizacion());
//			bitacora.setError(transactionProcomVO.getError());
//			bitacora.setDescError(transactionProcomVO.getDescError());
			bitacora.setTipoTarjeta(tipoTDC);
			bitacora.setStatusPago(status);
			dao = (InfraccionDao) getBean("InfraccionDao");
			dao.updateGDFInfraccion(bitacora);
		} catch (Exception e) {
			log.error("Ocurrio un error al update Detalle GDF Agua ." , e);
			throw new Exception(e);
		}
		
		return idAgua;
	}

	
	public String consumeConsultas(String json) throws Exception{
		HashMap<String, String> consulta = new HashMap<String, String>();
//		InfraccionDao dao = null;
		Gson gson = new Gson();
		try {
			JSONObject jsObject = new JSONObject(json);
			
			if (jsObject.has("id_usuario")){
				consulta.put("id_usuario",jsObject.getString("id_usuario"));
				log.info("id_usuario: " + jsObject.getString("id_usuario") );
			}
			if (jsObject.has("mes")){
				consulta.put("mes",jsObject.getString("mes"));
				log.info("mes: " + jsObject.getString("mes") );
			}
			if (jsObject.has("anio")){
				consulta.put("anio",jsObject.getString("anio"));
				log.info("anio: " + jsObject.getString("anio") );
			}
			
//			dao = (InfraccionDao) getBean("InfraccionDao");
			
//			InfraccionRespuestaVO infraccionRespuestaVO = new InfraccionRespuestaVO();
//			infraccionRespuestaVO = crearObjeto();
			
//			dao.insertGDFInfraccion(infraccionRespuestaVO);
			
//			dao.updateGDFInfraccion(infraccionRespuestaVO);
			
			json = "{\"consultaInfraccion\":" + gson.toJson(getDetallePagos(consulta)) + "}";
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener la consulta de Infracciones Services." , e);
			throw new Exception(e);
		}
		
		return json;
	}
	
	public List<InfraccionRespuestaVO> getDetallePagos(HashMap<String, String> consulta) throws Exception{
		List<InfraccionRespuestaVO> listaInfraccion = new ArrayList<InfraccionRespuestaVO>();
		InfraccionDao dao = null;
		try {
			dao = (InfraccionDao) getBean("InfraccionDao");
			
			listaInfraccion = dao.selectGDFInfraccion(consulta);
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los InfraccionService.getDetallePagos." , e);
			throw new Exception(e);
		}
		
		return listaInfraccion;
	}
	
	@Override
	public AbstractVO getDetallePagoProcom(HashMap<String, String> consulta) throws Exception{
		List<InfraccionRespuestaVO> listaInfraccion = new ArrayList<InfraccionRespuestaVO>();
		AbstractVO abstractVO = null;
		try {
			listaInfraccion = getDetallePagos(consulta);
			
			if(listaInfraccion.size() > 0){
				abstractVO = (AbstractVO) listaInfraccion.get(0);
			}
		} catch (Exception e) {
			log.error("Ocurrio un error al obtener los InfraccionService.getDetallePagoProcom." , e);
			throw new Exception(e);
		}
		
		return abstractVO;
	}
	
	private static InfraccionRespuestaVO crearObjeto(){
		InfraccionRespuestaVO infraccionRespuestaVO = new InfraccionRespuestaVO();
		infraccionRespuestaVO.setId_bitacora(110202);
		infraccionRespuestaVO.setId_usuario(13709053);
		infraccionRespuestaVO.setNo_autorizacion("098001");
		infraccionRespuestaVO.setPlaca("100ABC");
		infraccionRespuestaVO.setImporte("162.0");
		infraccionRespuestaVO.setFolio("03035361849");
		infraccionRespuestaVO.setLinea_captura("49030353618498JPK1ER");
		infraccionRespuestaVO.setFechainfraccion("2013-07-15");
		infraccionRespuestaVO.setActualizacion("0.0");
		infraccionRespuestaVO.setRecargos("0.0");
		infraccionRespuestaVO.setDias_multa("0");
		infraccionRespuestaVO.setError("0");
		infraccionRespuestaVO.setStatusPago(1);
		return infraccionRespuestaVO; 
	}

}
;