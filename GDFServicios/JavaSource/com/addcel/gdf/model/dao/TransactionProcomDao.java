/**
 * 
 */
package com.addcel.gdf.model.dao;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.bitacoras.TransactionProcomVO;


/**
 * @author ELopez
 *
 */

public class TransactionProcomDao extends SqlMapClientDaoSupport {
	private static Logger log = Logger.getLogger(TransactionProcomDao.class);
	
	public int insertTransactionProcom(TransactionProcomVO transactionProcom) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la TransactionProcomVO: " + transactionProcom);
		idBitacora = (Integer )getSqlMapClientTemplate().insert("insertTransactionProcom", transactionProcom);
		log.info("IdTransactionProcom: " + idBitacora);
		
		return idBitacora;
	}
	
	
}
