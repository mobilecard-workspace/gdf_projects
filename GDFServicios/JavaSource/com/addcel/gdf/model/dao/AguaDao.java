/**
 * 
 */
package com.addcel.gdf.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.agua.AguaRespuestaGuardaVO;
import com.addcel.gdf.vo.bitacoras.TBitacoraVO;


/**
 * @author ELopez
 *
 */
public class AguaDao extends SqlMapClientDaoSupport {
	private static Logger log = Logger.getLogger(AguaDao.class);
	
	public void insertGDFAgua(AguaRespuestaGuardaVO bitacora) {
		log.info("Datos Insert del Objeto Agua: " + bitacora);
		getSqlMapClientTemplate().insert("insertGDFAgua", bitacora);
		log.info("Exito en el insert Detalle Agua, IDBitacora: " + bitacora.getId_bitacora());
	}
	
	public int updateGDFAgua(AguaRespuestaGuardaVO bitacora) {
		Integer idAgua = 0; 
		
		log.info("Datos Update del Objeto  Agua: " + bitacora);
		idAgua = (Integer )getSqlMapClientTemplate().update("updateGDFAgua", bitacora);
		log.info("Exito en el update Detalle Agua, IDBitacora: " + bitacora.getId_bitacora());
		return idAgua;
	}
		
	public List<AguaRespuestaGuardaVO> selectGDFAgua(HashMap<String, String>  consultaAgua){
		List<AguaRespuestaGuardaVO> listaAgua = new ArrayList<AguaRespuestaGuardaVO>();
		try {
			log.debug("Consulta Bitacora Detalle Agua: " + consultaAgua);
			listaAgua = getSqlMapClientTemplate().queryForList("selectGDFAgua", consultaAgua);
			log.debug("Bitacora Detalle Agua lista tamaño: " + listaAgua.size());
		} catch (Exception e) {
			log.error("Ocurrio un error durante la obtencion de los comunicados", e);
		}
		return listaAgua;
	}

}
