/**
 * 
 */
package com.addcel.gdf.model.dao;

import org.apache.log4j.Logger;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.bitacoras.TBitacoraVO;
import com.addcel.gdf.vo.bitacoras.SubStrTarjetasVO;
import com.addcel.gdf.vo.bitacoras.CatalogoBinVO;
import java.util.List;

/**
 * @author ELopez
 *
 */

public class TBitacoraDao extends SqlMapClientDaoSupport {
	private static Logger log = Logger.getLogger(TBitacoraDao.class);
	
	
	
	public int insertTBitacora(TBitacoraVO bitacora) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la Bitacora: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().insert("insertTBitacora", bitacora);
		log.info("IdBitacora: " + idBitacora);
		
		return idBitacora;
	}
	
	public int updateTBitacora(TBitacoraVO bitacora) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la Bitacora: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().update("updateTBitacora", bitacora);

		return idBitacora;
	}
	
	public String getFechaActual() {
		log.info("Dentro de: getFechaActual" );
		return (String )getSqlMapClientTemplate().queryForObject("getFechaActual");
	}
	
	public int difFechaMin(String fechaToken) {
		log.info("Fecha: " + fechaToken);
		return (Integer )getSqlMapClientTemplate().queryForObject("difFechaMin", fechaToken);
	}
	@SuppressWarnings("unchecked")
	public List<CatalogoBinVO> getTipoTarjeta(SubStrTarjetasVO data){
		log.info("Obteniendo tipo de tarjeta: ");
		List<CatalogoBinVO> res = null;
		res = getSqlMapClientTemplate().queryForList("getTipoTarjeta", data);
		return res;		
	}
	public int insertCatalogoBin(CatalogoBinVO item){
		return (Integer)getSqlMapClientTemplate().insert("insertCatalogoBin", item);
	}
}
