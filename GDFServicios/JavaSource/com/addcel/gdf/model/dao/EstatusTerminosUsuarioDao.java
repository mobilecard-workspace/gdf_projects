/**
 * 
 */
package com.addcel.gdf.model.dao;

import org.apache.log4j.Logger;

/**
 * @author ELopez
 *
 */
public class EstatusTerminosUsuarioDao {
	private Logger log = Logger.getLogger(EstatusTerminosUsuarioDao.class);
	
//	public RespuestaTerminosVO buscaTerminosYCondiciones(int idAplicacion, int id_usuario){
//		
//		AplicacionUsuarioVO app = new AplicacionUsuarioVO();
//		RespuestaTerminosVO respuesta = new RespuestaTerminosVO();
//		List<DetalleTerminosVO> detalleResp = new ArrayList<DetalleTerminosVO> (); 
//		
//		app.setId_aplicacion(idAplicacion);
//		app.setId_usuario(id_usuario);
//		SqlSession s = null;
//		
//		try {
//			
//			s = SqlSessionFactoryBuilderImpl.getSqlSessionFactoryInstance().openSession();
//
//			EstatusTerminosUsuarioMapper mapper = s.getMapper(EstatusTerminosUsuarioMapper.class);
//			List<DetalleTerminosVO> terminosXApp = mapper.consultaTerminosPorApp(idAplicacion);
//			
//			log.info("Lista de t�rminos por app "+terminosXApp.size());
//			
//			if(terminosXApp.size() > 0){ //Si existen t�rminos para esa app
//				app.setEstatus(1);
//				List<DetalleTerminosVO> terminosAceptados = mapper.consultaTerminosPorEstatus(app); 
//				
//				log.info("Lista de t�rminos aceptados por el usuario: "+terminosAceptados.size());
//				
//				//Se pregunta por los t�rminos aceptados					
//				if(terminosAceptados.size() >0){
//					
//					if(terminosXApp.size() == terminosAceptados.size() ){
//						//Si son iguales a los que hay por app, entonces devuelve una lista vac�a y no hace nada
//						//la prop de aceptado se pone en vacio
//						log.info("El usuario acepto todos los t�rminos, se devuelve vac�o. ");
//						respuesta.setAceptado("vacio");
//						respuesta.setTerminos(detalleResp);
//						
//					}else if(terminosXApp.size() > terminosAceptados.size()){
//						
//						//Si hay mas t�rminos de los que acepto, se pregunta si le faltan por aceptar
//						
//						app.setEstatus(0);
//						detalleResp = mapper.consultaTerminosPorEstatus(app);
//						log.info("Se encontraron ["+detalleResp.size()+"] t�rminos por aceptar.");
//						
//						if(detalleResp.size() > 0){
//							respuesta.setAceptado("aceptar");								
//						} else {
//							// Si no tiene, busca los faltantes
//							detalleResp = buscaTerminosFaltantes(mapper,app,id_usuario);
//							
//							log.info("Al usuario faltan por aceptar ["+detalleResp.size()+"] t�rminos.");
//							if(detalleResp.size() > 0){
//								respuesta.setAceptado("aceptar");
//							} else {
//								respuesta.setAceptado("error");
//							}
//						}
//						
//						respuesta.setTerminos(detalleResp);
//						
//					} // del tama�o de los terminos por app
//					
//				}else if(terminosAceptados.size() == 0){ //si no tiene t�rminos aceptados, se busca si tiene t�rminos sin aceptar 
//					log.info("El usuario no tiene t�rminos aceptados");
//					app.setEstatus(0);
//					List<DetalleTerminosVO> terminosBuscar = mapper.consultaTerminosPorEstatus(app);
//					
//					log.info("Se encontraron ["+terminosBuscar.size()+"] t�rminos por aceptar.");
//					log.info("Lista de t�rminos buscados no aceptados por el usuario : "+terminosBuscar.toString());
//					
//					if(terminosBuscar.size() > 0){
//						//Si tiene terminos por aceptar se regresan al usuario
//						respuesta.setAceptado("aceptar");
//						respuesta.setTerminos(terminosBuscar);
//						
//					} else if(terminosBuscar.size() == 0){
//						//Si no tiene terminos por aceptar y no tiene terminos aceptados, entonces
//						//trae los terminos por app, los inserta con el id del usuario y los regresa 
//						//para que los acepte
//						boolean flag = insertaTerminos(terminosXApp,mapper,id_usuario);
//						
//						log.info("Respuesta de ingresar los t�rminos "+flag);
//						
//						if(flag){
//							respuesta.setAceptado("aceptar");
//							respuesta.setTerminos(terminosXApp);
//						}else{
//							respuesta.setAceptado("error");
//							respuesta.setTerminos(detalleResp);
//						}
//					}
//				}// de los t�rminos aceptados
//			} else {
//				//si no hay terminos para esa app devuelve el objeto vac�o					
//				respuesta.setAceptado("vacio");
//				respuesta.setTerminos(detalleResp);
//			}  // de los terminosXApp
//			s.commit();
//		} catch (Throwable t) {
//			s.rollback();
//			log.error("Error al buscar los t�rminos y condiciones: ",t);
//		} finally {
//			s.close();
//		}
//		return respuesta;
//	}
//	
//	public boolean actualizaTerminosYCondiciones(ListaTerminosUsuarioVO terminos){
//		
//		boolean flag = true;
//		SqlSession s = null;
//		
//		try {
//			s = SqlSessionFactoryBuilderImpl.getSqlSessionFactoryInstance().openSession();
//			EstatusTerminosUsuarioMapper mapper = s.getMapper(EstatusTerminosUsuarioMapper.class);
//			List<Integer> term = terminos.getTerminos();
//			Iterator<?> iter = term.iterator();
//			
//			while(iter.hasNext()){
//				TerminosUsuarioVO termUser = new TerminosUsuarioVO(); 
//				int tt = (Integer) iter.next();
//				
//				termUser.setId_usuario(terminos.getId_usuario());
//				termUser.setIdTermino(tt);
//				mapper.actualizaTerminosYCondiciones(termUser);
//			}
//			
//			s.commit();
//
//		} catch (Throwable t) {
//			s.rollback();
//			t.printStackTrace();
//			flag = false;				
//		} finally {
//			s.close();
//		}
//		return flag;
//	}
//	
//	public List<DetalleTerminosVO> buscaTerminosFaltantes(EstatusTerminosUsuarioMapper mapper, AplicacionUsuarioVO app, int id_usuario){
//		//Se buscan los terminos faltantes  para el usuario, se ingresan a la DB y se devuelven
//		// para que sean mostrados y aceptados
//		log.info("Buscando t�rminos faltantes ....");
//		List<DetalleTerminosVO> faltantes = new ArrayList<DetalleTerminosVO> ();
//		DetalleTerminosVO detalle = null;
//		TerminosCondicionesVO terminos = null;
//	
//		//Se buscan los faltantes
//		try{
//			faltantes = mapper.consultaTerminosFaltantes(app.getId_aplicacion()+"");
//		} catch(Exception ex){
//			ex.printStackTrace();
//			return faltantes;
//		}
//		
//		log.info("Lista de t�rminos faltantes : "+faltantes.toString());
//		//Se itera para guardar
//		Iterator<?> iterFaltan = faltantes.iterator();
//		try{
//			while(iterFaltan.hasNext()){
//				detalle = (DetalleTerminosVO) iterFaltan.next(); 				
//				terminos = new TerminosCondicionesVO();
//				terminos.setAceptado(0);
//				terminos.setId_usuario(id_usuario);
//				terminos.setIdTermino(detalle.getIdTermino());
//				//los inserto en la DB 
//				mapper.insertaTerminosYCondiciones(terminos);
//			}
//		} catch(Exception ex){
//			ex.printStackTrace();
//		}
//	
//		log.info("Lista de t�rminos a mostrar : "+faltantes.toString());
//		return faltantes;
//	}
//	
//	public boolean insertaTerminos(List<DetalleTerminosVO> terminosXApp, EstatusTerminosUsuarioMapper mapper,
//			int id_usuario){
//		//Si no tiene terminos por aceptar y no tiene terminos aceptados, entonces
//		//trae los terminos por app, los inserta con el id del usuario y los regresa 
//		//para que los acepte
//		
//		boolean flag = true;
//		DetalleTerminosVO detalle = null;
//		TerminosCondicionesVO tm = null;
//		
//		log.info("Insertando t�rminos .......");
//		log.info("Lista de t�rminos a insertar del usuario: "+terminosXApp.toString());
//		Iterator<?> itt = terminosXApp.iterator();
//		
//		try{
//			while(itt.hasNext()){
//				detalle = new DetalleTerminosVO();
//				tm  = new TerminosCondicionesVO();
//				
//				detalle = (DetalleTerminosVO) itt.next();
//				
//				tm.setAceptado(0);
//				tm.setId_usuario(id_usuario);
//				tm.setIdTermino(detalle.getIdTermino());
//				 
//				mapper.insertaTerminosYCondiciones(tm);
//			}
//		} catch(Exception e){			
//			log.error("Error en insertaTerminos ",e);
//			flag = false;
//		}
//		
//		return flag;
//	}
}
