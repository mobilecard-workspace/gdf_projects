/**
 * 
 */
package com.addcel.gdf.model.dao;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.bitacoras.TBitacoraProsaVO;


/**
 * @author ELopez
 *
 */

public class TBitacoraProsaDao extends SqlMapClientDaoSupport {
	private static Logger log = Logger.getLogger(TBitacoraProsaDao.class);
	
	
	
	public int insertTBitacoraProsa(TBitacoraProsaVO bitacora) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la Bitacora: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().insert("insertTBitacoraProsa", bitacora);
		log.info("IdBitacora: " + idBitacora);
		
		return idBitacora;
	}
	
	public int updateTBitacoraProsa(TBitacoraProsaVO bitacora) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la Bitacora: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().update("updateTBitacoraProsa", bitacora);

		return idBitacora;
	}
	
}
