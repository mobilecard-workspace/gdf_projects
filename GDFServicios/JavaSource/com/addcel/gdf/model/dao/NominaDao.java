/**
 * 
 */
package com.addcel.gdf.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.nomina.NominaRespuestaVO;


/**
 * @author ELopez
 *
 */
public class NominaDao extends SqlMapClientDaoSupport {
	private static Logger log = Logger.getLogger(NominaDao.class);
	
	public void insertGDFNomina(NominaRespuestaVO bitacora) {
		log.info("Datos Insert del Objeto Nomina: " + bitacora);
		getSqlMapClientTemplate().insert("insertGDFNomina", bitacora);
		log.info("Exito en el insert Detalle Nomina, IDBitacora: " + bitacora.getId_bitacora());
	}
	
	public int updateGDFNomina(NominaRespuestaVO bitacora) {
		Integer idNomina = 0; 
		
		log.info("Datos Update del Objeto  Nomina: " + bitacora);
		idNomina = (Integer )getSqlMapClientTemplate().update("updateGDFNomina", bitacora);
		log.info("Exito en el update Detalle Nomina, IDBitacora: " + bitacora.getId_bitacora());
		return idNomina;
	}
		
	public List<NominaRespuestaVO> selectGDFNomina(HashMap<String, String>  consultaNomina){
		List<NominaRespuestaVO> listaNomina = new ArrayList<NominaRespuestaVO>();
		try {
			log.debug("Consulta Bitacora Detalle Nomina: " + consultaNomina);
			listaNomina = getSqlMapClientTemplate().queryForList("selectGDFNomina", consultaNomina);
			log.debug("Bitacora Detalle Nomina lista tamaño: " + listaNomina.size());
		} catch (Exception e) {
			log.error("Ocurrio un error durante la consulta Bitacora Detalle Nomina", e);
		}
		return listaNomina;
	}
	
	

}
