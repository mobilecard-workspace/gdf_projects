/**
 * 
 */
package com.addcel.gdf.model.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.comunicados.ComunicadosAplicacionVO;
import com.addcel.gdf.vo.comunicados.ListaComunicadosVO;


/**
 * @author ELopez
 *
 */
public class ComunicadosDao extends SqlMapClientDaoSupport {
	private static Logger log = Logger.getLogger(ComunicadosDao.class);
		
	public ListaComunicadosVO consultaComunicados(int idAplicacion){
		
		ListaComunicadosVO listaComunicados = new ListaComunicadosVO();
		
		try {
			log.debug("Comunicados idAplicacion: " + idAplicacion);
			
			List<ComunicadosAplicacionVO>  comunicados = getSqlMapClientTemplate().queryForList("consultaComunicados", idAplicacion);
			listaComunicados.setComunicados(comunicados);
			
			log.debug("Comunicados lista tamaño: " + comunicados.size());

		} catch (Exception e) {
			log.error("Ocurrio un error durante la obtencion de los comunicados", e);
			
		}
		return listaComunicados;
	}

}
