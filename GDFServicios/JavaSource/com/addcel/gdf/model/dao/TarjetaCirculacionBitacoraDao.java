package com.addcel.gdf.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.tarjetaCirculacion.TarjetaCirculacionResponseVO;

public class TarjetaCirculacionBitacoraDao extends SqlMapClientDaoSupport{
	
	private static Logger log = Logger.getLogger(TarjetaCirculacionBitacoraDao.class);

	public void insertGDFTarjetaCirculacion(
			TarjetaCirculacionResponseVO bitacora) {
		log.info("Datos Insert del Objeto Tarjeta Circulacion: " + bitacora);
		getSqlMapClientTemplate().insert("insertGDFTarjetaCirculacion", bitacora);
		log.info("Exito en el insert Detalle Tarjeta Circulacion, IDBitacora: " + bitacora.getId_bitacora());
	}

	public void updateGDFTarjetaCirculacion(
			TarjetaCirculacionResponseVO bitacora) {
		log.info("Datos Update del Objeto  Tarejta Circulacion: " + bitacora);
		getSqlMapClientTemplate().update("updateGDFTarjetaCirculacion", bitacora);
		log.info("Exito en el update Detalle Tarejta Circulacion, IDBitacora: " + bitacora.getId_bitacora());
	}

	@SuppressWarnings("unchecked")
	public List<TarjetaCirculacionResponseVO> selectGDFTarjetaCirculacion(
			HashMap<String, String> consulta) {
		List<TarjetaCirculacionResponseVO> listaTarjetaCirculacion = new ArrayList<TarjetaCirculacionResponseVO>();
		try {
			log.debug("Consulta Bitacora Detalle Tarjeta Circulacion: " + consulta);
			listaTarjetaCirculacion = getSqlMapClientTemplate().queryForList("selectGDFTarjetaCirculacion", consulta);
			log.debug("Bitacora Detalle Tarejta Circulacion lista tamaño: " + listaTarjetaCirculacion.size());
		} catch (Exception e) {
			log.error("Ocurrio un error durante la busqueda de Tarejta Circulacion DAO", e);
		}
		return listaTarjetaCirculacion;
	}
}
