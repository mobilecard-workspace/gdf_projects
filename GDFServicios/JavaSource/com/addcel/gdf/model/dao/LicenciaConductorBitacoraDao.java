package com.addcel.gdf.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.licencia.LicenciaConductorResponseVO;

public class LicenciaConductorBitacoraDao extends SqlMapClientDaoSupport{

	private static Logger log = Logger.getLogger(TarjetaCirculacionBitacoraDao.class);

	public void insertGDFLicenciaConductor(LicenciaConductorResponseVO bitacora) {
		log.info("Datos Insert del Objeto Licencia Conductor: " + bitacora);
		getSqlMapClientTemplate().insert("insertGDFLicenciaConductor", bitacora);
		log.info("Exito en el insert Detalle Licencia Conductor, IDBitacora: " + bitacora.getId_bitacora());
	}

	public void updateGDFLicenciaConductor(LicenciaConductorResponseVO bitacora) {
		log.info("Datos Update del Objeto Licencia Conductor: " + bitacora);
		getSqlMapClientTemplate().update("updateGDFLicenciaConductor", bitacora);
		log.info("Exito en el update Detalle Licencia Conductor, IDBitacora: " + bitacora.getId_bitacora());
	}

	@SuppressWarnings("unchecked")
	public List<LicenciaConductorResponseVO> selectGDFLicenciaConductor(
			HashMap<String, String> consulta) {
		List<LicenciaConductorResponseVO> listaTarjetaCirculacion = new ArrayList<LicenciaConductorResponseVO>();
		try {
			log.debug("Consulta Bitacora Detalle Licencia Conductor: " + consulta);
			listaTarjetaCirculacion = getSqlMapClientTemplate().queryForList("selectGDFLicenciaConductor", consulta);
			log.debug("Bitacora Detalle Tarejta Circulacion lista tamaño: " + listaTarjetaCirculacion.size());
		} catch (Exception e) {
			log.error("Ocurrio un error durante la busqueda de Licencia Conductor DAO", e);
		}
		return listaTarjetaCirculacion;
	}
	
}
