package com.addcel.gdf.model.dao;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.TProveedorVO;
import com.addcel.gdf.vo.UsuarioVO;

public class TUsuarioDao extends SqlMapClientDaoSupport{
		
	private static final Logger log = Logger.getLogger(TUsuarioDao.class);
	
	public UsuarioVO selectUsuario(String id){
		UsuarioVO usuario=null;
		try{
			usuario=(UsuarioVO) getSqlMapClientTemplate().queryForObject("selectUsuario", id);
		}catch(Exception e){
			log.error("Ocurrio un error al obtener el usuario: ", e);
		}
		return usuario;
	}
	
	public TProveedorVO getTProveedor(String id){
		TProveedorVO tproveedorVO = null;
		try{
			tproveedorVO =(TProveedorVO) getSqlMapClientTemplate().queryForObject("selectTProveedor", id);
		}catch(Exception e){
			log.error("Ocurrio un error al obtener el usuario: ", e);
		}
		return tproveedorVO;
	}
	//MLS Método para obtener topes de pago por producto
	public String getMontoMaxProducto(Integer idProducto){
		String result = null;
		try{
			result = (String) getSqlMapClientTemplate().queryForObject("selectTMontoMAxProducto", idProducto);			
		}catch(Exception e){
			log.error("Ocurrio un error al obtener el usuario: ", e);
		}
		return result;		
	}
}
