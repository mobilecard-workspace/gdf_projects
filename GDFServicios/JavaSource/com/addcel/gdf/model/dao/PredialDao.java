/**
 * 
 */
package com.addcel.gdf.model.dao;

import java.util.ArrayList;


import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.gdf.vo.predial.PredialRespuestaAdeudoVO;
import com.addcel.gdf.vo.predial.PredialRespuestaSeleccionadoVencidoVO;
import com.addcel.gdf.vo.predial.PredialRespuestaAdeudoVencidoDetalleVO;


/**
 * @author ELopez
 *
 */
public class PredialDao extends SqlMapClientDaoSupport {
	private static Logger log = Logger.getLogger(PredialDao.class);
	
	public void insertGDFPredial(PredialRespuestaAdeudoVO bitacora) {
		log.info("Datos Insert del Objeto Predial: " + bitacora);
		getSqlMapClientTemplate().insert("insertGDFPredial", bitacora);
		log.info("Exito en el insert Detalle Predial, IDBitacora: " + bitacora.getId_bitacora());
	}	
	
	public int updateGDFPredial(PredialRespuestaAdeudoVO bitacora) {
		Integer idPredial = 0; 
		
		log.info("Datos Update del Objeto  Predial: " + bitacora);
		idPredial = (Integer )getSqlMapClientTemplate().update("updateGDFPredial", bitacora);
		log.info("Exito en el update Detalle Predial, IDBitacora: " + bitacora.getId_bitacora());
		return idPredial;
	}
		
	public List<PredialRespuestaAdeudoVO> selectGDFPredial(HashMap<String, String>  consultaPredial){
		List<PredialRespuestaAdeudoVO> listaPredial = new ArrayList<PredialRespuestaAdeudoVO>();
		try {
			log.debug("Consulta Bitacora Detalle Predial: " + consultaPredial);
			listaPredial = getSqlMapClientTemplate().queryForList("selectGDFPredial", consultaPredial);
			log.debug("Bitacora Detalle Predial lista tamaño: " + listaPredial.size());
		} catch (Exception e) {
			log.error("Ocurrio un error durante la obtencion de los comunicados", e);
		}
		return listaPredial;
	}
	
	public void insertGDFPredialVencido(PredialRespuestaSeleccionadoVencidoVO bitacora) {
		log.info("Datos Insert del Objeto PredialVencido: " + bitacora);
		getSqlMapClientTemplate().insert("insertGDFPredialVencido", bitacora);
		for(int i=0; i< bitacora.getDetalle().size();i++){			
			PredialRespuestaAdeudoVencidoDetalleVO predialDetalle = bitacora.getDetalle().get(i);
			predialDetalle.setId_bitacora(bitacora.getId_bitacora());
			getSqlMapClientTemplate().insert("insertGDFPredialVencidoDetalle", predialDetalle);
		}
		log.info("Exito en el insert Detalle Predial, IDBitacora: " + bitacora.getId_bitacora());
	}	

	public int updateGDFPredialVencido(PredialRespuestaSeleccionadoVencidoVO bitacora) {
		Integer idPredial = 0; 
		
		log.info("Datos Update del Objeto  Predial: " + bitacora);
		idPredial = (Integer )getSqlMapClientTemplate().update("updateGDFPredialVencido", bitacora);
		log.info("Exito en el update Detalle Predial, IDBitacora: " + bitacora.getId_bitacora());
		return idPredial;
	}
	public List<PredialRespuestaSeleccionadoVencidoVO> selectGDFPredialVencido(HashMap<String, String>  consultaPredial){
		List<PredialRespuestaSeleccionadoVencidoVO> listaPredial = new ArrayList<PredialRespuestaSeleccionadoVencidoVO>();
		List<PredialRespuestaAdeudoVencidoDetalleVO> listaPredialDetalle = new ArrayList<PredialRespuestaAdeudoVencidoDetalleVO>(); 
		try {
			log.debug("Consulta Bitacora Detalle Predial: " + consultaPredial);
			listaPredial = getSqlMapClientTemplate().queryForList("selectGDFPredialVencido", consultaPredial);
			for(int i=0; i<listaPredial.size();i++){
				HashMap<String, String>  consultaPredialDetalle = new HashMap<String, String>();
				consultaPredialDetalle.put("id_bitacora",String.valueOf(listaPredial.get(i).getId_bitacora()));
				listaPredialDetalle = getSqlMapClientTemplate().queryForList("selectGDFPredialVencidoDetalles", consultaPredialDetalle);
				listaPredial.get(i).setDetalle(listaPredialDetalle);				
			}			
			log.debug("Bitacora Detalle Predial lista tamaño: " + listaPredial.size());
		} catch (Exception e) {
			log.error("Ocurrio un error durante la obtencion de los comunicados", e);
		}
		return listaPredial;
	}	
}
