package com.addcel.gdf.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.addcel.gdf.service.Dependency;
import com.addcel.gdf.service.AguaService;
import com.addcel.gdf.service.InfraccionService;
import com.addcel.gdf.service.NominaService;
import com.addcel.gdf.service.PredialServices;
import com.addcel.gdf.service.TenenciaServices;
import com.addcel.gdf.service.ProcomService;
import com.addcel.gdf.service.ProcomThreeSecureService;
import com.addcel.gdf.vo.ProcomProsa.ProcomVO;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.json.me.JSONException;
import com.addcel.utils.json.me.JSONObject;

/**
 * Servlet implementation class Procom3DSecureGDF
 */
public class Procom3DSecureGDF extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(Procom3DSecureGDF.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Procom3DSecureGDF() {
        super();
        // TODO Auto-generated constructor stub
    }         
        	
	/**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	ProcomThreeSecureService procomThreeSecureSer = null;
    	ProcomVO procomVO = null;
    	
    	String json = (String) request.getAttribute("json");

		if (json == null) {
			json = (String) request.getParameter("json");
		}

		if (json == null) {
			try {
				json = getFromStream(request);
			} catch (UnsupportedEncodingException ueE) {
				json = "{\"error\":\"1\",\"numError\":\"UE1001\"}";
			} catch (IOException ioE) {
				json = "{\"error\":\"1\",\"numError\":\"A1001\"}";
			}
		}

		
		if (json != null) {
			json = AddcelCrypto.decryptSensitive(json);
			
			try {
				logger.debug("Procesando peticion");
				
		        procomThreeSecureSer = new ProcomThreeSecureService();
		        procomVO = procomThreeSecureSer.inicioProceso(json);
					
					// no existe el parametro
		        request.setAttribute("prosa", procomVO);
		    	request.getRequestDispatcher("/procom/comercioThreeSecure.jsp").forward(request, response);
		    	
			} catch (Exception e) {
				json = "{\"error\":\"1\",\"numError\":\"00000\"}";
				request.getRequestDispatcher("/procom/error.jsp").forward(request, response);
			}

		}
    }
    
    private String getFromStream(HttpServletRequest request)
			throws UnsupportedEncodingException, IOException {

		String json = null;
		InputStream is = null;

		is = (InputStream) request.getInputStream();

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {

				if (is != null) {
					is.close();
				}
			}
			json = writer.toString();
		}

		return json;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	

}
