package com.addcel.gdf.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.EstatusTerminosUsuarioDao;
import com.addcel.gdf.service.Dependency;
import com.addcel.gdf.service.AguaService;
import com.addcel.gdf.service.InfraccionService;
import com.addcel.gdf.service.NominaService;
import com.addcel.gdf.service.PredialServices;
import com.addcel.gdf.service.ProcomService;
import com.addcel.gdf.service.TenenciaServices;
import com.addcel.gdf.service.TerminosCondicionesService;
import com.addcel.gdf.vo.UsuarioVO;
import com.addcel.gdf.vo.agua.AguaPreguntaFutVO;
import com.addcel.gdf.vo.terminos.TerminosCondicionesVO;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.json.me.JSONException;
import com.addcel.utils.json.me.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class BuscaTerminos
 */
public class ReenvioReciboGDF extends HttpServlet {
	private static final Logger logger=Logger.getLogger(ReenvioReciboGDF.class);
	private static final long serialVersionUID = 1L;
	private Logger log = Logger.getLogger(ReenvioReciboGDF.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReenvioReciboGDF() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
    	
    	boolean isError = false;
    	String respJson = null;
    	String id_bitacora = null;
    	String id_usuario = null;
    	int id_producto = 0;
    	ProcomService ps=new ProcomService();
    	UsuarioVO usuario = null;
    	StringBuffer paramEmail = null;
    	String respuesta =  null;
    	// MLS captar mail del request, sino existe enviar el del usuario
    	String eMailJson = null;
    	try{
    		String json = (String) request.getAttribute("json");

    		if (json == null) {
    			json = (String) request.getParameter("json");
    		}

    		if (json == null) {
    			try {
    				json = getFromStream(request);
    			} catch (UnsupportedEncodingException ueE) {
    				isError = true;
    				json = "{\"error\":\"Falta el parametro json\",\"numError\":\"UE1002\"}";
    			} catch (IOException ioE) {
    				isError = true;
    				json = "{\"error\":\"Falta el parametro json\",\"numError\":\"A1002\"}";
    			}
    		}
    		
    		if (json != null) {

    			if (!isError) {
    				
    				json = AddcelCrypto.decryptHard(json);
    				
    				JSONObject jsObject = new JSONObject(json);
    				
    				if (jsObject.has("id_bitacora")){
    					id_bitacora = jsObject.getString("id_bitacora");
    				}else{
    					respJson = "{\"error\":\"Falta el parametro id_bitacora\",\"numError\":\"B0001\"}";
    				}
    				
    				if (jsObject.has("id_producto")){
    					id_producto = Integer.parseInt(jsObject.getString("id_producto"));
    				}else{
    					respJson = "{\"error\":\"Falta el parametro id_producto\",\"numError\":\"B0002\"}";
    				}
    				
    				if (jsObject.has("id_usuario")){
    					id_usuario = jsObject.getString("id_usuario");
    				}else{
    					respJson = "{\"error\":\"Falta el parametro id_usuario\",\"numError\":\"B0003\"}";
    				}
    				// inicio MLS captar mail del request, sino existe enviar el del usuario
    				if (jsObject.has("email")){
    					eMailJson = jsObject.getString("email");
    				}
    				
					if (id_producto > 0 && id_bitacora != null && id_usuario != null){
						// inicio MLS imprimir numero de tarjeta en pdf
						usuario = ps.selectUsuario(id_usuario);
						String jsonObject = ps.jsonObject( Long.parseLong(id_bitacora), id_producto, ps.getUltimos4TDC(usuario.getUsrTdcNumero()));        		
		        		//envio de mail
		        		logger.info("Envio de correo GDF");
		        		// fin MLS imprimir numero de tarjeta en pdf
		        		if(eMailJson!=null && usuario != null)
		        			usuario.setEmail(eMailJson);
		        		// fin MLS captar mail del request, sino existe enviar el del usuario
		        		if(usuario != null && usuario.getEmail() != null){
		        			 paramEmail = new StringBuffer()
		        					 .append("correo=").append(usuario.getEmail())
		        					 .append("&json=").append(jsonObject);
		        			 
		        			 logger.info("Invio de Emai Parametros: " + paramEmail);
		        			 respuesta = ps.peticionUrlPostParams("http://50.57.192.214:8080/MailSenderAddcel/envia-recibo-gdf/" 
		        					 + id_producto, paramEmail.toString());
//		        			 if("Ok".equals(respuesta)){
		        				 respJson = "{\"error\":\"Exito en el Reenvio del Recibo de Pago\",\"numError\":\"0\"}";
//		        			 }else{
//		        				 respJson = "{\"error\":\"Ocurrio un error, favor de intentar mas tarde\",\"numError\":\"A0002\"}";
//		        			 }
		        		 }else{
		        			 logger.error("ERROR imposible enviar email: el correo es nulo");
		        			 respJson = "{\"error\":\"ERROR imposible enviar email: el correo es nulo\",\"numError\":\"A0001\"}";
		        		 }        		 
					} 
    			}
    		} else {

    			respJson = "{\"error\":\"Falta el parametro json\",\"numError\":\"A0001\"}";
    		}
			
    	}catch(Exception e){
    		respJson = "{\"error\":\"Ocurrio un error, favor de intentar mas tarde\",\"numError\":\"A0002\"}";
    	}
        
    	respJson = AddcelCrypto.encryptHard(respJson);
    	
		response.setContentType("application/Json");
		response.setContentLength(respJson.length());
		OutputStream out = response.getOutputStream();
		out.write(respJson.getBytes());
		out.flush();
		out.close();
        
    }
    
    private String getFromStream(HttpServletRequest request)
			throws UnsupportedEncodingException, IOException {

		String json = null;
		InputStream is = null;

		is = (InputStream) request.getInputStream();

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {

				if (is != null) {
					is.close();
				}
			}
			json = writer.toString();
		}

		return json;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

}
