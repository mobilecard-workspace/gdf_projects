package com.addcel.gdf.servlet;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.addcel.gdf.service.ProcomAproviGDFService;
import com.addcel.gdf.vo.bitacoras.TransactionProcomVO;


/**
 * Servlet implementation class ComercioConGDF
 */
public class ProcomAproviGDF extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ProcomAproviGDF.class);   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProcomAproviGDF() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	logger.info("Inicio proseso ProcomAproviGDF.");
    	ProcomAproviGDFService procomAproviService = new ProcomAproviGDFService();
    	TransactionProcomVO transactionProcomVO = null;
    	RequestDispatcher rd = null;
    	HashMap<String,String> resp = null;
    	try{
	    	transactionProcomVO = procomAproviService.getProcomRequest(request);
	    	resp = procomAproviService.processProcom(transactionProcomVO);
//	        
   			if(resp.containsKey("respgdf")){
   				request.setAttribute("respgdf", resp.get("respgdf"));
   			}
   			if(resp.containsKey("monto")){
   				request.setAttribute("monto", resp.get("monto"));
   			}
   			if(resp.containsKey("autoBan")){
   				request.setAttribute("autoBan", resp.get("autoBan"));
   			}
   			if(resp.containsKey("refeBan")){
   				request.setAttribute("refeBan", resp.get("refeBan"));
   			}
   			if(resp.containsKey("Dispatcher")){
   				rd = request.getRequestDispatcher(resp.get("Dispatcher"));
   				logger.info("URL Dispatcher: " + resp.get("Dispatcher"));
   			}
	        
//   			rd = request.getRequestDispatcher("/procom/pago.jsp");
    	}catch(Exception e){
    		rd = request.getRequestDispatcher("/procom/error.jsp");
    	}
    	rd.forward(request, response);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
		
}
