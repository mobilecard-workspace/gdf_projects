package com.addcel.gdf.servlet;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.addcel.gdf.model.dao.EstatusTerminosUsuarioDao;
import com.addcel.gdf.vo.terminos.ListaTerminosUsuarioVO;
import com.addcel.gdf.vo.terminos.TerminosCondicionesVO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class ActualizaTerminos
 */
public class ActualizaTerminos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ActualizaTerminos() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
    	
    	Gson gSon = new GsonBuilder().disableHtmlEscaping().create();
        EstatusTerminosUsuarioDao dao = new EstatusTerminosUsuarioDao();
       
        String json = request.getParameter("json");
        
        ListaTerminosUsuarioVO terminos = gSon.fromJson(json, ListaTerminosUsuarioVO.class);
        
        boolean respuesta =true;// dao.actualizaTerminosYCondiciones(terminos);
        
        String respJson = gSon.toJson(respuesta);
        
		response.setContentType("application/Json");
		response.setContentLength(respJson.length());
		OutputStream out = response.getOutputStream();
		out.write(respJson.getBytes());
		out.flush();
		out.close();
        
    }
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request,response);
	}

}
