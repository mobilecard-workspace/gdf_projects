package com.addcel.gdf.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.addcel.gdf.model.dao.EstatusTerminosUsuarioDao;
import com.addcel.gdf.service.Dependency;
import com.addcel.gdf.service.AguaService;
import com.addcel.gdf.service.InfraccionService;
import com.addcel.gdf.service.NominaService;
import com.addcel.gdf.service.PredialServices;
import com.addcel.gdf.service.TenenciaServices;
import com.addcel.gdf.service.TerminosCondicionesService;
import com.addcel.gdf.vo.agua.AguaPreguntaFutVO;
import com.addcel.gdf.vo.terminos.TerminosCondicionesVO;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.json.me.JSONException;
import com.addcel.utils.json.me.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class BuscaTerminos
 */
public class ConsumidorTerminos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger log = Logger.getLogger(ConsumidorTerminos.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsumidorTerminos() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
    	
    	boolean isError = false;
    	String respJson = null;
    	TerminosCondicionesService termService = null;
    	TerminosCondicionesVO termVO = null;
    	Gson gson = new Gson();
    	
    	try{
    		String json = (String) request.getAttribute("json");

    		if (json == null) {
    			json = (String) request.getParameter("json");
    		}

    		if (json == null) {
    			try {
    				json = getFromStream(request);
    			} catch (UnsupportedEncodingException ueE) {
    				isError = true;
    				json = "{\"error\":\"1\",\"numError\":\"UE1001\"}";
    			} catch (IOException ioE) {
    				isError = true;
    				json = "{\"error\":\"1\",\"numError\":\"A1001\"}";
    			}
    		}
    		
    		if (json != null) {

    			if (!isError) {
    				
    				json = AddcelCrypto.decryptHard(json);
    				termVO = gson.fromJson(json, TerminosCondicionesVO.class);
    				termService = new TerminosCondicionesService();
    				
    				try {
						switch (termVO.getId_producto()) {
						case 1:
							respJson = termService.insertTerminosXUsuario(termVO);
							break;

						case 2:
							respJson = termService.selectTerminosXUsuario(termVO);
							break;
							
						case 3:
							respJson = termService.selectTerminosXAplicacion(termVO);
							break;
//							
						default:
							respJson = "{\"error\":\"No existe la operacion\",\"numError\":\"00010\"}";
							break;
						}
    					
    				} catch (JSONException e) {
    					json = "{\"error\":\"Error\",\"numError\":\"00000\"}";
    					// no existe el parametro, error al interpretar el json
    				}
    			}

    		} else {

    			json = "{\"error\":\"1\",\"numError\":\"A0001\"}";
    		}
	    	   
		    log.info("Respuesta de la consulta ConsumidorTerminos: "+respJson);
			
    	}catch(Exception e){
    		
    	}
        
    	respJson = AddcelCrypto.encryptHard(respJson);
    	
		response.setContentType("application/Json");
		response.setContentLength(respJson.length());
		OutputStream out = response.getOutputStream();
		out.write(respJson.getBytes());
		out.flush();
		out.close();
        
    }
    
    private String getFromStream(HttpServletRequest request)
			throws UnsupportedEncodingException, IOException {

		String json = null;
		InputStream is = null;

		is = (InputStream) request.getInputStream();

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {

				if (is != null) {
					is.close();
				}
			}
			json = writer.toString();
		}

		return json;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

}
