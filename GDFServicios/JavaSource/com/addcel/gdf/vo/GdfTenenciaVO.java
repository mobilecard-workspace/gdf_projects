/**
 * 
 */
package com.addcel.gdf.vo;

import java.util.Date;

/**
 * @author ELopez
 *
 */
public class GdfTenenciaVO {
		
	private int idTenencia;
	private int idBitacora;
	private String placa;
	private String ejercicio;
	private boolean interes;
	private boolean subsidio;
	private boolean condonacion;
	private int error;
	private String errorDesc;
	private double modelo;
	private double total;
	private String lineaCaptura;
	private Date vigencia;
	private String tipoServicio;
	private double valorFactura;
	private String cveVehicular;
	private Date fechaFactura; 
	private String tipoCalculo;
	private double tenencia;
	private double descuento; 
	private double derecho;
	private double tenActualizacion;
	private double tenRecargo;
	private double tendCondRecargo;
	private double totalTenencia;
	private double derActualizacion;
	private double derRecargo;
	private double totalDerecho;
	private double impuesto;
	private double derechos;
	private double actualizacion; 
	private double recargos;
	private double condRecargos;
	private double intAdicional;
	private double tenSubsidio;
	private Date   fechaIngresa;
	private Date   fechaModifica;
	private int enArchivo;
	
	public int getIdTenencia() {
		return idTenencia;
	}
	public void setIdTenencia(int idTenencia) {
		this.idTenencia = idTenencia;
	}
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getEjercicio() {
		return ejercicio;
	}
	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}
	
	public boolean isInteres() {
		return interes;
	}
	public void setInteres(boolean interes) {
		this.interes = interes;
	}
	public boolean isSubsidio() {
		return subsidio;
	}
	public void setSubsidio(boolean subsidio) {
		this.subsidio = subsidio;
	}
	public boolean isCondonacion() {
		return condonacion;
	}
	public void setCondonacion(boolean condonacion) {
		this.condonacion = condonacion;
	}
	public int getError() {
		return error;
	}
	public void setError(int error) {
		this.error = error;
	}
	public String getErrorDesc() {
		return errorDesc;
	}
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}
	public double getModelo() {
		return modelo;
	}
	public void setModelo(double modelo) {
		this.modelo = modelo;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getLineaCaptura() {
		return lineaCaptura;
	}
	public void setLineaCaptura(String lineaCaptura) {
		this.lineaCaptura = lineaCaptura;
	}
	public Date getVigencia() {
		return vigencia;
	}
	public void setVigencia(Date vigencia) {
		this.vigencia = vigencia;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public double getValorFactura() {
		return valorFactura;
	}
	public void setValorFactura(double valorFactura) {
		this.valorFactura = valorFactura;
	}
	public String getCveVehicular() {
		return cveVehicular;
	}
	public void setCveVehicular(String cveVehicular) {
		this.cveVehicular = cveVehicular;
	}
	public Date getFechaFactura() {
		return fechaFactura;
	}
	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}
	public String getTipoCalculo() {
		return tipoCalculo;
	}
	public void setTipoCalculo(String tipoCalculo) {
		this.tipoCalculo = tipoCalculo;
	}
	public double getTenencia() {
		return tenencia;
	}
	public void setTenencia(double tenencia) {
		this.tenencia = tenencia;
	}
	public double getDescuento() {
		return descuento;
	}
	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}
	public double getDerecho() {
		return derecho;
	}
	public void setDerecho(double derecho) {
		this.derecho = derecho;
	}
	public double getTenActualizacion() {
		return tenActualizacion;
	}
	public void setTenActualizacion(double tenActualizacion) {
		this.tenActualizacion = tenActualizacion;
	}
	public double getTenRecargo() {
		return tenRecargo;
	}
	public void setTenRecargo(double tenRecargo) {
		this.tenRecargo = tenRecargo;
	}
	public double getTendCondRecargo() {
		return tendCondRecargo;
	}
	public void setTendCondRecargo(double tendCondRecargo) {
		this.tendCondRecargo = tendCondRecargo;
	}
	public double getTotalTenencia() {
		return totalTenencia;
	}
	public void setTotalTenencia(double totalTenencia) {
		this.totalTenencia = totalTenencia;
	}
	public double getDerActualizacion() {
		return derActualizacion;
	}
	public void setDerActualizacion(double derActualizacion) {
		this.derActualizacion = derActualizacion;
	}
	public double getDerRecargo() {
		return derRecargo;
	}
	public void setDerRecargo(double derRecargo) {
		this.derRecargo = derRecargo;
	}
	public double getTotalDerecho() {
		return totalDerecho;
	}
	public void setTotalDerecho(double totalDerecho) {
		this.totalDerecho = totalDerecho;
	}
	public double getImpuesto() {
		return impuesto;
	}
	public void setImpuesto(double impuesto) {
		this.impuesto = impuesto;
	}
	public double getDerechos() {
		return derechos;
	}
	public void setDerechos(double derechos) {
		this.derechos = derechos;
	}
	public double getActualizacion() {
		return actualizacion;
	}
	public void setActualizacion(double actualizacion) {
		this.actualizacion = actualizacion;
	}
	public double getRecargos() {
		return recargos;
	}
	public void setRecargos(double recargos) {
		this.recargos = recargos;
	}
	public double getCondRecargos() {
		return condRecargos;
	}
	public void setCondRecargos(double condRecargos) {
		this.condRecargos = condRecargos;
	}
	public double getIntAdicional() {
		return intAdicional;
	}
	public void setIntAdicional(double intAdicional) {
		this.intAdicional = intAdicional;
	}
	public double getTenSubsidio() {
		return tenSubsidio;
	}
	public void setTenSubsidio(double tenSubsidio) {
		this.tenSubsidio = tenSubsidio;
	}
	public Date getFechaIngresa() {
		return fechaIngresa;
	}
	public void setFechaIngresa(Date fechaIngresa) {
		this.fechaIngresa = fechaIngresa;
	}
	public Date getFechaModifica() {
		return fechaModifica;
	}
	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}
	public int getEnArchivo() {
		return enArchivo;
	}
	public void setEnArchivo(int enArchivo) {
		this.enArchivo = enArchivo;
	}
		
}
