/**
 * 
 */
package com.addcel.gdf.vo.comunicados;

import java.util.Date;

/**
 * @author ELopez
 *
 */
public class ComunicadosAplicacionVO {

	private int idComunicado; 
	private String contenido; 
	private Date fechaInicio; 
	private Date fechaFin;
	private int estatus; 
	private int idEmpresa;
	private int idAplicacion; 
	private int prioridad; 
	private String fgColor;
	private String bgColor;
	private int tamFuente;
	
	
	public int getIdComunicado() {
		return idComunicado;
	}
	public void setIdComunicado(int idComunicado) {
		this.idComunicado = idComunicado;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public int getEstatus() {
		return estatus;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public int getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public int getIdAplicacion() {
		return idAplicacion;
	}
	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}
	public int getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}
	public String getFgColor() {
		return fgColor;
	}
	public void setFgColor(String fgColor) {
		this.fgColor = fgColor;
	}
	public String getBgColor() {
		return bgColor;
	}
	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}
	public int getTamFuente() {
		return tamFuente;
	}
	public void setTamFuente(int tamFuente) {
		this.tamFuente = tamFuente;
	}
	
	
}
