/**
 * 
 */
package com.addcel.gdf.vo.comunicados;

import java.util.List;

/**
 * @author Elopez
 *
 */
public class ListaComunicadosVO {

	private List<ComunicadosAplicacionVO> comunicados;
	

	public List<ComunicadosAplicacionVO> getComunicados() {
		return comunicados;
	}

	public void setComunicados(List<ComunicadosAplicacionVO> comunicados) {
		this.comunicados = comunicados;
	}
	
}
