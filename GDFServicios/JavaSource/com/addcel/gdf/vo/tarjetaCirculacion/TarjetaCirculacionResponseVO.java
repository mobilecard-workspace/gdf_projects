package com.addcel.gdf.vo.tarjetaCirculacion;

import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.addcel.gdf.vo.AbstractVO;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TarjetaCirculacionResponseVO extends AbstractVO{

	private String placa;
	
	private double importe;
	
	private int estado;	
	
	private String fechaModificacion;
	
	private String marca;
	
	private BigDecimal modelo;
	
	private String vencimiento;
	
	private String nombreSubConcepto;
	
	private String vigencia;
	
	private float importeInicial;
	
	private float importeFinal;
	
	private float descuento;
	
	private String concepto;
	
	private int clave; 
	
	private String nombreTramite;
	
	private BigDecimal ejercicio;

	private String numError;
	
	private BigDecimal monto_maximo_operacion;
	
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public BigDecimal getModelo() {
		return modelo;
	}

	public void setModelo(BigDecimal modelo) {
		this.modelo = modelo;
	}

	public String getVencimiento() {
		return vencimiento;
	}

	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}

	public String getNombreSubConcepto() {
		return nombreSubConcepto;
	}

	public void setNombreSubConcepto(String nombreSubConcepto) {
		this.nombreSubConcepto = nombreSubConcepto;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public float getImporteInicial() {
		return importeInicial;
	}

	public void setImporteInicial(float importeInicial) {
		this.importeInicial = importeInicial;
	}

	public float getImporteFinal() {
		return importeFinal;
	}

	public void setImporteFinal(float importeFinal) {
		this.importeFinal = importeFinal;
	}

	public float getDescuento() {
		return descuento;
	}

	public void setDescuento(float descuento) {
		this.descuento = descuento;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public int getClave() {
		return clave;
	}

	public void setClave(int clave) {
		this.clave = clave;
	}

	public String getNombreTramite() {
		return nombreTramite;
	}

	public void setNombreTramite(String nombreTramite) {
		this.nombreTramite = nombreTramite;
	}

	public BigDecimal getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(BigDecimal ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getNumError() {
		return numError;
	}

	public void setNumError(String numError) {
		this.numError = numError;
	}

	public BigDecimal getMonto_maximo_operacion() {
		return monto_maximo_operacion;
	}

	public void setMonto_maximo_operacion(BigDecimal monto_maximo_operacion) {
		this.monto_maximo_operacion = monto_maximo_operacion;
	}

	
}
