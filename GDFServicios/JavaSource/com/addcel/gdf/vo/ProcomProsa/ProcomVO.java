package com.addcel.gdf.vo.ProcomProsa;

public class ProcomVO {
	//Atributos obligatorios
		private String total;
		private String currency;
		private String address;
		private String orderId;
		private String merchant;
		private String store;
		private String term;
		private String digest;
		private String urlBack;
		
		//Atributos Opcionales
		private String usuario;
		private String idTramite;
			
		public ProcomVO(String total, String currency, String address,
				String orderId, String merchant, String store, String term,
				String digest, String urlBack, String usuario, String idTramite) {
			super();
			this.total = total;
			this.currency = currency;
			this.address = address;
			this.orderId = orderId;
			this.merchant = merchant;
			this.store = store;
			this.term = term;
			this.digest = digest;
			this.urlBack = urlBack;
			this.usuario = usuario;
			this.idTramite = idTramite;
		}
		public String getTotal() {
			return total;
		}
		public void setTotal(String total) {
			this.total = total;
		}
		public String getCurrency() {
			return currency;
		}
		public void setCurrency(String currency) {
			this.currency = currency;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getOrderId() {
			return orderId;
		}
		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}
		public String getMerchant() {
			return merchant;
		}
		public void setMerchant(String merchant) {
			this.merchant = merchant;
		}
		public String getStore() {
			return store;
		}
		public void setStore(String store) {
			this.store = store;
		}
		public String getTerm() {
			return term;
		}
		public void setTerm(String term) {
			this.term = term;
		}
		public String getDigest() {
			return digest;
		}
		public void setDigest(String digest) {
			this.digest = digest;
		}
		public String getUrlBack() {
			return urlBack;
		}
		public void setUrlBack(String urlBack) {
			this.urlBack = urlBack;
		}
		public String getUsuario() {
			return usuario;
		}
		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}
		public String getIdTramite() {
			return idTramite;
		}
		public void setIdTramite(String idTramite) {
			this.idTramite = idTramite;
		}
}
