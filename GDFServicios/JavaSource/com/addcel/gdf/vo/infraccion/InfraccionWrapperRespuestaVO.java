package com.addcel.gdf.vo.infraccion;

public class InfraccionWrapperRespuestaVO {

	private String importe_total;
    private String adeudo;
    private String error;
    private String error_desc;
    private InfraccionRespuestaVO[] arreglo_folios;
	public String getImporte_total() {
		return importe_total;
	}
	public void setImporte_total(String importe_total) {
		this.importe_total = importe_total;
	}
	public String getAdeudo() {
		return adeudo;
	}
	public void setAdeudo(String adeudo) {
		this.adeudo = adeudo;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getError_desc() {
		return error_desc;
	}
	public void setError_desc(String error_desc) {
		this.error_desc = error_desc;
	}
	public InfraccionRespuestaVO[] getArreglo_folios() {
		return arreglo_folios;
	}
	public void setArreglo_folios(InfraccionRespuestaVO[] arreglo_folios) {
		this.arreglo_folios = arreglo_folios;
	}
	
}



