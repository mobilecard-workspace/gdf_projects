/**
 * Tipo_respuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.vo.infraccion;

import java.math.BigDecimal;

import com.addcel.gdf.vo.AbstractVO;

public class InfraccionRespuestaVO extends AbstractVO {
	private String placa;
    private String folio;
    private String importe;
    private String fechainfraccion;
    private String actualizacion;
    private String recargos;
    private String dias_multa;
    private int codeCalc;
    private String multa;
    private BigDecimal monto_maximo_operacion;
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
		setTotalPago(importe);
	}
	public String getFechainfraccion() {
		return fechainfraccion;
	}
	public void setFechainfraccion(String fechainfraccion) {
		this.fechainfraccion = fechainfraccion;
	}
	public String getActualizacion() {
		return actualizacion;
	}
	public void setActualizacion(String actualizacion) {
		this.actualizacion = actualizacion;
	}
	public String getRecargos() {
		return recargos;
	}
	public void setRecargos(String recargos) {
		this.recargos = recargos;
	}
	public String getDias_multa() {
		return dias_multa;
	}
	public void setDias_multa(String dias_multa) {
		this.dias_multa = dias_multa;
	}
	public int getCodeCalc() {
		return codeCalc;
	}
	public void setCodeCalc(int codeCalc) {
		this.codeCalc = codeCalc;
	}
	public String getMulta() {
		return multa;
	}
	public void setMulta(String multa) {
		this.multa = multa;
	}
	public BigDecimal getMonto_maximo_operacion() {
		return monto_maximo_operacion;
	}
	public void setMonto_maximo_operacion(BigDecimal monto_maximo_operacion) {
		this.monto_maximo_operacion = monto_maximo_operacion;
	}

}
