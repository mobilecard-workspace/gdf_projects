package com.addcel.gdf.vo.tenencia;



public class TenenciaPreguntaVO {
	
	private String placa;			//Placa del Vehiculo.
	private String ejercicio;		//A�o de la tenencia que se quiere pagar.
	private boolean subsidio;		//bandera para otorgar subsidio.
	private boolean condonacion;	//bandera para descuento por condonacion.
	private boolean interes;		//bandera para calcular interes.
	
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getEjercicio() {
		return ejercicio;
	}
	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}
	public boolean isSubsidio() {
		return subsidio;
	}
	public void setSubsidio(boolean subsidio) {
		this.subsidio = subsidio;
	}
	public boolean isCondonacion() {
		return condonacion;
	}
	public void setCondonacion(boolean condonacion) {
		this.condonacion = condonacion;
	}
	public boolean isInteres() {
		return interes;
	}
	public void setInteres(boolean interes) {
		this.interes = interes;
	}
	
}
