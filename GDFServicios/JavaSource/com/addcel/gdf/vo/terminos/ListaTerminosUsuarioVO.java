/**
 * 
 */
package com.addcel.gdf.vo.terminos;

import java.util.List;

/**
 * @author ELopez
 *
 */
public class ListaTerminosUsuarioVO {
	
	private int id_usuario;
	private List<Integer> terminos;
	
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public List<Integer> getTerminos() {
		return terminos;
	}
	public void setTerminos(List<Integer> terminos) {
		this.terminos = terminos;
	}
	
}
