/**
 * 
 */
package com.addcel.gdf.vo.terminos;

/**
 * @author ELopez
 *
 */
public class TerminosCondiciones_tempVO {

	private int idTermino; 
	private int id_usuario;  
	private int  aceptado;
	
	public int getIdTermino() {
		return idTermino;
	}
	public void setIdTermino(int idTermino) {
		this.idTermino = idTermino;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getAceptado() {
		return aceptado;
	}
	public void setAceptado(int aceptado) {
		this.aceptado = aceptado;
	}
	
	
}
