package com.addcel.gdf.vo.nomina;

import java.math.BigDecimal;

public class NominaPreguntaVO {

	private String rfc;
	private String mes_pago_nomina;
	private String anio_pago_nomina;
	
	private BigDecimal remuneraciones;
	private BigDecimal tipo_declaracion;
	private BigDecimal num_trabajadores;
	
	private boolean interes;
	
	private String usuario;
	private String password;
	
	
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getMes_pago_nomina() {
		return mes_pago_nomina;
	}
	public void setMes_pago_nomina(String mes_pago_nomina) {
		this.mes_pago_nomina = mes_pago_nomina;
	}
	public String getAnio_pago_nomina() {
		return anio_pago_nomina;
	}
	public void setAnio_pago_nomina(String anio_pago_nomina) {
		this.anio_pago_nomina = anio_pago_nomina;
	}
	public BigDecimal getRemuneraciones() {
		return remuneraciones;
	}
	public void setRemuneraciones(BigDecimal remuneraciones) {
		this.remuneraciones = remuneraciones;
	}
	public BigDecimal getTipo_declaracion() {
		return tipo_declaracion;
	}
	public void setTipo_declaracion(BigDecimal tipo_declaracion) {
		this.tipo_declaracion = tipo_declaracion;
	}
	public BigDecimal getNum_trabajadores() {
		return num_trabajadores;
	}
	public void setNum_trabajadores(BigDecimal num_trabajadores) {
		this.num_trabajadores = num_trabajadores;
	}
	public boolean isInteres() {
		return interes;
	}
	public void setInteres(boolean interes) {
		this.interes = interes;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
