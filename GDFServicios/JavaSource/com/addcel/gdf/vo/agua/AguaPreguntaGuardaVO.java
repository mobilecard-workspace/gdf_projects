/**
 * InputFut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.vo.agua;


public class AguaPreguntaGuardaVO {
    private int folio_control;

    private java.lang.String cuenta;

    private java.lang.String bimestres;
    
    private java.lang.String anio;

    private java.lang.String folio;

    private java.lang.String programa;

    private java.lang.String uso;

    private java.lang.String empresa;

    private java.lang.String ip;

    private java.lang.String id_aplicacion;

    private java.lang.String passwd;

    public AguaPreguntaGuardaVO() {
    }

    public AguaPreguntaGuardaVO(
           int folio_control,
           java.lang.String cuenta,
           java.lang.String bimestres,
           java.lang.String folio,
           java.lang.String programa,
           java.lang.String uso,
           java.lang.String empresa,
           java.lang.String ip,
           java.lang.String id_aplicacion,
           java.lang.String passwd) {
           this.folio_control = folio_control;
           this.cuenta = cuenta;
           this.bimestres = bimestres;
           this.folio = folio;
           this.programa = programa;
           this.uso = uso;
           this.empresa = empresa;
           this.ip = ip;
           this.id_aplicacion = id_aplicacion;
           this.passwd = passwd;
    }


    /**
     * Gets the folio_control value for this InputFut.
     * 
     * @return folio_control
     */
    public int getFolio_control() {
        return folio_control;
    }


    /**
     * Sets the folio_control value for this InputFut.
     * 
     * @param folio_control
     */
    public void setFolio_control(int folio_control) {
        this.folio_control = folio_control;
    }


    /**
     * Gets the cuenta value for this InputFut.
     * 
     * @return cuenta
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }


    /**
     * Sets the cuenta value for this InputFut.
     * 
     * @param cuenta
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }


    /**
     * Gets the folio value for this InputFut.
     * 
     * @return folio
     */
    public java.lang.String getFolio() {
        return folio;
    }


    /**
     * Sets the folio value for this InputFut.
     * 
     * @param folio
     */
    public void setFolio(java.lang.String folio) {
        this.folio = folio;
    }


    /**
     * Gets the programa value for this InputFut.
     * 
     * @return programa
     */
    public java.lang.String getPrograma() {
        return programa;
    }


    /**
     * Sets the programa value for this InputFut.
     * 
     * @param programa
     */
    public void setPrograma(java.lang.String programa) {
        this.programa = programa;
    }


    /**
     * Gets the uso value for this InputFut.
     * 
     * @return uso
     */
    public java.lang.String getUso() {
        return uso;
    }


    /**
     * Sets the uso value for this InputFut.
     * 
     * @param uso
     */
    public void setUso(java.lang.String uso) {
        this.uso = uso;
    }


    /**
     * Gets the empresa value for this InputFut.
     * 
     * @return empresa
     */
    public java.lang.String getEmpresa() {
        return empresa;
    }


    /**
     * Sets the empresa value for this InputFut.
     * 
     * @param empresa
     */
    public void setEmpresa(java.lang.String empresa) {
        this.empresa = empresa;
    }


    /**
     * Gets the ip value for this InputFut.
     * 
     * @return ip
     */
    public java.lang.String getIp() {
        return ip;
    }


    /**
     * Sets the ip value for this InputFut.
     * 
     * @param ip
     */
    public void setIp(java.lang.String ip) {
        this.ip = ip;
    }


    /**
     * Gets the id_aplicacion value for this InputFut.
     * 
     * @return id_aplicacion
     */
    public java.lang.String getId_aplicacion() {
        return id_aplicacion;
    }


    /**
     * Sets the id_aplicacion value for this InputFut.
     * 
     * @param id_aplicacion
     */
    public void setId_aplicacion(java.lang.String id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }


    /**
     * Gets the passwd value for this InputFut.
     * 
     * @return passwd
     */
    public java.lang.String getPasswd() {
        return passwd;
    }


    /**
     * Sets the passwd value for this InputFut.
     * 
     * @param passwd
     */
    public void setPasswd(java.lang.String passwd) {
        this.passwd = passwd;
    }

	public java.lang.String getAnio() {
		return anio;
	}

	public void setAnio(java.lang.String anio) {
		this.anio = anio;
	}

	public void setBimestres(java.lang.String bimestres) {
		this.bimestres = bimestres;
	}
}
