/**
 * 
 */
package com.addcel.gdf.vo.bitacoras;

/**
 * @author ELopez
 *
 */
public class TBitacoraVO {
	
	private long id_bitacora;
	private long id_usuario;
	private int id_proveedor;
	private int id_producto;
	private String bit_fecha;
	private String bit_hora;
	private String bit_concepto;
	private String  bit_cargo;
	private String bit_ticket;
	private String bit_no_autorizacion;
	private int bit_codigo_error;
	private String bit_card_id;
	private int bit_status;
	private String imei;
	private String destino;
	private String tarjeta_compra;
	private String tipo;
	private String software;
	private String modelo;
	private String wkey;
	
	public String toString(){
		return new StringBuffer()
			.append("\nid_bitacora: ").append(id_bitacora)
			.append("\nid_usuario: ").append(id_usuario)
			.append("\nid_proveedor: ").append(id_proveedor)
			.append("\nid_producto: ").append(id_producto)
			.append("\nbit_fecha: ").append(bit_fecha)
			.append("\nbit_hora: ").append(bit_hora)
			.append("\nbit_concepto: ").append(bit_concepto)
			.append("\nbit_cargo: ").append(bit_cargo)
			.append("\nbit_ticket: ").append(bit_ticket)
			.append("\nbit_no_autorizacion: ").append(bit_no_autorizacion)
			.append("\nbit_codigo_error: ").append(bit_codigo_error)
			.append("\nbit_card_id: ").append(bit_card_id)
			.append("\nbit_status: ").append(bit_status)
			.append("\nimei: ").append(imei)
			.append("\ndestino: ").append(destino)
			.append("\ntarjeta_compra: ").append(tarjeta_compra)
			.append("\ntipo: ").append(tipo)
			.append("\nsoftware: ").append(software)
			.append("\nmodelo: ").append(modelo)
			.append("\nwkey: ").append(wkey).toString();
	}
	
	public long getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(long id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public long getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(long id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getId_proveedor() {
		return id_proveedor;
	}
	public void setId_proveedor(int id_proveedor) {
		this.id_proveedor = id_proveedor;
	}
	public int getId_producto() {
		return id_producto;
	}
	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}
	public String getBit_fecha() {
		return bit_fecha;
	}
	public void setBit_fecha(String bit_fecha) {
		this.bit_fecha = bit_fecha;
	}
	public String getBit_hora() {
		return bit_hora;
	}
	public void setBit_hora(String bit_hora) {
		this.bit_hora = bit_hora;
	}
	public String getBit_concepto() {
		return bit_concepto;
	}
	public void setBit_concepto(String bit_concepto) {
		this.bit_concepto = bit_concepto;
	}
	public String getBit_cargo() {
		return bit_cargo;
	}
	public void setBit_cargo(String bit_cargo) {
		this.bit_cargo = bit_cargo;
	}
	public String getBit_ticket() {
		return bit_ticket;
	}
	public void setBit_ticket(String bit_ticket) {
		this.bit_ticket = bit_ticket;
	}
	public String getBit_no_autorizacion() {
		return bit_no_autorizacion;
	}
	public void setBit_no_autorizacion(String bit_no_autorizacion) {
		this.bit_no_autorizacion = bit_no_autorizacion;
	}
	public int getBit_codigo_error() {
		return bit_codigo_error;
	}
	public void setBit_codigo_error(int bit_codigo_error) {
		this.bit_codigo_error = bit_codigo_error;
	}
	public String getBit_card_id() {
		return bit_card_id;
	}
	public void setBit_card_id(String bit_card_id) {
		this.bit_card_id = bit_card_id;
	}
	public int getBit_status() {
		return bit_status;
	}
	public void setBit_status(int bit_status) {
		this.bit_status = bit_status;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getTarjeta_compra() {
		return tarjeta_compra;
	}
	public void setTarjeta_compra(String tarjeta_compra) {
		this.tarjeta_compra = tarjeta_compra;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getWkey() {
		return wkey;
	}
	public void setWkey(String wkey) {
		this.wkey = wkey;
	}
	
	}
