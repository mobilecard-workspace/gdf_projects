package com.addcel.gdf.vo.predial;

import java.math.BigDecimal;

public class PredialRespuestaAdeudoVencidoDetalleVO {
	private long id_bitacora;
	private String actualizacion;
	private String cond_Gast_Ejec;
	private String cond_mult_omi;
	private BigDecimal cond_rec;
	private String impuesto_bimestral;
	private String multa_omision;
	private String periodo;
	private String recargos;
	private String requerido;
	private String subtotal;
	private String impuesto_emitido;
	private String impuesto_pagado = "0";
	public long getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(long id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getActualizacion() {
		return actualizacion;
	}
	public void setActualizacion(String actualizacion) {
		this.actualizacion = actualizacion;
	}
	public String getCond_Gast_Ejec() {
		return cond_Gast_Ejec;
	}
	public void setCond_Gast_Ejec(String cond_Gast_Ejec) {
		this.cond_Gast_Ejec = cond_Gast_Ejec;
	}
	public String getCond_mult_omi() {
		return cond_mult_omi;
	}
	public void setCond_mult_omi(String cond_mult_omi) {
		this.cond_mult_omi = cond_mult_omi;
	}
	public BigDecimal getCond_rec() {
		return cond_rec;
	}
	public void setCond_rec(BigDecimal cond_rec) {
		this.cond_rec = cond_rec;
	}
	public String getImpuesto_bimestral() {
		return impuesto_bimestral;
	}
	public void setImpuesto_bimestral(String impuesto_bimestral) {
		this.impuesto_bimestral = impuesto_bimestral;
	}
	public String getMulta_omision() {
		return multa_omision;
	}
	public void setMulta_omision(String multa_omision) {
		this.multa_omision = multa_omision;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getRecargos() {
		return recargos;
	}
	public void setRecargos(String recargos) {
		this.recargos = recargos;
	}
	public String getRequerido() {
		return requerido;
	}
	public void setRequerido(String requerido) {
		this.requerido = requerido;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getImpuesto_emitido() {
		return impuesto_emitido;
	}
	public void setImpuesto_emitido(String impuesto_emitido) {
		this.impuesto_emitido = impuesto_emitido;
	}
	public String getImpuesto_pagado() {
		return impuesto_pagado;
	}

}
