/**
 * Tipo_respuesta_adeudo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.vo.predial;

import com.addcel.gdf.vo.AbstractVO;
import java.math.BigDecimal;
import java.util.List;

public class PredialRespuestaAdeudoVencidoVO  extends AbstractVO {
	private int idError;
	private String mensajeError;
    private String cadbc;
    private String cuenta;
    private List <PredialRespuestaAdeudoVencidoDetalleVO> detalle;
    private BigDecimal gastos_ejecucion;
    private BigDecimal multa_incumplimiento;
    private BigDecimal monto_maximo_operacion;
    public PredialRespuestaAdeudoVencidoVO() {
    }
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public String getCadbc() {
		return cadbc;
	}
	public void setCadbc(String cadbc) {
		this.cadbc = cadbc;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public List<PredialRespuestaAdeudoVencidoDetalleVO> getDetalle() {
		return detalle;
	}
	public void setDetalle(List<PredialRespuestaAdeudoVencidoDetalleVO> detalle) {
		this.detalle = detalle;
	}
	public BigDecimal getGastos_ejecucion() {
		return gastos_ejecucion;
	}
	public void setGastos_ejecucion(BigDecimal gastos_ejecucion) {
		this.gastos_ejecucion = gastos_ejecucion;
	}
	public BigDecimal getMulta_incumplimiento() {
		return multa_incumplimiento;
	}
	public void setMulta_incumplimiento(BigDecimal multa_incumplimiento) {
		this.multa_incumplimiento = multa_incumplimiento;
	}
	public BigDecimal getMonto_maximo_operacion() {
		return monto_maximo_operacion;
	}
	public void setMonto_maximo_operacion(BigDecimal monto_maximo_operacion) {
		this.monto_maximo_operacion = monto_maximo_operacion;
	}

}
