/**
 * Tipo_respuesta_adeudo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.gdf.vo.predial;

import com.addcel.gdf.vo.AbstractVO;
import java.math.BigDecimal;

public class PredialRespuestaAdeudoVO  extends AbstractVO {
    private java.lang.String bimestre;
    private java.lang.String concepto;
    private java.lang.String cuentaP;
    private java.lang.String error_cel;
    private java.lang.String error_descripcion;
    private java.lang.String importe;
    private java.lang.Integer intImpuesto;
    private java.lang.String mensaje;
    private java.lang.String reduccion;
    private java.lang.String vencimiento;

    private BigDecimal monto_maximo_operacion;
    public PredialRespuestaAdeudoVO() {
    }

    /**
     * Gets the bimestre value for this Tipo_respuesta_adeudo.
     * 
     * @return bimestre
     */
    public java.lang.String getBimestre() {
        return bimestre;
    }


    /**
     * Sets the bimestre value for this Tipo_respuesta_adeudo.
     * 
     * @param bimestre
     */
    public void setBimestre(java.lang.String bimestre) {
        this.bimestre = bimestre;
    }


    /**
     * Gets the concepto value for this Tipo_respuesta_adeudo.
     * 
     * @return concepto
     */
    public java.lang.String getConcepto() {
        return concepto;
    }


    /**
     * Sets the concepto value for this Tipo_respuesta_adeudo.
     * 
     * @param concepto
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }


    /**
     * Gets the cuentaP value for this Tipo_respuesta_adeudo.
     * 
     * @return cuentaP
     */
    public java.lang.String getCuentaP() {
        return cuentaP;
    }


    /**
     * Sets the cuentaP value for this Tipo_respuesta_adeudo.
     * 
     * @param cuentaP
     */
    public void setCuentaP(java.lang.String cuentaP) {
        this.cuentaP = cuentaP;
    }


    /**
     * Gets the error_cel value for this Tipo_respuesta_adeudo.
     * 
     * @return error_cel
     */
    public java.lang.String getError_cel() {
        return error_cel;
    }


    /**
     * Sets the error_cel value for this Tipo_respuesta_adeudo.
     * 
     * @param error_cel
     */
    public void setError_cel(java.lang.String error_cel) {
        this.error_cel = error_cel;
    }


    /**
     * Gets the error_descripcion value for this Tipo_respuesta_adeudo.
     * 
     * @return error_descripcion
     */
    public java.lang.String getError_descripcion() {
        return error_descripcion;
    }


    /**
     * Sets the error_descripcion value for this Tipo_respuesta_adeudo.
     * 
     * @param error_descripcion
     */
    public void setError_descripcion(java.lang.String error_descripcion) {
        this.error_descripcion = error_descripcion;
    }


    /**
     * Gets the importe value for this Tipo_respuesta_adeudo.
     * 
     * @return importe
     */
    public java.lang.String getImporte() {
        return importe;
    }


    /**
     * Sets the importe value for this Tipo_respuesta_adeudo.
     * 
     * @param importe
     */
    public void setImporte(java.lang.String importe) {
        this.importe = importe;
    }


    /**
     * Gets the intImpuesto value for this Tipo_respuesta_adeudo.
     * 
     * @return intImpuesto
     */
    public java.lang.Integer getIntImpuesto() {
        return intImpuesto;
    }


    /**
     * Sets the intImpuesto value for this Tipo_respuesta_adeudo.
     * 
     * @param intImpuesto
     */
    public void setIntImpuesto(java.lang.Integer intImpuesto) {
        this.intImpuesto = intImpuesto;
    }

    /**
     * Gets the mensaje value for this Tipo_respuesta_adeudo.
     * 
     * @return mensaje
     */
    public java.lang.String getMensaje() {
        return mensaje;
    }


    /**
     * Sets the mensaje value for this Tipo_respuesta_adeudo.
     * 
     * @param mensaje
     */
    public void setMensaje(java.lang.String mensaje) {
        this.mensaje = mensaje;
    }


    /**
     * Gets the reduccion value for this Tipo_respuesta_adeudo.
     * 
     * @return reduccion
     */
    public java.lang.String getReduccion() {
        return reduccion;
    }


    /**
     * Sets the reduccion value for this Tipo_respuesta_adeudo.
     * 
     * @param reduccion
     */
    public void setReduccion(java.lang.String reduccion) {
        this.reduccion = reduccion;
    }


    /**
     * Gets the vencimiento value for this Tipo_respuesta_adeudo.
     * 
     * @return vencimiento
     */
    public java.lang.String getVencimiento() {
        return vencimiento;
    }


    /**
     * Sets the vencimiento value for this Tipo_respuesta_adeudo.
     * 
     * @param vencimiento
     */
    public void setVencimiento(java.lang.String vencimiento) {
        this.vencimiento = vencimiento;
    }

	public BigDecimal getMonto_maximo_operacion() {
		return monto_maximo_operacion;
	}

	public void setMonto_maximo_operacion(BigDecimal monto_maximo_operacion) {
		this.monto_maximo_operacion = monto_maximo_operacion;
	}
    
}
