package org.addcel.client;

import org.addcel.crypto.AddcelCrypto;
import org.addcel.crypto.Crypto;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Terminos;
import org.addcel.gdf.constant.TipoConsulta;
import org.addcel.gdf.enums.Licencias;
import org.addcel.util.AppUtils;
import org.addcel.util.DeviceUtils;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class Request {
	
	private static final String TAG = "Request";

	public static String buildComunicadosRequest(int idAplicacion) {
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("idAplicacion", idAplicacion);
			Log.d(TAG, json.toString());
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		return AddcelCrypto.encryptHard(json.toString());
	}
	
	public static String buildCondicionesRequest(int idAplicacion, int idProducto) {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.put("id_aplicacion", idAplicacion);
			JSON.put("id_producto", idProducto);
			Log.i("JSON condiciones", JSON.toString(1));
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		
		String cifrado = AddcelCrypto.encryptHard(JSON.toString());
		
		Log.i("cifrado terminos", cifrado);
		
		return cifrado;
		
		
	}
	
	public static String getPasswordUpdateRequestAsString(String login, String passwordS, String password) {
		JSONObject updateJSON = new JSONObject();
		
		try {
			updateJSON.put("login", login);
			updateJSON.put("passwordS", Crypto.sha1(passwordS));
			updateJSON.put("password", password);
			updateJSON.put("newPassword", password);
			
			Log.i(TAG, updateJSON.toString(1));
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String updateString = Crypto.aesEncrypt(Text.parsePass(password), updateJSON.toString());
		updateString = Text.mergeStr(updateString, password);
		
		return updateString;
	}
	
	public static String buildTenenciaRequest(String userId, String password, String placa, String ejercicio) {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.putOpt("toDO", Servicios.TENENCIA);
			JSON.putOpt("usuario", userId);
			JSON.putOpt("placa", placa);
			JSON.putOpt("ejercicio", ejercicio);
			JSON.putOpt("condonacion", false);
			JSON.putOpt("interes", false);
			JSON.putOpt("subsidio", true);
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String cifrado =  AddcelCrypto.encryptSensitive(password, JSON.toString());
		
		Log.i("cifrado tenencia", cifrado);
		
		return cifrado;
	}
	
	public static String buildInfraccionesRequest(String placa) {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.putOpt("toDO", Servicios.INFRACCION);
			JSON.putOpt("placa", placa);
			
			Log.i(TAG, JSON.toString());
		} catch ( JSONException e) {
			Log.e(TAG, "", e);
			return "";	
		}
		
		String cifrado =  AddcelCrypto.encryptSensitive(AppUtils.getKey(), JSON.toString());
		
		Log.i(TAG, cifrado);
		
		return cifrado;
	}
	
	
	public static String buildNominaRequest(String rfc, String mes, String anio, int tipo, int remuneraciones, int numTrabajadores) {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.put("toDO", Servicios.NOMINA);
			JSON.put("rfc", rfc);
			JSON.put("mes_pago_nomina", mes);
			JSON.put("anio_pago_nomina", anio);
			JSON.put("remuneraciones", remuneraciones);
			JSON.put("tipo_declaracion", tipo);
			JSON.put("num_trabajadores", numTrabajadores);
			JSON.put("interes", true);
			
			Log.i(TAG, "Request n�mina: " + JSON.toString());
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String cifrado =  AddcelCrypto.encryptSensitive(AppUtils.getKey(), JSON.toString());
		
		Log.i("cifrado nomina", cifrado);
		
		return cifrado;
	}
	
	public static String buildPredialRequest(String cuentaPredial) {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.put("toDO", Servicios.PREDIAL);
			JSON.put("cuenta", cuentaPredial);
		} catch (JSONException e) {
			return "";
		}
		
		String cifrado =  AddcelCrypto.encryptSensitive(AppUtils.getKey(), JSON.toString());
		
		Log.i("cifrado predial", cifrado);
		
		return cifrado;
	}
	
	public static String buildAguaRequest(String cuentaAgua) {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.put("toDO", Servicios.AGUA );
			JSON.put("cuenta", cuentaAgua);
		} catch (JSONException e){
			Log.e(TAG, "", e);
			return "";
		}
		
		String cifrado = AddcelCrypto.encryptSensitive(AppUtils.getKey(), JSON.toString());
		
		Log.i("cifrado agua", cifrado);
		
		return cifrado;
	}
	
	public static String buildLicenciaRequest(Licencias l, String rfc, String idUsuario) {
		
		JSONObject json = new JSONObject();
		
		try {
			
			switch (l) {
			case TRES_ANIOS:
				json.put("toDO", String.valueOf(Servicios.LICENCIA));
				break;
			case PERMANENTE:
				json.put("toDO", String.valueOf(Servicios.LICENCIA_PERMANENTE));
				break;
			default:
				break;
			}
			
			json.put("rfc", rfc).put("id_usuario", Long.valueOf(idUsuario));
			
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String cifrado = AddcelCrypto.encryptSensitive(AppUtils.getKey(), json.toString());
		
		Log.i("cifrado licencia", cifrado);
		
		return cifrado;
		
		
	}
	
	public static String buildTarjetaCirculacionRequest(String idUsuario, String marca, String modelo, String placa) {
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("toDO", Servicios.T_CIRCULACION)
				.put("id_usuario", Long.parseLong(idUsuario))
				.put("marca", marca)
				.put("modelo", modelo)
				.put("placa", placa);
			
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String cifrado = AddcelCrypto.encryptSensitive(AppUtils.getKey(), json.toString());
		
		Log.i("cifrado tCirculacion", cifrado);
		
		return cifrado;
		
	}
	
	public static String buildPVencidosRequest(String cuenta, String tipoConsulta, String... periodos) {
		JSONObject JSON  = new JSONObject();
		
		try {
			JSON.put("toDO", Servicios.PREDIAL_VENCIDOS);
			JSON.put("cuenta", cuenta);
			JSON.put("tipo_consulta_pv", tipoConsulta);
			
			if (tipoConsulta.equals(TipoConsulta.PERIODOS)) {
				String periodosPipe ="";
				
				for (String string : periodos) {
					string = string + "|";
					periodosPipe = periodosPipe + string;
				}
				
				periodosPipe = periodosPipe.substring(0, periodosPipe.length() - 1);
				Log.i(TAG, "Periodos con pipe: " + periodosPipe);
				
				JSON.put("periodo", periodosPipe);
				JSON.put("cadbc", "null");
			}
			
			
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String cifrado = AddcelCrypto.encryptSensitive(AppUtils.getKey(), JSON.toString());
		Log.i(TAG, cifrado);
		return cifrado;
	}
	
	public static String buildTokenRequest(String usuario, String password, int producto) {

		JSONObject json = new JSONObject();
		
		try {
			json.put("idProveedor", producto);
			json.put("usuario", usuario);
			json.put("password", password);
			Log.d(TAG, json.toString());
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		return AddcelCrypto.encryptSensitive(Text.getKey(), json.toString());
	}
	
	public static String buildProsaRequest(Context con, JSONObject json, String idUsuario, int idProducto) {
		try {
			json.put("id_usuario", idUsuario);
			json.put("id_producto", idProducto);
			json.put("imei", DeviceUtils.getIMEI(con));
			json.put("tipo", DeviceUtils.getTipo());
			json.put("software", DeviceUtils.getSWVersion());
			json.put("modelo_procom", DeviceUtils.getModel());
			json.put("wkey", DeviceUtils.getIMEI(con));
			json.put("cx", "0.0");
			json.put("cy", "0.0");
			
			Log.i(TAG, json.toString());
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String key = Text.getKey();
		
		return AddcelCrypto.encryptSensitive(key, json.toString().replace("\\", ""));
	}
	
	public static String buildConsultaRequest(String idUsuario, int idProducto, String mes, String anio) {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.put("id_producto", idProducto);
			JSON.put("id_usuario", idUsuario);
			JSON.put("mes", mes);
			JSON.put("anio", anio);
			
			Log.i("consulta JSON", JSON.toString());
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String cifrado = AddcelCrypto.encryptSensitive(AppUtils.getKey(), JSON.toString());
		
		Log.i("cifrado consulta", cifrado);
		
		return cifrado;
	}
	
	public static String buildReenvioRequest(String idUsuario, String idBitacora, int idProducto, String email) {
		JSONObject j = new JSONObject();
		
		try {
			j.put("id_bitacora", idBitacora);
			j.put("id_producto", idProducto);
			j.put("id_usuario", idUsuario);
			
			if (null != email && !"".equals(email))
				j.put("email", email);
			
			Log.i(TAG, j.toString());
			
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		return AddcelCrypto.encryptHard(j.toString());
	}
	
	public static String buildTerminosRequest() {
		
		JSONObject JSON = new JSONObject();
		
		try {
			JSON.put("id_aplicacion", Terminos.ID_APLICACION);
			JSON.put("id_producto", Terminos.ID_PRODUCTO);
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String cifrado = AddcelCrypto.encryptHard(JSON.toString());
		
		Log.i("cifrado terminos", cifrado);
		
		return cifrado;
		
		
	}
}
