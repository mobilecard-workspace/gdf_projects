package org.addcel.gdf.constant;

public class Errors {
	
	public static final String CONNECTION_TIMEOUT = "Error de conexi�n con servidor. Intente de nuevo m�s tarde.";
	public static final String SOCKET_TIMEOUT = "Se ha cerrado la conexi�n con servidor. " +
												"En caso de transacci�n bancaria, verificar en \"Mis Pagos\" si el proceso ha finalizado exitosamente	.";

}
