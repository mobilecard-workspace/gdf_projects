package org.addcel.gdf.constant;

public class Tramite {
	public static final int TENENCIA = 0;
	public static final int INFRACCIONES = 1;
	public static final int T_CIRCULACION = 2;
	public static final int LICENCIA = 3;
	
	public static final int NOMINA = 0;
	
	public static final int PREDIAL = 0;
	public static final int PREDIAL_VENCIDO = 1;
	public static final int AGUA = 2;
	
	public static final int CERTIFICACION = 0;
	public static final int CONSTANCIA = 1;
}
