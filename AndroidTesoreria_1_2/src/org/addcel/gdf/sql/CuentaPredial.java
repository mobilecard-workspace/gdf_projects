package org.addcel.gdf.sql;

public class CuentaPredial {

	private long id;
	private String cuenta;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return cuenta;
	}
}
