package org.addcel.gdf.sql;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class PlacaDataSource {

	private SQLiteDatabase dataBase;
	private Mysqlite sqlWizard;
	private String[] columnas = {Mysqlite.COLUMN_ID, Mysqlite.COLUMN_PLACA};
	
	public PlacaDataSource(Context context) {
		// TODO Auto-generated constructor stub
		sqlWizard = new Mysqlite(context);
	}
	
	/**
	 * <p>Abre conexi�n SQL</p>
	 * @throws SQLException
	 */
	public void open() throws SQLException {
		dataBase = sqlWizard.getWritableDatabase();
	}
	
	/**
	 * Cierra conexi�n SQL
	 */
	public void close() {
		sqlWizard.close();
	};
	
	/**
	 * 
	 * @param placa
	 * @return long con n�mero insertado
	 */
	public long insertaNuevo(String placa) {
		long insertada = 0;
		ContentValues valores= new ContentValues();
		valores.put(Mysqlite.COLUMN_PLACA, placa);
		insertada = dataBase.insert(Mysqlite.TABLE_PLACAS, Mysqlite.COLUMN_ID, valores);
		
		return insertada;
	}
	
	public void eliminarPlaca(String placa) {
		String formattedPlaca = "\"" + placa + "\"";
		dataBase.delete(Mysqlite.TABLE_PLACAS, Mysqlite.COLUMN_PLACA + " = " + formattedPlaca, null);
	}
	
	public List<Placa> getAllPlacas() {
		List<Placa> placas = new ArrayList<Placa>();
		
		Cursor cursor = dataBase.query(Mysqlite.TABLE_PLACAS, columnas, null, null, null, null, null);
		cursor.moveToFirst();
		
		while (! cursor.isAfterLast()) {
			Placa placa = CursorAResultado(cursor);
			placas.add(placa);
			cursor.moveToNext();
		}
		
		cursor.close();
		return placas;
		
	}
	
	public Placa getPlaca() {
		Cursor cursor = dataBase.query(Mysqlite.TABLE_PLACAS, columnas, null, null, null, null, null);
		cursor.moveToFirst();
		Placa placa = CursorAResultado(cursor);
		cursor.close();
		
		return placa;
	}
	
	public Placa getLast() {
		Cursor cursor = dataBase.query(Mysqlite.TABLE_PLACAS, columnas, null, null, null, null, null);
		if(cursor.moveToLast()){
			Placa placa = CursorAResultado(cursor);
			cursor.close();
			return placa;
		} else {
			cursor.close();
			return null;
		}	
	}
	
	public boolean isOpen() {
		return dataBase.isOpen();
	}
	
	/**
	 * <p>Obtiene datos del registro en tabla</p>
	 * @param cursor
	 * @return Placa 
	 */
	public Placa CursorAResultado(Cursor cursor) {
		Placa existPlaca = new Placa();
		existPlaca.setId(cursor.getLong(0));
		existPlaca.setPlaca(cursor.getString(1));
		
		return existPlaca;
	}
}
