package org.addcel.gdf.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.gdf.sql.Placa;
import org.addcel.gdf.sql.PlacaDataSource;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.addcel.util.ConnectivityUtil;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.addcel.widget.SpinnerValues;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class TarjetaCirculacionActivity extends SherlockActivity {
	
	private Spinner marcaSpinner, modeloSpinner, placaSpinner;
	private SpinnerValues marcaValues;
	private EditText placaEdit, placaConfEdit;
	private TextView placaView, importeView, capturaView, totalView;
	private Button calcularButton, pagarButton;

	private boolean ingresoDirecto;
	private SessionManager session;
	private JSONObject requestPago;
	PlacaDataSource dataSource;
	
	private double 	montoMaximoOperacion;
	
	private static final String TAG = "TarjetaCirculacionActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tarjetacirculacion);
		session = new SessionManager(TarjetaCirculacionActivity.this);
		
		ingresoDirecto = true;
		initGUI();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_tenencia, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_placa:
				if (session.isLoggedIn()) {
					Dialogos.showPlacaDialog(TarjetaCirculacionActivity.this, dataSource, Servicios.TENENCIA);
				} else {
					CustomToast.build(TarjetaCirculacionActivity.this, "Debes iniciar sesi�n mobilecard para poder guardar una placa.");
//					Toast.makeText(TarjetaCirculacionActivity.this, "Debes iniciar sesi�n mobilecard para poder guardar una placa.", Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(TarjetaCirculacionActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(TarjetaCirculacionActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					CustomToast.build(TarjetaCirculacionActivity.this, "Ya eres usuario MobileCard.");
//					Toast.makeText(TarjetaCirculacionActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
					
				} else {
					startActivity(new Intent(TarjetaCirculacionActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(TarjetaCirculacionActivity.this, DataUpdateActivity.class));
				} else {
					CustomToast.build(TarjetaCirculacionActivity.this, "Inicia Sesi�n para actualizar tus datos");
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					CustomToast.build(TarjetaCirculacionActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(TarjetaCirculacionActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		dataSource = new PlacaDataSource(TarjetaCirculacionActivity.this);
		dataSource.open();
		List<Placa> placas = dataSource.getAllPlacas();
		ArrayAdapter<Placa> adapter = new ArrayAdapter<Placa>(TarjetaCirculacionActivity.this, android.R.layout.simple_spinner_item, placas);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		placaSpinner.setAdapter(adapter);
		dataSource.close();
		
		
		ArrayAdapter<String> modeloAdapter = new ArrayAdapter<String>(
				TarjetaCirculacionActivity.this, 
				android.R.layout.simple_spinner_item, 
				AppUtils.getAniosModelo());
		
		modeloAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		modeloSpinner.setAdapter(modeloAdapter);
		modeloSpinner.setSelection(1);
		
		new WebServiceClient(new MarcasListener(), TarjetaCirculacionActivity.this, true, Url.MARCAS).execute();
		
	}
	
	private void initGUI() {
		
		placaSpinner = (Spinner) findViewById(R.id.spinner_placa);
		placaEdit = (EditText) findViewById(R.id.edit_placa);
		placaConfEdit = (EditText) findViewById(R.id.edit_placa_conf);
		marcaSpinner = (Spinner) findViewById(R.id.spinner_marca);
		modeloSpinner = (Spinner) findViewById(R.id.spinner_modelo);
		
		calcularButton = (Button) findViewById(R.id.button_calcular);
		pagarButton = (Button) findViewById(R.id.button_pagar);
		
		placaView = (TextView) findViewById(R.id.view_placas);
		importeView = (TextView) findViewById(R.id.view_importe);
		capturaView = (TextView) findViewById(R.id.view_captura);
		totalView = (TextView) findViewById(R.id.view_total);
		placaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (0 == arg2) {
					ingresoDirecto = true;
					placaEdit.setVisibility(View.VISIBLE);
					placaConfEdit.setVisibility(View.VISIBLE);
				} else {
					ingresoDirecto = false;
					placaEdit.setVisibility(View.GONE);
					placaConfEdit.setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				ingresoDirecto = true;
				placaEdit.setVisibility(View.VISIBLE);
				placaConfEdit.setVisibility(View.VISIBLE);
			}
		});
		
		calcularButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (ConnectivityUtil.checkConnection(TarjetaCirculacionActivity.this)) {
					consulta();
				} else {
					CustomToast.build(TarjetaCirculacionActivity.this, "No hay conexi�n a internet disponible.");
					
				}
			}
		});
		
		pagarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				if (requestPago == null) 
					CustomToast.build(TarjetaCirculacionActivity.this, "Consulte datos de pago.");
				else {
					
					double montoMax = requestPago.optDouble("monto_maximo_operacion", 5000.0);
					
					if (requestPago.optDouble("totalPago", 0) <= montoMax) {

						Intent i = new Intent(TarjetaCirculacionActivity.this, PagoActivity.class);
						i.putExtra("datos", requestPago.toString());
						i.putExtra("servicio", Servicios.T_CIRCULACION);
						startActivity(i);
					} else {
						CustomToast.build(TarjetaCirculacionActivity.this, "El l�mite m�ximo para la transacci�n es: " + Text.formatCurrency(montoMax, true));
					}
				}
			}
		});
		

		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		

		initData();
	}
	
	private String getPlaca(boolean ingresoDirecto) {
		if (ingresoDirecto)
			return placaEdit.getText().toString().trim();
		else 
			return ((Placa) placaSpinner.getSelectedItem()).getPlaca();
	}
	
	private void consulta() {
		
		if (session.isLoggedIn()) {
			if (validar()) {
				String userId = session.getUserDetails().get(SessionManager.USR_ID);
				String placa = getPlaca(ingresoDirecto);
				String ejercicio = (String) modeloSpinner.getSelectedItem();
				String marca = marcaValues.getValorSeleccionado();
				
				String request = Request.buildTarjetaCirculacionRequest(userId, marca, ejercicio, placa);
				Log.i(TAG, request);
				new WebServiceClient(
						new ConsultaListener(), 
						TarjetaCirculacionActivity.this, 
						true, 
						Url.CONSUMIDOR).execute(request);
			}
		} else {
			CustomToast.build(TarjetaCirculacionActivity.this, "Debes iniciar sesi�n mobilecard para poder calcular tu pago.");
		}
	}
	
	private boolean validar() {
		if (ingresoDirecto) {
			
			String placa = placaEdit.getText().toString().trim();
			String confirmacion = placaConfEdit.getText().toString().trim();
			
			if (placa == null || "".equals(placa)) {
				placaEdit.requestFocus();
				placaEdit.setError("El campo placa no puede ir vac�o");
				return false;
			}
			
			if (confirmacion == null || "".equals(confirmacion)) {
				placaConfEdit.requestFocus();
				placaConfEdit.setError("El campo confirmaci�n no puede ir vac�o");
				return false;
			}
			
			if (!placa.equals(confirmacion)) {
				placaConfEdit.requestFocus();
				placaConfEdit.setError("Confirmaci�n no coincide con placa capturada");
				return false;
			}
			return true;
		} else {
			return true;
		}
	}
	
	private void cleanScreen() {
		placaView.setText("");
		importeView.setText("");
		capturaView.setText("");
		totalView.setText("");
	}
	
	
	
	private class MarcasListener implements WSResponseListener {

		private static final String TAG = "MarcasListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (!TextUtils.isEmpty(response)) {
				
				try {
					JSONObject json = new JSONObject(AddcelCrypto.decryptHard(response));
					
					String numError = json.optString("numError", "-10");
					
					if (!numError.equals("0")) {

						CustomToast.build(
								TarjetaCirculacionActivity.this, 
								json.optString("error", "Error de Conexi�n. Intentelo de nuevo m�s tarde."));
						finish();
					} else {
					
						if (!json.isNull("marcas") && json.optJSONArray("marcas").length() > 0) {
							
							JSONArray array = json.getJSONArray("marcas");
							ArrayList<BasicNameValuePair> marcas = new ArrayList<BasicNameValuePair>(array.length());
							
							for (int i = 0; i < array.length(); i++) {
								
								JSONObject marcaJSON = array.getJSONObject(i);
								
								BasicNameValuePair marca = new BasicNameValuePair(
										marcaJSON.getString("marca"),
										marcaJSON.getString("id_marca"));
								
								marcas.add(marca);
							}
							
							marcaValues = new SpinnerValues(TarjetaCirculacionActivity.this, marcaSpinner, marcas);
							
						} else {
							CustomToast.build(TarjetaCirculacionActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
							finish();
						}
					}
					
				} catch (JSONException e) {
					CustomToast.build(TarjetaCirculacionActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
					finish();
				}
				
			} else {

				CustomToast.build(TarjetaCirculacionActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
				finish();
			}
		}
	}
	
	private class ConsultaListener implements WSResponseListener {
		
		private static final String TAG = "ConsultaListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (!TextUtils.isEmpty(response)) {
				
				try {
					JSONObject json = new JSONObject(AddcelCrypto.decryptSensitive(response));
					Log.i(TAG, json.toString());
					
					String numError = json.optString("numError", "-10");
					
					if (!numError.equals("0")) {
						AlertDialog.Builder builder = new AlertDialog.Builder(TarjetaCirculacionActivity.this)
						.setTitle("Informaci�n")
						.setMessage(json.optString("error", "Error de conexi�n. Intente de nuevo m�s tarde."))
						.setPositiveButton("OK", new OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
							}
						});
						
						builder.show();
					} else {
						
						requestPago = json;
						
						try {
							
							double importe = json.optDouble("importeFinal");
							double totalPago = json.optDouble("totalPago");
							importeView.setText(Text.formatCurrency(importe, true));
							totalView.setText(Text.formatCurrency(totalPago, true));
						} catch (NumberFormatException e) {
							importeView.setText(String.valueOf(json.optDouble("importe")));
							totalView.setText(String.valueOf(json.optDouble("totalPago")));
							
						}
						
						placaView.setText(json.optString("placa"));
						capturaView.setText(json.optString("linea_captura"));
						
						
					}
					
					
				} catch (JSONException e) {
					cleanScreen();
					CustomToast.build(TarjetaCirculacionActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
					Log.e(TAG, "JSONException", e);
				}
			} else {
				cleanScreen();
				CustomToast.build(TarjetaCirculacionActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
			}
			
		}
	}

}
