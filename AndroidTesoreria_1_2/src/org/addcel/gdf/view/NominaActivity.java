package org.addcel.gdf.view;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.addcel.util.Validador;
import org.addcel.widget.SpinnerValues;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class NominaActivity extends SherlockActivity {
	
	SessionManager session;
	EditText rfcText, confRfcText, remuneracionesText, numTrabajadoresText;
	Spinner tipoSpinner, mesSpinner, anioSpinner;
	SpinnerValues tipoValues;
	TextView rfcView, anioView, mesView, tipoView, gravadasView, impuestoView;
	TextView actualizacionView, recargosView, capturaView, totalView;
	Button enviarButton, pagarButton;
	JSONObject requestPago;
	
	private static final String TAG = "NominaActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nomina);
		
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(NominaActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(NominaActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					CustomToast.build(NominaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(NominaActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(NominaActivity.this, DataUpdateActivity.class));
				} else {
					CustomToast.build(NominaActivity.this, "Inicia Sesi�n para actualizar tus datos");
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					CustomToast.build(NominaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(NominaActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		session = new SessionManager(NominaActivity.this);
	}
	
	public void initGUI() {
		initData();
		
		rfcText = (EditText) findViewById(R.id.edit_rfc);
		confRfcText = (EditText) findViewById(R.id.edit_rfc_conf);
		tipoSpinner = (Spinner) findViewById(R.id.spinner_tipo);
		remuneracionesText = (EditText) findViewById(R.id.edit_remuneraciones);
		numTrabajadoresText = (EditText) findViewById(R.id.edit_trabajadores);
		mesSpinner = (Spinner) findViewById(R.id.spinner_mes);
		anioSpinner = (Spinner) findViewById(R.id.spinner_anio);
		enviarButton = (Button) findViewById(R.id.button_enviar);
		pagarButton = (Button) findViewById(R.id.button_pagar);
		
		rfcView = (TextView) findViewById(R.id.view_rfc); 
		anioView = (TextView) findViewById(R.id.view_anio); 
		mesView = (TextView) findViewById(R.id.view_mes);
		tipoView = (TextView) findViewById(R.id.view_tipo);
		gravadasView = (TextView) findViewById(R.id.view_gravadas);
		impuestoView = (TextView) findViewById(R.id.view_impuesto);
		actualizacionView = (TextView) findViewById(R.id.view_actualizacion);
		recargosView = (TextView) findViewById(R.id.view_recargos);
		capturaView =  (TextView) findViewById(R.id.view_captura);
		totalView = (TextView) findViewById(R.id.view_total);
		
		enviarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				sendData();
			}
		});
		
		pagarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				executePayment();
			}
		});
		
		setTipoSpinner();
		setMesSpinner();
		setAnioSpinner();
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
	}
	
	private void setTipoSpinner() {
		String[] tipoNombres = getResources().getStringArray(R.array.arr_tipo_declaracion);
		String[] tipoValores = {"1", "2", "3"};
		
		tipoValues = new SpinnerValues(NominaActivity.this, tipoSpinner, tipoNombres, tipoValores);
	}
	
	private void setMesSpinner() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(NominaActivity.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.arr_mes));
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		mesSpinner.setAdapter(adapter);
	}
	
	private void setAnioSpinner() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(NominaActivity.this, android.R.layout.simple_spinner_item, AppUtils.getAniosConsulta());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		anioSpinner.setAdapter(adapter);
	}
	
	private boolean validar() {
		String rfc = rfcText.getText().toString().trim();
		String confRfc = confRfcText.getText().toString().trim();
		String remuneraciones = remuneracionesText.getText().toString().trim();
		String numTrabajadores = numTrabajadoresText.getText().toString().trim();
		
		if (rfc == null || "".equals(rfc)) {
			rfcText.requestFocus();
			rfcText.setError("El campo RFC no puede ir vac�o");
			return false;
		}
		
		if (!Validador.esRFC(rfc)) {
			rfcText.requestFocus();
			rfcText.setError("RFC no v�lido");
			return false;
			
		}
		
		if (confRfc == null || "".equals(confRfc)) {
			confRfcText.requestFocus();
			confRfcText.setError("El campo confirmaci�n de RFC no puede ir vac�o");
			return false;
		}
		
		if (!confRfc.equals(rfc)) {
			confRfcText.requestFocus();
			confRfcText.setError("RFCs capturados no coinciden");
			return false;
		}
		
		if (remuneraciones == null || "".equals(remuneraciones)) {
			remuneracionesText.requestFocus();
			remuneracionesText.setError("El campo remuneraciones no puede ir vac�o");
			return false;
		}
		
		if (numTrabajadores == null || "".equals(numTrabajadores)) {
			numTrabajadoresText.requestFocus();
			numTrabajadoresText.setError("El campo n�mero de trabajadores no puede ir vac�o");
			return false;
		}
		
		return true;
	}
	
	public void sendData() {
		if (validar()) {
			String rfc = rfcText.getText().toString().trim();
			int remuneraciones = Integer.parseInt(remuneracionesText.getText().toString().trim());
			int numTrabajadores = Integer.parseInt(numTrabajadoresText.getText().toString().trim());
			String mes = (String) mesSpinner.getSelectedItem();
			String anio = (String) anioSpinner.getSelectedItem();
			int tipo = Integer.valueOf(tipoValues.getValorSeleccionado());
			
			String request = Request.buildNominaRequest(rfc, mes, anio, tipo, remuneraciones, numTrabajadores);
			
			new WebServiceClient(new NominaListener(), NominaActivity.this, true, Url.CONSUMIDOR).execute(request);
		}
	}
	
	public void executePayment() {
		if (requestPago == null) 
			Toast.makeText(NominaActivity.this, "Env�e datos de pago.", Toast.LENGTH_SHORT).show();
		else{
			
			double montoMax = requestPago.optDouble("monto_maximo_operacion", 5000.0);
			
			if (requestPago.optDouble("totalPago", 0) <= montoMax) {
			
				Intent i = new Intent(NominaActivity.this, PagoActivity.class);
				i.putExtra("datos", requestPago.toString());
				i.putExtra("servicio", Servicios.NOMINA);
				startActivity(i);
			} else {
				CustomToast.build(NominaActivity.this, "El l�mite m�ximo para la transacci�n es: " + Text.formatCurrency(montoMax, true));
			}
		}
	}
	
	private void cleanScreen() {
		rfcView.setText("");
		anioView.setText("");
		mesView.setText("");
		tipoView.setText("");
		gravadasView.setText("");
		impuestoView.setText("");
		actualizacionView.setText("");
		recargosView.setText("");
		capturaView.setText("");
		totalView.setText("");
	}
	
	private class NominaListener implements WSResponseListener {
		
		private static final String TAG = "NominaListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				Log.i(TAG, response);
				
				response = AddcelCrypto.decryptSensitive(response);
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					
					String errorString = json.optString("error", "-1");
					String errorDescripcion = json.optString("error_descripcion", "Error de conexi�n. Intente de nuevo m�s tarde");
					
					int error = Integer.parseInt(errorString); 
					
					switch (error) {
					case 0:
						requestPago = json;
						
						String remuneraciones = Text.formatCurrency(json.optDouble("remuneraciones"), true);
						String impuesto = Text.formatCurrency(json.optDouble("impuesto"), true);
						String actualizacion = Text.formatCurrency(json.optDouble("impuesto_actualizado"), true);
						String recargos = Text.formatCurrency(json.optDouble("recargos"), true);
						String total = Text.formatCurrency(json.optDouble("totalPago"), true);
						
						int tipo = json.optInt("tipo_declaracion", 1);
						String tipoDeclaracion = "";
						
						if (tipo > 0) {
							tipoDeclaracion = getResources().getStringArray(R.array.arr_tipo_declaracion)[tipo - 1];
						} else {
							tipoDeclaracion = tipoValues.getTextoSeleccionado();
						}
						
						rfcView.setText(json.optString("rfc"));
						anioView.setText(json.optString("anio_pago"));
						mesView.setText(json.optString("mes_pago"));
						tipoView.setText(tipoDeclaracion);
						gravadasView.setText(remuneraciones);
						impuestoView.setText(impuesto);
						actualizacionView.setText(actualizacion);
						recargosView.setText(recargos);
						capturaView.setText(json.optString("linea_captura"));
						totalView.setText(total);
						
						break;

					default:
						cleanScreen();
						
						AlertDialog.Builder builder = new AlertDialog.Builder(NominaActivity.this)
						.setTitle("Informaci�n")
						.setMessage(errorDescripcion)						
						.setPositiveButton("OK", new OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
							}
						});
						
						builder.show();
						break;
					}
				} catch (NumberFormatException e) {
					cleanScreen();
					CustomToast.build(NominaActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
					Log.e(TAG, "", e);
				} catch (JSONException e) {
					cleanScreen();
					CustomToast.build(NominaActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
					Log.e(TAG, "", e);
				}
			} else {
				cleanScreen();
				CustomToast.build(NominaActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
			}
			
		}
	}
	
	
}
