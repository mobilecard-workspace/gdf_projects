package org.addcel.gdf.view;

import java.util.Calendar;
import java.util.Date;

import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.enums.Certificaciones;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class CertificacionFechaActivity extends SherlockActivity {
	
	private SessionManager session;
	private EditText fechaText, cajaText, partidaText, importeText;
	private Certificaciones certificacion;
	
	private DatePickerDialog dateDialog;
	private int anio, mes, dia;
	private Date nacimientoDate;
	
	JSONObject requestPago;
	
	private static final String TAG = "CertificacionFechaActivity";
	
	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public Date getNacimientoDate() {
		return nacimientoDate;
	}

	public void setNacimientoDate(Date nacimientoDate) {
		this.nacimientoDate = nacimientoDate;
	}
	
	public DatePickerDialog getDatePickerDialog() {
		if (dateDialog == null) {
			final Calendar c = Calendar.getInstance();
			
			dateDialog = new DatePickerDialog(CertificacionFechaActivity.this, new OnDateSetListener() {
				
				@Override
				public void onDateSet(DatePicker view, int year, int monthOfYear,
						int dayOfMonth) {
					// TODO Auto-generated method stub
					dia = dayOfMonth;
					mes = monthOfYear;
					anio = year;
					
					c.set(anio, mes, dia);
					
					setNacimientoDate(c.getTime());
					fechaText.setText(Text.formatDate(getNacimientoDate()));
					
				}
			}, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
			
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			
			Log.i(TAG, "Versi�n SO: " + currentapiVersion);

			if (currentapiVersion > 10) {
				dateDialog.getDatePicker().setCalendarViewShown(false);
			}
		}
		
		return dateDialog;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_certificacion_fecha);
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(CertificacionFechaActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(CertificacionFechaActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					CustomToast.build(CertificacionFechaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(CertificacionFechaActivity.this, CertificacionFechaActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(CertificacionFechaActivity.this, DataUpdateActivity.class));
				} else {
					CustomToast.build(CertificacionFechaActivity.this, "Inicia Sesi�n para actualizar tus datos");
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					CustomToast.build(CertificacionFechaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(CertificacionFechaActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		session = new SessionManager(CertificacionFechaActivity.this);
		certificacion = (Certificaciones) getIntent().getSerializableExtra("certificacion");
	}
	
	private void initGUI() {
		
		initData();
		
		fechaText = (EditText) findViewById(R.id.text_fecha); 
		cajaText = (EditText) findViewById(R.id.text_caja);  
		partidaText = (EditText) findViewById(R.id.text_partida); 
		importeText =  (EditText) findViewById(R.id.text_importe); 
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.header_text)).setText(certificacion.getLeyenda());
		
	}
	
	private boolean validar() {
		return true;
	}
	
	public void showFecha(View v) {
		
		if (!getDatePickerDialog().isShowing()) {
			getDatePickerDialog().show();
		}
	}
	
	public void consultar(View v) {

		if (validar()) {
			
		}
		
	}

	public void enviarDatos(View v) {

	}

}
