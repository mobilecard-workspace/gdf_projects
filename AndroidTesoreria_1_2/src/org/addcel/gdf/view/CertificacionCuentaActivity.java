package org.addcel.gdf.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.client.WSClient;
import org.addcel.crypto.Cifrados;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Url;
import org.addcel.gdf.enums.Certificables;
import org.addcel.gdf.enums.Certificaciones;
import org.addcel.gdf.enums.TiposCertificacion;
import org.addcel.gdf.to.Concepto;
import org.addcel.gdf.to.Parametro;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSRespListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.addcel.util.Validador;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class CertificacionCuentaActivity extends SherlockActivity {

	private SessionManager session;
	private Spinner tramiteSpinner;
	private TextView tipoView, complementoView;
	private EditText tipoText, complementoText;
	
	private Certificaciones certificacion;
	private TiposCertificacion tipo;
	private List<Concepto> conceptos;
	private Concepto selectedConcepto;
	

	private static final String TAG = "CertificacionCuentaActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_certificacion_cuenta);
		
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(CertificacionCuentaActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(CertificacionCuentaActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					CustomToast.build(CertificacionCuentaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(CertificacionCuentaActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(CertificacionCuentaActivity.this, DataUpdateActivity.class));
				} else {
					CustomToast.build(CertificacionCuentaActivity.this, "Inicia Sesi�n para actualizar tus datos");
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					CustomToast.build(CertificacionCuentaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(CertificacionCuentaActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		session = new SessionManager(CertificacionCuentaActivity.this);
		certificacion = (Certificaciones) getIntent().getSerializableExtra("certificacion");
	}
	
	private void initGUI() {
		
		initData();
		
		tramiteSpinner = (Spinner) findViewById(R.id.spinner_tramite);
		tipoView = (TextView) findViewById(R.id.view_type);
		tipoText = (EditText) findViewById(R.id.text_type);
		complementoView = (TextView) findViewById(R.id.view_complemento);
		complementoText = (EditText) findViewById(R.id.text_complemento);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.header_text)).setText(certificacion.getLeyenda());
		
		if (null == conceptos)
		
			new WSClient(CertificacionCuentaActivity.this).setCifrado(Cifrados.SENSITIVE)
													  .setListener(new CertificadosListener())
													  .setUrl(Url.CERTIFICADOS)
													  .execute();
	}
		
		
	
	private boolean validar() {
		
		String clave = tipoText.getText().toString().trim();
		String comp = complementoText.getText().toString().trim();
		
		switch (tipo) {
		case CUENTA:
			
			if (TextUtils.isEmpty(clave) || !TextUtils.isDigitsOnly(clave) || clave.length() < 12 || clave.length() > 12) {
				tipoText.requestFocus();
				tipoText.setError("Cuenta Predial no v�lida");
				
				return false;
			}
			
			return true;
		case PLACA:
			
			if (TextUtils.isEmpty(clave) || clave.length() > 7 || clave.length() < 5) {
				tipoText.requestFocus();
				tipoText.setError("Placa no v�lida");
				
				return false;
			}
			
			if (TextUtils.isEmpty(comp)) {
				complementoText.requestFocus();
				complementoText.setError("Ingrese n�mero de serie del veh�culo.");
				
				return false;
			}
			
			
			return true;
			
		case RFC:
			
			if (!Validador.esRFC(clave)) {
				tipoText.requestFocus();
				tipoText.setError("RFC no v�lido");
				
				return false;
			}
			
			if (TextUtils.isEmpty(comp)) {
				complementoText.requestFocus();
				complementoText.setError("Ingrese raz�n o Denominaci�n social.");
				
				return false;
			}
			
		
			return true;

		default:
			return true;
		}
		
	}
	
	public void consultar(View v) {
		
		if (session.isLoggedIn()) {
			
			if (validar()) {
				
			}
		} else {
			
		}
		
	}
	
	private class CertificadosListener implements WSRespListener {
		
		private static final String TAG = "CertificadosListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			
			Log.e(TAG, "Error al obtener certificados");
			
			CustomToast.buildLong(CertificacionCuentaActivity.this, "Error de conexi�n intente de nuevo m�s tarde");
			finish();
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			
			int numError = response.optInt("numError", 10000);
			String error = response.optString("error", "Error de conexi�n intente de nuevo m�s tarde");
			
			switch (numError) {
			case 0:
				
				conceptos = new ArrayList<Concepto>();
				
				JSONArray conArray = response.optJSONArray("conceptos");
				
				for (int i = 0; i < conArray.length(); i++) {
					
					Concepto c = new Concepto(conArray.optJSONObject(i));
					conceptos.add(c);
					
				}
				
				ArrayAdapter<Concepto> adapter = new ArrayAdapter<Concepto>(CertificacionCuentaActivity.this, R.layout.multiline_spinner_item, conceptos);
				adapter.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
				tramiteSpinner.setAdapter(adapter);
				tramiteSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						Concepto c = (Concepto) arg0.getAdapter().getItem(arg2);
						
						selectedConcepto = c;
						
						List<Parametro> params = c.getParametros();
						
						tipoView.setText(params.get(0).getNombreParam());
						
						InputFilter[] FilterArray = new InputFilter[1];
						FilterArray[0] = new InputFilter.LengthFilter(Integer.valueOf(params.get(0).getLongitudMax()));
						tipoText.setFilters(FilterArray);
						
						if (params.size() == 1) {
							
							complementoText.setVisibility(View.GONE);
							complementoView.setVisibility(View.GONE);
							
						} else if (params.size() == 2) {
							
							complementoView.setVisibility(View.VISIBLE);
							complementoView.setText(params.get(1).getNombreParam());
							
							InputFilter[] FilterArray2 = new InputFilter[1];
							FilterArray2[0] = new InputFilter.LengthFilter(Integer.valueOf(params.get(1).getLongitudMax()));
							complementoText.setFilters(FilterArray2);
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub
						
					}
				});
				
				
				break;

			default:
				
				CustomToast.buildLong(CertificacionCuentaActivity.this, error);
				finish();
				
				break;
			}
			
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub

			Log.e(TAG, "Error al obtener certificados");
			CustomToast.buildLong(CertificacionCuentaActivity.this, "Error de conexi�n intente de nuevo m�s tarde");
			finish();
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub

			Log.e(TAG, "Error al obtener certificados");
			CustomToast.buildLong(CertificacionCuentaActivity.this, "Error de conexi�n intente de nuevo m�s tarde");
			finish();
		}
		
		
		
	}
	
	
}
