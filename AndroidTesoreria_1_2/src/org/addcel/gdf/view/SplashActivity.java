package org.addcel.gdf.view;

import org.addcel.gdf.R;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.util.AppUtils;
import org.addcel.util.ConnectivityUtil;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.version.UrlVersion;
import org.addcel.version.VersionTO;
import org.addcel.version.VersionWSClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class SplashActivity extends Activity {
	
	VersionTO request;
	
	private static final String TAG = "SplashActivity";
	
	boolean production = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		if (production) {
		
			request = new VersionTO("4", "4");
			Log.i(TAG, request.toString());
			
			if (ConnectivityUtil.checkConnection(SplashActivity.this)){
				new VersionWSClient(new VersionListener(), UrlVersion.GET).execute(request);
				Log.i(TAG, "HAY INTERNET");
			} else {
				Log.i(TAG, "NO HAY INTERNET");
				Toast.makeText(SplashActivity.this, "Verifique su conexi�n a internet.",Toast.LENGTH_SHORT).show();
				finish();
			}
		} else {
			startActivity(new Intent(SplashActivity.this, MenuActivity.class));
		}
			
	}
	
	private class VersionListener implements WSResponseListener {

		private final static String TAG = "VersionListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				
				try {
					JSONObject json = new JSONObject(response);
					
					String version = AppUtils.getVersionName(SplashActivity.this, SplashActivity.class);
					
					String[] servVersion = json.getString("version").split("[.]");
					String[] currVersion = version.split("[.]");
					
					boolean pasa = true;
					
					for (int i = 0; i < servVersion.length; i++) {
						if (i < currVersion.length) {
							int max = Integer.parseInt(servVersion[i]);
							int min = Integer.parseInt(currVersion[i]);
							if (max > min) {
								pasa = false;
								break;
							}
						} else {
							break;
						}
					}
					
					if (pasa) {
						Intent intent = new Intent(SplashActivity.this, MenuActivity.class);
						startActivity(intent);
					} else {
						String prior = json.getString("tipo");
						final String url = json.getString("url");
						
						if (prior.equals("1")) {
							Dialogos.makeOkAlert(SplashActivity.this, "Hay nueva nueva versi�n importante. \n"+
									" su versi�n: "+version+"\n"+
									" nueva versi�n: "+json.getString("version"), new DialogOkListener() {
										
										@Override
										public void ok(DialogInterface dialog, int id) {
											// TODO Auto-generated method stub
											Intent intent = new Intent(Intent.ACTION_VIEW);
											intent.setData(Uri.parse(url));
											startActivity(intent);
											
										}
										
										@Override
										public void cancel(DialogInterface dialog, int id) {
											// TODO Auto-generated method stub
										}
									});
						} else {
							Dialogos.makeYesNoAlert(SplashActivity.this, "Hay nueva nueva versi�n disponible. \n "+
									" su version: "+version+"\n"+
									" nueva version: "+json.getString("version")+"\n"+
									" �Desea Descargarla?", new DialogOkListener() {
										
										@Override
										public void ok(DialogInterface dialog, int id) {
											// TODO Auto-generated method stub
											Intent intent = new Intent(Intent.ACTION_VIEW);
											intent.setData(Uri.parse(url));
											startActivity(intent);
										}
										
										@Override
										public void cancel(DialogInterface dialog, int id) {
											// TODO Auto-generated method stub
											Intent intent = new Intent(SplashActivity.this, MenuActivity.class);
											startActivity(intent);
										}
									});
						}
					}
					
					
					
				} catch (JSONException e) {
					CustomToast.build(SplashActivity.this, "No hay conexi�n de internet disponible.");
					finish();
				}
			
			} else {
				CustomToast.build(SplashActivity.this, "No hay conexi�n de internet disponible.");
				finish();
			}
		}
		
	}
}
