package org.addcel.gdf.view;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class DetallePredialVencidoActivity extends SherlockActivity {
	
	String jsonString;
	JSONObject json;
	SessionManager session;
	
	LinearLayout detalleLayout;
	
	TextView actualizacionView, ejecucionView, ejecucionCondView,
	recargosView, multaView, multaCondView, capturaView, totalView;

	double emitido, pagado, actualizacion, ejecucion, ejecucionCondonados, recargos, multa, multaCondonada;
	
	private static final String TAG = "DetallePredialVencidoActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalle_predial_vencido);
		
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_reenvio, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		int itemId = item.getItemId();
		
		switch (itemId) {
		case R.id.action_reenviar:
			executeReenvio();
			break;

		case R.id.action_update:
			startActivity(new Intent(DetallePredialVencidoActivity.this, DataUpdateActivity.class));
			break;
			
		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		session = new SessionManager(DetallePredialVencidoActivity.this);
		jsonString = getIntent().getStringExtra("json");
		
		Log.d(TAG, "JSON a mostrar: " + jsonString);
		
		try {
			json = new JSONObject(jsonString);
		} catch (JSONException e) {
			Log.e(TAG, "", e);
		}
	}
	
	public void initGUI() {
		initData();		
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.text_secretaria)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.text_finanzas)).setTypeface(gothamBold);
	
		detalleLayout = (LinearLayout) findViewById(R.id.layout_detalle);
		actualizacionView = (TextView) findViewById(R.id.view_actualizacion);
		ejecucionView = (TextView) findViewById(R.id.view_ejecucion);
		ejecucionCondView = (TextView) findViewById(R.id.view_ejecucion_cond);
		recargosView = (TextView) findViewById(R.id.view_recargos);
		multaView = (TextView) findViewById(R.id.view_multa);
		multaCondView = (TextView) findViewById(R.id.view_multa_cond);
		capturaView = (TextView) findViewById(R.id.view_captura);
		totalView = (TextView) findViewById(R.id.view_total);
		
		String linea = json.optString("linea_captura");
		String totalCuenta = Text.formatCurrency(json.optDouble("total_cuenta", 0.0), true);
		
		JSONArray detalle = json.optJSONArray("detalle");
		
		for (int i = 0; i < detalle.length(); i++) {
			JSONObject detalleJSON = detalle.optJSONObject(i);
			detalleLayout.addView(buildDetalleLayout(detalleJSON));
			
			if (i < detalle.length() - 1) {
				detalleLayout.addView(buildSeparator());
			}
			
			multaCondonada += detalleJSON.optDouble("cond_mult_omi");
			ejecucionCondonados += detalleJSON.optDouble("cond_Gast_Ejec");
		}
		
		actualizacionView.setText(Text.formatCurrency(json.optDouble("sumActualizacion"), true));
		ejecucionView.setText(Text.formatCurrency(json.optDouble("gastos_ejecucion"), true));
		ejecucionCondView.setText(Text.formatCurrency(ejecucionCondonados, true));
		recargosView.setText(Text.formatCurrency(json.optDouble("sumRecargos"), true));
		multaView.setText(Text.formatCurrency(json.optDouble("sumMultas"), true));
		multaCondView.setText(Text.formatCurrency(multaCondonada, true));
		
		capturaView.setText(linea);
		totalView.setText(totalCuenta);
	}
	
	private LinearLayout buildDetalleLayout(JSONObject detalle) {
		
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(0, 0, 0, 0);
		
		LinearLayout detalleLayout = new LinearLayout(DetallePredialVencidoActivity.this);
		detalleLayout.setOrientation(LinearLayout.VERTICAL);
		detalleLayout.setLayoutParams(params);
		
		String actualizacionStr = Text.formatCurrency(detalle.optDouble("actualizacion"), true);
		String recargoStr = Text.formatCurrency(detalle.optDouble("recargos"), true);
		String multaStr = Text.formatCurrency(detalle.optDouble("multa_omision"), true);
		String subtotalStr = Text.formatCurrency(detalle.optDouble("subtotal"), true);
		
		LinearLayout periodo = buildRowLayout("Periodo", detalle.optString("periodo"));
		LinearLayout actualizacion = buildRowLayout("Autorizaci�n", actualizacionStr);
		LinearLayout recargo = buildRowLayout("Recargo", recargoStr);
		LinearLayout multa = buildRowLayout("Multa", multaStr);
		LinearLayout subtotal = buildRowLayout("Subtotal Periodo", subtotalStr);
		
		detalleLayout.addView(periodo);
		detalleLayout.addView(actualizacion);
		detalleLayout.addView(recargo);
		detalleLayout.addView(multa);
		detalleLayout.addView(subtotal);
		
		
		return detalleLayout;
	}
	
	private View buildSeparator() {
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, 1);
		params.setMargins(0, 4, 0, 4);
		
		View separator = new View(DetallePredialVencidoActivity.this);
		separator.setLayoutParams(params);
		separator.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
		
		return separator;
		
	}
	
	private LinearLayout buildRowLayout(String label, String valor) {
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		LinearLayout row = new LinearLayout(DetallePredialVencidoActivity.this);
		row.setLayoutParams(params);
		row.setOrientation(LinearLayout.HORIZONTAL);
		TextView labelView = buildText(true, label);
		row.addView(labelView);
		TextView valorView = buildText(false, valor);
		row.addView(valorView);
		
		return row;
	}
	
	private TextView buildText(boolean label, String text) {
		LayoutParams params = new android.widget.LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f);
		params.setMargins(8, 0, 8, 0);
		
		TextView view = new TextView(DetallePredialVencidoActivity.this);
		view.setLayoutParams(params);
		view.setText(text);
		view.setTextAppearance(DetallePredialVencidoActivity.this, android.R.style.TextAppearance_Small);
		
		if (label) {
			view.setTextColor(getResources().getColor(R.color.gdf_gris));
			view.setGravity(Gravity.LEFT);
		}
		else {
			view.setTextColor(getResources().getColor(android.R.color.darker_gray));
			view.setGravity(Gravity.RIGHT);
		}
		
		return view;
		
	}
	
	public void executeReenvio() {
		final String idUsuario = session.getUserDetails().get(SessionManager.USR_ID);
		final String idBitacora = json.optString("id_bitacora");
		final int idProducto = Servicios.PREDIAL_VENCIDOS;
		
		Dialogos.makeReenvioAlert(DetallePredialVencidoActivity.this, "�Desea reenviar recibo de pago?", new DialogOkListener() {
			
			@Override
			public void ok(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				String request = Request.buildReenvioRequest(idUsuario, idBitacora, idProducto, null);
				new WebServiceClient(new ReenvioListener(), DetallePredialVencidoActivity.this, true, Url.REENVIAR_RECIBO).execute(request);
			}
			
			@Override
			public void cancel(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				
				try {
					JSONObject json = new JSONObject();
					json.put("idUsuario", idUsuario);
					json.put("idBitacora", idBitacora);
					json.put("idProducto", idProducto);
					WebServiceClient client = new WebServiceClient(new ReenvioListener(), DetallePredialVencidoActivity.this, true, Url.REENVIAR_RECIBO);
					Dialogos.showEmailDialog(DetallePredialVencidoActivity.this, json, client);
				} catch (JSONException e) {
					Log.e(TAG, "", e);
				}
				
			}
		});
	}
	
	private class ReenvioListener implements WSResponseListener {
		private static final String TAG = "ReenvioListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					
					String error = json.optString("error", "Error de conexi�n. Intente de nuevo m�s tarde");
					CustomToast.build(DetallePredialVencidoActivity.this, error);
					
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					CustomToast.build(DetallePredialVencidoActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
				}
			} else {
				CustomToast.build(DetallePredialVencidoActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
			}
		}
	}
}

