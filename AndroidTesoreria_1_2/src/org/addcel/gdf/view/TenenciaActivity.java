package org.addcel.gdf.view;

import java.util.List;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.gdf.sql.Placa;
import org.addcel.gdf.sql.PlacaDataSource;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.ConnectivityUtil;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;

public class TenenciaActivity extends SherlockActivity {
	
	private Spinner placaSpinner, ejercicioSpinner;
	private EditText placaEdit, placaConfEdit;
	private TextView tenenciaView, subsidioView, recTenenciaView, actTenenciaView, 
					 derechosView, recDerechoView, actDerechoView, capturaView, totalView ;
	private Button calcularButton, pagarButton;
	private boolean ingresoDirecto;
	private SessionManager session;
	private JSONObject requestPago;
	PlacaDataSource dataSource;
	
	private double 	montoMaximoOperacion;
	
	private static final String TAG = "TenenciaActivity";
	
	private static final int ZBAR_SCANNER_REQUEST = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tenencia);
		session = new SessionManager(TenenciaActivity.this);
		
		ingresoDirecto = true;
		initGUI();
		
		

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_tenencia, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_placa:
				if (session.isLoggedIn()) {
					Dialogos.showPlacaDialog(TenenciaActivity.this, dataSource, Servicios.TENENCIA);
				} else {
					CustomToast.build(TenenciaActivity.this, "Debes iniciar sesi�n mobilecard para poder guardar una placa.");
//					Toast.makeText(TenenciaActivity.this, "Debes iniciar sesi�n mobilecard para poder guardar una placa.", Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(TenenciaActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(TenenciaActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					CustomToast.build(TenenciaActivity.this, "Ya eres usuario MobileCard.");
//					Toast.makeText(TenenciaActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
					
				} else {
					startActivity(new Intent(TenenciaActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(TenenciaActivity.this, DataUpdateActivity.class));
				} else {
					CustomToast.build(TenenciaActivity.this, "Inicia Sesi�n para actualizar tus datos");
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					CustomToast.build(TenenciaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(TenenciaActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		dataSource = new PlacaDataSource(TenenciaActivity.this);
		dataSource.open();
		List<Placa> placas = dataSource.getAllPlacas();
		ArrayAdapter<Placa> adapter = new ArrayAdapter<Placa>(TenenciaActivity.this, android.R.layout.simple_spinner_item, placas);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		placaSpinner.setAdapter(adapter);
		dataSource.close();
	}
	
	private void initGUI() {
		placaSpinner = (Spinner) findViewById(R.id.spinner_placa);
		ejercicioSpinner = (Spinner) findViewById(R.id.spinner_ejercicio);
		placaEdit = (EditText) findViewById(R.id.edit_placa);
		placaConfEdit = (EditText) findViewById(R.id.edit_placa_conf);
		calcularButton = (Button) findViewById(R.id.button_calcular);
		pagarButton = (Button) findViewById(R.id.button_pagar);
		
		tenenciaView = (TextView) findViewById(R.id.view_tenencia);
		subsidioView = (TextView) findViewById(R.id.view_subsidio);
		recTenenciaView = (TextView) findViewById(R.id.view_ten_recargo);
		actTenenciaView = (TextView) findViewById(R.id.view_actualizacion);
		derechosView = (TextView) findViewById(R.id.view_derecho);
		recDerechoView = (TextView) findViewById(R.id.view_der_recargo);
		actDerechoView = (TextView) findViewById(R.id.view_der_actualizacion);
		capturaView = (TextView) findViewById(R.id.view_captura);
		totalView = (TextView) findViewById(R.id.view_total);
		placaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (0 == arg2) {
					ingresoDirecto = true;
					placaEdit.setVisibility(View.VISIBLE);
					placaConfEdit.setVisibility(View.VISIBLE);
				} else {
					ingresoDirecto = false;
					placaEdit.setVisibility(View.GONE);
					placaConfEdit.setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				ingresoDirecto = true;
				placaEdit.setVisibility(View.VISIBLE);
				placaConfEdit.setVisibility(View.VISIBLE);
			}
		});
		
		calcularButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (ConnectivityUtil.checkConnection(TenenciaActivity.this)) {
					consulta();
				} else {
					CustomToast.build(TenenciaActivity.this, "No hay conexi�n a internet disponible.");
					
				}
			}
		});
		
		pagarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				if (requestPago == null) 
					CustomToast.build(TenenciaActivity.this, "Consulte datos de pago.");
				else {
					
					double montoMax = requestPago.optDouble("monto_maximo_operacion", 5000.0);
					
					if (requestPago.optDouble("totalPago", 0) <= montoMax) {

						Intent i = new Intent(TenenciaActivity.this, PagoActivity.class);
						i.putExtra("datos", requestPago.toString());
						i.putExtra("servicio", Servicios.TENENCIA);
						startActivity(i);
					} else {
						CustomToast.build(TenenciaActivity.this, "El l�mite m�ximo para la transacci�n es: " + Text.formatCurrency(montoMax, true));
					}
				}
			}
		});
		

		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		
		initData();
	}
	
	private String getPlaca(boolean ingresoDirecto) {
		if (ingresoDirecto)
			return placaEdit.getText().toString().trim();
		else 
			return ((Placa) placaSpinner.getSelectedItem()).getPlaca();
	}
	
	private void consulta() {
		
		if (session.isLoggedIn()) {
			if (validar()) {
				String userId = session.getUserDetails().get(SessionManager.USR_ID);
				String password = session.getUserDetails().get(SessionManager.USR_PWD);
				String placa = getPlaca(ingresoDirecto);
				String ejercicio = (String) ejercicioSpinner.getSelectedItem();
				
				String request = Request.buildTenenciaRequest(userId, password, placa, ejercicio);
				Log.i(TAG, request);
				new WebServiceClient(new ConsultaListener(), TenenciaActivity.this, true, Url.CONSUMIDOR).execute(request);
			}
		} else {
			CustomToast.build(TenenciaActivity.this, "Debes iniciar sesi�n mobilecard para poder calcular tu pago.");
		}
	}
	
	private boolean validar() {
		if (ingresoDirecto) {
			
			String placa = placaEdit.getText().toString().trim();
			String confirmacion = placaConfEdit.getText().toString().trim();
			
			if (placa == null || "".equals(placa)) {
				placaEdit.requestFocus();
				placaEdit.setError("El campo placa no puede ir vac�o");
				return false;
			}
			
			if (confirmacion == null || "".equals(confirmacion)) {
				placaConfEdit.requestFocus();
				placaConfEdit.setError("El campo confirmaci�n no puede ir vac�o");
				return false;
			}
			
			if (!placa.equals(confirmacion)) {
				placaConfEdit.requestFocus();
				placaConfEdit.setError("Confirmaci�n no coincide con placa capturada");
				return false;
			}
			return true;
		} else {
			return true;
		}
	}
	
	private void cleanScreen() {
		tenenciaView.setText("");
		recTenenciaView.setText("");
		derechosView.setText("");
		recDerechoView.setText("");
		actTenenciaView.setText("");
		actDerechoView.setText("");
		capturaView.setText("");
		totalView.setText("");
	}
	
	public void showPopup(View v) {
		if (null != v) {
			Dialogos.imgPopUpDialog(TenenciaActivity.this, Servicios.TENENCIA);
		}
	}
	
	public void callScanner(View v) {
		if (null != v) {
			
			if (session.isLoggedIn()) {
				Intent intent = new Intent(TenenciaActivity.this, ZBarScannerActivity.class);
				startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
			} else {
				CustomToast.build(TenenciaActivity.this, "Debes iniciar sesi�n mobilecard para poder calcular tu pago.");
			}
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case ZBAR_SCANNER_REQUEST:
			if (resultCode == RESULT_OK) {
				String placa = data.getStringExtra(ZBarConstants.SCAN_RESULT);
				
				if (!TextUtils.isEmpty(placa)) {
					
					placaSpinner.setSelection(0);
					placaEdit.setText(placa);
					placaConfEdit.setText(placa);
					
					consulta();
				} else {
					CustomToast.build(TenenciaActivity.this, data.getStringExtra(ZBarConstants.ERROR_INFO));
				}
			}
			break;

		default:
			cleanScreen();
			break;
		}
		
	}
	
	private class ConsultaListener implements WSResponseListener {
		
		private static final String TAG = "ConsultaListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (response != null && !"".equals(response)) {
				response = AddcelCrypto.decryptSensitive(response);
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					
					String numError = json.optString("numError", "");
					String error = json.optString("error","Error de conexi�n int�ntelo de nuevo m�s tarde");
					
					if (numError.equals("")) {
					
						requestPago = json;
						
						String tenencia = json.optString("tenencia");
						double tenenciaD = Double.parseDouble(tenencia);
						
						String subsidio = json.optString("tenSubsidio");
						double subsidioD = Double.parseDouble(subsidio);
						
						String tenRecargo = json.optString("tenRecargo");
						double tenRecargoD = Double.parseDouble(tenRecargo);
						
						String derechos = json.optString("derechos");
						double derechosD = Double.parseDouble(derechos);
						
						String derRecargo = json.optString("derRecargo");
						double derRecargoD = Double.parseDouble(derRecargo);
						
						
//						POSIBLEMENTE SE CAMBIAR� A "tenActualizacion"
						String actualizacion = json.optString("tenActualizacion");
						double actualizacionD = Double.parseDouble(actualizacion);
						
						String derActualizacion = json.optString("derActualizacion");
						double derActualizacionD = Double.parseDouble(derActualizacion);
						
						String lineaCaptura = json.optString("linea_captura");
						
						String totalPago = json.optString("totalPago");
						double totalPagoD = Double.parseDouble(totalPago);
						
						tenenciaView.setText(Text.formatCurrency(tenenciaD, true));
						subsidioView.setText(Text.formatCurrency(subsidioD, true));
						recTenenciaView.setText(Text.formatCurrency(tenRecargoD, true));
						derechosView.setText(Text.formatCurrency(derechosD, true));
						recDerechoView.setText(Text.formatCurrency(derRecargoD, true));
						actTenenciaView.setText(Text.formatCurrency(actualizacionD, true));
						actDerechoView.setText(Text.formatCurrency(derActualizacionD, true));
						capturaView.setText(lineaCaptura);
						totalView.setText(Text.formatCurrency(totalPagoD, true));
					} else {
						cleanScreen();
						Log.d(TAG, error);
						
						AlertDialog.Builder builder = new AlertDialog.Builder(TenenciaActivity.this)
						.setTitle("Informaci�n")
						.setMessage(error)						
						.setPositiveButton("OK", new OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
							}
						});
						
						builder.show();
					}
					
				} catch (JSONException e) {
					cleanScreen();
					CustomToast.build(TenenciaActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
//					Toast.makeText(TenenciaActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.",  Toast.LENGTH_SHORT).show();
					Log.e(TAG, "JSONException", e);
				}
			} else {
				cleanScreen();
				CustomToast.build(TenenciaActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
//				Toast.makeText(TenenciaActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.",  Toast.LENGTH_SHORT).show();
			}
			
		}
	}
	
	
}
