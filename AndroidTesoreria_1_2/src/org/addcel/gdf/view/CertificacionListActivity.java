package org.addcel.gdf.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.adapter.CertificacionesAdapter;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.enums.Certificaciones;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.Dialogos;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class CertificacionListActivity extends SherlockActivity {
	
	
	ListView tiposCertificacionList;
	SessionManager session;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		session = new SessionManager(CertificacionListActivity.this);
		
		setContentView(R.layout.activity_list_certificacion);
		
		((TextView) findViewById(R.id.view_titulo)).setTypeface(gothamBold);
		
		if (!session.isLoggedIn())
			Dialogos.showInitialDialog(CertificacionListActivity.this);
		
		tiposCertificacionList = (ListView) findViewById(R.id.list_tipo_certificacion);
		
		List<Certificaciones> certificaciones = new ArrayList<Certificaciones>(Arrays.asList(Certificaciones.values()));
		
		CertificacionesAdapter adapter = new CertificacionesAdapter(CertificacionListActivity.this, certificaciones);
		tiposCertificacionList.setAdapter(adapter);
		tiposCertificacionList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Certificaciones c = (Certificaciones) ((CertificacionesAdapter) arg0.getAdapter()).getItem(arg2);
				
				Intent intent = null;
				
				switch (c) {
				
				case CUENTA:
					intent = new Intent(CertificacionListActivity.this, CertificacionCuentaActivity.class);
					break;
				case FECHA:
					intent = new Intent(CertificacionListActivity.this, CertificacionFechaActivity.class);
					break;
					
				case LINEA:
					intent = new Intent(CertificacionListActivity.this, CertificacionLCActivity.class);
					break;
				default:
					break;
				}
				
				intent.putExtra("certificacion", c);
				startActivity(intent);
			}
		});
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
		case R.id.action_login:
			if (!session.isLoggedIn()) {
				startActivity(new Intent(CertificacionListActivity.this, LoginActivity.class));
			} else {
				Dialogos.makeYesNoAlert(CertificacionListActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
					
					@Override
					public void ok(DialogInterface dialog, int id) {
						// TODO Auto-generated method stub
						session.logoutUser();
						dialog.dismiss();
					}
					
					@Override
					public void cancel(DialogInterface dialog, int id) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
			}
			break;
		case R.id.action_registro:
			if (session.isLoggedIn()) {
				Toast.makeText(CertificacionListActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
			} else {
				startActivity(new Intent(CertificacionListActivity.this, RegistroActivity.class));
			}
			break;
		case R.id.action_update:
			if (session.isLoggedIn()) {
				startActivity(new Intent(CertificacionListActivity.this, DataUpdateActivity.class));
			} else {
				Toast.makeText(CertificacionListActivity.this, "Inicia Sesi�n para actualizar tus datos", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.action_password:
			if (session.isLoggedIn()) {
				Toast.makeText(CertificacionListActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
			} else {
				startActivity(new Intent(CertificacionListActivity.this, PassRecoverActivity.class));
			}
			break;

		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	

}
