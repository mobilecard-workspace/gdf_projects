package org.addcel.gdf.view;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.gdf.enums.Licencias;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class DetalleLicenciaActivity extends SherlockActivity {
	
	String jsonString;
	JSONObject json;
	SessionManager session;
	Licencias tipoLicencia;
	int idProducto;
	
	TextView rfcView, importeView, capturaView, totalView;
	
	private static final String TAG = "DetalleLicenciaActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalle_licencia);
		
		initGUI();
	}
	
	private void initData() {
		
		session = new SessionManager(DetalleLicenciaActivity.this);
		tipoLicencia = (Licencias) getIntent().getSerializableExtra("tipo_licencia");
		jsonString = getIntent().getStringExtra("json");
		
		switch (tipoLicencia) {
		case TRES_ANIOS:
			idProducto = Servicios.LICENCIA;
			break;
		case PERMANENTE:
			idProducto = Servicios.LICENCIA_PERMANENTE;
			break;
		default:
			break;
		}
		
		try {
			json = new JSONObject(jsonString);
		} catch (JSONException e) {
			Log.e(TAG, "", e);
		}
	}
	
	public void initGUI() {
		
		initData();

		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.text_secretaria)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.text_finanzas)).setTypeface(gothamBold);
		
		rfcView = (TextView) findViewById(R.id.view_rfc); 
		importeView = (TextView) findViewById(R.id.view_importe);
		capturaView =  (TextView) findViewById(R.id.view_captura);
		totalView = (TextView) findViewById(R.id.view_total);
		
		double importeFinal = json.optDouble("importeFinal");
		double totalPago = Double.parseDouble(json.optString("totalPago"));
		
		rfcView.setText(json.optString("rfc"));
		importeView.setText(Text.formatCurrency(importeFinal, true));
		capturaView.setText(json.optString("linea_captura"));
		totalView.setText(Text.formatCurrency(totalPago, true));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_reenvio, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		int itemId = item.getItemId();
		
		switch (itemId) {
		case R.id.action_reenviar:
			executeReenvio();
			break;
			
		case R.id.action_update:
			startActivity(new Intent(DetalleLicenciaActivity.this, DataUpdateActivity.class));
			break;
		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	public void executeReenvio() {
		
		final String idUsuario = session.getUserDetails().get(SessionManager.USR_ID);
		final String idBitacora = json.optString("id_bitacora");
		
		Dialogos.makeReenvioAlert(DetalleLicenciaActivity.this, "�Desea reenviar recibo de pago?", new DialogOkListener() {
			
			@Override
			public void ok(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				String request = Request.buildReenvioRequest(idUsuario, idBitacora, idProducto, null);
					
				new WebServiceClient(new ReenvioListener(), DetalleLicenciaActivity.this, true, Url.REENVIAR_RECIBO).execute(request);
			}
			
			@Override
			public void cancel(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				
				try {
					JSONObject json = new JSONObject();
					json.put("idUsuario", idUsuario);
					json.put("idBitacora", idBitacora);
					json.put("idProducto", idProducto);
					
					WebServiceClient client = new WebServiceClient(new ReenvioListener(), DetalleLicenciaActivity.this, true, Url.REENVIAR_RECIBO);
					Dialogos.showEmailDialog(DetalleLicenciaActivity.this, json, client);
				} catch (JSONException e) {
					Log.e(TAG, "", e);
				}
				
			}
		});
		
	}

	private class ReenvioListener implements WSResponseListener {
		
		private static final String TAG = "ReenvioListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					
					String error = json.optString("error", "Error de conexi�n. Intente de nuevo m�s tarde");
					Toast.makeText(DetalleLicenciaActivity.this, error, Toast.LENGTH_SHORT).show();
					
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					Toast.makeText(DetalleLicenciaActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(DetalleLicenciaActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
			}
			
			
		}
	}

}
