package org.addcel.gdf.view;

import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.enums.Certificaciones;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class CertificacionLCActivity extends SherlockActivity {

	private SessionManager session;
	private EditText lcText;
	private EditText importeText;
	private Certificaciones certificacion;
	
	JSONObject requestPago;

	private static final String TAG = "CertificacionLCActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_certificacion_lc);
		
		initGUI();
		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(CertificacionLCActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(CertificacionLCActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					CustomToast.build(CertificacionLCActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(CertificacionLCActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(CertificacionLCActivity.this, DataUpdateActivity.class));
				} else {
					CustomToast.build(CertificacionLCActivity.this, "Inicia Sesi�n para actualizar tus datos");
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					CustomToast.build(CertificacionLCActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(CertificacionLCActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}

	private void initData() {
		session = new SessionManager(CertificacionLCActivity.this);
		certificacion = (Certificaciones) getIntent().getSerializableExtra("certificacion");
	}
	
	private void initGUI() {
		
		initData();

		lcText = (EditText) findViewById(R.id.text_lc);
		importeText = (EditText) findViewById(R.id.text_importe);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.header_text)).setText(certificacion.getLeyenda());
	
	
	}

	private boolean validar() {

		String lc = lcText.getText().toString().trim();
		String importe = importeText.getText().toString().trim();

		if (TextUtils.isEmpty(lc)) {
			lcText.requestFocus();
			lcText.setError("El campo l�nea de captura no puede ir vac�o.");
			return false;
		}

		if (TextUtils.isEmpty(importe) || !TextUtils.isDigitsOnly(importe)) {
			importeText.requestFocus();
			importeText.setError("Importe no v�lido.");
			return false;
		}

		return true;
	}

	public void consultar(View v) {

		if (validar()) {
			
		}
		
	}

	public void enviarDatos(View v) {

	}

}
