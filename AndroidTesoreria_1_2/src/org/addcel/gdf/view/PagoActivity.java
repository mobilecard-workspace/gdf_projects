package org.addcel.gdf.view;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Errors;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Keys;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.gdf.enums.Licencias;
import org.addcel.gdf.to.DetalleTO;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.ConnectivityUtil;
import org.addcel.util.CustomToast;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class PagoActivity extends SherlockActivity {

	private SessionManager session;
	private String datos;
	private int servicio;

	private JSONObject requestPago;

	private TextView servicioView, montoView, capturaView;
	private EditText cvvText, passwordText;
	private Button pagarButton;

	private static final String TAG = "PagoActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pago);

		session = new SessionManager(PagoActivity.this);

		initGUI();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_pago, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int itemId = item.getItemId();

		switch (itemId) {
		case R.id.action_update:
			Log.i(TAG, "Bot�n actualizar");
			startActivity(new Intent(PagoActivity.this,
					DataUpdateActivity.class));
			break;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	private void initData() {
		datos = getIntent().getStringExtra("datos");
		servicio = getIntent().getIntExtra("servicio", -1);
		setRequestPago();
	}

	private void initGUI() {
		initData();

		Typeface gothamBook = Typeface.createFromAsset(getAssets(),
				Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(),
				Fonts.GOTHAM_BOLD);

		((TextView) findViewById(R.id.text_secretaria)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.text_finanzas)).setTypeface(gothamBold);

		servicioView = (TextView) findViewById(R.id.text_servicio);
		montoView = (TextView) findViewById(R.id.text_monto);
		capturaView = (TextView) findViewById(R.id.text_captura);

		cvvText = (EditText) findViewById(R.id.edit_cvv2);
		passwordText = (EditText) findViewById(R.id.edit_password);
		pagarButton = (Button) findViewById(R.id.button_pagar);

		pagarButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (validateData())
					executePayment();
			}
		});

		loadData();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		pagarButton.setEnabled(true);
		super.onRestart();
	}

	private void executePayment() {
		
		if (ConnectivityUtil.checkConnection(PagoActivity.this)){
		
			pagarButton.setEnabled(false);
			String request = Request.buildTokenRequest(Keys.USER, Keys.PASS,
					Keys.PROVEEDOR);
			new WebServiceClient(new TokenListener(), PagoActivity.this, true,
					Url.GET_TOKEN).execute(request);
		} else {

			Log.i(TAG, "NO HAY INTERNET");
			CustomToast.build(PagoActivity.this, "Verifica tu conexi�n a internet para completar la transacci�n.");
		}
	}

	private void loadData() {
		servicioView.setText(getServicioName(servicio));

		double totalD = 0.0;
		totalD = Double.parseDouble(requestPago.optString("totalPago"));
		capturaView.setText(requestPago.optString("linea_captura"));

		montoView.setText(Text.formatCurrency(totalD, true));
	}

	private void setRequestPago() {
		try {
			requestPago = new JSONObject(datos);
		} catch (JSONException e) {
			requestPago = new JSONObject();
			Log.e(TAG, "JSONException", e);
		}
	}

	private boolean validateData() {
		String password = passwordText.getText().toString().trim();
		String cvv = cvvText.getText().toString().trim();

		if (TextUtils.isEmpty(cvv)) {
			cvvText.setError("Ingrea el cvv2 de tu tarjeta bancaria.");
			cvvText.requestFocus();
			return false;
		}

		if (!TextUtils.isDigitsOnly(cvv)) {
			cvvText.setError("CVV2 inv�lido.");
			cvvText.requestFocus();
			return false;
		}

		if (cvv.length() < 3) {
			cvvText.setError("El CVV2 debe tener 3 d�gitos.");
			cvvText.requestFocus();
			return false;
		}

		if (TextUtils.isEmpty(password)) {
			passwordText.setError("Ingresa tu contrase�a");
			passwordText.requestFocus();
			return false;
		}

		if (password.length() < 8) {
			passwordText
					.setError("La contrase�a debe tener 8 - 12 caracteres.");
			passwordText.requestFocus();
			return false;
		}

		return true;

	}

	private String getServicioName(int servicio) {
		switch (servicio) {
		case Servicios.AGUA:
			return "Agua Vigente";
		case Servicios.INFRACCION:
			return "Infracci�n";
		case Servicios.NOMINA:
			return "N�mina";
		case Servicios.PREDIAL:
			return "Predial Vigente";
		case Servicios.TENENCIA:
			return "Tenencia";
		case Servicios.PREDIAL_VENCIDOS:
			return "Predial Vencido";
		case Servicios.T_CIRCULACION:
			return "Tarjeta de Circulaci�n";
		case Servicios.LICENCIA:
			return Licencias.TRES_ANIOS.getLeyenda();
		case Servicios.LICENCIA_PERMANENTE:
			return Licencias.PERMANENTE.getLeyenda();

		default:
			return "";
		}
	}

	private class TokenListener implements WSResponseListener {

		private static final String TAG = "TokenListener";

		@Override
		public void StringResponse(String response) {
			Log.i(TAG, response);

			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptSensitive(response);

				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());

					int numError = json.optInt("numError", -1);
					String error = json.optString("error",
							"Error de conexi�n. Intente de nuevo m�s tarde");

					switch (numError) {
					case 0:
						String token = json.optString("token");

						requestPago.put("token", token);
						requestPago.put("cvv2", cvvText.getText().toString()
								.trim());
						requestPago.put("password", passwordText.getText()
								.toString().trim());

						String request = Request.buildProsaRequest(
								PagoActivity.this,
								requestPago,
								session.getUserDetails().get(
										SessionManager.USR_ID), servicio);

						new WebServiceClient(new PagoListener(),
								PagoActivity.this, true, Url.PAGO_PROSA)
								.execute(request);
						break;

					default:
						CustomToast.build(PagoActivity.this, error);
						break;
					}

				} catch (JSONException e) {
					CustomToast.build(PagoActivity.this,
							"Error de conexi�n. Intente de nuevo m�s tarde");
					Log.e(TAG, "JSONException", e);
				}
			} else {
				switch (WebServiceClient.getError()) {
				case SIN_CONEXION:
					pagarButton.setEnabled(true);
					CustomToast.build(PagoActivity.this,
							Errors.CONNECTION_TIMEOUT);
					break;
				case TIMEOUT:
					pagarButton.setEnabled(true);
					CustomToast.build(PagoActivity.this,
							Errors.SOCKET_TIMEOUT);

				default:
					pagarButton.setEnabled(true);
					CustomToast.build(PagoActivity.this,
							"Error de conexi�n. Intente de nuevo m�s tarde.");
					break;
				}
			}

		}
	}

	private class PagoListener implements WSResponseListener {

		private static final String TAG = "PagoListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);

			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptSensitive(response);

				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());

					DetalleTO detalle = new DetalleTO();
					Intent intent = new Intent(PagoActivity.this,
							DetalleActivity.class);

					int numError = json.optInt("numError", -10);
					String error = json.optString("error",
							"Error de conexi�n. Intente de nuevo m�s tarde");

					switch (numError) {
					case 0:

						detalle.setExitoso(true)
								.setMensaje(error)
								.setFolio(json.optString("transaccion"))
								.setAutorizacion(json.optString("autorizacion"))
								.setMonto(requestPago.optString("totalPago"));

						intent.putExtra("detalle", detalle);

						startActivity(intent);
						finish();
						break;

					default:
						detalle.setExitoso(false).setMensaje(error);

						intent.putExtra("detalle", detalle);

						startActivity(intent);
						break;
					}
				} catch (JSONException e) {
					pagarButton.setEnabled(true);
					CustomToast.build(PagoActivity.this,
							"Error de conexi�n. Intente de nuevo m�s tarde");
					Log.e(TAG, "JSONException", e);
				}
			} else {
				
				Log.e(TAG, WebServiceClient.getError().toString());
				
				switch (WebServiceClient.getError()) {
				case SIN_CONEXION:
					pagarButton.setEnabled(true);
					CustomToast.build(PagoActivity.this,
							Errors.CONNECTION_TIMEOUT);
					break;
				case TIMEOUT:
					pagarButton.setEnabled(true);
					CustomToast.build(PagoActivity.this,
							Errors.SOCKET_TIMEOUT);

				default:
					pagarButton.setEnabled(true);
					CustomToast.build(PagoActivity.this,
							"Error de conexi�n. Intente de nuevo m�s tarde.");
					break;
				}
			}

		}
	}
}
