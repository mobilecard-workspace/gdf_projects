package org.addcel.gdf.view;

import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Tramite;
import org.addcel.gdf.sql.CuentaPredial;
import org.addcel.gdf.sql.PredialDataSource;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class ConstanciaAdeudosActivity extends SherlockActivity {
	
	Spinner cuentaSpinner;
	EditText cuentaText, confCuentaText, importeText;
	boolean ingresoDirecto;
	SessionManager session;
	JSONObject requestPago;
	PredialDataSource dataSource;
	
	private static final String TAG = "ConstanciaAdeudosActivity";
	

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_constancia_predial);
		session = new SessionManager(ConstanciaAdeudosActivity.this);
		
		ingresoDirecto = true;
		initGUI();
	}
	
	 @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        // TODO Auto-generated method stub
	        getSupportMenuInflater().inflate(R.menu.menu_predial, menu);
	        return super.onCreateOptionsMenu(menu);
	    }

	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        // TODO Auto-generated method stub
	        int id = item.getItemId();

	        switch (id) {
	            case R.id.action_predial:
	                if (session.isLoggedIn()) {
	                    Dialogos.showPredialDialog(Tramite.PREDIAL, this, dataSource);
	                } else {
	                	CustomToast.build(ConstanciaAdeudosActivity.this, "Debes iniciar sesi�n mobilecard para poder guardar una cuenta.");
	                }
	            break;
	            case R.id.action_login:
	                if (!session.isLoggedIn()) {
	                    startActivity(new Intent(ConstanciaAdeudosActivity.this, LoginActivity.class));
	                } else {
	                    Dialogos.makeYesNoAlert(ConstanciaAdeudosActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {

	                        @Override
	                        public void ok(DialogInterface dialog, int id) {
	                            // TODO Auto-generated method stub
	                            session.logoutUser();
	                            dialog.dismiss();
	                        }

	                        @Override
	                        public void cancel(DialogInterface dialog, int id) {
	                            // TODO Auto-generated method stub
	                            dialog.cancel();
	                        }
	                    });
	                }
	                break;
	            case R.id.action_registro:
	                if (session.isLoggedIn()) {
	                	CustomToast.build(ConstanciaAdeudosActivity.this, "Ya eres usuario MobileCard.");
	                } else {
	                    startActivity(new Intent(ConstanciaAdeudosActivity.this, RegistroActivity.class));
	                }
	                break;
	            case R.id.action_update:
	                if (session.isLoggedIn()) {
	                    startActivity(new Intent(ConstanciaAdeudosActivity.this, DataUpdateActivity.class));
	                } else {
	                	CustomToast.build(ConstanciaAdeudosActivity.this, "Inicia Sesi�n para actualizar tus datos");
	                }
	                break;
				case R.id.action_password:
					if (session.isLoggedIn()) {
						CustomToast.build(ConstanciaAdeudosActivity.this, "Ya eres usuario MobileCard.");
					} else {
						startActivity(new Intent(ConstanciaAdeudosActivity.this, PassRecoverActivity.class));
					}
					break;

	            default:
	                break;
	        }
	        return super.onOptionsItemSelected(item);
	    }
	    
	    private void initData() {
	        dataSource = new PredialDataSource(ConstanciaAdeudosActivity.this);
	        dataSource.open();
	        List<CuentaPredial> cuentas = dataSource.getAllCuentas();
	        ArrayAdapter<CuentaPredial> adapter = new ArrayAdapter<CuentaPredial>(ConstanciaAdeudosActivity.this, android.R.layout.simple_spinner_item, cuentas);
	        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	        cuentaSpinner.setAdapter(adapter);
	        dataSource.close();
	    }
	    
	    private void initGUI() {
	    	cuentaSpinner = (Spinner) findViewById(R.id.spinner_predial);
	    	cuentaText = (EditText) findViewById(R.id.edit_predial);
	    	confCuentaText = (EditText) findViewById(R.id.edit_predial_conf);
	    	importeText = (EditText) findViewById(R.id.edit_importe);
	    	
	    	cuentaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

	            @Override
	            public void onItemSelected(AdapterView<?> arg0, View arg1,
	                    int arg2, long arg3) {
	                // TODO Auto-generated method stub
	                if (0 == arg2) {
	                    ingresoDirecto = true;
	                    cuentaText.setVisibility(View.VISIBLE);
	                    confCuentaText.setVisibility(View.VISIBLE);
	                } else {
	                    ingresoDirecto = false;
	                    cuentaText.setVisibility(View.GONE);
	                    confCuentaText.setVisibility(View.GONE);
	                }
	            }

	            @Override
	            public void onNothingSelected(AdapterView<?> arg0) {
	                // TODO Auto-generated method stub
	                ingresoDirecto = true;
	                cuentaText.setVisibility(View.VISIBLE);
	                confCuentaText.setVisibility(View.VISIBLE);

	            }
	        });
	    	
	    	  Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
	          ((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);

	          initData();
	    }
	    
	    private String getCuenta(boolean ingresoDirecto) {
	    	
	    	if (ingresoDirecto)
	    		return cuentaText.getText().toString().trim();
	    	else
	    		return ((CuentaPredial) cuentaSpinner.getSelectedItem()).getCuenta();
	    	
	    }
	    
	    public void consultar(View v) {
	    	
	    	if (session.isLoggedIn()) {
	    		if (validar()) {
	    		
		    		String cuenta = getCuenta(ingresoDirecto);
		    		String monto = importeText.getText().toString().trim();
	    		
	    		}
	    	} else {
	    		CustomToast.build(ConstanciaAdeudosActivity.this, "Debes iniciar sesi�n mobilecard para poder consultar tu adeudo.");
	    	}
	    	
	    }
	    
	    private boolean validar() {
	        if (ingresoDirecto) {

	            String cuenta = cuentaText.getText().toString().trim();
	            String confirmacion = confCuentaText.getText().toString().trim();
	            String monto = importeText.getText().toString().trim();
	            
	            if (cuenta == null || "".equals(cuenta)) {
	                cuentaText.requestFocus();
	                cuentaText.setError("El campo cuenta no puede ir vac�o");
	                return false;
	            }

	            if (confirmacion == null || "".equals(confirmacion)) {
	                confCuentaText.requestFocus();
	                confCuentaText.setError("El campo confirmaci�n no puede ir vac�o");
	                return false;
	            }

	            if (!cuenta.equals(confirmacion)) {
	                confCuentaText.requestFocus();
	                confCuentaText.setError("Confirmaci�n no coincide con cuenta capturada");
	                return false;
	            }
	            
	            if (TextUtils.isEmpty(monto) || !TextUtils.isDigitsOnly(monto)) {
	            	importeText.requestFocus();
	            	importeText.setError("Importe no v�lido");
	            	return false;
	            }
	            
	            return true;
	        } else {
	        	
        		String monto = importeText.getText().toString().trim();
	        	
	            if (TextUtils.isEmpty(monto) || !TextUtils.isDigitsOnly(monto)) {
	            	importeText.requestFocus();
	            	importeText.setError("Importe no v�lido");
	            	return false;
	            }
	        	 
	            return true;
	        }
	    }
	    
	    private void cleanScreen() {
       	}
	    
	    
 }
