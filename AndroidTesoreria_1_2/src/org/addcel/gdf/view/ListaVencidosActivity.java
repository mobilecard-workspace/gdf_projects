package org.addcel.gdf.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.adapter.VencidosAdapter;
import org.addcel.gdf.constant.Fonts;
import org.addcel.util.CustomToast;
import org.addcel.util.Text;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class ListaVencidosActivity extends SherlockActivity {
	
	JSONObject json;
	String header;
	JSONArray detalle;
	List<JSONObject> vencidos;
	
	TextView headerView;
	ListView vencidosList;
	
	private static final String TAG = "ListaVencidosActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lista_vencidos);
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_vencidos, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		int itemId = item.getItemId();
		
		switch (itemId) {
			case R.id.action_aceptar:
				long[] ids = vencidosList.getCheckedItemIds();
				double montos = 0.0;
				
				if (ids == null || ids.length == 0) {
					CustomToast.build(ListaVencidosActivity.this, "No hay periodos seleccionados.");
				} else {
					List<String> periodos = new ArrayList<String>(ids.length);
					
					for (long id : ids) {
						Log.d(TAG, vencidos.get((int) id).toString());
						periodos.add(vencidos.get((int) id).optString("periodo"));
						montos += vencidos.get((int) id).optDouble("subtotal");
					}
//					
					double montoMaximoOperacion =  json.optDouble("monto_maximo_operacion", 0.0);
					
					if (montos <= montoMaximoOperacion) {
						Intent intent = new Intent(ListaVencidosActivity.this, PredialVencidoConfActivity.class);
						intent.putExtra("periodos", Text.getPeriodosConPipe(periodos));
						intent.putExtra("cuenta", header);
						startActivity(intent);
					} else {
						String montoMaxStr = Text.formatCurrency(montoMaximoOperacion, true);
						CustomToast.build(ListaVencidosActivity.this, "Debes acotar tu selecci�n para no exceder el l�mite de pago (" + montoMaxStr + ")");
					}
				}
				
				break;
	
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		try {
			json = new JSONObject(getIntent().getStringExtra("json"));
			header = json.optString("cuenta");
			detalle = json.optJSONArray("detalle");
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			finish();
		}
		
	}
	
	private void setVencidos() {
		vencidos = new ArrayList<JSONObject>(detalle.length());
		
		for (int i = 0; i < detalle.length(); i++) {
			vencidos.add(detalle.optJSONObject(i));
		}
		
		vencidosList.setAdapter(new VencidosAdapter(ListaVencidosActivity.this, vencidos));
		vencidosList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE); 
		
		for (int i = 0; i < vencidos.size(); i++) {
			JSONObject j  = vencidos.get(i);
			
			if (!j.optString("requerido").equals("NO")) {
				vencidosList.setItemChecked(i, true);
				
//				View v = vencidosList.getChildAt(i);
//				
//				try {
//					v.setEnabled(false);
//				} catch (NullPointerException e) {
//					Log.e(TAG, "", e);
//				}
			}
		}
	}
	
	public void initGUI() {
		initData();
		
		headerView = (TextView) findViewById(R.id.header_text);
		headerView.setText("Cuenta: " + header);
		
		vencidosList = (ListView) findViewById(R.id.list_vencidos);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		headerView.setTypeface(gothamBook);
		
		setVencidos();
		
	}
}
