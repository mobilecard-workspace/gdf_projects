package org.addcel.gdf.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.adapter.LicenciaAdapter;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.enums.Licencias;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.Dialogos;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class LicenciaListActivity extends SherlockActivity {	
	
	ListView tiposLicenciaList;
	SessionManager session;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		session = new SessionManager(LicenciaListActivity.this);
		
		setContentView(R.layout.activity_list_licencia);
		
		((TextView) findViewById(R.id.view_titulo)).setTypeface(gothamBold);
		
		if (!session.isLoggedIn())
			Dialogos.showInitialDialog(LicenciaListActivity.this);
		
		tiposLicenciaList = (ListView) findViewById(R.id.list_tipo_licencia);
		
		List<Licencias> licencias = new ArrayList<Licencias>(Arrays.asList(Licencias.values()));
		
		LicenciaAdapter adapter = new LicenciaAdapter(LicenciaListActivity.this, licencias);
		tiposLicenciaList.setAdapter(adapter);
		tiposLicenciaList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
		
		@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Licencias l = (Licencias)((LicenciaAdapter) arg0.getAdapter()).getItem(arg2);
				
				Intent intent = new Intent(LicenciaListActivity.this, LicenciaActivity.class);
				intent.putExtra("tipo_licencia", l);
				startActivity(intent);
			}	
		
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		int id = item.getItemId();
		
		switch (id) {
		case R.id.action_login:
			if (!session.isLoggedIn()) {
				startActivity(new Intent(LicenciaListActivity.this, LoginActivity.class));
			} else {
				Dialogos.makeYesNoAlert(LicenciaListActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
					
					@Override
					public void ok(DialogInterface dialog, int id) {
						// TODO Auto-generated method stub
						session.logoutUser();
						dialog.dismiss();
					}
					
					@Override
					public void cancel(DialogInterface dialog, int id) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
			}
			break;
		case R.id.action_registro:
			if (session.isLoggedIn()) {
				Toast.makeText(LicenciaListActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
			} else {
				startActivity(new Intent(LicenciaListActivity.this, RegistroActivity.class));
			}
			break;
		case R.id.action_update:
			if (session.isLoggedIn()) {
				startActivity(new Intent(LicenciaListActivity.this, DataUpdateActivity.class));
			} else {
				Toast.makeText(LicenciaListActivity.this, "Inicia Sesi�n para actualizar tus datos", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.action_password:
			if (session.isLoggedIn()) {
				Toast.makeText(LicenciaListActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
			} else {
				startActivity(new Intent(LicenciaListActivity.this, PassRecoverActivity.class));
			}
			break;

		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}

}
