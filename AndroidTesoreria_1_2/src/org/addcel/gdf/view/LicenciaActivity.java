package org.addcel.gdf.view;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.gdf.enums.Licencias;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.addcel.util.Validador;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class LicenciaActivity extends SherlockActivity {
	
	SessionManager session;
	EditText rfcText, confRfcText;
	TextView rfcView, importeView, capturaView, totalView;
	Button enviarButton, pagarButton;
	Licencias tipoLicencia;
	
	JSONObject requestPago;
	
	private static final String TAG = "LicenciaActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_licencia);
		
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(LicenciaActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(LicenciaActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					CustomToast.build(LicenciaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(LicenciaActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(LicenciaActivity.this, DataUpdateActivity.class));
				} else {
					CustomToast.build(LicenciaActivity.this, "Inicia Sesi�n para actualizar tus datos");
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					CustomToast.build(LicenciaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(LicenciaActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		session = new SessionManager(LicenciaActivity.this);
		tipoLicencia = (Licencias) getIntent().getSerializableExtra("tipo_licencia");
	}
	
	public void initGUI() {
		initData();
		
		rfcText = (EditText) findViewById(R.id.edit_rfc);
		confRfcText = (EditText) findViewById(R.id.edit_rfc_conf);
		enviarButton = (Button) findViewById(R.id.button_calcular);
		pagarButton = (Button) findViewById(R.id.button_pagar);
		
		rfcView = (TextView) findViewById(R.id.view_rfc); 
		importeView = (TextView) findViewById(R.id.view_importe);
		capturaView =  (TextView) findViewById(R.id.view_captura);
		totalView = (TextView) findViewById(R.id.view_total);
		
		enviarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (validar()) {
					sendData();
				}
			}
		});
		
		pagarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				executePayment();
			}
		});
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		((TextView) findViewById(R.id.header_text)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.header_text)).setText(tipoLicencia.getLeyenda());
	
	}
	
	private boolean validar() {

		String rfc = rfcText.getText().toString().trim();
		String confRfc = confRfcText.getText().toString().trim();
		
		if (rfc == null || "".equals(rfc)) {
			cleanScreen();
			rfcText.requestFocus();
			rfcText.setError("El campo RFC no puede ir vac�o");
			return false;
		}
		
		if (!Validador.esRFCSinHomoclave(rfc)) {
			cleanScreen();
			rfcText.requestFocus();
			rfcText.setError("RFC no v�lido");
			return false;
			
		}
		
		double dateDiff = AppUtils.getDateDifference(Text.parseRFC(rfc));
		Log.i(TAG, String.valueOf(dateDiff));
		
		if ( dateDiff < 18.0) {
			cleanScreen();
			rfcText.requestFocus();
			rfcText.setError("Debe cumplir con la mayor�a de edad para obtener su Licencia de conducir");
			return false;
		}
		
		if (confRfc == null || "".equals(confRfc)) {
			cleanScreen();
			confRfcText.requestFocus();
			confRfcText.setError("El campo confirmaci�n de RFC no puede ir vac�o");
			return false;
		}
		
		if (!confRfc.equals(rfc)) {
			cleanScreen();
			confRfcText.requestFocus();
			confRfcText.setError("RFCs capturados no coinciden");
			return false;
		}
		
		return true;
	}
	
	public void sendData() {
		if (session.isLoggedIn()) {
			if (validar()) {
				String rfc = rfcText.getText().toString().trim();
				
				String request = Request.buildLicenciaRequest(
						tipoLicencia, 
						rfc, 
						session.getUserDetails().get(SessionManager.USR_ID));
				
				new WebServiceClient(new LicenciaListener(), LicenciaActivity.this, true, Url.CONSUMIDOR).execute(request);
			}
		} else {
			CustomToast.build(LicenciaActivity.this, "Debes iniciar sesi�n mobilecard para poder calcular tu pago.");
		}
	}
	
	public void executePayment() {
		if (requestPago == null) 
			Toast.makeText(LicenciaActivity.this, "Env�e datos de pago.", Toast.LENGTH_SHORT).show();
		else{
			
			double montoMax = requestPago.optDouble("monto_maximo_operacion", 5000.0);
			
			if (requestPago.optDouble("totalPago", 0) <= montoMax) {
			
				Intent i = new Intent(LicenciaActivity.this, PagoActivity.class);
				i.putExtra("datos", requestPago.toString());
				
				switch (tipoLicencia) {
				case TRES_ANIOS:
					i.putExtra("servicio", Servicios.LICENCIA);
					break;
				case PERMANENTE:
					i.putExtra("servicio", Servicios.LICENCIA_PERMANENTE);
					break;
				default:
					break;
				}
				startActivity(i);
			} else {
				CustomToast.build(LicenciaActivity.this, "El l�mite m�ximo para la transacci�n es: " + Text.formatCurrency(montoMax, true));
			}
		}
	}
	
	private void cleanScreen() {
		rfcView.setText("");
		importeView.setText("");
		capturaView.setText("");
		totalView.setText("");
	}
	
	private class LicenciaListener implements WSResponseListener {

		private static final String TAG = "LicenciaListener";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			
			if (!TextUtils.isEmpty(response)) {
				
				try {
				
					JSONObject json =  new JSONObject(AddcelCrypto.decryptSensitive(response));
					
					String numError = json.optString("numError", "-10");
					
					if (!numError.equals("0")) {
						cleanScreen();
						
						AlertDialog.Builder builder = new AlertDialog.Builder(LicenciaActivity.this)
						.setTitle("Informaci�n")
						.setMessage(json.optString("error", "Error de conexi�n. Intente de nuevo m�s tarde."))						
						.setPositiveButton("OK", new OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
							}
						});
						
						builder.show();
						
					} else {
						
						requestPago = json;
						
						double importeFinal = json.optDouble("importeFinal");
						double totalPago = Double.parseDouble(json.optString("totalPago"));
						
						rfcView.setText(json.optString("rfc"));
						importeView.setText(Text.formatCurrency(importeFinal, true));
						capturaView.setText(json.optString("linea_captura"));
						totalView.setText(Text.formatCurrency(totalPago, true));
					}
										
				} catch (JSONException e) {
					cleanScreen();
					CustomToast.build(LicenciaActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.");
				}
								
			} else {
				cleanScreen();
				CustomToast.build(LicenciaActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.");
			}
			
		}
		
		
	}

}
