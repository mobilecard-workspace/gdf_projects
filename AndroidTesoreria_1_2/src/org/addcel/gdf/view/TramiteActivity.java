package org.addcel.gdf.view;

import java.util.Arrays;
import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.adapter.GdfListAdapter;
import org.addcel.gdf.constant.Departamento;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Tramite;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class TramiteActivity extends SherlockActivity {
	
	private List<String> tramites;
	private ListView tramitesList;
	private int departamento;
	private String tramite;
	private SessionManager session;
	
	private static final String TAG = "TramiteActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listmenu);
		
		session = new SessionManager(TramiteActivity.this);
		
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(TramiteActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(TramiteActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					Toast.makeText(TramiteActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
				} else {
					startActivity(new Intent(TramiteActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(TramiteActivity.this, DataUpdateActivity.class));
				} else {
					Toast.makeText(TramiteActivity.this, "Inicia Sesi�n para actualizar tus datos", Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					Toast.makeText(TramiteActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
				} else {
					startActivity(new Intent(TramiteActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		departamento = getIntent().getIntExtra("departamento", -1);
		
		if (Departamento.VEHICULOS == departamento)
			tramites = Arrays.asList(getResources().getStringArray(R.array.arr_vehiculos));
		else if (Departamento.EMPRESAS == departamento)
			tramites = Arrays.asList(getResources().getStringArray(R.array.arr_nomina));
		else if (Departamento.TRAMITES == departamento)
			tramites = Arrays.asList(getResources().getStringArray(R.array.arr_tramites));
		else if (Departamento.INMUEBLES == departamento)
			tramites = Arrays.asList(getResources().getStringArray(R.array.arr_inmuebles));
	}
	
	private void initGUI() {
		initData();
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.view_titulo)).setTypeface(gothamBold);
		
		if (Departamento.ERROR == departamento) {
			Toast.makeText(TramiteActivity.this, "Tr�mites no disponibles", Toast.LENGTH_LONG).show();
			finish();
		} else {
			
			if (!session.isLoggedIn())
				Dialogos.showInitialDialog(TramiteActivity.this);
			
			tramitesList = (ListView) findViewById(R.id.list_options);
			tramitesList.setAdapter(new GdfListAdapter(TramiteActivity.this, tramites));
			tramitesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					if (Departamento.INMUEBLES == departamento) 
						if (arg2 == Tramite.PREDIAL) 
							startActivity(new Intent(TramiteActivity.this, PredialActivity.class));
						else if (arg2 == Tramite.PREDIAL_VENCIDO)
//							Toast.makeText(TramiteActivity.this, "En proceso...", Toast.LENGTH_SHORT).show();
							startActivity(new Intent(TramiteActivity.this, PredialVencidoActivity.class));
						else if (arg2 == Tramite.AGUA)
							startActivity(new Intent(TramiteActivity.this, AguaActivity.class));
	
					if (Departamento.VEHICULOS == departamento)
						if (arg2 == Tramite.TENENCIA)
							startActivity(new Intent(TramiteActivity.this, TenenciaActivity.class));
						else if (arg2 == Tramite.INFRACCIONES)
							startActivity(new Intent(TramiteActivity.this, InfraccionActivity.class));
						else if (arg2 == Tramite.T_CIRCULACION)
							startActivity(new Intent(TramiteActivity.this, TarjetaCirculacionActivity.class));
						else if (arg2 == Tramite.LICENCIA) {
							startActivity(new Intent(TramiteActivity.this, LicenciaListActivity.class));
						}
					
					if (Departamento.TRAMITES == departamento) 
						if (arg2 == Tramite.CERTIFICACION) {
							startActivity(new Intent(TramiteActivity.this, CertificacionListActivity.class));
						} else if (arg2 == Tramite.CONSTANCIA) {
							startActivity(new Intent(TramiteActivity.this, ConstanciaAdeudosActivity.class));
						}
						
						
					if (Departamento.EMPRESAS == departamento)
						if (arg2 == Tramite.NOMINA)
							startActivity(new Intent(TramiteActivity.this, NominaActivity.class));
				}
			});
			
			
		}
	}
		
}
