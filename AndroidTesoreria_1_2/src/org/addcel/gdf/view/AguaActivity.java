package org.addcel.gdf.view;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class AguaActivity extends SherlockActivity {
	
	TextView headerView, contadorView;
	EditText cuentaText;
	Button consultarButton, pagarButton;
	SessionManager session;
	JSONObject requestPago;
	
	private static final String TAG = "AguaActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_agua);
		session = new SessionManager(AguaActivity.this);
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(AguaActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(AguaActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					CustomToast.build(AguaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(AguaActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(AguaActivity.this, DataUpdateActivity.class));
				} else {
					CustomToast.build(AguaActivity.this, "Inicia Sesi�n para actualizar tus datos");
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					CustomToast.build(AguaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(AguaActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void initGUI() {
		headerView = (TextView) findViewById(R.id.header_text);
		
		contadorView = (TextView) findViewById(R.id.text_contador);
		cuentaText = (EditText) findViewById(R.id.edit_agua);
		consultarButton = (Button) findViewById(R.id.button_consultar);
		pagarButton = (Button) findViewById(R.id.button_pagar);
		
		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		headerView.setTypeface(gothamBook);
		
		cuentaText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				int size = cuentaText.getText().toString().trim().length();
				contadorView.setText("" + (16 - size));
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		consultarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sendData();
			}
		});
		
		pagarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				executePayment();
			}
		});
	}
	
	public void sendData() {
		if (session.isLoggedIn()) {
			if (validar()) {
				String cuenta = cuentaText.getText().toString().trim();
				
				String request = Request.buildAguaRequest(cuenta);
				Log.i(TAG, request);
				new WebServiceClient(new AguaListener(), AguaActivity.this, true, Url.CONSUMIDOR).execute(request);
			}
		} else {
			CustomToast.build(AguaActivity.this, "Debes iniciar sesi�n mobilecard para poder calcular tu pago.");
			
		}
	}
	
	public void executePayment() {
		if (requestPago == null) 
			CustomToast.build(AguaActivity.this, "Env�e datos de pago.");
		else{
			
			double montoMax = requestPago.optDouble("monto_maximo_operacion", 5000.0);
			
			if (requestPago.optDouble("totalPago", 0) <= montoMax) {
			
				Intent i = new Intent(AguaActivity.this, PagoActivity.class);
				i.putExtra("datos", requestPago.toString());
				i.putExtra("servicio", Servicios.AGUA);
				startActivity(i);
			} else {
				CustomToast.build(AguaActivity.this, "El l�mite m�ximo para la transacci�n es: " + Text.formatCurrency(montoMax, true));
			}
		}
	}
	
	private boolean validar() {
		String cuenta = cuentaText.getText().toString().trim();
		
		if (cuenta == null || "".equals(cuenta)) {
			cuentaText.requestFocus();
			cuentaText.setError("El campo cuenta no puede ir vac�o");
			return false;
		}
		
		if (cuenta.length() < 16) {
			cuentaText.requestFocus();
			cuentaText.setError("El campo cuenta no puede tener menos de 16 caracteres.");
			return false;
			
		}
		return true;
	}
	
	private void cleanScreen() {
		((TextView) findViewById(R.id.text_bimestre)).setText("");
		((TextView) findViewById(R.id.text_anio_bimestre)).setText("");
		((TextView) findViewById(R.id.text_cuenta)).setText("");
		((TextView) findViewById(R.id.text_captura)).setText("");
		((TextView) findViewById(R.id.text_derecho)).setText("");
		((TextView) findViewById(R.id.text_iva)).setText("");
		((TextView) findViewById(R.id.text_total)).setText("");
	}
	
	private class AguaListener implements WSResponseListener {
		
		private static final String TAG = "AguaListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (response != null && !"".equals(response)) {
				response = AddcelCrypto.decryptSensitive(response);
				
				String error = "";
				String numErrorString = "";
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					
					if (json.has("numError")) {
						numErrorString = json.optString("numError", "-1");
					} else {
						numErrorString = "0";
					}
					
					error = json.optString("error", "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
					int numError = Integer.valueOf(numErrorString);
					
					switch (numError) {
					case 0:
						
						requestPago = json;
						
						double derechoSum = json.optInt("vderdom") + json.optInt("vderndom");
						String bimestre = json.optString("vbimestre");
						String anioBimestre = json.optString("vanio");
						String cuenta = json.optString("cuenta");
						String captura = json.optString("linea_captura");
						double derecho = derechoSum;
						double iva = json.optDouble("viva");
						String total = Text.formatCurrency(json.optDouble("totalPago"), true);
						
						requestPago.put("vderecho", Double.toString(derechoSum));
						
						
						((TextView) findViewById(R.id.text_bimestre)).setText(bimestre);
						((TextView) findViewById(R.id.text_anio_bimestre)).setText(anioBimestre);
						((TextView) findViewById(R.id.text_cuenta)).setText(cuenta);
						((TextView) findViewById(R.id.text_captura)).setText(captura);
						((TextView) findViewById(R.id.text_derecho)).setText(Text.formatCurrency(derecho, true));
						((TextView) findViewById(R.id.text_iva)).setText(Text.formatCurrency(iva, true));
						((TextView) findViewById(R.id.text_total)).setText(total);
						break;

					default:
						cleanScreen();
						
						AlertDialog.Builder builder = new AlertDialog.Builder(AguaActivity.this)
						.setTitle("Informaci�n")
						.setMessage(error)						
						.setPositiveButton("OK", new OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
							}
						});
						
						builder.show();
						break;
					}
					
					
				} catch (NumberFormatException e) {	
					cleanScreen();
					CustomToast.build(AguaActivity.this, error);
					Log.e("PagoAguaActivity", "Error en formato de error", e);
				} catch (JSONException e) {
					cleanScreen();
					CustomToast.build(AguaActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
					Log.e(TAG, "JSONException", e);
				}
			} else {
				cleanScreen();
				CustomToast.build(AguaActivity.this, "Error de Conexi�n. Intentelo de nuevo m�s tarde.");
			}
		}
	}
}
