package org.addcel.gdf.view;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class DetalleTarjetaCirculacionActivity extends SherlockActivity {
	
	String jsonString;
	JSONObject json;
	SessionManager session;
	
	private TextView placaView, importeView, capturaView, totalView;
	
	private static final String TAG = "DetalleTarjetaActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalle_tcirculacion);
		
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_reenvio, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		int itemId = item.getItemId();
		
		switch (itemId) {
		case R.id.action_reenviar:
			executeReenvio();
			break;
			
		case R.id.action_update:
			startActivity(new Intent(DetalleTarjetaCirculacionActivity.this, DataUpdateActivity.class));
			break;
		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	public void initGUI() {
		
		initData();

		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.text_secretaria)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.text_finanzas)).setTypeface(gothamBold);
		
		placaView = (TextView) findViewById(R.id.view_placas);
		importeView = (TextView) findViewById(R.id.view_importe);
		capturaView = (TextView) findViewById(R.id.view_captura);
		totalView = (TextView) findViewById(R.id.view_total);
		
		try {
			
			double importe = json.optDouble("importeFinal");
			double totalPago = json.optDouble("totalPago");
			importeView.setText(Text.formatCurrency(importe, true));
			totalView.setText(Text.formatCurrency(totalPago, true));
		} catch (NumberFormatException e) {
			importeView.setText(String.valueOf(json.optDouble("importe")));
			totalView.setText(String.valueOf(json.optDouble("totalPago")));
			
		}
		
		placaView.setText(json.optString("placa"));
		capturaView.setText(json.optString("linea_captura"));
	}
	
	private void initData() {
		session = new SessionManager(DetalleTarjetaCirculacionActivity.this);
		jsonString = getIntent().getStringExtra("json");
		
		try {
			json = new JSONObject(jsonString);
		} catch (JSONException e) {
			Log.e(TAG, "", e);
		}
	}
	
	public void executeReenvio() {
		
		final String idUsuario = session.getUserDetails().get(SessionManager.USR_ID);
		final String idBitacora = json.optString("id_bitacora");
		final int idProducto = Servicios.T_CIRCULACION;
		
		Dialogos.makeReenvioAlert(DetalleTarjetaCirculacionActivity.this, "�Desea reenviar recibo de pago?", new DialogOkListener() {
			
			@Override
			public void ok(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				String request = Request.buildReenvioRequest(idUsuario, idBitacora, idProducto, null);
				new WebServiceClient(new ReenvioListener(), DetalleTarjetaCirculacionActivity.this, true, Url.REENVIAR_RECIBO).execute(request);
			}
			
			@Override
			public void cancel(DialogInterface dialog, int id) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				
				try {
					JSONObject json = new JSONObject();
					json.put("idUsuario", idUsuario);
					json.put("idBitacora", idBitacora);
					json.put("idProducto", idProducto);
					WebServiceClient client = new WebServiceClient(new ReenvioListener(), DetalleTarjetaCirculacionActivity.this, true, Url.REENVIAR_RECIBO);
					Dialogos.showEmailDialog(DetalleTarjetaCirculacionActivity.this, json, client);
				} catch (JSONException e) {
					Log.e(TAG, "", e);
				}
				
			}
		});
		
	}

	private class ReenvioListener implements WSResponseListener {
		
		private static final String TAG = "ReenvioListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					
					String error = json.optString("error", "Error de conexi�n. Intente de nuevo m�s tarde");
					Toast.makeText(DetalleTarjetaCirculacionActivity.this, error, Toast.LENGTH_SHORT).show();
					
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					Toast.makeText(DetalleTarjetaCirculacionActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(DetalleTarjetaCirculacionActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
			}
			
			
		}
	}
}
