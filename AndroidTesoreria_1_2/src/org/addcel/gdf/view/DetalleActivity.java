package org.addcel.gdf.view;

import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.to.DetalleTO;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.Text;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class DetalleActivity extends SherlockActivity {
	
	SessionManager session;
	DetalleTO detalle;
	
	TextView mensajeView, folioView, autorizacionView, totalView;
	Button reintentarButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalle);
		
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_detalle, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		switch (item.getItemId()) {
			case R.id.action_finalizar:
				Intent intent = new Intent(DetalleActivity.this, MenuActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
				break;
			case R.id.action_update:
				startActivity(new Intent(DetalleActivity.this, DataUpdateActivity.class));
				break;
	
			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		session = new SessionManager(DetalleActivity.this);
		detalle = getIntent().getParcelableExtra("detalle");
	}
	
	private void initGUI() {
		initData();

		Typeface gothamBook = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOOK);
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.text_secretaria)).setTypeface(gothamBook);
		((TextView) findViewById(R.id.text_finanzas)).setTypeface(gothamBold);
		
		mensajeView = (TextView) findViewById(R.id.view_mensaje);
		folioView = (TextView) findViewById(R.id.view_folio);
		autorizacionView = (TextView) findViewById(R.id.view_autorizacion);
		totalView = (TextView) findViewById(R.id.view_total);
		reintentarButton = (Button) findViewById(R.id.button_reintentar);
		
		reintentarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});

		loadData();
	}
	
	private void loadData() {
		mensajeView.setText(detalle.getMensaje());
		folioView.setText(detalle.getFolio());
		
		if (detalle.isExitoso()) {
			autorizacionView.setText(detalle.getAutorizacion());
			totalView.setText(Text.formatCurrency(Double.parseDouble(detalle.getMonto()), true));
		} else {
			findViewById(R.id.label_folio).setVisibility(View.GONE);
			folioView.setVisibility(View.GONE);
			findViewById(R.id.label_autorizacion).setVisibility(View.GONE);
			autorizacionView.setVisibility(View.GONE);
			findViewById(R.id.label_total).setVisibility(View.GONE);
			totalView.setVisibility(View.GONE);
			reintentarButton.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (detalle.isExitoso()) {
			Intent intent = new Intent(DetalleActivity.this, MenuActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		} else 
			super.onBackPressed();
	}
}
