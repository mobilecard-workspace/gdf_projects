package org.addcel.gdf.to;

import org.addcel.interfaces.JSONable;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class LcCertificadoRequest implements Parcelable, JSONable {
	
	private String cuenta;
	private String datoSeguro;
	private String correo;
	private int documento;
	private String importe;
	private boolean constancia;
	private String nombreCuenta;
	private String fecha;
	private String caja;
	private String partida;
	private String lineaCaptura;
	
	public LcCertificadoRequest() {
		
	}
	
	public LcCertificadoRequest(Parcel source) {
		readFromParcel(source);
	}
	
	public LcCertificadoRequest(JSONObject json) {
		fromJSON(json);
	}


	public String getCuenta() {
		return cuenta;
	}

	public String getDatoSeguro() {
		return datoSeguro;
	}

	public String getCorreo() {
		return correo;
	}

	public int getDocumento() {
		return documento;
	}
	
	public String getImporte() {
		return importe;
	}

	public boolean isConstancia() {
		return constancia;
	}

	public String getNombreCuenta() {
		return nombreCuenta;
	}

	public String getFecha() {
		return fecha;
	}

	public String getCaja() {
		return caja;
	}

	public String getPartida() {
		return partida;
	}

	public String getLineaCaptura() {
		return lineaCaptura;
	}

	public LcCertificadoRequest setCuenta(String cuenta) {
		this.cuenta = cuenta;
		return this;
	}

	public LcCertificadoRequest setDatoSeguro(String datoSeguro) {
		this.datoSeguro = datoSeguro;
		return this;
	}

	public LcCertificadoRequest setCorreo(String correo) {
		this.correo = correo;
		return this;
	}

	public LcCertificadoRequest setDocumento(int documento) {
		this.documento = documento;
		return this;
	}
	
	public LcCertificadoRequest setImporte(String importe) {
		this.importe = importe;
		return this;
	}

	public LcCertificadoRequest setConstancia(boolean constancia) {
		this.constancia = constancia;
		return this;
	}

	public LcCertificadoRequest setNombreCuenta(String nombreCuenta) {
		this.nombreCuenta = nombreCuenta;
		return this;
	}

	public LcCertificadoRequest setFecha(String fecha) {
		this.fecha = fecha;
		return this;
	}

	public LcCertificadoRequest setCaja(String caja) {
		this.caja = caja;
		return this;
	}

	public LcCertificadoRequest setPartida(String partida) {
		this.partida = partida;
		return this;
	}

	public LcCertificadoRequest setLineaCaptura(String lineaCaptura) {
		this.lineaCaptura = lineaCaptura;
		
		return this;
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		
		try {
			
			return new JSONObject().put("cuenta", cuenta)
									.put("datoSeguro", datoSeguro)
									.put("correo", correo)
									.put("documento", documento)
									.put("importe", importe)
									.put("constancia", constancia)
									.put("nombreCuenta", nombreCuenta)
									.put("fecha", fecha)
									.put("caja", caja)
									.put("partida", partida)
									.put("lineaCaptura", lineaCaptura);
			
		} catch (JSONException e) {
			
			return null;
		}
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		setCaja(json.optString("caja"))
		.setConstancia(json.optBoolean("constancia"))
		.setCorreo(json.optString("correo"))
		.setCuenta(json.optString("cuenta"))
		.setDatoSeguro(json.optString("datoSeguro"))
		.setDocumento(json.optInt("documento"))
		.setFecha(json.optString("fecha"))
		.setCaja(json.optString("caja"))
		.setPartida(json.optString("partida"))
		.setLineaCaptura(json.optString("lineaCaptura"));
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(caja);
		dest.writeByte((byte) (constancia ? 1 : 0));
		dest.writeString(correo);
		dest.writeString(cuenta);
		dest.writeString(datoSeguro);
		dest.writeInt(documento);
		dest.writeString(fecha);
		dest.writeString(caja);
		dest.writeString(partida);
		dest.writeString(lineaCaptura);
	}
	
	public void readFromParcel(Parcel source) {
		
		caja = source.readString();
		constancia = source.readByte() == 1;
		correo = source.readString();
		cuenta = source.readString();
		datoSeguro = source.readString();
		documento = source.readInt();
		fecha = source.readString();
		caja = source.readString();
		partida = source.readString();
		lineaCaptura = source.readString();
		
	}
	
	public static final Parcelable.Creator<LcCertificadoRequest> CREATOR = new Creator<LcCertificadoRequest>() {
		
		@Override
		public LcCertificadoRequest[] newArray(int size) {
			// TODO Auto-generated method stub
			return new LcCertificadoRequest[size];
		}
		
		@Override
		public LcCertificadoRequest createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new LcCertificadoRequest(source);
		}
	};

}
