package org.addcel.gdf.to;

import android.os.Parcel;
import android.os.Parcelable;

public class DetalleTO implements Parcelable {

	private String mensaje;
	private String folio;
	private String autorizacion;
	private String monto;
	private boolean exitoso;

	public DetalleTO() {
	}

	public DetalleTO(Parcel in) {
		readFromParcel(in);
	}

	public String getMensaje() {
		return mensaje;
	}

	public DetalleTO setMensaje(String mensaje) {
		this.mensaje = mensaje;

		return this;
	}

	public String getFolio() {
		return folio;
	}

	public DetalleTO setFolio(String folio) {
		this.folio = folio;

		return this;
	}

	public String getAutorizacion() {
		return autorizacion;
	}

	public DetalleTO setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;

		return this;
	}

	public String getMonto() {
		return monto;
	}

	public DetalleTO setMonto(String monto) {
		this.monto = monto;

		return this;
	}

	public boolean isExitoso() {
		return exitoso;
	}

	public DetalleTO setExitoso(boolean exitoso) {
		this.exitoso = exitoso;

		return this;
	}

	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(mensaje);
		dest.writeString(folio);
		dest.writeString(autorizacion);
		dest.writeString(monto);
		dest.writeByte((byte) (exitoso ? 1 : 0));
	}

	public void readFromParcel(Parcel in) {
		mensaje = in.readString();
		folio = in.readString();
		autorizacion = in.readString();
		monto = in.readString();
		exitoso = in.readByte() != 0;
	}

	public static final Parcelable.Creator<DetalleTO> CREATOR = new Parcelable.Creator<DetalleTO>() {

		public DetalleTO createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new DetalleTO(source);
		}

		public DetalleTO[] newArray(int size) {
			// TODO Auto-generated method stub
			return new DetalleTO[size];
		}
	};

}
