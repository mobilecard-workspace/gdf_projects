package org.addcel.gdf.to;

import org.addcel.interfaces.JSONable;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class LcCertificadoResponse implements Parcelable, JSONable {
	
	private String lineaCaptura;
	private String lineaCapturaCB;
	private double importe;
	private String vencimiento;
	private String clave;
	private String folio;
	
	private static final String TAG = "LcCertificadoResponse";

	
	public LcCertificadoResponse() {}
	
	public LcCertificadoResponse(Parcel source) {
		readFromParcel(source);
	}
	
	public LcCertificadoResponse(JSONObject json) {
		fromJSON(json);
	}
	
	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		

		try {
			
			return new JSONObject().put("lineaCaptura", lineaCaptura)
									.put("lineaCapturaCB", lineaCapturaCB)
									.put("importe", importe)
									.put("vencimiento", vencimiento)
									.put("clave", clave)
									.put("folio", folio);
			
		} catch (JSONException e) {

			return null;
		}
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		lineaCaptura = json.optString("lineaCaptura");
		lineaCapturaCB = json.optString("lineaCapturaCB");
		importe = json.optDouble("importe");
		vencimiento = json.optString("vencimiento");
		clave = json.optString("clave");
		folio = json.optString("folio");
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(lineaCaptura);
		dest.writeString(lineaCapturaCB);
		dest.writeDouble(importe);
		dest.writeString(vencimiento);
		dest.writeString(clave);
		dest.writeString(folio);
	}
	
	public void readFromParcel(Parcel source) {
		lineaCaptura = source.readString();
		lineaCapturaCB = source.readString();
		importe = source.readDouble();
		vencimiento = source.readString();
		clave = source.readString();
		folio = source.readString();
	}
	
	public static final Parcelable.Creator<LcCertificadoResponse> CREATOR = new Creator<LcCertificadoResponse>() {
		
		@Override
		public LcCertificadoResponse[] newArray(int size) {
			// TODO Auto-generated method stub
			return new LcCertificadoResponse[size];
		}
		
		@Override
		public LcCertificadoResponse createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new LcCertificadoResponse(source);
		}
	};

}
