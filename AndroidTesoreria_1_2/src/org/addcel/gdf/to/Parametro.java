package org.addcel.gdf.to;

import org.addcel.interfaces.JSONable;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Parametro implements Parcelable, JSONable {
	
	private String nombreParam;
	private String tipo;
	private String longitudMax;
	
	public Parametro() {}
	
	public Parametro(Parcel source) {
		readFromParcel(source);
	}
	
	public Parametro(JSONObject json) {
		fromJSON(json);
	}
	
	public String getNombreParam() {
		return nombreParam;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public String getLongitudMax() {
		return longitudMax;
	}
	
	public Parametro setNombreParam(String _nombreParam) {
		nombreParam = _nombreParam;
		
		return this;
	}
	
	public Parametro setTipo(String _tipo) {
		nombreParam = _tipo;
		
		return this;
	}

	public Parametro setLongitudMax(String _longitudMax) {
		nombreParam = _longitudMax;
		
		return this;
	}
	
	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
	
		
		try {
			
			return new JSONObject().put("nombre_param", nombreParam).put("tipo", tipo).put("longitud_max", longitudMax);
			
		} catch (JSONException e) {
			
			Log.e("Parámetro", "", e);
			return null;
		}
		
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		setLongitudMax(json.optString("longitud_max"))
		.setNombreParam(json.optString("nombre_param"))
		.setTipo(json.optString("tipo"));

		
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(longitudMax);
		dest.writeString(nombreParam);
		dest.writeString(tipo);
	}

	public void readFromParcel(Parcel source) {
		longitudMax = source.readString();
		nombreParam = source.readString();
		tipo = source.readString();
	}
	
	public static final Parcelable.Creator<Parametro> CREATOR = new Creator<Parametro>() {
		
		@Override
		public Parametro[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Parametro[size];
		}
		
		@Override
		public Parametro createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Parametro(source);
		}
	};
}
