package org.addcel.gdf.to;

import java.util.ArrayList;
import java.util.List;

import org.addcel.interfaces.JSONable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Concepto implements Parcelable, JSONable {
	
	private int idConcepto;
	private String concepto;
	private List<Parametro> parametros;
	
	public Concepto() {
		parametros = new ArrayList<Parametro>();
	}
	
	public Concepto(Parcel source) {
		this();
		readFromParcel(source);
	}
	
	public Concepto(JSONObject json) {
		this();
		fromJSON(json);
	}
	
	
	public int getIdConcepto() {
		return idConcepto;
	}

	public String getConcepto() {
		return concepto;
	}

	public List<Parametro> getParametros() {
		return parametros;
	}

	public Concepto setIdConcepto(int idConcepto) {
		this.idConcepto = idConcepto;
		
		return this;
	}

	public Concepto setConcepto(String concepto) {
		this.concepto = concepto;
		
		return this;
	}

	public Concepto setParametros(List<Parametro> parametros) {
		this.parametros = parametros;
		
		return this;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return concepto;
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		
		try {
			
			JSONObject json = new JSONObject().put("concepto", concepto).put("id_concepto", idConcepto);
			
			if (!parametros.isEmpty()) {
				
				JSONArray parArray = new JSONArray();
			
				for (Parametro p: parametros) {
					
					parArray.put(p.toJSON());
				}
				
				json.put("parametros", parArray);
			}
			
			return json;
			
		} catch (JSONException e) {
			
			return null;
			
		}
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		setConcepto(json.optString("concepto")).setIdConcepto(json.optInt("id_concepto"));
		
		JSONArray parArray = json.optJSONArray("parametros");
		
		if (null != parArray && parArray.length() > 0) {
			
			for (int i = 0; i < parArray.length(); i++) {
				parametros.add(new Parametro(parArray.optJSONObject(i)));
			}
			
		}
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(idConcepto);
		dest.writeString(concepto);
		dest.writeTypedList(parametros);
	}
	
	public void readFromParcel(Parcel source) {
		
		idConcepto = source.readInt();
		concepto = source.readString();
		source.readTypedList(parametros, Parametro.CREATOR);
	}
	
	public static final Parcelable.Creator<Concepto> CREATOR = new Parcelable.Creator<Concepto>() {

		@Override
		public Concepto createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new Concepto(source);
		}

		@Override
		public Concepto[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Concepto[size];
		}
	};

}
