package org.addcel.gdf.enums;

public enum Certificables {

	ISAN("I.S.A.N.", TiposCertificacion.RFC),
	PREDIAL("PREDIAL", TiposCertificacion.CUENTA),
	NOMINA("NOMINA", TiposCertificacion.RFC),
	HOSPEDAJE("HOSPEDAJE", TiposCertificacion.RFC),
	TENENCIA("TENENCIA", TiposCertificacion.PLACA),
	T_CIRCULACION("TARJETA DE CIRCULACION", TiposCertificacion.PLACA),
	LOTERIAS("LOTERIAS", TiposCertificacion.RFC),
	IEPS("I.E.P.S. (GASOLINA)", TiposCertificacion.RFC),
	VERIFICACION("VERIFICACION EXTEMPORANEA", TiposCertificacion.PLACA);
	
	private String nombre;
	private TiposCertificacion tipo;

	
	private Certificables(String _nombre, TiposCertificacion _tipo) {
		nombre = _nombre;
		tipo = _tipo;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public TiposCertificacion getTipo() {
		return tipo;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getNombre();
	}
}
