package org.addcel.gdf.enums;

public enum Catalogos {
	
	OPERADORES("operador"), GENEROS("generos"), PAISES("paises");
	
	private String nombre;
	
	private Catalogos(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}
	
	

}
