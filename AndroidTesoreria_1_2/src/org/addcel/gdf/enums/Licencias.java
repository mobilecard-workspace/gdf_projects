package org.addcel.gdf.enums;

public enum Licencias {
	
	TRES_ANIOS("Licencia de conducir tipo \"A\" 3 a�os", "(Expedici�n y reposici�n)"),
	PERMANENTE("Licencia de conducir Tipo \"A\" Permanente","(Reposici�n)");

	private String leyenda;
	private String sub;

	private Licencias(String leyenda, String sub) {
		this.leyenda = leyenda;
		this.sub = sub;
	}

	public String getLeyenda() {
		return leyenda;
	}

	public String getSub() {
		return sub;
	}
	
	
}
