package org.addcel.gdf.enums;

public enum Certificaciones {

	CUENTA("Por cuenta/placa/rfc"),
	FECHA("Por fecha/caja/partida"),
	LINEA("Por L�nea de captura");
	
	private String leyenda;
	
	private Certificaciones(String _leyenda) {
		leyenda = _leyenda;
	}
	
	public String getLeyenda() {
		return leyenda;
	}
}
