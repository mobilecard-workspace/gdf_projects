/**
 * 
 */
package org.addcel.gdf.adapter;

import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * @author ADDCEL14
 *
 */
public class GdfListAdapter extends BaseAdapter {
	List<String> data;
	Activity act;
	LayoutInflater inflater;
	Typeface gothamBook;
	View v;
	
	public GdfListAdapter(Activity act, List<String> data){
		this.act = act;
		this.data = data;
//		gothamBook = Typeface.createFromAsset(act.getAssets(), Fonts.GOTHAM_BOOK);
		inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder viewHolder;
		
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.item_list, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.tramite = (TextView) convertView.findViewById(R.id.nombre_tramite);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		String datoString = data.get(position);
		
		if (null != datoString) {
			viewHolder.tramite.setText(datoString);
//			viewHolder.tramite.setTypeface(gothamBook);
		}
		
		return convertView;
	}
	
	private class ViewHolder {
		TextView tramite;
	}

}
