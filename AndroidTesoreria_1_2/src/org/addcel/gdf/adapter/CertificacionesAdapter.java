package org.addcel.gdf.adapter;

import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.enums.Certificaciones;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CertificacionesAdapter extends BaseAdapter {
	
	Context con;
	List<Certificaciones> data;
	LayoutInflater inflater;
	
	public CertificacionesAdapter(Context _con, List<Certificaciones> _data) {
		
		con = _con;
		data = _data;
		inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	// TODO Auto-generated method stub
		
		ViewHolder holder;
		
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.item_licencia, parent, false);
			holder = new ViewHolder();
			holder.nombreView = (TextView) convertView.findViewById(R.id.view_nombre);
			convertView.setTag(holder);
			
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Certificaciones c = data.get(position);
		
		holder.nombreView.setText(c.getLeyenda());
		
		return convertView;
	}
	
	private class ViewHolder {
		TextView nombreView;
	}

}
