package org.addcel.gdf.adapter;

import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.enums.Licencias;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class LicenciaAdapter extends BaseAdapter {
	
	Context con;
	List<Licencias> data;
	LayoutInflater inflater;
	
	public LicenciaAdapter(Context _con, List<Licencias> _data) {
		// TODO Auto-generated constructor stub
		con = _con;
		data = _data;
		inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}



	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}



	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}



	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder holder;
		
		if (null == convertView) {
			convertView = inflater.inflate(R.layout.item_licencia, parent, false);
			holder = new ViewHolder();
			holder.nombreView = (TextView) convertView.findViewById(R.id.view_nombre);
			holder.tipoView = (TextView) convertView.findViewById(R.id.view_tipo);
			convertView.setTag(holder);
			
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Licencias l = data.get(position);
		
		holder.nombreView.setText(l.getLeyenda() + " " + l.getSub());
		holder.tipoView.setText(l.getSub());
		
		return convertView;
	}
	
	private class ViewHolder {
		
		TextView nombreView;
		TextView tipoView;
	}

}
