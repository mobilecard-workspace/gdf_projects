package org.addcel.widget;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Checkable;
import android.widget.LinearLayout;

public class CheckableLinearLayout extends LinearLayout implements Checkable {
	
	private boolean isChecked;
	private List<Checkable> checkableViews;
	private OnCheckedChangeListener onCheckedChangeListener;
	
	public static interface OnCheckedChangeListener {
		public void onCheckedChanged(CheckableLinearLayout layout, boolean isChecked);
	}
	
	public CheckableLinearLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		initialise(attrs);
		// TODO Auto-generated constructor stub
	}
	
	public CheckableLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialise(attrs);
	}
	
	public CheckableLinearLayout(Context context, int checkableId) {
		super(context);
		initialise(null);
	}

	@Override
	public boolean isChecked() {
		// TODO Auto-generated method stub
		return isChecked;
	}

	@Override
	public void setChecked(boolean arg0) {
		// TODO Auto-generated method stub
		this.isChecked = arg0;
		for (Checkable c : checkableViews) {
			c.setChecked(arg0);
		}
		
		if (onCheckedChangeListener != null) {
			onCheckedChangeListener.onCheckedChanged(this, arg0);
		}
	}

	@Override
	public void toggle() {
		// TODO Auto-generated method stub
		this.isChecked = !this.isChecked;
		for (Checkable c : checkableViews) {
			c.toggle();
		}
	}
	
	public void setOnCheckedChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
		this.onCheckedChangeListener = onCheckedChangeListener;
	}
	
	@Override
	protected void onFinishInflate() {
		// TODO Auto-generated method stub
		super.onFinishInflate();
		
		final int childCount = this.getChildCount();
		for (int i = 0; i < childCount; i++) {
			findCheckableChildren(this.getChildAt(i));
		}
	}
	
	private void initialise(AttributeSet attrs) {
		this.isChecked = false;
		this.checkableViews = new ArrayList<Checkable>(5);
	}
	
	private void findCheckableChildren(View v) {
		if (v instanceof Checkable) {
			this.checkableViews.add((Checkable) v);
		}
		
		if (v instanceof ViewGroup) {
			final ViewGroup vg = (ViewGroup) v;
			final int childCount = vg.getChildCount();
			for (int i = 0; i < childCount; ++i) {
				findCheckableChildren(vg.getChildAt(i));
			}
		}
	}
	
	

}
