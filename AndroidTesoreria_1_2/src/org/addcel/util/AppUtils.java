package org.addcel.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.addcel.mobilecard.to.ItemCatalogoTO;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.util.Log;
import android.widget.ArrayAdapter;

public class AppUtils {
	
	private static String TAG = "AppUtils";
	
	private static final int MILLIS_IN_SECOND = 1000;
    private static final int SECONDS_IN_MINUTE = 60;
    private static final int MINUTES_IN_HOUR = 60;
    private static final int HOURS_IN_DAY = 24;
    private static final double DAYS_IN_YEAR = 365.25; //I know this value is more like 365.24...
    private static final double MILLISECONDS_IN_YEAR = (double) MILLIS_IN_SECOND * SECONDS_IN_MINUTE * MINUTES_IN_HOUR
            * HOURS_IN_DAY * DAYS_IN_YEAR;

	
	public static String getVersionName(Context context, Class<?> cls) 
    {
      try {
        ComponentName comp = new ComponentName(context, cls);
        PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
        return pinfo.versionName;
      } catch (android.content.pm.PackageManager.NameNotFoundException e) {
        return null;
      }
    }
	
	public static LinkedList<String> getAniosVigencia() {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);
		
		LinkedList<String> anios = new LinkedList<String>();
		
		int amount = anio + 7;
		
		for (int i = anio; i < amount; i++) {
			anios.add("" + i);
		}
		
		return anios;
	}
	
	public static List<String> getAniosConsulta() {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);
		
		List<String> anios = new LinkedList<String>();
		int amount = anio + 6;
		
		for (int i = anio; i < amount; i++) {
			anios.add("" + anio--);
		}
		
		return anios;
	}
	
	public static List<String> getAniosModelo() {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR) + 1;
		
		List<String> anios = new LinkedList<String>();
		
		for (int i = anio; i >= 1920; i--) {
			anios.add(String.valueOf(i));
		}
		
		return anios;
	}
	
	public static String getKey() {
		Long seed = new Date().getTime();
		
		String key = String.valueOf(seed).substring(0, 8);
		Log.d(TAG, "SENSITIVE KEY: " + key);
		
		return key;
	}
	
	public static ArrayAdapter<ItemCatalogoTO> createCatalogAdapter(Context c, List<ItemCatalogoTO> list) {
		
		ArrayAdapter<ItemCatalogoTO> adapter = new ArrayAdapter<ItemCatalogoTO>(c, android.R.layout.simple_spinner_item, list);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		return adapter;
	}
	
	public static int getAnioVigenciaPosition(String vigenciaAnio) {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);
		
		String anioInicial = String.valueOf(anio).substring(2, 4);
		int inicial = Integer.parseInt(anioInicial);
		int fin = Integer.parseInt(vigenciaAnio);
		
		return fin - inicial;
	}
	
	private static Date getDateValue(String s) {
		 SimpleDateFormat format = new SimpleDateFormat("yyMMdd", Locale.US);
		 try {
			 Date fecha = format.parse(s);
			 Log.i("getDateValue", fecha.toString());
			return fecha;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}
	
	public static double getDateDifference(String s) {
		
		Date d = getDateValue(s);
		
		double dateNum = (double) d.getTime();
		double actual = (double) new Date().getTime();
		
		Log.d(TAG, "getDateDifference nacimiento" + dateNum);
		Log.d(TAG, "getDateDifference Actual: " + actual);
		
		return (actual - dateNum) / MILLISECONDS_IN_YEAR;
		
	}
}
