package org.addcel.gdf.sql;

import java.util.LinkedList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class PredialDataSource {
	
	private SQLiteDatabase database;
	private Mysqlite sqlWizard;
	private String[] columnas = {Mysqlite.COLUMN_ID_CUENTA, Mysqlite.COLUMN_CUENTA};

	public PredialDataSource(Context context) {
		sqlWizard = new Mysqlite(context);
	}
	
	public void open() throws SQLException {
		database = sqlWizard.getWritableDatabase();
	}
	
	public void close() {
		sqlWizard.close();
	}
	
	public long insertaNuevo(String predial) {
		long insertada = 0;
		ContentValues valores = new ContentValues();
		valores.put(Mysqlite.COLUMN_CUENTA, predial);
		insertada = database.insert(Mysqlite.TABLE_PREDIAL, Mysqlite.COLUMN_ID_CUENTA, valores);
		
		return insertada;
	}
	
	public void eliminarPredial(String predial) {
		String formattedPredial = "\"" + predial + "";
		database.delete(Mysqlite.TABLE_PREDIAL, Mysqlite.COLUMN_CUENTA + "  = " + formattedPredial, null);
	}
	
	public List<CuentaPredial> getAllCuentas() {
		List<CuentaPredial> cuentas = new LinkedList<CuentaPredial>();
		
		Cursor cursor = database.query(Mysqlite.TABLE_PREDIAL, columnas, null, null, null, null, null);
		cursor.moveToFirst();
		
		while (! cursor.isAfterLast()) {
			CuentaPredial cuenta = CursorAResultado(cursor);
			cuentas.add(cuenta);
			cursor.moveToNext();
		}
		
		cursor.close();
		return cuentas;
	}
	
	public CuentaPredial getCuenta() {
		Cursor cursor = database.query(Mysqlite.TABLE_PREDIAL, columnas, null, null, null, null, null);
		cursor.moveToFirst();
		CuentaPredial cuenta = CursorAResultado(cursor);
		cursor.close();
		
		return cuenta;
	}
	
	public CuentaPredial getLast() {
		Cursor cursor = database.query(Mysqlite.TABLE_PREDIAL, columnas, null, null, null, null, null);
		if(cursor.moveToLast()){
			CuentaPredial cuenta = CursorAResultado(cursor);
			cursor.close();
			return cuenta;
		} else {
			cursor.close();
			return null;
		}	
		
	}
	
	public boolean isOpen() {
		return database.isOpen();
	}
	
	public CuentaPredial CursorAResultado(Cursor cursor) {
		CuentaPredial existCuenta = new CuentaPredial();
		existCuenta.setId(cursor.getLong(0));
		existCuenta.setCuenta(cursor.getString(1));
		
		return existCuenta;
	}
}
