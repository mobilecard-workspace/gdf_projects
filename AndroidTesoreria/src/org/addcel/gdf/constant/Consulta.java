package org.addcel.gdf.constant;

public class Consulta {
	public static final String TENENCIA = "consultaTenencia";
	public static final String INFRACCION = "consultaInfraccion";
	public static final String NOMINA = "consultaNomina";
	public static final String PREDIAL = "consultaPredial";
	public static final String AGUA = "consultaAgua";
	public static final String PREDIAL_VENCIDO = "consultaPredialVencido";
}
