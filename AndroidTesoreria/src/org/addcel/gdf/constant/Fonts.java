package org.addcel.gdf.constant;

public class Fonts {

	public static final String GOTHAM_BOOK = "fonts/Gotham-Book.ttf";
	public static final String GOTHAM_BOLD = "fonts/Gotham-Bold.ttf";
	public static final String BODONI = "fonts/BODONI.ttf";
}
