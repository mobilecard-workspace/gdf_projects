package org.addcel.gdf.view;

import java.util.ArrayList;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Consulta;
import org.addcel.gdf.constant.Servicios;
import org.addcel.gdf.constant.Url;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.AppUtils;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.widget.SpinnerValues;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class ConsultaActivity extends SherlockActivity {

	SessionManager session;
	
	Spinner operacionSpinner, mesSpinner, anioSpinner;
	SpinnerValues operacionValues;
	Button consultarButton;
	
	ArrayList <BasicNameValuePair> operaciones;
	
	private static final String TAG = "ConsultaActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_consulta);
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(ConsultaActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(ConsultaActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					CustomToast.build(ConsultaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(ConsultaActivity.this, RegistroActivity.class));
				}
				break;
				
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(ConsultaActivity.this, DataUpdateActivity.class));
				} else {
					CustomToast.build(ConsultaActivity.this, "Inicia Sesi�n para actualizar tus datos");
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					CustomToast.build(ConsultaActivity.this, "Ya eres usuario MobileCard.");
				} else {
					startActivity(new Intent(ConsultaActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		session = new SessionManager(ConsultaActivity.this);
		
		operaciones = new ArrayList<BasicNameValuePair>();
		operaciones.add(new BasicNameValuePair("N�mina", String.valueOf(Servicios.NOMINA)));
		operaciones.add(new BasicNameValuePair("Tenencia", String.valueOf(Servicios.TENENCIA)));
		operaciones.add(new BasicNameValuePair("Infracci�n", String.valueOf(Servicios.INFRACCION)));
		operaciones.add(new BasicNameValuePair("Predial Vigente", String.valueOf(Servicios.PREDIAL)));
		operaciones.add(new BasicNameValuePair("Agua Vigente", String.valueOf(Servicios.AGUA)));
		operaciones.add(new BasicNameValuePair("Predial Vencido", String.valueOf(Servicios.PREDIAL_VENCIDOS)));
	}
	
	public void initGUI() {
		initData();
		
		operacionSpinner = (Spinner) findViewById(R.id.spinner_operacion);
		operacionValues = new SpinnerValues(ConsultaActivity.this, operacionSpinner, operaciones);
		mesSpinner = (Spinner) findViewById(R.id.spinner_mes);
		setAnioSpinner();
		consultarButton = (Button) findViewById(R.id.button_consultar);
		consultarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (session.isLoggedIn()) {
					executeConsulta();
				} else {
					CustomToast.build(ConsultaActivity.this, "Inicia Sesi�n para consultar tus pagos");
				}
			}
		});
	}
	
	public void setAnioSpinner() {
		anioSpinner = (Spinner) findViewById(R.id.spinner_anio);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(ConsultaActivity.this, 
																android.R.layout.simple_spinner_item, 
																AppUtils.getAniosConsulta());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		anioSpinner.setAdapter(adapter);
	}
	
	public void executeConsulta() {
		String idUsuario = session.getUserDetails().get(SessionManager.USR_ID);
		int idProducto = Integer.parseInt(operacionValues.getValorSeleccionado());
		String mes = (String) mesSpinner.getSelectedItem();
		String anio = (String) anioSpinner.getSelectedItem();
		
		String request = Request.buildConsultaRequest(idUsuario, idProducto, mes, anio);
		new WebServiceClient(new ConsultaListener(), ConsultaActivity.this, true, Url.CONSULTA_PAGOS).execute(request);
	}
	
	private class ConsultaListener implements WSResponseListener {
		
		private static final String TAG = "ConsultaListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptSensitive(response);
				Log.d(TAG, response);
				
				try {
					JSONObject json = new JSONObject(response);
					JSONArray array = null;
					int servicio = -1;
					
					if (json.has(Consulta.TENENCIA)) {
						array = json.getJSONArray(Consulta.TENENCIA);
						servicio = Servicios.TENENCIA;
					} else if (json.has(Consulta.INFRACCION)) {
						array = json.getJSONArray(Consulta.INFRACCION);
						servicio = Servicios.INFRACCION;
					} else if (json.has(Consulta.NOMINA)) {
						array = json.getJSONArray(Consulta.NOMINA);
						servicio = Servicios.NOMINA;
					} else if (json.has(Consulta.PREDIAL)) {
						array = json.getJSONArray(Consulta.PREDIAL);
						servicio = Servicios.PREDIAL;
					} else if (json.has(Consulta.AGUA)) {
						array = json.getJSONArray(Consulta.AGUA);
						servicio = Servicios.AGUA;
					} else if (json.has(Consulta.PREDIAL_VENCIDO)) {
						array = json.getJSONArray(Consulta.PREDIAL_VENCIDO);
						servicio = Servicios.PREDIAL_VENCIDOS;
					}
					
					if (array.length() == 0) {
						CustomToast.build(ConsultaActivity.this, "No existen pagos para el tr�mite seleccionado.");
					} else {
						Intent intent = new Intent(ConsultaActivity.this, ResultadosActivity.class);
						intent.putExtra("resultados", array.toString());
						intent.putExtra("servicio", servicio);
						startActivity(intent);
					}
					
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					CustomToast.build(ConsultaActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
				}
			} else {
				CustomToast.build(ConsultaActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde");
			}
		}
	}
}
