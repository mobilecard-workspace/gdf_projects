package org.addcel.gdf.view;

import java.util.Arrays;
import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.adapter.GdfListAdapter;
import org.addcel.gdf.constant.Fonts;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.mobilecard.view.LoginActivity;
import org.addcel.mobilecard.view.PassRecoverActivity;
import org.addcel.mobilecard.view.RegistroActivity;
import org.addcel.session.SessionManager;
import org.addcel.util.Dialogos;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class DepartamentosActivity extends SherlockActivity {
	
	private List<String> departamentos;
	private ListView departamentosList;
	SessionManager session;
	
	private static final String TAG = "DepartamentosActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listmenu);
		session = new SessionManager(DepartamentosActivity.this);
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		
		switch (id) {
			case R.id.action_login:
				if (!session.isLoggedIn()) {
					startActivity(new Intent(DepartamentosActivity.this, LoginActivity.class));
				} else {
					Dialogos.makeYesNoAlert(DepartamentosActivity.this, "�Deseas cerrar tu sesi�n?", new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							session.logoutUser();
							dialog.dismiss();
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
				}
				break;
			case R.id.action_registro:
				if (session.isLoggedIn()) {
					Toast.makeText(DepartamentosActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
				} else {
					startActivity(new Intent(DepartamentosActivity.this, RegistroActivity.class));
				}
				break;
			case R.id.action_update:
				if (session.isLoggedIn()) {
					startActivity(new Intent(DepartamentosActivity.this, DataUpdateActivity.class));
				} else {
					Toast.makeText(DepartamentosActivity.this, "Inicia Sesi�n para actualizar tus datos", Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.action_password:
				if (session.isLoggedIn()) {
					Toast.makeText(DepartamentosActivity.this, "Ya eres usuario MobileCard.", Toast.LENGTH_SHORT).show();
				} else {
					startActivity(new Intent(DepartamentosActivity.this, PassRecoverActivity.class));
				}
				break;
	
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initGUI() {
		departamentos = Arrays.asList(getResources().getStringArray(R.array.arr_deptos));
		Log.i(TAG, departamentos.toString());
		
		Typeface gothamBold = Typeface.createFromAsset(getAssets(), Fonts.GOTHAM_BOLD);
		
		((TextView) findViewById(R.id.view_titulo)).setTypeface(gothamBold);
		
		departamentosList = (ListView) findViewById(R.id.list_options);
		departamentosList.setAdapter(new GdfListAdapter(DepartamentosActivity.this, departamentos));
		departamentosList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(DepartamentosActivity.this, TramiteActivity.class);
				intent.putExtra("departamento", arg2);
				startActivity(intent);
			}
		});
	}
}
