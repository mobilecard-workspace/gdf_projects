package org.addcel.gdf.view;

import java.util.ArrayList;
import java.util.List;

import org.addcel.gdf.R;
import org.addcel.gdf.adapter.ConsultaAdapter;
import org.addcel.gdf.constant.Servicios;
import org.addcel.mobilecard.view.DataUpdateActivity;
import org.addcel.session.SessionManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class ResultadosActivity extends SherlockActivity {
	
	SessionManager session;
	String resultadoString;
	ListView resultadosList;
	int servicio;
	List<JSONObject> resultados;
	
	private static final String TAG = "ResultadosActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resultados);
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_pago, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int itemId = item.getItemId();
		
		switch (itemId) {
		case R.id.action_update:
			startActivity(new Intent(ResultadosActivity.this, DataUpdateActivity.class));
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		session = new SessionManager(ResultadosActivity.this);
		resultadoString = getIntent().getStringExtra("resultados");
		servicio = getIntent().getIntExtra("servicio", -1);
	}
	
	public void setResultados() {
		try {
			JSONArray array = new JSONArray(resultadoString);
			resultados = new ArrayList<JSONObject>(array.length());
			
			for (int i = 0; i < array.length(); i++) {
				JSONObject json = array.getJSONObject(i);
				resultados.add(json);
			}
			
			ConsultaAdapter adapter = new ConsultaAdapter(ResultadosActivity.this, resultados, servicio);
			resultadosList.setAdapter(adapter);
			resultadosList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					JSONObject json = (JSONObject) ((ConsultaAdapter) arg0.getAdapter()).getItem(arg2);
					Intent intent = null;
					
					switch (servicio) {
						case Servicios.TENENCIA:
							intent = new Intent(ResultadosActivity.this, DetalleTenenciaActivity.class);
							break;
						case Servicios.INFRACCION:
							intent = new Intent(ResultadosActivity.this, DetalleInfraccionActivity.class);
							break;
						case Servicios.NOMINA:
							intent = new Intent(ResultadosActivity.this, DetalleNominaActivity.class);
							break;
						case Servicios.PREDIAL:
							intent = new Intent(ResultadosActivity.this, DetallePredialActivity.class);
							break;
						case Servicios.AGUA:
							intent = new Intent(ResultadosActivity.this, DetalleAguaActivity.class);
							break;
						case Servicios.PREDIAL_VENCIDOS:
							intent = new Intent(ResultadosActivity.this, DetallePredialVencidoActivity.class);
							break;
						default:
							break;
					}
					
					intent.putExtra("json", json.toString());
					startActivity(intent);
					finish();
				}
			});
		} catch (JSONException e) {
			Log.e(TAG,"JSONException", e);
			Toast.makeText(ResultadosActivity.this, "El detalle del tr�mite seleccionado no est� disponible.", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void initGUI() {
		initData();	
		
		resultadosList = (ListView) findViewById(R.id.list_resultados);
		setResultados();
		
		
	}
}
