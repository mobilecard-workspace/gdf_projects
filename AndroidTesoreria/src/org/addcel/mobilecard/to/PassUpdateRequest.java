package org.addcel.mobilecard.to;

import org.addcel.crypto.Crypto;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class PassUpdateRequest {
	
	private String login;
	private String passwordS;
	private String password;
	
	private static final String TAG = "PassUpdateRequest";
	
	public PassUpdateRequest(String login, String passwordS, String password) {
		this.login = login;
		this.passwordS = passwordS;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPasswordS() {
		return passwordS;
	}

	public void setPasswordS(String passwordS) {
		this.passwordS = passwordS;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Construye JSON y lo cifra
	 * @return String que se usa como request para actualizar password despu�s de hacer proceso de registro.
	 */
	public String build() {
		JSONObject json = new JSONObject();
		
		try {
			json.put("login", login);
			json.put("passwordS", Crypto.sha1(passwordS));
			json.put("password", password);
			json.put("newPassword", password);
			
			Log.d(TAG, json.toString());
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String request = Crypto.aesEncrypt(Text.parsePass(password), json.toString());
		request = Text.mergeStr(request, password);
		
		return request;
		
		
	}
	
	/**
	 * Construye JSON y lo cifra
	 * @return String que se usa como request para actualizar password despu�s de hacer proceso de recuperaci�n.
	 */
	public String buildRecovery() {
		JSONObject json = new JSONObject();
		
		try {
			json.put("login", login);
			json.put("password", passwordS);
			json.put("newPassword", password);
		} catch (JSONException e) {
			Log.e(TAG, "", e);
			return "";
		}
		
		String request = Crypto.aesEncrypt(Text.parsePass(passwordS), json.toString());
		request = Text.mergeStr(request, passwordS);
		
		return request;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Update: " + login;
	}
	
	
}
