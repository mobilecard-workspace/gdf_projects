package org.addcel.mobilecard.to;

public class ItemCatalogoTO {
	
	private String id;
	private String data;
	
	public ItemCatalogoTO() {}
	
	public ItemCatalogoTO(String id, String data) {
		this.id = id;
		this.data = data;
	}
	
	public String getId() {
		return id;
	}
	
	public String getData() {
		return data;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return data;
	}
	
	

}
