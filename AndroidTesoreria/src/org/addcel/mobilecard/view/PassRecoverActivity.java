package org.addcel.mobilecard.view;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.gdf.R;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.constant.UrlMc;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class PassRecoverActivity extends SherlockActivity {
	
	EditText loginEdit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mc_passrecover);
		initGUI();
	}
	
	private void initData() {}
	
	public void initGUI() {
		initData();
		
		loginEdit = (EditText) findViewById(R.id.edit_login);
	}
	
	private boolean validate() {
		if (loginEdit.getText().toString().isEmpty()) {
			loginEdit.requestFocus();
			loginEdit.setError("D�gite su nombre de usuario");
			return false;
		}
		
		if (loginEdit.getText().toString().length() < 8) {
			loginEdit.requestFocus();
			loginEdit.setError("El nombre de usuario no puede ser menor a 8 caracteres");
			return false;
		}
		
		return true;
	}
	
	private String buildRequest() {
		JSONObject json = new JSONObject();
		
		try {
			json.put("cadena", loginEdit.getText().toString().trim());
		} catch (JSONException e) {
			return "";
		}
		
		return AddcelCrypto.encryptHard(json.toString());
	}
	
	public void executeAction(View v) {
		if (null != v) {
			int id = v.getId();
			
			switch (id) {
			case R.id.button_recuperar:
				if (validate()) {
					new WebServiceClient(new RecoveryListener(), PassRecoverActivity.this, true, UrlMc.RECOVER_PASS).execute(buildRequest());
				}
				break;

			default:
				break;
			}
		}
	}
	
	private class RecoveryListener implements WSResponseListener {
		
		private static final String TAG = "RecoveryListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, "RESPONSE RECOVERY: " + response);
				
				if (response.equals("0")) {
					Toast.makeText(PassRecoverActivity.this, "Se ha enviado el password con �xito", Toast.LENGTH_SHORT).show();
					finish();
				} else {
					String [] error = response.split("\\|");
					Toast.makeText(PassRecoverActivity.this, error[1], Toast.LENGTH_SHORT).show();
				}
				
			} else {
				Toast.makeText(PassRecoverActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
			}
		}
	}

}
