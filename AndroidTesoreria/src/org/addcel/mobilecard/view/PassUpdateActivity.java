package org.addcel.mobilecard.view;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.Crypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.constant.UrlMc;
import org.addcel.mobilecard.to.PassUpdateRequest;
import org.addcel.session.SessionManager;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PassUpdateActivity extends Activity {
	private EditText viejoPassEdit;
	private EditText nuevoPassEdit;
	private EditText confPassEdit;
	private Button actualizarButton;
	
	private SessionManager session;
	
	private String id;
	private String user;
	private int status;
	
	private static final String TAG = "PassUpdateActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mc_passupdate);
		
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
	}
	
	private void initData() {
		session = new SessionManager(PassUpdateActivity.this);
		id = getIntent().getStringExtra("id");
		user = getIntent().getStringExtra("user");
		status = getIntent().getIntExtra("status", 98);
	}
	
	public void initGUI() {
		
		initData();

		
		viejoPassEdit = (EditText) findViewById(R.id.edit_pass_actual);
		nuevoPassEdit = (EditText) findViewById(R.id.edit_pass_nuevo);
		confPassEdit = (EditText) findViewById(R.id.edit_pass_confirmar);
		actualizarButton = (Button) findViewById(R.id.button_actualizar);
		
		actualizarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				executeUpdate();
			}
		});
	}
	
	public void executeUpdate() {
		if (validate()) {
			String passwordS = viejoPassEdit.getText().toString().trim();
			String password = nuevoPassEdit.getText().toString().trim();
			
			PassUpdateRequest request = new PassUpdateRequest(user, passwordS, password);
			Log.i(TAG, request.toString());
			
			
			switch (status) {
			case 98:
				new WebServiceClient(new PassUpdateListener(), PassUpdateActivity.this, true, UrlMc.UPDATE_PASS_AFTER_RECOVER).execute(request.buildRecovery());
				break;
			case 99:
				new WebServiceClient(new PassUpdateListener(), PassUpdateActivity.this, true, UrlMc.UPDATE_PASS).execute(request.build());
				break;
			default:
				break;
			}

		}
	}
	
	private boolean validate() {
		
		String viejoPass = viejoPassEdit.getText().toString().trim();
		String nuevoPass = nuevoPassEdit.getText().toString().trim();
		String confPass = confPassEdit.getText().toString().trim();
		
		if ("".equals(viejoPass)) {
			viejoPassEdit.setError("El campo contrase�a actual no puede ir vac�o.");
			return false;
		} else if (8 > viejoPass.length()) {
			viejoPassEdit.setError("Contrase�a actual debe ser mayor o igual a 8 caracteres.");
			return false;
		} else if ("".equalsIgnoreCase(nuevoPass)) {
			nuevoPassEdit.setError("El campo contrase�a nueva no puede ir vac�o.");
			return false;
		} else if (8 > nuevoPass.length()) {
			nuevoPassEdit.setError("Nueva contrase�a debe ser mayor o igual a 8 caracteres.");
			return false;
		} else if (!confPass.equals(nuevoPass)) {
			confPassEdit.setError("Confirmaci�n de contrase�a no coincide.");		
			return false;
		}
		
		return true;
	}
	
	private class PassUpdateListener implements WSResponseListener {
		
		
		private static final String TAG = "PassUpdateListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			String seed = "";
			
			if (status == 98)
				seed = Text.parsePass(viejoPassEdit.getText().toString().trim());
			else if (status == 99)
				seed = Text.parsePass(nuevoPassEdit.getText().toString().trim());
				
			response = Crypto.aesDecrypt(seed, response);
			
			Log.d(TAG, "Response: " + response);
			
			if (response != null && !response.isEmpty()) {
				
				try {
					JSONObject responseJSON = new JSONObject(response);
					Log.i("json_response", responseJSON.toString(1));
					
					int resultado = responseJSON.getInt("resultado");
					String mensaje = responseJSON.getString("mensaje");
					
					if (1 == resultado) {
						
						session.createLoginSession(id, user, nuevoPassEdit.getText().toString().trim());
						
						Toast.makeText(PassUpdateActivity.this, mensaje, Toast.LENGTH_SHORT).show();
						
						finish();
						
					} else {
						Toast.makeText(PassUpdateActivity.this, mensaje, Toast.LENGTH_SHORT).show();					
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Log.e(TAG, "", e);
					Toast.makeText(PassUpdateActivity.this, "Error de conexi�n, intente de nuevo m�s tarde...", Toast.LENGTH_SHORT).show();
				}
				
			} else {
				Toast.makeText(PassUpdateActivity.this, "Error de conexi�n, intente de nuevo m�s tarde...", Toast.LENGTH_SHORT).show();
			}
			
			
		}
		
	}
	
	
}
