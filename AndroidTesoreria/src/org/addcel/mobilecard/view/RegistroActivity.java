package org.addcel.mobilecard.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.addcel.client.Request;
import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.crypto.Crypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.gdf.constant.Keys;
import org.addcel.gdf.constant.Url;
import org.addcel.interfaces.DialogOkListener;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.constant.UrlMc;
import org.addcel.mobilecard.to.RegistroRequest;
import org.addcel.util.AppUtils;
import org.addcel.util.CustomToast;
import org.addcel.util.Dialogos;
import org.addcel.util.Text;
import org.addcel.util.Validador;
import org.addcel.widget.SpinnerValues;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class RegistroActivity extends SherlockActivity {
	
	private DatePickerDialog dateDialog;
	private int anio, mes, dia;
	private Date nacimientoDate;
	
	EditText usuarioEdit, celularEdit, confCelularEdit, correoEdit, confCorreoEdit;
	EditText nombreEdit, paternoEdit, maternoEdit, nacimientoEdit, casaEdit, oficinaEdit;
	EditText ciudadEdit, calleEdit, exteriorEdit, interiorEdit, coloniaEdit, cpEdit;
	EditText curpEdit, rfcEdit, numTarjetaEdit;
	
	Spinner proveedorSpinner, generoSpinner, estadoSpinner, tipoTarjetaSpinner, mesSpinner, anioSpinner;
	SpinnerValues proveedorValues, estadosValues, tipoTarjetaValues;
	
	Button terminosButton, registrarButton;
	CheckBox aceptadosCheck;
	
	RegistroRequest request;
	
	private String terminos;
	
	private static final String TAG = "RegistroActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mc_registro);
		
		initGUI();
		
		new WebServiceClient(new ProveedoresListener(), RegistroActivity.this, true, UrlMc.PROVEEDORES).execute();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_registro, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int itemId = item.getItemId();
		switch (itemId) {
		case R.id.action_login:
			startActivity(new Intent(RegistroActivity.this, LoginActivity.class));
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public Date getNacimientoDate() {
		return nacimientoDate;
	}

	public void setNacimientoDate(Date nacimientoDate) {
		this.nacimientoDate = nacimientoDate;
	}

	public void initGUI() {
		
		
		usuarioEdit = (EditText) findViewById(R.id.edit_usuario);
		usuarioEdit.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(usuarioEdit.getText().toString().trim().length() < 8) {
					usuarioEdit.setError("El campo usuario no puede ser menor a 8 caracteres");
				}
			}
		});
		
		celularEdit = (EditText) findViewById(R.id.edit_celular);
		confCelularEdit = (EditText) findViewById(R.id.edit_conf_celular);
		correoEdit = (EditText) findViewById(R.id.edit_correo);
		confCorreoEdit = (EditText) findViewById(R.id.edit_conf_correo);
		nombreEdit = (EditText) findViewById(R.id.edit_nombre);
		paternoEdit = (EditText) findViewById(R.id.edit_paterno);
		maternoEdit = (EditText) findViewById(R.id.edit_materno);
		nacimientoEdit = (EditText) findViewById(R.id.edit_fecha);
		casaEdit = (EditText) findViewById(R.id.edit_casa);
		oficinaEdit = (EditText) findViewById(R.id.edit_oficina);
		ciudadEdit = (EditText) findViewById(R.id.edit_ciudad);
		calleEdit = (EditText) findViewById(R.id.edit_calle);
		exteriorEdit = (EditText) findViewById(R.id.edit_exterior);
		interiorEdit = (EditText) findViewById(R.id.edit_interior);
		coloniaEdit = (EditText) findViewById(R.id.edit_colonia);
		cpEdit = (EditText) findViewById(R.id.edit_cp);
		rfcEdit = (EditText) findViewById(R.id.edit_rfc);
		numTarjetaEdit = (EditText) findViewById(R.id.edit_tarjeta);
		
		proveedorSpinner = (Spinner) findViewById(R.id.spinner_proveedor);
		generoSpinner = (Spinner) findViewById(R.id.spinner_genero);
		estadoSpinner = (Spinner) findViewById(R.id.spinner_estado);
		tipoTarjetaSpinner = (Spinner) findViewById(R.id.spinner_tarjeta);
		mesSpinner = (Spinner) findViewById(R.id.spinner_mes);
		anioSpinner = (Spinner) findViewById(R.id.spinner_anio);
		
		aceptadosCheck = (CheckBox) findViewById(R.id.check_aceptados);
		terminosButton = (Button) findViewById(R.id.button_terminos);
		registrarButton = (Button) findViewById(R.id.button_registrar);
		
		terminosButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String request = Request.buildTerminosRequest();
				new WebServiceClient(new TerminosListener(), RegistroActivity.this, true, Url.CONSUMIDOR_TERMINOS).execute(request);
			}
		});
		
		registrarButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (validate()) {
					executeRegister();
				}
			}
		});
	}
	
	private void setGeneroSpinner() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistroActivity.this, 
																android.R.layout.simple_spinner_item, 
																getResources().getStringArray(R.array.sexo));
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		generoSpinner.setAdapter(adapter);
		
	}
	
	private void setMesSpinner() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistroActivity.this, 
																android.R.layout.simple_spinner_item, 
																getResources().getStringArray(R.array.arr_mes));
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mesSpinner.setAdapter(adapter);
	}
	
	private void setAnioSpinner() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegistroActivity.this, 
																android.R.layout.simple_spinner_item, 
																AppUtils.getAniosVigencia());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		anioSpinner.setAdapter(adapter);
	}
	
	public DatePickerDialog getDatePickerDialog() {
		if (dateDialog == null) {
			final Calendar c = Calendar.getInstance();
			
			dateDialog = new DatePickerDialog(RegistroActivity.this, new OnDateSetListener() {
				
				@Override
				public void onDateSet(DatePicker view, int year, int monthOfYear,
						int dayOfMonth) {
					// TODO Auto-generated method stub
					dia = dayOfMonth;
					mes = monthOfYear;
					anio = year;
					
					c.set(anio, mes, dia);
					
					setNacimientoDate(c.getTime());
					nacimientoEdit.setText(Text.formatDateForShowing(getNacimientoDate()));
					
				}
			}, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
			
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;
			
			Log.i(TAG, "Versi�n SO: " + currentapiVersion);

			if (currentapiVersion > 10) {
				dateDialog.getDatePicker().setCalendarViewShown(false);
			}
		}
		
		return dateDialog;
	}
	
	public void showfechaDialog(View v) {
		switch (v.getId()) {
			case R.id.edit_fecha:
				if (!getDatePickerDialog().isShowing()) {
					getDatePickerDialog().show();
				}
			break;

		default:
			break;
		}
	}
	
	public boolean validate() {
		
		String usuario = usuarioEdit.getText().toString().trim();
		String celular = celularEdit.getText().toString().trim();
		String confCel = confCelularEdit.getText().toString().trim();
		String mail = correoEdit.getText().toString().trim();
		String confMail = confCorreoEdit.getText().toString().trim();
		String nombre = nombreEdit.getText().toString().trim();
		String paterno = paternoEdit.getText().toString().trim();
		String ciudad = ciudadEdit.getText().toString().trim();
		String calle = calleEdit.getText().toString().trim();
		String exterior = exteriorEdit.getText().toString().trim();
		String colonia = coloniaEdit.getText().toString().trim();
		String tarjeta = numTarjetaEdit.getText().toString().trim();
		
		if (!aceptadosCheck.isChecked()) {
			Log.i(TAG, "Terminos no aceptados...");
			terminosButton.requestFocus();
			CustomToast.build(RegistroActivity.this, "Acepte t�rminos y condiciones");
			return false;
		} else if (usuario.equals("")) {
			usuarioEdit.requestFocus();
			usuarioEdit.setError("El campo usuario no puede ir vac�o.");
			return false;
		} else if (usuario.length() < 8) {
			usuarioEdit.requestFocus();
			usuarioEdit.setError("El campo usuario no puede ser menor a 8 caracteres");
			return false;
		} else if (celular.equals("")) {
			celularEdit.requestFocus();
			celularEdit.setError("El campo celular no puede ir vac�o.");
			return false;
		} else if (celular.length() != 10) {
			celularEdit.requestFocus();
			celularEdit.setError("El campo celular debe constar de 10 caracteres.");
			return false;
		} else if (!confCel.equals(celular)) {
			confCelularEdit.requestFocus();
			confCelularEdit.setError("N�meros de celular no coinciden.");
			return false;
		} else if (mail.equals("")) {
			correoEdit.requestFocus();
			correoEdit.setError("El campo correo no puede ir vacion.");
			return false;
		} else if (!Validador.esCorreo(mail)) {
			correoEdit.requestFocus();
			correoEdit.setError("Correo electr�nico no v�lido.");
			return false;
		} else if (!confMail.equals(mail)) {
			confCorreoEdit.requestFocus();
			confCorreoEdit.setError("Correos electr�nicos no coinciden.");
			return false;
		} else if (nombre.equals("")) {
			nombreEdit.requestFocus();
			nombreEdit.setError("El campo nombre no puede ir vac�o.");
			return false;
		} else if (paterno.equals("")) {
			paternoEdit.requestFocus();
			paternoEdit.setError("El campo apellido paterno no puede ir vac�o.");
			return false;
		} else if (getNacimientoDate() == null) {
			nacimientoEdit.requestFocus();
			nacimientoEdit.setError("Seleccione fecha de nacimiento");
			return false;
		} else if (ciudad.equals("")) {
			ciudadEdit.requestFocus();
			ciudadEdit.setError("El campo ciudad no puede ir vac�o.");
			return false;
		} else if (calle.equals("")) {
			calleEdit.requestFocus();
			calleEdit.setError("El campo calle no puede ir vac�o.");
			return false;
		} else if (exterior.equals("")) {
			exteriorEdit.requestFocus();
			exteriorEdit.setError("El campo n�mero exterior no puede ir vac�o.");
			return false;
		} else if (colonia.equals("")) {
			coloniaEdit.requestFocus();
			coloniaEdit.setError("El campo colonia no puede ir vac�o.");
			return false;
		} else if (tarjeta.equals("")) {
			numTarjetaEdit.requestFocus();
			numTarjetaEdit.setError("El campo tarjeta no puede ir vac�o.");
			return false;
		} else if (tipoTarjetaValues.getValorSeleccionado().equals("1")) {
			if (!Validador.esTarjetaDeCreditoVisa(tarjeta)) {
				numTarjetaEdit.requestFocus();
				numTarjetaEdit.setError("N�mero de tarjeta no v�lido.");
				return false;
			}
		} else if (tipoTarjetaValues.getValorSeleccionado().equals("2")) {
			if (!Validador.esTarjetaDeCreditoMasterCard(tarjeta)) {
				numTarjetaEdit.requestFocus();
				numTarjetaEdit.setError("N�mero de tarjeta no v�lido.");
				return false;
			}		
		}
		return true;
	}
	
	public void executeRegister() {
		request = new RegistroRequest();
		request.setApellido(paternoEdit.getText().toString().trim());
		request.setCalle(calleEdit.getText().toString().trim());
		request.setCiudad(ciudadEdit.getText().toString().trim());
		request.setColonia(coloniaEdit.getText().toString().trim());
		request.setCp(cpEdit.getText().toString().trim());
		request.setIdEstado(estadosValues.getValorSeleccionado());
		request.setLogin(usuarioEdit.getText().toString().trim());
		request.setMail(correoEdit.getText().toString().trim());
		request.setMaterno(maternoEdit.getText().toString().trim());
		request.setNacimiento(Text.formatDate(getNacimientoDate()));
		request.setNombre(nombreEdit.getText().toString().trim());
		request.setNumExt(exteriorEdit.getText().toString().trim());
		request.setNumInterior(interiorEdit.getText().toString().trim());
		request.setProveedor(proveedorValues.getValorSeleccionado());
		request.setSexo(((String) generoSpinner.getSelectedItem()).substring(0, 1));
		request.setTarjeta(numTarjetaEdit.getText().toString().trim());
		request.setTelCasa(casaEdit.getText().toString().trim());
		request.setTelefono(celularEdit.getText().toString().trim());
		request.setTelOficina(oficinaEdit.getText().toString().trim());
		request.setTipoTarjeta(tipoTarjetaValues.getValorSeleccionado());
		
		String mes = (String) mesSpinner.getSelectedItem();
		String anio = (String) anioSpinner.getSelectedItem();
		
		request.setVigencia(mes +"/"+anio.substring(2, 4));
		
		new WebServiceClient(new RegistroListener(), RegistroActivity.this, true, UrlMc.REGISTRO).execute(request.build(RegistroActivity.this));
	}
	
	private class ProveedoresListener implements WSResponseListener {
		
		private static final String TAG = "ProveedoresListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONObject json = new JSONObject(response);
					JSONArray array = json.optJSONArray("proveedores");
					
					if (array.length() > 0) {
						ArrayList<BasicNameValuePair> proveedores = new ArrayList<BasicNameValuePair>(array.length());
						
						for (int i = 0; i < array.length(); i++) {
							JSONObject proveedor = array.optJSONObject(i);
							proveedores.add(new BasicNameValuePair(proveedor.optString("descripcion"), proveedor.optString("clave")));
						}
						
						proveedorValues = new SpinnerValues(RegistroActivity.this, proveedorSpinner, proveedores);
						setGeneroSpinner();
						
						new WebServiceClient(new EstadosListener(), RegistroActivity.this, true, UrlMc.ESTADOS).execute();
						
					} else {
						Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
						finish();
					}
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
					finish();
				}
			} else {
				Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}
	
	private class EstadosListener implements WSResponseListener {
		
		private static final String TAG = "EstadosListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				Log.i(TAG, response);
				
				try {
					JSONObject json = new JSONObject(response);
					JSONArray array = json.optJSONArray("estados");
					
					if (array.length() > 0) {
						ArrayList<BasicNameValuePair> proveedores = new ArrayList<BasicNameValuePair>(array.length());
						
						for (int i = 0; i < array.length(); i++) {
							JSONObject proveedor = array.optJSONObject(i);
							proveedores.add(new BasicNameValuePair(proveedor.optString("descripcion"), proveedor.optString("clave")));
						}
						
						estadosValues = new SpinnerValues(RegistroActivity.this, estadoSpinner, proveedores);
						new WebServiceClient(new TipoTarjetaListener(), RegistroActivity.this, true, UrlMc.TARJETAS).execute();
					} else {
						Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
						finish();
					}
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
					finish();
				}
			} else {
				Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}
	
	private class TipoTarjetaListener implements WSResponseListener {
		private static final String TAG = "TipoTarjetaListener";
		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null != response && !"".equals(response)) {
				response = AddcelCrypto.decryptHard(response);
				
				Log.i(TAG, response);
				
				try {
					JSONObject json = new JSONObject(response);
					JSONArray array = json.optJSONArray("tarjetas");
					
					if (array.length() > 0) {
						ArrayList<BasicNameValuePair> proveedores = new ArrayList<BasicNameValuePair>(array.length());
						
						for (int i = 0; i < array.length() - 1; i++) {
							JSONObject proveedor = array.optJSONObject(i);
							proveedores.add(new BasicNameValuePair(proveedor.optString("descripcion"), proveedor.optString("clave")));
						}
						
						tipoTarjetaValues = new SpinnerValues(RegistroActivity.this, tipoTarjetaSpinner, proveedores);
						setMesSpinner();
						setAnioSpinner();
					} else {
						Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
						finish();
					}
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
					finish();
				}
			} else {
				Toast.makeText(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}
	
	private class TerminosListener implements WSResponseListener {

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				
				response = AddcelCrypto.decryptHard(response);
				
				try {
					JSONObject json = new JSONObject(response);
					JSONArray consultaTerminosCondiciones = json.getJSONArray("consultaTerminosCondiciones");
					JSONObject termino = consultaTerminosCondiciones.getJSONObject(0);
					terminos = termino.getString("desc_termino");
					
					Dialogos.makeAcceptAlert(RegistroActivity.this, terminos, new DialogOkListener() {
						
						@Override
						public void ok(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							aceptadosCheck.setChecked(true);
						}
						
						@Override
						public void cancel(DialogInterface dialog, int id) {
							// TODO Auto-generated method stub
							aceptadosCheck.setChecked(false);
						}
					});
					
					
				} catch (JSONException e) {
					Toast.makeText(RegistroActivity.this, "Error de conexi�n, intente de nuevo m�s tarde...", Toast.LENGTH_SHORT).show(); 
					
				}
			} else {
				Toast.makeText(RegistroActivity.this, "Error de conexi�n, intente de nuevo m�s tarde...", Toast.LENGTH_SHORT).show(); 
				
			}
		}
		
	}
	
	private class RegistroListener implements WSResponseListener {
		
		private final static String TAG = "RegistroListener";
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null != response && !"".equals(response)) {
				response = Crypto.aesDecrypt(Text.parsePass(Keys.REGISTER), response);
				Log.d(TAG, response);
				
				try {
					JSONObject json = new JSONObject(response);
					Log.i(TAG, json.toString());
					
					int resultado = json.getInt("resultado");
					String mensaje = json.getString("mensaje");
					
					switch (resultado) {
						case 1:
							CustomToast.build(RegistroActivity.this, mensaje);
							finish();
							break;
	
						default:
							CustomToast.build(RegistroActivity.this, mensaje);
							break;
					}
				} catch (JSONException e) {
					Log.e(TAG, "", e);
					CustomToast.build(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.");
				}
			} else {
				CustomToast.build(RegistroActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.");
			}
		}
	}
}
