package org.addcel.mobilecard.view;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.Crypto;
import org.addcel.gdf.R;
import org.addcel.gdf.constant.Fonts;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilecard.constant.UrlMc;
import org.addcel.mobilecard.to.LoginRequest;
import org.addcel.session.SessionManager;
import org.addcel.util.CustomToast;
import org.addcel.util.Text;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class LoginActivity extends SherlockActivity{
	
	private EditText loginEdit, passEdit;
	private Button loginButton;
	private String login, password;
	private SessionManager session;
	
	private final static String TAG = "LoginActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		session = new SessionManager(LoginActivity.this);
		
		initGUI();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getSupportMenuInflater().inflate(R.menu.menu_login, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		int itemId = item.getItemId();
		
		switch (itemId) {
		case R.id.action_registro:
			startActivity(new Intent(LoginActivity.this, RegistroActivity.class));
			break;
		case R.id.action_password:
				startActivity(new Intent(LoginActivity.this, PassRecoverActivity.class));
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initGUI() {
		
		
		loginEdit = (EditText) findViewById(R.id.edit_login);
		passEdit = (EditText) findViewById(R.id.edit_password);
		loginButton = (Button) findViewById(R.id.button_login);
		
		loginButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				login = loginEdit.getText().toString().trim();
				password = passEdit.getText().toString().trim();
				
				login(login, password);
			}
		});
	}
	
	private void login(String login, String password) {
		if (validate(login, password)) {
			LoginRequest request = new LoginRequest(login, password);
			new WebServiceClient(new LoginListener(), LoginActivity.this, true, UrlMc.LOGIN).execute(request.build(LoginActivity.this));
		}
	}
	
	private boolean validate(String login, String pass) {
		if (login.length() < 8 ) {
			loginEdit.requestFocus();
			loginEdit.setError("Su nombre de usuario debe ser mayor o igual a 8 caracteres");
			return false;
		}
		
		if (pass.length() < 8) {
			passEdit.requestFocus();
			passEdit.setError("Su contrase�a debe ser mayor o igual a 8 caracteres");
			return false;
		}
		
		return true;
	}
	
	private class LoginListener implements WSResponseListener {
		
		private static final String TAG = "LoginListener";
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, response);
			
			if (null == response || "".equals(response)) {
				CustomToast.build(LoginActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.");
			} else {
				response = Crypto.aesDecrypt(Text.parsePass(password), response);
				try {
					JSONObject json = new JSONObject(response);
					
					Log.i("RESPONSE LOGIN", json.toString(1));
					
					int resultado = json.getInt("resultado");
					String mensaje = json.getString("mensaje");
					
					Intent intent = null;
					String[] mensajeArr = null;
					
					switch (resultado) {
					case 1:
						mensajeArr = mensaje.split("\\|");
						session.createLoginSession(mensajeArr[1], login, password);
						
						CustomToast.build(getApplicationContext(), mensajeArr[0]);
						
						finish();
						break;
					case 98:
						mensajeArr = mensaje.split("\\|");
						
						CustomToast.build(getApplicationContext(), mensajeArr[0]);
						
						intent = new Intent(LoginActivity.this, PassUpdateActivity.class);
						
//						Log.i(TAG, "Usuario que se manda a PasswordUpdate: " + login);
						intent.putExtra("id", mensajeArr[1]);
						intent.putExtra("user", login);
						intent.putExtra("status", 98);						
						
						startActivity(intent);
						finish();
						
						break;
					
					case 99:
						mensajeArr = mensaje.split("\\|");
						
						CustomToast.build(getApplicationContext(), mensajeArr[0]);
						
						intent = new Intent(LoginActivity.this, PassUpdateActivity.class);
						
//						Log.i(TAG, "Usuario que se manda a PasswordUpdate: " + login);
						intent.putExtra("id", mensajeArr[1]);
						intent.putExtra("user", login);
						intent.putExtra("status", 99);
						
						startActivity(intent);
						finish();
						break;

					default:
						CustomToast.build(getApplicationContext(), mensaje);
						break;
					}
				} catch (JSONException e) {
					CustomToast.build(LoginActivity.this, "Error de conexi�n. Intente de nuevo m�s tarde.");
					Log.e(TAG, "JSONException", e);
				}
			}
		}
	}
}
