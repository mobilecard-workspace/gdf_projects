<%@page import="java.io.PrintWriter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
<head>
    <title>Purchase Results</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="HandheldFriendly" content="true"/>
    <style type="text/css">
#contenedor {
	width: 275px;
	margin: 0 auto;
}

html {
	font-family: arial;
	font-size: 12px;
	font-weight: bold;
	color: #021455;
	background-color: #FFFFFF;
}

td {
	font-family: arial;
	font-size: 12px;
}

.title {
	font-family: arial;
	font-size: 12px;
}

.title2 {
	font-family: arial;
	font-size: 12px;
}

input,select {
	width: 240px;
	height: 35px;
	font-family: arial;
	font-size: 14px;
}

select.mes {
	width: 120px;
}

.anio {
	width: 120px;
}

p.info {
	margin-top: 5px;
	margin-bottom: 5px;
}
</style>    
</head>
<%@page import="java.util.Enumeration"%>
<body>
<div id="contenedor">
		<p style="text-align: center;">
			<img src="http://50.57.192.210:8080/TlajomulcoServicios/resources/logo_abc.png" width="100px" height="30px" align="center"/>
		</p>	
		<p style="text-align: center;">Portal 3D Secure ABC Capital</p>
		<p class="info">Resultado de la transacion:</p>
		<br />
		<br />
<form method="post" autocomplete="off" action="http://50.57.192.210:8080/BBRepository/abcCapital">
<%
	
		String name = null;
		String value = null;
		Enumeration en = request.getParameterNames();

		while(en.hasMoreElements()){
		  name = (String)en.nextElement();
		  value = request.getParameter(name);
		  
		  %>
		  <p><span style="font-family:arial;color:red;font-size:16px;font:bold"><%=name%>: </span><%=value%></p>
		  <%
				
		}
                				
 %>
 </form>
</div>
</body>
</html>
