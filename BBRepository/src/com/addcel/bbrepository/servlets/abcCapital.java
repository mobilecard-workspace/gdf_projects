package com.addcel.bbrepository.servlets;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class abcCapital extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3797588603699893718L;
	private static Logger log = Logger.getLogger(abcCapital.class);
	public abcCapital(){
		super();
	}
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String EM_Response = null, EM_Total = null, EM_OrderID = null, 
				   EM_Merchant = null, EM_Store = null, EM_Term = null,
				   EM_RefNum = null, EM_Auth = null, EM_Digest = null, 
				   email = null, cc_numberback = null, cc_nameback = null;

			log.info("Dentro del servicio: /abcCapital");

			Map<String, String[]> parameters = request.getParameterMap();

		    for(String key : parameters.keySet()) {
		        log.info(key);
		        String[] vals = parameters.get(key);
		        if(key.equals("EM_Response"))
		        	EM_Response = vals[0];
		        else if(key.equals("EM_Total"))
		        	EM_Total = vals[0];
		        else if(key.equals("EM_OrderID"))
		        	EM_OrderID = vals[0];
		        else if(key.equals("EM_Merchant"))
		        	EM_Merchant = vals[0];
		        else if(key.equals("EM_Store"))
		        	EM_Store = vals[0];
		        else if(key.equals("EM_Term"))
		        	EM_Term = vals[0];
		        else if(key.equals("EM_RefNum"))
		        	EM_RefNum = vals[0];
		        else if(key.equals("EM_Auth"))
		        	EM_Auth = vals[0];
		        else if(key.equals("EM_Digest"))
		        	EM_Digest = vals[0];
		        else if(key.equals("email"))
		        	email = vals[0];
		        else if(key.equals("cc_number"))
		        	cc_numberback = vals[0];
		        else if(key.equals("cc_name"))
		        	cc_nameback = vals[0];
		        
		        for(String val : vals)
		        	log.info(" -> " + val);
		    }
		    
		    if(EM_Response==null){//Invocacion directa
		    	Random  rnd = new Random();
		    	Integer orderId = new Integer((int)(rnd.nextDouble() * 90000));    	

		        String varTotal= "100";
		        String varMerchant = "7152659";
		        String varOrderId = orderId.toString();
		    	String varStore = "1234";
		    	String varTerm = "0";
		    	String varCurrency = "480";
		    	String varAddress = "PROSA";
		    	String digest = digestEnvioProsa(varMerchant, varStore, varTerm, varTotal, varCurrency, varOrderId);
		    	
		    	request.setAttribute("total", varTotal);
		    	request.setAttribute("currency", varCurrency);
		    	request.setAttribute("address", varAddress);
		    	request.setAttribute("order_id", varOrderId);
		    	request.setAttribute("merchant", varMerchant);
		    	request.setAttribute("store", varStore);
		    	request.setAttribute("term", varTerm);
		    	request.setAttribute("digest", digest);
		    	request.setAttribute("action", "http://50.57.192.210:8080/BBRepository/pagina-prosa.jsp");
		    	request.setAttribute("urlBack", "http://50.57.192.210:8080/BBRepository/abcCapital");
		    	
		    	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/comerciofin.jsp");
		    	dispatcher.forward(request, response);
		    } else {
		    	log.info("De vuelta de prosa:");
		    	request.setAttribute("EM_Response", EM_Response);
		    	request.setAttribute("EM_Total", EM_Total);
		    	request.setAttribute("EM_OrderID", EM_OrderID);
		    	request.setAttribute("EM_Merchant", EM_Merchant);
		    	request.setAttribute("EM_Store", EM_Store);
		    	request.setAttribute("EM_Term", EM_Term);
		    	request.setAttribute("EM_RefNum", EM_RefNum);
		    	request.setAttribute("EM_Auth", EM_Auth);
		    	request.setAttribute("EM_Digest", EM_Digest);
		    	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/fin-prosa.jsp");
		    	dispatcher.forward(request, response);
		    }

	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	public String digestEnvioProsa(String varMerchant,String varStore,String varTerm,String varTotal,String varCurrency,String varOrderId){
		return digest(varMerchant+varStore+varTerm+varTotal+varCurrency+varOrderId);		
	}
	private String digest(String text){
		log.info("Cadena antes de digest: "+ text);
		String digest="";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes(),0,text.length());
			byte[] sha1=md.digest();
			digest = convertToHex(sha1);
		} catch (NoSuchAlgorithmException e) {
			log.info("Error al encriptar - digest", e);
		}
		return digest;
	}
	private String convertToHex(byte[] data) { 
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));    
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    }
}
