package com.addcel.gdf.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class UtilDate {

	
	public static boolean isAdulto(String rfc){
		
		//String rfc = "sora000927";
		//int numChar;
		int index = -1;
		char caracter;
		do {
			index++;
			caracter = rfc.charAt(index);

		} while(!Character.isDigit(caracter));
		
		String sYear = rfc.substring(index, index+2);
		String sMonth = rfc.substring(index+2, index+4);
		String sDay = rfc.substring(index+4, index+6);
		
		int iYear = Integer.parseInt(sYear);
		int iMonth = Integer.parseInt(sMonth);
		int iDay = Integer.parseInt(sDay);
		
		Calendar calendar = Calendar.getInstance();

		long longFechaActual = calendar.getTime().getTime();
		
		int calYear = calendar.get(Calendar.YEAR) - 2000;

		if (iYear <= calYear ){
			
			iYear = iYear + 2000;
		} else {
			
			iYear = iYear + 1900;
		}
		
		//long dieciocho = 31558149760l * 18;
		
		long dieciocho = 568046695680l - 86400000;
		
		calendar.set(Calendar.YEAR, iYear);
		calendar.set(Calendar.MONTH, iMonth - 1);
		calendar.set(Calendar.DAY_OF_MONTH, iDay);
		
		long longFecha = calendar.getTime().getTime();
		
		long longPeriodo = longFechaActual - longFecha;
		
		if (longPeriodo >= dieciocho){
			
			return true;
		} else {
			return false;
		}
	}
	
	
	public static String[] getAniosModelo() {
		  Calendar c = Calendar.getInstance();
		  int anio = c.get(Calendar.YEAR) + 1;

		  Vector vector = new Vector();

		  for (int index = anio; index >= 1920; index--) {
			  vector.addElement(String.valueOf(index));
		  }
		  
		  String[] anios = new String[vector.size()];
		  
		  vector.copyInto(anios);
		  
		  return anios;
	}
	
	
	
	public static String getDD_MM_AAAAtoDDMMAAAA(String date){
		
		/*
				DD/MM/AAAA a DDMMAAAA 
		*/
		
		String day = date.substring(0, 2);
		String month = date.substring(3, 5);
		String year = date.substring(6, 10);
		
		
		return day + month + year;
	}
	

	public static String getAAAAMMDDtoDD_MM_AAAA(String date){
		
		/*
				AAAAMMDD a DD/MM/AAAA 
		*/
		String year = date.substring(0, 4);
		String month = date.substring(4, 6);
		String day = date.substring(6, 8);
		
		return day + "/" + month + "/" + year;
	}
	
	public static String convertAAAA_MM_DDtoDD_MM_AAAA(String date){

		/*
			YYYY-MM-DD a DD/MM/YYYY 
		*/
		
		String year = date.substring(0, 4);
		String month = date.substring(5, 7);
		String day = date.substring(8, 10);
		
		return day + "/" + month + "/" + year;
	}
	
	
	public static String getDateToDDMMAAAA(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int iYear = calendar.get(Calendar.YEAR);
		int iMonth = calendar.get(Calendar.MONTH);
		int iDay = calendar.get(Calendar.DAY_OF_MONTH);

		String sYear = Integer.toString(iYear);
		String sMonth = Integer.toString(iMonth + 1);
		String sDay = Integer.toString(iDay);

		sMonth = (sMonth.length() == 1) ? "0" + sMonth : sMonth;
		sDay = (sDay.length() == 1) ? "0" + sDay : sDay;
		
		return sDay + sMonth + sYear;
	}
	
	
	public static String getDateToDD_MM_AAAA(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int iYear = calendar.get(Calendar.YEAR);
		int iMonth = calendar.get(Calendar.MONTH);
		int iDay = calendar.get(Calendar.DAY_OF_MONTH);

		String sYear = Integer.toString(iYear);
		String sMonth = Integer.toString(iMonth + 1);
		String sDay = Integer.toString(iDay);

		sMonth = (sMonth.length() == 1) ? "0" + sMonth : sMonth;
		sDay = (sDay.length() == 1) ? "0" + sDay : sDay;
		
		
		StringBuffer value = new StringBuffer();
		
		value.append(sDay).append("/").append(sMonth).append("/").append(sYear);
		
		return value.toString();
	}
	
	
	
	public static boolean after(Date depart, Date arrive){

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(depart);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		Date a = calendar.getTime();
		
		calendar.setTime(arrive);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date b = calendar.getTime();
		
		long la = a.getTime();
		long lb = b.getTime();
		
		boolean value = (la <= lb); 
		
		return value;
	}
	
	
	public static String[] getYears(int value){
		
		String[] years = null;
		Vector vector = new Vector();
		
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(new Date());
		
		int year = calendar.get(calendar.YEAR);
		
		if (value < 0){
			for (int index = year; index > (year + value); index--){
				
				vector.addElement(String.valueOf(index));
			}
		} else {
			for (int index = year; index < (year + value); index++){
				
				vector.addElement(String.valueOf(index));
			}
		}

		years = new String[vector.size()];
		vector.copyInto(years);
		return years;
	}
	
	
	public static String getYYYY_MM_DDFromLong(long longDate){
		
		/*
			YYYY-MM-DD from long
		*/
		
		StringBuffer value = new StringBuffer();
		String dash = "-";
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(new Date(longDate));
		
		int year = calendar.get(calendar.YEAR);
		int month = calendar.get(calendar.MONTH) + 1;
		int day = calendar.get(calendar.DATE);
		
		String strYear = String.valueOf(year);
		String strMonth = String.valueOf(month);
		String strDay = String.valueOf(day);

		
		
		
		if (month < 10) {
			strMonth = "0" + strMonth;
		}

		if (day < 10) {
			strDay = "0" + strDay;
		}

		
		value.append(strYear).append(dash).append(strMonth).append(dash).append(strDay); 
		
		
		return value.toString();
	}
	
}
