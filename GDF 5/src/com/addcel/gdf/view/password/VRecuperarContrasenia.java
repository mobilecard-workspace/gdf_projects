package com.addcel.gdf.view.password;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPUpdatePass;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedSizeButton;

public class VRecuperarContrasenia extends CustomMainScreen implements FieldChangeListener, Viewable {

	private CustomSelectedSizeButton btnAceptar;
	private ColorEditField editNombreUsuario;
	
	public VRecuperarContrasenia() {
		
		super("Recuperación de contraseñas", true);

		editNombreUsuario = new ColorEditField("", "", 16, EditField.CONSUME_INPUT);

		btnAceptar = new CustomSelectedSizeButton("Recuperar contraseña", 1);
		btnAceptar.setChangeListener(this);

		add(editNombreUsuario);
		add(btnAceptar);
	}

	
	public void fieldChanged(Field field, int context) {

		if (field == btnAceptar){
			
			String nombreUsuario = editNombreUsuario.getText();
			
			if (nombreUsuario.length() >= 8){

				try {

					JSONObject jsonObject = new JSONObject();

					jsonObject.put("cadena", nombreUsuario);
					String data = AddcelCrypto.encryptHard(jsonObject.toString());

					GenericHTTPUpdatePass genericHTTP = new GenericHTTPUpdatePass(this, Url.URL_RECOVERY_PASSWORD, "json=" + data);
					genericHTTP.run();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			

			} else {
				Dialog.alert("Verifica el nombre de usuario.");
			}	
		}
	}
	
	
	public void setData(int request, JSONObject jsObject) {
		
		System.out.println(jsObject.toString());

		String mensaje = jsObject.optString("mensaje");

		Dialog.alert(mensaje);
	}

	
	public void sendMessage(String message) {
		
		Dialog.alert(message);
	}
}
