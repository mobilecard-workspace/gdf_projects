package com.addcel.gdf.view.password;

import com.addcel.gdf.model.connection.addcel.implementation.HttpListener;
import com.addcel.gdf.utils.UtilSecurity;
import com.addcel.gdf.utils.Utils;
import com.addcel.gdf.view.base.Viewable;


public class UpdateUserPassThread extends HttpListener{

	private String password;
	
	public UpdateUserPassThread(Viewable viewable, String url, String post, String password){

		super(viewable, url, post);
		
		this.password = password;
	}


	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		sTemp = new String(response, 0, response.length);
		sTemp = UtilSecurity.aesDecrypt(Utils.parsePass(password), sTemp);
	}
}

