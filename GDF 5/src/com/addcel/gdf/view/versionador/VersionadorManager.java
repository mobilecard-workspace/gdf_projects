package com.addcel.gdf.view.versionador;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class VersionadorManager extends HorizontalFieldManager {

	public VersionadorManager(long style) {
		super(style);
	}

	public int getPreferredWidth() {
		// return Display.getVerticalResolution();
		return Display.getWidth() / 2;
	}
}
