package com.addcel.gdf.view;

import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;

import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;

public class VVersionador extends CustomMainScreen {

	public FontFamily alphaSansFamily = null;
	
	public VVersionador(String title, final String url) {
		
		super(title, true);
		
		try {
			alphaSansFamily = FontFamily.forName("BBAlpha Serif");
			LabelField importante = new LabelField("�IMPORTANTE!");
			
			importante.setFont(alphaSansFamily.getFont(Font.PLAIN, 10, Ui.UNITS_pt));
			add(importante);
			
			LabelField nueva = new LabelField("Hay una nueva versi�n de Tesoreria GDF disponible.");
			nueva.setFont(alphaSansFamily.getFont(Font.PLAIN, 7, Ui.UNITS_pt));
			add(nueva);
			
			SlimSelectedButtonField descarga = new SlimSelectedButtonField("Descargar", ButtonField.CONSUME_CLICK){
				
				protected boolean navigationClick(int status, int time) {
					BrowserSession browserSession; 
	    			browserSession = Browser.getDefaultSession();
	    			
	    			//browserSession.displayPage("http://www.blackberry.com/developers/docs/6.0.0api/");
	    			browserSession.displayPage(url);
					return true;
				}
			};
			
			add(descarga);
			
			LabelField aclaracion = new LabelField(
					"Para dudas o aclaraciones contacta al tel�fono 55403124 Ext. 113 en la Cd. " +
					"de M�xico y �rea metropolitana.");

			aclaracion.setFont(alphaSansFamily.getFont(Font.PLAIN, 5, Ui.UNITS_pt));
			
			add(aclaracion);
			
		} catch (ClassNotFoundException e) {
			Dialog.alert("El sistema no pudo configurarse para ejecutarse.");
			e.printStackTrace();
		}
		

		
	}
}


/*
 
 
 
 			btnDescargar =  new GenericImageButtonField(MainClass.BTN_DESCARGAR_OVER,MainClass.BTN_DESCARGAR,"descargar"){
			
			protected boolean navigationClick(int status, int time) {
				BrowserSession browserSession; 
    			browserSession = Browser.getDefaultSession();
    			
    			browserSession.displayPage(tipo);
				return true;
			}
			
		};
 
 
 
 
 
 
 
 
 
 
 
 
 
 String version = getVersionName(getApplicationContext(), InicioActivity.class);
        
        try {
         String[] servVersion = jsonResponse.getString("version").split("[.]");
         String[] currVersion = version.split("[.]");
         
         boolean pasa = true;
         
         for (int i = 0; i < servVersion.length; i++) {
          if (i < currVersion.length) {
           int max = Integer.parseInt(servVersion[i]);
           int min = Integer.parseInt(currVersion[i]);
           if (max > min) {
            pasa = false;
            break;
           }
          } else {
           break;
          }
         }
         
         if (pasa) {
          startActivity(new Intent(InicioActivity.this, MenuAppActivity.class));
         } else {
[12:36:25 p.m.] Carlos Garc�a Sanoja: }
 */
