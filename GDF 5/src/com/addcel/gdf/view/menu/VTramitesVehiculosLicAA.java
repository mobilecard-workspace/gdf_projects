package com.addcel.gdf.view.menu;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ListField;
import net.rim.device.api.ui.component.ListFieldCallback;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.transacciones.VLicenciaPermanente;
import com.addcel.gdf.view.transacciones.VLicenciaTemporal;
import com.addcel.gdf.view.util.UtilColor;

public class VTramitesVehiculosLicAA extends CustomMainScreen implements ListFieldCallback {

	protected String listMembers[] = {
			"Licencia de conducir tipo \"A\" 3 a�os (expedici�n y reposici�n)",
			"Licencia de conducir tipo \"A\" permanente (reposici�n)"
	};

	public VTramitesVehiculosLicAA() {

		super("Licencia A", true, true);
		
		ListField mylist = new ListField() {

			protected boolean touchEvent(TouchEvent message) {
				switch (message.getEvent()) {
				case TouchEvent.CLICK:
					fieldChangeNotify(1);
					execute();
					break;
				case TouchEvent.UNCLICK:
					return true;
				}
				return super.touchEvent(message);
			}

			protected boolean navigationClick(int status, int time) {
				fieldChangeNotify(0);
				execute();
				return super.navigationClick(status, time);
			}

			protected boolean keyChar(char character, int status, int time) {
				if (character == Keypad.KEY_ENTER) {
					fieldChangeNotify(0);
					execute();
					return true;
				}

				return super.keyChar(character, status, time);
			}

			
			private void execute(){
				
				int index = getSelectedIndex();
				
				switch (index) {
				case 0:
					UiApplication.getUiApplication().pushScreen(new VLicenciaTemporal(DTO.STATUS_LICENCIA_TEMPORAL));
					break;
				case 1:
					UiApplication.getUiApplication().pushScreen(new VLicenciaPermanente(DTO.STATUS_LICENCIA_PERMANENTE));
					break;
				}
			}
		};

		mylist.setCallback(this);
		mylist.setSize(listMembers.length);

		add(mylist);
	}

	public void drawListRow(ListField listField, Graphics graphics, int index,
			int y, int width) {
		
		if (isFocus()){
			graphics.setColor(UtilColor.LABEL_FIELD_STRING);
		} else {
			graphics.setColor(UtilColor.LABEL_FIELD_STRING);
		}
		graphics.drawText(this.listMembers[index], 0, y, 50);
	}

	public Object get(ListField listField, int index) {
		return listMembers[index];
	}

	public int getPreferredWidth(ListField listField) {
		return 200;
	}

	public int indexOfList(ListField listField, String prefix, int start) {
		return -1;
	}
}


/*

public void drawListRow(ListField list, Graphics g, int index, int y, int w)
    {
        String text = (String)listElements.elementAt(index);
        String str1 = text.substring(0,text.indexOf("-"));
        String str2 = text.substring(text.indexOf("-")+2);
        String imageName = image[index];

   

       //Bitmap bitm =  get the bitmap  ( better to have prepared them before getting here, for performance )

     

        int xpos = LEFT_OFFSET;
        int ypos = TOP_OFFSET + y;
        int w = bitm.getWidth();
        int h = bitm.getHeight();
        int fontHeight = this.getFont().getHeight();

 

       g.drawBitmap( xpos, ypos, w, h, bitm, 0, 0 );

 

      xpos = w + SOME_OFFSET

      graphics.drawText(str1, xpos, ypos);
      ypos += fontHeight;

      graphics.drawText(str2, xpos, ypos);

      

    }


*/