package com.addcel.gdf.view.menu;

import org.json.me.JSONObject;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.custom.CustomLabelButtom;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.transacciones.VLicenciaPermanente;
import com.addcel.gdf.view.transacciones.VLicenciaTemporal;

public class VTramitesVehiculosLicA extends CustomMainScreen implements FieldChangeListener,
Viewable {

	
	private CustomLabelButtom lbTemporal;
	private CustomLabelButtom lbPermanente;
	
	
	public VTramitesVehiculosLicA() {
		
		super("Licencia \"A\"", true, true);
		
		String temporal =
		"Licencia de conducir tipo \"A\" 3 a�os (expedici�n y reposici�n)";
		
		String permanente =
		"Licencia de conducir tipo \"A\" permanente (reposici�n)";

		lbTemporal = new CustomLabelButtom(temporal, LabelField.FOCUSABLE, false);
		lbTemporal.setChangeListener(this);

		lbPermanente = new CustomLabelButtom(permanente, LabelField.FOCUSABLE, false);
		lbPermanente.setChangeListener(this);

		add(lbTemporal);
		add(lbPermanente);
	}
	

	public void setData(int request, JSONObject jsObject) {}
	

	public void sendMessage(String message) {}

	
	public void fieldChanged(Field field, int context) {

		if (field == lbTemporal){
			UiApplication.getUiApplication().pushScreen(new VLicenciaTemporal(DTO.STATUS_LICENCIA_TEMPORAL));
		} else if (field == lbPermanente){
			UiApplication.getUiApplication().pushScreen(new VLicenciaPermanente(DTO.STATUS_LICENCIA_PERMANENTE));
		}
	}
}
