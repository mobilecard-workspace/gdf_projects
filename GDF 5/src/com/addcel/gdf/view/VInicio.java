package com.addcel.gdf.view;

import java.util.Vector;

import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.Comunicado;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ComunicadosRichTextField;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomLabelButtom;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.VIndex;
import com.addcel.gdf.view.menu.VTramites;
import com.addcel.gdf.view.util.UtilColor;

public class VInicio extends CustomMainScreen implements FieldChangeListener,
		Viewable {

	private SlimSelectedButtonField consulta;
	private SlimSelectedButtonField tramites;

	private SlimSelectedButtonField anteriorGeneral;
	private SlimSelectedButtonField siguienteGeneral;
	private SlimSelectedButtonField avisoPrivacidad;
	private ComunicadosRichTextField avisosGenerales;

	private SplashScreen splashScreen;

	private Vector comunicados = new Vector();

	private CustomLabelButtom colorLabelField;
	
	public VInicio() {

		super("Tesorer�a GDF", true, true);

		splashScreen = SplashScreen.getInstance();

		Background background01 = BackgroundFactory
				.createSolidBackground(0xa3a5a8);

		consulta = new SlimSelectedButtonField("Mis pagos", ButtonField.CONSUME_CLICK);
		tramites = new SlimSelectedButtonField("Pagos en L�nea", ButtonField.CONSUME_CLICK);
		consulta.setChangeListener(this);
		tramites.setChangeListener(this);

		CustomButtonFieldManager buttonFieldManager01 = new CustomButtonFieldManager(
				tramites, consulta);
		buttonFieldManager01.add(tramites);
		buttonFieldManager01.add(consulta);

		add(buttonFieldManager01);

		add(new LabelField(""));
		LabelField field01 = new LabelField("Avisos al Contribuyente.", LabelField.USE_ALL_WIDTH) {
			protected void paint(Graphics g) {
				g.setColor(Color.WHITE);
				super.paint(g);
			}
		};
		field01.setBackground(background01);
		add(field01);

		avisosGenerales = new ComunicadosRichTextField();

		Background background = BackgroundFactory.createSolidBackground(UtilColor.EDIT_FIELD_BACKGROUND);
		avisosGenerales.setBackground(background);
		add(avisosGenerales);

		anteriorGeneral = new SlimSelectedButtonField("Anterior",
				ButtonField.CONSUME_CLICK);
		siguienteGeneral = new SlimSelectedButtonField("Siguiente",
				ButtonField.CONSUME_CLICK);

		anteriorGeneral.setChangeListener(this);
		siguienteGeneral.setChangeListener(this);

		CustomButtonFieldManager buttonFieldManagerGeneral = new CustomButtonFieldManager(
				anteriorGeneral, siguienteGeneral);
		buttonFieldManagerGeneral.add(anteriorGeneral);
		buttonFieldManagerGeneral.add(siguienteGeneral);
		add(buttonFieldManagerGeneral);

		add(new LabelField(""));

		avisoPrivacidad = new SlimSelectedButtonField("Aviso de privacidad", ButtonField.CONSUME_CLICK);
		avisoPrivacidad.setChangeListener(this);
		add(avisoPrivacidad);
		
		add(new LabelField(""));

		String footer = 
		"Conozca m�s sobre el funcionamiento de la aplicaci�n en nuestra p�gina web " +
		"http://www.finanzas.df.gob.mx/sat/tesoreriamovil/preguntasfrecuentes/";
		
		colorLabelField = new CustomLabelButtom(footer, LabelField.FOCUSABLE, true);
		
		colorLabelField.setChangeListener(this);
		
		add(colorLabelField);
		add(new LabelField(""));

		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("idAplicacion", 1);

			String post = jsonObject.toString();

			String data = AddcelCrypto.encryptHard(post);

			splashScreen.start();

			GenericHTTPHard genericHTTP = new GenericHTTPHard(data,
					Url.URL_GDF_COMUNICADOS, this);
			genericHTTP.run();

		} catch (JSONException e) {
			e.printStackTrace();
			sendMessage("Error al recopilar la informaci�n");
		}
	}

	public void fieldChanged(Field field, int context) {
			
		if (field == tramites) {
			UiApplication.getUiApplication().pushScreen(new VTramites());
		} else if (field == anteriorGeneral) {
			Comunicado comunicado = avisosGenerales.getComunicado();

			int index = comunicados.lastIndexOf(comunicado);
			int size = comunicados.size();

			if (index > 0) {
				comunicado = (Comunicado) comunicados.elementAt(index - 1);
				avisosGenerales.setComunicado(comunicado);
			}

		} else if (field == siguienteGeneral) {

			Comunicado comunicado = avisosGenerales.getComunicado();

			int index = comunicados.lastIndexOf(comunicado);
			int size = comunicados.size();

			if ((index + 1) < size) {
				comunicado = (Comunicado) comunicados.elementAt(index + 1);
				avisosGenerales.setComunicado(comunicado);
			}
		} else if (field == consulta) {
			UiApplication.getUiApplication().pushScreen(new VIndex("Consulta de pagos"));
		} else if (field == colorLabelField) {
			//UiApplication.getUiApplication().pushScreen(new VIndex("Consulta de pagos"));
			BrowserSession browserSession; 
			browserSession = Browser.getDefaultSession();   			
			browserSession.displayPage("http://www.finanzas.df.gob.mx/sat/tesoreriamovil/preguntasfrecuentes/");
		} else if (field == avisoPrivacidad){
			
			String aviso = 
					
			"Aviso de Privacidad\n\n" +
			"Los datos personales recabados ser�n protegidos, incorporados y tratados en el Sistema de Datos Personales " +
			"(En el Sistema del Impuesto sobre Nominas, en el Sistema del Impuesto Sobre Tenencia con Padr�n y en el Sistema " +
			"Integral del Impuesto Predial), los cuales tienen su fundamento en los art�culos 126, 156 y 160 del C�digo Fiscal " +
			"del Distrito Federal y en el numeral 13 de los Lineamientos para la Protecci�n de Datos Personales en el D.F., " +
			"cuya finalidad es realizar el pago de sus contribuciones, y podr�n ser transmitidos seg�n corresponda a la " +
			"Secretar�a de Hacienda y Cr�dito P�blico, al Instituto Mexicano del Seguro Social, a la Comisi�n de Derechos " +
			"Humanos del Distrito Federal, a los �rganos de Control y a los �rganos Jurisdiccionales, adem�s de otras " +
			"transmisiones previstas en la Ley de Protecci�n de Datos Personales para el Distrito Federal, lo anterior de " +
			"conformidad con lo establecido en la Ley de la materia.\n\n" +

			"Asimismo, Los datos no podr�n ser difundidos sin su consentimiento expreso, salvo las excepciones previstas " +
			"en la Ley. El responsable del sistema de datos personales es el C. Emilio Barriga Delgado, y la direcci�n " +
			"donde podr� ejercer los derechos de acceso, rectificaci�n, cancelaci�n y oposici�n, as� como la revocaci�n " +
			"del consentimiento es en Dr. Lavista No. 144, Acceso 4, Col. Doctores, Del. Cuauht�moc, de esta Ciudad. El " +
			"titular de los datos podr� dirigirse al Instituto de acceso a la Informaci�n P�blica del Distrito Federal, " +
			"donde recibir� asesor�a sobre los derechos que tutela la Ley de Protecci�n de Datos Personales para el Distrito " +
			"Federal al tel�fono: 56364636; correo electr�nico: datos.personales@infodf.org.mx o www.infodf.org.mx.";
			
			Dialog.alert(aviso);
		}
	}

	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();

		if (jsObject.has("comunicados")) {

			try {
				
				JSONArray jsonArray = jsObject.getJSONArray("comunicados");

				int length = jsonArray.length();

				for (int index = 0; index < length; index++) {

					JSONObject object = (JSONObject) jsonArray.get(index);

					Comunicado comunicado = new Comunicado();

					comunicado.setContenido(object.optString("contenido"));
					comunicado.setFgColor(object.optString("fgColor"));
					comunicado.setBgColor(object.optString("bgColor"));
					comunicado.setTamFuente(object.optString("tamFuente"));

					comunicados.addElement(comunicado);
				}

				if (comunicados.size() > 0) {
					Comunicado comunicado = (Comunicado) comunicados.elementAt(0);
					avisosGenerales.setComunicado(comunicado);
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
		}
	}

	public void sendMessage(String message) {
		splashScreen.remove();
		Dialog.alert(message);
	}
}
