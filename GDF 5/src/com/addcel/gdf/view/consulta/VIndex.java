package com.addcel.gdf.view.consulta;

import java.util.Calendar;
import java.util.Date;

import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.custom.CustomLabelButtom;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimChoiceButton;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.listas.agua.LAgua;
import com.addcel.gdf.view.consulta.listas.infracciones.LInfracciones;
import com.addcel.gdf.view.consulta.listas.licencia.LLicencia;
import com.addcel.gdf.view.consulta.listas.nomina.LNomina;
import com.addcel.gdf.view.consulta.listas.predial.LPredial;
import com.addcel.gdf.view.consulta.listas.predialvencidocons.LPredialConsulta;
import com.addcel.gdf.view.consulta.listas.tarjetaCirculacion.LTarjetaCirculacion;
import com.addcel.gdf.view.consulta.listas.tenencia.LTenencia;

public class VIndex extends CustomMainScreen implements FieldChangeListener, Viewable {

	//private ObjectChoiceField serviciosChoiceField;
	private SlimSelectedButtonField consultar = null;
	private DateField dateField;
	private CustomLabelButtom choiceButton;
	
	private int index;

	public VIndex(String title) {
		
		super(title, true);


		LabelField lbOperacion = new ColorLabelField("Seleccionar operaci�n: ", LabelField.NON_FOCUSABLE);
		
		choiceButton = new CustomLabelButtom(
				"Nomina", 
				LabelField.FOCUSABLE|LabelField.USE_ALL_WIDTH, 
				0
		);
		
		choiceButton.setChangeListener(this);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM  yyyy");
		dateField = new DateField("Seleccionar mes y a�o", System.currentTimeMillis(), dateFormat, DateField.DATE);
		
		consultar = new SlimSelectedButtonField("Consultar", ButtonField.CONSUME_CLICK);
		consultar.setChangeListener(this);

		add(lbOperacion);
		add(choiceButton);
		add(dateField);
		add(consultar);
	}
	
	
	public void setData(int request, JSONObject jsObject) {

		if (jsObject.has("consultaAgua")){
			
			if (validarArray(jsObject, "consultaAgua")){
				UiApplication.getUiApplication().pushScreen(new LAgua("Lista de pagos agua", jsObject));
			} else {
				Dialog.alert("No existe informaci�n que mostrar.");
			}
			
		} else if (jsObject.has("consultaNomina")){
			
			if (validarArray(jsObject, "consultaNomina")){
				UiApplication.getUiApplication().pushScreen(new LNomina("Lista de pagos n�mina", jsObject));
			} else {
				Dialog.alert("No existe informaci�n que mostrar.");
			}
		}  else if (jsObject.has("consultaPredial")){
			
			if (validarArray(jsObject, "consultaPredial")){
				UiApplication.getUiApplication().pushScreen(new LPredial("Lista de pagos predial", jsObject));
			} else {
				Dialog.alert("No existe informaci�n que mostrar.");
			}
		}else if (jsObject.has("consultaInfraccion")){
			
			if (validarArray(jsObject, "consultaInfraccion")){
				UiApplication.getUiApplication().pushScreen(new LInfracciones("Lista de pagos infracciones", jsObject));
			} else {
				Dialog.alert("No existe informaci�n que mostrar.");
			}
		} else if (jsObject.has("consultaTenencia")){
			
			if (validarArray(jsObject, "consultaTenencia")){
				UiApplication.getUiApplication().pushScreen(new LTenencia("Lista de pagos tenencias", jsObject));
			} else {
				Dialog.alert("No existe informaci�n que mostrar.");
			}
		} else if (jsObject.has("consultaPredialVencido")){
			
			if (validarArray(jsObject, "consultaPredialVencido")){
				UiApplication.getUiApplication().pushScreen(new LPredialConsulta("Lista de predial vencidos", jsObject));
			} else {
				Dialog.alert("No existe informaci�n que mostrar.");
			}
		} else if ((jsObject.has("consultaTarjetaCirculacion"))&&(index == 7)){
			
			if (validarArray(jsObject, "consultaTarjetaCirculacion")){
				UiApplication.getUiApplication().pushScreen(new LTarjetaCirculacion("Lista de pagos para Tarjeta de Circulaci�n", jsObject));
			} else {
				Dialog.alert("No existe informaci�n que mostrar.");
			}
			
		} else if ((jsObject.has("licenciaPagos"))&&(index == 8)){
			
			if (validarArray(jsObject, "licenciaPagos")){
				UiApplication.getUiApplication().pushScreen(new LLicencia("Lista de pagos para Licencia \"A\" 3 a�os", jsObject, false));
			} else {
				Dialog.alert("No existe informaci�n que mostrar.");
			}
		} else if ((jsObject.has("licenciaPagos"))&&(index == 9)){
			
			if (validarArray(jsObject, "licenciaPagos")){
				UiApplication.getUiApplication().pushScreen(new LLicencia("Lista de pagos para Licencia \"A\" Permanente", jsObject, true));
			} else {
				Dialog.alert("No existe informaci�n que mostrar.");
			}
		}
	}

	private boolean validarArray(JSONObject jsObject, String nameArray){
		
		JSONArray jsonArray = jsObject.optJSONArray(nameArray);
		
		boolean value = false;
		
		if (jsonArray == null ){
			
			value = false;
			
		} else if (jsonArray.length() == 0){
			
			value = false;
		} else if (jsonArray.length() > 0){
			
			value = true;
		}
		
		return value;
	}
	
	private void execute(CustomMainScreen screen, boolean value){
		
	}
	
	
	public void sendMessage(String message) {
		Dialog.alert(message);
	}

	
    public Date addDays(Date d, long days){
    	
        d.setTime(d.getTime() + days * 1000 * 60 * 60 * 24);
        return d;
    }
	
	
    
	public void fieldChanged(Field field, int context) {

		if (field == consultar){

			if (UserBean.nameLogin == null){
				UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
			} else {

				//index = serviciosChoiceField.getSelectedIndex();
				long dateEnd = dateField.getDate();

				index = choiceButton.getIndexChoice();
				
				index++;

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date(dateEnd));
				Date date = calendar.getTime();
				
				date = addDays(date, -7);
				
				long hoy = System.currentTimeMillis();
				
				boolean result = dateEnd < hoy;
				
				if (result){
					
					String dateFormatStart = UtilDate.getYYYY_MM_DDFromLong(date.getTime());
					String dateFormatEnd = UtilDate.getYYYY_MM_DDFromLong(dateEnd);
					
					JSONObject jsonObject = new JSONObject();
					
					try {

						calendar.setTime(new Date(dateEnd));

						int mes = calendar.get(Calendar.MONTH) + 1;
						int anio = calendar.get(Calendar.YEAR);

						jsonObject.put("id_usuario",  UserBean.idLogin);
						jsonObject.put("id_producto", index);
						jsonObject.put("mes", String.valueOf(mes));
						jsonObject.put("anio", String.valueOf(anio));

						String post = jsonObject.toString();
						
						String key = UtilDate.getDateToDDMMAAAA(new Date());
						
						String data = AddcelCrypto.encryptSensitive(key, post);
						final PagoPlacas pagoPlacas = new PagoPlacas(data, this, Url.URL_GDF_CONSULTA_PAGOS);
						
						UiApplication.getUiApplication().invokeAndWait(new Runnable() { 
							
							public void run() { 
								
								pagoPlacas.run();
							}
						});
						
					} catch (JSONException e) {
						Dialog.alert("Error al interpretar los datos.");
						e.printStackTrace();
					}

				} else {
					
					Dialog.alert("Verificar la fecha");
				}
			}
		} else if (field == choiceButton){
			
			if (context == 0){
				UiApplication.getUiApplication().pushScreen(new VListaServicios(choiceButton));
			}
		}
	}
}