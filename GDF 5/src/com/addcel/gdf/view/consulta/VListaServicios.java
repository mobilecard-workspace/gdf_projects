package com.addcel.gdf.view.consulta;

import org.json.me.JSONObject;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;

import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.custom.CustomLabelButtom;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimChoiceButton;

public class VListaServicios extends CustomMainScreen implements FieldChangeListener, Viewable {

	private final String nomina = "Nomina";
	private final String tenencia = "Tenencia";
	private final String infraccion = "Infracciones";
	private final String predial = "Pago de predial";
	private final String pagoAgua = "Pago de agua";
	private final String pagoAguaVencido = "Pago de predial vencido";

	private final String tarjetaCirculacion = "Tarjeta de circulaci�n";
	private final String licenciaTemporal = "Licencia de conducir Tipo \"A\" 3 a�os (Expedici�n y reposici�n)";
	private final String licenciaPermanente =  "Licencia de conducir Tipo \"A\" Permanente (Reposici�n)";
	
	private CustomLabelButtom choiceButton;
	
	public VListaServicios(CustomLabelButtom choiceButton) {
		
		super("Lista de servicios", true);
		
		this.choiceButton = choiceButton;
		
		String[] servicios = { nomina, tenencia, infraccion, predial, 
				pagoAgua, pagoAguaVencido, tarjetaCirculacion, 
				licenciaTemporal, licenciaPermanente};
		
		int length = servicios.length;
		
		for(int index = 0; index < length; index++){
			
			String title = servicios[index];
			CustomLabelButtom colorLabelField = new CustomLabelButtom(
					title, 
					LabelField.FOCUSABLE|LabelField.USE_ALL_WIDTH, 
					index
			);
			colorLabelField.setChangeListener(this);
			add(colorLabelField);
		}
	}

	public void setData(int request, JSONObject jsObject) {

		
	}

	public void sendMessage(String message) {

		
	}

	public void fieldChanged(Field field, int context) {

		if (field instanceof CustomLabelButtom){
			
			CustomLabelButtom labelButtom = (CustomLabelButtom)field;
			
			choiceButton.setText(labelButtom.getText());
			
			choiceButton.setIndexChoice(labelButtom.getIndexChoice());
			
			UiApplication.getUiApplication().popScreen(this);
		}
		
	}

}
