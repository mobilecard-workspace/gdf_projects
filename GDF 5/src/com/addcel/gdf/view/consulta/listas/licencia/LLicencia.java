package com.addcel.gdf.view.consulta.listas.licencia;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.consulta.listas.licencia.dto.Licencia;

public class LLicencia extends CustomMainScreen {

	public LLicencia(String title, JSONObject jsObject, boolean isPermanente) {
		super(title, true);
		
		try {
			JSONArray jsonArray = jsObject.getJSONArray("licenciaPagos");
			
			
			int length = jsonArray.length();
			
			for(int index = 0; index < length; index++){
				
				JSONObject jsonObject = (JSONObject) jsonArray.get(index);

				Licencia licencia = new Licencia();
				
				licencia.setPermanente(isPermanente);
				licencia.setRfc(jsonObject.optString("rfc"));
				licencia.setImporteFinal(jsonObject.optString("importeFinal"));
				licencia.setLinea_captura(jsonObject.optString("linea_captura"));
				licencia.setTotalPago(jsonObject.optString("totalPago"));

				licencia.setId_bitacora(jsonObject.optInt("id_bitacora"));
				//"id_bitacora":150676
				
				CustomGridFieldLicencia fieldManager = new CustomGridFieldLicencia(licencia);

				add(fieldManager);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
