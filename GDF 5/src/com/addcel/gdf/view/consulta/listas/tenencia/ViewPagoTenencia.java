package com.addcel.gdf.view.consulta.listas.tenencia;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedButtonField;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.listas.common.Adapter;
import com.addcel.gdf.view.consulta.listas.tenencia.dto.ListaTenencia;
import com.addcel.gdf.view.util.UtilNumber;


public class ViewPagoTenencia extends CustomMainScreen implements FieldChangeListener, Viewable {


	private HighlightsLabelField editTipoServicio;
	private HighlightsLabelField editLC;
	private HighlightsLabelField editVigencia;

	private HighlightsLabelField editTenencia;
	private HighlightsLabelField editTenActualizacion;
	private HighlightsLabelField editTenRecargo;
	private HighlightsLabelField editDerecho;
	private HighlightsLabelField editDerRecargo;
	private HighlightsLabelField editDerActualizacion;
	private HighlightsLabelField editTotalDerecho;
	private HighlightsLabelField editTotal;
	private HighlightsLabelField editLineacaptura;
	
	private LabelField textTipoServicio;
	private LabelField textLC;
	private LabelField textVigencia;
	
	private LabelField textTenencia;
	private LabelField textTenActualizacion;
	private LabelField textTenRecargo;
	private LabelField textDerecho;
	private LabelField textDerRecargo;
	private LabelField textDerActualizacion;
	private LabelField textTotalDerecho;
	private LabelField textTotal;
	private LabelField textLineacaptura;

	private CustomSelectedButtonField consultar = null;
	private SlimSelectedButtonField salir = null;
	
	private ListaTenencia listaTenencia;
	private SplashScreen splashScreen;
	
	
	public ViewPagoTenencia(String title, Adapter adapter) {
		
		super(title, true);
		
		this.listaTenencia = (ListaTenencia)adapter;
		
		splashScreen = SplashScreen.getInstance();
		addComponents(listaTenencia);
	}
	
	
	private void addComponents(ListaTenencia listaTenencia){

		
		textTipoServicio = new ColorLabelField("Servicio: ", LabelField.NON_FOCUSABLE);
		textLC = new ColorLabelField("LC: ", LabelField.NON_FOCUSABLE);
		textVigencia = new ColorLabelField("Vigencia: ", LabelField.NON_FOCUSABLE);

		textTenencia = new ColorLabelField("Tenencia: ", LabelField.NON_FOCUSABLE);		
		textTenActualizacion = new ColorLabelField("Act. Tenencia: ", LabelField.NON_FOCUSABLE);
		textTenRecargo = new ColorLabelField("Rec. Tenencia: ", LabelField.NON_FOCUSABLE);
		
		textDerecho = new ColorLabelField("Derechos: ", LabelField.NON_FOCUSABLE);
		textDerActualizacion = new ColorLabelField("Act. Derechos: ", LabelField.NON_FOCUSABLE);
		textDerRecargo = new ColorLabelField("Rec. Derechos: ", LabelField.NON_FOCUSABLE);
		textTotalDerecho = new ColorLabelField("Total Derechos: ", LabelField.NON_FOCUSABLE);
		
		textTotal = new ColorLabelField("Total a pagar: ", LabelField.NON_FOCUSABLE);
		textLineacaptura = new ColorLabelField("L�nea de Captura: ", LabelField.NON_FOCUSABLE);

		editTipoServicio = new HighlightsLabelField("", DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		editLC = new HighlightsLabelField("", DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		editVigencia = new HighlightsLabelField("", DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		
		editTenencia = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTenActualizacion = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTenRecargo = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDerecho = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDerRecargo = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDerActualizacion = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTotalDerecho = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editLineacaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		
		consultar = new CustomSelectedButtonField("Reenviar recibo", ButtonField.CONSUME_CLICK);
		consultar.setChangeListener(this);
		
		salir =  new SlimSelectedButtonField("Cancelar", ButtonField.CONSUME_CLICK);
		salir.setChangeListener(this);

		add(new NullField());
		
		addHorizontalFiels(textTipoServicio, editTipoServicio);
		addHorizontalFiels(textLC, editLC);
		addHorizontalFiels(textVigencia, editVigencia);

		add(new LabelField(""));

		addHorizontalFiels(textTenencia, editTenencia);
		addHorizontalFiels(textTenActualizacion, editTenActualizacion);
		addHorizontalFiels(textTenRecargo, editTenRecargo);
		addHorizontalFiels(textDerecho, editDerecho);
		addHorizontalFiels(textDerRecargo, editDerRecargo);
		addHorizontalFiels(textDerActualizacion, editDerActualizacion);
		addHorizontalFiels(textTotal, editTotal);

		add(consultar);
		add(salir);
		
		String tenencia = UtilNumber.formatCurrency(listaTenencia.getTenencia());
		String tenRecargo = UtilNumber.formatCurrency(listaTenencia.getTenRecargo());
		String derecho = UtilNumber.formatCurrency(listaTenencia.getDerecho());
		String derRecargo = UtilNumber.formatCurrency(listaTenencia.getRecargos());
		String totalDerecho = UtilNumber.formatCurrency(listaTenencia.getTotal()); //"totalDerecho"
		String total = UtilNumber.formatCurrency(listaTenencia.getTotalPago());
		String lineacaptura = listaTenencia.getLinea_captura();
		String tipoServicio = listaTenencia.getTipoServicio();
		String vigencia2 = listaTenencia.getVigencia();
		
		String tenActualizacion = UtilNumber.formatCurrency(listaTenencia.getTenActualizacion());
		String derActualizacion = UtilNumber.formatCurrency(listaTenencia.getDerActualizacion());

		editLC.setText(lineacaptura);
		editVigencia.setText(vigencia2);
		editTipoServicio.setText(tipoServicio);
		
		editTenencia.setText(tenencia);
		editTenActualizacion.setText(tenActualizacion);
		editTenRecargo.setText(tenRecargo);
		editDerecho.setText(derecho);
		editDerRecargo.setText(derRecargo);
		editDerActualizacion.setText(derActualizacion);
		editTotalDerecho.setText(totalDerecho);
		editTotal.setText(total);
		editLineacaptura.setText(lineacaptura);
		
	}


	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}

/*
	public void fieldChanged(Field field, int context) {

		if(field == consultar){

			JSONObject jsonObject = new JSONObject();

			try {

				jsonObject.put("id_bitacora", listaTenencia.getId_bitacora());
				jsonObject.put("id_producto", DTO.STATUS_TENENCIA);
				jsonObject.put("id_usuario", UserBean.idLogin);
				String post = jsonObject.toString();

				String data = AddcelCrypto.encryptHard(post);
				splashScreen.start();
				GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_GDF_REENVIO, this);
				genericHTTP.run();
				
			} catch (JSONException e) {
				e.printStackTrace();
				sendMessage("Error al recopilar la informacion");
			}
		}
	}
*/

	public void fieldChanged(Field field, int context) {

		if(field == consultar){
			
			BasicEditField fieldMail = new BasicEditField(BasicEditField.FILTER_EMAIL);
			 
			Dialog d = new Dialog("�Desea enviar la informaci�n a otra direcci�n de mail?, escribala:", new String[] {"Enviar a otra direcci�n", "Enviar", "Cancelar"}, new int[] {2,1,0}, 2, Bitmap.getPredefinedBitmap(Bitmap.QUESTION));
			d.add(fieldMail);
			 
			int result = d.doModal();
			 
			 if (result == 2) {
			 
				 sendComprobante(true, fieldMail.getText());

			} else if (result == 1) {
				 
				sendComprobante(false, fieldMail.getText());
			}
		} else if(field == salir){
			
			UiApplication.getUiApplication().popScreen(this);
		}
	}
	
	
	
	private void sendComprobante(boolean value, String email){
		
		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("id_bitacora", listaTenencia.getId_bitacora());
			jsonObject.put("id_producto", DTO.STATUS_TENENCIA);
			jsonObject.put("id_usuario", UserBean.idLogin);

			if (value){
				
				jsonObject.put("email", email);
			}
			
			String post = jsonObject.toString();

			String data = AddcelCrypto.encryptHard(post);

			splashScreen.start();
			
			GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_GDF_REENVIO, this);
			genericHTTP.run();
			
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessage("Error al recopilar la informacion");
		}
	}
	
	
	public void setData(int request, JSONObject jsObject){

		splashScreen.remove();
		
		if (jsObject.has("error")){
			
			String mensaje = jsObject.optString("error");
			 
		    Dialog dialog = new Dialog(Dialog.D_OK, mensaje, Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		 
		    int i = dialog.doModal();
		 
		    if (i == Dialog.OK) {
		        UiApplication.getUiApplication().popScreen(this);
		    } 
		}
	}
	
/*
	public void setData(int request, JSONObject jsObject){

		splashScreen.remove();
		
		if (jsObject.has("error")){
			
			String mensaje = jsObject.optString("error");
			sendMessage(mensaje);
		}
	}
*/

	public void sendMessage(String message) {
		splashScreen.remove();
		Dialog.alert(message);
	}
}

