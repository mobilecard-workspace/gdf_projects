package com.addcel.gdf.view.consulta.listas.common;

public interface Adapter {
/*
	private String fechaPago;
	private String linea_captura;
	private String id_producto;
	private String totalPago;
*/
	
	public String getFechaPago();
	public void setFechaPago(String fechaPago);
	
	public String getLinea_captura();
	public void setLinea_captura(String linea_captura);
	
	public String getId_producto();
	public void setId_producto(String id_producto);
	
	public String getTotalPago();
	public void setTotalPago(String totalPago);
}
