package com.addcel.gdf.view.consulta.listas.tarjetaCirculacion;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.listas.tarjetaCirculacion.dto.TarjetaCirculacion;

public class ViewPagoCirculacion extends CustomMainScreen implements FieldChangeListener, Viewable {
	
	private ColorLabelField labelDataPlaca;
	private ColorLabelField labelDataDerechos;
	private ColorLabelField labelDataLineaCaptura;
	private ColorLabelField labelDataTotal;
	
	private HighlightsLabelField editDataPlaca;
	private HighlightsLabelField editDataDerechos;
	private HighlightsLabelField editDataLineaCaptura;
	private HighlightsLabelField editDataTotal;
	
	private SlimSelectedButtonField consultar = null;
	private SlimSelectedButtonField salir = null;
	private SplashScreen splashScreen;
	private TarjetaCirculacion tarjetaCirculacion;

	public ViewPagoCirculacion(String title, TarjetaCirculacion tarjetaCirculacion) {

		super(title, true);

		this.tarjetaCirculacion = tarjetaCirculacion;

		splashScreen = SplashScreen.getInstance();

		labelDataPlaca = new ColorLabelField("Placa: ", LabelField.NON_FOCUSABLE);
		labelDataDerechos = new ColorLabelField("Derechos: ", LabelField.NON_FOCUSABLE);
		labelDataLineaCaptura = new ColorLabelField("Linea Captura: ", LabelField.NON_FOCUSABLE);
		labelDataTotal = new ColorLabelField("Total: ", LabelField.NON_FOCUSABLE);
		
		editDataPlaca = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDataDerechos = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDataLineaCaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDataTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);

		consultar = new SlimSelectedButtonField("Reenviar recibo", ButtonField.CONSUME_CLICK);
		consultar.setChangeListener(this);
		salir =  new SlimSelectedButtonField("Cancelar", ButtonField.CONSUME_CLICK);
		salir.setChangeListener(this);
		
		add(new NullField());

		add(new LabelField(""));
		addHorizontalFiels(labelDataPlaca, editDataPlaca);
		addHorizontalFiels(labelDataDerechos, editDataDerechos);
		addHorizontalFiels(labelDataLineaCaptura, editDataLineaCaptura);
		addHorizontalFiels(labelDataTotal, editDataTotal);
		add(new LabelField(""));
		
		
		CustomButtonFieldManager buttonFieldManager01 = new CustomButtonFieldManager(
				consultar, salir);
		buttonFieldManager01.add(consultar);
		buttonFieldManager01.add(salir);

		add(buttonFieldManager01);

		editDataPlaca.setText(tarjetaCirculacion.getPlaca());
		editDataDerechos.setText(tarjetaCirculacion.getImporteFinal());
		editDataLineaCaptura.setText(tarjetaCirculacion.getLinea_captura());
		editDataTotal.setText(tarjetaCirculacion.getTotalPago());
	}

	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}


	public void fieldChanged(Field field, int context) {

		if(field == consultar){
			
			BasicEditField fieldMail = new BasicEditField(BasicEditField.FILTER_EMAIL);
			 
			Dialog d = new Dialog("�Desea enviar la informaci�n a otra direcci�n de mail?, escribala:", new String[] {"Enviar a otra direcci�n", "Enviar", "Cancelar"}, new int[] {2,1,0}, 2, Bitmap.getPredefinedBitmap(Bitmap.QUESTION));
			d.add(fieldMail);
			 
			int result = d.doModal();
			 
			 if (result == 2) {
			 
				 sendComprobante(true, fieldMail.getText());

			} else if (result == 1) {
				 
				sendComprobante(false, fieldMail.getText());
			}
		} else if(field == salir){
			
			UiApplication.getUiApplication().popScreen(this);
		}
	}
	
	
	private void sendComprobante(boolean value, String email){
		
		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("id_bitacora", tarjetaCirculacion.getId_bitacora());
			
			jsonObject.put("id_producto", DTO.STATUS_TARJETA_CIRCULACION);

			jsonObject.put("id_usuario", UserBean.idLogin);

			if (value){
				
				jsonObject.put("email", email);
			}
			
			String post = jsonObject.toString();

			String data = AddcelCrypto.encryptHard(post);

			splashScreen.start();
			
			GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_GDF_REENVIO, this);
			genericHTTP.run();
			
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessage("Error al recopilar la informacion");
		}
	}
	

	public void setData(int request, JSONObject jsObject){

		splashScreen.remove();
		
		if (jsObject.has("error")){
			
			String mensaje = jsObject.optString("error");
			 
		    Dialog dialog = new Dialog(Dialog.D_OK, mensaje, Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		 
		    int i = dialog.doModal();
		 
		    if (i == Dialog.OK) {
		        UiApplication.getUiApplication().popScreen(this);
		    } 
		}
	}
	
	
	public void sendMessage(String message) {
		splashScreen.remove();
		Dialog.alert(message);
	}

}