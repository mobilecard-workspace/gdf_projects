package com.addcel.gdf.view.consulta.listas.common;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

import com.addcel.gdf.view.consulta.listas.predialvencido.dto.PredialVencidoTotal;
import com.addcel.gdf.view.util.UtilColor;

public class ObjectRichTextFieldTotal extends RichTextField {

	final public static int IMPUESTO_EMITIDO = 0;
	final public static int IMPUESTO_PAGADO = 1;
	final public static int ACTUALIZACION = 2;
	final public static int GASTOS_EJECUCION = 3;
	final public static int GASTOS_EJECUCION_CONDONADOS = 4;
	final public static int RECARGOS = 5;
	final public static int MULTA = 6;
	final public static int MULTA_CONDONADA = 7;
	final public static int CAPTURA = 8;
	final public static int TOTAL = 9;
	final public static int CAPTURA_TEXT = 10;
	
	final private String impuestoEmitido = "Impuesto emitido: ";
	final private String impuestoPagado = "Impuesto pagado: ";
	final private String actualizacion = "Actualizacion: ";
	final private String gastosEjecucion = "Gastos Ejecucion: ";
	final private String gastosEjecucionCondonados = "Gastos de Ejecucion Condonados: ";
	final private String recargos = "Recargos: ";
	final private String multa = "Multa: ";
	final private String multaCondonada = "Multa Condonada: ";
	final private String captura = "Captura: ";
	final private String total = "Total: ";
	
	private String title = "";
	private String data = "";
	
	private int format;
	private int size;
	private int type;
	private int alignment;


	public ObjectRichTextFieldTotal(PredialVencidoTotal adapter, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		format = Display.getWidth() / 2; 
		size = Display.getWidth();
		this.type = type;
		
		switch (type) {
		case ObjectRichTextFieldTotal.IMPUESTO_EMITIDO:
			title = impuestoEmitido;
			data = adapter.getImpuestoEmitido();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.IMPUESTO_PAGADO:
			title = impuestoPagado;
			data = adapter.getImpuestoPagado();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.ACTUALIZACION:
			title = actualizacion;
			data = adapter.getActualizacion();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.GASTOS_EJECUCION:
			title = gastosEjecucion;
			data = adapter.getGastosEjecucion();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.GASTOS_EJECUCION_CONDONADOS:
			title = gastosEjecucionCondonados;
			data = adapter.getGastosEjecucionCondonados();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.RECARGOS:
			title = recargos;
			data = adapter.getRecargos();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.MULTA:
			title = multa;
			data = adapter.getMulta();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.MULTA_CONDONADA:
			title = multaCondonada;
			data = adapter.getMultaCondonada();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.CAPTURA:
			title = captura;
			data = adapter.getCaptura();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.TOTAL:
			title = total;
			data = adapter.getTotal();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.CAPTURA_TEXT:
			title = captura;
			data = adapter.getCaptura();
			alignment = DrawStyle.LEFT;
			break;
		}
	}

	public void paint(Graphics graphics) {

		switch (type) {
		case CAPTURA:
			//graphics.setColor(UtilColor.LIST_TITLE);
			//graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
			graphics.setColor(UtilColor.LIST_DATA);
			graphics.drawText(data, 0, 0, alignment, size);
			break;
			
		case CAPTURA_TEXT:
			graphics.setColor(UtilColor.LIST_TITLE);
			graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
			break;
			
		default:
			graphics.setColor(UtilColor.LIST_TITLE);
			graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
			graphics.setColor(UtilColor.LIST_DATA);
			graphics.drawText(data, format, 0, alignment, size - format);
			break;
		}

		super.paint(graphics);
	}
}

