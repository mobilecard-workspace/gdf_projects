package com.addcel.gdf.view.consulta.listas.predialvencido;

import java.util.Date;
import java.util.Vector;

import net.rim.device.api.ui.container.VerticalFieldManager;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.consulta.listas.common.CustomGridFieldSelect;
import com.addcel.gdf.view.consulta.listas.predialvencido.dto.PredialVencido;

public class UtilPredialVencido {

	static public void getToken(Viewable viewable){

		try {

			JSONObject jsonObject = new JSONObject();

			jsonObject.put("idProveedor", 23);
			jsonObject.put("usuario", "userPrueba");
			jsonObject.put("password", "passwordPrueba");

			String key = UtilDate.getDateToDDMMAAAA(new Date());
			
			String data = "json=" + AddcelCrypto.encryptSensitive(key, jsonObject.toString());

			PagoPlacas pagoPlacas = new PagoPlacas(viewable, Url.URL_GDF_TOKEN, data);
			pagoPlacas.run();

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	static public double sumaImpuestoBimestral = 0;
	
	static public Vector getPredialVencidos(VerticalFieldManager fieldManagerElement){
		
		int fieldCout = fieldManagerElement.getFieldCount();
		
		sumaImpuestoBimestral = 0;
		
		Vector elements = new Vector();
		
		for(int index = 0; index < fieldCout; index++){
			
			CustomGridFieldSelect element = (CustomGridFieldSelect)fieldManagerElement.getField(index);
			
			if (element.isSelected()){
				
				PredialVencido predialVencido = element.getPredialVencido();
				
				String sImpuestoBimestral = predialVencido.getSubtotal2();
				
				sumaImpuestoBimestral = sumaImpuestoBimestral + Double.parseDouble(sImpuestoBimestral);
				
				elements.addElement(predialVencido);
			}
		}

		return elements;
	}
	
	
	static public void getPeriodosSeleccionados(Viewable viewable, Vector elements, String cuenta){
		

		try {
			
			// {"toDO":"6","cuenta":"048198090002", "tipo_consulta_pv":"Periodos", "cadbc":null, "periodo": "20082|20083"}
			
			String periodos = ""; 
			
			JSONObject jsonObject = new JSONObject();
			
			
			int size = elements.size();
			
			for(int index = 0; index < size; index++){

				PredialVencido predialVencido = (PredialVencido)elements.elementAt(index);
				
				if (index == 0){
					periodos = predialVencido.getPeriodo();
				} else {
					periodos = periodos + "|" + predialVencido.getPeriodo();	
				}

			}
			
			
			jsonObject.put("toDO", 6);
			jsonObject.put("cuenta", cuenta);
			jsonObject.put("tipo_consulta_pv", "Periodos");
			jsonObject.put("cadbc", "");
			jsonObject.put("periodo", periodos);
			
			String post = jsonObject.toString();

			String key = UtilDate.getDateToDDMMAAAA(new Date());
			
			String data = AddcelCrypto.encryptSensitive(key, post);

			PagoPlacas pagoPlacas = new PagoPlacas(data, viewable);
			pagoPlacas.run();
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	static public void convertToJSONArray(Vector elements){
		
		try {
		
			int size = elements.size();
			
			JSONArray jsonElements = new JSONArray();
			
			for(int index = 0; index < size; index++){

				JSONObject jsonElement = new JSONObject();

				PredialVencido predialVencido = (PredialVencido)elements.elementAt(index);
				
				jsonElement.put("actualizacion", predialVencido.getActualizacion());
				jsonElement.put("cond_Gast_Ejec", predialVencido.getCond_Gast_Ejec());
				jsonElement.put("cond_mult_omi", predialVencido.getCond_mult_omi());
				jsonElement.put("cond_rec", predialVencido.getCond_rec());
				jsonElement.put("multa_omision", predialVencido.getMulta_omision());
				jsonElement.put("recargos", predialVencido.getRecargos());
				jsonElement.put("subtotal", predialVencido.getSubtotal());
				jsonElement.put("periodo", predialVencido.getPeriodo());
				jsonElement.put("requerido", predialVencido.getRequerido());
				jsonElement.put("impuesto_bimestral", predialVencido.getImpuesto_bimestral());
				
				jsonElements.put(jsonElement);
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
