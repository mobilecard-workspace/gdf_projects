package com.addcel.gdf.view.consulta.listas.agua;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.consulta.listas.aaa.CustomGridFieldListaAgua;
import com.addcel.gdf.view.consulta.listas.agua.dto.ListaAgua;
import com.addcel.gdf.view.consulta.listas.common.CustomGridField;

public class LAgua extends CustomMainScreen implements FieldChangeListener, Viewable {

	
	private JSONObject jsObject;
	private Vector listaAgua;
	
	public LAgua(String title, JSONObject jsObject) {
		super(title, true);
		
		this.jsObject = jsObject;
		
		try {
			JSONArray jsonArray = jsObject.getJSONArray("consultaAgua");
			
			
			int length = jsonArray.length();
			
			listaAgua = new Vector();
			
			for(int index = 0; index < length; index++){
				
				JSONObject jsonObject = (JSONObject) jsonArray.get(index);
				
				String linea_captura = jsonObject.optString("linea_captura");
				String vtotal = jsonObject.optString("vtotal");
				String viva = jsonObject.optString("viva");
				String vuso = jsonObject.optString("vuso");
				String vderndom = jsonObject.optString("vderndom");
				String vderdom = jsonObject.optString("vderdom");
				String vbimestre = jsonObject.optString("vbimestre");
				String vanio = jsonObject.optString("vanio");
				String cuenta = jsonObject.optString("cuenta");
				String vigencia = jsonObject.optString("vigencia");
				String id_bitacora = jsonObject.optString("id_bitacora");
				String id_usuario = jsonObject.optString("id_usuario");
				String id_producto = jsonObject.optString("id_producto");
				String no_autorizacion = jsonObject.optString("no_autorizacion");
				String fechaPago = jsonObject.optString("fechaPago");
				String statusPago = jsonObject.optString("statusPago");
				String totalPago = jsonObject.optString("totalPago");

				ListaAgua agua = new ListaAgua();
				
				agua.setLinea_captura(linea_captura);
				agua.setVtotal(vtotal);
				agua.setViva(viva);
				agua.setVuso(vuso);
				agua.setVderndom(vderndom);
				agua.setVderdom(vderdom);
				agua.setVbimestre(vbimestre);
				agua.setVanio(vanio);
				agua.setCuenta(cuenta);
				agua.setVigencia(vigencia);
				agua.setId_bitacora(id_bitacora);
				agua.setId_usuario(id_usuario);
				agua.setId_producto(id_producto);
				agua.setNo_autorizacion(no_autorizacion);
				agua.setFechaPago(fechaPago);
				agua.setStatusPago(statusPago);
				agua.setTotalPago(totalPago);
				
				CustomGridField fieldManager = new CustomGridField(agua, DTO.STATUS_AGUA);
				
				add(fieldManager);
				
				listaAgua.addElement(agua);
			}
			
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
	}

	public void setData(int request, JSONObject jsObject) {
	}

	public void sendMessage(String message) {
	}

	public void fieldChanged(Field field, int context) {
	}

}
