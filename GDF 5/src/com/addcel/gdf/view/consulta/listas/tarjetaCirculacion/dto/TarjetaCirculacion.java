package com.addcel.gdf.view.consulta.listas.tarjetaCirculacion.dto;

public class TarjetaCirculacion {

	private String nombreSubConcepto;
	private String fechaPago;
	
	
	private String placa;
	private String importeFinal;
	private String linea_captura;
	private String totalPago;
	
	private int id_bitacora;
	
	public String getImporteFinal() {
		return importeFinal;
	}
	public void setImporteFinal(String importeFinal) {
		this.importeFinal = importeFinal;
	}
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
	}
	public String getNombreSubConcepto() {
		return nombreSubConcepto;
	}
	public void setNombreSubConcepto(String nombreSubConcepto) {
		this.nombreSubConcepto = nombreSubConcepto;
	}
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public int getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(int id_bitacora) {
		this.id_bitacora = id_bitacora;
	}

	


	
}
