package com.addcel.gdf.view.consulta.listas.common;



import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

import com.addcel.gdf.view.consulta.listas.predialvencido.dto.PredialVencido;
import com.addcel.gdf.view.util.UtilColor;

public class ObjectRichTextFieldLista extends RichTextField {


	final public static int IMPUESTO_EMITIDO = 0;
	final public static int IMPUESTO_PAGADO = 1;
	final public static int SALDO = 2;
	final public static int REQUERIDO = 3;
	final public static int PERIODO = 4;

	final private String impuestoEmitido = "Impuesto emitido: ";
	final private String impuestoPagado = "Impuesto pagado: ";
	final private String saldo = "Saldo: ";
	final private String requerido = "Requerido: ";
	final private String periodo = "Periodo: ";
	
	private String title = "";
	private String data = "";
	
	private int format;
	private int size;
	
	private int alignment;


	public ObjectRichTextFieldLista(PredialVencido adapter, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		format = Display.getWidth() / 2; 
		size = Display.getWidth();
		
		switch (type) {
		case ObjectRichTextFieldLista.IMPUESTO_EMITIDO:
			title = impuestoEmitido;
			data = adapter.getImpuesto_bimestral();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldLista.IMPUESTO_PAGADO:
			title = impuestoPagado;
			data = "$0.00";
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldLista.SALDO:
			title = saldo;
			data = adapter.getSubtotal();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldLista.REQUERIDO:
			title = requerido;
			data = adapter.getRequerido();
			alignment = DrawStyle.LEFT;
			break;
		case ObjectRichTextFieldLista.PERIODO:
			title = periodo;
			data = adapter.getPeriodo();
			alignment = DrawStyle.LEFT;
			break;
		}
	}

	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
		graphics.setColor(UtilColor.LIST_DATA);
		graphics.drawText(data, format, 0, alignment, size - format);

		super.paint(graphics);
	}
}

