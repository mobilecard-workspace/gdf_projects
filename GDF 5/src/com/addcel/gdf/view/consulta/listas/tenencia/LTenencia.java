package com.addcel.gdf.view.consulta.listas.tenencia;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.consulta.listas.common.CustomGridField;
import com.addcel.gdf.view.consulta.listas.tenencia.dto.ListaTenencia;

public class LTenencia extends CustomMainScreen {

	public LTenencia(String title, JSONObject jsObject) {
		super(title, true);
		
		try {
			JSONArray jsonArray = jsObject.getJSONArray("consultaTenencia");
			
			
			int length = jsonArray.length();
			
			for(int index = 0; index < length; index++){
				
				JSONObject jsonObject = (JSONObject) jsonArray.get(index);

				ListaTenencia listaTenencia = new ListaTenencia();

				listaTenencia.setPlaca(jsonObject.optString("placa"));
				listaTenencia.setCondonacion(jsonObject.optString("condonacion"));
				listaTenencia.setInteres(jsonObject.optString("interes"));
				listaTenencia.setSubsidio(jsonObject.optString("subsidio"));
				listaTenencia.setEjercicio(jsonObject.optString("ejercicio"));
				listaTenencia.setModelo(jsonObject.optString("modelo"));
				listaTenencia.setTotal(jsonObject.optString("total"));
				listaTenencia.setLinea_captura(jsonObject.optString("linea_captura"));
				listaTenencia.setVigencia(jsonObject.optString("vigencia"));
				listaTenencia.setTipoServicio(jsonObject.optString("tipoServicio"));
				listaTenencia.setCve_vehi(jsonObject.optString("cve_vehi"));
				listaTenencia.setTenencia(jsonObject.optString("tenencia"));
				listaTenencia.setDerecho(jsonObject.optString("derecho"));
				listaTenencia.setTenActualizacion(jsonObject.optString("tenActualizacion"));
				listaTenencia.setTenRecargo(jsonObject.optString("tenRecargo"));
				listaTenencia.setDerActualizacion(jsonObject.optString("derActualizacion"));
				listaTenencia.setDerRecargo(jsonObject.optString("derRecargo"));
				listaTenencia.setDerechos(jsonObject.optString("derechos"));
				listaTenencia.setActualizacion(jsonObject.optString("actualizacion"));
				listaTenencia.setRecargos(jsonObject.optString("recargos"));
				listaTenencia.setTenSubsidio(jsonObject.optString("tenSubsidio"));
				listaTenencia.setId_bitacora(jsonObject.optString("id_bitacora"));
				listaTenencia.setId_usuario(jsonObject.optString("id_usuario"));
				listaTenencia.setId_producto(jsonObject.optString("id_producto"));
				listaTenencia.setNo_autorizacion(jsonObject.optString("no_autorizacion"));
				listaTenencia.setFechaPago(jsonObject.optString("fechaPago"));
				listaTenencia.setStatusPago(jsonObject.optString("statusPago"));
				listaTenencia.setError(jsonObject.optString("error"));
				listaTenencia.setTotalPago(jsonObject.optString("totalPago"));

				CustomGridField fieldManager = new CustomGridField(listaTenencia, DTO.STATUS_TENENCIA);

				add(fieldManager);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
