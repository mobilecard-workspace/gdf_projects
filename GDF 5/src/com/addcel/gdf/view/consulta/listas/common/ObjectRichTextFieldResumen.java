package com.addcel.gdf.view.consulta.listas.common;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

import com.addcel.gdf.view.consulta.listas.predialvencido.dto.PredialVencido;
import com.addcel.gdf.view.util.UtilColor;

public class ObjectRichTextFieldResumen extends RichTextField {

	final public static int PERIODO = 0;
	final public static int IMPUESTO_EMITIDO = 1;
	final public static int IMPUESTO_PAGADO = 2;
	final public static int ACTUALIZACION = 3;	
	final public static int RECARGO = 4;
	final public static int MULTA = 5;
	final public static int SUBTOTAL = 6;

	final private String periodo = "Periodo: ";
	final private String impuestoEmitido = "Impuesto emitido: ";
	final private String impuestoPagado = "(-)Impuesto pagado: ";
	final private String actualizacion = "Actualización: ";
	final private String recargo = "Recargo: ";
	final private String multa = "Multa: ";
	final private String subtotal = "Subtotal periodo: ";
	
	private String title = "";
	private String data = "";
	
	private int format;
	private int size;
	private int alignment;
	
	public ObjectRichTextFieldResumen(PredialVencido adapter, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		format = Display.getWidth() / 2; 
		size = Display.getWidth();

		switch (type) {
		case ObjectRichTextFieldResumen.PERIODO:
			title = periodo;
			data = adapter.getPeriodo();
			alignment = DrawStyle.LEFT;
			break;
		case ObjectRichTextFieldResumen.IMPUESTO_EMITIDO:
			title = impuestoEmitido;
			data = adapter.getImpuesto_bimestral();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldResumen.IMPUESTO_PAGADO:
			title = impuestoPagado;
			data = "$ 0.00";
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldResumen.ACTUALIZACION:
			title = actualizacion;
			data = adapter.getActualizacion();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldResumen.RECARGO:
			title = recargo;
			data = adapter.getRecargos();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldResumen.MULTA:
			title = multa;
			data = adapter.getMulta_omision();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldResumen.SUBTOTAL:
			title = subtotal;
			data = adapter.getSubtotal();
			alignment = DrawStyle.RIGHT;
			break;
		}
	}

	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
		graphics.setColor(UtilColor.LIST_DATA);
		graphics.drawText(data, format, 0, alignment, size - format);

		super.paint(graphics);
	}
}
