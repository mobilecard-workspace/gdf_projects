package com.addcel.gdf.view.consulta.listas.predialvencido;

import java.util.Date;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.VerticalFieldManager;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VPagoAbiertoPredialVencido;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedSizeButton;
import com.addcel.gdf.view.consulta.listas.common.CustomGridFieldResumen;
import com.addcel.gdf.view.consulta.listas.common.CustomGridFieldTotal;
import com.addcel.gdf.view.consulta.listas.predialvencido.dto.PredialVencido;
import com.addcel.gdf.view.consulta.listas.predialvencido.dto.PredialVencidoTotal;
import com.addcel.gdf.view.util.UtilNumber;


public class LPredialVencidoResumen extends CustomMainScreen implements FieldChangeListener, Viewable {

	private VerticalFieldManager fieldManagerElement;
	
	private CustomSelectedSizeButton pagar;
	private CustomSelectedSizeButton cancelar;
	
	private JSONObject jsResumenPredialVencidos;
	
	private String token;
	
	
	public LPredialVencidoResumen(String title, JSONObject jsResumenPredialVencidos) {
		
		super(title, false);

		try {

			this.jsResumenPredialVencidos = jsResumenPredialVencidos;

			pagar = new CustomSelectedSizeButton("Pagar", 2);
			cancelar = new CustomSelectedSizeButton("Cancelar", 2);
			
			pagar.setChangeListener(this);
			cancelar.setChangeListener(this);
			
			CustomButtonFieldManager buttonFieldManager = new CustomButtonFieldManager(pagar, cancelar);
			buttonFieldManager.add(pagar);
			buttonFieldManager.add(cancelar);

			String totalPago = jsResumenPredialVencidos.optString("totalPago");
			String sumMultas = jsResumenPredialVencidos.optString("sumMultas");
			String sumRecargos = jsResumenPredialVencidos.optString("sumRecargos");
			String linea_captura = jsResumenPredialVencidos.optString("linea_captura");
			String gastos_ejecucion = jsResumenPredialVencidos.optString("gastos_ejecucion");
			String sumActualizacion = jsResumenPredialVencidos.optString("sumActualizacion");
			String impuestoPagado = jsResumenPredialVencidos.optString("impuestoPagado");
			String impuestoEmitido = jsResumenPredialVencidos.optString("impuestoEmitido");
			
			PredialVencidoTotal total = new PredialVencidoTotal();

			total.setImpuestoEmitido(UtilNumber.formatCurrency(impuestoEmitido));
			total.setImpuestoPagado(UtilNumber.formatCurrency(impuestoPagado));
			total.setActualizacion(UtilNumber.formatCurrency(sumActualizacion));
			total.setGastosEjecucion(UtilNumber.formatCurrency(gastos_ejecucion));
			total.setGastosEjecucionCondonados("$ 0.00");
			total.setRecargos(UtilNumber.formatCurrency(sumRecargos));
			total.setMulta(UtilNumber.formatCurrency(sumMultas));
			total.setMultaCondonada("$ 0.00");
			total.setCaptura(linea_captura);
			total.setTotal(UtilNumber.formatCurrency(totalPago));
			
			CustomGridFieldTotal customGridFieldTotal = new CustomGridFieldTotal(total, 0);
			add(customGridFieldTotal);
			
			JSONArray jsonArray = jsResumenPredialVencidos.getJSONArray("detalle");

			int length = jsonArray.length();

			fieldManagerElement = new VerticalFieldManager();
			
			for(int index = 0; index < length; index++){

				JSONObject onbject = (JSONObject) jsonArray.get(index);

				PredialVencido predialVencido = new PredialVencido();

				predialVencido.setActualizacion(UtilNumber.formatCurrency(onbject.optString("actualizacion")));
				predialVencido.setCond_Gast_Ejec(onbject.optString("cond_Gast_Ejec"));
				predialVencido.setCond_mult_omi(onbject.optString("cond_mult_omi"));
				predialVencido.setCond_rec(onbject.optString("cond_rec"));
				predialVencido.setMulta_omision(UtilNumber.formatCurrency(onbject.optString("multa_omision")));
				predialVencido.setRecargos(UtilNumber.formatCurrency(onbject.optString("recargos")));
				predialVencido.setSubtotal(UtilNumber.formatCurrency(onbject.optString("subtotal")));
				predialVencido.setPeriodo(onbject.optString("periodo"));
				predialVencido.setRequerido(onbject.optString("requerido"));
				predialVencido.setImpuesto_bimestral(UtilNumber.formatCurrency(onbject.optString("impuesto_bimestral")));

				CustomGridFieldResumen fieldManager = new CustomGridFieldResumen(predialVencido, DTO.STATUS_PREDIAL);

				fieldManagerElement.add(fieldManager);
			}

			add(fieldManagerElement);
			add(buttonFieldManager);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		addMenuItem(mPagar);
		addMenuItem(mCancelar);
	}

	private MenuItem mPagar = new MenuItem("Pagar", 110, 10) {
		public void run() {
			pagar();
		}
	};
	
	private MenuItem mCancelar = new MenuItem("Cancelar", 110, 10) {
		public void run() {
			Screen screen = UiApplication.getUiApplication().getActiveScreen();
			UiApplication.getUiApplication().popScreen(screen);
		}
	};
	
	public void fieldChanged(Field field, int context) {

		if (field == cancelar){
			UiApplication.getUiApplication().popScreen(this);
		} else if (field == pagar){

			pagar();
		}
	}
	
	
	private void pagar(){
		
		try {
			String imei = UtilBB.getImei();
			jsResumenPredialVencidos.put("id_producto", 6);
			jsResumenPredialVencidos.put("id_usuario", UserBean.idLogin);
			
			jsResumenPredialVencidos.put("password", UserBean.password);
			jsResumenPredialVencidos.put("imei", imei);
			jsResumenPredialVencidos.put("tipo", UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
			jsResumenPredialVencidos.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
			jsResumenPredialVencidos.put("modelo_procom", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
			jsResumenPredialVencidos.put("wkey", imei);
			jsResumenPredialVencidos.put("cx", "0.0");
			jsResumenPredialVencidos.put("cy", "0.0");

			jsResumenPredialVencidos.put("token", token);

			jsResumenPredialVencidos.put("multa_incumplimiento", 0);
			jsResumenPredialVencidos.put("concepto", "Impuesto Predial Vencido");
			jsResumenPredialVencidos.put("error_descripcion", "0");
			
			jsResumenPredialVencidos.put("error_cel", "");
			
			UiApplication.getUiApplication().popScreen(this);
			VPagoAbiertoPredialVencido vPagoAbierto = new VPagoAbiertoPredialVencido("Pago de Predial Vencido", jsResumenPredialVencidos, "Predial Vencido");
			UiApplication.getUiApplication().pushScreen(vPagoAbierto);
			
		} catch (JSONException e) {

			e.printStackTrace();
			Dialog.alert("Error al adjuntar la información.");
		}
	}


	public void setData(int request, JSONObject jsObject) {

		// {"numError":0,"token":"08ziZvz4OhFB0BES+zBUe70ur73fz70Ry40LmChapDESszy8qmiUw=","error":""}
		
		System.out.println(jsObject.toString());
		
		if (jsObject.has("numError")){
			
			
			int numError = jsObject.optInt("numError");
			
			if (numError == 0){
				
				if (jsObject.has("token")){
					token = jsObject.optString("token");
				}

				String imei = UtilBB.getImei();

				try {
					
					jsResumenPredialVencidos.put("id_producto", 6);
					jsResumenPredialVencidos.put("id_usuario", UserBean.idLogin);
					
					jsResumenPredialVencidos.put("password", UserBean.password);
					jsResumenPredialVencidos.put("imei", imei);
					jsResumenPredialVencidos.put("tipo", UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
					jsResumenPredialVencidos.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
					jsResumenPredialVencidos.put("modelo_procom", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
					jsResumenPredialVencidos.put("wkey", imei);
					jsResumenPredialVencidos.put("cx", "0.0");
					jsResumenPredialVencidos.put("cy", "0.0");

					jsResumenPredialVencidos.put("token", token);

					jsResumenPredialVencidos.put("multa_incumplimiento", 0);
					jsResumenPredialVencidos.put("concepto", "Impuesto Predial Vencido");
					jsResumenPredialVencidos.put("error_descripcion", "0");
					
					jsResumenPredialVencidos.put("error_cel", "");

					jsResumenPredialVencidos.put("cvv2", "563");

					String post = jsResumenPredialVencidos.toString();

					String key = UtilDate.getDateToDDMMAAAA(new Date());
					
					System.out.println(key);
					System.out.println(key);
					System.out.println(key);
					
					
					String data = AddcelCrypto.encryptSensitive(key, post);

					PagoPlacas pagoPlacas = new PagoPlacas(data, this, Url.URL_PAGO_PROSA);
					pagoPlacas.run();
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
				
				
				
			} else {
				
				String error = jsObject.optString("error");
				Dialog.alert(error);
			}
		}
		
	}


	public void sendMessage(String message) {
		
	}
}

