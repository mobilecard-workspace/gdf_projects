package com.addcel.gdf.view.consulta.listas.predialvencidocons;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.consulta.listas.predialvencidocons.dto.PredialVencidoDetalle;
import com.addcel.gdf.view.consulta.listas.predialvencidocons.dto.PredialVencidoMaestro;


public class LPredialConsulta extends CustomMainScreen{


	public LPredialConsulta(String title, JSONObject jsObject) {
		super(title, true);
		
		try {

			JSONArray jsonArray = jsObject.getJSONArray("consultaPredialVencido");

			int length = jsonArray.length();
			
			for(int index = 0; index < length; index++){
				
				JSONObject onbject = (JSONObject) jsonArray.get(index);

				PredialVencidoMaestro predialMaestro = new PredialVencidoMaestro();
 
				predialMaestro.setConcepto(onbject.optString("concepto"));
				predialMaestro.setLinea_captura(onbject.optString("linea_captura"));
				predialMaestro.setId_bitacora(onbject.optString("id_bitacora"));
				predialMaestro.setId_usuario(onbject.optString("id_usuario"));
				predialMaestro.setId_producto(onbject.optString("id_producto"));
				predialMaestro.setNo_autorizacion(onbject.optString("no_autorizacion"));
				predialMaestro.setFechaPago(onbject.optString("fechaPago"));
				predialMaestro.setStatusPago(onbject.optString("statusPago"));
				predialMaestro.setTotalPago(onbject.optString("totalPago"));
				predialMaestro.setGastos_ejecucion(onbject.optString("gastos_ejecucion"));
				predialMaestro.setMulta_incumplimiento(onbject.optString("multa_incumplimiento"));
				predialMaestro.setTotal_cuenta(onbject.optString("total_cuenta"));
				predialMaestro.setBimestres(onbject.optString("bimestres"));
				predialMaestro.setSumActualizacion(onbject.optString("sumActualizacion"));
				predialMaestro.setSumRecargos(onbject.optString("sumRecargos"));
				predialMaestro.setSumMultas(onbject.optString("sumMultas"));
				predialMaestro.setImpuestoEmitido(onbject.optString("impuestoEmitido"));
				predialMaestro.setImpuestoPagado(onbject.optString("impuestoPagado"));
				predialMaestro.setError(onbject.optString("error"));
				predialMaestro.setBanco(onbject.optString("banco"));
				
				JSONArray jsonElements = onbject.getJSONArray("detalle");
				
				int lengthE = jsonElements.length();
				
				for(int indexE = 0; indexE < lengthE; indexE++){
					
					JSONObject onbjectE = (JSONObject) jsonElements.get(index);

					PredialVencidoDetalle listaPredialE = new PredialVencidoDetalle();

					listaPredialE.setCond_rec(onbjectE.optString("cond_rec"));
					listaPredialE.setMulta_omision(onbjectE.optString("multa_omision"));
					listaPredialE.setPeriodo(onbjectE.optString("periodo"));
					listaPredialE.setRecargos(onbjectE.optString("recargos"));
					listaPredialE.setImpuesto_emitido(onbjectE.optString("impuesto_emitido"));
					listaPredialE.setId_bitacora(onbjectE.optString("id_bitacora"));
					listaPredialE.setImpuesto_pagado(onbjectE.optString("impuesto_pagado"));
					listaPredialE.setCond_Gast_Ejec(onbjectE.optString("cond_Gast_Ejec"));
					listaPredialE.setImpuesto_bimestral(onbjectE.optString("impuesto_bimestral"));
					listaPredialE.setActualizacion(onbjectE.optString("actualizacion"));
					listaPredialE.setCond_mult_omi(onbjectE.optString("cond_mult_omi"));
					listaPredialE.setRequerido(onbjectE.optString("requerido"));
					listaPredialE.setSubtotal(onbjectE.optString("subtotal"));

					predialMaestro.addPredial(listaPredialE);
				}

				CustomGridFieldConsulta fieldManager = new CustomGridFieldConsulta(predialMaestro, jsObject);
				
				add(fieldManager);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}