package com.addcel.gdf.view.consulta.listas.tarjetaCirculacion;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.consulta.listas.tarjetaCirculacion.dto.TarjetaCirculacion;

public class LTarjetaCirculacion extends CustomMainScreen {

	public LTarjetaCirculacion(String title, JSONObject jsObject) {
		super(title, true);
		
		try {
			JSONArray jsonArray = jsObject.getJSONArray("consultaTarjetaCirculacion");
			
			
			int length = jsonArray.length();
			
			for(int index = 0; index < length; index++){
				
				JSONObject jsonObject = (JSONObject) jsonArray.get(index);

				TarjetaCirculacion tarjetaCirculacion = new TarjetaCirculacion();

				tarjetaCirculacion.setPlaca(jsonObject.optString("placa"));
				tarjetaCirculacion.setImporteFinal(jsonObject.optString("importeFinal"));
				tarjetaCirculacion.setLinea_captura(jsonObject.optString("linea_captura"));
				tarjetaCirculacion.setTotalPago(jsonObject.optString("totalPago"));
				tarjetaCirculacion.setId_bitacora(jsonObject.optInt("id_bitacora"));
				
				CustomGridFieldTarjeta fieldManager = new CustomGridFieldTarjeta(tarjetaCirculacion);

				add(fieldManager);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
