package com.addcel.gdf.view.consulta.listas.infracciones;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.consulta.listas.common.CustomGridField;
import com.addcel.gdf.view.consulta.listas.infracciones.dto.ListaInfracciones;

public class LInfracciones extends CustomMainScreen{


	public LInfracciones(String title, JSONObject jsObject) {
		super(title, true);
		
		try {
			JSONArray jsonArray = jsObject.getJSONArray("consultaInfraccion");
			
			
			int length = jsonArray.length();
			
			for(int index = 0; index < length; index++){
				
				JSONObject object = (JSONObject) jsonArray.get(index);

				ListaInfracciones listaInfracciones = new ListaInfracciones();

				listaInfracciones.setPlaca(object.optString("placa"));
				listaInfracciones.setFolio(object.optString("folio"));
				listaInfracciones.setLinea_captura(object.optString("linea_captura"));
				listaInfracciones.setImporte(object.optString("importe"));
				listaInfracciones.setFechainfraccion(object.optString("fechainfraccion"));
				listaInfracciones.setActualizacion(object.optString("actualizacion"));
				listaInfracciones.setRecargos(object.optString("recargos"));
				listaInfracciones.setDias_multa(object.optString("dias_multa"));
				listaInfracciones.setCodeCalc(object.optString("codeCalc"));
				listaInfracciones.setId_bitacora(object.optString("id_bitacora"));
				listaInfracciones.setId_usuario(object.optString("id_usuario"));
				listaInfracciones.setId_producto(object.optString("id_producto"));
				listaInfracciones.setNo_autorizacion(object.optString("no_autorizacion"));
				listaInfracciones.setFechaPago(object.optString("fechaPago"));
				listaInfracciones.setStatusPago(object.optString("statusPago"));
				listaInfracciones.setError(object.optString("error"));
				listaInfracciones.setTotalPago(object.optString("totalPago"));
				
				CustomGridField fieldManager = new CustomGridField(listaInfracciones, DTO.STATUS_INFRACCION);

				add(fieldManager);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
