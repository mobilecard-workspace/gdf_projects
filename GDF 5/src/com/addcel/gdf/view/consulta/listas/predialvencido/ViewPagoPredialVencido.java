package com.addcel.gdf.view.consulta.listas.predialvencido;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.listas.predial.dto.ListaPredial;
import com.addcel.gdf.view.util.UtilNumber;


public class ViewPagoPredialVencido extends CustomMainScreen implements FieldChangeListener, Viewable {

	private LabelField textConcepto = null;
	private LabelField textCuentaP = null;
	private LabelField textBimestre = null;
	private LabelField textLCaptura = null;
	private LabelField textImporte = null;
	private LabelField textReduccion = null;
	private LabelField textTotal = null;

	private HighlightsLabelField editConcepto = null;
	private HighlightsLabelField editCuentaP = null;
	private HighlightsLabelField editBimestre = null;
	private HighlightsLabelField editLCaptura = null;
	private HighlightsLabelField editImporte = null;
	private HighlightsLabelField editReduccion = null;
	private HighlightsLabelField editTotal = null;

	private SlimSelectedButtonField consultar = null;
	
	private SplashScreen splashScreen;
	private ListaPredial listaPredial;

	
	public ViewPagoPredialVencido(String title, ListaPredial listaPredial) {

		super(title, true);

		this.listaPredial = listaPredial;
		
		splashScreen = SplashScreen.getInstance();

		textConcepto = new ColorLabelField("Concepto: ", LabelField.NON_FOCUSABLE);
		textCuentaP = new ColorLabelField("Cuenta Predial: ", LabelField.NON_FOCUSABLE);
		textBimestre = new ColorLabelField("Bimestre: ", LabelField.NON_FOCUSABLE);
		textLCaptura = new ColorLabelField("Captura: ", LabelField.NON_FOCUSABLE);
		textImporte = new ColorLabelField("Importe: ", LabelField.NON_FOCUSABLE);
		textReduccion = new ColorLabelField("Reducci�n: ", LabelField.NON_FOCUSABLE);
		textTotal = new ColorLabelField("Total: ", LabelField.NON_FOCUSABLE);

		editConcepto = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editCuentaP = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editBimestre = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editLCaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editImporte = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editReduccion = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);

		consultar = new SlimSelectedButtonField("Reenviar recibo", ButtonField.CONSUME_CLICK);
		consultar.setChangeListener(this);

		add(new NullField());
		
		add(new LabelField(""));
		
		addHorizontalFiels(textConcepto, editConcepto);
		addHorizontalFiels(textCuentaP, editCuentaP);
		addHorizontalFiels(textBimestre, editBimestre);
		addHorizontalFiels(textLCaptura, editLCaptura);
		addHorizontalFiels(textImporte, editImporte);
		addHorizontalFiels(textReduccion, editReduccion);
		addHorizontalFiels(textTotal, editTotal);
		add(consultar);
		
		
		editConcepto.setText(listaPredial.getConcepto());
		editCuentaP.setText(listaPredial.getCuentaP());
		editBimestre.setText(listaPredial.getBimestre());
		editLCaptura.setText(listaPredial.getLinea_captura());
		editImporte.setText(UtilNumber.formatCurrency(listaPredial.getIntImpuesto()));
		editReduccion.setText(UtilNumber.formatCurrency(listaPredial.getReduccion()));
		editTotal.setText(UtilNumber.formatCurrency(listaPredial.getTotalPago()));
	}

	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}


	public void fieldChanged(Field field, int context) {

		if(field == consultar){
			
			BasicEditField fieldMail = new BasicEditField(BasicEditField.FILTER_EMAIL);
			 
			Dialog d = new Dialog("�Desea enviar la informaci�n a otra direcci�n de mail?, escribala:", new String[] {"Enviar a otra direcci�n", "Enviar", "Cancelar"}, new int[] {2,1,0}, 2, Bitmap.getPredefinedBitmap(Bitmap.QUESTION));
			d.add(fieldMail);
			 
			int result = d.doModal();
			 
			 if (result == 2) {
			 
				 sendComprobante(true, fieldMail.getText());

			} else if (result == 1) {
				 
				sendComprobante(false, fieldMail.getText());
			}
		}
	}
	
	
	private void sendComprobante(boolean value, String email){
		
		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("id_bitacora", listaPredial.getId_bitacora());
			jsonObject.put("id_producto", DTO.STATUS_PREDIAL);
			jsonObject.put("id_usuario", UserBean.idLogin);

			if (value){
				
				jsonObject.put("email", email);
			}
			
			String post = jsonObject.toString();

			String data = AddcelCrypto.encryptHard(post);

			splashScreen.start();
			
			GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_GDF_REENVIO, this);
			genericHTTP.run();
			
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessage("Error al recopilar la informacion");
		}
	}
	

	public void setData(int request, JSONObject jsObject){

		splashScreen.remove();
		
		if (jsObject.has("error")){
			
			String mensaje = jsObject.optString("error");
			 
		    Dialog dialog = new Dialog(Dialog.D_OK, mensaje, Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		 
		    int i = dialog.doModal();
		 
		    if (i == Dialog.OK) {
		        UiApplication.getUiApplication().popScreen(this);
		    } 
		}
	}
	
	
	/*
	public void setData(int request, JSONObject jsObject){

		splashScreen.remove();
		
		if (jsObject.has("error")){
			
			String mensaje = jsObject.optString("error");
			sendMessage(mensaje);
		}
	}
	*/
	
	public void sendMessage(String message) {
		splashScreen.remove();
		Dialog.alert(message);
	}

}

