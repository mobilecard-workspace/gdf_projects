package com.addcel.gdf.view.consulta.listas.licencia.dto;

public class Licencia {

	private String concepto;
	private String fechaPago;
	private String ejercicio;
	
	private String rfc;
	private String importeFinal;
	private String linea_captura;
	private String totalPago;
	
	private int id_bitacora;
	
	private boolean isPermanente;

	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getImporteFinal() {
		return importeFinal;
	}
	public void setImporteFinal(String importeFinal) {
		this.importeFinal = importeFinal;
	}
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getEjercicio() {
		return ejercicio;
	}
	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}
	public int getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(int id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public boolean isPermanente() {
		return isPermanente;
	}
	public void setPermanente(boolean isPermanente) {
		this.isPermanente = isPermanente;
	}
}
