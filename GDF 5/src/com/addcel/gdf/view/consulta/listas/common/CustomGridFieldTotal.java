package com.addcel.gdf.view.consulta.listas.common;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import com.addcel.gdf.view.consulta.listas.predialvencido.dto.PredialVencidoTotal;
import com.addcel.gdf.view.util.UtilColor;

public class CustomGridFieldTotal extends VerticalFieldManager {

	//private PredialVencido predialVencido;

	private boolean isSelected = false;
	
	public CustomGridFieldTotal(PredialVencidoTotal predialVencido, int servicio) {

		super();
		
		//add(new ObjectRichTextFieldTotal(predialVencido, ObjectRichTextFieldTotal.IMPUESTO_EMITIDO));
		//add(new ObjectRichTextFieldTotal(predialVencido, ObjectRichTextFieldTotal.IMPUESTO_PAGADO));
		add(new ObjectRichTextFieldTotal(predialVencido, ObjectRichTextFieldTotal.ACTUALIZACION));
		add(new ObjectRichTextFieldTotal(predialVencido, ObjectRichTextFieldTotal.GASTOS_EJECUCION));
		add(new ObjectRichTextFieldTotal(predialVencido, ObjectRichTextFieldTotal.GASTOS_EJECUCION_CONDONADOS));
		add(new ObjectRichTextFieldTotal(predialVencido, ObjectRichTextFieldTotal.RECARGOS));
		add(new ObjectRichTextFieldTotal(predialVencido, ObjectRichTextFieldTotal.MULTA));
		add(new ObjectRichTextFieldTotal(predialVencido, ObjectRichTextFieldTotal.MULTA_CONDONADA));
		add(new ObjectRichTextFieldTotal(predialVencido, ObjectRichTextFieldTotal.CAPTURA_TEXT));
		add(new ObjectRichTextFieldTotal(predialVencido, ObjectRichTextFieldTotal.CAPTURA));
		add(new ObjectRichTextFieldTotal(predialVencido, ObjectRichTextFieldTotal.TOTAL));
		add(new NullField());
	}

	public boolean setData() {
		return true;
	}

	protected void paint(Graphics graphics){

		int width = getPreferredWidth();
		int height = getPreferredHeight();
		
		graphics.clear();
		
		if (isFocus()){
			graphics.setColor(UtilColor.LIST_FOCUS);
			graphics.setGlobalAlpha(180);
			graphics.fillRoundRect(0, 0, width, height, 1, 1);
			graphics.setGlobalAlpha(255);
		} else if(isSelected){
			graphics.setColor(UtilColor.LIST_SELECTED);
			graphics.fillRoundRect(0, 0, width, height, 1, 1);
		} else {
			graphics.setColor(UtilColor.LIST_UNSELECTED);
			graphics.fillRoundRect(0, 0, width, height, 1, 1);
		}
		
		graphics.setColor(Color.WHITE);
		
		graphics.fillRoundRect(0, height-2, width, height, 1, 1);

		super.paint(graphics);
	}

	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		//setSelected();
		return super.navigationClick(status, time);
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			//setSelected();
			return true;
		}

		return super.keyChar(character, status, time);
	}

	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK){
			fieldChangeNotify(0);
			//setSelected();
			return true;
		}

		return super.touchEvent(message);
	}
	
	public boolean isSelected() {
		return isSelected;
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
}