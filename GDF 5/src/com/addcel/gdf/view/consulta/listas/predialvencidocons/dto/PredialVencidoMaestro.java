package com.addcel.gdf.view.consulta.listas.predialvencidocons.dto;

import java.util.Vector;

public class PredialVencidoMaestro {

	private String gastos_ejecucion;
	private String multa_incumplimiento;
	private String total_cuenta;
	private String bimestres;
	private String concepto;
	private String sumActualizacion;
	private String sumRecargos;
	private String sumMultas;
	private String impuestoEmitido;
	private String impuestoPagado;
	private String id_bitacora;
	private String id_usuario;
	private String id_producto;
	private String no_autorizacion;
	private String linea_captura;
	private String fechaPago;
	private String statusPago;
	private String error;
	private String totalPago;
	private String banco;
	
	private Vector predialesVencidos;
	
	public void addPredial(PredialVencidoDetalle predialVencidoDetalle){
		
		if (predialesVencidos == null){
			predialesVencidos = new Vector();
		}
		
		predialesVencidos.addElement(predialVencidoDetalle);
	}


	public Vector getPredialesVencidos(){

		return predialesVencidos;
	}
	
	
	public String getGastos_ejecucion() {
		return gastos_ejecucion;
	}
	public void setGastos_ejecucion(String gastos_ejecucion) {
		this.gastos_ejecucion = gastos_ejecucion;
	}
	public String getMulta_incumplimiento() {
		return multa_incumplimiento;
	}
	public void setMulta_incumplimiento(String multa_incumplimiento) {
		this.multa_incumplimiento = multa_incumplimiento;
	}
	public String getTotal_cuenta() {
		return total_cuenta;
	}
	public void setTotal_cuenta(String total_cuenta) {
		this.total_cuenta = total_cuenta;
	}
	public String getBimestres() {
		return bimestres;
	}
	public void setBimestres(String bimestres) {
		this.bimestres = bimestres;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getSumActualizacion() {
		return sumActualizacion;
	}
	public void setSumActualizacion(String sumActualizacion) {
		this.sumActualizacion = sumActualizacion;
	}
	public String getSumRecargos() {
		return sumRecargos;
	}
	public void setSumRecargos(String sumRecargos) {
		this.sumRecargos = sumRecargos;
	}
	public String getSumMultas() {
		return sumMultas;
	}
	public void setSumMultas(String sumMultas) {
		this.sumMultas = sumMultas;
	}
	public String getImpuestoEmitido() {
		return impuestoEmitido;
	}
	public void setImpuestoEmitido(String impuestoEmitido) {
		this.impuestoEmitido = impuestoEmitido;
	}
	public String getImpuestoPagado() {
		return impuestoPagado;
	}
	public void setImpuestoPagado(String impuestoPagado) {
		this.impuestoPagado = impuestoPagado;
	}
	public String getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(String id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getId_producto() {
		return id_producto;
	}
	public void setId_producto(String id_producto) {
		this.id_producto = id_producto;
	}
	public String getNo_autorizacion() {
		return no_autorizacion;
	}
	public void setNo_autorizacion(String no_autorizacion) {
		this.no_autorizacion = no_autorizacion;
	}
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getStatusPago() {
		return statusPago;
	}
	public void setStatusPago(String statusPago) {
		this.statusPago = statusPago;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
}
