package com.addcel.gdf.view.consulta.listas.aaa;

import com.addcel.gdf.view.consulta.listas.agua.dto.ListaAgua;
import com.addcel.gdf.view.util.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;


public class ListaAguaRichTextField extends RichTextField {


	final public static int DATE_FECHA_PAGO = 0;
	final public static int DATE_CUENTA = 1;
	
	final private String fechaPago = "Fecha de pago: ";
	final private String cuenta = "Cuenta de Agua: ";
	
	private String showData = "";
	private String data = "";
	private ListaAgua listaAgua;

	
	private int format;
	private int size;
	
	public ListaAgua getScheduled(){
		return listaAgua;
	}
	
	public ListaAguaRichTextField(ListaAgua listaAgua, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		
		format = (Display.getWidth()*9) / 20; 
		size = Display.getWidth();
		this.listaAgua = listaAgua;
		
		switch (type) {
		case ListaAguaRichTextField.DATE_FECHA_PAGO:
			showData = cuenta;
			data = listaAgua.getCuenta();
			break;
		case ListaAguaRichTextField.DATE_CUENTA:
			showData = fechaPago;
			data = listaAgua.getFechaPago();
			break;
		}
	}

	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_TITLE);
		graphics.drawText(showData, 0, 0, DrawStyle.RIGHT, format);
		graphics.setColor(UtilColor.LIST_DATA);
		graphics.drawText(data, format, 0, DrawStyle.LEFT, size);

		super.paint(graphics);
	}
}
