package com.addcel.gdf.view.consulta.listas.aaa;

import com.addcel.gdf.view.consulta.listas.agua.dto.ListaAgua;
import com.addcel.gdf.view.consulta.listas.nomina.dto.ListaNomina;
import com.addcel.gdf.view.consulta.listas.predial.dto.ListaPredial;
import com.addcel.gdf.view.util.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;


public class ListaPredialRichTextField extends RichTextField {


	final public static int DATE_CUENTA_PREDIAL = 0;
	final public static int DATE_FECHA = 1;
	
	final private String cuentaPredial = "Cuenta Predial: ";
	final private String fecha = "Fecha: ";
	
	private String title = "";
	private String data = "";
	private ListaPredial listaPredial;

	
	private int format;
	private int size;
	
	public ListaPredialRichTextField(ListaPredial listaPredial, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		
		format = (Display.getWidth()*9) / 20; 
		size = Display.getWidth();
		this.listaPredial = listaPredial;
		
		switch (type) {
		case ListaPredialRichTextField.DATE_CUENTA_PREDIAL:
			title = cuentaPredial;
			data = listaPredial.getCuentaP();
			break;
		case ListaPredialRichTextField.DATE_FECHA:
			title = fecha;
			data = listaPredial.getFechaPago();
			break;
		}
	}

	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
		graphics.setColor(UtilColor.LIST_DATA);
		graphics.drawText(data, format, 0, DrawStyle.LEFT, size);

		super.paint(graphics);
	}
}
