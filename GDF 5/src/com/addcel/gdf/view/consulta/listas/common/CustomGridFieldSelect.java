package com.addcel.gdf.view.consulta.listas.common;

import net.rim.device.api.system.Display;
import net.rim.device.api.system.KeypadListener;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import com.addcel.gdf.view.consulta.listas.predialvencido.dto.PredialVencido;
import com.addcel.gdf.view.util.UtilColor;

public class CustomGridFieldSelect extends VerticalFieldManager {

	private PredialVencido predialVencido;
	private int servicio;
	private boolean isSelected = false;
	private boolean isRequerido = false;
	public CustomGridFieldSelect(PredialVencido predialVencido, int servicio) {

		super();

		this.predialVencido = predialVencido;
		this.servicio = servicio;
		
		String requerido = predialVencido.getRequerido();
		
		requerido = requerido.toUpperCase();
		
		if(requerido.equals("SI")){
			isRequerido = true;
			setSelected();
		} 
		
		add(new ObjectRichTextFieldLista(predialVencido, ObjectRichTextFieldLista.REQUERIDO));
		add(new ObjectRichTextFieldLista(predialVencido, ObjectRichTextFieldLista.PERIODO));
		//add(new ObjectRichTextFieldLista(predialVencido, ObjectRichTextFieldLista.IMPUESTO_EMITIDO));
		//add(new ObjectRichTextFieldLista(predialVencido, ObjectRichTextFieldLista.IMPUESTO_PAGADO));
		add(new ObjectRichTextFieldLista(predialVencido, ObjectRichTextFieldLista.SALDO));
		add(new NullField());
	}


	public boolean setData() {
		return true;
	}


	protected void paint(Graphics graphics){

		int width = getPreferredWidth();
		int height = getPreferredHeight();
		
		graphics.clear();
		
		if (isFocus()){
			graphics.setColor(UtilColor.LIST_FOCUS);
			graphics.setGlobalAlpha(180);
			graphics.fillRoundRect(0, 0, width, height, 1, 1);
			graphics.setGlobalAlpha(255);
		} else if(isSelected){
			graphics.setColor(UtilColor.LIST_SELECTED);
			graphics.fillRoundRect(0, 0, width, height, 1, 1);
		} else {
			graphics.setColor(UtilColor.LIST_UNSELECTED);
			graphics.fillRoundRect(0, 0, width, height, 1, 1);
		}
		
		graphics.setColor(Color.WHITE);
		
		graphics.fillRoundRect(0, height-2, width, height, 1, 1);
		/*
		graphics.drawLine(0, height-2, width, height);
		graphics.drawLine(0, height-1, width, height);
		graphics.drawLine(0, height, width, height);
		*/
		super.paint(graphics);
	}

	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		setSelected();
		//return super.navigationClick(status, time);
		
		//System.out.println("status: " + status);
		//System.out.println("STATUS_TRACKWHEEL: " + KeypadListener.STATUS_TRACKWHEEL);
		//System.out.println("STATUS_FOUR_WAY: " + KeypadListener.STATUS_FOUR_WAY);
		
		return true;
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			setSelected();
			return true;
		}

		return super.keyChar(character, status, time);
	}

	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK){
			fieldChangeNotify(0);
			setSelected();
			return true;
		}

		return super.touchEvent(message);
	}
	
	public boolean isSelected() {
		return isSelected;
	}

	private void setSelected(){
		
		if (isRequerido){
			isSelected = true;
		} else {
			isSelected = !isSelected;
		}
	}
	
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}


	public PredialVencido getPredialVencido() {
		return predialVencido;
	}
}