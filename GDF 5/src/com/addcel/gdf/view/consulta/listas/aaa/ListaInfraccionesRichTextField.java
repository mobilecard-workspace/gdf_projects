package com.addcel.gdf.view.consulta.listas.aaa;

import com.addcel.gdf.view.consulta.listas.agua.dto.ListaAgua;
import com.addcel.gdf.view.consulta.listas.infracciones.dto.ListaInfracciones;
import com.addcel.gdf.view.consulta.listas.nomina.dto.ListaNomina;
import com.addcel.gdf.view.consulta.listas.predial.dto.ListaPredial;
import com.addcel.gdf.view.util.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;


public class ListaInfraccionesRichTextField extends RichTextField {


	final public static int DATE_INFRACCION = 0;
	final public static int DATE_FECHA = 1;
	
	final private String cuentaPredial = "Infracción: ";
	final private String fecha = "Fecha: ";
	
	private String title = "";
	private String data = "";
	//private ListaInfracciones listaInfracciones;

	
	private int format;
	private int size;
	
	public ListaInfraccionesRichTextField(ListaInfracciones listaInfracciones, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		
		format = (Display.getWidth()*9) / 20; 
		size = Display.getWidth();
		//this.listaInfracciones = listaInfracciones;
		
		switch (type) {
		case ListaInfraccionesRichTextField.DATE_INFRACCION:
			title = cuentaPredial;
			data = listaInfracciones.getFolio();
			break;
		case ListaInfraccionesRichTextField.DATE_FECHA:
			title = fecha;
			data = listaInfracciones.getFechaPago();
			break;
		}
	}

	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
		graphics.setColor(UtilColor.LIST_DATA);
		graphics.drawText(data, format, 0, DrawStyle.LEFT, size);

		super.paint(graphics);
	}
}
