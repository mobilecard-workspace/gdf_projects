package com.addcel.gdf.view.consulta.listas.infracciones;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.Adeudo;
import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedButtonField;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.listas.infracciones.dto.ListaInfracciones;
import com.addcel.gdf.view.util.UtilNumber;



public class ViewPagoInfracciones extends CustomMainScreen implements FieldChangeListener, Viewable {

	private HighlightsLabelField editFecha;
	private HighlightsLabelField editFolio;
	private HighlightsLabelField editMulta;
	private HighlightsLabelField editActualizacion;
	private HighlightsLabelField editRecargo;
	
	private HighlightsLabelField editImporte;
	private HighlightsLabelField editLineaCaptura;
	
	private LabelField textFecha;
	private LabelField textFolio;
	private LabelField textMulta;
	private LabelField textActualizacion;
	private LabelField textRecargo;
	
	private LabelField textImporte;
	private LabelField textLineaCaptura;

	private CustomSelectedButtonField consultar = null;
	private SlimSelectedButtonField salir = null;
	
	private boolean listoPagar = false;
	private SplashScreen splashScreen;
	private Adeudo adeudo;
	

	private int indexDerecho = 0;
	private HorizontalFieldManager fieldManagerDerecho = null;
	private boolean isDerecho = true;
	
	private ListaInfracciones listaInfracciones;
	
	
	public ViewPagoInfracciones(String title, ListaInfracciones listaInfracciones) {
		
		super(title, true);
		this.listaInfracciones = listaInfracciones;
		splashScreen = SplashScreen.getInstance();
		addComponents();
	}
	
	private void addComponents(){


		textFecha = new ColorLabelField("Fecha de infracci�n: ", LabelField.NON_FOCUSABLE);
		textFolio = new ColorLabelField("Folio: ", LabelField.NON_FOCUSABLE);
		textMulta = new ColorLabelField("Multa: ", LabelField.NON_FOCUSABLE);
		textActualizacion = new ColorLabelField("Actualizaci�n: ", LabelField.NON_FOCUSABLE);
		textRecargo = new ColorLabelField("Recargos: ", LabelField.NON_FOCUSABLE);
		
		textImporte = new ColorLabelField("Total a pagar: ", LabelField.NON_FOCUSABLE);
		textLineaCaptura = new ColorLabelField("L�nea de captura: ", LabelField.NON_FOCUSABLE);
		
		editFecha = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editFolio = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editMulta = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editActualizacion = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editRecargo = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		
		editImporte = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editLineaCaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);

		consultar = new CustomSelectedButtonField("Reenviar recibo", ButtonField.CONSUME_CLICK);
		consultar.setChangeListener(this);

		salir =  new SlimSelectedButtonField("Cancelar", ButtonField.CONSUME_CLICK);
		salir.setChangeListener(this);
		
		add(new NullField());
		
		add(new LabelField(""));
		
		addHorizontalFiels(textFecha, editFecha);
		addHorizontalFiels(textFolio, editFolio);
		addHorizontalFiels(textMulta, editMulta);
		addHorizontalFiels(textActualizacion, editActualizacion);
		addHorizontalFiels(textRecargo, editRecargo);
		addHorizontalFiels(textImporte, editImporte);
		
		add(new LabelField(""));
		add(textLineaCaptura);
		add(editLineaCaptura);
		
		add(new LabelField(""));
		
		add(consultar);
		add(salir);
		
		editFecha.setText(listaInfracciones.getFechaPago());
		editFolio.setText(listaInfracciones.getFolio());
		editMulta.setText(listaInfracciones.getFolio());
		editActualizacion.setText(UtilNumber.formatCurrency(listaInfracciones.getActualizacion()));
		editRecargo.setText(UtilNumber.formatCurrency(listaInfracciones.getRecargos()));
		editImporte.setText(UtilNumber.formatCurrency(listaInfracciones.getTotalPago()));
		editLineaCaptura.setText(listaInfracciones.getLinea_captura());
	}
	
	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}
	
	
	public void fieldChanged(Field field, int context) {

		if(field == consultar){
			
			BasicEditField fieldMail = new BasicEditField(BasicEditField.FILTER_EMAIL);
			 
			Dialog d = new Dialog("�Desea enviar la informaci�n a otra direcci�n de mail?, escribala:", new String[] {"Enviar a otra direcci�n", "Enviar", "Cancelar"}, new int[] {2,1,0}, 2, Bitmap.getPredefinedBitmap(Bitmap.QUESTION));
			d.add(fieldMail);
			 
			int result = d.doModal();
			 
			 if (result == 2) {
			 
				 sendComprobante(true, fieldMail.getText());

			} else if (result == 1) {
				 
				sendComprobante(false, fieldMail.getText());
			}
		} else if(field == salir){
			
			UiApplication.getUiApplication().popScreen(this);
		}
	}

	
	private void sendComprobante(boolean value, String email){
		
		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("id_bitacora", listaInfracciones.getId_bitacora());
			jsonObject.put("id_producto", DTO.STATUS_INFRACCION);
			jsonObject.put("id_usuario", UserBean.idLogin);

			if (value){
				
				jsonObject.put("email", email);
			}
			
			String post = jsonObject.toString();

			String data = AddcelCrypto.encryptHard(post);

			splashScreen.start();
			
			GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_GDF_REENVIO, this);
			genericHTTP.run();
			
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessage("Error al recopilar la informacion");
		}
	}
	
	
	
	public void setData(int request, JSONObject jsObject){

		splashScreen.remove();
		
		if (jsObject.has("error")){
			
			String mensaje = jsObject.optString("error");
			 
		    Dialog dialog = new Dialog(Dialog.D_OK, mensaje, Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		 
		    int i = dialog.doModal();
		 
		    if (i == Dialog.OK) {
		        UiApplication.getUiApplication().popScreen(this);
		    } 
		}
	}

	
	public void sendMessage(String message) {
		splashScreen.remove();
		Dialog.alert(message);
	}


	private void agregarCamposActualizacion(){
		
		if (!isDerecho){
			this.insert(fieldManagerDerecho, indexDerecho);
			isDerecho = true;
		}
		
		this.invalidate();
	}
	
}
