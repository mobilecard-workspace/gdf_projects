package com.addcel.gdf.view.consulta.listas.aaa;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import org.json.me.JSONObject;

import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.consulta.listas.agua.dto.ListaAgua;
import com.addcel.gdf.view.consulta.listas.infracciones.ViewPagoInfracciones;
import com.addcel.gdf.view.consulta.listas.infracciones.dto.ListaInfracciones;
import com.addcel.gdf.view.consulta.listas.nomina.dto.ListaNomina;
import com.addcel.gdf.view.consulta.listas.predial.dto.ListaPredial;
import com.addcel.gdf.view.util.UtilColor;


public class CustomGridFieldInfracciones extends VerticalFieldManager implements Viewable {

	private ListaInfracciones listaInfracciones;
	
	public CustomGridFieldInfracciones(ListaInfracciones listaInfracciones) {
		super();
		
		this.listaInfracciones = listaInfracciones;
		
		add(new ListaInfraccionesRichTextField(listaInfracciones, ListaInfraccionesRichTextField.DATE_FECHA));
		add(new ListaInfraccionesRichTextField(listaInfracciones, ListaInfraccionesRichTextField.DATE_INFRACCION));

		add(new NullField());
	}


	public boolean setData() {
		return true;
	}


	protected void paint(Graphics graphics){

		graphics.clear();
		
		if (isFocus()){
			graphics.setColor(UtilColor.LIST_FOCUS);
			graphics.setGlobalAlpha(180);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
			graphics.setGlobalAlpha(255);
		} else {
			graphics.setColor(UtilColor.LIST_UNSELECTED);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
		}
		
		super.paint(graphics);
	}	
	
	
	
	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		showView();
		return super.navigationClick(status, time);
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			showView();
			return true;
		}

		return super.keyChar(character, status, time);
	}

	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK){
			fieldChangeNotify(0);
			showView();
			return true;
		}

		return super.touchEvent(message);
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}


	private void showView(){
		
		
		UiApplication.getUiApplication().pushScreen(new ViewPagoInfracciones("Resumen de Infracciones", this.listaInfracciones));
		
	}


	public void setData(int request, JSONObject jsObject) {
	}


	public void sendMessage(String message) {
	}
}