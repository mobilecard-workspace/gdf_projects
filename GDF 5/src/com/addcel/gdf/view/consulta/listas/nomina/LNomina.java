package com.addcel.gdf.view.consulta.listas.nomina;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.consulta.listas.common.CustomGridField;
import com.addcel.gdf.view.consulta.listas.nomina.dto.ListaNomina;

public class LNomina extends CustomMainScreen{


	public LNomina(String title, JSONObject jsObject) {
		super(title, true);
		
		try {
			JSONArray jsonArray = jsObject.getJSONArray("consultaNomina");
			
			
			int length = jsonArray.length();
			
			for(int index = 0; index < length; index++){
				
				JSONObject object = (JSONObject) jsonArray.get(index);
				
				ListaNomina listaNomina = new ListaNomina();

				listaNomina.setClave(object.optString("clave"));
				listaNomina.setRfc(object.optString("rfc"));
				listaNomina.setMes_pago(object.optString("mes_pago"));
				listaNomina.setAnio_pago(object.optString("anio_pago"));
				listaNomina.setRemuneraciones(object.optString("remuneraciones"));
				listaNomina.setImpuesto(object.optString("impuesto"));
				listaNomina.setImpuesto_actualizado(object.optString("impuesto_actualizado"));
				listaNomina.setRecargos(object.optString("recargos"));
				listaNomina.setRecargos_condonado(object.optString("recargos_condonado"));
				listaNomina.setInteres(object.optString("interes"));
				listaNomina.setTotal(object.optString("total"));
				listaNomina.setVigencia(object.optString("vigencia"));
				listaNomina.setLinea_captura(object.optString("linea_captura"));
				listaNomina.setLineacapturaCB(object.optString("lineacapturaCB"));
				listaNomina.setTipo_declaracion(object.optString("tipo_declaracion"));
				listaNomina.setNum_trabajadores(object.optString("num_trabajadores"));
				listaNomina.setId_bitacora(object.optString("id_bitacora"));
				listaNomina.setId_usuario(object.optString("id_usuario"));
				listaNomina.setId_producto(object.optString("id_producto"));
				listaNomina.setNo_autorizacion(object.optString("no_autorizacion"));
				listaNomina.setFechaPago(object.optString("fechaPago"));
				listaNomina.setStatusPago(object.optString("statusPago"));
				listaNomina.setError(object.optString("error"));
				listaNomina.setTotalPago(object.optString("totalPago"));
				CustomGridField fieldManager = new CustomGridField(listaNomina, DTO.STATUS_NOMINA);
				
				add(fieldManager);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
	}
}
