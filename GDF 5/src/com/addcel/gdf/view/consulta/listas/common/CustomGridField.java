package com.addcel.gdf.view.consulta.listas.common;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.view.consulta.listas.agua.ViewPagoAgua;
import com.addcel.gdf.view.consulta.listas.agua.dto.ListaAgua;
import com.addcel.gdf.view.consulta.listas.infracciones.ViewPagoInfracciones;
import com.addcel.gdf.view.consulta.listas.infracciones.dto.ListaInfracciones;
import com.addcel.gdf.view.consulta.listas.nomina.ViewPagoNomina;
import com.addcel.gdf.view.consulta.listas.nomina.dto.ListaNomina;
import com.addcel.gdf.view.consulta.listas.predial.ViewPagoPredial;
import com.addcel.gdf.view.consulta.listas.predial.dto.ListaPredial;
import com.addcel.gdf.view.consulta.listas.tenencia.ViewPagoTenencia;
import com.addcel.gdf.view.util.UtilColor;


public class CustomGridField extends VerticalFieldManager {

	private Adapter adapter;
	private int servicio;
	
	public CustomGridField(Adapter adapter, int servicio) {

		super();

		this.adapter = adapter;
		this.servicio = servicio;
		
		add(new ObjectRichTextField(adapter, ObjectRichTextField.FECHA));
		add(new ObjectRichTextField(adapter, ObjectRichTextField.LC));
		add(new ObjectRichTextField(adapter, ObjectRichTextField.CONCEPTO));
		add(new ObjectRichTextField(adapter, ObjectRichTextField.IMPORTE));
		
		add(new NullField());
	}


	public boolean setData() {
		return true;
	}


	protected void paint(Graphics graphics){

		graphics.clear();
		
		if (isFocus()){
			graphics.setColor(UtilColor.LIST_FOCUS);
			graphics.setGlobalAlpha(180);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
			graphics.setGlobalAlpha(255);
		} else {
			graphics.setColor(UtilColor.LIST_UNSELECTED);
			graphics.fillRoundRect(0, 0, getPreferredWidth(), getPreferredHeight(), 1, 1);
		}
		
		super.paint(graphics);
	}	
	
	
	
	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		showView();
		return super.navigationClick(status, time);
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			showView();
			return true;
		}

		return super.keyChar(character, status, time);
	}

	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK){
			fieldChangeNotify(0);
			showView();
			return true;
		}

		return super.touchEvent(message);
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}

	private void showView(){
		
		switch (servicio) {
		case DTO.STATUS_NOMINA:
			UiApplication.getUiApplication().pushScreen(new ViewPagoNomina("Resumen", (ListaNomina)this.adapter));
			break;
		case DTO.STATUS_TENENCIA:
			UiApplication.getUiApplication().pushScreen(new ViewPagoTenencia("Resumen de Tenencia", this.adapter));
			break;
		case DTO.STATUS_INFRACCION:
			UiApplication.getUiApplication().pushScreen(new ViewPagoInfracciones("Resumen de Infracciones", (ListaInfracciones)this.adapter));
			break;
		case DTO.STATUS_PREDIAL:
			UiApplication.getUiApplication().pushScreen(new ViewPagoPredial("Resumen de Pago Predial", (ListaPredial)this.adapter));
			break;
		case DTO.STATUS_AGUA:
			UiApplication.getUiApplication().pushScreen(new ViewPagoAgua("Resumen", (ListaAgua)this.adapter));
			break;
		}
	}
}