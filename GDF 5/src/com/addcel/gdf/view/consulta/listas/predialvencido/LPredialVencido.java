package com.addcel.gdf.view.consulta.listas.predialvencido;

import java.util.Vector;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedSizeButton;
import com.addcel.gdf.view.consulta.listas.common.CustomGridFieldSelect;
import com.addcel.gdf.view.consulta.listas.predialvencido.dto.PredialVencido;
import com.addcel.gdf.view.util.UtilNumber;

public class LPredialVencido extends CustomMainScreen implements FieldChangeListener, Viewable {

	private VerticalFieldManager fieldManagerElement;
	
	private CustomSelectedSizeButton pagar;
	private CustomSelectedSizeButton cancelar;
	
	private JSONObject jsListaPredialVencidos;
	private JSONObject jsListaPredialVencidosMandar;
	
	private String token;
	private String cuenta;
	
	private double montoMaximo;
	
	
	public LPredialVencido(String title, JSONObject jsListaPredialVencidos) {
		
		super(title, false);
		
		LabelField textInfo = new LabelField("Selecciona los periodos a pagar.", LabelField.NON_FOCUSABLE|LabelField.FIELD_HCENTER);
		
		add(textInfo);
		
		this.jsListaPredialVencidos = jsListaPredialVencidos;

		String sMontoMaximo = jsListaPredialVencidos.optString("monto_maximo_operacion");

		montoMaximo = Double.parseDouble(sMontoMaximo);

		this.cuenta = jsListaPredialVencidos.optString("cuenta");

		pagar = new CustomSelectedSizeButton("Pagar", 2);
		cancelar = new CustomSelectedSizeButton("Cancelar", 2);
		
		pagar.setChangeListener(this);
		cancelar.setChangeListener(this);
		
		CustomButtonFieldManager buttonFieldManager = new CustomButtonFieldManager(pagar, cancelar);
		buttonFieldManager.add(pagar);
		buttonFieldManager.add(cancelar);
		
		try {

			JSONArray jsonArray = jsListaPredialVencidos.getJSONArray("detalle");

			int length = jsonArray.length();

			fieldManagerElement = new VerticalFieldManager();
			
			for(int index = 0; index < length; index++){

				JSONObject onbject = (JSONObject) jsonArray.get(index);

				PredialVencido predialVencido = new PredialVencido();

				predialVencido.setActualizacion(onbject.optString("actualizacion"));
				predialVencido.setCond_Gast_Ejec(onbject.optString("cond_Gast_Ejec"));
				predialVencido.setCond_mult_omi(onbject.optString("cond_mult_omi"));
				predialVencido.setCond_rec(onbject.optString("cond_rec"));
				predialVencido.setMulta_omision(onbject.optString("multa_omision"));
				predialVencido.setRecargos(onbject.optString("recargos"));
				predialVencido.setSubtotal(UtilNumber.formatCurrency(onbject.optString("subtotal")));
				predialVencido.setSubtotal2(onbject.optString("subtotal"));
				predialVencido.setPeriodo(onbject.optString("periodo"));
				predialVencido.setRequerido(onbject.optString("requerido"));
				predialVencido.setImpuesto_bimestral(UtilNumber.formatCurrency(onbject.optString("impuesto_bimestral")));
				predialVencido.setImpuesto_bimestral2(onbject.optString("impuesto_bimestral"));
				CustomGridFieldSelect fieldManager = new CustomGridFieldSelect(predialVencido, DTO.STATUS_PREDIAL);

				fieldManagerElement.add(fieldManager);
			}

			add(fieldManagerElement);
			add(buttonFieldManager);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		addMenuItem(mPagar);
		addMenuItem(mCancelar);
	}

	private MenuItem mPagar = new MenuItem("Pagar", 110, 10) {
		public void run() {
			pagar();
		}
	};
	
	private MenuItem mCancelar = new MenuItem("Cancelar", 110, 10) {
		public void run() {
			Screen screen = UiApplication.getUiApplication().getActiveScreen();
			UiApplication.getUiApplication().popScreen(screen);
		}
	};
	
	
	public void fieldChanged(Field field, int context) {

		if (field == cancelar){
			UiApplication.getUiApplication().popScreen(this);
		} else if (field == pagar){
			
			pagar();
		}
	}
	
	private void pagar(){
		
		UtilPredialVencido util = new UtilPredialVencido();
		
		Vector elements = util.getPredialVencidos(fieldManagerElement);

		if (elements.size() > 0){
			
			if (util.sumaImpuestoBimestral > montoMaximo){
				
				String monto = String.valueOf(montoMaximo);
				
				String value = UtilNumber.formatCurrency(monto);
				
				Dialog.alert("Debes acotar tu selecci�n para no exceder el l�mite de pago que es de " +  value);
			} else {
				util.getPeriodosSeleccionados(this, elements, cuenta);
			}
			
		} else {
			
			Dialog.alert("No hay periodos seleccionados.");
		}
		

	}
	
	

	public void setData(int request, JSONObject jsObject) {

		System.out.println(jsObject.toString());
		
		if (jsObject.has("gastos_ejecucion")){
			
			LPredialVencidoResumen lPredialVencidoResumen = new LPredialVencidoResumen("Pago de predial vencido", jsObject);
			
			UiApplication.getUiApplication().popScreen(this);
			UiApplication.getUiApplication().pushScreen(lPredialVencidoResumen);
			
			
		} else if (jsObject.has("numError")){
			
			int numError = jsObject.optInt("numError", -1);
			
			if (numError != 0){
				
				String error = jsObject.optString("error");
				
				Dialog.alert(error);
			}
		} else if (jsObject.has("idError")){
			
			int idError = jsObject.optInt("idError", -1);
			
			if (idError != 0) {
				
				String error = jsObject.optString("mensajeError");
				
				Dialog.alert(error);
			}
		}
	}


	public void sendMessage(String message) {
		
	}
}
