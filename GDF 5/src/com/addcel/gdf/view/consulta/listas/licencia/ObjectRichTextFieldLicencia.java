package com.addcel.gdf.view.consulta.listas.licencia;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

import com.addcel.gdf.view.consulta.listas.licencia.dto.Licencia;
import com.addcel.gdf.view.util.UtilColor;

public class ObjectRichTextFieldLicencia extends RichTextField {


	final public static int RFC = 0;
	final public static int IMPORTE_FINAL = 1;
	final public static int LINEA_CAPTURA = 2;
	final public static int TOTAL_PAGO = 3;
	
	final private String rfc = "RFC: ";
	final private String importeFinal = "Importe Final: ";
	final private String linea_captura = "LC: ";
	final private String totalPago = "Total pago: ";
	
	private String title = "";
	private String data = "";
	
	private int format;
	private int size;
	
	public ObjectRichTextFieldLicencia(Licencia adapter, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		format = (Display.getWidth()*7) / 20; 
		size = Display.getWidth();

		switch (type) {
		
		case ObjectRichTextFieldLicencia.RFC:
			title = rfc;
			data = adapter.getRfc();
			break;
		case ObjectRichTextFieldLicencia.IMPORTE_FINAL:
			title = importeFinal;
			data = adapter.getImporteFinal();
			break;
		case ObjectRichTextFieldLicencia.LINEA_CAPTURA:
			title = linea_captura;
			data = adapter.getLinea_captura();
			break;
		case ObjectRichTextFieldLicencia.TOTAL_PAGO:
			title = totalPago;
			data = adapter.getTotalPago();
			break;
		}
	}

	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
		graphics.setColor(UtilColor.LIST_DATA);
		graphics.drawText(data, format, 0, DrawStyle.LEFT, size);

		super.paint(graphics);
	}
}
