package com.addcel.gdf.view.consulta.listas.predialpago.dto;

import org.json.me.JSONObject;

import com.addcel.gdf.view.util.UtilNumber;

public class Predial {

	private String bimestre;
	private String id_bitacora;
	private String banco;
	private String importe;
	private String id_producto;
	private String id_usuario;
	private String intImpuesto;
	private String error_descripcion;
	private String statusPago;
	private String totalPago;
	private String concepto;
	private String linea_captura;
	private String error_cel;
	private String cuentaP;
	private String reduccion;
	private String vencimiento;
	
	private JSONObject json;
	
	public String getBimestre() {
		return bimestre;
	}
	public void setBimestre(String bimestre) {
		this.bimestre = bimestre;
	}
	public String getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(String id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getId_producto() {
		return id_producto;
	}
	public void setId_producto(String id_producto) {
		this.id_producto = id_producto;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getIntImpuesto() {
		return intImpuesto;
	}
	public void setIntImpuesto(String intImpuesto) {
		this.intImpuesto = intImpuesto;
	}
	public String getError_descripcion() {
		return error_descripcion;
	}
	public void setError_descripcion(String error_descripcion) {
		this.error_descripcion = error_descripcion;
	}
	public String getStatusPago() {
		return statusPago;
	}
	public void setStatusPago(String statusPago) {
		this.statusPago = statusPago;
	}
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
	}
	public String getError_cel() {
		return error_cel;
	}
	public void setError_cel(String error_cel) {
		this.error_cel = error_cel;
	}
	public String getCuentaP() {
		return cuentaP;
	}
	public void setCuentaP(String cuentaP) {
		this.cuentaP = cuentaP;
	}
	public String getReduccion() {
		return reduccion;
	}
	public void setReduccion(String reduccion) {
		this.reduccion = reduccion;
	}
	public String getVencimiento() {
		return vencimiento;
	}
	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}
	
	public String toString(){
		
		return bimestre + " - " + UtilNumber.formatCurrency(totalPago);
	}
	public JSONObject getJson() {
		return json;
	}
	public void setJson(JSONObject json) {
		this.json = json;
	}
}
