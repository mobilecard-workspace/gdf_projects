package com.addcel.gdf.view.consulta.listas.common;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.view.util.UtilColor;
import com.addcel.gdf.view.util.UtilNumber;

public class ObjectRichTextField extends RichTextField {


	final public static int FECHA = 0;
	final public static int LC = 1;
	final public static int CONCEPTO = 2;
	final public static int IMPORTE = 3;
	
	/*
	   a)Fecha				fechaPago
	   b)Linea de Captura	linea_captura
	   c)Concepto			id_producto
	   d)Importe			totalPago
	*/
	
	final private String fecha = "Fecha: ";
	final private String lc = "LC: ";
	final private String concepto = "Concepto: ";
	final private String importe = "Importe: ";
	
	private String title = "";
	private String data = "";
	
	private int format;
	private int size;
	
	public ObjectRichTextField(Adapter adapter, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		format = (Display.getWidth()*6) / 20; 
		size = Display.getWidth();
		
		switch (type) {
		case ObjectRichTextField.FECHA:
			title = fecha;
			data = adapter.getFechaPago();
			break;
		case ObjectRichTextField.LC:
			title = lc;
			data = adapter.getLinea_captura();
			break;
		case ObjectRichTextField.CONCEPTO:
			title = concepto;
			String idProducto = adapter.getId_producto();
			int id = Integer.valueOf(idProducto).intValue();
			
			switch (id) {
			case DTO.STATUS_NOMINA:
				data = "Nomina";
				break;
			case DTO.STATUS_TENENCIA:
				data = "Tenencia";
				break;
			case DTO.STATUS_INFRACCION:
				data = "Infraccion";
				break;
			case DTO.STATUS_PREDIAL:
				data = "Predial";
				break;
			case DTO.STATUS_AGUA:
				data = "Agua";
				break;
			}
			
			break;
		case ObjectRichTextField.IMPORTE:
			title = importe;
			data = UtilNumber.formatCurrency(adapter.getTotalPago());
			break;
		}
	}

	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
		graphics.setColor(UtilColor.LIST_DATA);
		graphics.drawText(data, format, 0, DrawStyle.LEFT, size);

		super.paint(graphics);
	}
}
