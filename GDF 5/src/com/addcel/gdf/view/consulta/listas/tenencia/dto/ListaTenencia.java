package com.addcel.gdf.view.consulta.listas.tenencia.dto;

import com.addcel.gdf.view.consulta.listas.common.Adapter;


public class ListaTenencia implements Adapter {

	private String placa;
	private String condonacion;
	private String interes;
	private String subsidio;
	private String ejercicio;
	private String modelo;
	private String total;
	private String linea_captura;
	private String vigencia;
	private String tipoServicio;
	private String cve_vehi;
	private String tenencia;
	private String derecho;
	private String tenActualizacion;
	private String tenRecargo;
	private String derActualizacion;
	private String derRecargo;
	private String derechos;
	private String actualizacion;
	private String recargos;
	private String tenSubsidio;
	private String id_bitacora;
	private String id_usuario;
	private String id_producto;
	private String no_autorizacion;
	private String fechaPago;
	private String statusPago;
	private String error;
	private String totalPago;
	
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
	}
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}
	
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getCondonacion() {
		return condonacion;
	}
	public void setCondonacion(String condonacion) {
		this.condonacion = condonacion;
	}
	public String getInteres() {
		return interes;
	}
	public void setInteres(String interes) {
		this.interes = interes;
	}
	public String getSubsidio() {
		return subsidio;
	}
	public void setSubsidio(String subsidio) {
		this.subsidio = subsidio;
	}
	public String getEjercicio() {
		return ejercicio;
	}
	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public String getCve_vehi() {
		return cve_vehi;
	}
	public void setCve_vehi(String cve_vehi) {
		this.cve_vehi = cve_vehi;
	}
	public String getTenencia() {
		return tenencia;
	}
	public void setTenencia(String tenencia) {
		this.tenencia = tenencia;
	}
	public String getDerecho() {
		return derecho;
	}
	public void setDerecho(String derecho) {
		this.derecho = derecho;
	}
	public String getTenActualizacion() {
		return tenActualizacion;
	}
	public void setTenActualizacion(String tenActualizacion) {
		this.tenActualizacion = tenActualizacion;
	}
	public String getTenRecargo() {
		return tenRecargo;
	}
	public void setTenRecargo(String tenRecargo) {
		this.tenRecargo = tenRecargo;
	}
	public String getDerActualizacion() {
		return derActualizacion;
	}
	public void setDerActualizacion(String derActualizacion) {
		this.derActualizacion = derActualizacion;
	}
	public String getDerRecargo() {
		return derRecargo;
	}
	public void setDerRecargo(String derRecargo) {
		this.derRecargo = derRecargo;
	}
	public String getDerechos() {
		return derechos;
	}
	public void setDerechos(String derechos) {
		this.derechos = derechos;
	}
	public String getActualizacion() {
		return actualizacion;
	}
	public void setActualizacion(String actualizacion) {
		this.actualizacion = actualizacion;
	}
	public String getRecargos() {
		return recargos;
	}
	public void setRecargos(String recargos) {
		this.recargos = recargos;
	}
	public String getTenSubsidio() {
		return tenSubsidio;
	}
	public void setTenSubsidio(String tenSubsidio) {
		this.tenSubsidio = tenSubsidio;
	}
	public String getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(String id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getId_producto() {
		return id_producto;
	}
	public void setId_producto(String id_producto) {
		this.id_producto = id_producto;
	}
	public String getNo_autorizacion() {
		return no_autorizacion;
	}
	public void setNo_autorizacion(String no_autorizacion) {
		this.no_autorizacion = no_autorizacion;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getStatusPago() {
		return statusPago;
	}
	public void setStatusPago(String statusPago) {
		this.statusPago = statusPago;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}
