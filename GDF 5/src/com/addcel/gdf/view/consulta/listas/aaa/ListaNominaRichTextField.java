package com.addcel.gdf.view.consulta.listas.aaa;

import com.addcel.gdf.view.consulta.listas.agua.dto.ListaAgua;
import com.addcel.gdf.view.consulta.listas.nomina.dto.ListaNomina;
import com.addcel.gdf.view.util.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;


public class ListaNominaRichTextField extends RichTextField {


	final public static int DATE_RFC = 0;
	final public static int DATE_MES = 1;
	final public static int DATE_ANIO = 2;
	final public static int DATE_REMUNERACIONES = 3;
	final public static int DATE_TRABAJADORES = 4;
	
	
	final private String rfc = "RFC: ";
	final private String mes = "Mes: ";
	final private String anio = "A�o: ";
	final private String remuneraciones = "Remuneraciones: ";
	final private String numTrabajadores = "Num. trabajadores: ";

	
	private String title = "";
	private String data = "";
	private ListaNomina listaNomina;

	
	private int format;
	private int size;
	
	public ListaNominaRichTextField(ListaNomina listaNomina, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		
		format = (Display.getWidth()*9) / 20; 
		size = Display.getWidth();
		this.listaNomina = listaNomina;
		
		switch (type) {
		case ListaNominaRichTextField.DATE_RFC:
			title = rfc;
			data = listaNomina.getRfc();
			break;
		case ListaNominaRichTextField.DATE_MES:
			title = mes;
			data = listaNomina.getMes_pago();
			break;
		case ListaNominaRichTextField.DATE_ANIO:
			title = anio;
			data = listaNomina.getAnio_pago();
			break;
		case ListaNominaRichTextField.DATE_REMUNERACIONES:
			title = remuneraciones;
			data = listaNomina.getRemuneraciones();
			break;
		case ListaNominaRichTextField.DATE_TRABAJADORES:
			title = numTrabajadores;
			data = listaNomina.getNum_trabajadores();
			break;
		}
	}

	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
		graphics.setColor(UtilColor.LIST_DATA);
		graphics.drawText(data, format, 0, DrawStyle.LEFT, size);

		super.paint(graphics);
	}
}
