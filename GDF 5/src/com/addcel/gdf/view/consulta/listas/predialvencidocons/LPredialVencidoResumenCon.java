package com.addcel.gdf.view.consulta.listas.predialvencidocons;

import java.util.Vector;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.container.VerticalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedSizeButton;
import com.addcel.gdf.view.consulta.listas.predialvencidocons.dto.PredialVencidoDetalle;
import com.addcel.gdf.view.consulta.listas.predialvencidocons.dto.PredialVencidoMaestro;

public class LPredialVencidoResumenCon extends CustomMainScreen implements FieldChangeListener, Viewable {

	private VerticalFieldManager fieldManagerElement;
	
	private CustomSelectedSizeButton reenviar;
	private CustomSelectedSizeButton cancelar;
	
	private String token;
	private JSONObject jsonObject;
	
	private SplashScreen splashScreen;
	private PredialVencidoMaestro predialMaestro;
	
	public LPredialVencidoResumenCon(String title, PredialVencidoMaestro predialMaestro, JSONObject jsonObject) {

		super(title, false);

		this.jsonObject = jsonObject;
		this.predialMaestro = predialMaestro;
		
		splashScreen = SplashScreen.getInstance();

		reenviar = new CustomSelectedSizeButton("Reenviar", 2);
		cancelar = new CustomSelectedSizeButton("Cancelar", 2);

		reenviar.setChangeListener(this);
		cancelar.setChangeListener(this);

		CustomButtonFieldManager buttonFieldManager = new CustomButtonFieldManager(reenviar, cancelar);
		buttonFieldManager.add(reenviar);
		buttonFieldManager.add(cancelar);

		CustomGridFieldTotal customGridFieldTotal = new CustomGridFieldTotal(predialMaestro, 0);
		add(customGridFieldTotal);

		Vector elements = predialMaestro.getPredialesVencidos();

		int size = elements.size();

		fieldManagerElement = new VerticalFieldManager();

		for (int index = 0; index < size; index++) {

			PredialVencidoDetalle predialVencidoDetalle = (PredialVencidoDetalle) elements.elementAt(index);

			CustomGridFieldResumen fieldManager = new CustomGridFieldResumen(predialVencidoDetalle, DTO.STATUS_PREDIAL);

			fieldManagerElement.add(fieldManager);
		}

		add(fieldManagerElement);
		add(buttonFieldManager);

		//addMenuItem(mPagar);
		//addMenuItem(mCancelar);
	}

	
	public void fieldChanged(Field field, int context) {

		if (field == cancelar){
			UiApplication.getUiApplication().popScreen(this);
		} else if (field == reenviar){

			BasicEditField fieldMail = new BasicEditField(BasicEditField.FILTER_EMAIL);
			 
			Dialog d = new Dialog("�Desea enviar la informaci�n a otra direcci�n de mail?, escribala:", new String[] {"Enviar a otra direcci�n", "Enviar", "Cancelar"}, new int[] {2,1,0}, 2, Bitmap.getPredefinedBitmap(Bitmap.QUESTION));
			d.add(fieldMail);
			 
			int result = d.doModal();
			 
			 if (result == 2) {
			 
				 sendComprobante(true, fieldMail.getText());

			} else if (result == 1) {
				 
				sendComprobante(false, fieldMail.getText());
			}
		}
	}
	
	private void sendComprobante(boolean value, String email){
		
		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("id_bitacora", predialMaestro.getId_bitacora());
			jsonObject.put("id_producto", DTO.STATUS_PREDIAL_VENCIDO);
			jsonObject.put("id_usuario", UserBean.idLogin);

			if (value){
				
				jsonObject.put("email", email);
			}
			
			String post = jsonObject.toString();

			String data = AddcelCrypto.encryptHard(post);

			splashScreen.start();
			
			GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_GDF_REENVIO, this);
			genericHTTP.run();
			
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessage("Error al recopilar la informacion");
		}
	}


	public void setData(int request, JSONObject jsObject) {
		
		splashScreen.remove();
		
		if (jsObject.has("error")){
			
			String mensaje = jsObject.optString("error");
			 
		    Dialog dialog = new Dialog(Dialog.D_OK, mensaje, Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		 
		    int i = dialog.doModal();
		 
		    if (i == Dialog.OK) {
		        UiApplication.getUiApplication().popScreen(this);
		    } 
		}
		
	}


	public void sendMessage(String message) {
		
	}
}
