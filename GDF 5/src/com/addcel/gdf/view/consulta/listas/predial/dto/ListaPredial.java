package com.addcel.gdf.view.consulta.listas.predial.dto;

import com.addcel.gdf.view.consulta.listas.common.Adapter;

public class ListaPredial implements Adapter{

	private String bimestre;
	private String concepto;
	private String cuentaP;
	private String intImpuesto;
	private String linea_captura;
	private String reduccion;
	private String vencimiento;
	private String id_bitacora;
	private String id_usuario;
	private String id_producto;
	private String no_autorizacion;
	private String fechaPago;
	private String statusPago;
	private String total;
	private String totalPago;
	
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
	}
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}
	public String getBimestre() {
		return bimestre;
	}
	public void setBimestre(String bimestre) {
		this.bimestre = bimestre;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getCuentaP() {
		return cuentaP;
	}
	public void setCuentaP(String cuentaP) {
		this.cuentaP = cuentaP;
	}
	public String getIntImpuesto() {
		return intImpuesto;
	}
	public void setIntImpuesto(String intImpuesto) {
		this.intImpuesto = intImpuesto;
	}
	public String getReduccion() {
		return reduccion;
	}
	public void setReduccion(String reduccion) {
		this.reduccion = reduccion;
	}
	public String getVencimiento() {
		return vencimiento;
	}
	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}
	public String getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(String id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getId_producto() {
		return id_producto;
	}
	public void setId_producto(String id_producto) {
		this.id_producto = id_producto;
	}
	public String getNo_autorizacion() {
		return no_autorizacion;
	}
	public void setNo_autorizacion(String no_autorizacion) {
		this.no_autorizacion = no_autorizacion;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getStatusPago() {
		return statusPago;
	}
	public void setStatusPago(String statusPago) {
		this.statusPago = statusPago;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
}
