package com.addcel.gdf.view.consulta.listas.predialvencidocons;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

import com.addcel.gdf.view.consulta.listas.common.ObjectRichTextFieldTotal;
import com.addcel.gdf.view.consulta.listas.predialvencido.dto.PredialVencidoTotal;
import com.addcel.gdf.view.consulta.listas.predialvencidocons.dto.PredialVencidoMaestro;
import com.addcel.gdf.view.util.UtilColor;

public class ObjectTextFieldMaestro extends RichTextField {

	final public static int IMPUESTO_EMITIDO = 0;
	final public static int IMPUESTO_PAGADO = 1;
	final public static int ACTUALIZACION = 2;
	final public static int GASTOS_EJECUCION = 3;
	final public static int GASTOS_EJECUCION_CONDONADOS = 4;
	final public static int RECARGOS = 5;
	final public static int MULTA = 6;
	final public static int MULTA_CONDONADA = 7;
	final public static int CAPTURA = 8;
	final public static int TOTAL = 9;
	
	final private String impuestoEmitido = "Impuesto emitido: ";
	final private String impuestoPagado = "Impuesto pagado: ";
	final private String actualizacion = "Actualizacion: ";
	final private String gastosEjecucion = "Gastos Ejecucion: ";
	final private String gastosEjecucionCondonados = "Gastos de Ejecucion Condonados: ";
	final private String recargos = "Recargos: ";
	final private String multa = "Multa: ";
	final private String multaCondonada = "Multa Condonada: ";
	final private String captura = "Captura: ";
	final private String total = "Total: ";
	
	private String title = "";
	private String data = "";
	
	private int format;
	private int size;
	
	private int alignment;


	public ObjectTextFieldMaestro(PredialVencidoMaestro adapter, int type) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);
		
		format = Display.getWidth() / 2; 
		size = Display.getWidth();
		
		switch (type) {
		case ObjectRichTextFieldTotal.IMPUESTO_EMITIDO:
			title = impuestoEmitido;
			data = adapter.getImpuestoEmitido();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.IMPUESTO_PAGADO:
			title = impuestoPagado;
			data = adapter.getImpuestoPagado();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.ACTUALIZACION:
			title = actualizacion;
			data = adapter.getSumActualizacion();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.GASTOS_EJECUCION:
			title = gastosEjecucion;
			data = adapter.getGastos_ejecucion();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.GASTOS_EJECUCION_CONDONADOS:
			title = gastosEjecucionCondonados;
			data = adapter.getGastos_ejecucion();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.RECARGOS:
			title = recargos;
			data = adapter.getSumRecargos();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.MULTA:
			title = multa;
			data = adapter.getSumMultas();
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.MULTA_CONDONADA:
			title = multaCondonada;
			data = "$ 0.00";
			alignment = DrawStyle.RIGHT;
			break;
		case ObjectRichTextFieldTotal.CAPTURA:
			title = captura;
			data = adapter.getLinea_captura();
			alignment = DrawStyle.LEFT;
			break;
		case ObjectRichTextFieldTotal.TOTAL:
			title = total;
			data = adapter.getTotalPago();
			alignment = DrawStyle.RIGHT;
			break;
		}
	}

	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, format);
		graphics.setColor(UtilColor.LIST_DATA);
		graphics.drawText(data, format, 0, alignment, size - format);

		super.paint(graphics);
	}
}

