package com.addcel.gdf.view.consulta.listas.infracciones.dto;

import com.addcel.gdf.view.consulta.listas.common.Adapter;

public class ListaInfracciones implements Adapter{


	private String placa;
	private String folio;
	private String linea_captura;
	private String importe;
	private String fechainfraccion;
	private String actualizacion;
	private String recargos;
	private String dias_multa;
	private String codeCalc;
	private String id_bitacora;
	private String id_usuario;
	private String id_producto;
	private String no_autorizacion;
	private String fechaPago;
	private String statusPago;
	private String error;
	private String totalPago;
	
	
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
	}
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}
	
	
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getFechainfraccion() {
		return fechainfraccion;
	}
	public void setFechainfraccion(String fechainfraccion) {
		this.fechainfraccion = fechainfraccion;
	}
	public String getActualizacion() {
		return actualizacion;
	}
	public void setActualizacion(String actualizacion) {
		this.actualizacion = actualizacion;
	}
	public String getRecargos() {
		return recargos;
	}
	public void setRecargos(String recargos) {
		this.recargos = recargos;
	}
	public String getDias_multa() {
		return dias_multa;
	}
	public void setDias_multa(String dias_multa) {
		this.dias_multa = dias_multa;
	}
	public String getCodeCalc() {
		return codeCalc;
	}
	public void setCodeCalc(String codeCalc) {
		this.codeCalc = codeCalc;
	}
	public String getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(String id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getId_producto() {
		return id_producto;
	}
	public void setId_producto(String id_producto) {
		this.id_producto = id_producto;
	}
	public String getNo_autorizacion() {
		return no_autorizacion;
	}
	public void setNo_autorizacion(String no_autorizacion) {
		this.no_autorizacion = no_autorizacion;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getStatusPago() {
		return statusPago;
	}
	public void setStatusPago(String statusPago) {
		this.statusPago = statusPago;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}
