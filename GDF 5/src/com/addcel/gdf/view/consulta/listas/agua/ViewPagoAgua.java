package com.addcel.gdf.view.consulta.listas.agua;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.listas.agua.dto.ListaAgua;
import com.addcel.gdf.view.util.UtilNumber;

public class ViewPagoAgua extends CustomMainScreen implements FieldChangeListener, Viewable {

	private LabelField textBimestre = null;
	private LabelField textAnioBimestre = null;
	private LabelField textCuentaAgua = null;
	private LabelField textIVA = null;
	private LabelField textTotal = null;
	private LabelField textLCaptura = null;

	private ColorEditField editAgua = null;
	private HighlightsLabelField editBimestre = null;
	private HighlightsLabelField editAnioBimestre = null;
	private HighlightsLabelField editCuentaAgua = null;
	private HighlightsLabelField editIVA = null;
	private HighlightsLabelField editTotal = null;
	private HighlightsLabelField editLCaptura = null;
	
	private SlimSelectedButtonField consultar = null;
	private SlimSelectedButtonField salir = null;
	
	private SplashScreen splashScreen;

	private String sCuentaAgua = null;
	
	private ListaAgua listaAgua;

	public ViewPagoAgua(String title, ListaAgua listaAgua) {
		
		super(title, true);

		this.listaAgua = listaAgua;
		
		splashScreen = SplashScreen.getInstance();

		textBimestre = new ColorLabelField("Bimestre: ", LabelField.NON_FOCUSABLE) ;
		textAnioBimestre = new ColorLabelField("A�o del bimestre: ", LabelField.NON_FOCUSABLE) ;
		textCuentaAgua = new ColorLabelField("Cuenta agua: ", LabelField.NON_FOCUSABLE) ;
		textIVA = new ColorLabelField("Iva: ", LabelField.NON_FOCUSABLE) ;
		textTotal = new ColorLabelField("Total: ", LabelField.NON_FOCUSABLE) ;
		textLCaptura = new ColorLabelField("Linea de captura: ", LabelField.NON_FOCUSABLE) ;

		editAgua = new ColorEditField("", "1234567890123456");
		editBimestre = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editAnioBimestre =  new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editCuentaAgua =  new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editIVA =  new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTotal =  new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editLCaptura =  new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		

		consultar =  new SlimSelectedButtonField("Reenviar recibo", ButtonField.CONSUME_CLICK);
		consultar.setChangeListener(this);
		
		salir =  new SlimSelectedButtonField("Cancelar", ButtonField.CONSUME_CLICK);
		salir.setChangeListener(this);

		editBimestre.setText(listaAgua.getVbimestre());
		editAnioBimestre.setText(listaAgua.getVanio());
		editCuentaAgua.setText(listaAgua.getCuenta());
		editIVA.setText(UtilNumber.formatCurrency(listaAgua.getViva()));
		editTotal.setText(UtilNumber.formatCurrency(listaAgua.getTotalPago()));
		editLCaptura.setText(listaAgua.getLinea_captura());

		add(new NullField());
		
		add(new NullField());
		
		addHorizontalFiels(textBimestre, editBimestre);
		addHorizontalFiels(textAnioBimestre, editAnioBimestre);
		addHorizontalFiels(textCuentaAgua, editCuentaAgua);
		addHorizontalFiels(textIVA, editIVA);
		addHorizontalFiels(textTotal, editTotal);
		addHorizontalFiels(textLCaptura, editLCaptura);
		
		add(consultar);
		add(salir);
	}

	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}
	
	
	
	public void fieldChanged(Field field, int context) {

		if(field == consultar){
			
			BasicEditField fieldMail = new BasicEditField(BasicEditField.FILTER_EMAIL);
			 
			Dialog d = new Dialog("�Desea enviar la informaci�n a otra direcci�n de mail?, escribala:", new String[] {"Enviar a otra direcci�n", "Enviar", "Cancelar"}, new int[] {2,1,0}, 2, Bitmap.getPredefinedBitmap(Bitmap.QUESTION));
			d.add(fieldMail);
			 
			int result = d.doModal();
			 
			 if (result == 2) {
			 
				 sendComprobante(true, fieldMail.getText());

			} else if (result == 1) {
				 
				sendComprobante(false, fieldMail.getText());
			}
		} else if(field == salir){
			
			UiApplication.getUiApplication().popScreen(this);
		}
	}
	
	private void sendComprobante(boolean value, String email){
		
		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("id_bitacora", listaAgua.getId_bitacora());
			jsonObject.put("id_producto", DTO.STATUS_AGUA);
			jsonObject.put("id_usuario", UserBean.idLogin);

			if (value){
				
				jsonObject.put("email", email);
			}
			
			String post = jsonObject.toString();

			String data = AddcelCrypto.encryptHard(post);

			splashScreen.start();

			GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_GDF_REENVIO, this);
			genericHTTP.run();
			
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessage("Error al recopilar la informacion");
		}
	}
	

	public void setData(int request, JSONObject jsObject){

		splashScreen.remove();
		
		if (jsObject.has("error")){
			
			String mensaje = jsObject.optString("error");
			 
		    Dialog dialog = new Dialog(Dialog.D_OK, mensaje, Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		 
		    int i = dialog.doModal();
		 
		    if (i == Dialog.OK) {
		        UiApplication.getUiApplication().popScreen(this);
		    } 
		}
	}
	
	
	
	
	
	
	


	public void sendMessage(String message) {
		splashScreen.remove();
		Dialog.alert(message);
	}

	/*
	
	public void fieldChanged(Field field, int context) {

		if(field == consultar){
			
			JSONObject jsonObject = new JSONObject();

			try {

				jsonObject.put("id_bitacora", listaAgua.getId_bitacora());
				jsonObject.put("id_producto", DTO.STATUS_AGUA);
				jsonObject.put("id_usuario", UserBean.idLogin);
				String post = jsonObject.toString();

				String data = AddcelCrypto.encryptHard(post);
				splashScreen.start();
				GenericHTTPHard genericHTTP = new GenericHTTPHard(data, Url.URL_GDF_REENVIO, this);
				genericHTTP.run();
				
			} catch (JSONException e) {
				e.printStackTrace();
				sendMessage("Error al recopilar la informacion");
			}
		}
	}
	
	
	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();
		
		if (jsObject.has("error")){
			
			String mensaje = jsObject.optString("error");
			sendMessage(mensaje);
		}
	}
	
	*/
}
