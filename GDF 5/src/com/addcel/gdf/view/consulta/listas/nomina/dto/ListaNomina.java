package com.addcel.gdf.view.consulta.listas.nomina.dto;

import com.addcel.gdf.view.consulta.listas.common.Adapter;

public class ListaNomina implements Adapter{

	private String clave;
	private String rfc;
	private String mes_pago;
	private String anio_pago;
	private String remuneraciones;
	private String impuesto;
	private String impuesto_actualizado;
	private String recargos;
	private String recargos_condonado;
	private String interes;
	private String Total;
	private String vigencia;
	private String linea_captura;
	private String lineacapturaCB;
	private String tipo_declaracion;
	private String num_trabajadores;
	private String id_bitacora;
	private String id_usuario;
	private String id_producto;
	private String no_autorizacion;
	
	private String fechaPago;
	private String statusPago;
	private String error;
	
	private String totalPago;
	
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
		
	}
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}
	
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getMes_pago() {
		return mes_pago;
	}
	public void setMes_pago(String mes_pago) {
		this.mes_pago = mes_pago;
	}
	public String getAnio_pago() {
		return anio_pago;
	}
	public void setAnio_pago(String anio_pago) {
		this.anio_pago = anio_pago;
	}
	public String getRemuneraciones() {
		return remuneraciones;
	}
	public void setRemuneraciones(String remuneraciones) {
		this.remuneraciones = remuneraciones;
	}
	public String getImpuesto() {
		return impuesto;
	}
	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}
	public String getImpuesto_actualizado() {
		return impuesto_actualizado;
	}
	public void setImpuesto_actualizado(String impuesto_actualizado) {
		this.impuesto_actualizado = impuesto_actualizado;
	}
	public String getRecargos() {
		return recargos;
	}
	public void setRecargos(String recargos) {
		this.recargos = recargos;
	}
	public String getRecargos_condonado() {
		return recargos_condonado;
	}
	public void setRecargos_condonado(String recargos_condonado) {
		this.recargos_condonado = recargos_condonado;
	}
	public String getInteres() {
		return interes;
	}
	public void setInteres(String interes) {
		this.interes = interes;
	}
	public String getTotal() {
		return Total;
	}
	public void setTotal(String total) {
		Total = total;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getLineacapturaCB() {
		return lineacapturaCB;
	}
	public void setLineacapturaCB(String lineacapturaCB) {
		this.lineacapturaCB = lineacapturaCB;
	}
	public String getTipo_declaracion() {
		return tipo_declaracion;
	}
	public void setTipo_declaracion(String tipo_declaracion) {
		this.tipo_declaracion = tipo_declaracion;
	}
	public String getNum_trabajadores() {
		return num_trabajadores;
	}
	public void setNum_trabajadores(String num_trabajadores) {
		this.num_trabajadores = num_trabajadores;
	}
	public String getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(String id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getId_producto() {
		return id_producto;
	}
	public void setId_producto(String id_producto) {
		this.id_producto = id_producto;
	}
	public String getNo_autorizacion() {
		return no_autorizacion;
	}
	public void setNo_autorizacion(String no_autorizacion) {
		this.no_autorizacion = no_autorizacion;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getStatusPago() {
		return statusPago;
	}
	public void setStatusPago(String statusPago) {
		this.statusPago = statusPago;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}
