package com.addcel.gdf.view.consulta.listas.agua.dto;

import com.addcel.gdf.view.consulta.listas.common.Adapter;

public class ListaAgua implements Adapter{


	private String linea_captura;
	private String vtotal;
	private String viva;
	private String vuso;
	private String vderndom;
	private String vderdom;
	private String vbimestre;
	private String vanio;
	private String cuenta;
	private String vigencia;
	private String id_bitacora;
	private String id_usuario;
	private String id_producto;
	private String no_autorizacion;
	private String fechaPago;
	private String statusPago;

	private String totalPago;
	
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
		
	}
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}
	
	public String getVtotal() {
		return vtotal;
	}
	public void setVtotal(String vtotal) {
		this.vtotal = vtotal;
	}
	public String getViva() {
		return viva;
	}
	public void setViva(String viva) {
		this.viva = viva;
	}
	public String getVuso() {
		return vuso;
	}
	public void setVuso(String vuso) {
		this.vuso = vuso;
	}
	public String getVderndom() {
		return vderndom;
	}
	public void setVderndom(String vderndom) {
		this.vderndom = vderndom;
	}
	public String getVderdom() {
		return vderdom;
	}
	public void setVderdom(String vderdom) {
		this.vderdom = vderdom;
	}
	public String getVbimestre() {
		return vbimestre;
	}
	public void setVbimestre(String vbimestre) {
		this.vbimestre = vbimestre;
	}
	public String getVanio() {
		return vanio;
	}
	public void setVanio(String vanio) {
		this.vanio = vanio;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(String id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getId_producto() {
		return id_producto;
	}
	public void setId_producto(String id_producto) {
		this.id_producto = id_producto;
	}
	public String getNo_autorizacion() {
		return no_autorizacion;
	}
	public void setNo_autorizacion(String no_autorizacion) {
		this.no_autorizacion = no_autorizacion;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getStatusPago() {
		return statusPago;
	}
	public void setStatusPago(String statusPago) {
		this.statusPago = statusPago;
	}

}
