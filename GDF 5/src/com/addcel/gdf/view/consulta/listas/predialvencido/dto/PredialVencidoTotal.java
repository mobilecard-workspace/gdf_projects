package com.addcel.gdf.view.consulta.listas.predialvencido.dto;

public class PredialVencidoTotal {

	private String impuestoEmitido;
	private String impuestoPagado;
	private String actualizacion;
	private String gastosEjecucion;
	private String gastosEjecucionCondonados;
	private String recargos;
	private String multa;
	private String multaCondonada;
	private String captura;
	private String total;
	
	public String getImpuestoEmitido() {
		return impuestoEmitido;
	}
	public void setImpuestoEmitido(String impuestoEmitido) {
		this.impuestoEmitido = impuestoEmitido;
	}
	public String getImpuestoPagado() {
		return impuestoPagado;
	}
	public void setImpuestoPagado(String impuestoPagado) {
		this.impuestoPagado = impuestoPagado;
	}
	public String getActualizacion() {
		return actualizacion;
	}
	public void setActualizacion(String actualizacion) {
		this.actualizacion = actualizacion;
	}
	public String getGastosEjecucion() {
		return gastosEjecucion;
	}
	public void setGastosEjecucion(String gastosEjecucion) {
		this.gastosEjecucion = gastosEjecucion;
	}
	public String getGastosEjecucionCondonados() {
		return gastosEjecucionCondonados;
	}
	public void setGastosEjecucionCondonados(String gastosEjecucionCondonados) {
		this.gastosEjecucionCondonados = gastosEjecucionCondonados;
	}
	public String getRecargos() {
		return recargos;
	}
	public void setRecargos(String recargos) {
		this.recargos = recargos;
	}
	public String getMulta() {
		return multa;
	}
	public void setMulta(String multa) {
		this.multa = multa;
	}
	public String getMultaCondonada() {
		return multaCondonada;
	}
	public void setMultaCondonada(String multaCondonada) {
		this.multaCondonada = multaCondonada;
	}
	public String getCaptura() {
		return captura;
	}
	public void setCaptura(String captura) {
		this.captura = captura;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	
}
