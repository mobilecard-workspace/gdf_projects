package com.addcel.gdf.view.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class UtilDate {

	
	public static String getDD_MM_AAAAtoDDMMAAAA(String date){
		
		/*
				DD/MM/AAAA a DDMMAAAA 
		*/
		
		String day = date.substring(0, 2);
		String month = date.substring(3, 5);
		String year = date.substring(6, 10);
		
		
		return day + month + year;
	}
	

	public static String getAAAAMMDDtoDD_MM_AAAA(String date){
		
		/*
				AAAAMMDD a DD/MM/AAAA 
		*/
		String year = date.substring(0, 4);
		String month = date.substring(4, 6);
		String day = date.substring(6, 8);
		
		return day + "/" + month + "/" + year;
	}
	
	
	
	
	public static String getDateToDDMMAAAA(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int iYear = calendar.get(Calendar.YEAR);
		int iMonth = calendar.get(Calendar.MONTH);
		int iDay = calendar.get(Calendar.DAY_OF_MONTH);

		String sYear = Integer.toString(iYear);
		String sMonth = Integer.toString(iMonth + 1);
		String sDay = Integer.toString(iDay);

		sMonth = (sMonth.length() == 1) ? "0" + sMonth : sMonth;
		sDay = (sDay.length() == 1) ? "0" + sDay : sDay;
		
		return sDay + sMonth + sYear;
	}
	
	
	public static String getDateToDD_MM_AAAA(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int iYear = calendar.get(Calendar.YEAR);
		int iMonth = calendar.get(Calendar.MONTH);
		int iDay = calendar.get(Calendar.DAY_OF_MONTH);

		String sYear = Integer.toString(iYear);
		String sMonth = Integer.toString(iMonth + 1);
		String sDay = Integer.toString(iDay);

		sMonth = (sMonth.length() == 1) ? "0" + sMonth : sMonth;
		sDay = (sDay.length() == 1) ? "0" + sDay : sDay;
		
		
		StringBuffer value = new StringBuffer();
		
		value.append(sDay).append("/").append(sMonth).append("/").append(sYear);
		
		return value.toString();
	}
	
	
	
	public static boolean after(Date depart, Date arrive){

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(depart);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		Date a = calendar.getTime();
		
		calendar.setTime(arrive);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date b = calendar.getTime();
		
		long la = a.getTime();
		long lb = b.getTime();
		
		boolean value = (la <= lb); 
		
		return value;
	}
	
	
	public static String[] getYears(int value){
		
		String[] years = null;
		Vector vector = new Vector();
		
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(new Date());
		
		int year = calendar.get(calendar.YEAR);
		
		if (value < 0){
			for (int index = year; index > (year + value); index--){
				
				vector.addElement(String.valueOf(index));
			}
		} else {
			for (int index = year; index < (year + value); index++){
				
				vector.addElement(String.valueOf(index));
			}
		}
		

		
		
		years = new String[vector.size()];
		
		vector.copyInto(years);
		
		return years;
	}
	
	
	public static String getYYYY_MM_DDFromLong(long longDate){
		
		/*
			YYYY-MM-DD from long
		*/
		
		StringBuffer value = new StringBuffer();
		String dash = "-";
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(new Date(longDate));
		
		int year = calendar.get(calendar.YEAR);
		int month = calendar.get(calendar.MONTH) + 1;
		int day = calendar.get(calendar.DATE);
		
		String strYear = String.valueOf(year);
		String strMonth = String.valueOf(month);
		String strDay = String.valueOf(day);

		
		
		
		if (month < 10) {
			strMonth = "0" + strMonth;
		}

		if (day < 10) {
			strDay = "0" + strDay;
		}

		
		value.append(strYear).append(dash).append(strMonth).append(dash).append(strDay); 
		
		
		return value.toString();
	}
	
}
