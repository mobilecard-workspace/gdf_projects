package com.addcel.gdf.view.util;


import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.component.BitmapField;

public class UtilIcon {

	public static BitmapField getTitle(){

		String resolution = String.valueOf(Display.getWidth())  + "logoSefin.png";
		Bitmap encodedImage = Bitmap.getBitmapResource(resolution);
		BitmapField bitmapField = new BitmapField(encodedImage);

		return bitmapField;
	}

	
	public static BitmapField getTitleInicio(){

		String resolution = String.valueOf(Display.getWidth())  + "logoSefinInicio.png";
		Bitmap encodedImage = Bitmap.getBitmapResource(resolution);
		BitmapField bitmapField = new BitmapField(encodedImage);

		return bitmapField;
	}
	
	
	public static BitmapField getSplash(){
		
		int width = Display.getWidth();
		int height = Display.getHeight();
		
		StringBuffer stringBuffer = new StringBuffer();
		
		stringBuffer.append("GDF_");
		stringBuffer.append(width).append("x").append(height).append(".png");
		
		String splash = stringBuffer.toString();
		
		Bitmap encodedImage = Bitmap.getBitmapResource(splash);
		BitmapField bitmapField = new BitmapField(encodedImage);

		
		return bitmapField;
	}
	
}
