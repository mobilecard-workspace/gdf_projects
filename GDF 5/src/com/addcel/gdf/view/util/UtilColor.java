package com.addcel.gdf.view.util;

import net.rim.device.api.ui.Color;

public class UtilColor {

	public final static int BUTTON_FOCUS = 0x0081c8;
	public final static int BUTTON_SELECTED = 0xa3a5a8;
	public final static int BUTTON_UNSELECTED = 0XA231f20;
	
	public final static int SLIM_BUTTON_FOCUS = 0x0081c8;
	public final static int SLIM_BUTTON_SELECTED = 0X77787b;
	public final static int SLIM_BUTTON_UNSELECTED = 0X77787b;
	
	public final static int BUTTON_STRING = Color.WHITE; 


	public final static int LABEL_FIELD_STRING = 0X434345;
	public final static int EDIT_FIELD_STRING = 0X434345;
	public final static int EDIT_FIELD_BACKGROUND = 0xd1d3d4;
	
	public final static int BACKGROUND = Color.WHITE;
	
	public final static int TITLE_BG_TOP_LEFT = 0x434345;
	public final static int TITLE_BG_TOP_RIGHT = 0x434345;
	public final static int TITLE_BG_BOTTOM_RIGHT = 0xa3a5a8;
	public final static int TITLE_BG_BOTTOM_LEFT = 0xa3a5a8;
	
	public final static int TITLE_YE_TOP_LEFT = 0xfff09d;
	public final static int TITLE_YE_TOP_RIGHT = 0xfff09d;
	public final static int TITLE_YE_BOTTOM_RIGHT = 0xfad605;
	public final static int TITLE_YE_BOTTOM_LEFT = 0xfad605;
	
	public final static int TITLE_IMAGE_TOP_LEFT = 0xd1d3d4;
	public final static int TITLE_IMAGE_TOP_RIGHT = 0xd1d3d4;
	public final static int TITLE_IMAGE_BOTTOM_RIGHT = 0xd1d3d4;
	public final static int TITLE_IMAGE_BOTTOM_LEFT = 0xa3a5a8;

	public final static int TITLE_STRING_GDF = 0x77787b;

/*
	title
	data
	
	background = 
	focus
*/
	
	public final static int LIST_TITLE = 0x0081c8;
	public final static int LIST_DATA = 0x4d4d4f;
	
	public final static int LIST_FOCUS = 0xfad605;
	public final static int LIST_SELECTED = 0Xff8080;
	public final static int LIST_UNSELECTED = 0Xd1d3d4;

	
	
	public static int toRGB(int r, int g, int b){
		return (r<<16)+(g<<8)+b;		 
	}

	public static int getBlack(){
		return toRGB(30, 30, 30);
	}

	public static int getDarkGray(){
		return toRGB(120, 120, 120);
	}

	public static int getLightGray(){
		return toRGB(200, 200, 200);
	}

	public static int getLightOrange(){
		return toRGB(255, 184, 101);
	}

	public static int getDarkOrange(){
		return toRGB(255, 159, 45);
	}
}
