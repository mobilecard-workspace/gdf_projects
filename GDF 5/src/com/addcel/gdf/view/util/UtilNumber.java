package com.addcel.gdf.view.util;

import javax.microedition.global.Formatter;

public class UtilNumber {	
	
	public static String formatCurrency(String dValue){

		String data = dValue;
		
		if ((dValue == null)||(dValue.equals(""))){
			
			dValue = "0.0";
		}
		
		double value = Double.parseDouble(dValue);
		
		Formatter formatter = new Formatter();
	
		return "$ " + formatter.formatNumber(value, 2);
	}
	
	
/*
	public static String formatCurrency(String dValue){

		String data = dValue;
		
		if ((dValue == null)||(dValue.equals(""))){
			
			dValue = "0.0";
		}
		
		
		if ((dValue != null)&&(!dValue.equals(""))){
			
			Formatter formatter = null;
			
			if (formatter == null){
				
				try{
					formatter = new Formatter("US");
				} catch (Exception e){
					
					e.printStackTrace();
				}

			}
			
			try {
				
				double number = Double.parseDouble(dValue); 
				
				//String formatted = formatter.formatCurrency(number, "MXN");
				String formatted = formatter.formatCurrency(number, "USD");
				
				int length = formatted.length();
				
				formatted = formatted.substring(0, length - 4);

				length = formatted.length();
				int indexPunto = formatted.indexOf(".");
				
				String parteEntera = formatted.substring(0, indexPunto);
				String parteDecimal = formatted.substring(indexPunto, length);
				
				int size = parteEntera.length();
				
				Vector buffer = new Vector();
				
				boolean inicial = true;
				
				while(parteEntera.length() > 3){
					
					String parteTemp = parteEntera.substring(size - 3);
					parteEntera = parteEntera.substring(0, size - 3);
					
					buffer.addElement(parteTemp);

					
					size = parteEntera.length();
				}
				
				
				buffer.addElement(parteEntera);
					
				
				int tamanio = buffer.size();
				
				StringBuffer temp = new StringBuffer();
				
				for(int index = tamanio - 1; index >= 0; index--){
					
					temp.append(buffer.elementAt(index)).append(",");
				}
				
				data = temp.toString().substring(0, temp.length()-1);
				
				data = data + parteDecimal;			

			} catch (Exception e){
				
			}
		}
		
		return "$ " + data; 
	}
	
	*/
}
