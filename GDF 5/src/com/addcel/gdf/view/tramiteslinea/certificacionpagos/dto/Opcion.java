package com.addcel.gdf.view.tramiteslinea.certificacionpagos.dto;

public class Opcion {

	private int index;
	private String value;

	public Opcion(String value, int index){
		
		this.value = value;
		this.index = index;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	public String toString(){
		return value;
	}
}
