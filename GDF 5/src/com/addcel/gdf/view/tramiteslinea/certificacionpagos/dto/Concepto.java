package com.addcel.gdf.view.tramiteslinea.certificacionpagos.dto;

public class Concepto {

	private int id_concepto;
	private String concepto;
	
	private Parametro[] parametros;
	
	public int getId_concepto() {
		return id_concepto;
	}
	public void setId_concepto(int id_concepto) {
		this.id_concepto = id_concepto;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	
	public Parametro[] getParametros() {
		return parametros;
	}
	public void setParametros(Parametro[] parametros) {
		this.parametros = parametros;
	}
	
	public String toString(){
		
		return concepto;
	}
}
