package com.addcel.gdf.view.tramiteslinea.certificacionpagos.model;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.view.tramiteslinea.certificacionpagos.dto.Concepto;
import com.addcel.gdf.view.tramiteslinea.certificacionpagos.dto.Parametro;

public class ConverterJsonToObject {

	public Concepto[] execute(String json) throws JSONException{
		
		JSONObject jsonObject = new JSONObject(json);
		
		JSONArray arrayConceptos = jsonObject.optJSONArray("conceptos"); 
		
		int length = arrayConceptos.length();
		
		Concepto[] conceptos = new Concepto[length];
		
		for(int index = 0; index < length; index++){
			
			JSONObject jsonConcepto = arrayConceptos.optJSONObject(index);

			Concepto concepto = new Concepto();

			concepto.setConcepto(jsonConcepto.optString("concepto"));
			concepto.setId_concepto(jsonConcepto.optInt("id_concepto"));

			conceptos[index] = concepto;

			JSONArray arrayParametros = jsonConcepto.optJSONArray("parametros");

			int length2 = arrayParametros.length();
			
			Parametro[] parametros = new Parametro[length2];
			concepto.setParametros(parametros);
			
			for(int index2 = 0; index2 < length2; index2++){
				
				JSONObject jsonParametro = arrayParametros.optJSONObject(index2);
				
				Parametro parametro = new Parametro();
				
				parametro.setNombre_param(jsonParametro.optString("nombre_param"));
				parametro.setTipo(jsonParametro.optString("tipo"));
				parametro.setLongitud_max(jsonParametro.optString("longitud_max"));
				
				parametros[index2] = parametro;
			}
		}
		
		return conceptos;
	}
}
