package com.addcel.gdf.view.tramiteslinea.certificacionpagos.dto;

public class Parametro {

	private String nombre_param;
	private String tipo;
	private String longitud_max;
	

	
	public String getNombre_param() {
		return nombre_param;
	}
	public void setNombre_param(String nombre_param) {
		this.nombre_param = nombre_param;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getLongitud_max() {
		return longitud_max;
	}
	public void setLongitud_max(String longitud_max) {
		this.longitud_max = longitud_max;
	}

}
