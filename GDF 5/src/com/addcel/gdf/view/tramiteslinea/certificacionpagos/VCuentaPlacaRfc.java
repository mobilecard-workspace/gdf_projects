package com.addcel.gdf.view.tramiteslinea.certificacionpagos;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTP;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.tramiteslinea.certificacionpagos.dto.Opcion;
import com.addcel.gdf.view.tramiteslinea.certificacionpagos.model.ConverterJsonToObject;

public class VCuentaPlacaRfc extends CustomMainScreen implements FieldChangeListener, Viewable {

	protected Opcion listMembers[] = {
			new Opcion("Seleccione tr�mite", 0),
			new Opcion("ISAN", 3),
			new Opcion("Predial", 1),
			new Opcion("N�mina", 3),
			new Opcion("Hospedaje", 3),
			new Opcion("Tenencia", 2),
			new Opcion("Tarjeta de circulaci�n", 2),
			new Opcion("Loterias", 3),
			new Opcion("I.E.P.S (gasolina)", 3),
			new Opcion("Verificaci�n extempor�nea", 2)
	};
	
	private VerticalFieldManager fieldManager;
	private ObjectChoiceField choiceTramite;
	

	private String jsonString = 
			
			"{	\"conceptos\": [	{		\"id_concepto\": 1,		\"concepto\": \"I.S.A.N\",		\"parametros\": [{			\"nombre_param\": \"RFC\",			\"tipo\": \"String\",			\"longitud_max\": \"15\"		},		{			\"nombre_param\": \"Nombre o Denominacion o Raz�n Social\",			\"tipo\": \"String\",			\"longitud_max\": \"100\"		}]	},	{		\"id_concepto\": 2,		\"concepto\": \"N�mina\",		\"parametros\": [{			\"nombre_param\": \"RFC\",			\"tipo\": \"String\",			\"longitud_max\": \"15\"		},		{			\"nombre_param\": \"Nombre o Denominacion o Raz�n Social\",			\"tipo\": \"String\",			\"longitud_max\": \"100\"		}]	}]}";
	
	
	
	public VCuentaPlacaRfc(String title) {
		
		super(title, true);
		
		
		

		
		
		
		
		
		GenericHTTP genericHTTP = new GenericHTTP(null, Url.GDF_TIPO_CERTIFICADOS, this);
		genericHTTP.run();
		
		fieldManager = new VerticalFieldManager();
		
		choiceTramite = new ObjectChoiceField("", listMembers);
		choiceTramite.setChangeListener(this);
		add(choiceTramite);
		add(fieldManager);
		
	}

	public void setData(int request, JSONObject jsObject) {

		System.out.println(jsObject);
		

		
	}

	public void sendMessage(String message) {
		// TODO Auto-generated method stub
		
	}

	public void fieldChanged(Field field, int arg1) {

		if (field == choiceTramite){

			Opcion opcion = getObjectFromChoice(choiceTramite);

			fieldManager.deleteAll();
			
			switch (opcion.getIndex()) {
			
			case 1:
				ColorLabelField lbCuentaPredial = new ColorLabelField("Cuenta Predial: ", LabelField.NON_FOCUSABLE);
				ColorEditField editCuentaPredial = new ColorEditField("", "");
				fieldManager.add(lbCuentaPredial);
				fieldManager.add(editCuentaPredial);
				break;
				
			case 2:
				ColorLabelField lbPlaca = new ColorLabelField("Placa: ", LabelField.NON_FOCUSABLE);
				ColorEditField editPlaca = new ColorEditField("", "");

				ColorLabelField lbNumeroSerie = new ColorLabelField("N�mero de serie: ", LabelField.NON_FOCUSABLE);
				ColorEditField editNumeroSerie = new ColorEditField("", "");

				fieldManager.add(lbPlaca);
				fieldManager.add(editPlaca);
				fieldManager.add(new LabelField());
				fieldManager.add(lbNumeroSerie);
				fieldManager.add(editNumeroSerie);
				
				break;
				
			case 3:
				ColorLabelField lbRFC = new ColorLabelField("RFC: ", LabelField.NON_FOCUSABLE);
				ColorEditField editRFC = new ColorEditField("", "");

				ColorLabelField lbRazonSocial = new ColorLabelField("Raz�n social: ", LabelField.NON_FOCUSABLE);
				ColorEditField editRazonSocial = new ColorEditField("", "");

				fieldManager.add(lbRFC);
				fieldManager.add(editRFC);
				fieldManager.add(new LabelField());
				fieldManager.add(lbRazonSocial);
				fieldManager.add(editRazonSocial);
				break;
			}
		}
	}
	
	
	private Opcion getObjectFromChoice(ObjectChoiceField choiceField){
		
		int index = choiceField.getSelectedIndex();
		Opcion opcion = (Opcion)choiceField.getChoice(index);
		
		return opcion;
	}
	
}



/*
"ISAN",
"Predial",
"N�mina",
"Hospedaje",
"Tenencia",
"Tarjeta de circulaci�n",
"Loterias",
"I.E.P.S (gasolina)",
"Verificaci�n extempor�nea"
*/

/*	
ISAN - 2
PREDIAL - 1
N�MINA - 2
HOSPEDAJE - 2
TENENCIA - 3
TARJETA DE CIRCULACI�N - 3
LOTERIAS - 2
I.E.P.S (GASOLINA) - 2
VERIFICACI�N EXTEMPORANEA - 3
*/


