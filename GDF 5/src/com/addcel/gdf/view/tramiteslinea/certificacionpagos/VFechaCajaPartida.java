package com.addcel.gdf.view.tramiteslinea.certificacionpagos;

import java.util.Date;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.LabelField;

import org.json.me.JSONObject;

import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;

public class VFechaCajaPartida extends CustomMainScreen implements FieldChangeListener, Viewable {
	
	public VFechaCajaPartida(String title) {

		super(title, true);

		DateField dateFecha = new DateField("Fecha", new Date().getTime(), DateField.DATE);
		
		ColorLabelField lbCaja = new ColorLabelField("Caja: ", LabelField.NON_FOCUSABLE);
		ColorEditField editCaja = new ColorEditField("", "");

		ColorLabelField lbPartida = new ColorLabelField("Partida: ", LabelField.NON_FOCUSABLE);
		ColorEditField editPartida = new ColorEditField("", "");

		ColorLabelField lbImporte = new ColorLabelField("Importe: ", LabelField.NON_FOCUSABLE);
		ColorEditField editImporte = new ColorEditField("", "");
		
		add(dateFecha);
		add(new LabelField());
		add(lbCaja);
		add(editCaja);
		add(new LabelField());
		add(lbPartida);
		add(editPartida);
		add(new LabelField());
		add(lbImporte);
		add(editImporte);

		
		
		
	}

	public void setData(int request, JSONObject jsObject) {
		// TODO Auto-generated method stub
		
	}

	public void sendMessage(String message) {
		// TODO Auto-generated method stub
		
	}

	public void fieldChanged(Field arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	
}

