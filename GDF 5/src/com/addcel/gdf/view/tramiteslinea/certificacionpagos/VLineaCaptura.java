package com.addcel.gdf.view.tramiteslinea.certificacionpagos;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;

import org.json.me.JSONObject;

import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;

public class VLineaCaptura extends CustomMainScreen implements FieldChangeListener, Viewable {
	
	public VLineaCaptura(String title) {
		
		super(title, true);
		
		ColorLabelField lbLC = new ColorLabelField("Linea de captura: ", LabelField.NON_FOCUSABLE);
		ColorEditField editLC = new ColorEditField("", "");

		
		
		ColorLabelField lbImporte = new ColorLabelField("Importe: ", LabelField.NON_FOCUSABLE);
		ColorEditField editImporte = new ColorEditField("", "");
		

		add(lbLC);
		add(editLC);
		add(new LabelField());
		add(lbImporte);
		add(editImporte);
		
	}

	public void setData(int request, JSONObject jsObject) {
		// TODO Auto-generated method stub
		
	}

	public void sendMessage(String message) {
		// TODO Auto-generated method stub
		
	}

	public void fieldChanged(Field arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	
}


