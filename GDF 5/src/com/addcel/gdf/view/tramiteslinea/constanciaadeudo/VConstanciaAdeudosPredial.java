package com.addcel.gdf.view.tramiteslinea.constanciaadeudo;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ListField;
import net.rim.device.api.ui.component.ListFieldCallback;

import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;

public class VConstanciaAdeudosPredial extends CustomMainScreen implements ListFieldCallback {

	public VConstanciaAdeudosPredial(String title) {

		super(title, true, true);


		ColorLabelField lbCuentaPredial = new ColorLabelField("Cuenta predial: ", LabelField.NON_FOCUSABLE);
		ColorEditField editCuentaPredial = new ColorEditField("", "");

		SlimSelectedButtonField btnBuscar = new SlimSelectedButtonField("Buscar", ButtonField.CONSUME_CLICK);

		ColorEditField editConcepto = new ColorEditField("Concepto: ", "", 100, EditField.NON_FOCUSABLE);
		ColorEditField editCuenta = new ColorEditField("Cuenta: ", "", 100, EditField.NON_FOCUSABLE);

		add(lbCuentaPredial);
		add(editCuentaPredial);
		add(new LabelField());
		add(btnBuscar);
		add(new LabelField());
		add(editConcepto);
		add(editCuenta);
		
		
		
	}

	public void drawListRow(ListField arg0, Graphics arg1, int arg2, int arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	public Object get(ListField listField, int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public int getPreferredWidth(ListField listField) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int indexOfList(ListField listField, String prefix, int start) {
		// TODO Auto-generated method stub
		return 0;
	}
}