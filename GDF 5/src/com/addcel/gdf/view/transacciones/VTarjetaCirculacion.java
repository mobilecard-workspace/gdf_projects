package com.addcel.gdf.view.transacciones;

import java.util.Date;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.dto.licencia.MarcaCarro;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTP;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTPHard;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoSAbierto;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedSizeButton;

public class VTarjetaCirculacion extends CustomMainScreen implements FieldChangeListener, Viewable {

	private final int PETICION_MARCAS = 0;
	private final int PETICION_TARJETA_CIRCULACION = 1;
	
	private SplashScreen splashScreen;
	
	private ColorLabelField labelIngresoPlaca;
	private ColorLabelField labelConfirmaPlaca;


	private ColorLabelField labelDataPlaca;
	private ColorLabelField labelDataDerechos;
	private ColorLabelField labelDataLineaCaptura;
	private ColorLabelField labelDataTotal;
	
	private HighlightsLabelField editDataPlaca;
	private HighlightsLabelField editDataDerechos;
	private HighlightsLabelField editDataLineaCaptura;
	private HighlightsLabelField editDataTotal;

	private ObjectChoiceField choiceMarca;
	private ObjectChoiceField choiceModelo;
	private ObjectChoiceField choicePlacas;

	private ColorEditField editIngresoPlaca;
	private ColorEditField editConfirmaPlaca;
	
	private CustomSelectedSizeButton btnCalcular;
	private CustomSelectedSizeButton btnRealizarPAgo;
	
	private int peticion;
	
	private JSONObject jsObject;
	
	private double montoMAximo;
	private double totalPago;
	
	
	public VTarjetaCirculacion() {
		
		super("Tarjeta de circulaci�n", true);
		splashScreen = SplashScreen.getInstance();
		addComponents();

		splashScreen.start();

		peticion = PETICION_MARCAS;
		GenericHTTPHard genericHTTP = new GenericHTTPHard("", Url.GDF_CIRCULACION_MARCAS, this);
		genericHTTP.run();
	}
	
	private void addComponents(){
		
		String[] aniosModelo = UtilDate.getAniosModelo();
		
		labelIngresoPlaca = new ColorLabelField("Ingresa tu placa:");
		labelConfirmaPlaca = new ColorLabelField("Confirma tu placa:");
		
		choiceMarca = new ObjectChoiceField("Marca del veh�culo:", null);
		choiceModelo = new ObjectChoiceField("Modelo del veh�culo:", aniosModelo);
		choicePlacas = new ObjectChoiceField("Placas del veh�culo:", null);

		editIngresoPlaca = new ColorEditField("", "");
		editConfirmaPlaca = new ColorEditField("", "");


		labelDataPlaca = new ColorLabelField("Placa: ", LabelField.NON_FOCUSABLE);
		labelDataDerechos = new ColorLabelField("Derechos: ", LabelField.NON_FOCUSABLE);
		labelDataLineaCaptura = new ColorLabelField("Linea Captura: ", LabelField.NON_FOCUSABLE);
		labelDataTotal = new ColorLabelField("Total: ", LabelField.NON_FOCUSABLE);
		
		editDataPlaca = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDataDerechos = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDataLineaCaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDataTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		
		
		btnCalcular = new CustomSelectedSizeButton("Calcular", 1);
		btnRealizarPAgo = new CustomSelectedSizeButton("Realizar pago", 1);
		btnCalcular.setChangeListener(this);
		btnRealizarPAgo.setChangeListener(this);
		
		
		//add(labelMarca);
		add(choiceMarca);
		add(new LabelField());
		
		//add(labelModelo);
		add(choiceModelo);
		add(new LabelField());
		
		//add(labelplacas);
		add(choicePlacas);
		add(new LabelField());
		
		add(labelIngresoPlaca);
		add(editIngresoPlaca);
		add(new LabelField());
		
		add(labelConfirmaPlaca);
		add(editConfirmaPlaca);
		add(new LabelField());
		
		add(btnCalcular);
		add(new LabelField());
		addHorizontalFiels(labelDataPlaca, editDataPlaca);
		addHorizontalFiels(labelDataDerechos, editDataDerechos);
		addHorizontalFiels(labelDataLineaCaptura, editDataLineaCaptura);
		addHorizontalFiels(labelDataTotal, editDataTotal);
		add(new LabelField());
		add(btnRealizarPAgo);
		
	}
	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}
	
	public void setData(int request, JSONObject jsObject) {

		
		splashScreen.remove();
		/*
		System.out.println(jsObject.toString());
		
		// {"numError":-1,"error":"Descripcion Error"}
		
		String numError = jsObject.optString("numError", null);
		//int numError = jsObject.optInt("numError", 0);
		String error = jsObject.optString("error");
		
		if ((numError != null)&&(numError.equals("0"))){
		*/
			switch(peticion){
			
			case PETICION_MARCAS:

				JSONArray jsonArray = jsObject.optJSONArray("marcas");
				
				int lengh = jsonArray.length();
				
				MarcaCarro[] marcaCarros = new MarcaCarro[lengh];
				
				for(int index = 0; index < lengh; index++){
					
					JSONObject jsMarca = jsonArray.optJSONObject(index);
					
					MarcaCarro marcaCarro = new MarcaCarro();
					
					marcaCarro.setId_marca(jsMarca.optString("id_marca"));
					marcaCarro.setMarca(jsMarca.optString("marca"));
					
					marcaCarros[index] = marcaCarro;
				}
				
				choiceMarca.setChoices(marcaCarros);

				break;
			
			case PETICION_TARJETA_CIRCULACION:

				this.jsObject = jsObject;
				setInformacion(jsObject);

				break;
			
			}
		/*
		} else {
			
			Dialog.alert(error);
		}
		*/
	}

	
	private void setInformacion(JSONObject jsonObject){
		
		if (jsonObject == null){
			
			setEditTextEmpty();
			this.jsObject = null;
		} else {
			
			String jsonPlaca = jsonObject.optString("placa", null);
			String jsonImporte = jsonObject.optString("importe", null);
			String jsonLinea_captura = jsonObject.optString("linea_captura", null);
			
			String jsonMontoMaximo = jsonObject.optString("monto_maximo_operacion", "5000");
			String jsonTotalPago = jsonObject.optString("totalPago", null);
			
			montoMAximo = Double.parseDouble(jsonMontoMaximo);
			totalPago = Double.parseDouble(jsonTotalPago);
			
			if ((jsonPlaca != null)&&(jsonImporte != null)&&(jsonLinea_captura != null)){
				
				editDataPlaca.setText(jsonPlaca);
				editDataDerechos.setText(jsonImporte);
				editDataLineaCaptura.setText(jsonLinea_captura);
				editDataTotal.setText(jsonTotalPago);
			} else {
				setEditTextEmpty();
				this.jsObject = null;
				Dialog.alert("Falto informaci�n, por favor repetir la acci�n.");
			}
		}
	}
	
	private void setEditTextEmpty(){
		editDataPlaca.setText("");
		editDataDerechos.setText("");
		editDataLineaCaptura.setText("");
		editDataTotal.setText("");
	}

	public void sendMessage(String message) {}


	public void fieldChanged(Field field, int context) {
		
		if (field == btnCalcular){

			if (UserBean.nameLogin == null){
				UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
			} else {
				
				try {
					
					if (verificarDatos()){
						
						if (verificarPlacas()){
							
							JSONObject jsonValues = getValues();

							String key = UtilDate.getDateToDDMMAAAA(new Date());

							String data = AddcelCrypto.encryptSensitive(key, jsonValues.toString());

							peticion = PETICION_TARJETA_CIRCULACION;
							GenericHTTP genericHTTP = new GenericHTTP(data, Url.URL_GDF, this);
							genericHTTP.run();

						} else {
							setEditTextEmpty();
							this.jsObject = null;
							Dialog.alert("Los n�meros de placas no son iguales.");
						}
						
						
					} else {
						setEditTextEmpty();
						this.jsObject = null;
						Dialog.alert("El n�mero de placa deben de ser de al menos 4 caracteres y estar en los campos" +
								" \"Ingresa tu placa\" y \"Confirma tu placa\".");
					}
					
				} catch (JSONException e) {
					setEditTextEmpty();
					this.jsObject = null;
					Dialog.alert("Intente de nuevo por favor.");
					e.printStackTrace();
				}
				
			}
			
		} else if (field == btnRealizarPAgo){
			
			if (this.jsObject != null){
				
				if (totalPago <= montoMAximo){
					
					UiApplication.getUiApplication().popScreen(this);
					UiApplication.getUiApplication().pushScreen(new VPagoSAbierto("Tarjeta de Circulaci�n", jsObject, "Tarjeta de Circulaci�n", DTO.STATUS_TARJETA_CIRCULACION));
				} else {
					Dialog.alert("El l�mite m�ximo para la transacci�n es: " + montoMAximo);
				}

				
			} else {
				Dialog.alert("Oprima por favor el bot�n \"Calcular\".");
			}
		}
	}
	
	
	private boolean verificarPlacas(){
		
		String placa = editIngresoPlaca.getText();
		String placaConfirma = editConfirmaPlaca.getText();
		
		placa = placa.toUpperCase();
		placaConfirma = placaConfirma.toUpperCase();
		
		editIngresoPlaca.setText(placa);
		editConfirmaPlaca.setText(placaConfirma);
		
		return placa.equals(placaConfirma);
	}
	
	
	private boolean verificarDatos(){
		
		int iPlaca = editIngresoPlaca.getTextLength();
		int iPlacaConfirma = editConfirmaPlaca.getTextLength();
		
		if((iPlaca >= 4)&&(iPlacaConfirma >= 4)){
			
			return true;
		} else {
			
			return false;
		}
	}
	
	
	private JSONObject getValues() throws JSONException{
		
		String idUsuario = UserBean.idLogin;
		String placa = editConfirmaPlaca.getText();
		
		int marcaIndex = choiceMarca.getSelectedIndex();
		MarcaCarro marcaCarro = (MarcaCarro) choiceMarca.getChoice(marcaIndex);
		
		int modeloIndex = choiceModelo.getSelectedIndex();
		String modeloCarro = (String) choiceModelo.getChoice(modeloIndex);
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("toDO", "7");
		jsonObject.put("placa", placa);
		jsonObject.put("id_usuario", idUsuario);
		jsonObject.put("marca", marcaCarro.getId_marca());
		jsonObject.put("modelo", modeloCarro);
		
		return jsonObject;
	}
}



/*
TarjetaCirculacion tarjeta = new TarjetaCirculacion();

tarjeta.setId_producto(jsObject.optInt("id_producto"));
tarjeta.setId_usuario(jsObject.optInt("id_usuario"));
tarjeta.setId_bitacora(jsObject.optInt("id_bitacora"));
tarjeta.setClave(jsObject.optInt("clave"));
tarjeta.setModelo(jsObject.optInt("modelo"));
tarjeta.setEstado(jsObject.optInt("estado"));

tarjeta.setStatusPago(jsObject.optString("statusPago"));
tarjeta.setLinea_captura(jsObject.optString("linea_captura"));
tarjeta.setConcepto(jsObject.optString("concepto"));
tarjeta.setDescuento(jsObject.optString("descuento"));
tarjeta.setImporteFinal(jsObject.optString("importeFinal"));
tarjeta.setPlaca(jsObject.optString("placa"));
tarjeta.setImporte(jsObject.optString("importe"));
tarjeta.setMarca(jsObject.optString("marca"));
tarjeta.setNombreSubConcepto(jsObject.optString("nombreSubConcepto"));
tarjeta.setVigencia(jsObject.optString("vigencia"));
tarjeta.setImporteInicial(jsObject.optString("importeInicial"));
tarjeta.setError(jsObject.optString("error"));
tarjeta.setTotalPago(jsObject.optString("totalPago"));
tarjeta.setBanco(jsObject.optString("banco"));
*/



/*
	{
		"toDO": "7",
		"placa": "884YAB",
		"id_usuario": 1372462440000,
		"marca": "B",
		"modelo": "2009"
	}
	
*/

//labelMarca = new ColorLabelField("Marca del veh�culo:");
//labelModelo = new ColorLabelField("Modelo del veh�culo:");
//labelplacas = new ColorLabelField("Placas del veh�culo:");
