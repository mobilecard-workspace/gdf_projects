package com.addcel.gdf.view.transacciones;

import java.util.Date;
import java.util.TimeZone;

import net.rim.device.api.i18n.Locale;
import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.io.http.HttpDateParser;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.GenericHTTP;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoSAbierto;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.CustomSelectedSizeButton;

public class VLicenciaTemporal extends CustomMainScreen implements FieldChangeListener, Viewable {

	SplashScreen splashScreen;

	
	private final int PETICION_LICENCIA_3ANIOS = 0;
	
	private ColorLabelField labelRFC;
	private ColorLabelField labelRFCConfirma;
	
	private ColorEditField editRFC;
	private ColorEditField editRFCConfirma;
	
	private CustomSelectedSizeButton btnCalcular;
	private CustomSelectedSizeButton btnRealizarPago;
	
	private ColorLabelField labelDataRFC;
	private ColorLabelField labelDataDerechos;
	private ColorLabelField labelDataLineaCaptura;
	private ColorLabelField labelDataTotal;
	
	private HighlightsLabelField editDataRFC;
	private HighlightsLabelField editDataDerechos;
	private HighlightsLabelField editDataLineaCaptura;
	private HighlightsLabelField editDataTotal;

	
	private int peticion;
	
	private int idProducto;
	
	private double montoMAximo;
	private double totalPago;
	
	public VLicenciaTemporal(int idProducto) {
		
		super("Licencia de conducir \"A\" 3 a�os", true);
		splashScreen = SplashScreen.getInstance();
		this.idProducto = idProducto;
		addComponents();
	}


	private void addComponents(){
		
		labelRFC = new ColorLabelField("Ingresa tu RFC:");
		labelRFCConfirma = new ColorLabelField("Confirma tu RFC:");

		editRFC = new ColorEditField("", "");
		editRFCConfirma = new ColorEditField("", "");
		
		
		btnCalcular = new CustomSelectedSizeButton("Calcular", 1);
		btnRealizarPago = new CustomSelectedSizeButton("Realizar pago", 1);
		btnCalcular.setChangeListener(this);
		btnRealizarPago.setChangeListener(this);
		
		labelDataRFC = new ColorLabelField("RFC: ", LabelField.NON_FOCUSABLE);
		labelDataDerechos = new ColorLabelField("Derechos: ", LabelField.NON_FOCUSABLE);
		labelDataLineaCaptura = new ColorLabelField("Linea Captura: ", LabelField.NON_FOCUSABLE);
		labelDataTotal = new ColorLabelField("Total: ", LabelField.NON_FOCUSABLE);
		
		editDataRFC = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDataDerechos = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDataLineaCaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDataTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		
		add(labelRFC);
		add(editRFC);
		add(new LabelField());
		
		add(labelRFCConfirma);
		add(editRFCConfirma);
		add(new LabelField());
		
		add(btnCalcular);
		add(new LabelField());
		addHorizontalFiels(labelDataRFC, editDataRFC);
		addHorizontalFiels(labelDataDerechos, editDataDerechos);
		addHorizontalFiels(labelDataLineaCaptura, editDataLineaCaptura);
		addHorizontalFiels(labelDataTotal, editDataTotal);
		add(new LabelField());
		add(btnRealizarPago);
	}
	
	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}


	private JSONObject jsObject;
	
	public void setData(int request, JSONObject jsObject) {
		
		System.out.println(jsObject.toString());
		
		switch(peticion){
		
		case PETICION_LICENCIA_3ANIOS:
			this.jsObject = jsObject;
			setInformacion(jsObject);
			break;
		}
	}

	public void sendMessage(String message) {}

	public void fieldChanged(Field field, int context) {
		
		if (field == btnCalcular){
	
			if (UserBean.nameLogin == null){
				UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
			} else {
				
				try {
					
					if (verificarDatos()){
						
						if (verificarRFC()){

							String rfc = editRFCConfirma.getText();
							
							if (UtilDate.isAdulto(rfc)){
								
								JSONObject jsonValues = getValues();
								
								String key = UtilDate.getDateToDDMMAAAA(new Date());

								String data = AddcelCrypto.encryptSensitive(key, jsonValues.toString());

								peticion = PETICION_LICENCIA_3ANIOS;
								GenericHTTP genericHTTP = new GenericHTTP(data, Url.URL_GDF, this);
								genericHTTP.run();
							
							} else {
								
								Dialog.alert("Debe cumplir con la mayor�a de edad para obtener su Licencia de conducir.");
							}
								
						} else {
							setEditTextEmpty();
							this.jsObject = null;
							Dialog.alert("Los n�meros de RFC no son iguales.");
						}
						
					} else {
						setEditTextEmpty();
						this.jsObject = null;
						Dialog.alert("El n�mero de RFC debe de ser de al menos 10 caracteres y estar en los campos" +
								" \"Ingresa tu RFC\" y \"Confirma tu RFC\".");
					}
					
				} catch (JSONException e) {
					setEditTextEmpty();
					this.jsObject = null;
					Dialog.alert("Intente de nuevo por favor.");
					e.printStackTrace();
				}
				
			}
			
		} else if (field == btnRealizarPago){
			
			if (this.jsObject != null){
				if (totalPago <= montoMAximo){
					UiApplication.getUiApplication().popScreen(this);
					UiApplication.getUiApplication().pushScreen(new VPagoSAbierto("Licencia de conducir tipo \"A\" 3 a�os", jsObject, "Licencia de conducir tipo \"A\" 3 a�os", idProducto));
				} else {
					Dialog.alert("El l�mite m�ximo para la transacci�n es: " + montoMAximo);
				}
				
			} else {
				Dialog.alert("Oprima por favor el bot�n \"Calcular\".");
			}
		}
	}
	
	
	private boolean verificarRFC(){
		
		String rfc = editRFC.getText();
		String rfcConfirma = editRFCConfirma.getText();
		
		rfc = rfc.toUpperCase();
		rfcConfirma = rfcConfirma.toUpperCase();
		
		editRFC.setText(rfc);
		editRFCConfirma.setText(rfcConfirma);
		
		return rfc.equals(rfcConfirma);
	}
	
	private boolean verificarDatos(){
		
		int iRFC = editRFC.getTextLength();
		int iRFCConfirma = editRFCConfirma.getTextLength();
		
		if((iRFC >= 10)&&(iRFCConfirma >= 10)){
			
			return true;
		} else {
			
			return false;
		}
	}

	
/*
	{
		"toDO": "8",
		"rfc": "OANJ830926",
		"id_usuario": 1372462440000
	}
*/
	private JSONObject getValues() throws JSONException{
		
		String idUsuario = UserBean.idLogin;
		String rfc = editRFCConfirma.getText();

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("toDO", "8");
		jsonObject.put("rfc", rfc);
		jsonObject.put("id_usuario", idUsuario);
		
		return jsonObject;
	}
	
	
	private void setInformacion(JSONObject jsonObject){
		
		if (jsonObject == null){
			
			setEditTextEmpty();
			this.jsObject = null;
		} else {
			
			String jsonRFC = jsonObject.optString("rfc", null);
			String jsonImporte = jsonObject.optString("importeFinal", null);
			String jsonLinea_captura = jsonObject.optString("linea_captura", null);
			
			String jsonMontoMaximo = jsonObject.optString("monto_maximo_operacion", "5000");
			String jsonTotalPago = jsonObject.optString("totalPago", null);
			
			montoMAximo = Double.parseDouble(jsonMontoMaximo);
			totalPago = Double.parseDouble(jsonTotalPago);
			
			
			if ((jsonRFC != null)&&(jsonImporte != null)&&(jsonLinea_captura != null)){
				
				editDataRFC.setText(jsonRFC);
				editDataDerechos.setText(jsonImporte);
				editDataLineaCaptura.setText(jsonLinea_captura);
				editDataTotal.setText(jsonTotalPago);
				//private ColorLabelField labelDataTotal;
				//private HighlightsLabelField editDataTotal;
			} else {
				setEditTextEmpty();
				this.jsObject = null;
				Dialog.alert("Falto informaci�n, por favor repetir la acci�n.");
			}
		}
	}
	
	private void setEditTextEmpty(){
		editDataRFC.setText("");
		editDataDerechos.setText("");
		editDataLineaCaptura.setText("");
		editDataTotal.setText("");
	}
}
