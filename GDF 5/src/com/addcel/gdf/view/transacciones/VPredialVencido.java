package com.addcel.gdf.view.transacciones;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotFoundException;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.component.TextField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.model.rms.DataRMS;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.listas.predialvencido.LPredialVencido;

public class VPredialVencido extends CustomMainScreen implements FieldChangeListener, Viewable {

	
	private ObjectChoiceField editPrediales = null;
	
	private LabelField textPredial = null;
	//private LabelField textConcepto = null;
	//private LabelField textCuentaP = null;
	//private LabelField textBimestre = null;
	//private LabelField textAnio = null;
	//private LabelField textVencimiento = null;
	//private LabelField textLCaptura = null;
	//private LabelField textImporte = null;
	//private LabelField textIVA = null;
	//private LabelField textReduccion = null;
	//private LabelField textTotal = null;

	private ColorEditField editPredial = null;
	//private HighlightsLabelField editConcepto = null;
	//private HighlightsLabelField editCuentaP = null;
	//private HighlightsLabelField editBimestre = null;
	//private HighlightsLabelField editAnio = null;
	//private HighlightsLabelField editLCaptura = null;
	//private HighlightsLabelField editImporte = null;
	//private HighlightsLabelField editIVA = null;
	//private HighlightsLabelField editReduccion = null;
	//private HighlightsLabelField editTotal = null;

	private SlimSelectedButtonField consultar = null;
	private SlimSelectedButtonField agregar = null;
	//private SlimSelectedButtonField pagar = null;
	
	private SplashScreen splashScreen;
	private String sCuentaPredial;

	private JSONObject jsData;

	public VPredialVencido(String title) {

		super(title, true);

		splashScreen = SplashScreen.getInstance();

		//add(new ColorLabelField("Selecciona los periodos a pagar.", LabelField.NON_FOCUSABLE));
		
		
		DataRMS placas = new DataRMS(DataRMS.PREDIAL);
		
		try {
			String sPrediales[] = placas.getData();
			editPrediales = new ObjectChoiceField("", sPrediales, 0);			
		} catch (RecordStoreFullException e) {
			Dialog.alert("Ya no se puede almacenar información");
			e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			Dialog.alert("No se encuentra en sistema de almacenamiento");
			e.printStackTrace();
		} catch (RecordStoreException e) {
			Dialog.alert("El almacenamiento no se encuentra a disposición");
			e.printStackTrace();
		} catch (IOException e) {
			Dialog.alert("La lectura de la información es inexacta.");
			e.printStackTrace();
		}

		textPredial = new ColorLabelField("Cuenta Predial: ", LabelField.NON_FOCUSABLE);
		editPredial = new ColorEditField("", "");

		consultar = new SlimSelectedButtonField("Consultar", ButtonField.CONSUME_CLICK);
		agregar = new SlimSelectedButtonField("Agregar predial", ButtonField.CONSUME_CLICK);
		consultar.setChangeListener(this);
		agregar.setChangeListener(this);

		add(editPrediales);

		addHorizontalFiels(textPredial, editPredial);

		CustomButtonFieldManager manager = new CustomButtonFieldManager(consultar, agregar);
		manager.add(consultar);
		manager.add(agregar);
		add(manager);
	}

	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}
	
	
	
	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();

		jsData = jsObject;
		
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		
		if (jsObject.has("error")){
			String error = jsObject.optString("error");
			
			if (error.trim().length() > 0){
				Dialog.alert(error);
			}
		} else if (jsObject.has("cadbc")){
			
			LPredialVencido predialVencido = new LPredialVencido("Lista de predial vencidos", jsObject);
			UiApplication.getUiApplication().pushScreen(predialVencido);
		} else if(jsObject.has("idError")){
			
			int idError = jsObject.optInt("idError");
			
			if (idError < 0){
				
				String mensaje = jsObject.optString("mensajeError");
				
				Dialog.alert(mensaje);
			}
		}
	}

	public void sendMessage(String message) {
	}

	
	public void fieldChanged(Field field, int arg) {

		if (field == consultar){

			if (UserBean.nameLogin == null){
				UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
			} else {
				getData();
			}

		} else if(field == agregar){
			agregarPredial();
		} 
	}


	private void getData() {

		int iCuentaAgua = editPrediales.getSelectedIndex();
		
		if (iCuentaAgua == 0){
			sCuentaPredial = editPredial.getText();
			if (sCuentaPredial != null){
				sCuentaPredial = sCuentaPredial.toUpperCase();
				editPredial.setText(sCuentaPredial);
			}
		} else {
			sCuentaPredial = (String) editPrediales.getChoice(iCuentaAgua);
		}


		if (sCuentaPredial != null) {
			splashScreen.start();

			JSONObject jsonObject = new JSONObject();

			try {
				
				jsonObject.put("toDO", 6);
				jsonObject.put("cuenta", sCuentaPredial);
				jsonObject.put("tipo_consulta_pv", "Adeudos");
				
				String post = jsonObject.toString();

				String key = UtilDate.getDateToDDMMAAAA(new Date());
				
				String data = AddcelCrypto.encryptSensitive(key, post);
				
				PagoPlacas pagoPlacas = new PagoPlacas(data, this);
				pagoPlacas.run();

			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert("Error al interpretar los datos capturados.");
			}
			
		} else {
			Dialog.alert("Verifique los datos capturados");
		}
	}


	private void agregarPredial() {
		
		BasicEditField inputField = new BasicEditField();

		Dialog d = new Dialog(Dialog.D_OK_CANCEL, "Escriba su Predial:", Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		d.add(inputField);

		int i = d.doModal();

		if (i == Dialog.OK) {

			try {
				
				String predial = inputField.getText();
				
				if (predial != null){
					DataRMS placas = new DataRMS(DataRMS.PREDIAL);
					predial = predial.toUpperCase();
					placas.addData(predial);
					String sPlacas[] = placas.getData();
					editPrediales.setChoices(sPlacas);
				} else {
					Dialog.alert("Verificar valor de placa");
				}

			} catch (RecordStoreFullException e) {
				Dialog.alert("Ya no se puede almacenar información");
				e.printStackTrace();
			} catch (RecordStoreNotFoundException e) {
				Dialog.alert("No se encuentra en sistema de almacenamiento");
				e.printStackTrace();
			} catch (RecordStoreException e) {
				Dialog.alert("El almacenamiento no se encuentra a disposición");
				e.printStackTrace();
			} catch (IOException e) {
				Dialog.alert("La lectura de la información es inexacta.");
				e.printStackTrace();
			}
		}
	}
}

