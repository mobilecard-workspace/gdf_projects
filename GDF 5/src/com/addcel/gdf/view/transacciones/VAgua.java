package com.addcel.gdf.view.transacciones;

import java.io.IOException;
import java.util.Date;

import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotFoundException;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.model.rms.DataRMS;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoSAbierto;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.util.UtilNumber;

//Derecho


public class VAgua extends CustomMainScreen implements FieldChangeListener, Viewable {

	
	private ObjectChoiceField editChoiceAgua = null;
	
	private LabelField textAgua = null;
	private LabelField textBimestre = null;
	
	private LabelField textDerecho = null;
	
	private LabelField textAnioBimestre = null;
	private LabelField textCuentaAgua = null;
	private LabelField textIVA = null;
	private LabelField textTotal = null;
	private LabelField textLCaptura = null;

	private ColorEditField editAgua = null;
	private HighlightsLabelField editBimestre = null;
	
	private HighlightsLabelField editDerecho = null;
	
	private HighlightsLabelField editAnioBimestre = null;
	private HighlightsLabelField editCuentaAgua = null;
	private HighlightsLabelField editIVA = null;
	private HighlightsLabelField editTotal = null;
	private HighlightsLabelField editLCaptura = null;
	
	private SlimSelectedButtonField consultar = null;
	private SlimSelectedButtonField agregar = null;
	private SlimSelectedButtonField pagar = null;

	private SplashScreen splashScreen;

	private String sCuentaAgua;
	private String montoMaximoOperacion;
	private String total;
	
	private JSONObject jsData;
	
	
	public VAgua(String title) {
		super(title, true);


		splashScreen = SplashScreen.getInstance();

		DataRMS dataRMS = new DataRMS(DataRMS.AGUA);

		try {
			String sAguas[] = dataRMS.getData();
			editChoiceAgua = new ObjectChoiceField("", sAguas, 0);
		} catch (RecordStoreFullException e) {
			Dialog.alert("Ya no se puede almacenar informaci�n");
			e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			Dialog.alert("No se encuentra en sistema de almacenamiento");
			e.printStackTrace();
		} catch (RecordStoreException e) {
			Dialog.alert("El almacenamiento no se encuentra a disposici�n");
			e.printStackTrace();
		} catch (IOException e) {
			Dialog.alert("La lectura de la informaci�n es inexacta.");
			e.printStackTrace();
		}

		textAgua =  new ColorLabelField("Cuenta Agua: ", LabelField.NON_FOCUSABLE);
		textBimestre = new ColorLabelField("Bimestre: ", LabelField.NON_FOCUSABLE) ;
		textDerecho = new ColorLabelField("Derecho: ", LabelField.NON_FOCUSABLE) ;
		textAnioBimestre = new ColorLabelField("A�o del bimestre: ", LabelField.NON_FOCUSABLE) ;
		textCuentaAgua = new ColorLabelField("Cuenta agua: ", LabelField.NON_FOCUSABLE) ;
		textIVA = new ColorLabelField("Iva: ", LabelField.NON_FOCUSABLE) ;
		textTotal = new ColorLabelField("Total: ", LabelField.NON_FOCUSABLE) ;
		textLCaptura = new ColorLabelField("Linea de captura: ", LabelField.NON_FOCUSABLE) ;

		editAgua = new ColorEditField("", "");
		editBimestre = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editDerecho = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editAnioBimestre =  new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editCuentaAgua =  new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editIVA =  new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTotal =  new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editLCaptura =  new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		
		consultar = new SlimSelectedButtonField("Consultar", ButtonField.CONSUME_CLICK);
		agregar =  new SlimSelectedButtonField("Agregar", ButtonField.CONSUME_CLICK);
		pagar =  new SlimSelectedButtonField("Pagar", ButtonField.CONSUME_CLICK);

		consultar.setChangeListener(this);
		agregar.setChangeListener(this);
		pagar.setChangeListener(this);
		
		add(editChoiceAgua);
		addHorizontalFiels(textAgua, editAgua);
		CustomButtonFieldManager manager = new CustomButtonFieldManager(consultar, agregar);
		manager.add(consultar);
		manager.add(agregar);
		add(manager);
		add(new LabelField(""));

		/*
		   a).-Cuenta
		   b).-A�o
		   c).-Bimestre
		   d).-Derecho
		   e).-IVA
		   f).-Linea Captura
		   g).-Total
		*/
		addHorizontalFiels(textCuentaAgua, editCuentaAgua);
		addHorizontalFiels(textAnioBimestre, editAnioBimestre);
		addHorizontalFiels(textBimestre, editBimestre);
		addHorizontalFiels(textDerecho, editDerecho);
		addHorizontalFiels(textIVA, editIVA);
		//addHorizontalFiels(textLCaptura, editLCaptura);
		add(textLCaptura);
		add(editLCaptura);
		addHorizontalFiels(textTotal, editTotal);
		add(pagar);
	}

	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}
	
	
	
	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();

		System.out.println(jsObject.toString());
		
		jsData = jsObject;
		
		
		String numError = jsObject.optString("numError", null);
		
		if ((numError != null)&&(numError.equals("0"))){
			
			setEditText(editBimestre, jsObject, "vbimestre", false);
			setEditText(editDerecho, jsObject, "vderdom", true);
			setEditText(editAnioBimestre, jsObject, "vanio", false);
			setEditText(editIVA, jsObject, "viva", true);
			setEditText(editTotal, jsObject, "totalPago", true);
			setEditText(editLCaptura, jsObject, "linea_captura", false);

			montoMaximoOperacion = jsObject.optString("monto_maximo_operacion", "5000.00");
			total = jsObject.optString("totalPago");
			
			editCuentaAgua.setText(sCuentaAgua);

		} else {

			String error = jsObject.optString("error", null);

			editBimestre.setText("");
			editDerecho.setText("");
			editAnioBimestre.setText("");
			editCuentaAgua.setText("");
			editIVA.setText("");
			editTotal.setText("");
			editLCaptura.setText("");

			if (error != null){
				Dialog.alert(error);
			}
		}

	}
	
	
	private void setEditText(LabelField labelField, JSONObject jsObject, String jsonLabel, boolean isNumber){
		
		String value = jsObject.optString(jsonLabel, null);
		
		if (value != null){
			
			if (isNumber){
				value = UtilNumber.formatCurrency(value);
			} 			
			labelField.setText(value);
		} else {
			labelField.setText("");
		}
	}
	

	public void sendMessage(String message) {
	}

	public void fieldChanged(Field field, int context) {

		if (field == consultar){
			
			if (UserBean.nameLogin == null){
				UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
			} else {
				getData();
			}

		} else if(field == agregar){
			agregarCuentaAgua();
		} else if(field == pagar){
			
			double dMontoMaximoOperacion = Double.parseDouble(montoMaximoOperacion);
			double dTotal = Double.parseDouble(total);
			
			
			if (dTotal <= dMontoMaximoOperacion){
				try {

					String imei = UtilBB.getImei();

					jsData.put("id_usuario", UserBean.idLogin);
					jsData.put("id_producto", DTO.STATUS_AGUA);
					jsData.put("imei", imei);
					jsData.put("tipo", UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
					jsData.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
					jsData.put("modelo_procom", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
					jsData.put("wkey", imei);
					jsData.put("cx", "0.0");
					jsData.put("cy", "0.0");
					
					String post = jsData.toString();
					String key = UtilDate.getDateToDDMMAAAA(new Date());
					
					String data = AddcelCrypto.encryptSensitive(key, post);

					UiApplication.getUiApplication().popScreen(this);
					
					UiApplication.getUiApplication().pushScreen(new VPagoSAbierto("Pago", jsData, "Agua", DTO.STATUS_AGUA));
					
				} catch (JSONException e) {
					e.printStackTrace();
					sendMessage("Error al agregar la informaci�n para mandarla.");
				}
			} else {
				
				Dialog.alert("El l�mite m�ximo para la transacci�n es: " + montoMaximoOperacion);
			}
			
	
		}
	}


	private void getData() {

		int iCuentaAgua = editChoiceAgua.getSelectedIndex();
		
		if (iCuentaAgua == 0){
			sCuentaAgua = editAgua.getText();
			if (sCuentaAgua != null){
				sCuentaAgua = sCuentaAgua.toUpperCase();
				editAgua.setText(sCuentaAgua);
			}
		} else {
			sCuentaAgua = (String) editChoiceAgua.getChoice(iCuentaAgua);
		}


		if ((sCuentaAgua != null) && (sCuentaAgua.length() > 4)) {
			splashScreen.start();

			JSONObject jsonObject = new JSONObject();

			try {
				jsonObject.put("toDO", DTO.STATUS_AGUA);
				jsonObject.put("cuenta", sCuentaAgua);

				String post = jsonObject.toString();

				String key = UtilDate.getDateToDDMMAAAA(new Date());
				
				String data = AddcelCrypto.encryptSensitive(key, post);

				PagoPlacas pagoPlacas = new PagoPlacas(data, this);
				pagoPlacas.run();

			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert("Error al interpretar los datos capturados.");
			}
		} else {
			Dialog.alert("Verifique los datos capturados");
		}
	}	

	
	private void agregarCuentaAgua() {
		BasicEditField inputField = new BasicEditField();

		Dialog d = new Dialog(Dialog.D_OK_CANCEL, "Escriba su Cuenta de Agua:",
				Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		d.add(inputField);

		int i = d.doModal();

		if (i == Dialog.OK) {

			try {
				
				String predial = inputField.getText();
				
				if (predial != null){
					DataRMS placas = new DataRMS(DataRMS.AGUA);
					predial = predial.toUpperCase();
					placas.addData(predial);
					String sPlacas[] = placas.getData();
					editChoiceAgua.setChoices(sPlacas);
				} else {
					Dialog.alert("Verificar valor de placa");
				}

			} catch (RecordStoreFullException e) {
				Dialog.alert("Ya no se puede almacenar informaci�n");
				e.printStackTrace();
			} catch (RecordStoreNotFoundException e) {
				Dialog.alert("No se encuentra en sistema de almacenamiento");
				e.printStackTrace();
			} catch (RecordStoreException e) {
				Dialog.alert("El almacenamiento no se encuentra a disposici�n");
				e.printStackTrace();
			} catch (IOException e) {
				Dialog.alert("La lectura de la informaci�n es inexacta.");
				e.printStackTrace();
			}
		}
	}

}

/*
editBimestre.setText(jsObject.optString("vbimestre"));
editDerecho.setText(UtilNumber.formatCurrency(jsObject.optString("vderdom")));
editAnioBimestre.setText(jsObject.optString("vanio"));
editCuentaAgua.setText(sCuentaAgua);
editIVA.setText(UtilNumber.formatCurrency(jsObject.optString("viva")));
editTotal.setText(UtilNumber.formatCurrency(jsObject.optString("totalPago")));
editLCaptura.setText(jsObject.optString("linea_captura"));
*/