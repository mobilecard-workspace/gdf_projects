package com.addcel.gdf.view.transacciones;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotFoundException;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.DTO;
import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.model.rms.DataRMS;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoSAbierto;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.animated.SplashScreen;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomButtonFieldManager;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.consulta.listas.predialpago.dto.Predial;
import com.addcel.gdf.view.util.UtilNumber;

public class VPredial extends CustomMainScreen implements FieldChangeListener, Viewable {

	// 056108110232 cuenta predial
	
	private ObjectChoiceField editPrediales = null;
	
	private ObjectChoiceField choicePrediales = null;
	
	private LabelField textPredial = null;
	private LabelField textConcepto = null;
	private LabelField textCuentaP = null;
	private LabelField textBimestre = null;
	private LabelField textAnio = null;

	private LabelField textLCaptura = null;
	private LabelField textImporte = null;
	private LabelField textIVA = null;
	private LabelField textReduccion = null;
	private LabelField textTotal = null;

	private ColorEditField editPredial = null;
	private HighlightsLabelField editConcepto = null;
	private HighlightsLabelField editCuentaP = null;
	private HighlightsLabelField editBimestre = null;
	private HighlightsLabelField editAnio = null;
	//private HighlightsLabelField editVencimiento = null;
	private HighlightsLabelField editLCaptura = null;
	private HighlightsLabelField editImporte = null;
	private HighlightsLabelField editIVA = null;
	private HighlightsLabelField editReduccion = null;
	private HighlightsLabelField editTotal = null;

	private SlimSelectedButtonField consultar = null;
	private SlimSelectedButtonField agregar = null;
	private SlimSelectedButtonField pagar = null;
	
	private SplashScreen splashScreen;
	private String sCuentaPredial;

	private JSONObject jsData;

	private Predial[] prediales = null;
	private Predial predial = null;
	
	private String total;
	private String montoMaximoOperacion;
	
	public VPredial(String title) {

		super(title, true);

		splashScreen = SplashScreen.getInstance();

		DataRMS placas = new DataRMS(DataRMS.PREDIAL);
		
		
		choicePrediales = new ObjectChoiceField();
		choicePrediales.setChangeListener(this);
		
		try {
			String sPrediales[] = placas.getData();
			editPrediales = new ObjectChoiceField("", sPrediales, 0);	
			editPrediales.setChangeListener(this);
		} catch (RecordStoreFullException e) {
			Dialog.alert("Ya no se puede almacenar informaci�n");
			e.printStackTrace();
		} catch (RecordStoreNotFoundException e) {
			Dialog.alert("No se encuentra en sistema de almacenamiento");
			e.printStackTrace();
		} catch (RecordStoreException e) {
			Dialog.alert("El almacenamiento no se encuentra a disposici�n");
			e.printStackTrace();
		} catch (IOException e) {
			Dialog.alert("La lectura de la informaci�n es inexacta.");
			e.printStackTrace();
		}

		textPredial = new ColorLabelField("Cuenta Predial: ", LabelField.NON_FOCUSABLE);

		textConcepto = new ColorLabelField("Concepto: ", LabelField.NON_FOCUSABLE);
		textCuentaP = new ColorLabelField("Cuenta Predial: ", LabelField.NON_FOCUSABLE);
		textBimestre = new ColorLabelField("Bimestre: ", LabelField.NON_FOCUSABLE);
		textAnio = new ColorLabelField("A�o: ", LabelField.NON_FOCUSABLE);
		//textVencimiento = new ColorLabelField("Vencimiento: ", LabelField.NON_FOCUSABLE);
		textLCaptura = new ColorLabelField("Captura: ", LabelField.NON_FOCUSABLE);
		textImporte = new ColorLabelField("Importe: ", LabelField.NON_FOCUSABLE);
		textIVA = new ColorLabelField("IVA: ", LabelField.NON_FOCUSABLE);
		textReduccion = new ColorLabelField("Reducci�n: ", LabelField.NON_FOCUSABLE);
		textTotal = new ColorLabelField("Total: ", LabelField.NON_FOCUSABLE);

		editPredial = new ColorEditField("", "");

		editConcepto = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editCuentaP = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editBimestre = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editAnio = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		//editVencimiento = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editLCaptura = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editImporte = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editIVA = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editReduccion = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);
		editTotal = new HighlightsLabelField("", DrawStyle.RIGHT|LabelField.USE_ALL_WIDTH);


		consultar = new SlimSelectedButtonField("Consultar", ButtonField.CONSUME_CLICK);
		agregar = new SlimSelectedButtonField("Agregar predial", ButtonField.CONSUME_CLICK);
		pagar = new SlimSelectedButtonField("Pagar", ButtonField.CONSUME_CLICK);
		consultar.setChangeListener(this);
		agregar.setChangeListener(this);
		pagar.setChangeListener(this);

		add(editPrediales);

		addHorizontalFiels(textPredial, editPredial);

		
		
		CustomButtonFieldManager manager = new CustomButtonFieldManager(consultar, agregar);
		manager.add(consultar);
		manager.add(agregar);
		add(manager);
		add(new LabelField(""));
		
		
		ColorLabelField textChoice = new ColorLabelField("Seleccionar el per�odo: ", LabelField.NON_FOCUSABLE);
		add(textChoice);
		add(choicePrediales);
		
		add(new LabelField(""));
		/*
		   a).-Cuenta
		   b).-A�o
		   c).-Bimestre
		   d).-Derecho
		   e).-IVA
		   f).-Linea Captura
		   g).-Total
		*/
		
		addHorizontalFiels(textCuentaP, editCuentaP);
		addHorizontalFiels(textAnio, editAnio);
		addHorizontalFiels(textBimestre, editBimestre);
		addHorizontalFiels(textImporte, editImporte);

		addHorizontalFiels(textLCaptura, editLCaptura);
		addHorizontalFiels(textTotal, editTotal);
		
		add(pagar);
	}

	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}
	
	
	
	public void setData(int request, JSONObject jsObject) {

		splashScreen.remove();

		//jsData = jsObject;
		
		if (jsObject.has("numError")){
			
			String error = jsObject.optString("error");
			
			limpiarInformacion();
			
			if (error.trim().length() > 0){
				Dialog.alert(error);
			}
			
		} else {
			
			try {
				
				
				JSONArray jsonArray = jsObject.getJSONArray("predialRespuestas");
				
				int length = jsonArray.length();
				
				prediales = new Predial[2];
				
				for(int index = 0; index < length; index++){
					
					JSONObject jsonObject = jsonArray.getJSONObject(index);
					
					
					Predial predial = new Predial();
					
					predial.setBimestre(jsonObject.optString("bimestre"));
					predial.setId_bitacora(jsonObject.optString("id_bitacora"));
					predial.setBanco(jsonObject.optString("banco"));
					predial.setImporte(jsonObject.optString("importe"));
					predial.setId_bitacora(jsonObject.optString("id_producto"));
					predial.setId_usuario(jsonObject.optString("id_usuario"));
					predial.setIntImpuesto(jsonObject.optString("intImpuesto"));
					predial.setError_descripcion(jsonObject.optString("error_descripcion"));
					predial.setStatusPago(jsonObject.optString("statusPago"));
					predial.setTotalPago(jsonObject.optString("totalPago"));
					predial.setConcepto(jsonObject.optString("concepto"));
					predial.setLinea_captura(jsonObject.optString("linea_captura"));
					predial.setError_cel(jsonObject.optString("error_cel"));
					predial.setCuentaP(jsonObject.optString("cuentaP"));
					predial.setReduccion(jsonObject.optString("reduccion"));
					predial.setVencimiento(jsonObject.optString("vencimiento"));
					predial.setJson(jsonObject);
					prediales[index] = predial;
				}
				choicePrediales.setChangeListener(null);
				choicePrediales.setChoices(prediales);
				choicePrediales.setChangeListener(this);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			predial = prediales[0];
			
			jsData = predial.getJson();

			setInformacion(jsData);
		}


	}

	private void setInformacion(JSONObject jsData){
		
		editConcepto.setText(jsData.optString("concepto"));
		editCuentaP.setText(jsData.optString("cuentaP"));
		editBimestre.setText(jsData.optString("bimestre"));
		
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		
		editAnio.setText(String.valueOf(year));
		editLCaptura.setText(jsData.optString("linea_captura"));
		editImporte.setText(UtilNumber.formatCurrency(jsData.optString("importe"))); //importe intImpuesto
		editReduccion.setText(jsData.optString("reduccion"));
		editTotal.setText(UtilNumber.formatCurrency(jsData.optString("totalPago")));
		
		
	}
	
	
	private void limpiarInformacion(){
		
		choicePrediales.setChangeListener(null);
		choicePrediales.setChoices(null);
		choicePrediales.setChangeListener(this);
		editAnio.setText("");
		editLCaptura.setText("");
		editImporte.setText(""); //importe intImpuesto
		editReduccion.setText("");
		editTotal.setText("");
		
		editConcepto.setText("");
		editCuentaP.setText("");
		editBimestre.setText("");
	}
	
	public void sendMessage(String message) {
	}

	
	public void fieldChanged(Field field, int arg) {

		if (field == consultar){
			if (UserBean.nameLogin == null){
				UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
			} else {
				getData();
			}
		} else if(field == agregar){
			agregarPredial();
		} else if(field == pagar){
			
			if(jsData == null){
				
				Dialog.alert("Seleccionar predial");
				
			} else {
				
				
				String total = jsData.optString("totalPago");
				String montoMaximoOperacion = jsData.optString("monto_maximo_operacion", "5000.00");
				
				double dTotal = Double.parseDouble(total);
				double dMontoMaximoOperacion = Double.parseDouble(montoMaximoOperacion);

				if (dTotal <= dMontoMaximoOperacion){
					try {

						String imei = UtilBB.getImei();
						
						jsData.put("id_usuario", UserBean.idLogin);
						jsData.put("id_producto", DTO.STATUS_PREDIAL);
						jsData.put("imei", imei);
						jsData.put("tipo", UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
						jsData.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
						jsData.put("modelo_procom", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
						jsData.put("wkey", imei);
						jsData.put("cx", "0.0");
						jsData.put("cy", "0.0");
						
						UiApplication.getUiApplication().popScreen(this);
						UiApplication.getUiApplication().pushScreen(new VPagoSAbierto("Pago", jsData, "Predial", DTO.STATUS_PREDIAL));
						
					} catch (JSONException e) {
						e.printStackTrace();
						sendMessage("Error al agregar la informaci�n para mandarla.");
					}
				} else {
					
					Dialog.alert("El l�mite m�ximo para la transacci�n es: " + montoMaximoOperacion);
				}
				

			}

		}  else if(field == choicePrediales){
			
			int index = choicePrediales.getSelectedIndex();
			
			System.out.println("index: " + index);
				
			Predial predial = (Predial)choicePrediales.getChoice(index);
			jsData = predial.getJson();
			setInformacion(jsData);

		} else if(field == editPrediales){
			
			int index = editPrediales.getSelectedIndex();
			
			if (index > 0){
				editPredial.setText("");
			}
		}
	}
	
	
	private void getData() {

		int iCuentaAgua = editPrediales.getSelectedIndex();
		
		if (iCuentaAgua == 0){
			sCuentaPredial = editPredial.getText();
			if (sCuentaPredial != null){
				sCuentaPredial = sCuentaPredial.toUpperCase();
				editPredial.setText(sCuentaPredial);
			}
		} else {
			sCuentaPredial = (String) editPrediales.getChoice(iCuentaAgua);
		}


		if (sCuentaPredial != null) {
			splashScreen.start();

			JSONObject jsonObject = new JSONObject();

			try {
				
				jsonObject.put("toDO", DTO.STATUS_PREDIAL);
				jsonObject.put("cuenta", sCuentaPredial);

				String post = jsonObject.toString();

				String key = UtilDate.getDateToDDMMAAAA(new Date());
				
				String data = AddcelCrypto.encryptSensitive(key, post);

				PagoPlacas pagoPlacas = new PagoPlacas(data, this);
				pagoPlacas.run();

			} catch (JSONException e) {
				e.printStackTrace();
				Dialog.alert("Error al interpretar los datos capturados.");
			}
		} else {
			Dialog.alert("Verifique los datos capturados");
		}
	}	


	private void agregarPredial() {
		
		BasicEditField inputField = new BasicEditField();

		Dialog d = new Dialog(Dialog.D_OK_CANCEL, "Escriba su Predial:", Dialog.OK, null, Dialog.DEFAULT_CLOSE);
		d.add(inputField);

		int i = d.doModal();

		if (i == Dialog.OK) {

			try {
				
				String predial = inputField.getText();
				
				if (predial != null){
					DataRMS placas = new DataRMS(DataRMS.PREDIAL);
					predial = predial.toUpperCase();
					placas.addData(predial);
					String sPlacas[] = placas.getData();
					editPrediales.setChangeListener(null);
					editPrediales.setChoices(sPlacas);
					editPrediales.setChangeListener(this);
				} else {
					Dialog.alert("Verificar valor de placa");
				}

			} catch (RecordStoreFullException e) {
				Dialog.alert("Ya no se puede almacenar informaci�n");
				e.printStackTrace();
			} catch (RecordStoreNotFoundException e) {
				Dialog.alert("No se encuentra en sistema de almacenamiento");
				e.printStackTrace();
			} catch (RecordStoreException e) {
				Dialog.alert("El almacenamiento no se encuentra a disposici�n");
				e.printStackTrace();
			} catch (IOException e) {
				Dialog.alert("La lectura de la informaci�n es inexacta.");
				e.printStackTrace();
			}
		}
	}
}


/*
VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure GDF");
pagoWeb.execute(data);
UiApplication.getUiApplication().popScreen(this);
UiApplication.getUiApplication().pushScreen(pagoWeb);
*/