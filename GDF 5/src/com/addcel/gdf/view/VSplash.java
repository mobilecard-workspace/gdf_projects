package com.addcel.gdf.view;

import java.util.Timer;
import java.util.TimerTask;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.component.BitmapField;

import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.Versionador;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.util.UtilIcon;

public class VSplash extends CustomMainScreen {

	public VSplash() {

		super(0xfad605);

		BitmapField splash = UtilIcon.getSplash();

		add(splash);
		
		Timer timer = new Timer();

		Wait wait = new Wait(this);
		
		timer.schedule(wait, 2800);
	}

	class Wait extends TimerTask {

		private VSplash viewSplash;

		public Wait(VSplash viewSplash) {

			this.viewSplash = viewSplash;
			
			Versionador versionador = new Versionador("", Url.URL_VERSIONADOR, new VInicio());
			versionador.run();
		}

		public void run() {

			synchronized (Application.getEventLock()) {
				//UiApplication.getUiApplication().popScreen(viewSplash);
				//UiApplication.getUiApplication().pushScreen(new VInicio());
			}
		}
	}
}
