package com.addcel.gdf.view;

import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.browser.field2.BrowserFieldConfig;

import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.view.components.custom.CustomMainScreen;

public class VPagoWeb extends CustomMainScreen {


	public VPagoWeb(String title) {
		super(title, true);
	}


	public void execute(String get){
		
		BrowserFieldConfig config = new BrowserFieldConfig();
        config.setProperty(BrowserFieldConfig.ENABLE_COOKIES,Boolean.TRUE);
        config.setProperty(BrowserFieldConfig.JAVASCRIPT_ENABLED, Boolean.TRUE);
        config.setProperty(BrowserFieldConfig.NAVIGATION_MODE,BrowserFieldConfig.NAVIGATION_MODE_POINTER);
        BrowserField browserField = new BrowserField(config); 
        browserField.requestContent(Url.URL_PAGO_PROSA + "?json=" + get);

	    add(browserField);
	}

}
