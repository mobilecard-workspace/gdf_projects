package com.addcel.gdf.view;

import java.util.Date;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.implementation.PagoPlacas;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.UtilDate;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorEditField;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.ColorPasswordEditField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.util.UtilNumber;

public class VPagoSAbierto extends CustomMainScreen implements FieldChangeListener, Viewable {
	
	private ColorLabelField mensajeTxt;
	
	private ColorLabelField passwordTxt;
	private ColorLabelField cvv2Txt;
	
	private ColorPasswordEditField passwordEdit;
	private ColorEditField cvv2Edit;
	
	private ColorLabelField servicioLabel;
	private ColorLabelField montoLabel;
	private ColorLabelField lineaCapturaLabel;
	
	private HighlightsLabelField servicioTxt;
	private HighlightsLabelField montoTxt;
	private HighlightsLabelField lineaCapturaTxt;

	private SlimSelectedButtonField confirma = null;

	private JSONObject jsData;
	private String servicio;
	
	private String totalPago;
	private String linea_captura;
	
	private int idServicio;
	
	public VPagoSAbierto(String title, JSONObject jsData, String servicio, int idServicio) {
		
		super(title, true);
		
		this.jsData = jsData;
		this.servicio = servicio;
		
		this.idServicio = idServicio;
		
		String importe = jsData.optString("importe", null);
		totalPago = jsData.optString("totalPago", null);
		
		if (totalPago == null){
			
			totalPago = importe;
		}

		
		linea_captura = jsData.optString("linea_captura");
		
		mensajeTxt = new ColorLabelField("Introduce tus datos de pago.", LabelField.NON_FOCUSABLE);
		
		passwordTxt = new ColorLabelField("Contrase�a Mobilecard: ", LabelField.NON_FOCUSABLE);
		cvv2Txt = new ColorLabelField("CVV2 tarjeta: ", LabelField.NON_FOCUSABLE);
		
		passwordEdit = new ColorPasswordEditField("", "", 12, EditField.FILTER_DEFAULT);
		cvv2Edit = new ColorEditField("", "", 4, EditField.FILTER_INTEGER);
		
		servicioLabel = new ColorLabelField("Servicio: ", LabelField.NON_FOCUSABLE);
		montoLabel = new ColorLabelField("Monto a pagar: ", LabelField.NON_FOCUSABLE);
		lineaCapturaLabel = new ColorLabelField("Linea de captura: ", LabelField.NON_FOCUSABLE);
		
		servicioTxt = new HighlightsLabelField(servicio, DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		montoTxt = new HighlightsLabelField(UtilNumber.formatCurrency(totalPago), DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		lineaCapturaTxt = new HighlightsLabelField(linea_captura, DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		
		add(mensajeTxt);
		
		addHorizontalFiels(servicioLabel, servicioTxt);
		addHorizontalFiels(montoLabel, montoTxt);
		
		add(lineaCapturaLabel);
		add(lineaCapturaTxt);
		
		add(cvv2Txt);
		add(cvv2Edit);
		
		add(passwordTxt);
		add(passwordEdit);
		
		confirma = new SlimSelectedButtonField("Pagar", ButtonField.CONSUME_CLICK);
		confirma.setChangeListener(this);
		add(new LabelField());
		add(confirma);
	}
	
	
	
	private void addHorizontalFiels(Field label, Field field){
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}


	public void setData(int request, JSONObject jsObject) {
		hacerAccion(jsObject);
	}

	private void hacerAccion(JSONObject jsObject){

		String token = jsObject.optString("token", null);
		int numError = jsObject.optInt("numError", -1);
		
		if (token != null){

			try {

				int iCvv2 = cvv2Edit.getTextLength();
				int iPassword = passwordEdit.getTextLength();
				
				if ((iCvv2 >=3)&&(iPassword >= 8)){
					String cvv2 = cvv2Edit.getText();
					String password = passwordEdit.getText();

					String imei = UtilBB.getImei();
					
					jsData.put("token", token);
					jsData.put("cvv2", cvv2);
					jsData.put("password", password);
					
					jsData.put("id_usuario", UserBean.idLogin);
					jsData.put("id_producto", idServicio);
					jsData.put("imei", imei);
					jsData.put("tipo", UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
					jsData.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
					jsData.put("modelo_procom", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
					jsData.put("wkey", imei);
					jsData.put("cx", "0.0");
					jsData.put("cy", "0.0");

					String key = UtilDate.getDateToDDMMAAAA(new Date());
					
					String data = "json=" + AddcelCrypto.encryptSensitive(key, jsData.toString());
					
					PagoPlacas pagoPlacas = new PagoPlacas(this, Url.URL_PAGO_PROSA, data);
					pagoPlacas.run();
				
				} else {
					
					Dialog.alert("Verifique el n�mero de caracteres del CVV2(de 3 a 4) y la contrase�a(de 8 a 12).");
				}
				
				


			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else {
			
			UiApplication.getUiApplication().pushScreen(new VDetalle("Detalle transacci�n", jsObject, totalPago, this));
		}
	}



	public void sendMessage(String message) {}


	public void fieldChanged(Field field, int context) {
		
		if (field == confirma){

			try {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put("idProveedor", 23);
				jsonObject.put("usuario", "userPrueba");
				jsonObject.put("password", "passwordPrueba");

				String key = UtilDate.getDateToDDMMAAAA(new Date());
				
				String data = "json=" + AddcelCrypto.encryptSensitive(key, jsonObject.toString());

				PagoPlacas pagoPlacas = new PagoPlacas(this, Url.URL_GDF_TOKEN, data);
				pagoPlacas.run();

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}


/*
VPagoWeb pagoWeb = new VPagoWeb("Pago 3D-Secure GDF");
pagoWeb.execute(data);
UiApplication.getUiApplication().popScreen(this);
UiApplication.getUiApplication().pushScreen(pagoWeb);

PagoPlacas pagoPlacas = new PagoPlacas(this, Url.URL_EDOMEX_PAGO, jsonObject.toString());
pagoPlacas.run();
*/