package com.addcel.gdf.view;

import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;

import org.json.me.JSONObject;

import com.addcel.gdf.view.base.Viewable;
import com.addcel.gdf.view.components.color.ColorLabelField;
import com.addcel.gdf.view.components.color.HighlightsLabelField;
import com.addcel.gdf.view.components.custom.CustomLabelButtom;
import com.addcel.gdf.view.components.custom.CustomMainScreen;
import com.addcel.gdf.view.components.custom.SlimSelectedButtonField;
import com.addcel.gdf.view.util.UtilNumber;

public class VDetalle extends CustomMainScreen implements FieldChangeListener,
		Viewable {

	private ColorLabelField errorLabel;

	private ColorLabelField folioLabel;
	private ColorLabelField autorizacionLabel;
	private ColorLabelField totalLabel;

	private HighlightsLabelField folioTxt;
	private HighlightsLabelField autorizacionTxt;
	private HighlightsLabelField totalTxt;

	private SlimSelectedButtonField reintentar;
	private SlimSelectedButtonField finalizar;
	
	private JSONObject jsObject;
	private CustomMainScreen screen;

	public VDetalle(String title, JSONObject jsObject, String totalPago, CustomMainScreen screen) {
		
		super(title, true);

		this.jsObject = jsObject;
		this.screen = screen;

		// {"autorizacion":94266,"numError":"El pago fue exitoso.","error":0,"transaccion":421,"referencia folio":111393}

		String autorizacion = jsObject.optString("autorizacion", "");
		String referencia = jsObject.optString("referencia", "");
		String error = jsObject.optString("error", "");
		int numError = jsObject.optInt("numError", -1);
		
		errorLabel = new ColorLabelField(error, LabelField.NON_FOCUSABLE);

		folioLabel = new ColorLabelField("Folio: ", LabelField.NON_FOCUSABLE);
		autorizacionLabel = new ColorLabelField("Autorizacion: ", LabelField.NON_FOCUSABLE);
		totalLabel = new ColorLabelField("Total: ", LabelField.NON_FOCUSABLE);

		folioTxt = new HighlightsLabelField(referencia, DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		autorizacionTxt = new HighlightsLabelField(autorizacion, DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);
		totalTxt = new HighlightsLabelField(UtilNumber.formatCurrency(totalPago), DrawStyle.LEFT|LabelField.USE_ALL_WIDTH);

		
		String footer = 
				"Para dudas o aclaraciones contacta al tel�fono 55403124 Ext. 113 en la Cd. " +
						"de M�xico y �rea metropolitana.";
		
		CustomLabelButtom colorLabelField = new CustomLabelButtom(footer, LabelField.NON_FOCUSABLE, true);
		
		if (numError != 0){
			add(colorLabelField);
			add(new LabelField());
		}
		
		add(errorLabel);
		addHorizontalFiels(folioLabel, folioTxt);
		addHorizontalFiels(autorizacionLabel, autorizacionTxt);
		addHorizontalFiels(totalLabel, totalTxt);

		reintentar = new SlimSelectedButtonField("Regresar", ButtonField.CONSUME_CLICK);
		finalizar = new SlimSelectedButtonField("Finalizar", ButtonField.CONSUME_CLICK);
		reintentar.setChangeListener(this);
		finalizar.setChangeListener(this);
		
		add(new LabelField());
		
		if (numError == 0){
			add(finalizar);
		} else {
			add(reintentar);
		}
	}

	private void addHorizontalFiels(Field label, Field field) {
		HorizontalFieldManager manager = new HorizontalFieldManager();
		manager.add(label);
		manager.add(field);
		add(manager);
	}

	public void setData(int request, JSONObject jsObject) {

	}

	public void sendMessage(String message) {

	}

	public void fieldChanged(Field arg0, int arg1) {
		
		if (arg0 == finalizar){
			
			UiApplication.getUiApplication().popScreen(this);
			
			UiApplication.getUiApplication().invokeLater(new Runnable() {
				public void run() {
					UiApplication.getUiApplication().popScreen(screen);
				}
			});
			
		} else if (arg0 == reintentar){
			UiApplication.getUiApplication().popScreen(this);
		}
	}
}
