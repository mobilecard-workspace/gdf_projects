package com.addcel.gdf.view.components.color.title;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.addcel.gdf.view.util.UtilColor;

public class IconLabelField extends LabelField{

	private String text;
	int width = 0;
	int color;
	
	public IconLabelField(String text){
		super("", LabelField.NON_FOCUSABLE|LabelField.USE_ALL_WIDTH|LabelField.FIELD_HCENTER);
		this.text = text;
		width = Display.getWidth();
		color = UtilColor.LABEL_FIELD_STRING;
		
		//Background background = BackgroundFactory.createSolidBackground(UtilColor.TITLE_YELLOW);
		Background background = BackgroundFactory.createLinearGradientBackground(UtilColor.TITLE_YE_TOP_LEFT, UtilColor.TITLE_YE_TOP_RIGHT, UtilColor.TITLE_YE_BOTTOM_RIGHT, UtilColor.TITLE_YE_BOTTOM_LEFT);
		setBackground(background);
	}

	
	public IconLabelField(String text, long style ){
		super("", style);
		this.text = text;
		width = Display.getWidth();
		color = UtilColor.LABEL_FIELD_STRING;
	}
	
	
	public IconLabelField(String text, long style, int color){
		super("", style);
		this.text = text;
		width = Display.getWidth();
		this.color = color;
	}

	public void paint(Graphics graphics) {

		graphics.setColor(Color.WHITE);
		graphics.drawText(text, 0, 0, DrawStyle.HCENTER, width);

		super.paint(graphics);
	}
}