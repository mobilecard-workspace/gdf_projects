package com.addcel.gdf.view.components.color;



import com.addcel.gdf.view.util.UtilColor;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.PasswordEditField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class ColorPasswordEditField extends PasswordEditField  {

	public ColorPasswordEditField(String label, String initialValue){
		super(label, initialValue);
		background();
	}
	
	public ColorPasswordEditField(String label, String initialValue, int maxNumChars, long style){
		super(label, initialValue, maxNumChars, style);
		background();
	}
	
	private void background(){
		Background background = BackgroundFactory.createSolidBackground(UtilColor.EDIT_FIELD_BACKGROUND);
		setBackground(background);
	}
	
	
	public void paint(Graphics graphics) {
	    graphics.setColor(UtilColor.EDIT_FIELD_STRING);
	    super.paint(graphics);
	}
	
	
}
