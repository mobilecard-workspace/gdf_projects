package com.addcel.gdf.view.components.color;



import com.addcel.gdf.view.util.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class ColorRichTextField extends RichTextField{

	private String title;
	private int width = 0;
	
	
	public ColorRichTextField(String title){
		super(RichTextField.NON_FOCUSABLE);
		this.title = title;
		this.width = Display.getWidth();
	}
	
	
	protected void paint(Graphics graphics) {
		graphics.setBackgroundColor(UtilColor.EDIT_FIELD_BACKGROUND);
		graphics.clear();
		graphics.setColor(UtilColor.EDIT_FIELD_STRING);
		graphics.drawText(title, 0, 0,
				DrawStyle.HCENTER, width);

		super.paint(graphics);
	}
}
