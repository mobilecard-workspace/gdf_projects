package com.addcel.gdf.view.components.color;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.addcel.gdf.dto.Comunicado;
import com.addcel.gdf.view.util.UtilColor;

public class ComunicadosRichTextField extends RichTextField {

	private Comunicado comunicado;
	
	private String contenido = "";
	private int color = UtilColor.EDIT_FIELD_STRING;
	private int background = UtilColor.EDIT_FIELD_BACKGROUND;
	private int fuente = 11;
	private int width;
	
	public ComunicadosRichTextField(){
		super(RichTextField.NON_FOCUSABLE);
		this.width = Display.getWidth();
		
		Font bold = this.getFont().derive(Font.BOLD);
        this.setFont(bold);
	}


	protected void paint(Graphics graphics) {
		graphics.setColor(color);
		super.paint(graphics);
	}

	
	
	public Comunicado getComunicado() {
		return comunicado;
	}

	public void setComunicado(Comunicado comunicado) {
		
		contenido = comunicado.getContenido();
		
		setText(contenido);
		
		String bgColor = comunicado.getBgColor();
		String fgColor = comunicado.getFgColor();
		String tamFuente = comunicado.getTamFuente();
		
		color = Integer.valueOf(fgColor, 16).intValue();
		background = Integer.valueOf(bgColor, 16).intValue();
		fuente = Integer.valueOf(tamFuente).intValue();

		Background bg = BackgroundFactory.createSolidBackground(background);
		setBackground(bg);
		
		this.comunicado = comunicado;
	}
}
