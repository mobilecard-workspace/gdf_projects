package com.addcel.gdf.view.components.custom;

public interface FieldManagerObserver {

	public void notify(CustomSelectedSizeButton button);
}
