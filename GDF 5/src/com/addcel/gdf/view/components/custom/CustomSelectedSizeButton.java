package com.addcel.gdf.view.components.custom;

import com.addcel.gdf.view.util.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.component.ButtonField;

public class CustomSelectedSizeButton extends Field {
	
	private String label;

	private boolean isSelected = false;
	
	private int sizeWidth;
	private int sizeHeigth;
	private int numberElements;
	
	private int preferredHeight;
	
	private int fontHeight;
	
	private FieldManagerObserver managerObserver; 

	public CustomSelectedSizeButton(String label, int numberElements) {
		super(ButtonField.CONSUME_CLICK|ButtonField.FIELD_HCENTER);
		this.label = label;
		this.numberElements = numberElements;
		this.sizeWidth = Display.getWidth()/numberElements;
		this.fontHeight = getFont().getHeight();
		this.preferredHeight = fontHeight * 2;
	}
	
	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
		invalidate();
	}

	public int getPreferredHeight() {
		return preferredHeight;
	}

	public int getPreferredWidth() {
		return sizeWidth;
	}

	protected void layout(int width, int height) {
		setExtent(Math.min(width, getPreferredWidth()), Math.min(height, getPreferredHeight()));
	}

	protected void paint(Graphics graphics) {
		
		if (isFocus()){
			graphics.setColor(UtilColor.BUTTON_FOCUS);
			graphics.fillRect(0, 0, getWidth(), getHeight());
			graphics.setColor(UtilColor.BUTTON_STRING);
			graphics.drawText(label, 0, fontHeight/2, DrawStyle.HCENTER, sizeWidth);
		} else if (isSelected) {
			graphics.setColor(UtilColor.BUTTON_SELECTED);
			graphics.fillRect(0, 0, getWidth(), getHeight());
			graphics.setColor(UtilColor.BUTTON_STRING);
			graphics.drawText(label, 0, fontHeight/2, DrawStyle.HCENTER, sizeWidth);
		} else {
			graphics.setColor(UtilColor.BUTTON_UNSELECTED);
			graphics.fillRect(0, 0, getWidth(), getHeight());
			graphics.setColor(UtilColor.BUTTON_STRING);
			graphics.drawText(label, 0, fontHeight/2, DrawStyle.HCENTER, sizeWidth);
		}
		
		graphics.setColor(Color.WHITE);
		graphics.drawLine(0, getHeight()-2, getWidth(), getHeight()-2);
		graphics.drawLine(0, getHeight()-1, getWidth(), getHeight()-1);
		graphics.drawLine(0, getHeight(), getWidth(), getHeight());
	}

	public boolean isFocusable() {
		return true;
	}
	
	public boolean isSelectable() {
		return true;
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
	
	protected boolean navigationClick(int status, int time) {
		setEvent();
		return true;
	}
	
	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			setEvent();
			return true;
		}
		return super.keyChar(character, status, time);
	}
	
	
	private void setEvent(){
		
		fieldChangeNotify(0);
		isSelected = !isSelected;
		if (managerObserver != null){
			managerObserver.notify(this);
		}
	}

	public void setManagerObserver(FieldManagerObserver managerObserver) {
		this.managerObserver = managerObserver;
	}
}
