package com.addcel.gdf.view.components.custom;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.component.LabelField;

import com.addcel.gdf.view.util.UtilColor;

public class CustomLabelButtom extends LabelField{

	private String text;
	int width = 0;
	int color;
	
	public CustomLabelButtom(String text){
		super(text, LabelField.NON_FOCUSABLE);
		this.text = text;
		width = Display.getWidth();
		color = UtilColor.LABEL_FIELD_STRING;
		
		Font font = Font.getDefault().derive(Font.PLAIN,6,Ui.UNITS_pt);
		this.setFont(font);
	}

	
	public CustomLabelButtom(String text, long style, boolean customFont){
		super(text, style);
		this.text = text;
		width = Display.getWidth();
		color = UtilColor.LABEL_FIELD_STRING;
		
		if (customFont){
			Font font = Font.getDefault().derive(Font.PLAIN,6,Ui.UNITS_pt);
			this.setFont(font);
		}
	}
	
	
	private int indexChoice = -1;
	
	public CustomLabelButtom(String text, long style, int indexChoice){
		super(text, style);
		this.text = text;
		width = Display.getWidth();
		color = UtilColor.LABEL_FIELD_STRING;
		this.setIndexChoice(indexChoice);
	}
	
	
	
	public CustomLabelButtom(String text, long style, int color, boolean customFont){
		super(text, style);
		this.text = text;
		width = Display.getWidth();
		this.color = color;
		
		if (customFont){
			Font font = Font.getDefault().derive(Font.PLAIN,6,Ui.UNITS_pt);
			this.setFont(font);
		}
	}

	public void paint(Graphics graphics) {

		graphics.setColor(color);
		graphics.drawText(text, 0, 0, DrawStyle.LEFT, width);

		super.paint(graphics);
	}

	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
		super.setText(text);
		invalidate();
	}


	public boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		return true;
	}
	
	public boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			return true;
		}
		return super.keyChar(character, status, time);
	}

	public boolean isFocusable() {
		return true;
	}


	public int getIndexChoice() {
		return indexChoice;
	}


	public void setIndexChoice(int indexChoice) {
		this.indexChoice = indexChoice;
	}
}