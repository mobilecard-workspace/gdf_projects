package com.addcel.gdf.view.components.custom;


import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.TransitionContext;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.UiEngineInstance;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.addcel.InfoThread.CreditInfoThread;
import com.addcel.gdf.view.VAutenticacion;
import com.addcel.gdf.view.VPagoWeb;
import com.addcel.gdf.view.components.color.title.IconLabelField;
import com.addcel.gdf.view.components.color.title.TitleLabelField;
import com.addcel.gdf.view.password.VRecuperarContrasenia;
import com.addcel.gdf.view.util.UtilColor;
import com.addcel.gdf.view.util.UtilIcon;


public class CustomMainScreen extends MainScreen{

	protected Font font = null;
	private int width;
	//private int height;

	public CustomMainScreen(String title, boolean insertMenu){
		
		super(Screen.DEFAULT_CLOSE);

	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		
		setBackground(UtilColor.BACKGROUND);

		width = Display.getWidth();
		//height = Display.getHeight();

		Background background = BackgroundFactory.createLinearGradientBackground(UtilColor.TITLE_IMAGE_TOP_LEFT, UtilColor.TITLE_IMAGE_TOP_RIGHT, UtilColor.TITLE_IMAGE_BOTTOM_RIGHT, UtilColor.TITLE_IMAGE_BOTTOM_RIGHT);

		TitleLabelField labelField = new TitleLabelField(title);
		add(labelField);

		BitmapField imageDF = UtilIcon.getTitle();

		HorizontalFieldManager hFieldManager = new HorizontalFieldManager(HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER);
		hFieldManager.setBackground(background);
		
		hFieldManager.add(imageDF);
		add(hFieldManager);
		
		add(new IconLabelField(""));
		add(new LabelField(""));
		
		if (insertMenu){
			addMenuItem(mIniciarSesion);
			addMenuItem(mRegistro);
			addMenuItem(mEditarRegistro);
		}
	}

	
	public CustomMainScreen(String title, boolean insertMenu, boolean inico){
		
		super(Screen.DEFAULT_CLOSE);

	    TransitionContext transition = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition.setIntAttribute(TransitionContext.ATTR_DURATION, 300);
	    transition.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_LEFT);
	    transition.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    UiEngineInstance engine = Ui.getUiEngineInstance();
	    engine.setTransition(null, this, UiEngineInstance.TRIGGER_PUSH, transition);
	    
	    TransitionContext transition2 = new TransitionContext(TransitionContext.TRANSITION_SLIDE);
	    transition2.setIntAttribute(TransitionContext.ATTR_DURATION, 500);
	    transition2.setIntAttribute(TransitionContext.ATTR_DIRECTION, TransitionContext.DIRECTION_RIGHT);
	    transition2.setIntAttribute(TransitionContext.ATTR_STYLE, TransitionContext.STYLE_PUSH);
	    
	    engine.setTransition(this, null, UiEngineInstance.TRIGGER_POP, transition2);
	    engine.setAcceptableDirections(Display.DIRECTION_PORTRAIT);
		
		setBackground(UtilColor.BACKGROUND);

		width = Display.getWidth();
		//height = Display.getHeight();

		Background background = BackgroundFactory.createLinearGradientBackground(UtilColor.TITLE_IMAGE_TOP_LEFT, UtilColor.TITLE_IMAGE_TOP_RIGHT, UtilColor.TITLE_IMAGE_BOTTOM_RIGHT, UtilColor.TITLE_IMAGE_BOTTOM_RIGHT);

		TitleLabelField labelField = new TitleLabelField(title);
		add(labelField);

		BitmapField imageDF = null;
		
		if (inico){
			imageDF = UtilIcon.getTitleInicio();
		} else {
			imageDF = UtilIcon.getTitle();
		}
	
		HorizontalFieldManager hFieldManager = new HorizontalFieldManager(HorizontalFieldManager.USE_ALL_WIDTH|Field.FIELD_VCENTER);
		hFieldManager.setBackground(background);
		
		hFieldManager.add(imageDF);
		add(hFieldManager);
		
		add(new IconLabelField(""));
		add(new LabelField(""));
		
		if (insertMenu){
			addMenuItem(mIniciarSesion);
			addMenuItem(mRegistro);
			addMenuItem(mEditarRegistro);
			addMenuItem(mRecuperarContrasenia);
		}
	}

	public CustomMainScreen(int color){
		super(Screen.DEFAULT_CLOSE);
		setBackground(color);
		width = Display.getWidth();
		//height = Display.getHeight();
	}


	public int getPreferredWidth(){
		return width;
	}

	public boolean onSavePrompt(){
	    return true;
	} 
	
	public void setBackground(int color){
		
    	VerticalFieldManager manager = null;
    	Background background = null;
    	
    	manager = (VerticalFieldManager)getMainManager();
		background = BackgroundFactory.createSolidBackground(color);
		manager.setBackground(background);
	}

	public Font setFont(){
		
		Font font = null;  
		FontFamily alphaSansFamily;
		try {
			alphaSansFamily = FontFamily.forName("BBAlpha Sans Condensed");
		   	font = alphaSansFamily.getFont(Font.PLAIN, 7, Ui.UNITS_pt);
			setFont(font);
		} catch (ClassNotFoundException e) {
			Dialog.alert("No se pudo cargar la Fuente");
		}
	   	
	   	return font;
	}
	
	
	
	private MenuItem mIniciarSesion = new MenuItem("Ingresar", 110, 10) {
		public void run() {
			UiApplication.getUiApplication().pushScreen(new VAutenticacion());
		}
	};
	
	private MenuItem mRegistro = new MenuItem("Registro", 110, 10) {
		public void run() {
			
			CreditInfoThread creditInfoThread = new CreditInfoThread("proveedores", CreditInfoThread.REGISTRO);
			creditInfoThread.run();

		}
	};
	
	private MenuItem mEditarRegistro = new MenuItem("Actualizar Registro", 110, 10) {
		public void run() {
			
			if (UserBean.nameLogin == null) {
				UiApplication.getUiApplication().pushScreen(new VAutenticacion(new VPagoWeb("Pago 3D-Secure GDF")));
			} else {
				CreditInfoThread creditInfoThread = new CreditInfoThread("proveedores", CreditInfoThread.ACTUALIZACION);
				creditInfoThread.run();
			}
		}
	};
	
	
	private MenuItem mRecuperarContrasenia = new MenuItem("Recuperar contraseņa", 110, 10) {
		public void run() {
			

			UiApplication.getUiApplication().pushScreen(new VRecuperarContrasenia());
			
			

		}
	};
}
