package com.addcel.gdf.view.components.custom;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;

import com.addcel.gdf.view.util.UtilColor;

public class SlimSelectedButtonField extends Field {

	private String label;
	private boolean isSelected = false;

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
		invalidate();
	}

	public SlimSelectedButtonField(String label, long style) {
		super(style);
		this.label = label;
	}
	
	public int getPreferredHeight() {
		return getFont().getHeight() + 8;
	}

	public int getPreferredWidth() {
		return Display.getWidth()/2;
	}

	protected void layout(int width, int height) {
		setExtent(Math.min(width, getPreferredWidth()), Math.min(height, getPreferredHeight()));
	}

	protected void paint(Graphics graphics) {

		if (isFocus()){
			graphics.setColor(UtilColor.SLIM_BUTTON_FOCUS);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(Color.WHITE);
			graphics.drawText(label, 4, 4, DrawStyle.HCENTER);
		} else if (isSelected) {
			graphics.setColor(UtilColor.SLIM_BUTTON_SELECTED);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(Color.WHITE);
			graphics.drawText(label, 4, 4, DrawStyle.HCENTER);
		} else {
			graphics.setColor(UtilColor.SLIM_BUTTON_UNSELECTED);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(Color.WHITE);
			graphics.drawText(label, 4, 4, DrawStyle.HCENTER);
		}
	}


	public boolean isFocusable() {
		return true;
	}

	
	public boolean isSelectable() {
		return true;
	}


	protected void drawFocus(Graphics graphics, boolean on) {
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
	
	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		isSelected = !isSelected;
		return true;
	}
	
	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			isSelected = !isSelected;
			return true;
		}
		return super.keyChar(character, status, time);
	}
}

