package com.addcel.gdf.view.components.custom;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.container.HorizontalFieldManager;

public class CustomButtonFieldManager extends HorizontalFieldManager{

	private Field basicFlight;
	private Field roundFlight;
	private int width;
	private int preferredWidth;

	public Field getBasicFlight(){
		return basicFlight;
	}
	
	public Field getRoundFlight(){
		return roundFlight;
	}
	
	public CustomButtonFieldManager(Field basicFlight, Field roundFlight){
		
		super();
		width = Display.getWidth();
		preferredWidth = width/2;
		this.basicFlight = basicFlight;
		this.roundFlight = roundFlight;
	}
	
	public int getPreferredWidth() {
		return preferredWidth;
	}
}