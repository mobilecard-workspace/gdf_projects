package com.addcel.gdf.view.components.custom;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.DialogFieldManager;


public final class CustomDialog extends Dialog {

	private EditField placas;

	public CustomDialog(String choices[], int values[]) {
		
		super("Enter a username", choices, values, Dialog.OK, Bitmap.getPredefinedBitmap(Bitmap.INFORMATION), Dialog.GLOBAL_STATUS);
		placas = new EditField("Placa: ", "", 50, EditField.EDITABLE);
		net.rim.device.api.ui.Manager delegate = getDelegate();
		
		if (delegate instanceof DialogFieldManager) {
			DialogFieldManager dfm = (DialogFieldManager) delegate;
			net.rim.device.api.ui.Manager manager = dfm.getCustomManager();
			if (manager != null) {
				manager.insert(placas, 0);
			}
		}
	}

	public String getPlaca() {
		return placas.getText();
	}

}

/*
public class CustomDialog extends UiApplication implements Runnable {


	public void run() {

		final int answer = Dialog.ask(Dialog.D_YES_NO, "continue?");

		if (answer == Dialog.YES) {
			System.out.println("user clicked yes");
		} else {
			System.exit(0);
		}

		pushScreen(new MyScreen("App loaded"));
	}

	class MyScreen extends MainScreen {

		public MyScreen(final String msg) {

			final LabelField title = new LabelField("First Screen", LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH);
			setTitle(title);
			this.add(new RichTextField(msg));

		}

	}

}

*/
/*
public static void main(final String[] args) {

	final CustomDialog test = new CustomDialog();
	test.invokeLater(test);

	test.enterEventDispatcher();

}
*/