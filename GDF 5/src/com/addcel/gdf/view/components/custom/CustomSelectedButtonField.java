package com.addcel.gdf.view.components.custom;


import com.addcel.gdf.view.util.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;

public class CustomSelectedButtonField extends Field {
	
	private String label;
	private boolean isSelected = false;
	
	private int preferredHeight;
	private int preferredWidth;
	
	public CustomSelectedButtonField(String label, long style) {
		super(style);
		this.label = label;
		
		this.preferredHeight = getFont().getHeight() + 8;
		this.preferredWidth = Display.getWidth()/2;
	}
	
	public int getPreferredHeight() {
		return preferredHeight;
	}

	public int getPreferredWidth() {
		return preferredWidth;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
		invalidate();
	}

	protected void layout(int width, int height) {
		setExtent(Math.min(width, getPreferredWidth()), Math.min(height, getPreferredHeight()));
	}

	protected void paint(Graphics graphics) {

		
		if (isFocus()){
			graphics.setColor(UtilColor.BUTTON_FOCUS);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(Color.WHITE);
			graphics.drawText(label, 4, 4, DrawStyle.HCENTER);
		} else if (isSelected) {
			graphics.setColor(UtilColor.BUTTON_UNSELECTED);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(Color.WHITE);
			graphics.drawText(label, 4, 4, DrawStyle.HCENTER);
		} else {
			graphics.setColor(UtilColor.BUTTON_UNSELECTED);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(Color.WHITE);
			graphics.drawText(label, 4, 4, DrawStyle.HCENTER);
		}
	}


	public boolean isFocusable() {
		return true;
	}

	
	public boolean isSelectable() {
		return true;
	}


	protected void drawFocus(Graphics graphics, boolean on) {
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
	
	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		isSelected = !isSelected;
		return true;
	}
	
	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			isSelected = !isSelected;
			return true;
		}
		return super.keyChar(character, status, time);
	}
	
	
	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK){
			fieldChangeNotify(0);
			isSelected = !isSelected;
			return true;
		}

		return super.touchEvent(message);
	}
}

