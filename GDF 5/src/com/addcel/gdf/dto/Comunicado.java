package com.addcel.gdf.dto;

public class Comunicado {

	private String contenido;
	private String fgColor;
	private String bgColor;
	private String tamFuente;
	
	public String getContenido() {
		
		if (contenido == null){
			contenido = "";
		}
		
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public String getFgColor() {
		return fgColor;
	}
	public void setFgColor(String fgColor) {
		this.fgColor = fgColor;
	}
	public String getBgColor() {
		return bgColor;
	}
	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}
	public String getTamFuente() {
		return tamFuente;
	}
	public void setTamFuente(String tamFuente) {
		this.tamFuente = tamFuente;
	}
}
