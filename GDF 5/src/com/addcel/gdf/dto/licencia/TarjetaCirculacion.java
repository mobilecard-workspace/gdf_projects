package com.addcel.gdf.dto.licencia;

public class TarjetaCirculacion {

	private String statusPago;
	private String linea_captura;
	private int id_producto;
	private int id_usuario;
	private int id_bitacora;
	private int clave;
	private String concepto;
	private String descuento;
	private String importeFinal;
	private String placa;
	private String importe;
	private int estado;
	private String marca;
	private int modelo;
	private String nombreSubConcepto;
	private String vigencia;
	private String importeInicial;
	private String error;
	private String totalPago;
	private String banco;
	
	
	public String getStatusPago() {
		return statusPago;
	}
	public void setStatusPago(String statusPago) {
		this.statusPago = statusPago;
	}
	public String getLinea_captura() {
		return linea_captura;
	}
	public void setLinea_captura(String linea_captura) {
		this.linea_captura = linea_captura;
	}
	public int getId_producto() {
		return id_producto;
	}
	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getId_bitacora() {
		return id_bitacora;
	}
	public void setId_bitacora(int id_bitacora) {
		this.id_bitacora = id_bitacora;
	}
	public int getClave() {
		return clave;
	}
	public void setClave(int clave) {
		this.clave = clave;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public String getDescuento() {
		return descuento;
	}
	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}
	public String getImporteFinal() {
		return importeFinal;
	}
	public void setImporteFinal(String importeFinal) {
		this.importeFinal = importeFinal;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public int getModelo() {
		return modelo;
	}
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}
	public String getNombreSubConcepto() {
		return nombreSubConcepto;
	}
	public void setNombreSubConcepto(String nombreSubConcepto) {
		this.nombreSubConcepto = nombreSubConcepto;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getImporteInicial() {
		return importeInicial;
	}
	public void setImporteInicial(String importeInicial) {
		this.importeInicial = importeInicial;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getTotalPago() {
		return totalPago;
	}
	public void setTotalPago(String totalPago) {
		this.totalPago = totalPago;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
}
