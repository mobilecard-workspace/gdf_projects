package com.addcel.gdf.dto.licencia;

public class MarcaCarro {

	private String id_marca;
	private String marca;
	
	public String getId_marca() {
		return id_marca;
	}
	public void setId_marca(String id_marca) {
		this.id_marca = id_marca;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String toString(){
		return marca;
	}
}
