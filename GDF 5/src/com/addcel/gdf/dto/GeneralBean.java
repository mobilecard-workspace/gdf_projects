package com.addcel.gdf.dto;

public class GeneralBean  {

	private String description = "";
	private String clave = "";
	private String claveE = "";
	private String path = "";	
	private String claveWS = "";
	private String compatibleAmex = "";
	private String compatibleVM = "";
	private String name = "";	


	public GeneralBean(String description, String clave, String path,
			String claveWS,String compatible,String compatiblevisa) {
		super();
		this.compatibleVM=compatiblevisa;
		this.description = description;
		this.clave = clave;
		this.compatibleAmex=compatible;
		this.path = path;
		this.claveWS = claveWS;
	}
	public GeneralBean(String description, String clave,String name,String claveE) {
		super();
		this.name=name;
		this.description = description;
		this.clave = clave;
		this.claveE=claveE;
	}
	public GeneralBean(String description, String clave,String name) {
		super();
		this.name=name;
		this.description = description;
		this.clave = clave;
	}
	public GeneralBean(String description, String clave) {
		super();
		this.description = description;
		this.clave = clave;
	}
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String toString(){
		return description;
	}
	
	public String getvisa() {
		return compatibleVM;
	}

	public void setvisa(String path) {
		this.compatibleVM = path;
	}
	public String getamex() {
		return compatibleAmex;
	}

	public void setamex(String path) {
		this.compatibleAmex = path;
	}
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getClaveWS() {
		return claveWS;
	}

	public void setClaveWS(String claveWS) {
		this.claveWS = claveWS;
	}
	
	public String getClaveE() {
		return claveE;
	}

	public void setClaveE(String claveWS) {
		this.claveE = claveWS;
	}

	public GeneralBean() {
		super();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String description) {
		this.name = description;
	}

	
	public String getClave() {
		return clave;
	}
	
	public void setClave(String clave) {
		this.clave = clave;
	}
}
