package com.addcel.gdf.dto;

public class UsuarioTag  {
    
    private String usuario;
    private String tipotag;
    private String etiqueta;
    private String numero;
    private String dv;
  
    
    
    public UsuarioTag(double cantidad){
    }
    
    

    public UsuarioTag(String usuario,String tipotag,String etiqueta,String numero,String dv){
    	this.usuario=usuario;
    	this.tipotag=tipotag;
    	this.etiqueta=etiqueta;
    	this.numero=numero;
    	this.dv=dv;
    }


    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    
        
    public String getTipoTag() {
        return tipotag;
    }

    public void setTipoTag(String tipotag) {
        this.tipotag = tipotag;
    }
    
    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }
    
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
    
    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }
      

}

