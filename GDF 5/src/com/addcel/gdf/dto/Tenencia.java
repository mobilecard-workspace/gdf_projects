package com.addcel.gdf.dto;

public class Tenencia {
	
	private String placa;			//Placa del Vehiculo.
	private String ejercicio2;		//A�o de la tenencia que se quiere pagar.
	private boolean condonacion;	//bandera para descuento por condonacion.
	private boolean interes;		//bandera para calcular interes.
	private boolean subsidio;		//bandera para otorgar subsidio.
	
	private String error = "0";		//nada o 0 = no hay errores, 1 = tiene error.
	private int ejercicio;	        //A�o de la tenencia que se quiere pagar.
	private String descrip; 		//Descripcion del error.
	private	int modelo;		        //modelo del vehiculo que porta la placa.
	private int total;		        //Importe final con la sumas y restas correspondientes.
	private String lineacaptura;	//20 digitos de referencia para el pago(L�nea Captura).
	private long vigencia;			//Fecha de vigencia de la linea de captura.
	
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getEjercicio2() {
		return ejercicio2;
	}
	public void setEjercicio2(String ejercicio2) {
		this.ejercicio2 = ejercicio2;
	}
	public boolean isCondonacion() {
		return condonacion;
	}
	public void setCondonacion(boolean condonacion) {
		this.condonacion = condonacion;
	}
	public boolean isInteres() {
		return interes;
	}
	public void setInteres(boolean interes) {
		this.interes = interes;
	}
	public boolean isSubsidio() {
		return subsidio;
	}
	public void setSubsidio(boolean subsidio) {
		this.subsidio = subsidio;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public int getEjercicio() {
		return ejercicio;
	}
	public void setEjercicio(int ejercicio) {
		this.ejercicio = ejercicio;
	}
	public String getDescrip() {
		return descrip;
	}
	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}
	public int getModelo() {
		return modelo;
	}
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getLineacaptura() {
		return lineacaptura;
	}
	public void setLineacaptura(String lineacaptura) {
		this.lineacaptura = lineacaptura;
	}
	public long getVigencia() {
		return vigencia;
	}
	public void setVigencia(long vigencia) {
		this.vigencia = vigencia;
	}
}
