package com.addcel.gdf.model.rms;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Vector;

import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotFoundException;

public class DataRMS {

	public final static int PLACAS = 0;
	public final static int PREDIAL = 1;
	public final static int AGUA = 2;
	
	public final static String SPLACAS = "placa";
	public final static String SPREDIAL = "predial";
	public final static String SAGUA = "agua";
	
	private RecordStore rs;
	private String nameRS;

	
	public DataRMS(int selector){
		
		switch (selector) {
		case PLACAS:
			nameRS  = SPLACAS;
			break;

		case PREDIAL:
			nameRS  = SPREDIAL;
			break;
			
		case AGUA:
			nameRS  = SAGUA;
			break;
		}
	}
	
	
	public void addData(String data) throws RecordStoreFullException,
			RecordStoreNotFoundException, RecordStoreException, IOException {

		rs = RecordStore.openRecordStore(nameRS, true);

		byte[] record;

		ByteArrayOutputStream strmBytes = new ByteArrayOutputStream();
		DataOutputStream strmDataType = new DataOutputStream(strmBytes);

		strmDataType.writeUTF(data);
		strmDataType.flush();

		record = strmBytes.toByteArray();
		rs.addRecord(record, 0, record.length);

		strmBytes.reset();

		strmBytes.close();
		strmDataType.close();

		rs.closeRecordStore();
	}


	public String[] getData() throws RecordStoreFullException,
			RecordStoreNotFoundException, RecordStoreException, IOException {

		Vector vector = new Vector();
		String placas[] = null;

		vector.addElement("Ingreso directo de " + nameRS);
		
		rs = RecordStore.openRecordStore(nameRS, true);

		RecordEnumeration enumeration = rs.enumerateRecords(null, null, false);
		while (enumeration.hasNextElement()) {

			byte[] record = enumeration.nextRecord();
			ByteArrayInputStream bais = new ByteArrayInputStream(record);
			DataInputStream dis = new DataInputStream(bais);
			String data = dis.readUTF();
			vector.addElement(data);
		}

		placas = new String[vector.size()];
		vector.copyInto(placas);
		rs.closeRecordStore();

		return placas;
	}


	public void deleteData() throws RecordStoreFullException, RecordStoreNotFoundException, RecordStoreException{

		rs = RecordStore.openRecordStore(nameRS, true);

		RecordEnumeration enumeration = rs.enumerateRecords(null, null, false);
		while (enumeration.hasNextElement()) {

			int id = enumeration.nextRecordId();
			rs.deleteRecord(id);
		}

		rs.closeRecordStore();

		RecordStore.deleteRecordStore(nameRS);
	}
}

