package com.addcel.gdf.model.connection;

public class Url {
	
	public static String URL = "http://50.57.192.214:8080";
	//public static String URL = "http://www.mobilecard.mx:8080";

	
	public static String GDF_TIPO_CERTIFICADOS = URL + "/GDFServices/getCertificados";
	
	
	
	public static String GDF_CIRCULACION_MARCAS = URL + "/GDFServices/consultaCatalogoMarcas";

	public static String URL_GDF = URL + "/GDFServices/Consumidor";
	
	public static String URL_GDF_TOKEN = URL + "/GDFServices/getToken";
	public static String URL_PAGO_PROSA = URL + "/GDFServices/ProsaPago";
	public static String URL_VERSIONADOR = "http://www.mobilecard.mx:8080/Vitamedica/getVersion?idApp=4&idPlataforma=3";
	public static String URL_GDF_CONSULTA_PAGOS = URL + "/GDFServices/ConsultaPagos";
	public static String URL_GDF_COMUNICADOS = URL + "/GDFServices/BuscaComunicados";
	//public static String URL_GDF = "http://" + URL + "/GDFServices/Consumidor";

	public static String URL_GDF_REENVIO = URL + "/GDFServices/ReenvioReciboGDF";
	
	public static String URL_USER_INSERT = URL + "/AddCelBridge/Servicios/adc_userInsert.jsp";

	public static String URL_LOGIN = URL + "/AddCelBridge/Servicios/adc_userLogin.jsp";

	public static String URL_GET_BANKS = URL + "/AddCelBridge/Servicios/adc_getBanks.jsp";
	public static String URL_GET_CARDTYPES = URL + "/AddCelBridge/Servicios/adc_getCardType.jsp";
	public static String URL_GET_PROVIDERS = URL + "/AddCelBridge/Servicios/adc_getProviders.jsp";
	public static String URL_GET_ESTADOS = URL + "/AddCelBridge/Servicios/adc_getEstados.jsp";
	public static String URL_GET_USERDATA = URL + "/AddCelBridge/Servicios/adc_getUserData.jsp";

	public static String ADC_USER_UPDATE = URL + "/AddCelBridge/Servicios/adc_userUpdate.jsp";
	
	public static String URL_CONSUMER = URL + "/GatewayGDF/Consumer";
	public static String URL_TERMS = URL + "/GDFServices/ConsumidorTerminos";
	public static String URL_UPDATE_PASS_MAIL = URL + "/AddCelBridge/Servicios/adc_userPasswordUpdateMail.jsp";
	
	public static String URL_RECOVERY_PASSWORD = URL + "/AddCelBridge/Servicios/adc_RecoveryUP.jsp";
	
	public static String URL_RECOVERY_PASSWORD_UPDATE = URL + "/AddCelBridge/Servicios/adc_userPasswordUpdate.jsp";
}
