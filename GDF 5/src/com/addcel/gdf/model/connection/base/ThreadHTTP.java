package com.addcel.gdf.model.connection.base;

import java.io.IOException;


import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.ui.UiApplication;

import org.xml.sax.SAXException;

import com.addcel.gdf.view.base.Viewable;


public class ThreadHTTP extends Thread {

	private HttpConfigurator httpConfigurator = null;
	private int request = 0;
	private com.addcel.gdf.view.base.Viewable viewable = null;
	private URLEncodedPostData encodedPostData = null;

	public void setHttpConfigurator(int request, Viewable viewable, URLEncodedPostData encodedPostData) {
		
		this.request = request;
		this.viewable = viewable;
		this.encodedPostData = encodedPostData;
	}

	public void run() {
		SingletonHTTP singleton = SingletonHTTP.getInstance();
		singleton.setConfiguration(request, viewable, encodedPostData);

		try {

			singleton.execute();

		} catch (net.rim.device.api.xml.parsers.ParserConfigurationException e) {
			e.printStackTrace();
			sendError("Aviso: El analizador de xml tuvo un error.");
		}
		/*
		catch (JSONException e) {
			e.printStackTrace();
			sendError("Aviso: Problemas con informacion recibida");
		}
		*/
		
		catch (IOException e) {
			e.printStackTrace();
			sendError("Aviso: Problemas con conexi�n al servidor");
		}

	}

	private void sendError(String error) {
		/*
		 * actionError = new Action();
		 * 
		 * actionError.createError(error);
		 */
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				// controller.receiveAction(actionError);
			}
		});
	}
}