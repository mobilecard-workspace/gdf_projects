package com.addcel.gdf.model.connection.base;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.io.http.HttpProtocolConstants;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.xml.parsers.ParserConfigurationException;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.view.base.Viewable;

public class SingletonHTTP {

	private static SingletonHTTP instance = null;
	private HttpConfigurator httpConfigurator = null;

	private HttpConnection httpConnection;
	private InputStream inputStream;

	private int request = 0;
	private com.addcel.gdf.view.base.Viewable viewable = null;
	private URLEncodedPostData encodedPostData = null;

	private SingletonHTTP() {
	}

	public void setConfiguration(int request, Viewable viewable,
			URLEncodedPostData encodedPostData) {

		this.request = request;
		this.viewable = viewable;
		this.encodedPostData = encodedPostData;
	}

	public synchronized static SingletonHTTP getInstance() {
		if (instance == null) {
			synchronized (SingletonHTTP.class) {
				SingletonHTTP inst = instance;
				if (inst == null) {
					synchronized (SingletonHTTP.class) {
						instance = new SingletonHTTP();
					}
				}
			}
		}
		return instance;
	}

	public void execute() throws ParserConfigurationException, 
			IOException {

		httpConfigurator = new HttpConfigurator();

		String url = httpConfigurator.getURL(request);

		try {

			String id = "0";
			StringBuffer stringBuffer = new StringBuffer();

			httpConnection = (HttpConnection) Connector.open(url);

			httpConnection.setRequestMethod(HttpConnection.POST);
			httpConnection.setRequestProperty(
					HttpProtocolConstants.HEADER_CONTENT_TYPE,
					encodedPostData.getContentType());
			System.out.println(url + "?" + encodedPostData.toString());
			byte[] postBytes = encodedPostData.getBytes();
			httpConnection.setRequestProperty(
					HttpProtocolConstants.HEADER_CONTENT_LENGTH,
					Integer.toString(postBytes.length));
			OutputStream strmOut = httpConnection.openOutputStream();
			strmOut.write(postBytes);
			strmOut.flush();
			strmOut.close();

			inputStream = httpConnection.openInputStream();
			int c;
			while ((c = inputStream.read()) != -1) {

				stringBuffer.append((char) c);
			}

			System.out.println(stringBuffer.toString());

			try {

				final JSONObject jsObject = new JSONObject(stringBuffer.toString());
				
				UiApplication.getUiApplication().invokeLater(new Runnable() {
					public void run() {

						viewable.setData(request, jsObject);
					}
				});
				
				
			} catch (JSONException e) {

				e.printStackTrace();
				final JSONObject jsObject = null;
				
				UiApplication.getUiApplication().invokeLater(new Runnable() {
					public void run() {

						viewable.setData(request, jsObject);
					}
				});
			}

		} finally {

			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (httpConnection != null) {
					httpConnection.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

/*
 * public synchronized void execute(String i) throws
 * ParserConfigurationException, SAXException, IOException, JSONException {
 * 
 * String xml = null; HttpConnection httpConnection = null; InputStream in =
 * null; try {
 * 
 * if (httpConfigurator == null) { httpConfigurator = new HttpConfigurator(); }
 * httpConnection = (HttpConnection) Connector.open(httpConfigurator
 * .getSource()); in = httpConnection.openInputStream();
 * 
 * httpConnection.setRequestMethod(HttpConnection.POST);
 * 
 * OutputStream dos = httpConnection.openOutputStream(); dos.write(postByte);
 * dos.flush(); dos.close();
 * 
 * String destino = null; int available = in.available(); OutputStream out = new
 * FileOutputStream(available, destino); byte[] buffer = new byte[available];
 * while (true) { int n = in.read(buffer); if (n < 0) break; out.write(buffer,
 * 0, n); } in.close(); out.close();
 * 
 * System.out.println(destino); JSONObject jsObject = new JSONObject(destino);
 * 
 * UiApplication.getUiApplication().invokeLater(new Runnable() { public void
 * run() { // controller.receiveAction(action); } });
 * 
 * } finally {
 * 
 * if (in != null) { try { in.close(); } catch (Exception e) { } }
 * 
 * if (httpConnection != null) { try { httpConnection.close(); } catch
 * (Exception e) { } } }
 * 
 * }
 */