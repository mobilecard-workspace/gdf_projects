package com.addcel.gdf.model.connection.base;

import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.view.base.Viewable;





public class HttpConfigurator {

	private String url = "http://201.161.23.42:8085/ETNWSConsumer/";
	//private String url = "http://localhost/ETNWSConsumer/";
	
	private String connectionType = "";

	public HttpConfigurator() {

		String idealConnection = UtilBB.checkConnectionType();
		
		connectionType = ";" + idealConnection + ";"
				+ "ConnectionTimeout=30000";
	}


	public String getURL(int request) {

		String url = null;

		switch (request) {
		case Viewable.DEPART:
			url = this.url + "ObtenerOrigenes" + connectionType;
			break;

		case Viewable.ARRIVE:
			url = this.url + "ObtenerDestinos" + connectionType;
			break;

		case Viewable.SCHEDULED:
			url = this.url + "ObtenerCorridas" + connectionType;
			break;

		case Viewable.VROUTE:
			url = this.url + "ConsultarItinerario" + connectionType;
			break;

		case Viewable.SEE_DIAGRAM:
			url = this.url + "ConsultarDiagrama" + connectionType;
			break;

		case Viewable.SEE_OCCUPANCY:
			url = this.url + "ConsultarOcupacion" + connectionType;
			break;

		case Viewable.CREATE:
			url = this.url + "ConsultarReservacion" + connectionType;
			break;
		}

		return url;
	}
}


