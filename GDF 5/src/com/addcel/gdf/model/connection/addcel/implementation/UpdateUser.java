package com.addcel.gdf.model.connection.addcel.implementation;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.utils.UtilSecurity;
import com.addcel.gdf.utils.Utils;
import com.addcel.gdf.view.base.Viewable;

public class UpdateUser extends HttpListener {

	
	private String password;
	
	public UpdateUser(String post, Viewable viewable, String URL) {
		super(post, URL, viewable);
	}

	public UpdateUser(String post, Viewable viewable) {
		super(post, Url.URL_GDF, viewable);
	}

	public UpdateUser(Viewable viewable, String url, String post) {
		super(viewable, url, post);
	}

	
	public UpdateUser(Viewable viewable, String url, String post, String password) {
		super(viewable, url, post);
		this.password = password;
	}
	
	public void receiveHttpResponse(int appCode, byte[] response) {

		JSONObject jsObject = null;
		String json = new String(response);
		
		//String password = UserBean.password;
		
		json = UtilSecurity.aesDecrypt(Utils.parsePass(password), json);

		try {
			
			jsObject = new JSONObject(json);
			sendData(jsObject);
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessageError("Error en lectura de JSON");
		}
	}
}


