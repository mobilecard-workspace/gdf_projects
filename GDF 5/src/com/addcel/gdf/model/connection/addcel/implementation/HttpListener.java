package com.addcel.gdf.model.connection.addcel.implementation;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;

import org.json.me.JSONObject;

import com.addcel.gdf.view.base.Viewable;

public abstract class HttpListener {

	private String url;
	private HttpPoster httpPoster;
	private String post;
	private Viewable viewable;
	
	private boolean select = false; 

	public HttpListener(String post, String url, Viewable viewable) {

		httpPoster = new HttpPoster();
		this.post = post;
		this.viewable = viewable;
		this.url = url;
		select = true;
	}

	
	public HttpListener(Viewable viewable, String url, String post) {

		httpPoster = new HttpPoster();
		this.post = post;
		this.viewable = viewable;
		this.url = url;
	}
	
	public void run() {
		connect();
	}

	public void connect() {

		try {

			
			if (select){
				httpPoster.sendHttpRequest(this, this.url, this.post);
			} else {
				
				if (this.post == null){
					httpPoster.sendHttpRequest(this, this.url);
				} else {
					
					String url = this.url + "?" + this.post;
					httpPoster.sendHttpRequest(this, url);
				}
			}

		} catch (Exception e) {

			e.printStackTrace();
			sendMessageError();
		}
	}

	public void sendMessageError() {

		synchronized (Application.getEventLock()) {
			viewable.sendMessage("Error");
		}
	}


	public void handleHttpError(int errorCode, String error) {
		sendMessageError(error);
	}


	public void sendMessageError(final String error) {

		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {

				viewable.sendMessage(error);
			}
		});
		
		
	}


	public void sendData(JSONObject jsObject) {

		
		final JSONObject jsObject01 = jsObject;

		
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {

				viewable.setData(0, jsObject01);
			}
		});
	}


	public abstract void receiveHttpResponse(int appCode, byte[] response);

}
