package com.addcel.gdf.model.connection.addcel.base;

import java.util.Vector;

/**
 *
 * @author Administrador
 */

public interface HttpListener {
    
    public void receiveHttpResponse(int appCode, byte[] response);
    public void handleHttpError(int errorCode,String error);   
    public void receiveEstatus(String msg);
    public void receiveHeaders(Vector _headers);
    public boolean isDestroyed();
}
