package com.addcel.gdf.model.connection.addcel.implementation;

import java.io.UnsupportedEncodingException;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.base.Viewable;

public class GenericHTTPUpdatePass extends HttpListener {

	public GenericHTTPUpdatePass(String post, String url, Viewable viewable) {
		super(post, url, viewable);
	}

	
	public GenericHTTPUpdatePass(Viewable viewable, String url, String post){
		super(viewable, url, post);
	}
	
	
	public void receiveHttpResponse(int appCode, byte[] response) {

		JSONObject jsObject = null;
		String json;
		try {
			json = new String(response, "UTF-8");
			json = AddcelCrypto.decryptHard(json);

			String newString = new String(json.getBytes("UTF-8"), "UTF-8");

			String mensaje = null;
			
			if (newString.equals("0")){
				
				mensaje = "Se ha enviado un password nuevo a tu correo, revisalo por favor.";
				
			} else {
				
				mensaje = newString.substring(newString.indexOf("|") + 1, newString.length());
			}
			
			System.out.println(mensaje);
			
			jsObject = new JSONObject();
			
			jsObject.put("mensaje", mensaje);
			
			sendData(jsObject);
			
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			sendMessageError("Error en lectura de la respuesta");
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessageError("Error en lectura de la respuesta");
		}
	}
}