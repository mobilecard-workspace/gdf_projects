package com.addcel.gdf.model.connection.addcel.implementation;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.utils.add.AddcelCrypto;
import com.addcel.gdf.view.base.Viewable;

public class PagoPlacas extends HttpListener {

	public PagoPlacas(String post, Viewable viewable, String URL) {
		super(post, URL, viewable);
	}

	public PagoPlacas(String post, Viewable viewable) {
		super(post, Url.URL_GDF, viewable);
	}
	
	public PagoPlacas(Viewable viewable, String url, String post) {
		super(viewable, url, post);
	}

	public void receiveHttpResponse(int appCode, byte[] response) {

		JSONObject jsObject = null;
		String json = new String(response);

		json = AddcelCrypto.decryptSensitive(json);

		try {
			
			jsObject = new JSONObject(json);
			sendData(jsObject);
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessageError("Error en lectura de JSON");
		}
	}
}


