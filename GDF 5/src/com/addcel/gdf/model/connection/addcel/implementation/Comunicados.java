package com.addcel.gdf.model.connection.addcel.implementation;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.gdf.view.base.Viewable;

public class Comunicados extends HttpListener {


	public Comunicados(Viewable viewable, String url, String post) {
		super(viewable, url, post);
	}


	public void receiveHttpResponse(int appCode, byte[] response) {

		JSONObject jsObject = null;
		String json = new String(response);

		try {
			
			jsObject = new JSONObject(json);
			sendData(jsObject);
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessageError("Error en lectura de JSON");
		}
	}
}