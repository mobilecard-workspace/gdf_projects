package com.addcel.gdf.model.connection.addcel.InfoThread;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import com.addcel.gdf.dto.UserBean;
import com.addcel.gdf.model.connection.Url;
import com.addcel.gdf.model.connection.addcel.base.Communicator;
import com.addcel.gdf.model.connection.addcel.base.HttpListener;
import com.addcel.gdf.model.connection.addcel.json.JSONParser;
import com.addcel.gdf.utils.UtilBB;
import com.addcel.gdf.utils.UtilSecurity;

import net.rim.device.api.system.Application;


public class User implements HttpListener {
	
	public String url = Url.URL_GET_USERDATA;
	public String post = "";
	public String tipo = "";

	public User(String json) {

		this.post = "json=" + json;
	}

	public void run() {
		connect();
	}

	public void connect() {

		String idealConnection = UtilBB.checkConnectionType();

		if (idealConnection != null) {

			if (this.url != null) {
				try {

					Communicator communicator = Communicator.getInstance();
					communicator.sendHttpPost(this, this.url, this.post);
					
				} catch (Exception e) {

					e.printStackTrace();
					getMessageError();
				}
			}
		} else {
			/*
			
			MENSAJE INDICANDO QUE VUELVA A TRATAR.
			
			Error
			
			idealConnection = UtilBB.checkConnectionType();
			this.connect();
			*/
			
		}
	}

	public void handleHttpError(int errorCode, String error) {
		getMessageError();
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {
	}

	public void receiveHeaders(Vector _headers) {
	}

	public JSONParser jsParser = new JSONParser();

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		StringBuffer sb = new StringBuffer();

		try {

			sb.append(new String(response, 0, response.length, "UTF-8"));
			sTemp = new String(sb.toString());
			// sTemp = Crypto.aesDecrypt(UtilBB.parsePass(MainClass.p), sTemp);
			sTemp = UtilSecurity.aesDecrypt(UtilSecurity.parsePass(""), sTemp);
			System.out.println(sTemp);
			UserBean userBean = jsParser.getUser(sTemp);

			//UiApplication.getUiApplication();
			synchronized (Application.getEventLock()) {
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	public void getMessageError() {

		synchronized (Application.getEventLock()) {
			// MainClass.splashScreen.remove();
			// UiApplication.getUiApplication().pushScreen(new
			// MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde",
			// Field.NON_FOCUSABLE));
		}

	}

}
