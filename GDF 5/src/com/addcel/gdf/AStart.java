package com.addcel.gdf;

import org.json.me.JSONException;

import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ObjectChoiceField;

import com.addcel.gdf.view.VSplash;
import com.addcel.gdf.view.tramiteslinea.certificacionpagos.dto.Concepto;
import com.addcel.gdf.view.tramiteslinea.certificacionpagos.model.ConverterJsonToObject;

public class AStart extends UiApplication {

	
	private String jsonString = 
			
			"{	\"conceptos\": [	{		\"id_concepto\": 1,		\"concepto\": \"I.S.A.N\",		\"parametros\": [{			\"nombre_param\": \"RFC\",			\"tipo\": \"String\",			\"longitud_max\": \"15\"		},		{			\"nombre_param\": \"Nombre o Denominacion o Raz�n Social\",			\"tipo\": \"String\",			\"longitud_max\": \"100\"		}]	},	{		\"id_concepto\": 2,		\"concepto\": \"N�mina\",		\"parametros\": [{			\"nombre_param\": \"RFC\",			\"tipo\": \"String\",			\"longitud_max\": \"15\"		},		{			\"nombre_param\": \"Nombre o Denominacion o Raz�n Social\",			\"tipo\": \"String\",			\"longitud_max\": \"100\"		}]	}]}";
	

	
	
	public static void main(String[] args) {

		AStart theApp = new AStart();
		theApp.enterEventDispatcher();
	}

	public AStart() {

		/*
		try {
			
			

			
			ConverterJsonToObject jsonToObject = new ConverterJsonToObject();
			
			Concepto[] conceptos = jsonToObject.execute(jsonString);

			
			ObjectChoiceField choiceField = new ObjectChoiceField("example", conceptos);
			
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		
		pushScreen(new VSplash());
	}
}
